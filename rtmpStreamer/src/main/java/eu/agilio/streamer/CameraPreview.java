package eu.agilio.streamer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.drm.DrmStore;
import android.graphics.*;
import android.hardware.Camera;
import android.util.FloatMath;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import eu.agilio.streamer.manager.CameraManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bogdan on 06.01.2015.
 */
public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {
    private static final String TAG = "CameraPreview";
    private SurfaceHolder mHolder;
    private RtmpStreamer streamer;
    private CameraManager cameraManager;
    private Paint paint;
    private boolean drawingRect = false;
    private boolean actionPinch = false;
    private float initialDist;
    private int left;
    private int top;
    private int right;
    private int bottom;

    public CameraPreview(Context context, CameraManager manager) {
        super(context);
        this.cameraManager = manager;
        this.streamer = RtmpStreamer.getInstance();
        // Install a SurfaceHolder.Callback so we get notified when the
        // underlying surface is created and destroyed.
        mHolder = getHolder();
        cameraManager.createCameraParameters(streamer);
        mHolder.addCallback(this);
        setWillNotDraw(false);

        paint = new Paint();
        paint.setColor(0xffffffff);
        paint.setStrokeWidth(4);
        paint.setStyle(Paint.Style.STROKE);
    }

    public void surfaceCreated(SurfaceHolder holder) {
        Log.d(TAG, "surfaceCreated");
        // The Surface has been created, now tell the camera where to draw the preview.
        try {
            cameraManager.getCamera().setPreviewDisplay(this.getHolder());
            //mCamera.setPreviewDisplay(holder);
            cameraManager.cameraPreview();
            //mCamera.startPreview();
        } catch (IOException e) {
            Log.d(TAG, "Error setting camera preview: " + e.getMessage());
            Log.d(TAG, String.format(
                   "Failed starting camera with preview: %s", e));
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.d(TAG,"surfaceDestroyed");

        // empty. Take care of releasing the Camera preview in your activity.
        try {
            cameraManager.stopCameraPreview();
            //mCamera.stopPreview();
        } catch (Exception e) {
            // ignore: tried to stop a non-existent preview
        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        Log.d(TAG, "surfaceChanged");

        // If your preview can change or rotate, take care of those events here.
        // Make sure to stop the preview before resizing or reformatting it.
        if (mHolder.getSurface() == null) {
            // preview surface does not exist
            return;
        }
        // stop preview before making changes
        try {
            cameraManager.stopCameraPreview();
            //mCamera.stopPreview();
        } catch (Exception e) {
            // ignore: tried to stop a non-existent preview
        }
        Log.d(TAG,"actual width: "+getWidth()+", actual height: "+getHeight());
        Log.d(TAG, "width: " + w + ", height: " + h);
        // set preview size and make any resize, rotate or
        // reformatting changes here

        // start preview with new settings

        try {
            cameraManager.getCamera().setPreviewDisplay(mHolder);
            cameraManager.cameraPreview();

        } catch (Exception e) {
            Log.d(TAG, "Error starting camera preview: " + e.getMessage());
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getPointerCount() > 1) {
            actionPinch = true;
            if (event.getPointerCount() == 2) {
               pinchZoom(event);
            }
        } else if (event.getAction() == MotionEvent.ACTION_UP && !drawingRect) {
            if (actionPinch) {
                actionPinch = false;
                return true;
            }
            tapToFocus(event);
        }
        return true;
    }

    public void tapToFocus(MotionEvent event) {
        Camera camera = cameraManager.getCamera();
        camera.cancelAutoFocus();
        float x = event.getX();
        float y = event.getY();
        new Thread(new FocusRectangle((int) x, (int) y)).start();
        Rect focusRect = calculateTapArea(x, y, 1f);
        Camera.Parameters parameters = camera.getParameters();
        List<String> focusModes = parameters.getSupportedFocusModes();
        if (focusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
        }
        if (parameters.getMaxNumFocusAreas() > 0) {
            List<Camera.Area> myList = new ArrayList<Camera.Area>();
            myList.add(new Camera.Area(focusRect, 900));
            parameters.setFocusAreas(myList);
        }
        try {
            camera.setParameters(parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        camera.autoFocus(new Camera.AutoFocusCallback() {
            @Override
            public void onAutoFocus(boolean success, Camera camera) {
                camera.cancelAutoFocus();
                Camera.Parameters parameters = camera.getParameters();
                List<String> focusModes = parameters.getSupportedFocusModes();
                if (focusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
                    parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
                    camera.setParameters(parameters);
                }
            }
        });
    }

    public void pinchZoom(MotionEvent event) {
        Camera camera = cameraManager.getCamera();
        if (event.getActionMasked() == MotionEvent.ACTION_POINTER_DOWN) {
            initialDist = getFingerSpacing(event);
        } else if (event.getAction() == MotionEvent.ACTION_MOVE && camera.getParameters().isZoomSupported()) {
            camera.cancelAutoFocus();
            handleZoom(event, camera.getParameters());
        }
    }

    private void handleZoom(MotionEvent event, Camera.Parameters params) {
        int maxZoom = params.getMaxZoom();
        int zoom = params.getZoom();
        float newDist = getFingerSpacing(event);
        if (newDist > initialDist + 5) {
            initialDist = newDist;
            if (zoom < maxZoom - 1)
                zoom += 2;
        } else if (newDist < initialDist - 5) {
            initialDist = newDist;
            if (zoom > 0)
                zoom -= 2;
        }
        params.setZoom(zoom);
        cameraManager.getCamera().setParameters(params);
    }

    private float getFingerSpacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return FloatMath.sqrt(x * x + y * y);
    }

    private Rect calculateTapArea(float x, float y, float coefficient) {
        float focusAreaSize = 300;
        int areaSize = Float.valueOf(focusAreaSize * coefficient).intValue();

        int left = validate(Float.valueOf((x / this.getWidth()) * 2000 - 1000).intValue(), areaSize);
        int top = validate(Float.valueOf((y / this.getHeight()) * 2000 - 1000).intValue(), areaSize);

        RectF rectF = new RectF(left, top, left + areaSize, top + areaSize);

        return new Rect(Math.round(rectF.left), Math.round(rectF.top), Math.round(rectF.right), Math.round(rectF.bottom));
    }

    private int validate(int touchCoordinate, int focusAreaSize) {
        if (touchCoordinate + focusAreaSize / 2 > 1000) {
            return 1000 - focusAreaSize * 3 / 2;
        }
        if (touchCoordinate - focusAreaSize / 2 < -1000) {
            return -1000 + focusAreaSize / 2;
        }
        return touchCoordinate - focusAreaSize / 2;
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (drawingRect) {
            canvas.drawRect(left, top, right, bottom, paint);
        }
    }

    class FocusRectangle implements Runnable {

        public static final int WIDTH = 60;
        public static final int HEIGHT = 35;

        public FocusRectangle(int x, int y) {
            drawingRect = true;
            left = x - WIDTH;
            top = y - HEIGHT;
            right = x + WIDTH;
            bottom = y + HEIGHT;
        }

        @Override
        public void run() {
            for (int i = 0; i <= 25; ++i) {
                if (i <= 9) {
                    -- left;     -- top;
                    ++ right;    ++ bottom;
                } else {
                    ++ left;     ++ top;
                    -- right;    -- bottom;
                }
                postInvalidate();
                try {
                    Thread.sleep(40);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            drawingRect = false;
            postInvalidate();
        }
    }
}
