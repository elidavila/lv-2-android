package eu.agilio.streamer.callback;

/**
 * Created by Bogdan on 06.03.2015.
 */
public interface ConnectionNotifier {

    public void timeout();
}
