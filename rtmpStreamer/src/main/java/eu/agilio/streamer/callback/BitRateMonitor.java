package eu.agilio.streamer.callback;

/**
 * Created by Bogdan on 14.12.2015.
 */
public interface BitRateMonitor {

    public void bitrateChanged(int initialValue, int currentValue);

}
