package eu.agilio.streamer.callback;

/**
 * Created by Bogdan on 03.03.2015.
 */
public interface StreamerStateCallback {
    void event(int state);
}
