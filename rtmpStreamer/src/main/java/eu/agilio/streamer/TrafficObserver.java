package eu.agilio.streamer;

import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Bogdan on 21.01.2015.
 */
public class TrafficObserver {
    private static final String TAG = "TrafficObserver";
    private static TrafficObserver instance = null;
    private final long SAMPLING_RATE = 850;
    private TimerTask timerTask;
    private Timer timer;
    private AtomicInteger treshHold;
    private int offset;
    private int increasingCounter = 0;
    private int decreasingCounter = 0;
    private AtomicBoolean hasBeenStarted = new AtomicBoolean(false);

    private TrafficObserver() {
    }

    public static TrafficObserver getInstance() {
        if (instance == null) {
            instance = new TrafficObserver();
        }
        return instance;
    }

    public void startMonitoring() {
        treshHold = new AtomicInteger(1);
        offset = 0;
        increasingCounter = 0;
        decreasingCounter = 0;
        timer = new Timer();
        timerTask = new TimerTask() {

            @Override
            public void run() {
                getPacketStatus();
            }
        };
        if (timer != null && timerTask != null) {
            timer.schedule(timerTask, 0, SAMPLING_RATE);
            hasBeenStarted.set(true);
        }
    }

    public void cancelMonitoring() {
        if (hasBeenStarted.get()) {
            timer.cancel();
            hasBeenStarted.set(false);
        }
    }

    private void getPacketStatus() {
        int counter = FfmpegNdk.readFrameCounter();
        Log.d(TAG, "counter: " + counter);

        if (counter <= treshHold.get()) {
            increasingCounter++;
        } else {
            increasingCounter = 0;
        }

        if (counter >= treshHold.get() + offset) {
            decreasingCounter++;
        } else {
            decreasingCounter = 0;
        }

//        Log.d(TAG, "getPacketStatus after increments ");
//        Log.d(TAG, "counter: " + counter);
//        Log.d(TAG, "treshHold: " + treshHold.get());
//        Log.d(TAG, "increasingCounter: " + increasingCounter);
//        Log.d(TAG, "decreasingCounter: " + decreasingCounter);
//        Log.d(TAG, "---------------------------");

        if (increasingCounter == 3) {
            RtmpStreamer.getInstance().increaseBitRate();
            increasingCounter = 0;
        }

        if (decreasingCounter == 3) {
            RtmpStreamer.getInstance().decreaseBitRate();
            decreasingCounter = 0;
        }
    }
}
