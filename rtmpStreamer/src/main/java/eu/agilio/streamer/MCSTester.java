package eu.agilio.streamer;

import android.hardware.Camera;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import eu.agilio.streamer.encoders.TestEncoder;
import eu.agilio.streamer.manager.CameraManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * User: Voicu
 * Since: 7/7/14 6:31 PM
 */
public class MCSTester {

    public static final int FORMAT_UNKNOWN = -100;
    public static final int FORMAT_NV12 = 1;
    public static final int FORMAT_NV21 = 2;
    public static final int FORMAT_YV12 = 3;
    public static final int FORMAT_YUV420 = 4;
    private static final String TAG = "MCS_Tester_Java";
    // color format is not resolution dependent (if it is, I fucking give up)
    private static int colorFormat = FORMAT_UNKNOWN;
    private static List<PerResolutionSettings> perResolutionSettingses; // Gollum!
    private static File SETTINGS_FILE = new File(Environment.getExternalStorageDirectory() + "/eu_agilio_android_rtmp.settings");

    static List<byte[]> doStreamerTest(final String name, TestImageFactory testImageFactory, int width, int height) {
        return doStreamerTest(name, testImageFactory.getTestImages(), width, height);
    }

    public static byte[] convertImageIfNeeded(byte[] image, int width, int height) {
        if (hasLinePadding(width, height)) {
            Log.d(TAG, String.format("adding line padding to image of size %d", image.length));
            image = Utils.addYUVLinePadding(image, width, height, getLinePaddingSize(width, height));
        }
        if (hasPlanePadding(width, height)) {
            Log.d(TAG, String.format("adding plane padding to image of size %d", image.length));
            image = Utils.addYUVPlanePadding(image);
        }
        return image;
    }

    static List<byte[]> doStreamerTest(final String name, final List<byte[]> images, int width, int height) {
        final int[] streamerTestResult = new int[1];
        List<byte[]> results = new ArrayList<byte[]>();

        Log.e(TAG, "before enqueuTestimages");
        TestEncoder encoder = new TestEncoder(width, height);
        encoder.performTest(images);

        Log.e(TAG, "after enqueuTestimages");
        streamerTestResult[0] = FfmpegNdk.testVideo(images.size());


        if (streamerTestResult[0] < 0) {
            Log.d(TAG, String.format("%s: Failed in streamer test: error code %d", name, streamerTestResult[0]));
            return null;
        }

        Log.d(TAG, String.format("%s: tester error code is OK", name));

        for (int i = 0; i < images.size(); i++) {
            byte[] result = new byte[width * height * 3 / 2];
            FfmpegNdk.getTestFrame(result, i);
            results.add(result);
        }

        FfmpegNdk.cleanupAfterTest(images.size());

        return results;
    }


    static int testLinePadding(int width, int height) {
        Log.d(TAG, String.format("testing line padding"));
        byte[] original = SampleImage.getAsYuv(width, height);
        // CustomLogger.sendImage(original, String.format("LinePadding_%d_%d_original", width, height));
        List<byte[]> testImages = new ArrayList<byte[]>();
        testImages.add(original);

        Log.d(TAG, String.format("starting test: %dx%d", width, height));
        List<byte[]> result_ = doStreamerTest("line padding test", testImages, width, height);
        if (result_ == null) {
            return -1;
        }
        Log.d(TAG, String.format("test returned succesfully"));
        byte[] result = result_.get(0);
        int diff = 0, threshold;
        int pc = width * height; // pixel count
        Log.d(TAG, String.format("calculating difference between input and output image"));
        for (int i = 0; i < pc; i++) {
            diff += Math.abs((original[i] & 0xFF) - (result[i] & 0xFF));
        }
        // CustomLogger.sendImage(result, String.format("LinePadding_%d_%d_result", width, height));
        Log.d(TAG, String.format("difference is %d", diff));
        threshold = pc * 8;
        if (diff > threshold) {
            PerResolutionSettings settings = getPerResolutionSettings(width, height);
            int[] paddingSizes = new int[5];
            paddingSizes[0] = 16;
            paddingSizes[1] = 32;
            paddingSizes[2] = 48;
            paddingSizes[3] = 64;
            paddingSizes[4] = 96;

            Log.d(TAG, String.format("%dx%d: difference is over threshold (%s), doing more checks", width, height, threshold));

            for (int padding : paddingSizes) {
                diff = 0;
                int delta = 0;

                for (int i = 0; i < pc; i++) {
                    diff += Math.abs((original[delta + i] & 0xFF) - (result[i] & 0xFF));
                    if (i % width == width - 1) {
                        delta += padding;
                    }
                }
                Log.d(TAG, String.format("%dx%d: difference with padding %d is %d", width, height, padding, diff));
                if (diff < threshold) {
                    Log.d(TAG, String.format("%dx%d: second difference confirms line padding of %d", width, height, padding));
                    settings.hasLinePadding = true;
                    settings.linePaddingSize = padding;
                }
            }
            if (!settings.hasLinePadding) {
                Log.d(TAG, String.format("Couldn't find a matching line padding for %dx%d", width, height));
            }
        } else {
            Log.d(TAG, String.format("difference give high probability there is no line padding on %dx%d", width, height));
        }

        return 0;
    }

    /**
     * According to this comment https://groups.google.com/forum/#!msg/android-platform/awaNwgb6EbY/a-YiIOwaL0QJ
     * we might have a padding between the Y plane and UV plane of the NV12/NV21 formats. This padding appears for
     * some Qualcomm devices. The encoders on those devices expect the chroma plane (UV) to be aligned to the 2048
     * byte boundary. As such we create an image in a resolution that is not a multiple of 2048.
     * e.g. 320x240: 320*240 = 76800 = 2048 * 37.5, which means that on said Qualcomm devices there must be a
     * padding of 1024 bytes between the luminance (Y) and chroma (UV) planes.
     * <p/>
     * By creating an image which has the 2048 bytes immediately after the luminance plane set to a high value (127)
     * we test whether the encoder uses the first 1024 bytes or not. If it doens't use those bytes and it means the
     * encoder expects the padding. We test whether the encoder skips or uses those bytes by looking at the chroma
     * values returned after we decode the MediaCodec encoded frame.
     * <p/>
     * Specifically, for 800x480, if the encoder needs the 2048 byte padding the resulting chroma sum will be about
     * half of what we give it. If it doesn't need the padding, the resulting sum will be about the same. The result
     * is not exact because the h264 encoding is lossy which means we need to test whether the values are inside a
     * threshold.
     * <p/>
     * One must take care in choosing the resolution with which to test this behavior because some resolutions'
     * pixel counts are multiples of 2048, while other might (not sure yet) have require a different padding size
     * (i.e. not 1024).
     */
    static int testPlanePadding(int width, int height) {
        final int value = 127;
        final int pc = width * height;
        Log.d(TAG, String.format("doing plane padding test: %dx%d value=%d", width, height, value));
        List<byte[]> result_ = doStreamerTest("plane padding test", new TestImageFactory() {
            @Override
            public List<byte[]> getTestImages() {
                List<byte[]> images = new ArrayList<byte[]>();
                byte[] image = new byte[pc * 3 / 2];
                for (int i = 0; i < pc * 3 / 2; i++) {
                    if (i >= pc && i < pc + 2048) {
                        image[i] = (byte) value;
                    } else {
                        image[i] = 0;
                    }
                }
                images.add(image);
                return images;
            }
        }, width, height);
        if (result_ == null) {
            Log.d(TAG, String.format("test failed"));
            return -1;
        }
        Log.d(TAG, String.format("test completed succesfully - checking results"));
        byte[] result = result_.get(0);
       // CustomLogger.sendImage(result, String.format("PlanePadding_%d_%d", width, height));
        int count = 0, threshold = 100;

        for (int i = 0; i < pc / 2; i++) {
            if ((result[pc + i] & 0xFF) > threshold) {
                count += 1;
            }
        }
        int requiredCount = 2048;
        if (colorFormat == FORMAT_YUV420 || colorFormat == FORMAT_YV12) {
            requiredCount = 1024;
        }
        if (count != requiredCount) {
            Log.d(TAG, String.format("count of values over threshold is %d which means there is a plane padding", count));
            getPerResolutionSettings(width, height).hasPlanePadding = true;
        } else {
            Log.d(TAG, String.format("count of values is %d which disproves that the encoder has a plane padding", count));
        }

        return 0;

    }

    /**
     * The test here verifies whether the encoder uses an NV12, NV21, YUV420 or YV12 format for input.
     * <p/>
     * We test this by comparing the input images' color counts with the images decoded (by ffmpeg) after being
     * encoded by MediaCodec.
     * The first image is given as full blue (U = max) in the YUV format, the second image is given as full blue
     * in the NV12 format.
     * <p/>
     * The checks below test for all four known options NV12, NV21, YUV420 and YV12. Although we now always use the
     * YUV420SemiPlanar format (i.e. NV12 or NV21) when we create the MediaCodec there might be a possibility that
     * the YUV420Planar format might be used in the future, that's why the two unused tests are written here.
     * <p/>
     * The color counts are very calculated in a basic way - all the chroma values in the image are summed together.
     * This is good enough for the few options we need to go through. If a more complex format appears in the future
     * or a lot more formats are going to be used by the encoders, a different approach might be required.
     */
    static int testColorFormat() {
        final int width = 640, height = 480, value = 127;
        final int pc = width * height;

        List<byte[]> result_ = doStreamerTest("color format test", new TestImageFactory() {
            @Override
            public List<byte[]> getTestImages() {
                List<byte[]> images = new ArrayList<byte[]>();
                images.add(getTestImage1(width, height, value));
                images.add(getTestImage2(width, height, value));
                return images;
            }
        }, width, height);
        if (result_ == null) {
            return -1;
        }
        Log.d(TAG, String.format("test finished succesfully"));

        //CustomLogger.sendImage(result_.get(0), String.format("ColorFormat_%d_%d_YUV_result", width, height));
       // CustomLogger.sendImage(result_.get(1), String.format("ColorFormat_%d_%d_NV12_result", width, height));

        int u1, v1, u2, v2;
        u1 = FfmpegNdk.getTestU(0);
        v1 = FfmpegNdk.getTestV(0);
        u2 = FfmpegNdk.getTestU(1);
        v2 = FfmpegNdk.getTestV(1);
        Log.d(TAG, String.format("u/v = %d/%d, %d/%d", u1, v1, u2, v2));

        int TOTAL_VALUE = pc / 4 * value;
        int LOWER_BOUND = TOTAL_VALUE / 16;
        int UPPER_BOUND = TOTAL_VALUE - LOWER_BOUND;
        int LOWER_HALF_BOUND = TOTAL_VALUE / 2 - LOWER_BOUND;
        int UPPER_HALF_BOUND = TOTAL_VALUE / 2 + LOWER_BOUND;

        if (u1 > LOWER_HALF_BOUND && u1 < UPPER_HALF_BOUND && v1 > LOWER_HALF_BOUND && v1 < UPPER_HALF_BOUND) {
            if (u2 > UPPER_BOUND && v2 < LOWER_BOUND) {
                colorFormat = FORMAT_NV12;
            } else if (u2 < LOWER_BOUND && v2 > UPPER_BOUND) {
                colorFormat = FORMAT_NV21;
            }
        }
        if (u2 > LOWER_HALF_BOUND && u2 < UPPER_HALF_BOUND && v2 > LOWER_HALF_BOUND && v2 < UPPER_HALF_BOUND) {
            if (u1 > UPPER_BOUND && v1 < LOWER_BOUND) {
                colorFormat = FORMAT_YUV420;
            } else if (u1 < LOWER_BOUND && v1 > UPPER_BOUND) {
                colorFormat = FORMAT_YV12;
            }
        }

        return 0;
    }

    static byte[] getTestImage1(int width, int height, int value) {
        byte[] pictureBuffer = new byte[width * height * 3 / 2];
        int pc = width * height;
        // setup for YUV format (full blue)
        Log.d(TAG, String.format("setting up first frame (full blue YUV)"));

        for (int i = 0; i < pc; i++) {
            pictureBuffer[i] = 0;
        }
        for (int i = 0; i < pc / 4; i++) {
            pictureBuffer[pc + i] = (byte) value;
            pictureBuffer[pc + pc / 4 + i] = (byte) 0;
        }

        return pictureBuffer;
    }

    static byte[] getTestImage2(int width, int height, int value) {
        byte[] pictureBuffer = new byte[width * height * 3 / 2];
        int pc = width * height;

        // setup for NV12 format (also full blue)
        // Y value is kept from previous format
        Log.d(TAG, String.format("setting up second frame (full blue NV12)"));

        for (int i = 0; i < pc; i++) {
            pictureBuffer[i] = 0;
        }
        for (int i = 0; i < pc / 4; i++) {
            pictureBuffer[pc + 2 * i] = (byte) value;
            pictureBuffer[pc + 2 * i + 1] = (byte) 0;
        }
//        for (int i=0; i<width/2; i++) {
//            for (int j=0; j<height/2; j++) {
//                pictureBuffer[pc+2*(j*width/2+i)] = (byte)(240 * i / (width / 2));
//                //pictureBuffer[pc+2*(j*width/2+i)+1] = (byte)(240 * j / (height / 2));
//            }
//        }

        return pictureBuffer;
    }

    static int testMediaCodec(boolean useBackCamera, CameraManager cameraManager) {
        if (!SETTINGS_FILE.exists()) {
            try {
                perResolutionSettingses = new ArrayList<PerResolutionSettings>();
                //TODO find a sollution for the problem() innitially it was RtmpStreamer.getResolutions()
                for (Camera.Size size : cameraManager.getResolutions(useBackCamera)) {
                    Log.d(TAG, String.format("testing encoder for resolution %dx%d", size.width, size.height));

                    // constructors are overrated anyway
                    PerResolutionSettings settings = new PerResolutionSettings();
                    settings.width = size.width;
                    settings.height = size.height;
                    perResolutionSettingses.add(settings);

                    testLinePadding(size.width, size.height);
                    // testPlanePadding(size.width, size.height);
                }
                testColorFormat();
            } catch (Exception e) {
                Log.d(TAG, String.format("failed during tests"), e);
                return -1;
            }

            Log.d(TAG, String.format("saving test results to settings file"));
            JSONObject json = new JSONObject();
            try {
                json.put("apiVersionUsed", Build.VERSION.SDK_INT);
                json.put("colorFormat", colorFormat);
                JSONArray perResolutionSettings_json = new JSONArray();
                for (PerResolutionSettings settings : perResolutionSettingses) {
                    JSONObject settings_json = new JSONObject();
                    settings_json.put("width", settings.width);
                    settings_json.put("height", settings.height);
                    settings_json.put("hasLinePadding", settings.hasLinePadding);
                    settings_json.put("linePaddingSize", settings.linePaddingSize);
                    settings_json.put("hasPlanePadding", settings.hasPlanePadding);
                    perResolutionSettings_json.put(settings_json);
                }
                json.put("perResolutionSettings", perResolutionSettings_json);

                FileOutputStream fos = new FileOutputStream(SETTINGS_FILE);

                Log.d(TAG, String.format("writing to file [%s] the following json \n%s\n", SETTINGS_FILE.getAbsolutePath(), json.toString(4)));
                fos.write(json.toString(4).getBytes());

                fos.close();
            } catch (JSONException e) {
                Log.d(TAG, String.format("Failed creating json to save settings"), e);
            } catch (Exception e) {
                Log.d(TAG, String.format("Failed creating settings file"), e);
            }
        } else {
            try {
                Log.d(TAG, String.format("Loading codec information from settings file"));

                FileInputStream fis = new FileInputStream(SETTINGS_FILE);
                byte[] buffer = new byte[fis.available()];
                fis.read(buffer);
                fis.close();
                String json_str = new String(buffer);
                Log.d(TAG, String.format("The following json was read from [%s]: \n%s\n", SETTINGS_FILE.getAbsolutePath(), json_str));
                JSONObject object = new JSONObject(json_str);

                int apiVersionUsed = object.getInt("apiVersionUsed");
                if (apiVersionUsed < Build.VERSION.SDK_INT) {
                    Log.d(TAG, String.format("Settings file has an older api version (%d in file, %d current), redoing tests...",
                            apiVersionUsed, Build.VERSION.SDK_INT));

                    SETTINGS_FILE.delete();
                    return testMediaCodec(useBackCamera, cameraManager);
                }
                colorFormat = object.getInt("colorFormat");
                perResolutionSettingses = new ArrayList<PerResolutionSettings>();
                JSONArray perResolutionSettings_json = object.getJSONArray("perResolutionSettings");
                for (int i = 0; i < perResolutionSettings_json.length(); i++) {
                    JSONObject settings_json = perResolutionSettings_json.getJSONObject(i);

                    PerResolutionSettings settings = new PerResolutionSettings();
                    settings.width = settings_json.getInt("width");
                    settings.height = settings_json.getInt("height");
                    settings.hasLinePadding = settings_json.getBoolean("hasLinePadding");
                    settings.linePaddingSize = settings_json.getInt("linePaddingSize");
                    settings.hasPlanePadding = settings_json.getBoolean("hasPlanePadding");

                    perResolutionSettingses.add(settings);
                }
            } catch (Exception e) {
                Log.d(TAG, String.format("failed reading json with settings"), e);

                // delete settings and retry (this will redo the tests and properly save the settings)
                SETTINGS_FILE.delete();
                return -1;
            }
        }

        return 0;
    }

    static int getColorFormat() {
        return colorFormat;
    }

    static PerResolutionSettings getPerResolutionSettings(int width, int height) {
        for (PerResolutionSettings settings : perResolutionSettingses) {
            if (settings.width == width && settings.height == height) {
                return settings;
            }
        }
        return null;
    }

    static boolean hasLinePadding(int width, int height) {
        PerResolutionSettings settings = getPerResolutionSettings(width, height);
        return settings != null && settings.hasLinePadding;
    }

    static int getLinePaddingSize(int width, int height) {
        PerResolutionSettings settings = getPerResolutionSettings(width, height);
        if (settings == null) {
            return -1;
        }
        return settings.linePaddingSize;
    }

    static boolean hasPlanePadding(int width, int height) {
        PerResolutionSettings settings = getPerResolutionSettings(width, height);
        return settings != null && settings.hasPlanePadding;
    }

    public static abstract class TestImageFactory {
        public abstract List<byte[]> getTestImages();
    }

    public static class PerResolutionSettings {
        int width, height;
        boolean hasLinePadding = false;
        int linePaddingSize = -1;
        boolean hasPlanePadding = false;
    }

}
