package eu.agilio.streamer.muxer;

import android.media.MediaCodec;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.util.Log;
import eu.agilio.streamer.callback.MovieCreatorCallBack;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * Created by Bogdan on 21.03.2015.
 */
public class FileMuxerWorker implements Runnable{

    private static final String TAG = "FileMuxerWorker";
    private String audioFile;
    private String videoFile;
    private String movieFile;

    private MediaExtractor audioExtractor = null;
    private MediaExtractor videoExtractor = null;

    private MediaFormat audioFormat;
    private MediaFormat videoFormat;

    private MovieMuxer muxer;
    private MovieCreatorCallBack callback;

    public FileMuxerWorker(String audioFile, String videoFile, String movieFile, MovieCreatorCallBack callback){
        this.audioFile = audioFile;
        this.videoFile = videoFile;
        this.movieFile = movieFile;
        this.callback = callback;
        this.audioExtractor = new MediaExtractor();
        this.videoExtractor = new MediaExtractor();
        this.muxer = new MovieMuxer(movieFile);
    }

    @Override
    public void run() {
        try {
            if(callback!=null){
                callback.movieCreationStarted();
            }
            audioExtractor.setDataSource(audioFile);
            audioFormat = selectTrack(audioExtractor);
            if (audioFormat == null) {
                throw new RuntimeException("No audio track found in " + audioFile);
            }
            audioExtractor.selectTrack(0);
            muxer.addAudioTrack(audioFormat);

            videoExtractor.setDataSource(videoFile);
            videoFormat = selectTrack(videoExtractor);
            if (audioFormat == null) {
                throw new RuntimeException("No video track found in " + videoFile);
            }
            videoExtractor.selectTrack(0);
            muxer.addVideoTrack(videoFormat);

            muxer.start();

            extractAudio();

            if (audioExtractor != null) {
                audioExtractor.release();
                audioExtractor = null;
            }

            extractVideo();

            if (videoExtractor != null) {
                videoExtractor.release();
                videoExtractor = null;
            }


            muxer.stopMuxer();
            new File(audioFile).delete();
            new File(videoFile).delete();

            if(callback!=null){
                callback.movieCreationEnded(movieFile);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private MediaFormat selectTrack(MediaExtractor extractor) {
        // Select the first video track we find, ignore the rest.
        int numTracks = extractor.getTrackCount();
        if(numTracks !=0){
            return extractor.getTrackFormat(0);
        }
        return null;
    }

    private void extractAudio() throws IOException {
        MediaCodec.BufferInfo info = new MediaCodec.BufferInfo();
        MediaCodec.BufferInfo bufferInfo = new MediaCodec.BufferInfo();
        ByteBuffer inputBuf = ByteBuffer.allocate(1024*1024);

        boolean outputDone = false;
        boolean inputDone = false;

        while (!outputDone) {
            // Feed more data to the decoder.
            if (!inputDone) {
                inputBuf.clear();
                int chunkSize = audioExtractor.readSampleData(inputBuf, 0);
                if (chunkSize < 0) {
                    // End of stream -- send empty frame with EOS flag set.
                    inputDone = true;
                    outputDone = true;
                } else {

                    bufferInfo.presentationTimeUs = audioExtractor.getSampleTime();
                    bufferInfo.offset = inputBuf.position();
                    bufferInfo.size = chunkSize;
                    bufferInfo.flags = audioExtractor.getSampleFlags();
                    muxer.addAudioData(inputBuf, bufferInfo);
                    audioExtractor.advance();
                }
            }
        }
    }

    private void extractVideo() throws IOException {
        MediaCodec.BufferInfo info = new MediaCodec.BufferInfo();
        MediaCodec.BufferInfo bufferInfo = new MediaCodec.BufferInfo();
        ByteBuffer inputBuf = ByteBuffer.allocate(1024*1024);

        boolean outputDone = false;
        boolean inputDone = false;

        while (!outputDone) {
            if (!inputDone) {
                inputBuf.clear();
                int chunkSize = videoExtractor.readSampleData(inputBuf, 0);
                if (chunkSize < 0) {
                    // End of stream -- send empty frame with EOS flag set.
                    inputDone = true;
                    outputDone = true;
                } else {
                    bufferInfo.presentationTimeUs = videoExtractor.getSampleTime();
                    bufferInfo.offset = inputBuf.position();
                    bufferInfo.size = chunkSize;
                    bufferInfo.flags = videoExtractor.getSampleFlags();
                    muxer.addVideoData(inputBuf, bufferInfo);
                    videoExtractor.advance();
                }
            }
        }
    }
}