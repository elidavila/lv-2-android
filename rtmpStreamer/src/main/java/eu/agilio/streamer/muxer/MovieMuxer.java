package eu.agilio.streamer.muxer;

import android.media.MediaCodec;
import android.media.MediaFormat;
import android.media.MediaMuxer;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * Created by Bogdan on 21.03.2015.
 */
public class MovieMuxer {
    private static final String TAG = "MovieMuxer";
    private MediaMuxer muxer;
    private int audioTrackIndex;
    private int videoTrackIndex;

    public MovieMuxer(String path) {
        try {
            muxer = new MediaMuxer(path, MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void start() {
        muxer.start();
    }

    public void addAudioTrack(MediaFormat audioFormat) {
        audioTrackIndex = muxer.addTrack(audioFormat);
    }

    public void addVideoTrack(MediaFormat videoFormat) {
        videoTrackIndex = muxer.addTrack(videoFormat);
    }

    public void stopMuxer() {
        if (muxer != null) {
            muxer.stop();
            muxer.release();
            muxer = null;
        }
    }

    public void addVideoData(ByteBuffer inputBuffer, MediaCodec.BufferInfo bufferInfo) {
        muxer.writeSampleData(videoTrackIndex, inputBuffer, bufferInfo);
    }

    public synchronized void addAudioData(ByteBuffer inputBuffer, MediaCodec.BufferInfo bufferInfo) {
        muxer.writeSampleData(audioTrackIndex, inputBuffer, bufferInfo);
    }

}
