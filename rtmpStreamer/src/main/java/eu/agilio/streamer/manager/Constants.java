package eu.agilio.streamer.manager;

/**
 * Created by Bogdan on 16.01.2015.
 */
public interface Constants {

    public static final String SCOPE = "action";
    public static final int ACTION_STOP_STREAMER = 0;
    public static final int ACTION_START_STREAMER = 1;
    public static final int ACTION_CHANGE_BITRATE = 2;
    public static final int ACTION_STOP_SESSION = 3;
    public static final String AUDIO_FILE = "audio_file";
    public static final String VIDEO_FILE = "video_file";
    public static final String MOVIE_FILE = "movie_file";
}
