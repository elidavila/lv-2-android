package eu.agilio.streamer.manager;

import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.util.Log;
import eu.agilio.streamer.CameraPreview;
import eu.agilio.streamer.MCSTester;
import eu.agilio.streamer.RtmpStreamer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static eu.agilio.streamer.RtmpStreamer.*;

/**
 * Created by Bogdan on 07.01.2015.
 */
public class CameraManager {

    private static final String TAG = "CameraManager";
    private Camera camera;
    private volatile boolean useFrontCamera = true;
    private boolean flashOn = false;
    private boolean videoStabilizationOn = false;

    public CameraManager() {
    }

    public synchronized Camera getCamera() {
        return this.camera;
    }

    public synchronized Camera aquireCamera(boolean useFrontCamera) {
        if (camera == null) {
            this.useFrontCamera = useFrontCamera;
            if (!useFrontCamera) {
                camera = Camera.open();
            } else {
                boolean foundFrontCamera = setFrontCamera();
                if (!foundFrontCamera) {
                    throw new RuntimeException("Device doesn't have a front camera");
                }
            }
        }
        return camera;
    }

    private boolean setFrontCamera() {
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        for (int i = 0; i < Camera.getNumberOfCameras(); i++) {
            Camera.getCameraInfo(i, cameraInfo);
            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                camera = Camera.open(i);
                return true;
            }
        }
        return false;
    }

    private int haveFrontCamera() {
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        for (int i = 0; i < Camera.getNumberOfCameras(); i++) {
            Camera.getCameraInfo(i, cameraInfo);
            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                return i;
            }
        }
        return -1;
    }

    public void flipCamera(CameraPreview cameraPreview) throws IOException {
        int value = haveFrontCamera();
        if (value >= 0) {
            releaseCamera();
            if (useFrontCamera) {
                camera = Camera.open();
            } else {
                camera = Camera.open(value);
            }
            useFrontCamera = !useFrontCamera;
            createCameraParameters(RtmpStreamer.getInstance());
            camera.setPreviewDisplay(cameraPreview.getHolder());
            camera.setPreviewCallback(RtmpStreamer.getInstance());

            camera.startPreview();
        }
    }

    /**
     * Get all the resolutions compatible with the device's camera and the rtmp
     * streamer
     *
     * @return list of compatible resolutions
     */
    public List<Camera.Size> getResolutions(boolean useFrontCamera) {
        aquireCamera(useFrontCamera);
        List<Camera.Size> resolutions = new ArrayList<Camera.Size>();
        Camera.Parameters parameters = camera.getParameters();
        for (Camera.Size size : parameters.getSupportedPreviewSizes()) {
            if (size.width >= 320 && size.height >= 240) {
                resolutions.add(size);
            }
        }
        return resolutions;
    }

    private boolean supportsContinuousFocusMode(Camera.Parameters parameters) {
        for (String s : parameters.getSupportedFocusModes()) {
            if (s.equals(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO)) {
                return true;
            }
        }
        return false;
    }

    public void createCameraParameters(RtmpStreamer streamer) {
        if (camera == null) {
            aquireCamera(useFrontCamera);
        }
        camera.setDisplayOrientation(streamer.getDisplayOrientation());

        boolean supportsYV12 = false;
        boolean supportsNV16 = false;
        boolean supportsNV21 = false;

        Camera.Parameters parameters = camera.getParameters();
        if (parameters.getPreviewSize() != null) {

            parameters.setPreviewSize(streamer.getImageWidth(), streamer.getImageHeight());
            Log.d(TAG, "width: " + streamer.getImageWidth() + ", height: " + streamer.getImageHeight());
            parameters.setRecordingHint(true);
            if (supportsContinuousFocusMode(parameters)) {
                parameters
                        .setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
            }

            if (hasFlash()) {
                if (flashOn) {
                    parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                } else {
                    parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                }
            }

            if (isVideoStabilizationSupported()) {
                if (videoStabilizationOn) {
                    parameters.setVideoStabilization(true);
                } else {
                    parameters.setVideoStabilization(false);
                }
            }

            for (int format : parameters.getSupportedPreviewFormats()) {
                if (format == ImageFormat.YV12) {
                    Log.d(TAG, String.format(
                            "camera supports YV12 (format %d)", format));
                    supportsYV12 = true;
                } else if (format == ImageFormat.NV16) {
                    Log.d(TAG, String.format(
                            "camera supports NV16 (format %d)", format));
                    supportsNV16 = true;
                } else if (format == ImageFormat.NV21) {
                    Log.d(TAG, String.format(
                            "camera supports NV21 (format %d)", format));
                    supportsNV21 = true;
                }
                Log.d(TAG,
                        String.format("Have format with id %d", format));
            }
            if (streamer.getMediaCodecFormat() == MCSTester.FORMAT_NV21 && supportsNV21) {
                Log.d(TAG,
                        String.format("encoder has nv21 format and camera supports nv21, no conversion necessary"));
                parameters.setPreviewFormat(ImageFormat.NV21);
                streamer.setConversionMode(CONVERSION_NONE);
            } else if (streamer.getMediaCodecFormat() == MCSTester.FORMAT_NV12
                    && supportsNV16) { // NOTE nv16 seems to actually be
                // nv12, hence we use it as such
                Log.d(TAG,
                        String.format("encoder has nv12 format and camera supports nv12, no conversion necessary"));
                parameters.setPreviewFormat(ImageFormat.NV16);
                streamer.setConversionMode(CONVERSION_NONE);
            } else if (streamer.getMediaCodecFormat() == MCSTester.FORMAT_YV12
                    && supportsYV12) {
                Log.d(TAG,
                        String.format("encoder has yv12 format and camera supports yv12, no conversion necessary"));
                parameters.setPreviewFormat(ImageFormat.YV12);
                streamer.setConversionMode(CONVERSION_NONE);
            } else if (streamer.getMediaCodecFormat() == MCSTester.FORMAT_NV21
                    && supportsNV16) {
                Log.d(TAG,
                        String.format("encoder has nv21 format and camera supports nv12, using nv12 to nv21 conversion"));
                parameters.setPreviewFormat(ImageFormat.NV16);
                streamer.setConversionMode(CONVERSION_NV16_TO_NV21);
            } else if (streamer.getMediaCodecFormat() == MCSTester.FORMAT_NV12
                    && supportsNV21) {
                Log.d(TAG,
                        String.format("encoder has nv12 format and camera supports nv21, using nv21 to nv12 conversion"));
                parameters.setPreviewFormat(ImageFormat.NV21);
                streamer.setConversionMode(CONVERSION_NV21_TO_NV16);
            } else if (streamer.getMediaCodecFormat() == MCSTester.FORMAT_NV12
                    && supportsYV12) {
                Log.d(TAG,
                        String.format("encoder has nv12 format and camera supports yv12, using yv12 to nv12 conversion"));
                parameters.setPreviewFormat(ImageFormat.YV12);
                streamer.setConversionMode(CONVERSION_YV12_TO_NV12);
            } else if (streamer.getMediaCodecFormat() == MCSTester.FORMAT_NV21
                    && supportsYV12) {
                Log.d(TAG,
                        String.format("encoder has nv21 format and camera supports yv12, using yv12 to nv21 conversion"));
                parameters.setPreviewFormat(ImageFormat.YV12);
                streamer.setConversionMode(CONVERSION_YV12_TO_NV21);
            }
            streamer.setCameraFormat(parameters.getPreviewFormat());

            Log.d(TAG, String.format(
                    "Camera parameters: %dx%d at %d fps", streamer.getImageWidth(),
                    streamer.getImageHeight(), streamer.getFrameRate()));
            Log.d(TAG,
                    String.format("Preview format id: %d", parameters.getPreviewFormat()));
            camera.setParameters(parameters);
            Log.d(TAG, String.format(
                    "Camera parameters: %dx%d at %d fps", streamer.getImageWidth(),
                    streamer.getImageHeight(), streamer.getFrameRate()));
        }
    }

    private boolean isVideoStabilizationSupported() {
        Camera.Parameters params = camera.getParameters();
        Log.e("Stabilization", params.isVideoSnapshotSupported() + "");
        return params.isVideoStabilizationSupported();
    }

    public void videoStabilizationOn() {
        videoStabilizationOn = true;
        if (isVideoStabilizationSupported()) {
            Camera.Parameters params = camera.getParameters();
            params.setVideoStabilization(true);
            camera.setParameters(params);
        }
    }

    public void videoStabilizationOff() {
        videoStabilizationOn = false;
        if (isVideoStabilizationSupported()) {
            Camera.Parameters params = camera.getParameters();
            params.setVideoStabilization(false);
            camera.setParameters(params);
        }
    }

    private boolean hasFlash() {
        Camera.Parameters params = camera.getParameters();
        List<String> flashModes = params.getSupportedFlashModes();
        if (flashModes == null) {
            return false;
        }

        for (String flashMode : flashModes) {
            if (Camera.Parameters.FLASH_MODE_TORCH.equals(flashMode)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Requests from the camera the fps ranges suported. NOTE the values will be a multiples of 1000
     *
     * @return a list supported fps range (int[] <=> [min, max] pair)
     */
    public List<int[]> getSuportertedFpsRange() {
        return this.camera.getParameters().getSupportedPreviewFpsRange();
    }

    /**
     * Checks to see if a certain fps range is supported by the camera
     *
     * @param min lower value of fps which might be returned in a sec. Has to be a multiple 1000
     * @param max higher value of fps which might be returned in a sec. Has to be a multiple 1000 and >= min
     * @return true if the fps range is supported or false otherwise
     */
    public boolean isfFpsRangeSupported(int min, int max) {
        for (int[] range : getSuportertedFpsRange()) {
            if (range[0] == min && range[1] == max) {
                return true;
            }
        }
        return false;
    }

    /**
     * Sets a range of (minFPS, maxFPS) to the camera hardware if this range is supported, otherwise the existing fps
     * range remains set. The min value might be equal to the max value if this range is supported and they have to be
     * multiples of 1000
     *
     * @param min lower value of fps which will be set and by dividing it with 1000 represents the minimum number of
     *            frames which might be returned in one second. Has to be a multiple 1000
     * @param max higher value of fps which will be set and by dividing it with 1000 represents the maximum number of
     *            frames which might be returned in one second. Has to be a multiple 1000
     * @return true if the fps range has been set with success(meaning that is supported by the camera), false otherwise
     *         (meaning that the camera hardware does not support this fps range)
     */
    public boolean setFpsRange(int min, int max) {
        if (isfFpsRangeSupported(min, max)) {
            Camera.Parameters parameters = camera.getParameters();
            parameters.setPreviewFpsRange(min, max);
            camera.setParameters(parameters);
            return true;
        }
        return false;
    }

    public void flashOn() {
        flashOn = true;
        if (hasFlash()) {
            Camera.Parameters parameters = camera.getParameters();
            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
            camera.setParameters(parameters);
        }
    }

    public void flashOff() {
        flashOn = false;
        if (hasFlash()) {
            Camera.Parameters parameters = camera.getParameters();
            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
            camera.setParameters(parameters);
        }
    }

    public void cameraPreview() {
        if (camera != null) {
            camera.startPreview();
        }
    }

    public void stopCameraPreview() {
        if (camera != null) {
            camera.stopPreview();
        }
    }

    public void releaseCamera() {
        if (camera != null) {
            Log.d(TAG, String.format("releasing camera"));
            camera.stopPreview();
            camera.setPreviewCallback(null);
            camera.release();
            camera = null;
            Log.d(TAG, String.format("done releasing camera"));
        }
    }

    public void setPreViewCallBack(Camera.PreviewCallback callBack) {
        if (camera != null) {
            camera.setPreviewCallback(callBack);
        }
    }

    public boolean usingFrontCamera() {
        return this.useFrontCamera;
    }
}
