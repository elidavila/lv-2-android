package eu.agilio.streamer.datastructures;

/**
 * Created by Bogdan on 16.03.2015.
 */
public class DataPacket {

    public byte[] data;
    public long pts;

    public DataPacket(byte[] data, long pts){
        this.data = data;
        this.pts = pts;
    }
}
