package eu.agilio.streamer;

import android.app.Activity;
import android.util.Log;

import java.nio.ByteBuffer;

/**
 * User: Voicu
 * Since: 5/28/14 3:08 PM
 */
public class FfmpegNdk {

    private static final String TAG = "FFMPEG_NDK_JAVA";
    private static boolean wasInitialized = false;

    static {
        Log.d(TAG, String.format("loading ffmpegndk library"));
        try {
            System.loadLibrary("avutil-52");
            System.loadLibrary("swresample-0");
            System.loadLibrary("avcodec-55");
            System.loadLibrary("avformat-55");
            System.loadLibrary("ffmpegndk");
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, String.format("failed loading ffmpegndk: %s", e));
        }
    }

    public static native void init();

    synchronized public static void initOnce() {
        if (!wasInitialized) {
            init();
            wasInitialized = true;
        }
    }

    public static native int testVideo(int num_frames);
    public static native int getTestU(int frame_num);
    public static native int getTestV(int frame_num);
    public static native int getTestFrame(byte[] output, int frame_num);
    public static native int cleanupAfterTest(int num_frames);
    public static native int startStreaming(String url, Activity activity, int width, int height);
    public static native int readFrameCounter();
    public static native void releaseData();
    public static native int incTimeout(int seconds);
    public static native void rotate90degrees(ByteBuffer buffer);
    public static native void rotate180degrees(ByteBuffer buffer);
    public static native void rotate270degrees(ByteBuffer buffer);
    public static native void convertNV21ToNV12(ByteBuffer buffer);
    public static native void enableDebug();
    public static native void disableDebug();


    ////////////////////////new methods to use/////////////////////
    public static native boolean writeHeaders(int audioBitRate, int videoBitRate, int width, int height);
    public static native boolean haveDataPackets();
    public static native int deque();
    /**
     * The default value for the dropLimit is 300 packets. You can use this method to modify this limit. Dropping
     * packets means that they will not be put in the sending queue.
     */
    public static native void changeDropLimit(int dropLimit);
    public static native void enqueuePacket(ByteBuffer buffer_param, int jsize, int type, int keyFrame, long pts);
    public static native void enqueueTestPacket(ByteBuffer buffer_param, int jsize, int type, int keyFrame);
    public static native void flushPacketQueue();
    public static native void setAudioProbeData(ByteBuffer buffer_param, int position, int limit, int audioBitRate);
    public static native void setVideoProbeData(ByteBuffer buffer_param, int position, int limit, int videoBitRate, int videoWidth, int videoHeight);
    ///////////////////////////////////////////////////////////////
}
