package eu.agilio.streamer;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import eu.agilio.streamer.manager.Constants;

/**
 * Created by Bogdan on 16.01.2015.
 */
public class StreamerService extends IntentService {

    private static final String TAG = "StreamerService";

    public StreamerService() {
        super("StreamerService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            int scope = bundle.getInt(Constants.SCOPE);
            switch (scope) {
                case Constants.ACTION_STOP_STREAMER:
                    RtmpStreamer.getInstance().stop();
                    break;
                case Constants.ACTION_START_STREAMER:
                    RtmpStreamer.getInstance().start();
                    break;
                case Constants.ACTION_CHANGE_BITRATE:
                    RtmpStreamer.getInstance().changeVideoBitRate();
                    break;
                case Constants.ACTION_STOP_SESSION:
                    RtmpStreamer.getInstance().closeSession();
                    break;
            }
            Log.d(TAG, "onHandleIntent with scope: " + scope);
        }
    }
}
