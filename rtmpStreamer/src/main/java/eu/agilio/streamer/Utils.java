package eu.agilio.streamer;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * User: Voicu
 * Since: 5/20/14 5:57 PM
 */
public class Utils {

    static public String bufferToHexString(byte[] buffer, int offset, int length) {
        return bufferToHexString(buffer, offset, length, 16);
    }

    static public String bufferToHexString(byte[] buffer, int offset, int length, int wrap) {
        String s = "";
        for (int i = offset, j = 0; i < offset + length; i++, j++) {
            s = s + String.format("%02X", buffer[i]);
            if (j % 4 == 3) s = s + " ";
            if (j % wrap == (wrap - 1)) s = s + "\n";
        }
        return s;
    }

    /**
     * Very basic url calling function. Takes a String for posting data (or null for no data) and returns the result as a String
     *
     * @param location url of resource
     * @param postData can be null
     * @return data returned by server
     * @throws IOException
     */
    static public String callUrl(String location, String postData) throws IOException {
        String r;
        URL url = new URL(location);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        try {
            if (postData != null) {
                urlConnection.setDoOutput(true);
                urlConnection.setRequestProperty("Content-Type", "application/json");
                byte[] data = postData.getBytes();
                urlConnection.setFixedLengthStreamingMode(data.length);
                OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
                out.write(data);
                out.close();
            }
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            r = "";
            while (in.available() > 0) {
                byte[] buffer = new byte[1024];
                int len = in.read(buffer, 0, buffer.length);
                r += new String(buffer, 0, len);
            }
            in.close();
            return r;
        } finally {
            urlConnection.disconnect();
        }
    }

    static byte[] addYUVLinePadding(byte[] image, int width, int height, int padding) {
        byte[] result = new byte[(width + padding) * height * 3 / 2];

        int delta = 0;
        for (int i = 0; i < image.length; i++) {
            result[delta + i] = image[i];
            if (i % width == width - 1) {
                delta += padding;
            }
        }

        return result;
    }

    static byte[] addYUVPlanePadding(byte[] image) {
        int planeBorder = image.length / 3 * 2;
        if (planeBorder % 2048 == 0) {
            return image;
        }
        int paddingRequired = 2048 - planeBorder % 2048;
        byte[] result = new byte[image.length + paddingRequired];

        System.arraycopy(image, 0, result, 0, planeBorder);
        for (int i = 0; i < paddingRequired; i++) {
            result[planeBorder + i] = 0;
        }
        System.arraycopy(image, planeBorder, result, planeBorder + paddingRequired, planeBorder / 2);

        return result;
    }

}
