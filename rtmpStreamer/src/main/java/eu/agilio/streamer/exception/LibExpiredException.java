package eu.agilio.streamer.exception;

public class LibExpiredException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 374598786949032267L;

    public LibExpiredException() {
        super();
    }

    public LibExpiredException(String detailMessage) {
        super(detailMessage);
    }

    public LibExpiredException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public LibExpiredException(Throwable throwable) {
        super(throwable);
    }
}
