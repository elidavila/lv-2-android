package eu.agilio.streamer;

import android.app.Activity;
import android.content.Intent;
import android.hardware.Camera;
import android.media.*;
import android.os.Bundle;
import android.util.Log;
import eu.agilio.streamer.callback.BitRateMonitor;
import eu.agilio.streamer.callback.ConnectionNotifier;
import eu.agilio.streamer.callback.MovieCreatorCallBack;
import eu.agilio.streamer.callback.StreamerStateCallback;
import eu.agilio.streamer.datastructures.DataPacket;
import eu.agilio.streamer.encoders.AudioEncoder;
import eu.agilio.streamer.encoders.VideoEncoder;
import eu.agilio.streamer.exception.LibExpiredException;
import eu.agilio.streamer.manager.CameraManager;
import eu.agilio.streamer.manager.Constants;
import eu.agilio.streamer.muxer.FileMuxerWorker;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ShortBuffer;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by Bogdan on 08.01.2015.
 */
public class RtmpStreamer implements
        Camera.PreviewCallback {

    public static final int SAMPLE_RATE_LOW = 22050;
    public static final int SAMPLE_RATE_NORMAL = 44100;
    public static final int SAMPLE_RATE_HIGH = 96000;
    // end
    public static final int NEW_STATE_LIB_EXPIRED = -1;
    public static final int NEW_STATE_READY = 1;
    public static final int NEW_STATE_STARTING = 2;
    public static final int NEW_STATE_STREAMING = 3;
    public static final int NEW_STATE_STOPPING = 4;
    public static final int NEW_STATE_STOPPED = 5;
    public static final int NEW_STATE_INVALID = 6;
    public static final int ERROR_STARTING = 6;
    public static final int ERROR_STREAMING = 7;

    public static final String STATE_READY = "STATE_READY";
    public static final String STATE_STARTING = "STATE_STARTING";
    public static final String STATE_STREAMING = "STATE_STREAMING";
    public static final String STATE_STOPPING = "STATE_STOPPING";
    public static final String STATE_STOPPED = "STATE_STOPPED";
    public static final String STATE_INVALID = "STATE_INVALID";
    public static final String STATE_LIB_EXPIRED = "STATE_LIB_EXPIRED";

    public static final int CONVERSION_NONE = 0;
    /**
     * For understanding all the different formats a look here
     * https://wiki.videolan.org/YUV/ and here http://en.wikipedia.org/wiki/YUV
     * will help a lot. Like written elsewhere in comments, NV16 on android
     * refers to NV12, which is like NV21 but the UV values are reversed.
     */
    public static final int CONVERSION_NV21_TO_NV16 = 1; // note that NV16 on
    // android systems
    // actually means
    // NV12...
    public static final int CONVERSION_NV16_TO_NV21 = 1; // the conversion is
    // symmetrical
    public static final int CONVERSION_YV12_TO_NV21 = 2;
    public static final int CONVERSION_YV12_TO_NV12 = 3;
    public static final String TAG = "RtmpStreamer";
    // Expiration END date
    // change this before creating a jar
    private static final int EXP_YEAR = 2014;
    // months go from 0 to 11 (0->January and so on...)
    private static final int EXP_MONTH = 10;
    private static final int EXP_DAY = 30;

    private static Map<String, Integer> stateToEvent = new HashMap<String, Integer>();

    static {
        stateToEvent.put(STATE_READY, NEW_STATE_READY);
        stateToEvent.put(STATE_STARTING, NEW_STATE_STARTING);
        stateToEvent.put(STATE_STREAMING, NEW_STATE_STREAMING);
        stateToEvent.put(STATE_STOPPING, NEW_STATE_STOPPING);
        stateToEvent.put(STATE_STOPPED, NEW_STATE_STOPPED);
        stateToEvent.put(STATE_INVALID, NEW_STATE_INVALID);
        stateToEvent.put(STATE_LIB_EXPIRED, NEW_STATE_LIB_EXPIRED);
    }

    private static boolean wasInitialized = false;
    private static int mediaCodecFormat;
    /**
     * @param url      RTMP url
     * @param videoBitRate  in kBits/s
     * @param callback callback to use when informing the caller of stream related
     * events
     */

    private static RtmpStreamer instance;

    long framePTS;
    private String rtmpUrl;
    private String rtmpUser = null;
    private String rtmpPassword = null;
    private String ffmpegLink;
    private int displayOrientation = 0; // in degrees
    private int videoOrientation = 0; // in degrees
    private boolean videoDisabled = false, audioMuted = false;
    private int imageWidth;
    private int imageHeight;
    private int sampleAudioRateInHz = 44100;
    private int frameRate = 30;
    private int videoBitRate;
    private int audioBitRate = 64000;
    private int recordingBitRate;
    private int keyframeInterval = 1; // in seconds
    private boolean useFrontCamera = false;
    private AudioRecord audioRecord;
    private Thread audioThread = null;
    private Thread senderThread = null;
    private boolean runAudioThread = false;
    private AtomicLong currentAudioTs = new AtomicLong(0);
    private AtomicLong lastAudioUpdate = new AtomicLong(0);
    private BlockingQueue<DataPacket> preProcessedFramesQueue;
    private VideoEncoder videoEncoder;
    private AudioEncoder audioEncoder;
    private boolean cameraIsDirty;
    private int cameraFormat;
    private int conversionMode;
    private String state;
    private StreamerStateCallback callback;
    private boolean mediaCodecLinePadding, mediaCodecPlanePadding;
    private int mediaCodecLinePaddingSize;
    private Activity activity;
    private int minBitRate;
    private int maxBitRate;
    private BitRateMonitor bitrateMonitor = null;
    private int previousBitRate;
    private AtomicBoolean isAdaptiveBitRateEnabled = new AtomicBoolean(false);
    private int baseBitRate;
    private boolean isRecordingEnabled = false;
    private String timeStamp;
    private MovieCreatorCallBack movieCreatorCallBack;
    private MediaRecorder recorder;
    private String moviePathDirectory;
    private String movieFilePath;
    private String videoFilePath;
    private String audioFilePath;
    private AtomicBoolean hadVideoExtradata = new AtomicBoolean(false);
    private AtomicBoolean hadAudioExtradata = new AtomicBoolean(false);

    private volatile boolean audioIsRunning = false;
    private volatile boolean sendPackets = false;
    private ConnectionNotifier connectionNotifier;

    private Thread framesPreProcessorThread;
    private volatile boolean runFramePreProcessor = false;
    private Camera camera;
    private CameraManager cameraManager;

    private RtmpStreamer() {
        this.preProcessedFramesQueue = new LinkedBlockingQueue<DataPacket>();
    }

    public static synchronized RtmpStreamer getInstance() {
        if (instance == null) {
            instance = new RtmpStreamer();
        }
        return instance;
    }

    public synchronized void initData(boolean useBackCamera) {
        if (!wasInitialized) {
            int ret;
            FfmpegNdk.initOnce();
            ret = MCSTester.testMediaCodec(useBackCamera, cameraManager);
            Log.d(TAG, "testMediaCodec has returned: " + ret);
            if (ret < 0) {
                Log.d(TAG,
                        String.format("failed testing media codec: %d", ret));
            }
            mediaCodecFormat = MCSTester.getColorFormat();

            Log.d(TAG,
                    String.format("mediaCodecFormat = %d", mediaCodecFormat));

            wasInitialized = true;
        }
    }

    public static int getMediaCodecFormat() {
        return mediaCodecFormat;
    }

    public void setMovieDirectory(String path) {

        if (!path.endsWith(File.separator)) {
            moviePathDirectory = path + File.separator;
        } else {
            moviePathDirectory = path;
        }
    }

    public void enableAdaptiveBitRate() {
        isAdaptiveBitRateEnabled.set(true);
    }

    public void disableAdaptiveBitRate() {
        isAdaptiveBitRateEnabled.set(false);
    }

    public void enableRecording() {
        isRecordingEnabled = true;
    }

    public void disableRecording() {
        isRecordingEnabled = false;
    }

    public void init(CameraManager manager, String url, int recordingBitRate, int bitrate, int minBitRate, int maxBitRate, int width, int height,
                     StreamerStateCallback callback, Activity activity, ConnectionNotifier connectionNotifier) {
        try {
            this.cameraManager = manager;
            this.callback = callback;
            this.recordingBitRate = recordingBitRate * 1000;
            this.activity = activity;
            this.minBitRate = minBitRate * 1000;
            this.maxBitRate = maxBitRate * 1000;
            this.connectionNotifier = connectionNotifier;
            initData(useFrontCamera);
            setUrl(url);
            setResolution(width, height);
            Log.e(TAG, "width: " + width + ", height: " + height);

            this.videoBitRate = 1000 * bitrate;

            checkExp();
            setState(STATE_READY);
        } catch (Exception e) {
            if (e instanceof LibExpiredException) {
                Log.d(TAG, String.format("library has expired"), e);
                setState(STATE_LIB_EXPIRED);
            } else {
                Log.d(TAG,
                        String.format("failed initializing streamer"), e);
                setState(STATE_INVALID);
            }
        }
    }

    public int getDisplayOrientation() {
        return displayOrientation;
    }

    /**
     * Sets the preview orientation
     *
     * @param displayOrientation in degrees
     */
    public void setDisplayOrientation(int displayOrientation) {
        this.displayOrientation = displayOrientation;
    }

    public int getImageWidth() {
        return imageWidth;
    }

    public void setConversionMode(int conversationMode) {
        this.conversionMode = conversationMode;
    }

    public int getCameraFormat() {
        return cameraFormat;
    }

    public void setCameraFormat(int cameraFormat) {
        this.cameraFormat = cameraFormat;
    }

    public int getFrameRate() {
        return frameRate;
    }

    /**
     * Sets how many frames per second should the camera use.
     */
    public void setFrameRate(int frameRate) {
        this.frameRate = frameRate;
        cameraIsDirty = true;
    }

    private void checkExp() throws LibExpiredException {
        Calendar expiryDate = Calendar.getInstance();
        expiryDate.set(EXP_YEAR, EXP_MONTH, EXP_DAY);
        Calendar currentTime = Calendar.getInstance();
        if (currentTime.after(expiryDate)) {
            //TODO uncomment this in order to throw an exception when the trial has expired
            //throw new LibExpiredException();
        }
    }

    public void stopStreamer() {
        sendServiceIntent(Constants.ACTION_STOP_STREAMER);
    }

    public void stopSession() {
        sendServiceIntent(Constants.ACTION_STOP_SESSION);
    }

    public void startStreamer() {
        sendServiceIntent(Constants.ACTION_START_STREAMER);
    }

    private void sendServiceIntent(int scope) {
        Intent intent = new Intent(activity, StreamerService.class);
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.SCOPE, scope);
        intent.putExtras(bundle);
        activity.startService(intent);
    }

    /**
     * Start the stream, need to be in STATE_READY for this to work, throws
     * IllegalStateException otherwise
     *
     * @throws IllegalStateException
     */
    protected void start() throws IllegalStateException {
        try {
            checkExp();
        } catch (LibExpiredException e) {
            Log.d(TAG, String.format("library has expired"), e);
            setState(STATE_LIB_EXPIRED);
        }
        if (!state.equals(STATE_READY)) {
            throw new IllegalStateException(String.format(
                    "Cannot start from [%s], need [%s]", state, STATE_READY));
        }
        timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        if (isRecordingEnabled) {
            movieFilePath = moviePathDirectory + "movie_" + timeStamp + ".mp4";
            videoFilePath = moviePathDirectory + "video_" + timeStamp + ".mp4";
            audioFilePath = moviePathDirectory + "audio_" + timeStamp + ".mp4";
        }
        startRecording();
    }

    /**
     * Stop the stream, need to be in STATE_STREAMING for this to work, throws
     * IllegalStateException otherwise
     *
     * @throws IllegalStateException
     */
    protected void stop() throws IllegalStateException {
        if (state == null) {
            Log.e(TAG, "stop state e null");
            return;
        }

        if (state.equals(STATE_READY)) {
            Log.e(TAG, "stop STATE_READY");
            return;
        }

        if (!state.equals(STATE_STREAMING)) {
            throw new IllegalStateException(String.format(
                    "Cannot stop from [%s], need [%s]", state, STATE_STREAMING));
        } else {
            Log.e(TAG, "execut stop streaming");
            stopRecording();
        }
    }

    /**
     * Resets the stream to allow calling start again. Needs to be in
     * STATE_STOPPED for this to work, throws IllegalStateException otherwise
     *
     * @throws IllegalStateException
     */
    public void reset() throws IllegalStateException {
        if (!(state.equals(STATE_STOPPED) || state.equals(STATE_READY))) {
            throw new IllegalStateException(String.format(
                    "Cannot reset from [%s], need [%s] or [%s]", state,
                    STATE_STOPPED, STATE_READY));
        }
        //TODO decide between release camera and stop preview if necessary
        //CameraManager.getInstance().stopCameraPreview();
        //releaseCamera();
        setState(STATE_READY);
    }

    private void updateFfmpegLink() {
        if (rtmpUser != null && rtmpPassword != null) {
            ffmpegLink = String.format("%s?user=%s&password=%s", rtmpUrl,
                    rtmpUser, rtmpPassword);
        } else {
            ffmpegLink = rtmpUrl;
        }
    }

    public void setFrontCamera() {
        useFrontCamera = false;
    }

    public void setBackCamera() {
        useFrontCamera = true;
    }

    /**
     * Set a different url to stream to. Effects only appear after the next
     * start()
     */
    public void setUrl(String url) {
        rtmpUrl = url;
        updateFfmpegLink();
    }

    /**
     * Set a username to add to the url.
     */
    public void setUser(String user) {
        rtmpUser = user;
        updateFfmpegLink();
    }

    /**
     * Set a username to add to the url.
     */
    public void setPassword(String password) {
        rtmpPassword = password;
        updateFfmpegLink();
    }

    /**
     * Disable video - only audio will be streamed.
     */
    public void disableVideo() {
        videoDisabled = true;
    }

    /**
     * (Re)enable video
     */
    public void enableVideo() {
        videoDisabled = false;
    }

    /**
     * Disable audio - only video will be streamed.
     */
    public void muteAudio() {
        audioMuted = true;
    }

    /**
     * (Re)enable audio
     */
    public void unmuteAudio() {
        audioMuted = false;
    }

    /**
     * Sets the video's orientation
     *
     * @param videoOrientation in degrees
     */
    public void setVideoOrientation(int videoOrientation) {
        this.videoOrientation = videoOrientation;
    }

    public int getVideoOrientation(){
        return videoOrientation;
    }

    /**
     * Set a different resolution for the stream. Effects only appear after the
     * next start()
     */
    public void setResolution(int width, int height) {
        mediaCodecLinePadding = MCSTester.hasLinePadding(width, height);
        mediaCodecPlanePadding = MCSTester.hasPlanePadding(width, height);
        mediaCodecLinePaddingSize = MCSTester.getLinePaddingSize(width, height);
        Log.d(TAG, String.format(
                "set resolution to %dx%d, paddings for are %s(%d)/%s", width,
                height, mediaCodecLinePadding, mediaCodecLinePaddingSize,
                mediaCodecPlanePadding));
        imageWidth = width;
        imageHeight = height;
        cameraIsDirty = true;
    }

    /**
     * Set a different videoBitRate for the stream. Effects only appear after the
     * next start()
     *
     * @param videoBitRate in kBits/s
     */
    public void setVideoBitRate(int videoBitRate) {
        this.videoBitRate = 1000 * videoBitRate;
        if (state.equals(STATE_STREAMING))
            sendServiceIntent(Constants.ACTION_CHANGE_BITRATE);
    }

    /**
     * Set the interval between 2 keyframes
     *
     * @param keyframeInterval in seconds
     */
    public void setKeyframeInterval(int keyframeInterval) {
        this.keyframeInterval = keyframeInterval;
    }

    public void setCameraIsDirty() {
        cameraIsDirty = true;
    }

    /**
     * Sets the audio sample rate. The SAMPLE_RATE_* constants can be used.
     *
     * @param audioRate the audio sample rate in Hz
     */
    public void setAudioRate(int audioRate) {
        this.sampleAudioRateInHz = audioRate;
    }

    private void callback(int id) {
        this.callback.event(id);
    }

    private void setState(String state) {
        this.state = state;
        callback(stateToEvent.get(state));
    }

    protected void changeVideoBitRate() {
        if(state ==null){
            Log.d(TAG,"null state");
            return;
        }
        if (state.equals(STATE_STREAMING)) {
            videoEncoder.changeBitrate(videoBitRate);
            if (bitrateMonitor != null) {
                bitrateMonitor.bitrateChanged(this.previousBitRate, videoBitRate);
            }
        }
    }

    public boolean allowStreaming() {
        return this.sendPackets;
    }

    public void onResume(boolean usefrontCamera){
        if(!videoDisabled && cameraManager!=null){
            cameraManager.aquireCamera(usefrontCamera);
        }
    }

    public void onPause(){
        RtmpStreamer.getInstance().forceStop();
        RtmpStreamer.getInstance().stopStreamer();
        if(!videoDisabled) {
            if(cameraManager!=null) {
                cameraManager.setPreViewCallBack(null);
                cameraManager.releaseCamera();
            }
            RtmpStreamer.getInstance().setCameraIsDirty();
        }
    }

    void startRecording() {
        setState(STATE_STARTING);
        if (!videoDisabled) {
            if (cameraIsDirty) {
                Log.d(TAG, "creez parametri camerei");
                cameraManager.createCameraParameters(this);
                cameraIsDirty = false;
            }
            this.baseBitRate = videoBitRate;

            runFramePreProcessor = true;
            framesPreProcessorThread = new Thread(new FramePreProcessor());
            videoEncoder = new VideoEncoder(imageWidth, imageHeight, frameRate, videoBitRate, keyframeInterval);
            hadVideoExtradata.set(false);
        } else {
            hadVideoExtradata.set(true);
        }
        runAudioThread = true;
        audioThread = new Thread(new AudioWorker(), "RA: audio");

        sendPackets = true;

        hadAudioExtradata.set(false);

        audioEncoder = new AudioEncoder(sampleAudioRateInHz, audioBitRate, isRecordingEnabled, audioFilePath);

        senderThread = new Thread(new PacketSenderWorker(), "ndk loop");


        Log.d(TAG, String.format("Calling native method startStreaming"));

        if (FfmpegNdk.startStreaming(ffmpegLink, activity, imageWidth, imageHeight) != 0) {
            Log.e(TAG, "setez state ready");
            setState(STATE_READY);
        } else {
            Log.d(TAG, String.format("start streaming"));
            audioThread.start();
            audioEncoder.start();

            if(!videoDisabled) {
                cameraManager.setPreViewCallBack(this);
                framesPreProcessorThread.start();
                videoEncoder.start();
                if (isAdaptiveBitRateEnabled.get())
                    TrafficObserver.getInstance().startMonitoring();
            }

            setState(STATE_STREAMING);

            Log.d(TAG, String.format("All Threads have started"));
        }
        Log.d(TAG, String.format("Native method startStreaming finished"));

    }

    private void stopRecording() {
        setState(STATE_STOPPING);
        if(!videoDisabled) {
            cameraManager.setPreViewCallBack(null);
            if (isAdaptiveBitRateEnabled.get()) {
                TrafficObserver.getInstance().cancelMonitoring();
            }
            runFramePreProcessor = false;
            videoEncoder.stop();
        }
        runAudioThread = false;

        sendPackets = false;
        audioEncoder.stop();

        FfmpegNdk.flushPacketQueue();

        try {
            if (audioThread != null && audioThread.isAlive()) {
                Log.d(TAG, String.format("stopRecording: joining audioThread"));
                audioThread.join();
                audioThread = null;
                Log.d(TAG, String.format("stopRecording:  audioThread has joined"));
            }

            if(!videoDisabled) {
                if (framesPreProcessorThread != null && framesPreProcessorThread.isAlive()) {
                    Log.d(TAG, String.format("stopRecording: joining framesPreProcessorThread"));
                    synchronized (preProcessedFramesQueue) {
                        preProcessedFramesQueue.notify();
                    }
                    framesPreProcessorThread.join();
                    framesPreProcessorThread = null;
                }

                preProcessedFramesQueue.clear();
                videoEncoder.joinVideoWorker();
            }

            Log.d(TAG, String.format("stopRecording: joining mediaCodecStreamer threads"));
            audioEncoder.joinWorkers();


            if (senderThread != null && senderThread.isAlive()) {
                Log.d(TAG, String.format("stopRecording: joining senderThread"));
                senderThread.join();
                senderThread = null;
            }

            Log.d(TAG, String.format("stopRecording: all thread have ended"));
        } catch (InterruptedException e) {
            Log.d(TAG, String.format("stopRecording: interrupted while waiting for worker threads to finish"));
        }
        hadAudioExtradata.set(false);
        hadVideoExtradata.set(false);
        FfmpegNdk.releaseData();

        if (isRecordingEnabled && !videoDisabled) {
            new Thread(new FileMuxerWorker(audioFilePath, videoFilePath, movieFilePath, this.movieCreatorCallBack)).start();
        }

        this.videoBitRate = baseBitRate;
        setState(STATE_STOPPED);
    }

    @Override
    synchronized public void onPreviewFrame(byte[] data, Camera camera) {
        if (audioIsRunning) {
            framePTS = currentAudioTs.get() + System.currentTimeMillis() - lastAudioUpdate.get();
            DataPacket packet = new DataPacket(data, framePTS);
            synchronized (preProcessedFramesQueue) {
                if (preProcessedFramesQueue.offer(packet)) {
                    preProcessedFramesQueue.notify();
                    //   Log.d(TAG,"pun video Frame");
                }
            }

        }
    }

    public int getImageHeight() {
        return imageHeight;
    }

    public void increaseBitRate() {
        Log.d(TAG, "increaseBitRate");
        if (isAdaptiveBitRateEnabled.get() && videoBitRate + 10000 <= maxBitRate) {
            int newBitrate = videoBitRate / 1000 + 10;
            previousBitRate = videoBitRate;
            setVideoBitRate(newBitrate);
            Log.d(TAG,"previousBitRate: "+previousBitRate+", newBitrate: "+newBitrate);
        }
    }

    public void decreaseBitRate() {
        Log.d(TAG, "decreaseBitRate");
        if (isAdaptiveBitRateEnabled.get() && videoBitRate - 150000 >= minBitRate) {
            int newBitrate = videoBitRate / 1000 - 150;
            previousBitRate = videoBitRate;
            setVideoBitRate(newBitrate);
        } else {
            Log.d(TAG,"current videoBitRate: "+ videoBitRate);
        }
    }

    public void registerBitRateMonitor(BitRateMonitor monitor) {
        this.bitrateMonitor = monitor;
    }

    public void setMovieCreatorCallBack(MovieCreatorCallBack callBack) {
        this.movieCreatorCallBack = callBack;
    }

    public void hadFirstVideoPacket() {
        if (hadAudioExtradata.get() && !hadVideoExtradata.get()) {
            senderThread.start();
            Log.e(TAG, "senderThread started by videoEncoder");
        }
        hadVideoExtradata.set(true);
        Log.e(TAG, "hadFirstVideoPacket");
    }

    public void hadFirstAudioPacket() {
        if (hadVideoExtradata.get() && !hadAudioExtradata.get()) {
            senderThread.start();
            Log.d("FFMPEG_NDK_C", "senderThread started by audioEncoder");
        }
        hadAudioExtradata.set(true);
        Log.e(TAG, "hadFirstAudioPacket");
    }

    public void closeSession() {
        stop();
        cameraManager.stopCameraPreview();
    }

    public void forceStop() {
        if (isRecordingEnabled) {
            if (recorder != null) {
                recorder.stop();
                releaseMediaRecorder();
            }
        }
    }

    private boolean prepareVideoRecorder() throws IOException {

        // Step 1: Unlock and set camera to MediaRecorder
        recorder = new MediaRecorder();
        camera = cameraManager.getCamera();
        camera.unlock();
        recorder.setCamera(camera);

        // Step 2: Set sources
        recorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);

        // Step 3: Set a CamcorderProfile (requires API Level 8 or higher)
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        recorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
        recorder.setVideoFrameRate(frameRate);
        recorder.setVideoSize(imageWidth, imageHeight);
        recorder.setVideoEncodingBitRate(recordingBitRate);
       // recorder.setOrientationHint(90);
        //recorder.setProfile(CamcorderProfile.get(0,CamcorderProfile.QUALITY_HIGH));

        // Step 4: Set output file
        recorder.setOutputFile(videoFilePath);

        try {
            recorder.prepare();
        } catch (IllegalStateException e) {
            Log.d(TAG, "IllegalStateException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder();
        } catch (IOException e) {
            Log.d(TAG, "IOException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder();
        }
        recorder.start();
        try {
            camera.reconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }
        camera.setPreviewCallback(this);
        return true;
    }

    private void releaseMediaRecorder() {
        if (recorder != null) {
            recorder.reset();   // clear recorder configuration
            recorder.release(); // release the recorder object
            recorder = null;
            camera.lock();           // lock camera for later use
        }
    }

    public void refreshVideoEncoder() {
        //TODO revert width with height If necessary on the encoder side and all necessary at the encoder side
        Log.d(TAG,"refreshVideoEncoder with new data");
    }

    public void setBitrate(int bitrate) {
        this.videoBitRate = 1000 * bitrate;
        if (state.equals(STATE_STREAMING))
            sendServiceIntent(Constants.ACTION_CHANGE_BITRATE);
    }

    class AudioWorker implements Runnable {
        @Override
        public void run() {
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);
            int blockSize;
            int bufferSize;
            ShortBuffer audioBuffer;
            int bufferReadResult;

            bufferSize = AudioRecord.getMinBufferSize(sampleAudioRateInHz,
                    AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);
            audioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC, sampleAudioRateInHz,
                    AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT, bufferSize);

            short[] audioDataShort = new short[bufferSize];

            audioBuffer = ShortBuffer.wrap(audioDataShort);
            audioRecord.startRecording();

            long totalAudioData = 0;
            blockSize = audioEncoder.findBufferSize();
            if(blockSize>bufferSize){
                blockSize = bufferSize;
            }
            try {
                while (runAudioThread) {

                    bufferReadResult = audioRecord.read(audioDataShort, 0, blockSize);
                    if (bufferReadResult > 0) {
                        totalAudioData += bufferReadResult;
                        currentAudioTs.set(1000000 * totalAudioData / sampleAudioRateInHz);
                        lastAudioUpdate.set(System.currentTimeMillis());
                        if (audioMuted) {
                            for (int i = 0; i < bufferReadResult; i++) {
                                audioDataShort[i] = 0;
                            }
                        }

                        audioEncoder.addAudioData(audioBuffer, bufferReadResult, currentAudioTs.get());
                        if (!audioIsRunning) {
                            audioIsRunning = true;
                            try {
                                if (isRecordingEnabled)
                                    prepareVideoRecorder();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                Log.e(TAG, " AUDIO WORKER before releasing recorder");
                audioIsRunning = false;
                if (isRecordingEnabled) {
                    if (recorder != null) {
                        recorder.stop();
                        releaseMediaRecorder();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (audioRecord != null) {
                audioRecord.stop();
                audioRecord.release();
                audioRecord = null;
                Log.v(TAG, "audioRecord released");
            }
            currentAudioTs.set(0);
            Log.v(TAG, "AudioThread Finished");
        }
    }

    class PacketSenderWorker implements Runnable {

        @Override
        public void run() {
            Log.e(TAG, "Sender Thread has started");
            int stopCounter = 0;
            if (FfmpegNdk.writeHeaders(audioBitRate, videoBitRate, imageWidth, imageHeight)) {
                Log.e(TAG, "headers are written");
                while (sendPackets) {
                    if (FfmpegNdk.haveDataPackets()) {
                        if(FfmpegNdk.deque()<0){
                            stopCounter++;
                        } else{
                            stopCounter = 0;
                        }
                    } else {
                        try {
                            Thread.sleep(10);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                if(stopCounter>=10){
                    if(connectionNotifier!=null) {
                        connectionNotifier.timeout();
                    }
                    break;
                }
                }
                FfmpegNdk.flushPacketQueue();
            } else {
                stopStreamer();
                Log.i(TAG, "terminating because headers could not be written");
            }
            Log.e(TAG, "------senderThread ended");
        }
    }


    public class FramePreProcessor implements Runnable {

        @Override
        public void run() {
            DataPacket packet;
            int packetSize = imageHeight*imageWidth*3/2;
            ByteBuffer buffer = ByteBuffer.allocateDirect(packetSize);

            byte[] data = new byte[packetSize];
            buffer.get(data,0,packetSize);
            convertYV12ToNV12(data);

            while (runFramePreProcessor) {
                if (!haveFrames()) {
                    return;
                }
                packet = preProcessedFramesQueue.poll();
                if (packet.data == null) {
                    // Log.d(TAG,"null data");
                    continue;
                }
                buffer.position(0);
                buffer.put(packet.data);

                if (videoOrientation == 90) {
                    if(cameraManager.usingFrontCamera()){
                        FfmpegNdk.rotate270degrees(buffer);
                    } else {
                        FfmpegNdk.rotate90degrees(buffer);
                    }
                }

                if(conversionMode == CONVERSION_NV21_TO_NV16) {
                    FfmpegNdk.convertNV21ToNV12(buffer);
                }

                try {
                    buffer.position(0);
                    buffer.limit(packetSize);
                    buffer.get(packet.data);
                    videoEncoder.addVideoFrame(packet.data,
                            packet.pts);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        /**
         * The padding must be done after the image format
         * conversion because the conversion assume the data has no
         * padding. It would be a little more efficient to do the
         * padding and conversion at the same time, especially for
         * conversions that require allocating a different buffer.
         * This should be done sometime in the future, when the
         * library is stable enough to try to do speed optimization.
         */
//                if (mediaCodecLinePadding) {
//                    packet.data = Utils.addYUVLinePadding(
//                            packet.data, imageWidth, imageHeight,
//                            mediaCodecLinePaddingSize);
//                }
//                if (mediaCodecPlanePadding) {
//                    packet.data = Utils
//                            .addYUVPlanePadding(packet.data);
//                }

        private byte[] rotateYUV420Degree90(byte[] data)
        {
            byte [] yuv = new byte[imageWidth*imageHeight*3/2];
            // Rotate the Y luma
            int i = 0;
            for(int x = 0;x < imageWidth;x++)
            {
                for(int y = imageHeight-1;y >= 0;y--)
                {
                    yuv[i] = data[y*imageWidth+x];
                    i++;
                }
            }
            // Rotate the U and V color components
            i = imageWidth*imageHeight*3/2-1;
            for(int x = imageWidth-1;x > 0;x=x-2)
            {
                for(int y = 0;y < imageHeight/2;y++)
                {
                    yuv[i] = data[(imageWidth*imageHeight)+(y*imageWidth)+x];
                    i--;
                    yuv[i] = data[(imageWidth*imageHeight)+(y*imageWidth)+(x-1)];
                    i--;
                }
            }
            return yuv;
        }

        private void rotateYUV420Degree180(byte[] data) {
            byte aux;
            Log.d(TAG,"length: "+data.length+", width: "+imageWidth+", height: "+imageHeight);
            for(int i = 0;i< imageHeight ; i++){
                for(int j = 0; j<imageWidth; j++){
                    aux = data[i*imageWidth + j];
                    data[i*imageWidth + j] = data[i*(imageWidth+1)-j-1];
                    data[i*(imageWidth+1)-j-1] = aux;
                }
            }

            int w = imageWidth/2;
            int h = imageHeight/4;
            int d = imageHeight*imageWidth;

            for(int i = 0; i< h;i++){
                for(int j= 0;j<w;j++){
                    //rotate U plane
                    aux = data[d+i*h+2*j];
                    data[d+i*h+2*j] = data[d+i*(h+1)-2*(j-1)];
                    data[d+i*(h+1)-2*(j-1)] = aux;
                    //rotate Y plane
                    aux = data[d+i*h+2*j + 1];
                    data[d+i*h+2*j+1] = data[d+i*(h+1)-2*(j-1)+1];
                    data[d+i*(h+1)-2*(j-1)+1] = aux;
                }
            }
        }

        private void convertNV21ToNV16(byte data[]) {
            //  Log.e(TAG,"data length: "+data.length);
            int dl6 = data.length / 6, dl64 = dl6 * 4;
            for (int i = 0; i < dl6; i++) {
                byte b = data[dl64 + 2 * i];
                data[dl64 + 2 * i] = data[dl64 + 2 * i + 1];
                data[dl64 + 2 * i + 1] = b;
            }
        }

        private void convertYV12ToNV12(byte[] data) {
            int dl6 = data.length / 6;
            int dl64 = dl6 * 4;

            byte[] tmp = new byte[dl6 * 2];

            for (int i = 0; i < dl6; i++) {
                tmp[2 * i + 1] = data[dl64 + i];
                tmp[2 * i] = data[dl64 + dl6 + i];
            }
            System.arraycopy(tmp, 0, data, dl64, dl6 * 2);
        }

        private void convertYV12ToNV21(byte[] data) {
            int dl6 = data.length / 6, dl64 = dl6 * 4;

            byte[] tmp = new byte[dl6 * 2];

            for (int i = 0; i < dl6; i++) {
                tmp[2 * i] = data[dl64 + i];
                tmp[2 * i + 1] = data[dl64 + dl6 + i];
            }
            System.arraycopy(tmp, 0, data, dl64, dl6 * 2);
        }

        private boolean haveFrames() {
            synchronized (preProcessedFramesQueue) {
                while (preProcessedFramesQueue.isEmpty()) {
                    if (runFramePreProcessor) {
                        try {
                            preProcessedFramesQueue.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    } else {
                        return false;
                    }
                }
            }
            return true;
        }
    }
}