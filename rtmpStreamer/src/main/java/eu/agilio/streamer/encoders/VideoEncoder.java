package eu.agilio.streamer.encoders;

import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaFormat;
import android.util.Log;
//import eu.agilio.streamer.CustomLogger;
import eu.agilio.streamer.FfmpegNdk;
import eu.agilio.streamer.RtmpStreamer;

import java.io.IOException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by Bogdan on 20.01.2015.
 */
public class VideoEncoder {

    private static final String TAG = "VideoEncoder";
    private MediaFormat videoFormat;
    private MediaCodec videoEncoder;
    private ByteBuffer[] videoInputs;
    private ByteBuffer[] videoOutputs;
    private int imageWidth, imageHeight, frameRate, bitrate, keyframeInterval;
    private volatile boolean running;
    private Thread videoWorker;
    private AtomicBoolean changeBitRate;
    private int newBitRate;
    private int inputBuffer;
    private int i = 0;
    private int prevPosition ;
    private int remaining ;
    private int maxLength ;
    private int length;

    public VideoEncoder(int imageWidth, int imageHeight, int frameRate, int bitrate, int keyframeInterval) {
        if(RtmpStreamer.getInstance().getVideoOrientation() == 90){
            this.imageWidth = imageHeight;
            this.imageHeight = imageWidth;
        } else {
            this.imageWidth = imageWidth;
            this.imageHeight = imageHeight;
        }

        this.frameRate = frameRate;
        this.bitrate = bitrate;
        this.keyframeInterval = keyframeInterval;
        changeBitRate = new AtomicBoolean(false);
    }

    public void changeBitrate(int bitrate){
        this.newBitRate = bitrate;
        changeBitRate.set(true);
    }

    public void resetEncoder(){
        videoFormat.setInteger(MediaFormat.KEY_BIT_RATE, newBitRate);
        videoEncoder.flush();
        videoEncoder.stop();
        videoEncoder.configure(videoFormat,null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
        videoEncoder.start();
        videoInputs = videoEncoder.getInputBuffers();
        videoOutputs = videoEncoder.getOutputBuffers();
        synchronized (changeBitRate){
            changeBitRate.set(false);
            changeBitRate.notify();
        }
        // Log.d(TAG,"am setat bitrate-ul la: "+newBitRate);
    }


    public void start() {
        initVideoCodec();
        running = true;
        videoWorker = new Thread(new VideoWorker(), "MCS: video");
        videoWorker.start();
    }

    public void stop() {
        running = false;
        synchronized (changeBitRate){
            changeBitRate.notify();
        }
    }

    public void joinVideoWorker() throws InterruptedException {
        if (videoWorker.isAlive()) {
            videoWorker.join();
        }
        releaseVideoCodec();
    }

    private void initVideoCodec() {
        //  CustomLogger.log(TAG, String.format("creating video encoder, parameters: res %dx%d, fps %d, bitrate %d", imageWidth, imageHeight, frameRate, bitrate));
        try {
            videoEncoder = MediaCodec.createEncoderByType("video/avc");
            videoFormat = MediaFormat.createVideoFormat("video/avc", imageWidth, imageHeight);
            videoFormat.setInteger(MediaFormat.KEY_BIT_RATE, bitrate);
            videoFormat.setInteger(MediaFormat.KEY_FRAME_RATE, frameRate);
            videoFormat.setInteger(MediaFormat.KEY_COLOR_FORMAT, MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420SemiPlanar);
            videoFormat.setInteger(MediaFormat.KEY_I_FRAME_INTERVAL, keyframeInterval);
            videoEncoder.configure(videoFormat, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
            videoEncoder.start();
            videoInputs = videoEncoder.getInputBuffers();
            videoOutputs = videoEncoder.getOutputBuffers();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void releaseVideoCodec() {
        if (videoEncoder != null) {
            videoEncoder.flush();
            videoEncoder.stop();
            videoEncoder.release();
            videoEncoder = null;
        }
        //  CustomLogger.log(TAG, String.format("media codec objects released"));
    }

    public void addVideoFrame(byte[] data, long pts) throws InterruptedException {
        if(changeBitRate.get()){
            resetEncoder();
        }
        i = 0;
        while (!running) {
            Thread.sleep(100);
            i += 1;
            if (i > 5) return;
        }
        length = data.length;
        while (running && length > 0) {
            while ((inputBuffer = videoEncoder.dequeueInputBuffer(1000)) < 0) {
                if (inputBuffer == MediaCodec.INFO_TRY_AGAIN_LATER) {
//                    if (lastBufferErrorMsg + 2000 < System.currentTimeMillis()) {
//                        lastBufferErrorMsg = System.currentTimeMillis();
//                    }
                    //   Log.e(TAG,"NU are buffer liber");


                }
                if (!running) return;
                Thread.sleep(100);
            }

            videoInputs[inputBuffer].clear();
            prevPosition = videoInputs[inputBuffer].position();
            remaining = videoInputs[inputBuffer].remaining();
            maxLength = Math.min(remaining, length);
            if (maxLength < length) {
                // we can't add part of the picture to the video encoder's buffer so we need to wait
                // until the encoder has a large enough buffer that fits the entire picture
                //CustomLogger.log(TAG, String.format("buffer not large enough for video frame, waiting for a bit " + Thread.currentThread().getName()));
                videoEncoder.queueInputBuffer(inputBuffer, prevPosition, 0, 0, 0); // queue it back with no data
                Thread.sleep(10); // retry after a short while
                continue;
            } else {
                videoInputs[inputBuffer].put(data,0, data.length);
                videoEncoder.queueInputBuffer(inputBuffer, prevPosition, maxLength, pts, 0);
                length -= maxLength;
            }
        }
    }

    private class VideoWorker implements Runnable {
        private boolean hadFirstPacket = false;

        @Override
        public void run() {
            Thread.currentThread().setName("VideoWorker");
            try {
                //  CustomLogger.log(TAG, String.format("VideoWorker: started " + Thread.currentThread().getName()));
                MediaCodec.BufferInfo videoBufferInfo = new MediaCodec.BufferInfo();
                boolean hadVideo;
                int videoBufferIndex;

                while (running) {
                    if(changeBitRate.get()){
                        while (changeBitRate.get()){
                            synchronized (changeBitRate){
                                try {
                                    changeBitRate.wait();
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                    hadVideo = true;

                    videoBufferIndex = videoEncoder.dequeueOutputBuffer(videoBufferInfo, 0);
                    //   Log.e(TAG,"running, videoBufferIndex: "+videoBufferIndex);
                    if (videoBufferIndex == MediaCodec.INFO_TRY_AGAIN_LATER) {
                        hadVideo = false;
                    } else if (videoBufferIndex == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
                        videoOutputs = videoEncoder.getOutputBuffers();
                    } else if (videoBufferIndex == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                        //       CustomLogger.log(TAG, String.format("output format changed for video " + Thread.currentThread().getName()));
                    } else if (videoBufferIndex < 0) {
                        //       CustomLogger.log(TAG, String.format("unexpected result for video: %d " + Thread.currentThread().getName(), videoBufferIndex));
                    } else {
                        videoOutputs[videoBufferIndex].position(videoBufferInfo.offset);
                        videoOutputs[videoBufferIndex].limit(videoBufferInfo.size);

                        if (!hadFirstPacket) {
                            hadFirstPacket = true;
                            FfmpegNdk.setVideoProbeData(videoOutputs[videoBufferIndex], videoBufferInfo.offset, videoBufferInfo.size, bitrate, imageWidth, imageHeight);
                            RtmpStreamer.getInstance().hadFirstVideoPacket();
                        } else {
                            if(RtmpStreamer.getInstance().allowStreaming()) {
                                FfmpegNdk.enqueuePacket(videoOutputs[videoBufferIndex], videoOutputs[videoBufferIndex].remaining(), 0, videoBufferInfo.flags, videoBufferInfo.presentationTimeUs / 1000);
                            }
                        }
                        videoEncoder.releaseOutputBuffer(videoBufferIndex, false);
                    }

                    if (!hadVideo) {
                        try {
                            Thread.sleep(10);
                        } catch (InterruptedException e) {
                            //          CustomLogger.log(TAG, String.format("mediaCodecWorker interrupted: %s " + Thread.currentThread().getName(), e));
                            break;
                        }
                    }
                }
            } catch (IllegalStateException e) {
                //  CustomLogger.log(TAG, String.format("failure during buffer reading: %s " + Thread.currentThread().getName(), e));
            } catch (BufferUnderflowException e) {
                //  CustomLogger.log(TAG, String.format("buffer underflow while reading: %s " + Thread.currentThread().getName(), e));
            } finally {
                //  CustomLogger.log(TAG, String.format("VideoWorker: stopped " + Thread.currentThread().getName()));
            }
        }
    }
}