package eu.agilio.streamer.encoders;

import android.media.MediaCodec;
import android.media.MediaFormat;
import android.media.MediaMuxer;
import android.util.Log;
import eu.agilio.streamer.FfmpegNdk;
import eu.agilio.streamer.RtmpStreamer;

import java.io.IOException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.ShortBuffer;

/**
 * Created by Bogdan on 20.01.2015.
 */
public class AudioEncoder {

    private static final String TAG = "AudioEncoder";
    private int inputBuffer;
    private int i;
    private int offset;
    private int prevPosition;
    private int remaining;
    private int maxLength;
    private boolean saveLocally;
    private MediaCodec audioEncoder;
    private ByteBuffer[] audioInputs;
    private ByteBuffer[] audioOutputs;
    private int sampleAudioRateInHz;
    private volatile boolean running;
    private Thread audioWorker;
    private MediaMuxer muxer = null;
    private int audioTrack;
    private int bitrate;

    public AudioEncoder(int sampleAudioRateInHz, int bitrate,
                        boolean saveLocally, String audioFilePath) {
        this.sampleAudioRateInHz = sampleAudioRateInHz;
        this.saveLocally = saveLocally;
        this.bitrate = bitrate;
        iniAudioCodec();
        if (saveLocally) {
            try {
                muxer = new MediaMuxer(audioFilePath, MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void start() {
        running = true;
        audioWorker = new Thread(new AudioWorker(), "MCS: audio");
        audioWorker.start();
    }

    public void stop() {
        running = false;
        saveLocally = false;
    }

    public void joinWorkers() throws InterruptedException {
        Log.i(TAG, "---start method joinAudioWorker-----");
        if (audioWorker.isAlive()) {
            audioWorker.join();
        }
        Log.i(TAG, "---end method joinAudioWorker-----");

        releaseAudioCodec();
    }

    private void iniAudioCodec() {
        try {
            audioEncoder = MediaCodec.createEncoderByType("audio/mp4a-latm");
            MediaFormat audioFormat = MediaFormat.createAudioFormat("audio/mp4a-latm", sampleAudioRateInHz, 1);
            audioFormat.setInteger(MediaFormat.KEY_BIT_RATE, bitrate);
            audioEncoder.configure(audioFormat, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
            audioEncoder.start();

            audioInputs = audioEncoder.getInputBuffers();
            audioOutputs = audioEncoder.getOutputBuffers();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void releaseAudioCodec() {
        audioEncoder.stop();
        audioEncoder.release();
        audioEncoder = null;

        if (muxer != null) {
            muxer.stop();
            muxer.release();
            muxer = null;
        }
        Log.d(TAG, "audio codec objects released");
    }

    public int findBufferSize(){
        int size = 0;
        while ((inputBuffer = audioEncoder.dequeueInputBuffer(1000)) < 0) {
            //  CustomLogger.log(TAG, String.format("MCS: addAudioData: sleeping 100ms (ib = %d)", inputBuffer));
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        audioInputs[inputBuffer].clear();
        size = audioInputs[inputBuffer].remaining() / 2;
        audioEncoder.queueInputBuffer(inputBuffer, 0, 0, 0, 0);

        return size;
    }

    public void addAudioData(ShortBuffer data, int length, long pts) throws InterruptedException {
        //   Log.e(TAG,"adaog date");
        i = 0;
        while (!running) {
            Thread.sleep(100);
            i += 1;
            if (i > 5) return;
        }
        offset = 0;
        length *= 2;
        while (running && length > 0) {
            while ((inputBuffer = audioEncoder.dequeueInputBuffer(1000)) < 0) {
                if (!running) return;
                //  CustomLogger.log(TAG, String.format("MCS: addAudioData: sleeping 100ms (ib = %d)", inputBuffer));
                Thread.sleep(100);
            }

            audioInputs[inputBuffer].clear();
            prevPosition = audioInputs[inputBuffer].position();
            remaining = audioInputs[inputBuffer].remaining();
            maxLength = Math.min(remaining, length);
         //   Log.d(TAG,"prevPosition: "+prevPosition+", remaining: "+remaining+", maxLength");
            data.position(offset / 2);
            data.limit((offset + maxLength) / 2);
            audioInputs[inputBuffer].asShortBuffer().put(data);
            audioEncoder.queueInputBuffer(inputBuffer, prevPosition, maxLength, pts, 0);
           // Log.e(TAG,"length: "+length+", maxlenght: "+maxLength);
            offset += maxLength;
            length -= maxLength;
        }
    }

    private class AudioWorker implements Runnable {
        private boolean hadFirstPacket = false;

        @Override
        public void run() {
            MediaCodec.BufferInfo audioBufferInfo = new MediaCodec.BufferInfo();
            boolean hadAudio;
            int audioBufferIndex;
            try {
                Log.d(TAG, String.format("AudioWorker: started"));
                while (running) {

                    hadAudio = true;

                    audioBufferIndex = audioEncoder.dequeueOutputBuffer(audioBufferInfo, 0);
                    if (audioBufferIndex == MediaCodec.INFO_TRY_AGAIN_LATER) {
                        hadAudio = false;
                    } else if (audioBufferIndex == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
                        audioOutputs = audioEncoder.getOutputBuffers();
                    } else if (audioBufferIndex == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                        Log.d(TAG, String.format("output format changed for audio"));
                        if (saveLocally) {
                            audioTrack = muxer.addTrack(audioEncoder.getOutputFormat());
                            muxer.start();
                        }
                    } else if (audioBufferIndex < 0) {
                        Log.d(TAG, String.format("unexpected result for audio: %d", audioBufferIndex));
                    } else {
                        audioOutputs[audioBufferIndex].position(audioBufferInfo.offset);
                        audioOutputs[audioBufferIndex].limit(audioBufferInfo.size);
                        if (!hadFirstPacket) {
                            hadFirstPacket = true;
                            FfmpegNdk.setAudioProbeData(audioOutputs[audioBufferIndex], audioBufferInfo.offset, audioBufferInfo.size, 128000);
                            RtmpStreamer.getInstance().hadFirstAudioPacket();
                        } else {
                            if (RtmpStreamer.getInstance().allowStreaming()) {
//                                if(FfmpegNdk.queueSize()<=200) {
                                    FfmpegNdk.enqueuePacket(audioOutputs[audioBufferIndex], audioOutputs[audioBufferIndex].remaining(), 1, 0, audioBufferInfo.presentationTimeUs / 1000);
//                                }
                                if (saveLocally) {
                                 //   Log.e(TAG,"audio pts: "+audioBufferInfo.presentationTimeUs);
                                    audioOutputs[audioBufferIndex].position(audioBufferInfo.offset);
                                    audioOutputs[audioBufferIndex].limit(audioBufferInfo.offset + audioBufferInfo.size);
                                    muxer.writeSampleData(audioTrack, audioOutputs[audioBufferIndex], audioBufferInfo);
                                }
                            }
                        }
                        audioEncoder.releaseOutputBuffer(audioBufferIndex, false);
                    }

                    if (!hadAudio) {
                        try {
                            Thread.sleep(10);
                        } catch (InterruptedException e) {
                            Log.d(TAG, String.format("mediaCodecWorker interrupted: %s", e));
                            break;
                        }
                    }
                }
            } catch (IllegalStateException e) {
                Log.d(TAG, String.format("failure during buffer reading: %s", e));
                e.printStackTrace();
            } catch (BufferUnderflowException e) {
                Log.d(TAG, String.format("buffer underflow while reading: %s", e));
                e.printStackTrace();
            } finally {
                Log.d(TAG, String.format("AudioWorker: stopped"));
            }

            Log.i(TAG, "AudioWorker has finished");
        }
    }
}