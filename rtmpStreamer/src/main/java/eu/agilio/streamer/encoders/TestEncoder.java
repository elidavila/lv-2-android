package eu.agilio.streamer.encoders;

import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaFormat;
//import eu.agilio.streamer.CustomLogger;
import eu.agilio.streamer.FfmpegNdk;
import eu.agilio.streamer.MCSTester;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Random;

/**
 * Created by Bogdan on 20.02.2015.
 */
public class TestEncoder {

    private static final String TAG = "TestEncoder";
    private MediaCodec codec;
    private ByteBuffer videoInputs[];
    private ByteBuffer videoOutputs[];
    private int width;
    private int height;

    public TestEncoder(int width, int height) {
        try {
            codec = MediaCodec.createEncoderByType("video/avc");
            this.width = width;
            this.height = height;
            MediaFormat videoFormat = MediaFormat.createVideoFormat("video/avc", width, height);
            videoFormat.setInteger(MediaFormat.KEY_BIT_RATE, 1000000);
            videoFormat.setInteger(MediaFormat.KEY_FRAME_RATE, 25);
            videoFormat.setInteger(MediaFormat.KEY_COLOR_FORMAT, MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420SemiPlanar);
            videoFormat.setInteger(MediaFormat.KEY_I_FRAME_INTERVAL, 1);
            codec.configure(videoFormat, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
            codec.start();

            videoInputs = codec.getInputBuffers();
            videoOutputs = codec.getOutputBuffers();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void enqueImage(byte[] image, long pts) {
        int inputBuffer;
        image = MCSTester.convertImageIfNeeded(image, width, height);
        while ((inputBuffer = codec.dequeueInputBuffer(1000)) < 0) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        videoInputs[inputBuffer].clear();
        videoInputs[inputBuffer].put(image);
        codec.queueInputBuffer(inputBuffer, 0, image.length, pts, 0);
    }

    private void dequeCodec() {
        MediaCodec.BufferInfo videoBufferInfo = new MediaCodec.BufferInfo();
        boolean hadVideo = false;
        while (!hadVideo) {
            int videoBufferIndex = codec.dequeueOutputBuffer(videoBufferInfo, 0);
            if (videoBufferIndex == MediaCodec.INFO_TRY_AGAIN_LATER) {

            } else if (videoBufferIndex == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
                videoOutputs = codec.getOutputBuffers();
            } else if (videoBufferIndex == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
               // CustomLogger.log(TAG, String.format("output format changed for video " + Thread.currentThread().getName()));
            } else if (videoBufferIndex < 0) {
               // CustomLogger.log(TAG, String.format("unexpected result for video: %d " + Thread.currentThread().getName(), videoBufferIndex));
            } else {
                videoOutputs[videoBufferIndex].position(videoBufferInfo.offset);
                videoOutputs[videoBufferIndex].limit(videoBufferInfo.size);

                FfmpegNdk.enqueueTestPacket(videoOutputs[videoBufferIndex], videoOutputs[videoBufferIndex].remaining(), 0, videoBufferInfo.flags);
                codec.releaseOutputBuffer(videoBufferIndex, false);
                hadVideo = true;
            }
        }
    }

    public void performTest(List<byte[]> images) {
        addDummyImage(images);
        boolean isExtradata = true;
        int i = 0;
        for (byte[] image : images) {
            image = MCSTester.convertImageIfNeeded(image, width, height);
            this.enqueImage(image, i * 4000);
            i++;
            if (isExtradata) {
                dequeCodec();
                isExtradata = false;
            }
            dequeCodec();
        }
        stopCodec();
        images.remove(images.size() - 1);
    }

    private void stopCodec() {
        codec.stop();
        codec.release();
    }

    private void addDummyImage(List<byte[]> images) {
        byte[] dummy = new byte[width * height * 3 / 2];
        Random random = new Random();
        for (int k = 0; k < dummy.length; k++) {
            if (k < 4096) {
                dummy[k] = (byte) random.nextInt();
            } else {
                dummy[k] = 0;
            }
        }

        images.add(dummy);
    }
}
