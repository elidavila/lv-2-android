package com.app.livideo.activities;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.app.livideo.R;
import com.app.livideo.dialogs.ShareImageDialog;
import com.app.livideo.models.Artist;
import com.app.livideo.models.ArtistPicture;
import com.app.livideo.models.User;
import com.app.livideo.models.UserImage;
import com.app.livideo.models.validators.ArtistValidator;
import com.app.livideo.models.validators.UserImageValidator;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.CustomViewPager;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.goka.flickableview.FlickableImageView;

import java.io.File;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DisplayUserImagesActivity extends AppCompatActivity {

    @Bind(R.id.display_image_pager) public CustomViewPager mPager;
    DisplayImagePagerAdapter mAdapter;
    static Artist mArtist;
    static User mUser;
    static List<ArtistPicture> mImages;
    static List<UserImage> mUserImages;
    static boolean isUser = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_user_images);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        ButterKnife.bind(this);

        isUser = (getIntent().getBooleanExtra(AppConstants.IS_USER_INTENT, false));
        if (isUser) {
            // From My Profile (Broadcasters)
            mUser = (User) getIntent().getSerializableExtra(AppConstants.ARTIST_KEY_INTENT);
            mUserImages = mUser.getPictures();
            if (!UserImageValidator.isValid(mUserImages.get(mUserImages.size() - 1)))
                mUserImages.remove(mUserImages.size() - 1); // Remove the blank image from camera
        } else if (ArtistValidator.isValid((Artist) getIntent().getSerializableExtra(AppConstants.ARTIST_KEY_INTENT))) {
            // From Artist Page
            mArtist = (Artist) getIntent().getSerializableExtra(AppConstants.ARTIST_KEY_INTENT);
            mImages = mArtist.getArtistPictures();
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.an_error_string), Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        // Set Adapter
        mAdapter = new DisplayImagePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mAdapter);
        mPager.setCurrentItem(getIntent().getIntExtra(AppConstants.ARTIST_POSITION_KEY_INTENT, 0));
    }

    static class DisplayImagePagerAdapter extends FragmentStatePagerAdapter {
        public DisplayImagePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            if (isUser)
                return mUserImages.size();
            else
                return mImages.size();
        }

        @Override
        public Fragment getItem(int position) {
            return DisplayImageFragment.newInstance(position);
        }
    }

    public static class DisplayImageFragment extends Fragment {
        @Bind(R.id.display_image_view) FlickableImageView imageView;
        @Bind(R.id.display_image_share) ImageView shareIcon;
        @Bind(R.id.display_image_progress_bar) ImageView mProgressBar;
        private String pictureId;
        private String pictureUrl;
        private boolean isFile = false;
        private View mView;
        int mPosition;
        File mFile;
        Bitmap mBitmap;

        static DisplayImageFragment newInstance(int pos) {
            DisplayImageFragment f = new DisplayImageFragment();
            Bundle args = new Bundle();
            args.putInt("pos", pos);
            f.setArguments(args);
            return f;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            mPosition = getArguments() != null ? getArguments().getInt("pos") : 1;
            setRetainInstance(true);
            Glide.with(getContext()).load(R.drawable.loading_anim).asGif().into(mProgressBar);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            mView = inflater.inflate(R.layout.display_image_view, container, false);
            ButterKnife.bind(this, mView);

            if (isUser) {
                pictureId = mUserImages.get(mPosition).getPictureId();
                pictureUrl = mUserImages.get(mPosition).getUrl();
            } else {
                pictureId = mImages.get(mPosition).getPictureId();
                pictureUrl = mImages.get(mPosition).getUrl();
            }

            // Check if already shared before so we can show file (faster solution)
            mFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), pictureId + ".jpg");
            if (mFile.exists() && mFile.isFile()) {
                pictureUrl = "file://" + mFile.getPath();
                isFile = true;
            }

            // Set image
            Glide.with(this)
                    .load(pictureUrl)
                    .asBitmap()
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                            mBitmap = Bitmap.createScaledBitmap(resource, 1500, 1500, false);
                            imageView.setImageBitmap(mBitmap);
                            shareIcon.setVisibility(View.VISIBLE);
                            if (mProgressBar != null)
                                mProgressBar.setVisibility(View.GONE);

                            imageView.setOnFlickListener(new FlickableImageView.OnFlickableImageViewFlickListener() {
                                @Override
                                public void onStartFlick() {

                                }

                                @Override
                                public void onFinishFlick() {
                                    if (mView != null)
                                        mView.setVisibility(View.INVISIBLE);
                                    if (getActivity() != null)
                                        getActivity().finish();
                                }
                            });

                            imageView.setOnSingleTapListener(new FlickableImageView.OnFlickableImageViewSingleTapListener() {
                                @Override
                                public void onSingleTapConfirmed() {
                                    if (getActivity() != null) {
                                        getActivity().finish();
                                    }
                                }
                            });

                            imageView.setOnZoomListener(new FlickableImageView.OnFlickableImageViewZoomListener() {
                                @Override
                                public void onStartZoom() {
                                    ((DisplayUserImagesActivity) getActivity()).mPager.setPagingEnabled(false);
                                }

                                @Override
                                public void onBackFromMinScale() {
                                    ((DisplayUserImagesActivity) getActivity()).mPager.setPagingEnabled(true);
                                }
                            });

                            imageView.setDoubleTapEnabled(false);
                        }

                        @Override
                        public void onLoadFailed(Exception e, Drawable errorDrawable) {
                            super.onLoadFailed(e, errorDrawable);
                            if (mProgressBar != null)
                                mProgressBar.setVisibility(View.GONE);
                            Toast.makeText(getContext(), getString(R.string.an_error_string), Toast.LENGTH_SHORT).show();
//                            getActivity().finish();
                        }
                    });
            return mView;
        }

        @Override
        public void onDestroyView() {
            super.onDestroyView();
            mView = null;
        }

        @OnClick(R.id.display_image_share)
        protected void onShareClick() {
            ShareImageDialog dialog = new ShareImageDialog(getActivity(), isFile, mFile, mBitmap, pictureId);
            dialog.show();
        }
    }
}
