package com.app.livideo.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.app.livideo.R;
import com.app.livideo.fragments.ArtistRecordViewFragment;
import com.app.livideo.lv_app.LVInject;
import com.bumptech.glide.Glide;

import javax.inject.Inject;

import butterknife.ButterKnife;

/**
 * Created by MWheeler on 4/12/2016.
 */
public class OpenMicReviewActivity extends AppCompatActivity {
    @Inject Context mContext;

    private static final int CONTAINER_ID = R.id.fragment_container;
    public static final String RECORD_BUNDLE = "recordBundleItems";
    boolean isActive = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        initialize();
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActive = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isActive = true;
    }

    private void initialize() {
        LVInject.inject(this);
        ButterKnife.bind(this);
        ImageView mProgressBar = (ImageView) findViewById(R.id.viewer_settings_progress_bar);
        Glide.with(mContext).load(R.drawable.loading_anim).asGif().into(mProgressBar);

        isActive = true;
        ArtistRecordViewFragment recordPreviewFragment = new ArtistRecordViewFragment();
        recordPreviewFragment.setArguments(getIntent().getBundleExtra(ArtistRecordActivity.RECORD_BUNDLE));
        getSupportFragmentManager().beginTransaction().add(CONTAINER_ID, recordPreviewFragment).commit();
    }

    public void switchToRecordScreen(Bundle args){
        if (isActive) {
            Intent intent = new Intent(this, VideoRecordActivity.class);
            intent.putExtra(RECORD_BUNDLE, args);
            startActivityForResult(intent, 0);
        }
        finish();
    }
}
