package com.app.livideo.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.TextView;

import com.app.livideo.R;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.ScreenDensityUtil;
import com.app.livideo.utils.ViewUtils;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ViewerOrArtistActivity extends Activity {
    @Bind(R.id.viewer_or_artist_title) TextView title_text_view;
    boolean isActive = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewer_or_artist);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        ButterKnife.bind(this);
        isActive = true;
        ViewUtils.setMargins(title_text_view, 0, ScreenDensityUtil.getScreenHeightPixels(getApplicationContext(), 5), 0, 0);
    }

    @OnClick(R.id.im_a_fan_button)
    protected void onImAFanClick() {
        if (isActive) {
            Intent intent = new Intent(this, EnterPhoneNumberActivity.class);
            intent.putExtra(AppConstants.IS_USER_INTENT, true);
            startActivity(intent);
        }
    }

    @OnClick(R.id.im_a_celebrity_button)
    protected void onImACelebrityClick() {
        if (isActive) {
            Intent intent = new Intent(this, ArtistSocialSignUp.class);
            intent.putExtra(AppConstants.IS_USER_INTENT, false);
            startActivity(intent);
        }
    }

    @OnClick(R.id.viewer_or_artist_back_button)
    protected void onBackClick() {
        finish();
    }
}
