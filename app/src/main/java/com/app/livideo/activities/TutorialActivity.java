package com.app.livideo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.app.livideo.R;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.Preferences;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnPageChange;

public class TutorialActivity extends AppCompatActivity {

    @Bind(R.id.artist_tut_pager) ViewPager mPager;
    @Bind(R.id.artist_tut_indicator) LinearLayout pager_indicator;
    @Bind(R.id.header_main_img) ImageView logo;
//    @Bind(R.id.header_right_text) TextView skipText;
    @Bind(R.id.header_left_icon) ImageView backButton;
    TutFragmentPagerAdapter mAdapter;
    private ImageView[] dots;
    protected static int[] mImages;
    protected static boolean isBroadcaster;
    boolean isActive = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);
        ButterKnife.bind(this);
        isActive = true;
        boolean hasSeenTutorial = Preferences.getPreference(AppConstants.KEY_HAS_SEEN_TUTORIAL, getApplicationContext(), true);

        // Header
        logo.setVisibility(View.GONE);
        if (hasSeenTutorial) {
            backButton.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_back_wshadow));
            backButton.setVisibility(View.VISIBLE);
        } else {
            Preferences.setPreference(AppConstants.KEY_HAS_SEEN_TUTORIAL, true, getApplicationContext());
//            skipText.setText(getString(R.string.skip));
//            skipText.setTextColor(ContextCompat.getColor(this, R.color.inverse_text_color));
//            skipText.setVisibility(View.VISIBLE);
        }

        // Images
        isBroadcaster = getIntent().getBooleanExtra(AppConstants.KEY_IS_BROADCASTER, false);
        if (isBroadcaster) {
            mImages = new int[]{
                    R.drawable.artist_tutorial1,
                    R.drawable.artist_tutorial2,
                    R.drawable.artist_tutorial3,
                    R.drawable.artist_tutorial4,
                    R.drawable.artist_tutorial5,
                    R.drawable.artist_tutorial6
            };
        } /*else {
            mImages = new int[] {
                    R.drawable.viewer_tutorial1,
                    R.drawable.viewer_tutorial2,
                    R.drawable.viewer_tutorial3,
                    R.drawable.viewer_tutorial4,
                    R.drawable.viewer_tutorial5,
                    R.drawable.viewer_tutorial6
            };
        }*/

        // View
        mAdapter = new TutFragmentPagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mAdapter);
        mPager.setCurrentItem(0);
        setPageViewIndicator();
    }

    @OnPageChange(value = R.id.artist_tut_pager, callback = OnPageChange.Callback.PAGE_SELECTED)
    protected void onPageSelected(int position) {
        for (int i = 0; i < mImages.length; i++)
            dots[i].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.non_selected_item_dot));
        dots[position].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.selected_item_dot));

//        if (position == mImages.length - 1)
//            skipText.setText(getString(R.string.artist_recording_done_string));
    }

//    @OnClick(R.id.both_header_right_text)
    protected void onSkipClick() {
        Preferences.setPreference(AppConstants.KEY_HAS_SEEN_TUTORIAL, true, getApplicationContext());
        if (isActive) {
            Intent mIntent = new Intent(this, MainMenuActivity.class);
            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(mIntent);
        }
    }

//    @OnClick(R.id.both_header_left_icon)
    protected void onBackClick() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    private void setPageViewIndicator() {
        dots = new ImageView[mImages.length];

        for (int i = 0; i < mImages.length; i++) {
            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.non_selected_item_dot));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(8, 0, 8, 0);
            pager_indicator.addView(dots[i], params);
        }

        dots[0].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.selected_item_dot));
    }

    static class TutFragmentPagerAdapter extends FragmentStatePagerAdapter {
        public TutFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return mImages.length;
        }

        @Override
        public Fragment getItem(int position) {
            return TutImageFragment.newInstance(position);
        }
    }

    public static class TutImageFragment extends Fragment {
        private View mView;
        int mNum;

        static TutImageFragment newInstance(int num) {
            TutImageFragment f = new TutImageFragment();
            Bundle args = new Bundle();
            args.putInt("num", num);
            f.setArguments(args);
            return f;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            mNum = getArguments() != null ? getArguments().getInt("num") : 1;
            setRetainInstance(true);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            mView = inflater.inflate(R.layout.simple_image_view, container, false);
            final ImageView imageView = (ImageView) mView.findViewById(R.id.img_pager_item);
            imageView.setImageResource(mImages[mNum]);
            return mView;
        }

        @Override
        public void onDestroyView() {
            super.onDestroyView();
            mView = null;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActive = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isActive = true;
    }
}
