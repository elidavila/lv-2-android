package com.app.livideo.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.app.livideo.fragments.WebContentFragment;

public class BigDataViewer extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().add(android.R.id.content, WebContentFragment.newSingleType(getIntent().getStringExtra(WebContentFragment.URL), false)).commit();
        }
    }
}
