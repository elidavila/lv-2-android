package com.app.livideo.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.app.livideo.R;
import com.app.livideo.dialogs.CustomAlertDialog;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.Preferences;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class ConfirmImageActivity extends AppCompatActivity {
    private static final int MAX_SIZE = 4096;
    boolean isActive = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirn_image);
        isActive = true;
        ImageView imageView = (ImageView) findViewById(R.id.image_container);
        File tempPhoto = (File) getIntent().getSerializableExtra(AppConstants.TAKEN_PHOTE);

        if (tempPhoto != null) {
            String photoPath = tempPhoto.getAbsolutePath();
            Bitmap bmp = BitmapFactory.decodeFile(photoPath);

            if (bmp != null){
                CameraActivity cameraActivity = new CameraActivity();
                Matrix matrix = new Matrix();
                switch (getIntent().getIntExtra(AppConstants.TAKEN_ORIENTATION, -1)) {
                    case AppConstants.PORTRAIT:
                        if (Preferences.getPreference(AppConstants.LAST_CAMERA_USED, this, -1) == cameraActivity.getBackFacingCamera()) {
                            matrix.postRotate(90);
                        } else if (Preferences.getPreference(AppConstants.LAST_CAMERA_USED, this, -1) == cameraActivity.getFrontFacingCamera()) {
                            matrix.postRotate(270);
                        }
                        break;
                    case AppConstants.LANDSCAPE:
                        matrix.postRotate(0);
                        break;
                    case AppConstants.REVERSE_LAND:
                        matrix.postRotate(180);
                        break;
                }

                // Check if we need to resize bitmap based on maximum size allowed
                if (bmp.getWidth() > MAX_SIZE || bmp.getHeight() > MAX_SIZE) {
//                    Log.e("CROP IMAGE", "RESIZE NEEDED.");
                    bmp = resizeHugeBitmap(bmp); // resize image

                    // Check after resize if bitmap is valid size for displaying
                    if (bmp.getWidth() > MAX_SIZE || bmp.getHeight() > MAX_SIZE) {
                        displayErrorMessage();
                        return;
                    }
                }


                bmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);

                FileOutputStream fOut;
                try {
                    fOut = new FileOutputStream(tempPhoto);
                    bmp.compress(Bitmap.CompressFormat.JPEG, 75, fOut);
                    fOut.flush();
                    fOut.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                imageView.setImageURI(Uri.fromFile(tempPhoto));

            }

            Button retakeButton = (Button) findViewById(R.id.button_retake);
            retakeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isActive) {
                        if (getParent() == null) {
                            setResult(Activity.RESULT_CANCELED, new Intent());
                        } else {
                            getParent().setResult(Activity.RESULT_CANCELED, new Intent());
                        }
                        finish();
                    }
                }
            });

            Button usePhotoButton = (Button) findViewById(R.id.button_use);
            usePhotoButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getParent() == null) {
                        setResult(Activity.RESULT_OK, getIntent());
                    } else {
                        getParent().setResult(Activity.RESULT_OK, getIntent());
                    }
                    finish();
                }
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isActive = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActive = false;
    }

    private void displayErrorMessage() {
        Dialog dialog = new CustomAlertDialog(this, getString(R.string.access_denied_string), false);
        dialog.show();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                finish();
            }
        });
    }


    private Bitmap resizeHugeBitmap(Bitmap bitmap) {
        Bitmap resizedBitmap;
        int originalWidth = bitmap.getWidth();
        int originalHeight = bitmap.getHeight();
        int newWidth = -1;
        int newHeight = -1;
        float multFactor;
        if (originalHeight > originalWidth) {
            newHeight = MAX_SIZE;
            multFactor = (float) originalWidth / (float) originalHeight;
            newWidth = (int) (newHeight * multFactor);
        } else if (originalWidth > originalHeight) {
            newWidth = MAX_SIZE;
            multFactor = (float) originalHeight / (float) originalWidth;
            newHeight = (int) (newWidth * multFactor);
        } else if (originalHeight == originalWidth) {
            newHeight = MAX_SIZE;
            newWidth = MAX_SIZE;
        }
        resizedBitmap = Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, false);
        return resizedBitmap;
    }
}
