package com.app.livideo.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.app.livideo.R;
import com.app.livideo.fragments.BaseRecordFragment;
import com.app.livideo.fragments.GoLiveFragment;
import com.app.livideo.fragments.OpenMicFragment;
import com.app.livideo.fragments.ScheduledFragment;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.utils.Preferences;

import javax.inject.Inject;

public class VideoRecordActivity extends AppCompatActivity {

    private static final String LOGTAG = VideoRecordActivity.class.getSimpleName();
    private static final int FRAGMENT_CONTAINER = R.id.video_record_container;
    Fragment recordVideoFragment;
    Bundle bundle;
    int recordingType;
    @Inject Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_record);
        LVInject.inject(this);

        bundle = getIntent().getBundleExtra(ArtistRecordActivity.RECORD_BUNDLE);
        recordingType = bundle.getInt(ArtistRecordActivity.RECORDING_TYPE, 1);
        if (recordingType == ArtistRecordActivity.RECORD_TYPE_GO_LIVE) {
            recordVideoFragment = new GoLiveFragment();
        } else if (recordingType == ArtistRecordActivity.RECORD_TYPE_SCHEDULE) {
            recordVideoFragment = new ScheduledFragment();
        } else if (recordingType == ArtistRecordActivity.RECORD_TYPE_OPEN_MIC) {
            recordVideoFragment = new OpenMicFragment();
        }

        recordVideoFragment.setArguments(getIntent().getBundleExtra(ArtistRecordActivity.RECORD_BUNDLE));
        PhoneStateChangeListener pscl = new PhoneStateChangeListener();
        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        tm.listen(pscl, PhoneStateListener.LISTEN_CALL_STATE);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(FRAGMENT_CONTAINER, recordVideoFragment).commit();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (Preferences.getPreference(ArtistRecordActivity.SHARED_PREF_IS_RECDORDING, mContext, false)) {
            if (recordingType == ArtistRecordActivity.RECORD_TYPE_GO_LIVE) {
                ((GoLiveFragment) recordVideoFragment).stopRecording();
            } else if (recordingType == ArtistRecordActivity.RECORD_TYPE_SCHEDULE) {
                ((ScheduledFragment) recordVideoFragment).stopRecording();
            }
        }
        finish();
    }

    @Override
    public void onBackPressed() {
        if (Preferences.getPreference(ArtistRecordActivity.SHARED_PREF_IS_RECDORDING, mContext, false)) {
            if (!Preferences.getPreference(ArtistRecordActivity.SHARED_PREF_IS_GOING_LIVE, mContext, false)) {
                ((BaseRecordFragment) recordVideoFragment).stopRecording();
                setResult(RESULT_CANCELED);
                finish();
            }
        } else {
            setResult(RESULT_CANCELED);
            finish();
        }
    }

    private class PhoneStateChangeListener extends PhoneStateListener {
        public boolean wasRinging;

        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            switch (state) {
                case TelephonyManager.CALL_STATE_RINGING:
                    Log.i(LOGTAG, "RINGING");
                    wasRinging = true;
                    break;
                case TelephonyManager.CALL_STATE_OFFHOOK:
                    Log.i(LOGTAG, "OFFHOOK");

                    if (Preferences.getPreference(ArtistRecordActivity.SHARED_PREF_IS_RECDORDING, mContext, false)) {
                        if (recordingType == ArtistRecordActivity.RECORD_TYPE_GO_LIVE) {
                            ((GoLiveFragment) recordVideoFragment).stopRecording();
                        } else if (recordingType == ArtistRecordActivity.RECORD_TYPE_SCHEDULE) {
                            ((ScheduledFragment) recordVideoFragment).stopRecording();
                        }
                    }
                    finish();
                    // this should be the last piece of code before the break
                    wasRinging = true;
                    break;
                case TelephonyManager.CALL_STATE_IDLE:
                    Log.i(LOGTAG, "IDLE");
                    // this should be the last piece of code before the break
                    wasRinging = false;
                    break;
            }
        }
    }
}
