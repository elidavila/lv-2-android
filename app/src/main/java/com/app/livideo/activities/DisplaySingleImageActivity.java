package com.app.livideo.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.app.livideo.R;
import com.app.livideo.utils.AppConstants;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.goka.flickableview.FlickableImageView;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DisplaySingleImageActivity extends AppCompatActivity {

    @Bind(R.id.display_image_view) FlickableImageView imageView;
    @Bind(R.id.display_image_progress_bar) ImageView mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.display_image_view);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        ButterKnife.bind(this);
        Glide.with(this).load(R.drawable.loading_anim).asGif().into(mProgressBar);
        String pictureUrl = getIntent().getStringExtra(AppConstants.KEY_PICTURE_URL);
        Glide.with(this)
                .load(pictureUrl)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        if (mProgressBar != null)
                            mProgressBar.setVisibility(View.GONE);
                        Toast.makeText(DisplaySingleImageActivity.this, getString(R.string.an_error_string), Toast.LENGTH_SHORT).show();
                        finish();
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        if (mProgressBar != null)
                            mProgressBar.setVisibility(View.GONE);
                        imageView.setOnFlickListener(new FlickableImageView.OnFlickableImageViewFlickListener() {
                            @Override
                            public void onStartFlick() {

                            }

                            @Override
                            public void onFinishFlick() {
                                imageView.setVisibility(View.INVISIBLE);
                                finish();
                            }
                        });

                        imageView.setOnSingleTapListener(new FlickableImageView.OnFlickableImageViewSingleTapListener() {
                            @Override
                            public void onSingleTapConfirmed() {
                                finish();
                            }
                        });
                        return false;
                    }
                })
                .into(imageView);
    }
}
