package com.app.livideo.activities;

import android.content.Intent;
import android.location.Address;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.app.livideo.R;
import com.app.livideo.fragments.ArtistRecordHeadlineFragment;
import com.app.livideo.fragments.ArtistRecordViewFragment;
import com.app.livideo.utils.AndroidUtils;
import com.app.livideo.utils.LocationUtils;
import com.app.livideo.views.RecordLayout;
import com.google.android.gms.maps.model.LatLng;

import butterknife.ButterKnife;

public class ArtistRecordActivity extends AppCompatActivity {

    private static final int FRAGMENT_CONTAINER = R.id.artist_record_fragment_container;

    ArtistRecordHeadlineFragment recordNewVideoFragment;
    ArtistRecordViewFragment recordPreviewFragment;

    //Shared Preferences
    public static final String SHARED_PREF_NAME = "ArtistRecordHeadlineFragment";
    public static final String SHARED_PREF_DATA_KEY_VIDEO_FILE_PATH = "currentVideoFile";
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;

    /**
     * Is currently recording
     */
    public static final String SHARED_PREF_IS_RECDORDING = "recordingStatus";
    /**
     * Is planning on live streaming
     */
    public static final String SHARED_PREF_IS_GOING_LIVE = "goLiveStatus";

    //Bundle Items
    public static final String RECORD_BUNDLE = "recordBundleItems";
    public static final String SHARE_LOCATION = "shareLocal";
    public static final String SHARE_FACEBOOK = "shareFacebook";
    public static final String SHARE_TWITTER = "shareTwitter";
    public static final String SHARE_TIME = "shareTime";
    public static final String LOCATION = "location";
    public static final String RECORDING_TYPE = "recordingType";
    public static final int RECORD_TYPE_GO_LIVE = 0;
    public static final int RECORD_TYPE_SCHEDULE = 1;
    public static final int RECORD_TYPE_OPEN_MIC = 2;
    public static final int RECORD_CODE = 101;
    boolean isActive = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist_record);
        ButterKnife.bind(this);
        init();
        RecordLayout.setActivity(this);
    }

    private void init() {
        isActive = true;
        recordNewVideoFragment = new ArtistRecordHeadlineFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(FRAGMENT_CONTAINER, recordNewVideoFragment).commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        isActive = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActive = false;
    }

    @Override
    public void onBackPressed() {
        if (recordNewVideoFragment.isAdded()) {
            if (recordNewVideoFragment.isKeyboardOpen()) {
                recordNewVideoFragment.softKeyboard.closeSoftKeyboard();
                recordNewVideoFragment.softKeyboardHide();
                super.onBackPressed();
            } else
                super.onBackPressed();
        } else
            super.onBackPressed();
    }

    public void switchToRecordNewVideo(Bundle args) {
        recordNewVideoFragment.setArguments(args);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(FRAGMENT_CONTAINER, recordNewVideoFragment).commitAllowingStateLoss();
    }

    public void switchToRecordScreen(Bundle args) {
        if (isActive) {
            Intent intent = new Intent(this, VideoRecordActivity.class);
            intent.putExtra(RECORD_BUNDLE, args);
            startActivityForResult(intent, RECORD_CODE);
        }
    }

    public void switchToPreviewVideo(Bundle args) {
        recordPreviewFragment = new ArtistRecordViewFragment();
        recordPreviewFragment.setArguments(args);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(FRAGMENT_CONTAINER, recordPreviewFragment).commitAllowingStateLoss();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case RECORD_CODE:
                    switchToPreviewVideo(data.getBundleExtra(RECORD_BUNDLE));
                    break;
                case REQUEST_CHECK_SETTINGS:
                    // Get Current Location
                    new LocationUtils(ArtistRecordActivity.this, new LocationUtils.OnLocationListener() {
                        @Override
                        public void onGetLocation(Location location, Address address, LatLng latitudeLongitude) {
                            if (address != null) {
                                recordNewVideoFragment.mLastLocation = address;
                                recordNewVideoFragment.mLocationShare.setSelected(!recordNewVideoFragment.mLocationShare.isSelected());
                            }
                        }

                        @Override
                        public void onErrorWhileGettingLocation(String message) {
                            AndroidUtils.showShortToast(message, ArtistRecordActivity.this);
                        }
                    });
                    break;
            }
        } else if (resultCode == RESULT_CANCELED) {
            finish();
        } else {
            finish();
        }
    }
}
