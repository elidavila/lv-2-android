package com.app.livideo.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.app.livideo.R;
import com.app.livideo.api.APICalls;
import com.app.livideo.interfaces.DataAvailabilityHandler;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.User;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.Constants;
import com.app.livideo.utils.TextUtils;
import com.bumptech.glide.Glide;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class EnterDisplayNameActivity extends AppCompatActivity {

    @Inject RequestQueue mRequestQueue;
    @Inject LVSession mSession;
    @Inject Context mContext;

    @Bind(R.id.header_left_icon) ImageView header_left_icon;
    @Bind(R.id.header_title) TextView header_title;
    @Bind(R.id.enter_value_title) TextView enter_value_title;
    @Bind(R.id.enter_value_error_text) TextView error_text_view;
    @Bind(R.id.enter_value_action_text) TextView action_text_view;
    @Bind(R.id.enter_value_icon) ImageView enter_value_icon;
    @Bind(R.id.enter_value_edittext) EditText enter_value_edit_text;
    @Bind(R.id.enter_value_progress_bar) ImageView mProgressBar;
    @Bind(R.id.enter_value_touch_blocker) FrameLayout touch_blocker;
    @Bind(R.id.enter_value_footer_text) TextView enter_value_footer_text;

    private String mDisplayName;
    private boolean isNameValid, isActive = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enter_value_layout);
        ButterKnife.bind(this);
        LVInject.inject(this);
        initialize();
    }

    @Override
    protected void onResume() {
        super.onResume();
        isActive = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActive = false;
    }

    private void initialize() {
        // Header
        header_title.setText(getIntent().getStringExtra(AppConstants.EMAIL_KEY_INTENT));
        header_left_icon.setImageResource(R.drawable.ic_back_wshadow);

        // Body
        isActive = true;
        enter_value_title.setText(getString(R.string.create_a_display_name));
        enter_value_icon.setImageResource(R.drawable.ic_chat_off);
        enter_value_edit_text.setHint(R.string.username_string);
        action_text_view.setText(getString(R.string.next_string));
        enter_value_footer_text.setText(getString(R.string.email_registration_displayName));
        Glide.with(mContext).load(R.drawable.loading_anim).asGif().into(mProgressBar);
    }

    @OnClick(R.id.enter_value_action_button)
    protected void onNextClick() {
        if (isActive && isNameValid && !TextUtils.isNullOrEmpty(enter_value_edit_text.getText().toString())) {
            Intent intent = new Intent(this, EnterPasswordActivity.class);
            // previous intent extras
            intent.putExtra(AppConstants.IS_USER_INTENT, getIntent().getBooleanExtra(AppConstants.IS_USER_INTENT, true));
            intent.putExtra(AppConstants.USER_ID_KEY_INTENT, getIntent().getStringExtra(AppConstants.USER_ID_KEY_INTENT));
            intent.putExtra(AppConstants.DEVICE_ID_KEY_INTENT, getIntent().getStringExtra(AppConstants.DEVICE_ID_KEY_INTENT));
            intent.putExtra(AppConstants.PHONE_NUMBER_KEY_INTENT, getIntent().getStringExtra(AppConstants.PHONE_NUMBER_KEY_INTENT));
            intent.putExtra(AppConstants.ACTIVITY_KEY_INTENT, getIntent().getStringExtra(AppConstants.ACTIVITY_KEY_INTENT));
            intent.putExtra(AppConstants.EMAIL_KEY_INTENT, getIntent().getStringExtra(AppConstants.EMAIL_KEY_INTENT));

            // new intent extras
            intent.putExtra(AppConstants.DISPLAY_NAME_KEY_INTENT, mDisplayName);
            startActivity(intent);
        }
    }

    @OnTextChanged(value = R.id.enter_value_edittext, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    protected void verifyDisplayName() {
        mDisplayName = enter_value_edit_text.getText().toString();

        // Check for empty display name
        if (TextUtils.isNullOrEmpty(mDisplayName)) {
            displayUIError(true, null);
            return;
        }

        // Check for spaces in display name
        if (mDisplayName.contains(" ")) {
            displayUIError(true, getString(R.string.email_registration_displayName));
            return;
        }

        // Check for maximum amount of letters
        if (mDisplayName.length() > 25) {
            displayUIError(true, getString(R.string.display_name_too_long));
            return;
        }

        // Check name availability from server
        touch_blocker.setVisibility(View.VISIBLE);
        APICalls.requestDataAvailabilityCheck(mSession, EnterDisplayNameActivity.this, mContext,
                Constants.ACTION_USERNAME_AV, User.Keys.PARAMS_DISPLAY_NAME, mDisplayName,
                getIntent().getStringExtra(AppConstants.DEVICE_ID_KEY_INTENT),
                mRequestQueue, new DataAvailabilityHandler() {
                    @Override
                    public void RunCompleted(boolean isAvailable) {
                        touch_blocker.setVisibility(View.GONE);
                        if (isAvailable) {
                            displayUIError(false, null);
                        } else {
                            displayUIError(true, getString(R.string.email_registration_displayName_exists));
                        }
                    }
                });
    }

    @OnClick(R.id.enter_value_x)
    protected void onClearPhoneTextClick() {
        enter_value_edit_text.getText().clear();
    }

    private void displayUIError(final boolean isError, final String errorMsg) {
        isNameValid = !isError;
        if (isError) {
            enter_value_icon.setImageResource(R.drawable.ic_chat_off);
            if (errorMsg != null) {
                error_text_view.setText(errorMsg);
                error_text_view.setVisibility(View.VISIBLE);
            } else {
                error_text_view.setVisibility(View.INVISIBLE);
            }
        } else {
            enter_value_icon.setImageResource(R.drawable.ic_chat_on);
            error_text_view.setVisibility(View.INVISIBLE);
        }
    }

    @OnClick(R.id.header_left_card_view)
    protected void onBackClick() {
        finish();
    }
}
