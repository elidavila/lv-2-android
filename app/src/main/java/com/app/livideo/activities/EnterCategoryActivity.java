package com.app.livideo.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.app.livideo.R;
import com.app.livideo.api.APICalls;
import com.app.livideo.dialogs.CustomAlertDialog;
import com.app.livideo.interfaces.ArtistTypesHandler;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.ViewUtils;
import com.bumptech.glide.Glide;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EnterCategoryActivity extends AppCompatActivity {

    @Inject RequestQueue mRequestQueue;
    @Inject LVSession mSession;
    @Inject Context mContext;

    @Bind(R.id.header_left_icon) ImageView header_left_icon;
    @Bind(R.id.header_title) TextView header_title;
    @Bind(R.id.enter_category_number_picker) NumberPicker category_picker_view;
    @Bind(R.id.enter_category_yes_card_view) CardView yes_card_view;
    @Bind(R.id.enter_category_no_card_view) CardView no_card_view;
    @Bind(R.id.enter_category_action_button) CardView next_card_view;
    @Bind(R.id.enter_category_progress_bar) ImageView mProgressBar;

    private boolean isYesSelected, isNoSelected, isActive = true;
    private static final String IS_ALL_AGES = "1";
    private static final String IS_NOT_ALL_AGES = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_category);
        ButterKnife.bind(this);
        LVInject.inject(this);
        initialize();
        requestArtistTypesThenAddCategoryPicker();
    }

    private void initialize() {
        isActive = true;
        header_left_icon.setImageResource(R.drawable.ic_back_wshadow);
        Glide.with(mContext).load(R.drawable.loading_anim).asGif().into(mProgressBar);
    }

    @Override
    protected void onResume() {
        super.onResume();
        isActive = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActive = false;
    }

    @OnClick(R.id.enter_category_action_button)
    protected void onNextClick() {
        // Check if Artist has rated their content
        if (isActive) {
            if (!isYesSelected && !isNoSelected) {
                CustomAlertDialog dialog = new CustomAlertDialog(this, getString(R.string.you_need_to_rate_your_content), false);
                dialog.show();
                return;
            }

            // Add new intent extras
            Intent intent = new Intent(this, EnterMoneyActivity.class);
            intent.putExtra(AppConstants.ARTIST_CATEGORY_KEY_INTENT, getCategoryFromPicker());
            if (isYesSelected)
                intent.putExtra(AppConstants.IS_ALL_AGES_KEY, IS_NOT_ALL_AGES);
            else
                intent.putExtra(AppConstants.IS_ALL_AGES_KEY, IS_ALL_AGES);

            // Add old intent extras
            intent.putExtra(AppConstants.SOCIAL_ID_KEY_INTENT, getIntent().getStringExtra(AppConstants.SOCIAL_ID_KEY_INTENT));
            intent.putExtra(AppConstants.SOCIAL_TYPE_KEY_INTENT, getIntent().getStringExtra(AppConstants.SOCIAL_TYPE_KEY_INTENT));
            intent.putExtra(AppConstants.USER_ID_KEY_INTENT, getIntent().getStringExtra(AppConstants.USER_ID_KEY_INTENT));
            intent.putExtra(AppConstants.DEVICE_ID_KEY_INTENT, getIntent().getStringExtra(AppConstants.DEVICE_ID_KEY_INTENT));
            intent.putExtra(AppConstants.PHONE_NUMBER_KEY_INTENT, getIntent().getStringExtra(AppConstants.PHONE_NUMBER_KEY_INTENT));
            intent.putExtra(AppConstants.ACTIVITY_KEY_INTENT, getIntent().getStringExtra(AppConstants.ACTIVITY_KEY_INTENT));
            intent.putExtra(AppConstants.EMAIL_KEY_INTENT, getIntent().getStringExtra(AppConstants.EMAIL_KEY_INTENT));
            intent.putExtra(AppConstants.DISPLAY_NAME_KEY_INTENT, getIntent().getStringExtra(AppConstants.DISPLAY_NAME_KEY_INTENT));
            intent.putExtra(AppConstants.PASSWORD_KEY_INTENT, getIntent().getStringExtra(AppConstants.PASSWORD_KEY_INTENT));
            intent.putExtra(AppConstants.GENDER_KEY_INTENT, getIntent().getStringExtra(AppConstants.GENDER_KEY_INTENT));
            intent.putExtra(AppConstants.BIRTHDAY_KEY_INTENT, getIntent().getStringExtra(AppConstants.BIRTHDAY_KEY_INTENT));
            startActivity(intent);
        }
    }

    private String getCategoryFromPicker() {
        return mSession.getArtistTypesList().get(category_picker_view.getValue());
    }

    public void requestArtistTypesThenAddCategoryPicker() {
        category_picker_view.setVisibility(View.INVISIBLE);
        APICalls.requestArtistTypes(EnterCategoryActivity.this, mContext, mSession, mRequestQueue, new ArtistTypesHandler() {
            @Override
            public void RunCompleted(boolean isAvailable, List<String> artistTypes) {
                category_picker_view.setVisibility(View.VISIBLE);
                if (isAvailable) {
                    mSession.setArtistTypesList(artistTypes);
                    addCategoryPickerView();
                }
            }
        });
    }

    private void addCategoryPickerView() {
        category_picker_view.setMinValue(0);
        category_picker_view.setMaxValue(mSession.getArtistTypesList().size() - 1);
        category_picker_view.setWrapSelectorWheel(false);
        category_picker_view.setDisplayedValues(mSession.getArtistTypesList().toArray(new String[mSession.getArtistTypesList().size()]));
        ViewUtils.modifyPickerDivider(category_picker_view, Color.BLACK, 0);
        ViewUtils.setNumberPickerTextColor(category_picker_view, Color.BLACK);
        category_picker_view.setValue(mSession.getArtistTypesList().size() / 2);
    }

    @OnClick(R.id.enter_category_yes_card_view)
    protected void onYesClick() {
        // Handle Male Click
        if (isYesSelected) {
            isYesSelected = false;
            yes_card_view.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.livideo_grey));
            next_card_view.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.livideo_grey));
        } else {
            isYesSelected = true;
            yes_card_view.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.chat_blue));
            next_card_view.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.chat_blue));
        }

        // Handle Female default value
        no_card_view.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.livideo_grey));
        isNoSelected = false;
    }

    @OnClick(R.id.enter_category_no_card_view)
    protected void onNoClick() {
        // Handle Female Click
        if (isNoSelected) {
            isNoSelected = false;
            no_card_view.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.livideo_grey));
            next_card_view.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.livideo_grey));
        } else {
            isNoSelected = true;
            no_card_view.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.chat_blue));
            next_card_view.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.chat_blue));
        }

        // Handle Male default value
        yes_card_view.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.livideo_grey));
        isYesSelected = false;
    }

    @OnClick(R.id.header_left_card_view)
    protected void onBackClick() {
        finish();
    }

    @Override
    public void onBackPressed() {
        // Do nothing
    }
}
