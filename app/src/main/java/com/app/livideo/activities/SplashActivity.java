package com.app.livideo.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.android.volley.RequestQueue;
import com.app.livideo.R;
import com.app.livideo.interfaces.SessionHandler;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.validators.UserValidator;
import com.app.livideo.notifications.GCMRegistrationIntentService;
import com.app.livideo.utils.AndroidUtils;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.Constants;
import com.app.livideo.utils.Preferences;
import com.app.livideo.utils.SessionUtils;
import com.app.livideo.utils.UrlBuilder;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class SplashActivity extends AppCompatActivity {

    @Inject Context mContext;
    @Inject LVSession mSession;
    @Inject RequestQueue mRequestQueue;
    SessionUtils sessionUtils;
    public static String ANA_MONTANA_USER_ID = "";
    public static String LIVIDEO_USER_ID = "";
    public static String LIL_WAYNE_USER_ID = "";
    public static String LETTY_USER_ID = "";
    public static String mServerUrl = "";
    public static String mImgUploadString = "";
    public static String mStreamingServer = "";
    public static UrlBuilder.Endpoint mThumbnailEndpoint = null;
    public static UrlBuilder.Endpoint mImgUploadEndpoint = null;
    boolean isActive = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        LVInject.inject(this);
        ButterKnife.bind(this);
        isActive = true;
        switch (UrlBuilder.DEFAULT_HOST) {
            case SOFT_LAUNCH_SERVER:
                LIVIDEO_USER_ID = AppConstants.LIVIDEO_USER_ID_SOFT_LAUNCH;
                LIL_WAYNE_USER_ID = AppConstants.LIL_WAYNE_USER_ID_SOFT_LAUNCH;
                LETTY_USER_ID = AppConstants.LETTY_USER_ID_SOFT_LAUNCH;
                mServerUrl = UrlBuilder.create(UrlBuilder.Endpoint.API).build(UrlBuilder.Host.SOFT_LAUNCH_SERVER);
                mThumbnailEndpoint = UrlBuilder.Endpoint.SOFT_LAUNCH_THUMBNAILS;
                mImgUploadEndpoint = UrlBuilder.Endpoint.SOFT_LAUNCH_IMG_UPLOAD;
                mImgUploadString = AppConstants.SOFT_LAUNCH_IMG_UPLOAD;
                mStreamingServer = AppConstants.SOFTLAUNCH_SERVER;
                break;
            case PRODUCTION_SERVER:
                LIVIDEO_USER_ID = AppConstants.LIVIDEO_USER_ID_PROD;
                LIL_WAYNE_USER_ID = AppConstants.LIL_WAYNE_USER_ID_PROD;
                LETTY_USER_ID = AppConstants.LETTY_USER_ID_PROD;
                ANA_MONTANA_USER_ID = AppConstants.ANA_MONTANA_USER_ID_PROD;
                mServerUrl = UrlBuilder.create(UrlBuilder.Endpoint.API).build(UrlBuilder.Host.PRODUCTION_SERVER);
                mThumbnailEndpoint = UrlBuilder.Endpoint.PRODUCTION_THUMBNAILS;
                mImgUploadEndpoint = UrlBuilder.Endpoint.PRODUCTION_IMG_UPLOAD;
                mImgUploadString = AppConstants.PRODUCTION_IMG_UPLOAD;
                mStreamingServer = AppConstants.PRODUCTION_SERVER;
                break;
            case DEBUG_SERVER:
                LIVIDEO_USER_ID = AppConstants.LIVIDEO_USER_ID_DEBUG;
                mServerUrl = UrlBuilder.create(UrlBuilder.Endpoint.API).build(UrlBuilder.Host.DEBUG_SERVER);
                mThumbnailEndpoint = UrlBuilder.Endpoint.DEBUG_THUMBNAILS;
                mImgUploadEndpoint = UrlBuilder.Endpoint.DEBUG_IMG_UPLOAD;
                mImgUploadString = AppConstants.DEBUG_IMG_UPLOAD;
                mStreamingServer = AppConstants.DEBUG_SERVER;
                break;
        }

        if (AndroidUtils.checkPlayServices(mContext, this)) {
            LoadLivideo();
            mContext.startService(new Intent(mContext, GCMRegistrationIntentService.class));
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActive = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isActive = true;
    }

    private void LoadLivideo() {
        int userType = Preferences.getPreference(Constants.USER_TYPE_KEY, mContext, -1);
        if (userType == -1) {
            if (isActive) {
                startActivity(new Intent(this, GetStartedActivity.class));
                finish();
            }
            return;
        }

        if (UserValidator.isValid(mSession.getUser()) && SessionUtils.isCaching)
            LaunchProperActivity(userType);
        else
            LoginUser();
    }

    private void LoginUser() {
        sessionUtils = new SessionUtils(this, mContext);
        sessionUtils.cacheUserSession(new SessionHandler() {
            @Override
            public void RunCompleted(boolean successful, int userType) {
                if (successful) {
                    LaunchProperActivity(userType);
                } else {
                    LaunchLoginActivity();
                }
            }
        });
    }

    private void LaunchProperActivity(int userType) {
        switch (userType) {
            case Constants.GUEST_USER:
            case Constants.VIEWER_USER:
                LaunchNewMainMenu();
                break;
            case Constants.BROADCASTER_USER:
                int cameraFirst = Preferences.getPreference(Constants.CAMERA_TYPE_KEY, mContext, -1);
                if (cameraFirst == Constants.OPEN_CAMERA_FIRST)
                    LaunchRecordActivity();
                else
                    LaunchNewMainMenu();
                break;
            default:
                LaunchLoginActivity();
        }
    }

    private void LaunchArtistPage(Uri data) {

    }

    private void LaunchStream(Uri data) {

    }

    private void LaunchRecordActivity() {
        if (isActive) {
            Intent intent = new Intent(SplashActivity.this, ArtistRecordActivity.class);
            intent.putExtra(AppConstants.FROM_SPLASH, true);
            intent.putExtras(getIntent());
            startActivity(intent);
            finish();
        }
    }

    private void LaunchNewMainMenu() {
        if (isActive) {
            Intent intent = new Intent(SplashActivity.this, MainMenuActivity.class);
            intent.putExtra(AppConstants.KEY_FROM_RE_CACHE, true);
            intent.putExtras(getIntent());
            intent.setData(getIntent().getData());
            startActivity(intent);
            finish();
        }
    }

    private void LaunchLoginActivity() {
        if (isActive) {
            Intent i = new Intent(SplashActivity.this, GetStartedActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        System.exit(0);
    }
}
