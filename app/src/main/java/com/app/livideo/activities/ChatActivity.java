package com.app.livideo.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.app.livideo.R;
import com.app.livideo.api.APICalls;
import com.app.livideo.dialogs.CustomAlertDialog;
import com.app.livideo.fragments.ChatArtistFragment;
import com.app.livideo.fragments.ChatPrivateFragment;
import com.app.livideo.fragments.WhispersFragment;
import com.app.livideo.interfaces.CodeNumberHandler;
import com.app.livideo.interfaces.StringResultHandler;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.Artist;
import com.app.livideo.models.User;
import com.app.livideo.models.validators.ArtistValidator;
import com.app.livideo.models.validators.UserValidator;
import com.app.livideo.services.S3UploadService;
import com.app.livideo.utils.AndroidUtils;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.FileUtils;
import com.app.livideo.utils.TextUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ChatActivity extends AppCompatActivity {

    @Bind(R.id.chat_drawer_root_layout) DrawerLayout mDrawer;
    @Inject RequestQueue mRequestQueue;
    @Inject LVSession mSession;
    @Inject Context mContext;
    ChatArtistFragment mArtistChatFragment;
    public WhispersFragment mWhispersFragment;
    ChatPrivateFragment mPrivateChatFragment;
    boolean isWhisperVisible = false;
    public boolean ignoreEndPrivateChat = false;
    File imageFile;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);
        LVInject.inject(this);
        if (mSession == null || !UserValidator.isValid(mSession.getUser())) {
            AndroidUtils.restartApp(mContext);
            return;
        }
        initVariables();
        addChatFragment(getIntent());
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mSession == null || !UserValidator.isValid(mSession.getUser())) {
            AndroidUtils.restartApp(mContext);
            return;
        }

        replaceWhisperView();
        ignoreEndPrivateChat = false;
    }

    private void replaceWhisperView() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        if (mWhispersFragment != null)
            ft.replace(R.id.chat_whisper_container, mWhispersFragment);
        else
            ft.replace(R.id.chat_whisper_container, new WhispersFragment());
        ft.commitAllowingStateLoss();
    }

    private void initVariables() {
        mArtistChatFragment = new ChatArtistFragment();
        mWhispersFragment = new WhispersFragment();
    }

    public void addChatFragment(Intent intent) {
        saveCurrentArtist(intent);
        getSupportFragmentManager().beginTransaction().add(R.id.chat_frame_container, mArtistChatFragment).commit();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        LVInject.inject(this);
        replaceChatFragment(intent);
    }

    public void replaceChatFragment(Intent intent) {
        saveCurrentArtist(intent);
        getSupportFragmentManager().beginTransaction().replace(R.id.chat_frame_container, mArtistChatFragment).commit();
    }

    public void openDrawer() {
        isWhisperVisible = true;
        mDrawer.openDrawer(GravityCompat.END);
    }

    public void closeDrawer() {
        isWhisperVisible = false;
        mDrawer.closeDrawer(GravityCompat.END);
    }

    public void saveCurrentArtist(Intent intent) {
        Artist mArtist = (Artist) intent.getSerializableExtra(AppConstants.ARTIST_KEY_INTENT);
        if (ArtistValidator.isValid(mArtist))
            mSession.setCurrentArtist(mArtist);
    }

    public void startPrivateChat(final User privateChatUser) {
        ignoreEndPrivateChat = true;
        closeDrawer();

        if (mArtistChatFragment != null && mArtistChatFragment.mProgressBar != null)
            mArtistChatFragment.mProgressBar.setVisibility(View.VISIBLE);

        APICalls.requestStartPrivateChat(privateChatUser.getUserID(), ChatActivity.this, mContext, mRequestQueue, mSession, new CodeNumberHandler() {
            @Override
            public void RunCompleted(int code) {
                if (mArtistChatFragment != null && mArtistChatFragment.mProgressBar != null)
                    mArtistChatFragment.mProgressBar.setVisibility(View.GONE);
                switch (code) {
                    case AppConstants.STREAMCODE_SUCCESS:
                        addPrivateChatFragment(privateChatUser);
                        break;
                    case AppConstants.USERCODE_BLOCKED:
                        AndroidUtils.showShortToast(getString(R.string.has_blocked_you), ChatActivity.this);
                        break;
                    case AppConstants.NO_ARTIST_WHISPER:
                        AndroidUtils.showShortToast(getString(R.string.cant_private_chat_broadcaster), ChatActivity.this);
                        break;
                    case AppConstants.NON_MEMBERSHIP_WHISPER:
                        AndroidUtils.showShortToast(getString(R.string.user_has_no_membership), ChatActivity.this);
                        break;
                }
            }
        });
    }

    private void addPrivateChatFragment(User privateChatUser) {
        mPrivateChatFragment = new ChatPrivateFragment();
        getIntent().putExtra(AppConstants.USER, privateChatUser);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.chat_frame_container, mPrivateChatFragment)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

    public void onBackPressedFromArtistChat() {
        APICalls.requestEndPrivateChat(mSession.getCurrentArtist().getArtistId(), ChatActivity.this, mContext, mRequestQueue, mSession);
        finish();
        super.onBackPressed();
    }

    @Override
    public void onBackPressed() {
        if (isWhisperVisible) {
            closeDrawer();
        } else if (mPrivateChatFragment == null || !mPrivateChatFragment.isVisible()) {
            onBackPressedFromArtistChat();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (resultCode) {
            case Activity.RESULT_OK:
                switch (requestCode) {
                    case AppConstants.SELECT_PICTURE:
                        compressAndUploadImage(FileUtils.getFilePath(data.getData(), mContext));
                        break;
                    case AppConstants.TAKE_PICTURE:
                        File receivedFile = (File) data.getSerializableExtra(AppConstants.TAKEN_PHOTE);
                        compressAndUploadImage(receivedFile.getAbsolutePath());
                        break;
                }
        }
    }

    private void compressAndUploadImage(String path) {
        File mImageFile = new File(path);
        if (mImageFile != null && mImageFile.exists() && mImageFile.isFile()) {
            Bitmap mBitmap = getBitmap(path);
            imageFile = new File(mContext.getCacheDir(), System.currentTimeMillis() + ".jpg");
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            mBitmap.compress(Bitmap.CompressFormat.JPEG, 75, bos);
            byte[] bitmapdata = bos.toByteArray();
            FileOutputStream fos;
            try {
                fos = new FileOutputStream(imageFile);
                fos.write(bitmapdata);
                fos.flush();
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
                imageFile = null;
            }

            if (imageFile != null) {
                Toast.makeText(mContext, R.string.sending, Toast.LENGTH_SHORT).show();
                APICalls.requestUniquePictureID(ChatActivity.this, mContext, mRequestQueue, mSession, new StringResultHandler() {
                    @Override
                    public void onRunCompleted(String newUniquePictureId) {
                        StartS3UploadService(imageFile, newUniquePictureId, mSession.getCurrentPrivateChatUser().getUserID());
                    }
                });
            }
        } else {
            Dialog dialog = new CustomAlertDialog(this, getString(R.string.access_denied_string), false);
            dialog.show();
        }
    }

    private void StartS3UploadService(File file, String privatePictureId, String privateUserId) {
        Intent intent = new Intent(mContext, S3UploadService.class);
        intent.putExtra(S3UploadService.EXTRA_LOCAL_FILE, file);
        intent.putExtra(S3UploadService.EXTRA_IS_PICTURE, true);
        intent.putExtra(AppConstants.PRIVATE_PICTURE_ID, privatePictureId);
        intent.putExtra(AppConstants.PRIVATE_USER_ID, privateUserId);
        startService(intent);
    }

    private void displayErrorMessage() {
        Dialog dialog = new CustomAlertDialog(this, getString(R.string.access_denied_string), false);
        dialog.show();
    }

    private Bitmap getBitmap(String path) {
        if (!TextUtils.isNullOrEmpty(path)) {
            Bitmap mBitmap = BitmapFactory.decodeFile(path);    // Get bitmap image from file path
            if (mBitmap != null) {

                // Set rotation value
                int rotation = 0, rotationInDegrees = 0;
                try {
                    ExifInterface exifInterface = new ExifInterface(path);
                    rotation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                    rotationInDegrees = FileUtils.exifToDegrees(rotation);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                // Check if we need to resize bitmap based on maximum size allowed
                if (mBitmap.getWidth() > AppConstants.MAX_PHOTO_SIZE || mBitmap.getHeight() > AppConstants.MAX_PHOTO_SIZE) {
                    mBitmap = FileUtils.resizeHugeBitmap(mBitmap); // resize image
                }

                // Check if image needs rotation
                if (rotation != 0f) {
                    Matrix matrix = new Matrix();
                    matrix.preRotate(rotationInDegrees);   // matrix that will rotate our image
                    return Bitmap.createBitmap(mBitmap, 0, 0, mBitmap.getWidth(), mBitmap.getHeight(), matrix, false);
                } else {
                    return mBitmap;
                }
            } else {
                displayErrorMessage();
            }
        } else {
            displayErrorMessage();
        }
        return null;
    }

    public void handleGhostIconAnimation(boolean shouldAnimate) {
        if (mArtistChatFragment == null)
            return;
        if (shouldAnimate) {
            mArtistChatFragment.startAnimatingGhostIcon();
        } else {
            mArtistChatFragment.stopAnimatingGhostIcon();
        }
    }
}
