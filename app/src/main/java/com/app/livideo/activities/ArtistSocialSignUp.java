package com.app.livideo.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.android.volley.RequestQueue;
import com.app.livideo.R;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.social_login.InstagramLogin;
import com.app.livideo.social_login.TwitterLogin;
import com.app.livideo.social_login.YoutubeLogin;
import com.app.livideo.utils.AndroidUtils;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.Constants;
import com.app.livideo.utils.Preferences;
import com.bumptech.glide.Glide;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ArtistSocialSignUp extends AppCompatActivity {

    @Inject RequestQueue mRequestQueue;
    @Inject LVSession mSession;
    @Inject Context mContext;
    @Bind(R.id.social_sign_up_progress_bar) ImageView mProgressBar;
    @Bind(R.id.social_sign_up_touch_blocker) public FrameLayout touch_blocker;
    public static TwitterAuthClient sTwitterAuthClient;
    public boolean isActive = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist_social_sign_up);
        ButterKnife.bind(this);
        LVInject.inject(this);
        Glide.with(mContext).load(R.drawable.loading_anim).asGif().into(mProgressBar);
        isActive = true;
        touch_blocker.setOnTouchListener(AppConstants.touchListner);
    }

    @Override
    protected void onResume() {
        super.onResume();
        isActive = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActive = false;
    }

    @OnClick(R.id.social_sign_up_youtube)
    protected void onYoutubeClick() {
        YoutubeLogin.CreateInstance(ArtistSocialSignUp.this).requestLoginWithGoogle();
    }

    @OnClick(R.id.social_sign_up_twitter)
    protected void onTwitterClick() {
        TwitterLogin.CreateInstance(ArtistSocialSignUp.this).loginWithTwitter(mRequestQueue, touch_blocker);
    }

    @OnClick(R.id.social_sign_up_instagram)
    protected void onInstagramClick() {
        InstagramLogin.CreateInstance(ArtistSocialSignUp.this).loginWithInstagram(mRequestQueue, touch_blocker);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (Preferences.getPreference(Constants.TYPE_CALLBACK, mContext, "").equals(Constants.TYPE_TWITTER)) {
            sTwitterAuthClient.onActivityResult(requestCode, resultCode, data);
        } else {
            AndroidUtils.showShortToast(mContext.getString(R.string.retry_later), this);
        }
    }

    @OnClick(R.id.social_sign_up_back_button)
    protected void onBackClick() {
        finish();
    }
}
