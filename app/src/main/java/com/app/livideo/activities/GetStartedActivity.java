package com.app.livideo.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.app.livideo.R;
import com.app.livideo.api.APICalls;
import com.app.livideo.dialogs.VerifyNewDeviceDialog;
import com.app.livideo.interfaces.LoginHandler;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.User;
import com.app.livideo.notifications.GCMRegistrationIntentService;
import com.app.livideo.utils.AndroidUtils;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.Constants;
import com.app.livideo.utils.LVUtils;
import com.app.livideo.utils.Preferences;
import com.app.livideo.utils.SessionUtils;
import com.app.livideo.utils.TextUtils;
import com.bumptech.glide.Glide;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GetStartedActivity extends AppCompatActivity {

    @Inject RequestQueue mRequestQueue;
    @Inject LVSession mSession;
    @Inject Context mContext;
    
    @Bind(R.id.get_started_guest) TextView guest_view;
    @Bind(R.id.get_started_touch_blocker) FrameLayout touch_blocker;
    @Bind(R.id.get_started_progress_bar) ImageView mProgressBar;
    
    boolean isActive = true;
    private String deviceID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_started);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        ButterKnife.bind(this);
        LVInject.inject(this);
        touch_blocker.setOnTouchListener(AppConstants.touchListner);
        Glide.with(mContext).load(R.drawable.loading_anim).asGif().into(mProgressBar);
    }

    @OnClick(R.id.get_started_button)
    protected void onGetStartedClick() {
        if (isActive)
            startActivity(new Intent(this, ViewerOrArtistActivity.class));
    }

    @OnClick(R.id.get_started_sign_in)
    protected void onSignInClick() {
        if (isActive)
            startActivity(new Intent(this, SignInActivity.class));
    }

    @OnClick(R.id.get_started_guest)
    protected void onGuestClick() {
        AndroidUtils.hideKeyboard(mContext, guest_view);
        touch_blocker.setVisibility(View.VISIBLE);
        APICalls.SendAWSIdentity(mContext, mRequestQueue, mSession);
        LoginGuest();
    }

    private void LoginGuest() {
        deviceID = Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.ANDROID_ID);
        String userID = "temp_" + deviceID;
        APICalls.requestLoginUser(GetStartedActivity.this, userID, deviceID, mContext, mSession, mRequestQueue, new LoginHandler() {
            @Override
            public void RunCompleted(int UserCode, User user) {
                if (isActive) {
                    switch (UserCode) {
                        case Constants.USERCODE_SUCCESS:
                            user.setCurrentRole(User.UserRole.GUEST);
                            PerformBasicApiCalls(user);
                            break;
                        case Constants.USERCODE_INVALID_RESPONSE:
                            touch_blocker.setVisibility(View.GONE);
                            AndroidUtils.showShortToast(mContext.getString(R.string.invaled_server), GetStartedActivity.this);
                            break;
                        case Constants.USERCODE_NOT_EXISTS:
                            touch_blocker.setVisibility(View.GONE);
                            AndroidUtils.showShortToast(mContext.getString(R.string.user_does_not_exist), GetStartedActivity.this);
                            break;
                        case Constants.USERCODE_OTHER_ERROR:
                            touch_blocker.setVisibility(View.GONE);
                            AndroidUtils.showShortToast(mContext.getString(R.string.retry_later), GetStartedActivity.this);
                            break;
                        case Constants.USERCODE_OTHER_DEVICE:
                            touch_blocker.setVisibility(View.GONE);
                            Bundle bundle = new Bundle();
                            bundle.putString(AppConstants.DEVICE_ID_KEY_INTENT, deviceID);
                            VerifyNewDeviceDialog dialog = new VerifyNewDeviceDialog(GetStartedActivity.this, bundle);
                            dialog.show();
                            break;
                        default:
                            touch_blocker.setVisibility(View.GONE);
                            AndroidUtils.showShortToast(mContext.getString(R.string.wrong_un_or_pw), GetStartedActivity.this);  //wrong password response
                    }
                }
            }
        });
    }

    private void PerformBasicApiCalls(User user) {
        SaveGuestInfo(user);
        mContext.startService(new Intent(mContext, GCMRegistrationIntentService.class));
        APICalls.SendUserLocation(this, mContext, mSession, mRequestQueue);
        APICalls.requestArtistTypes(GetStartedActivity.this, mContext, mSession, mRequestQueue, null);
        launchMainActivity();
    }

    private void launchMainActivity() {
        SessionUtils.isCaching = true;
        touch_blocker.setVisibility(View.GONE);
        LaunchMainMenu();
    }

    private void SaveGuestInfo(User user) {
        Preferences.setPreference(Constants.KEY_USER_ID, "temp_" + deviceID, mContext);
        Preferences.setPreference(Constants.USER_TYPE_KEY, Constants.GUEST_USER, mContext);
        user.setCurrentRole(User.UserRole.GUEST);
        Preferences.setPreference(Constants.KEY_DEVICE, deviceID, mContext);
        user.setDeviceID(deviceID);
        mSession.setUser(user);
    }

    private void LaunchMainMenu() {
        if (isActive) {
            startActivity(new Intent(GetStartedActivity.this, MainMenuActivity.class));
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isActive = true;
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl)) {
            LVUtils.setServerUrl();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActive = false;
    }
}
