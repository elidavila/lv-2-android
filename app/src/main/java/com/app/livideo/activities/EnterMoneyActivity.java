package com.app.livideo.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.app.livideo.R;
import com.app.livideo.api.APICalls;
import com.app.livideo.dialogs.CustomAlertDialog;
import com.app.livideo.interfaces.RegistrationHandler;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.User;
import com.app.livideo.utils.AndroidUtils;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.Constants;
import com.app.livideo.utils.Preferences;
import com.bumptech.glide.Glide;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EnterMoneyActivity extends AppCompatActivity {

    private static final int CHECK_VIEW = 1, WIRE_VIEW = 2, PAYPAL_VIEW = 3;
    @Inject RequestQueue mRequestQueue;
    @Inject LVSession mSession;
    @Inject Context mContext;
    @Bind(R.id.enter_money_icon) ImageView money_icon;
    @Bind(R.id.enter_money_check_button) CardView check_card_view;
    @Bind(R.id.enter_money_wire_transfer_button) CardView wire_transfer_card_view;
    @Bind(R.id.enter_money_paypal_button) CardView paypal_card_view;
    @Bind(R.id.enter_money_action_button) CardView next_card_view;
    @Bind(R.id.enter_money_touch_blocker) FrameLayout touchBlocker;
    @Bind(R.id.header_left_icon) ImageView header_left_icon;
    @Bind(R.id.header_title) TextView header_title;
    @Bind(R.id.progress_bar) ImageView mProgressBar;
    private boolean isCheckValid, isWireTransferValid, isPayPalValid, isActive;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_money);
        ButterKnife.bind(this);
        LVInject.inject(this);
        touchBlocker.setOnTouchListener(AppConstants.touchListner);
        header_title.setText(R.string.awesome_password);
        header_left_icon.setImageResource(R.drawable.ic_back_wshadow);
        Glide.with(mContext).load(R.drawable.loading_anim).asGif().into(mProgressBar);
    }

    @OnClick(R.id.enter_money_check_button)
    protected void onCheckClick() {
        if (isCheckValid) {
            toggleCheckedButton(CHECK_VIEW, false);
        } else {
            toggleCheckedButton(CHECK_VIEW, true);
        }
    }

    @OnClick(R.id.enter_money_wire_transfer_button)
    protected void onWireTransferClick() {
        if (isWireTransferValid) {
            toggleCheckedButton(WIRE_VIEW, false);
        } else {
            toggleCheckedButton(WIRE_VIEW, true);
        }
    }

    @OnClick(R.id.enter_money_paypal_button)
    protected void onPayPalClick() {
        if (isPayPalValid) {
            toggleCheckedButton(PAYPAL_VIEW, false);
        } else {
            toggleCheckedButton(PAYPAL_VIEW, true);
        }
    }

    @OnClick(R.id.enter_money_action_button)
    protected void onNextClick() {
        if (!isCheckValid && !isWireTransferValid && !isPayPalValid) {
            CustomAlertDialog dialog = new CustomAlertDialog(this, getString(R.string.you_need_to_choose_one_option), false);
            dialog.show();
        } else {
            requestArtistRegistration();
        }
    }

    private void requestArtistRegistration() {
        if (isCheckValid)
            getIntent().putExtra(AppConstants.ARTIST_MONEY_KEY_INTENT, "check");
        else if (isWireTransferValid)
            getIntent().putExtra(AppConstants.ARTIST_MONEY_KEY_INTENT, "wire");
        else if (isPayPalValid)
            getIntent().putExtra(AppConstants.ARTIST_MONEY_KEY_INTENT, "paypal");

        touchBlocker.setVisibility(View.VISIBLE);
        APICalls.requestBroadcasterSocialRegistration(mSession, EnterMoneyActivity.this,
                mContext, getIntent(), mRequestQueue, new RegistrationHandler() {
                    @Override
                    public void RunCompleted(int RegistrationCode, final String userID) {
                        if (isActive) {
                            touchBlocker.setVisibility(View.GONE);
                            Preferences.setPreference(Constants.CAMERA_TYPE_KEY, Constants.OPEN_CAMERA_FIRST, EnterMoneyActivity.this);
                            switch (RegistrationCode) {
                                case Constants.REGISTRATIONCODE_SUCCESS:
                                    SaveUserData(userID);
                                    LaunchEnterProfilePictureActivity();
                                    break;
                                case Constants.REGISTRATIONCODE_ALREADY_EXISTS:
                                    AndroidUtils.showLongToast(getString(R.string.regestration_exists), EnterMoneyActivity.this);
                                    break;
                                case Constants.REGISTRATIONCODE_OTHER_ERROR:
                                    AndroidUtils.showLongToast(getString(R.string.retry_later), EnterMoneyActivity.this);
                                    break;
                            }
                        }
                    }
                });
    }

    private void SaveUserData(final String newUserID) {
        Preferences.setPreference(Constants.USER_TYPE_KEY, Constants.BROADCASTER_USER, mContext);
        mSession.getUser().setCurrentRole(User.UserRole.BROADCASTER);

        Preferences.setPreference(Constants.KEY_SOCIAL_ID, getIntent().getStringExtra(AppConstants.SOCIAL_ID_KEY_INTENT), mContext);
        mSession.getUser().setSocialID(getIntent().getStringExtra(AppConstants.SOCIAL_ID_KEY_INTENT));

        Preferences.setPreference(Constants.KEY_SOCIAL_TYPE, getIntent().getStringExtra(AppConstants.SOCIAL_TYPE_KEY_INTENT), mContext);
        mSession.getUser().setSocialType(getIntent().getStringExtra(AppConstants.SOCIAL_TYPE_KEY_INTENT));

        Preferences.setPreference(Constants.KEY_USER_ID, newUserID, mContext);
        mSession.getUser().setUserID(newUserID);

        Preferences.setPreference(Constants.KEY_DEVICE, getIntent().getStringExtra(AppConstants.DEVICE_ID_KEY_INTENT), mContext);
        mSession.getUser().setDeviceID(getIntent().getStringExtra(AppConstants.DEVICE_ID_KEY_INTENT));

        Preferences.setPreference(Constants.KEY_PHONE, getIntent().getStringExtra(AppConstants.PHONE_NUMBER_KEY_INTENT), mContext);

        Preferences.setPreference(Constants.KEY_DISPLAY_NAME, getIntent().getStringExtra(AppConstants.DISPLAY_NAME_KEY_INTENT), mContext);
        mSession.getUser().setDisplayName(getIntent().getStringExtra(AppConstants.DISPLAY_NAME_KEY_INTENT));

        Preferences.setPreference(Constants.KEY_BIRTHDATE, getIntent().getStringExtra(AppConstants.BIRTHDAY_KEY_INTENT), mContext);
        mSession.getUser().setBirthdate(getIntent().getStringExtra(AppConstants.BIRTHDAY_KEY_INTENT));

        Preferences.setPreference(Constants.KEY_EMAIL, getIntent().getStringExtra(AppConstants.EMAIL_KEY_INTENT), mContext);
        mSession.getUser().setEmail(getIntent().getStringExtra(AppConstants.EMAIL_KEY_INTENT));

        Preferences.setPreference(Constants.KEY_PW, getIntent().getStringExtra(AppConstants.PASSWORD_KEY_INTENT), mContext);

        Preferences.setPreference(AppConstants.ARTIST_CATEGORY_KEY_INTENT, getIntent().getStringExtra(AppConstants.ARTIST_CATEGORY_KEY_INTENT), mContext);
        mSession.getUser().setArtistType(getIntent().getStringExtra(AppConstants.ARTIST_CATEGORY_KEY_INTENT));

        Preferences.setPreference(AppConstants.IS_ALL_AGES_KEY, getIntent().getStringExtra(AppConstants.IS_ALL_AGES_KEY), mContext);
        mSession.getUser().setIsAllAges(getIntent().getStringExtra(AppConstants.IS_ALL_AGES_KEY));
    }

    private void toggleCheckedButton(final int checkedView, final boolean shouldCheck) {
        if (shouldCheck) {
            money_icon.setImageResource(R.drawable.ic_money_on);
            switch (checkedView) {
                case CHECK_VIEW:
                    check_card_view.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.chat_blue));
                    next_card_view.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.chat_blue));
                    wire_transfer_card_view.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.livideo_grey));
                    paypal_card_view.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.livideo_grey));
                    isCheckValid = true; isWireTransferValid = false; isPayPalValid = false;
                    break;
                case WIRE_VIEW:
                    wire_transfer_card_view.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.chat_blue));
                    next_card_view.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.chat_blue));
                    check_card_view.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.livideo_grey));
                    paypal_card_view.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.livideo_grey));
                    isCheckValid = false; isWireTransferValid = true; isPayPalValid = false;
                    break;
                case PAYPAL_VIEW:
                    paypal_card_view.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.chat_blue));
                    next_card_view.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.chat_blue));
                    wire_transfer_card_view.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.livideo_grey));
                    check_card_view.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.livideo_grey));
                    isCheckValid = false; isWireTransferValid = false; isPayPalValid = true;
                    break;
            }
        } else {
            money_icon.setImageResource(R.drawable.ic_money_off);
            check_card_view.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.livideo_grey));
            next_card_view.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.livideo_grey));
            wire_transfer_card_view.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.livideo_grey));
            paypal_card_view.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.livideo_grey));
            isCheckValid = false; isWireTransferValid = false; isPayPalValid = false;
        }
    }

    private void LaunchEnterProfilePictureActivity() {
        if (isActive) {
            Intent intent = new Intent(EnterMoneyActivity.this, EnterProfilePictureActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.putExtra(AppConstants.IS_USER_INTENT, false);
            intent.putExtra(AppConstants.ARTIST_CATEGORY_KEY_INTENT, getIntent().getStringExtra(AppConstants.ARTIST_CATEGORY_KEY_INTENT));
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isActive = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActive = false;
    }
}