package com.app.livideo.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.android.volley.RequestQueue;
import com.app.livideo.R;
import com.app.livideo.fragments.HelpContentFragment;
import com.app.livideo.fragments.WebContentFragment;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.User;
import com.app.livideo.utils.AppConstants;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NewSettingsActivity extends AppCompatActivity {

    public static final String FRAGMENT_TYPE = "fragmentType";
    public static final int HELP = 0;
    public static final int PROFILE = 1;
    public static final int TERMS = 2;
    public static final int PRIVACY = 3;
    public static final int FAQ = 4;
    @Bind(R.id.header_left_icon) ImageView leftImage;
    @Inject Context mContext;
    @Inject LVSession mSession;
    @Inject RequestQueue mRequestQueue;

    private static final int CONTAINER_ID = R.id.fragment_container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_settings);
        LVInject.inject(this);
        ButterKnife.bind(this);
        leftImage.setImageResource(R.drawable.ic_back_wshadow);
        switch (getIntent().getIntExtra(FRAGMENT_TYPE, 4)) {
            case HELP:
                HelpContentFragment helpContentFragment = new HelpContentFragment();
                android.support.v4.app.FragmentTransaction ft1 = getSupportFragmentManager().beginTransaction();
                ft1.replace(CONTAINER_ID, helpContentFragment);
                ft1.addToBackStack(null);
                ft1.commit();
                return;
            case PROFILE:

                return;
            case TERMS:
                WebContentFragment webContentFragment;
                if (mSession.getUser().getUserRole() == User.UserRole.VIEWER || mSession.getUser().getUserRole() == User.UserRole.GUEST) {
                    webContentFragment = WebContentFragment.newSingleType(AppConstants.TermsViewer, true);
                } else if (mSession.getUser().getUserRole() == User.UserRole.BROADCASTER) {
                    webContentFragment = WebContentFragment.newSingleType(AppConstants.TermsBroadcaster, true);
                } else {
                    webContentFragment = new WebContentFragment();
                }
                android.support.v4.app.FragmentTransaction ft2 = getSupportFragmentManager().beginTransaction();
                ft2.replace(CONTAINER_ID, webContentFragment);
                ft2.addToBackStack(null);
                ft2.commit();
                return;
            case PRIVACY:
                WebContentFragment webContentFragment3 = WebContentFragment.newSingleType(AppConstants.Privacy, true);
                android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(CONTAINER_ID, webContentFragment3);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                return;
            default:
                finish();
        }
    }

    public void navigateToFAQ() {
        WebContentFragment webContentFragment2;
        if (mSession.getUser().getUserRole() == User.UserRole.VIEWER || mSession.getUser().getUserRole() == User.UserRole.GUEST) {
            webContentFragment2 = WebContentFragment.newSingleType(AppConstants.FAQViewer, true);
        } else if (mSession.getUser().getUserRole() == User.UserRole.BROADCASTER) {
            webContentFragment2 = WebContentFragment.newSingleType(AppConstants.FAQBroadcaster, true);
        } else {
            webContentFragment2 = new WebContentFragment();
        }
        android.support.v4.app.FragmentTransaction ft3 = getSupportFragmentManager().beginTransaction();
        ft3.replace(CONTAINER_ID, webContentFragment2);
        ft3.addToBackStack(null);
        ft3.commit();
    }

    @OnClick(R.id.header_left_card_view)
    public void backArrowPressed(){
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}

