package com.app.livideo.activities;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Matrix;
import android.graphics.SurfaceTexture;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.TextureView;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.app.livideo.BuildConfig;
import com.app.livideo.R;
import com.app.livideo.adapters.ArtistChatAdapter;
import com.app.livideo.api.APICalls;
import com.app.livideo.exoplayer.EventLogger;
import com.app.livideo.exoplayer.player.DemoPlayer;
import com.app.livideo.exoplayer.player.ExtractorRendererBuilder;
import com.app.livideo.exoplayer.player.HlsRendererBuilder;
import com.app.livideo.interfaces.ArtistHandler;
import com.app.livideo.interfaces.ChatMessagesHandler;
import com.app.livideo.interfaces.DataAvailabilityHandler;
import com.app.livideo.interfaces.StarStreamHandler;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.Artist;
import com.app.livideo.models.ArtistChatMessage;
import com.app.livideo.models.Stream;
import com.app.livideo.models.User;
import com.app.livideo.models.validators.ArtistChatMessageValidator;
import com.app.livideo.models.validators.ArtistPictureValidator;
import com.app.livideo.models.validators.ArtistValidator;
import com.app.livideo.models.validators.StreamValidator;
import com.app.livideo.models.validators.UserValidator;
import com.app.livideo.utils.AndroidUtils;
import com.app.livideo.utils.AnimationUtils;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.LVUtils;
import com.app.livideo.utils.PicassoCircleTransform;
import com.app.livideo.utils.ScreenDensityUtil;
import com.app.livideo.utils.TextUtils;
import com.app.livideo.utils.TimeUtils;
import com.app.livideo.utils.ViewUtils;
import com.bumptech.glide.Glide;
import com.google.android.exoplayer.ExoPlayer;
import com.google.android.exoplayer.audio.AudioCapabilities;
import com.google.android.exoplayer.audio.AudioCapabilitiesReceiver;
import com.google.android.exoplayer.drm.UnsupportedDrmException;
import com.google.android.exoplayer.metadata.GeobMetadata;
import com.google.android.exoplayer.metadata.PrivMetadata;
import com.google.android.exoplayer.metadata.TxxxMetadata;
import com.google.android.exoplayer.util.Util;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.PicassoTools;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class PlayerActivity extends AppCompatActivity implements SurfaceHolder.Callback, DemoPlayer.Listener,
        DemoPlayer.Id3MetadataListener, AudioCapabilitiesReceiver.Listener, TextureView.SurfaceTextureListener {

    private static final String HLS_EXTENSION = "m3u8", TAG = PlayerActivity.class.getSimpleName();
    private static final int PLAY_BUTTON_ANIMATION_LENGTH = 500, ARTIST_DETAIL_FADE_ANIMATION_LENGTH = 500,
            UPDATE_CHAT_INTERVAL = 1500, ARTIST_DETAIL_DELAY_ANIMATION_LENGTH = 3000, LOGO_FADE_IN_ANIMATION_LENGTH = 1000;
    private static final Object synchronizationLock = new Object();
    @Inject RequestQueue mRequestQueue;
    @Inject LVSession mSession;
    @Inject Context mContext;
    @Bind(R.id.surface_view) TextureView textureView;
    @Bind(R.id.activity_player_layout) RelativeLayout root;
    @Bind(R.id.viewer_player_header) View header;
    @Bind(R.id.player_star_chat_view) View star_chat_view;
    @Bind(R.id.header_left_icon) ImageView closeButton;
    @Bind(R.id.header_right_icon) ImageView muteButton;
    @Bind(R.id.header_main_img) ImageView headerLivideo;
    @Bind(R.id.player_artist_details) View artist_details_view;
    @Bind(R.id.player_artist_icon) ImageView artist_details_image_view;
    @Bind(R.id.player_artist_headline) TextView artist_details_headline_view;
    @Bind(R.id.player_artist_name) TextView artist_details_name_view;
    @Bind(R.id.player_artist_time_stamp) TextView artist_details_time_stamp_view;
    @Bind(R.id.video_frame) FrameLayout videoFrame;
    @Bind(R.id.activity_player_progress_bar) ImageView mProgressBar;
    @Bind(R.id.player_footer_star_image_view) ImageView star_image_view;
    @Bind(R.id.player_footer_star_text_view) TextView star_text_view;
    @Bind(R.id.player_comment_recyclerview) RecyclerView commentView;
    @Bind(R.id.viewer_player_transparent_view) ImageView transparentView;
    @Bind(R.id.player_play_pause) ImageView playPauseButton;
    @Bind(R.id.player_chat_layout) View msgView;
    @Bind(R.id.chat_edit_msg) EditText commentEditTextView;
    @Bind(R.id.chat_send_msg_button) TextView sendButton;
    @Bind(R.id.player_footer_chat_icon) ImageView chat_icon;
    @Bind(R.id.player_footer_star_card_view) CardView star_card_view;
    Stream mStream;
    AudioManager mAudioManager;
    LinearLayoutManager mLayoutManager;
    ArtistChatAdapter artistChatAdapter;
    Handler fadeOutArtistDetailHandler = new Handler();
    int videoWidth, videoHeight, currentVolume = 0, FROM_PAGE = 0;
    private long playerPosition;
    private boolean playerNeedsPrepare, callOnce = true, isHLS = false;
    private boolean isActive = true, shouldStopUpdatingView = false, isUserAtBottom;
    private boolean shouldFadeOutArtistDetail = true, isArtistDetailVisible = true, isDownloadingChat;
    private EventLogger eventLogger;
    private DemoPlayer player;
    private Uri contentUri;
    private AudioCapabilitiesReceiver audioCapabilitiesReceiver;
    private Animation playButtonAnimation, artistDetailFadeOutAnimation, artistDetailFadeInAnimation, logoFadeInAnimation;

    Runnable fadeOutArtistDetailRunnable = new Runnable() {
        @Override
        public void run() {
            if (shouldFadeOutArtistDetail)
                artist_details_view.startAnimation(artistDetailFadeOutAnimation);
        }
    };

    @OnTextChanged(R.id.chat_edit_msg)
    protected void onChatMessageChange(CharSequence text) {
        if (text.length() == 0) {
            sendButton.setEnabled(false);
            sendButton.setAlpha(0.3f);
        } else {
            sendButton.setEnabled(true);
            sendButton.setAlpha(1);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        LVInject.inject(this);
        ButterKnife.bind(this);
        startPlayerView();
    }

    private void startPlayerView() {
        initVariables();
        initHeaderFooter();
        setArtistDetailsView();
        initAnimations();
        initVideo();
        initChat();
    }

    @OnClick(R.id.player_footer_star_card_view)
    protected void onStarClick() {
        star_card_view.setEnabled(false);
        if (mStream.isDidStar()) {
            star_image_view.setImageResource(R.drawable.ic_staroffhollow_wshadow);
            mStream.setDidStar(false);
            requestStarStream("0");
            if (!TextUtils.isNullOrEmpty(star_text_view.getText().toString())) {
                star_text_view.setText(String.valueOf(Integer.valueOf(star_text_view.getText().toString()) - 1));
                star_text_view.setTextColor(ContextCompat.getColor(mContext, R.color.livideo_white));
            }
        } else {
            star_image_view.setImageResource(R.drawable.ic_staron_wshadow);
            mStream.setDidStar(true);
            requestStarStream("1");
            if (!TextUtils.isNullOrEmpty(star_text_view.getText().toString())) {
                star_text_view.setText(String.valueOf(Integer.valueOf(star_text_view.getText().toString()) + 1));
                star_text_view.setTextColor(ContextCompat.getColor(mContext, R.color.livideo_yellow));
            }
        }
    }

    private void requestStarStream(String didStar) {
        APICalls.requestStarStream(mStream.getStreamId(), didStar, PlayerActivity.this, mContext, mSession, mRequestQueue, new StarStreamHandler() {
            @Override
            public void RunCompleted(int code, String stars) {
                star_card_view.setEnabled(true);
                star_text_view.setText(stars);
            }
        });
    }

    private void setStarNumber() {
        APICalls.requestGetStreamStars(mStream.getStreamId(), PlayerActivity.this, mContext, mSession, mRequestQueue, new StarStreamHandler() {
            @Override
            public void RunCompleted(int code, String stars) {
                synchronized (synchronizationLock) {
                    if (!star_text_view.getText().toString().equalsIgnoreCase(stars))
                        star_text_view.setText(stars);
                }
            }
        });
    }

    private void setArtistDetailsView() {
        final int artistImageSize = ScreenDensityUtil.getScreenHeightPixels(mContext, 15);
        artist_details_image_view.setMinimumWidth(artistImageSize);
        APICalls.requestArtistInfo(mStream.getArtistId(), PlayerActivity.this, mContext, mSession, mRequestQueue, new ArtistHandler() {
            @Override
            public void RunCompleted(boolean isAvailable, final Artist artist) {
                if (ArtistValidator.isValid(artist)) {
                    if (ArtistPictureValidator.isValid(artist.getArtistPictures())) {
                        Picasso.with(mContext)
                                .load(artist.getArtistPictures().get(artist.getArtistPictures().size() - 1).getUrl())
                                .resize(artistImageSize, artistImageSize)
                                .transform(new PicassoCircleTransform())
                                .into(artist_details_image_view, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                        artist_details_headline_view.setText(mStream.getHeadline());
                                        artist_details_name_view.setText(artist.getDisplayName());
                                        artist_details_time_stamp_view.setText(TimeUtils.convertTimeForStream(mStream.getTime(), mContext));
                                    }

                                    @Override
                                    public void onError() {

                                    }
                                });
                    }
                }
            }
        });
    }

    private void initChat() {
        mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mLayoutManager.setStackFromEnd(true);
        commentView.setLayoutManager(mLayoutManager);

        if (StreamValidator.isValid(mStream)) {
            if (ArtistChatMessageValidator.isValid(mStream.getStreamComments())) {
                addChatView(mStream.getStreamComments()); // Chat already cached
            } else {
                requestChatMsgs(false, true); // Get new chat data
            }
        }
    }

    private void requestChatMsgs(final boolean isUpdating, final boolean shouldScrollToBottom) {
        if (StreamValidator.isValid(mStream)) {
            String messageId = "0";

            if (isUpdating && ArtistChatMessageValidator.isValid(mStream.getStreamComments())) {
                messageId = mStream.getStreamComments().get(mStream.getStreamComments().size() - 1).getMessageId();
            }

            isDownloadingChat = true;
            APICalls.requestStreamChatMsgs(PlayerActivity.this, mContext, mStream.getStreamId(), mStream.getArtistId(), messageId, "70", mSession, mRequestQueue, new ChatMessagesHandler() {
                @Override
                public void onRunComplete(List<ArtistChatMessage> newChatMessages) {
                    isDownloadingChat = false;
                    // ERROR CHECK
                    if (!StreamValidator.isValid(mStream) || newChatMessages == null)
                        return;

                    // REVERSE LIST
                    Collections.reverse(newChatMessages);

                    // INITIALIZE
                    if (!isUpdating || !ArtistChatMessageValidator.isValid(mStream.getStreamComments())) {
                        addChatView(newChatMessages);
                        return;
                    }

                    // UPDATE ONLY
                    if (ArtistChatMessageValidator.isValid(newChatMessages)) {
                        synchronized (synchronizationLock) {
                            isUserAtBottom = ViewUtils.isLastItemDisplaying(commentView); // IMPORTANT HERE
                            mStream.getStreamComments().addAll(newChatMessages);
                            artistChatAdapter.bind(mStream.getStreamComments());
                            if (shouldScrollToBottom || isUserAtBottom) {
                                commentView.scrollToPosition(mStream.getStreamComments().size() - 1);
                                if (shouldScrollToBottom) {
                                    shouldStopUpdatingView = false;
                                    loadComments();
                                }
                            } else {
                                commentView.scrollToPosition(mStream.getStreamComments().size());
                            }
                        }
                    }
                }
            });
        }
    }

    // Set chat data
    private void addChatView(List<ArtistChatMessage> newChatMessages) {
        mStream.setStreamComments(newChatMessages);
        if (StreamValidator.isValid(mStream) && ArtistChatMessageValidator.isValid(mStream.getStreamComments())) {
            artistChatAdapter = new ArtistChatAdapter(PlayerActivity.this, mStream.getArtistId(), mStream.getStreamComments(), null);
            commentView.setAdapter(artistChatAdapter);
        }
    }

    private void initVariables() {
        mStream = (Stream) getIntent().getSerializableExtra(AppConstants.ACTIVE_STREAM_KEY_INTENT);
        Log.e("ACTIVE STREAM", "Stream ID: " + mStream.getStreamId() + ", URL: " + mStream.getUrl());
        FROM_PAGE = getIntent().getIntExtra(AppConstants.FROM_PAGE_KEY, 0);
        AndroidUtils.hideKeyboard(mContext, root);
        textureView.setSurfaceTextureListener(this);
        Glide.with(mContext).load(R.drawable.loading_anim).asGif().into(mProgressBar);

        // Audio
        audioCapabilitiesReceiver = new AudioCapabilitiesReceiver(this, this);
        audioCapabilitiesReceiver.register();
    }

    private void initHeaderFooter() {
        headerLivideo.setVisibility(View.VISIBLE);
        msgView.setVisibility(View.GONE);
        sendButton.setEnabled(false);
        sendButton.setAlpha(0.3f);

        // X icon (Exit)
        closeButton.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_x_wshadows));
        closeButton.setVisibility(View.VISIBLE);

        // Mute / UnMute Audio Button
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        toggleMuteButton((mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC) == 0));
        muteButton.setVisibility(View.VISIBLE);

        // Star on / off
        setStarNumber();
        if (mStream.isDidStar()) {
            star_image_view.setImageResource(R.drawable.ic_staron_wshadow);
            star_text_view.setTextColor(ContextCompat.getColor(mContext, R.color.livideo_yellow));
        } else {
            star_image_view.setImageResource(R.drawable.ic_staroffhollow_wshadow);
            star_text_view.setTextColor(ContextCompat.getColor(mContext, R.color.livideo_white));
        }
    }

    private void initAnimations() {
        playButtonAnimation = AnimationUtils.getMyAlphaAnimation(1, 0, PLAY_BUTTON_ANIMATION_LENGTH, new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                playPauseButton.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                playPauseButton.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        logoFadeInAnimation = AnimationUtils.getMyAlphaAnimation(0, 1, LOGO_FADE_IN_ANIMATION_LENGTH, new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                transparentView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        artistDetailFadeOutAnimation = AnimationUtils.getMyAlphaAnimation(1, 0, ARTIST_DETAIL_FADE_ANIMATION_LENGTH, new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                artist_details_view.setVisibility(View.VISIBLE);
                isArtistDetailVisible = true;
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                artist_details_view.setVisibility(View.GONE);
                isArtistDetailVisible = false;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });


        artistDetailFadeInAnimation = AnimationUtils.getMyAlphaAnimation(0, 1, ARTIST_DETAIL_FADE_ANIMATION_LENGTH, new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                artist_details_view.setVisibility(View.VISIBLE);
                isArtistDetailVisible = true;
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    protected void toggleControlsVisibility() {
        if (player == null)
            preparePlayer();

        // Play / Pause Video
        mProgressBar.setVisibility(View.GONE);
        playPauseButton.setVisibility(View.VISIBLE);
        if (player.getPlayerControl().isPlaying()) {
            shouldFadeOutArtistDetail = false;
            player.getPlayerControl().pause();
            player.setPlayWhenReady(false);
            playPauseButton.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_play_circled));
            fadeOutArtistDetailHandler.removeCallbacks(fadeOutArtistDetailRunnable);
            if (!isArtistDetailVisible) {
                artist_details_view.startAnimation(artistDetailFadeInAnimation);
            }
        } else {
            shouldFadeOutArtistDetail = true;
            player.getPlayerControl().start();
            player.setPlayWhenReady(true);
            playPauseButton.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_pause_circled));
            if (isArtistDetailVisible)
                fadeOutArtistDetailHandler.postDelayed(fadeOutArtistDetailRunnable, ARTIST_DETAIL_DELAY_ANIMATION_LENGTH);
        }
        playPauseButton.startAnimation(playButtonAnimation);
    }

    private void initVideo() {
        if (StreamValidator.isValid(mStream)) {
            // Check if video is HLS (go live)
            if (mStream.getUrl().contains(HLS_EXTENSION))
                isHLS = true;
            contentUri = Uri.parse(mStream.getUrl());
        } else {
            Toast.makeText(mContext, getString(R.string.retry_later), Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    private void updateTextureViewSize(float videoWidth, float videoHeight) {
        float viewWidth = ScreenDensityUtil.getScreenDimensions(mContext)[ScreenDensityUtil.WIDTH];
        float viewHeight = ScreenDensityUtil.getScreenDimensions(mContext)[ScreenDensityUtil.HEIGHT];

        float scaleX = 1.0f;
        float scaleY = 1.0f;

        float viewRatio = viewWidth / viewHeight;
        float videoRatio = videoWidth / videoHeight;
        if (viewRatio > videoRatio) {
            // video is higher than view
            scaleY = videoHeight / videoWidth * viewRatio;
        } else {
            //video is wider than view
            scaleX = videoWidth / videoHeight / viewRatio;
        }

        Matrix matrix = new Matrix();
        matrix.setScale(scaleX, scaleY, viewWidth / 2, viewHeight / 2);
        textureView.setTransform(matrix);
    }

    @OnClick(R.id.player_footer_chat_icon)
    protected void onCommentsBubbleClick() {
        if (mSession.getUser().getUserRole() == User.UserRole.GUEST) {
            LVUtils.displayGuestWatchDialog(PlayerActivity.this, null);
            return;
        }

        if (commentView.getVisibility() == View.VISIBLE) {
            commentView.setVisibility(View.GONE);
            msgView.setVisibility(View.GONE);
            chat_icon.setImageResource(R.drawable.ic_chat_white_two_off_wshadow);
        } else {
            commentView.setVisibility(View.VISIBLE);
            msgView.setVisibility(View.VISIBLE);
            chat_icon.setImageResource(R.drawable.ic_chat_white_two_on_wshadow);
        }
    }

    @OnClick(R.id.player_chat_layout)
    protected void onChatBoxClick() {
        if (mSession.getUser().getUserRole() == User.UserRole.GUEST) {
            LVUtils.displayGuestWatchDialog(PlayerActivity.this, null);
        }
    }

    @OnClick(R.id.chat_send_msg_button)
    protected void onSendButtonClick() {
        if (mSession.getUser().getUserRole() != User.UserRole.GUEST) {
            String message = commentEditTextView.getText().toString().trim();
            if (!TextUtils.isNullOrEmpty(message)) {
                //Clear stuff
                sendButton.setEnabled(false);
                commentEditTextView.setText("");

                //Send request to server
                shouldStopUpdatingView = true;
                APICalls.requestSendStreamChatMessage(PlayerActivity.this, mContext, mStream.getStreamId(),
                        mStream.getArtistId(), message, mSession, mRequestQueue, new DataAvailabilityHandler() {
                    @Override
                    public void RunCompleted(boolean isAvailable) {
                        sendButton.setEnabled(true);
                        requestChatMsgs(true, true);
                    }
                });
            }
        }
    }

    @OnClick(R.id.header_left_icon)
    protected void onCloseIconClick() {
        shouldStopUpdatingView = true;
        finish();
    }

    @OnClick(R.id.header_right_icon)
    protected void onMuteVideoClick() {
        if (mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC) == 0) {
            if (currentVolume == 0)
                currentVolume = 1;
            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, currentVolume, 0);
            toggleMuteButton(false);
        } else {
            currentVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0);
            toggleMuteButton(true);
        }
    }

    private void toggleMuteButton(boolean shouldMute) {
        muteButton.setImageResource((shouldMute) ? R.drawable.ic_soundoff_wshadows : R.drawable.ic_soundon_wshadows);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_VOLUME_UP)) {
            toggleMuteButton(false);
        } else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
            if (mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC) < 5) {
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        if (mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC) == 0) {
                            toggleMuteButton(true);
                        }
                    }
                }, 200);
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    private void showControls() {
        header.setVisibility(View.VISIBLE);
        star_chat_view.setVisibility(View.VISIBLE);
        playPauseButton.setVisibility(View.VISIBLE);
    }

    private void fadeInAndShowLogo() {
        transparentView.setVisibility(View.VISIBLE);
        transparentView.startAnimation(logoFadeInAnimation);
        root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AndroidUtils.hideKeyboard(mContext, root);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mSession == null || !UserValidator.isValid(mSession.getUser())) {
            AndroidUtils.restartApp(mContext);
            return;
        }

        isActive = true;
        if (player == null) {
            preparePlayer();
            if (player != null && player.getSurface() == null && surface != null)
                player.setSurface(surface);
        } else {
            player.setBackgrounded(false);
        }

        isActive = true;
        shouldStopUpdatingView = false;
        loadComments();
        loadStars();
    }

    private void loadComments() {
        if (shouldStopUpdatingView)
            return;

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!shouldStopUpdatingView) {
                    if (!isDownloadingChat)
                        requestChatMsgs(true, false);
                    loadComments();
                }
            }
        }, UPDATE_CHAT_INTERVAL);
    }

    private void loadStars() {
        if (shouldStopUpdatingView)
            return;

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!shouldStopUpdatingView) {
                    setStarNumber();
                    loadStars();
                }
            }
        }, UPDATE_CHAT_INTERVAL);
    }

    @Override
    public void onPause() {
        super.onPause();
        releasePlayer();
        isActive = false;
        shouldStopUpdatingView = true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        audioCapabilitiesReceiver.unregister();
        releasePlayer();
        shouldStopUpdatingView = true;
        PicassoTools.clearCache(Picasso.with(mContext));
    }

    @Override
    public void onAudioCapabilitiesChanged(AudioCapabilities audioCapabilities) {
        if (player == null)
            return;
        boolean backgrounded = player.getBackgrounded();
        releasePlayer();
        preparePlayer();
        player.setBackgrounded(backgrounded);
    }

    private void preparePlayer() {
        if (player == null) {
            if (isHLS) {
                player = new DemoPlayer(new HlsRendererBuilder(this, Util.getUserAgent(this, "LiVideo"), contentUri.toString()));
            } else {
                player = new DemoPlayer(new ExtractorRendererBuilder(this, Util.getUserAgent(this, "LiVideo"), contentUri));
            }
            player.addListener(this);
            player.setMetadataListener(this);
            player.seekTo(playerPosition);
            playerNeedsPrepare = true;
            eventLogger = new EventLogger();
            eventLogger.startSession();
            player.addListener(eventLogger);
            player.setInfoListener(eventLogger);
            player.setInternalErrorListener(eventLogger);
        }

        if (playerNeedsPrepare) {
            player.prepare();
            playerNeedsPrepare = false;
        }
    }

    private void releasePlayer() {
        if (player != null) {
            playerPosition = player.getCurrentPosition();
            player.release();
            player = null;
            eventLogger.endSession();
            eventLogger = null;
        }
    }

    @Override
    public void onStateChanged(boolean playWhenReady, int playbackState) {
        switch (playbackState) {
            case ExoPlayer.STATE_BUFFERING:
                Log.e(TAG, "Buffering %" + player.getBufferedPercentage() + "");
                mProgressBar.setVisibility(View.VISIBLE);
                break;
            case ExoPlayer.STATE_IDLE:
                Log.e(TAG, "idle");
                playPauseButton.setVisibility(View.GONE);
                mProgressBar.setVisibility(View.GONE);
                finishSuccessfulVideo();
                if (!isHLS) {
                    AndroidUtils.showShortToast(getString(R.string.could_not_play), this);
                }
                break;
            case ExoPlayer.STATE_PREPARING:
                Log.e(TAG, "preparing");
                mProgressBar.setVisibility(View.VISIBLE);
                break;
            case ExoPlayer.STATE_READY:
                handleVideoStart(playbackState);
                break;
            case ExoPlayer.STATE_ENDED:
                Log.e(TAG, "playbackstate is ended"); //NON-NLS
                if (!isHLS)
                    finishSuccessfulVideo();
                break;
            default:
                playPauseButton.setVisibility(View.GONE);
                mProgressBar.setVisibility(View.GONE);
                Log.e(TAG, "unknown"); //NON-NLS
                break;
        }
    }

    private void handleVideoStart(final int playbackState) {
        Log.e(TAG, "Ready Ready");
        mProgressBar.setVisibility(View.GONE);
        if (player == null)
            preparePlayer();
        player.setPlayWhenReady(true);
        playPauseButton.setImageResource(R.drawable.ic_pause_circled);

        // Toggle controls whenever USER clicks on video frame
        root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleControlsVisibility();
            }
        });

        // Fade out Artist Detail View after X seconds
        if (isArtistDetailVisible)
            fadeOutArtistDetailHandler.postDelayed(fadeOutArtistDetailRunnable, ARTIST_DETAIL_DELAY_ANIMATION_LENGTH);
    }

    private void finishSuccessfulVideo() {
        if (BuildConfig.DEBUG) Log.e("VIDEO STATUS", "FINISHING VIDEO");
        if (mProgressBar != null) mProgressBar.setVisibility(View.GONE);
        if (playPauseButton != null) playPauseButton.setVisibility(View.GONE);
        if (isArtistDetailVisible) artist_details_view.startAnimation(artistDetailFadeOutAnimation);
        fadeInAndShowLogo();
        callOnce = false;   // Do not call end streams api call
        if (mSession != null && UserValidator.isValid(mSession.getUser())
                && StreamValidator.isValid(mStream)
                && !mSession.getUser().getUserID().equalsIgnoreCase(mStream.getArtistId())
                && callOnce) {
            callOnce = false;
            APICalls.requestEndStream(this, mContext, mStream, mSession, mRequestQueue);
        }
    }

    @Override
    public void onError(Exception e) {
        if (e instanceof UnsupportedDrmException) {
            // Special case DRM failures.
            UnsupportedDrmException unsupportedDrmException = (UnsupportedDrmException) e;
            int stringId = Util.SDK_INT < 18 ? R.string.drm_error_not_supported
                    : unsupportedDrmException.reason == UnsupportedDrmException.REASON_UNSUPPORTED_SCHEME
                    ? R.string.drm_error_unsupported_scheme : R.string.drm_error_unknown;
            Toast.makeText(mContext, stringId, Toast.LENGTH_LONG).show();
        }
        playerNeedsPrepare = true;
        showControls();
    }

    @Override
    public void onVideoSizeChanged(int width, int height, int unappliedRotationDegrees, float pixelWidthAspectRatio) {
        videoWidth = width;
        videoHeight = height;

        // Set the view
        updateTextureViewSize(videoWidth, videoHeight);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onId3Metadata(Map<String, Object> metadata) {
        for (Map.Entry<String, Object> entry : metadata.entrySet()) {
            if (TxxxMetadata.TYPE.equals(entry.getKey())) {
                TxxxMetadata txxxMetadata = (TxxxMetadata) entry.getValue();
                Log.i(TAG, String.format("ID3 TimedMetadata %s: description=%s, value=%s", //NON-NLS
                        TxxxMetadata.TYPE, txxxMetadata.description, txxxMetadata.value));
            } else if (PrivMetadata.TYPE.equals(entry.getKey())) {
                PrivMetadata privMetadata = (PrivMetadata) entry.getValue();
                Log.i(TAG, String.format("ID3 TimedMetadata %s: owner=%s", //NON-NLS
                        PrivMetadata.TYPE, privMetadata.owner));
            } else if (GeobMetadata.TYPE.equals(entry.getKey())) {
                GeobMetadata geobMetadata = (GeobMetadata) entry.getValue();
                Log.i(TAG, String.format("ID3 TimedMetadata %s: mimeType=%s, filename=%s, description=%s", //NON-NLS
                        GeobMetadata.TYPE, geobMetadata.mimeType, geobMetadata.filename,
                        geobMetadata.description));
            } else {
                Log.i(TAG, String.format("ID3 TimedMetadata %s", entry.getKey())); //NON-NLS
            }
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (player != null) {
            player.setSurface(holder.getSurface());
        }
    }


    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        // Do nothing.
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        if (player != null) {
            player.blockingClearSurface();
        }
    }
    Surface surface;
    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int width, int height) {
        surface = new Surface(surfaceTexture);
        if (player == null)
            preparePlayer();
        player.setSurface(surface);
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        return true;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {

    }
}
