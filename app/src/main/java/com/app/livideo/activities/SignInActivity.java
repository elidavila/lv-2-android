package com.app.livideo.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.android.volley.RequestQueue;
import com.app.livideo.R;
import com.app.livideo.adapters.CountryAdapter;
import com.app.livideo.api.APICalls;
import com.app.livideo.async_tasks.AsyncPhoneInitTask;
import com.app.livideo.dialogs.VerifyNewDeviceDialog;
import com.app.livideo.interfaces.LoginHandler;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.Country;
import com.app.livideo.models.User;
import com.app.livideo.utils.AndroidUtils;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.Constants;
import com.app.livideo.utils.LVUtils;
import com.app.livideo.utils.TextUtils;
import com.bumptech.glide.Glide;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class SignInActivity extends AppCompatActivity {

    @Inject RequestQueue mRequestQueue;
    @Inject LVSession mSession;
    @Inject Context mContext;

    @Bind(R.id.spinner) Spinner mSpinner;
    @Bind(R.id.phone) EditText phoneNumberEditView;
    @Bind(R.id.login_password) EditText password;
    @Bind(R.id.login_touch_blocker) View touchBlocker;
    @Bind(R.id.login_progress_bar) ImageView mProgressBar;

    protected String mLastEnteredPhone;
    protected CountryAdapter mAdapter;
    boolean isLoggingIn = false;
    protected PhoneNumberUtil mPhoneNumberUtil = PhoneNumberUtil.getInstance();
    String phoneNumberText, pw, userID, deviceID;
    Country country;
    boolean isActive = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);
        LVInject.inject(this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mSpinner.setOnItemSelectedListener(mOnItemSelectedListener);
        mAdapter = new CountryAdapter(mContext);
        mSpinner.setAdapter(mAdapter);
        new AsyncPhoneInitTask(mContext, mAdapter, mSpinner, phoneNumberEditView).execute();
        touchBlocker.setOnTouchListener(AppConstants.touchListner);
        Glide.with(mContext).load(R.drawable.loading_anim).asGif().into(mProgressBar);
        isActive = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActive = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isActive = true;
    }

    @OnClick(R.id.sign_in_back_button)
    protected void onBackClick() {
        finish();
    }

    @OnClick(R.id.login_button)
    protected void onViewerLoginClick() {
        isLoggingIn = true;
        AndroidUtils.hideKeyboard(mContext, phoneNumberEditView);
        phoneNumberEditView.setError(null);
        touchBlocker.setVisibility(View.VISIBLE);
        phoneNumberText = validate();   //Validate Phone Number
        if (phoneNumberText != null) {
            APICalls.SendAWSIdentity(mContext, mRequestQueue, mSession);
            phoneNumberText = phoneNumberText.substring(1); // take out the "+"
            LoginViewer();
        } else {
            touchBlocker.setVisibility(View.GONE);
            isLoggingIn = false;
            phoneNumberEditView.requestFocus();
            phoneNumberEditView.setError(getString(R.string.invalled_phone));
        }
    }

    @OnTextChanged(R.id.phone)
    void onTextChanged(CharSequence text) {
        mLastEnteredPhone = text.toString();
    }

    @OnClick(R.id.forgot_pw)
    protected void onForgotPasswordClick() {
        if (isActive) {
            Intent intent = new Intent(this, EnterPhoneNumberActivity.class);
            intent.putExtra(AppConstants.ACTIVITY_KEY_INTENT, AppConstants.FROM_FORGOT_PW_ACTIVITY);
            startActivity(intent);
        }
    }

    protected String validate() {
        String region = null, phoneNumber = null;
        if (mLastEnteredPhone != null) {
            try {
                if (!mLastEnteredPhone.contains("+") && country != null) {
                    mLastEnteredPhone = "+" + country.getCountryCode() + mLastEnteredPhone;
                }
                Phonenumber.PhoneNumber p = mPhoneNumberUtil.parse(mLastEnteredPhone, null);
                phoneNumber = "+" + p.getCountryCode() + p.getNationalNumber();
                region = mPhoneNumberUtil.getRegionCodeForNumber(p);
            } catch (NumberParseException ignore) {
            }
        }

        if (region != null)
            return phoneNumber;
        else
            return null;
    }

    private void LoginViewer() {
        pw = TextUtils.getSHA1String(password.getText().toString());
        deviceID = Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.ANDROID_ID);
        userID = ".";
        APICalls.requestPhoneNumberLogin(this, mContext, phoneNumberText, pw, userID, deviceID, mSession, mRequestQueue, new LoginHandler() {
            @Override
            public void RunCompleted(int UserCode, User user) {
                switch (UserCode) {
                    case Constants.USERCODE_SUCCESS:
                        LVUtils.StartLiVideo(user, phoneNumberText, true, touchBlocker, SignInActivity.this, mContext, mSession, mRequestQueue, null);
                        break;
                    case Constants.USERCODE_INVALID_RESPONSE:
                        isLoggingIn = false;
                        touchBlocker.setVisibility(View.GONE);
                        AndroidUtils.showLongToast(getString(R.string.invaled_server), SignInActivity.this);
                        break;
                    case Constants.USERCODE_NOT_EXISTS:
                        isLoggingIn = false;
                        touchBlocker.setVisibility(View.GONE);
                        AndroidUtils.showLongToast(getString(R.string.user_does_not_exist), SignInActivity.this);
                        break;
                    case Constants.USERCODE_OTHER_ERROR:
                        isLoggingIn = false;
                        touchBlocker.setVisibility(View.GONE);
                        AndroidUtils.showLongToast(getString(R.string.retry_later), SignInActivity.this);
                        break;
                    case Constants.USERCODE_OTHER_DEVICE:
                        isLoggingIn = false;
                        touchBlocker.setVisibility(View.GONE);
                        Bundle bundle = new Bundle();
                        bundle.putString(AppConstants.DEVICE_ID_KEY_INTENT, deviceID);
                        bundle.putString(AppConstants.PASSWORD_KEY_INTENT, pw);
                        bundle.putString(AppConstants.PHONE_NUMBER_KEY_INTENT, phoneNumberText);
                        VerifyNewDeviceDialog dialog = new VerifyNewDeviceDialog(SignInActivity.this, bundle);
                        dialog.show();
                        break;
                    default:
                        isLoggingIn = false;
                        touchBlocker.setVisibility(View.GONE);
                        AndroidUtils.showShortToast(getString(R.string.wrong_un_or_pw), SignInActivity.this);  //wrong password response
                }
            }
        });
    }

    protected AdapterView.OnItemSelectedListener mOnItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            country = (Country) mSpinner.getItemAtPosition(position);
            if (mLastEnteredPhone != null && mLastEnteredPhone.startsWith(country.getCountryCodeStr()))
                return;
            phoneNumberEditView.getText().clear();
            phoneNumberEditView.setSelection(phoneNumberEditView.length());
            mLastEnteredPhone = null;
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };
}
