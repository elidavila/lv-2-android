package com.app.livideo.activities;

import android.app.ListActivity;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.app.livideo.R;
import com.app.livideo.api.APICalls;
import com.app.livideo.dialogs.ConfirmMessageDialog;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.Constants;
import com.app.livideo.utils.TextUtils;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ContactsSelectionActivity extends ListActivity implements View.OnClickListener {

    public String[] Contacts = {};
    public int[] to = {};
    public ListView myListView;
    @Bind(R.id.header_left_icon) ImageView backButton;
    @Bind(R.id.header_right_icon) ImageView confirmButton;
    @Bind(R.id.no_contacts) TextView noContactsView;
    @Inject RequestQueue mRequestQueue;
    @Inject LVSession mSession;
    @Inject Context mContext;
    private String webUrl;
    boolean isActive = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts_selection);
        initialize();

        Cursor mCursor = null;
        if (getIntent().getStringExtra(AppConstants.CONTACT_TYPE).equalsIgnoreCase(AppConstants.CONTACT_EMAIL)) {
            mCursor = getEmailContacts();
        } else if (getIntent().getStringExtra(AppConstants.CONTACT_TYPE).equalsIgnoreCase(AppConstants.CONTACT_PHONE)) {
            mCursor = getPhoneContacts();
        } else {
            setResult(RESULT_CANCELED);
            finish();
        }

        ListAdapter adapter = new SimpleCursorAdapter(this, R.layout.contacts_item, mCursor,
                Contacts = new String[]{ContactsContract.Contacts.DISPLAY_NAME},
                to = new int[]{android.R.id.text1});
        setListAdapter(adapter);
        if (adapter.isEmpty()) {
            noContactsView.setVisibility(View.VISIBLE);
        }
        myListView = getListView();
        myListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
    }

    private void initialize() {
        LVInject.inject(this);
        ButterKnife.bind(this);
        isActive = true;
        backButton.setImageResource(R.drawable.ic_back_wshadow);
        confirmButton.setImageResource(R.drawable.ic_checkwhite_wshadows);
        confirmButton.setOnClickListener(this);
        webUrl = getIntent().getStringExtra(AppConstants.SHOW_CONTACTS_DIALOGUE);
    }

    @OnClick(R.id.header_left_card_view)
    protected void leftHeaderIconPressed() {
        onBackPressed();
    }

    private Cursor getPhoneContacts() {
        Uri uri = ContactsContract.Contacts.CONTENT_URI;
        String[] projection = new String[]{ContactsContract.Contacts._ID, ContactsContract.Contacts.DISPLAY_NAME};
        String selection = ContactsContract.Contacts.HAS_PHONE_NUMBER + " = '" + ("1") + "'";
        String sortOrder = ContactsContract.Contacts.DISPLAY_NAME + " COLLATE LOCALIZED ASC";
        return getContentResolver().query(uri, projection, selection, null, sortOrder);
    }

    private Cursor getEmailContacts() {
        String[] projection = new String[]{ContactsContract.Contacts._ID,
                ContactsContract.Contacts.DISPLAY_NAME, ContactsContract.CommonDataKinds.Email.DATA};
        return getContentResolver().query(ContactsContract.Data.CONTENT_URI,
                projection,
                ContactsContract.Contacts.Data.MIMETYPE + "=?",
                new String[]{ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE},
                ContactsContract.Data.DISPLAY_NAME + " COLLATE LOCALIZED ASC");
    }

    @Override
    public void onClick(View v) {
        SparseBooleanArray checkedPositions = getListView().getCheckedItemPositions();
        long[] id = getListView().getCheckedItemIds();
        String[] returnValue = new String[id.length];
        if (getIntent().getStringExtra(AppConstants.CONTACT_TYPE).equalsIgnoreCase(AppConstants.CONTACT_EMAIL)) {
            Cursor cursor = ((SimpleCursorAdapter) getListAdapter()).getCursor();
            StringBuilder sb = new StringBuilder();
            StringBuilder sb2 = new StringBuilder();
            for (int i = 0; i < checkedPositions.size(); i++) {
                if (checkedPositions.valueAt(i)) {
                    if (cursor.moveToPosition(checkedPositions.keyAt(i))) {
                        sb.append(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA))).append("; ");
                        sb2.append(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA))).append(",");
                    }
                }
            }

            if (isActive) {
                if (TextUtils.isNullOrEmpty(webUrl))
                    APICalls.requstLogInvites(this, mContext, mSession, mRequestQueue, Constants.INVITE_TYPE_EMAIL, sb2.toString());
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", sb.toString(), null));
                emailIntent.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.invite_message));
                startActivity(Intent.createChooser(emailIntent, ""));
                finish();
            }
        } else if (getIntent().getStringExtra(AppConstants.CONTACT_TYPE).equalsIgnoreCase(AppConstants.CONTACT_PHONE)) {
            for (int i = 0; i < id.length; i++) {
                returnValue[i] = getPhoneNumber(id[i]);
            }

            StringBuilder stringBuilder = new StringBuilder();
            for (String s : returnValue) {
                stringBuilder.append(s).append(",");
            }

            // Log Invites for friends only
            if (TextUtils.isNullOrEmpty(webUrl))
                APICalls.requstLogInvites(this, mContext, mSession, mRequestQueue, Constants.INVITE_TYPE_TEXT, stringBuilder.toString());

            ConfirmMessageDialog dialog;
            if (TextUtils.isNullOrEmpty(webUrl)) {
                dialog = new ConfirmMessageDialog(this, returnValue);
            } else {
                dialog = new ConfirmMessageDialog(this, returnValue, webUrl);
            }
            dialog.show();
            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    finish();
                }
            });
        } else {
            finish();
        }
    }

    private String getPhoneNumber(long id) {
        String phone = null;
        Cursor phonesCursor;
        phonesCursor = queryPhoneNumbers(id);
        if (phonesCursor == null || phonesCursor.getCount() == 0) {
            return null;
        } else if (phonesCursor.getCount() == 1) {
            phone = phonesCursor.getString(phonesCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
        } else {
            phonesCursor.moveToPosition(-1);
            while (phonesCursor.moveToNext()) {
                phone = phonesCursor.getString(phonesCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                break;
            }
        }
        return phone;
    }

    private Cursor queryPhoneNumbers(long contactId) {
        ContentResolver cr = getContentResolver();
        Uri baseUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, contactId);
        Uri dataUri = Uri.withAppendedPath(baseUri, ContactsContract.Contacts.Data.CONTENT_DIRECTORY);

        Cursor c = cr.query(dataUri, new String[]{ContactsContract.CommonDataKinds.Phone._ID, ContactsContract.CommonDataKinds.Phone.NUMBER,
                        ContactsContract.CommonDataKinds.Phone.IS_SUPER_PRIMARY, ContactsContract.RawContacts.ACCOUNT_TYPE, ContactsContract.CommonDataKinds.Phone.TYPE,
                        ContactsContract.CommonDataKinds.Phone.LABEL}, ContactsContract.Contacts.Data.MIMETYPE + "=?",
                new String[]{ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE}, null);
        if (c != null && c.moveToFirst()) {
            return c;
        }
        return null;
    }
}
