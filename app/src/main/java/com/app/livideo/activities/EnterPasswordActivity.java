package com.app.livideo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.livideo.R;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.TextUtils;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class EnterPasswordActivity extends AppCompatActivity {

    @Bind(R.id.header_left_icon) ImageView header_left_icon;
    @Bind(R.id.header_title) TextView header_title;

    @Bind(R.id.enter_value_title) TextView enter_value_title;
    @Bind(R.id.enter_value_icon) ImageView enter_value_icon;
    @Bind(R.id.enter_value_edittext) EditText enter_value_edit_text;
    @Bind(R.id.enter_value_error_text) TextView error_text_view;
    @Bind(R.id.enter_value_action_text) TextView action_text_view;
    @Bind(R.id.enter_value_footer_text) TextView enter_value_footer_text;

    private boolean isPasswordValid, isViewer, isActive = true;
    private String mPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enter_value_layout);
        ButterKnife.bind(this);
        initialize();
    }

    private void initialize() {
        // Header
        header_title.setText(getString(R.string.welcome_display_name).replace("%%DISPLAYNAME%%", getIntent().getStringExtra(AppConstants.DISPLAY_NAME_KEY_INTENT)));
        header_left_icon.setImageResource(R.drawable.ic_back_wshadow);

        // Body
        isActive = true;
        enter_value_title.setText(R.string.choose_a_password);
        enter_value_icon.setImageResource(R.drawable.ic_lock_off);
        action_text_view.setText(getString(R.string.next_string));
        enter_value_footer_text.setText(getString(R.string.email_registration_password));
        enter_value_edit_text.setHint(R.string.easy_to_remember);

        // Variables
        isViewer = getIntent().getBooleanExtra(AppConstants.IS_USER_INTENT, true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        isActive = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActive = false;
    }

    @OnClick(R.id.enter_value_action_button)
    protected void onNextClick() {
        if (isActive && isPasswordValid) {
            Intent intent = new Intent(EnterPasswordActivity.this, EnterBirthdayActivity.class);
            if (!isViewer) {
                intent.putExtra(AppConstants.SOCIAL_ID_KEY_INTENT, getIntent().getStringExtra(AppConstants.SOCIAL_ID_KEY_INTENT));
                intent.putExtra(AppConstants.SOCIAL_TYPE_KEY_INTENT, getIntent().getStringExtra(AppConstants.SOCIAL_TYPE_KEY_INTENT));
            }

            // previous intent extras
            intent.putExtra(AppConstants.IS_USER_INTENT, isViewer);
            intent.putExtra(AppConstants.USER_ID_KEY_INTENT, getIntent().getStringExtra(AppConstants.USER_ID_KEY_INTENT));
            intent.putExtra(AppConstants.DEVICE_ID_KEY_INTENT, getIntent().getStringExtra(AppConstants.DEVICE_ID_KEY_INTENT));
            intent.putExtra(AppConstants.PHONE_NUMBER_KEY_INTENT, getIntent().getStringExtra(AppConstants.PHONE_NUMBER_KEY_INTENT));
            intent.putExtra(AppConstants.ACTIVITY_KEY_INTENT, getIntent().getStringExtra(AppConstants.ACTIVITY_KEY_INTENT));
            intent.putExtra(AppConstants.EMAIL_KEY_INTENT, getIntent().getStringExtra(AppConstants.EMAIL_KEY_INTENT));
            intent.putExtra(AppConstants.DISPLAY_NAME_KEY_INTENT, getIntent().getStringExtra(AppConstants.DISPLAY_NAME_KEY_INTENT));

            // new intent extras
            intent.putExtra(AppConstants.PASSWORD_KEY_INTENT, TextUtils.getSHA1String(mPassword));
            startActivity(intent);
        }
    }

    @OnTextChanged(value = R.id.enter_value_edittext, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    protected void onPasswordTextChanged() {
        mPassword = enter_value_edit_text.getText().toString();
        isPasswordValid = false;

        // Check if pw is empty
        if (TextUtils.isNullOrEmpty(mPassword)) {
            displayUIError(true, null);
            return;
        }

        // Check if pw contains spaces
        if (mPassword.contains(" ")){
            displayUIError(true, getString(R.string.password_no_spaces));
            return;
        }

        // Check if pw meets requirements
        if (mPassword.length() >= 7 && !mPassword.contains(" ")) {
            displayUIError(false, null);    // Password is valid here
        } else {
            displayUIError(true, null);
        }
    }

    private void displayUIError(boolean isError, String errorMsg) {
        isPasswordValid = !isError;
        if (isError) {
            enter_value_icon.setImageResource(R.drawable.ic_lock_off);
            if (errorMsg != null) {
                error_text_view.setText(errorMsg);
                error_text_view.setVisibility(View.VISIBLE);
            } else {
                error_text_view.setVisibility(View.INVISIBLE);
            }
        } else {
            enter_value_icon.setImageResource(R.drawable.ic_lock_on);
            error_text_view.setVisibility(View.INVISIBLE);
        }
    }

    @OnClick(R.id.header_left_card_view)
    protected void onBackClick() {
        finish();
    }
}
