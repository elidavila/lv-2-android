package com.app.livideo.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.android.volley.RequestQueue;
import com.app.livideo.BuildConfig;
import com.app.livideo.R;
import com.app.livideo.api.APICalls;
import com.app.livideo.dialogs.CustomAlertDialog;
import com.app.livideo.dialogs.SubscribeSuccessDialog;
import com.app.livideo.interfaces.DataAvailabilityHandler;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.User;
import com.app.livideo.models.validators.ArtistValidator;
import com.app.livideo.models.validators.UserValidator;
import com.app.livideo.utils.Preferences;
import com.app.livideo.utils.inappbilling.IabConstants;
import com.app.livideo.utils.inappbilling.IabHelper;
import com.app.livideo.utils.inappbilling.IabResult;
import com.app.livideo.utils.inappbilling.Purchase;
import com.bumptech.glide.Glide;
import com.crashlytics.android.Crashlytics;

import javax.inject.Inject;

public class PurchaseActivity extends AppCompatActivity implements IabConstants {

    @Inject Context mContext;
    @Inject LVSession mSession;
    @Inject RequestQueue mRequestQueue;
    IabHelper billingHelper;
    static final String ITEM_SKU = "livideo.membership";
    static final String LOGTAG = PurchaseActivity.class.getSimpleName();
    ImageView mProgressBar;

    String tokenSaved = "tokenSaved";
    String tokenValue = "tokenValue";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase);
        LVInject.inject(this);
        mProgressBar = (ImageView) findViewById(R.id.purchase_progress_bar);
        Glide.with(mContext).load(R.drawable.loading_anim).asGif().into(mProgressBar);
        if (mSession != null) {
            if (ArtistValidator.isValid(mSession.getCurrentArtist())) {
                if (BuildConfig.DEBUG
                        || mSession.getCurrentArtist().getArtistId().equalsIgnoreCase(SplashActivity.LIVIDEO_USER_ID)
                        || mSession.getUser().getUserRole() == User.UserRole.BROADCASTER) {
                    requestFreeSubscription(mSession.getCurrentArtist().getArtistId());
                } else {
                    System.gc();
                    billingHelper = new IabHelper(this, PUBLIC_KEY);
                    billingHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
                        @Override
                        public void onIabSetupFinished(IabResult result) {
                            if (result.isSuccess()) {
                                billingHelper.launchSubscriptionPurchaseFlow(
                                        PurchaseActivity.this,
                                        ITEM_SKU,
                                        101,
                                        mPurchaseFinishedListener);
                            }
                        }
                    });
                }
            } else {
                handleError(R.string.app_error, "");
            }
        } else {
            handleError(R.string.app_error, "");
        }
    }

    private void requestFreeSubscription(String artistId) {
        mProgressBar.setVisibility(View.VISIBLE);
        APICalls.requestFreeArtistSubscription(this, mContext, mSession, artistId, mRequestQueue, new DataAvailabilityHandler() {
            @Override
            public void RunCompleted(boolean isAvailable) {
                mProgressBar.setVisibility(View.GONE);
                if (isAvailable) {
                    addArtistToLocalSession();
                    SubscribeSuccessDialog dialog = new SubscribeSuccessDialog(PurchaseActivity.this);
                    dialog.show();
                    dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            finish();
                            if (mSession != null && mSession.getCurrentRunnable() != null)
                                mSession.getCurrentRunnable().run();
                        }
                    });
                } else {
                    mSession.setCurrentRunnable(null);
                    CustomAlertDialog dialog = new CustomAlertDialog(PurchaseActivity.this, getString(R.string.livideo_subcription_error), false);
                    dialog.show();
                    dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            onBackPressed();
                        }
                    });
                }
            }
        });
    }

    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        @Override
        public void onIabPurchaseFinished(IabResult result, Purchase info) {
            if (result.isSuccess()) {
                runWatchAPICall(info.getToken(), mSession.getCurrentArtist().getArtistId());
            } else {
                handleError(R.string.purchase_failed, result.getMessage());
            }
        }
    };

    private void handleError(int stringID, String additional) {
        if (mSession != null)
            mSession.setCurrentRunnable(null);
        CustomAlertDialog dialog = new CustomAlertDialog(PurchaseActivity.this, PurchaseActivity.this.getString(stringID) +" " +additional, true);
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                onBackPressed();
            }
        });
        dialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (billingHelper != null) {
            billingHelper.dispose();
        }
        billingHelper = null;
        mSession.setCurrentRunnable(null);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!billingHelper.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        } else {
            Log.d(LOGTAG, "onActivityResult handled by IABUtil.");
        }
    }

    @Override
    public void onBackPressed() {
        if (mProgressBar.getVisibility() != View.VISIBLE) {
            super.onBackPressed();
        }
    }

    private void runWatchAPICall(final String token, final String artistID) {
        mProgressBar.setVisibility(View.VISIBLE);
        Preferences.setPreference(tokenSaved, true, this);
        Preferences.setPreference(tokenValue, token, this);

        APICalls.requestAddHeroId(this, mContext, mSession, artistID, mRequestQueue, new Runnable() {
            @Override
            public void run() {
                APICalls.requestAddArtistToSubscriptions(PurchaseActivity.this, mContext, mSession, artistID, token, mRequestQueue, new DataAvailabilityHandler() {
                    @Override
                    public void RunCompleted(boolean isAvailable) {
                        mProgressBar.setVisibility(View.GONE);
                        if (isAvailable) {
                            addArtistToLocalSession();
                            SubscribeSuccessDialog dialog = new SubscribeSuccessDialog(PurchaseActivity.this);
                            dialog.show();
                            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    finish();
                                    if (mSession != null && mSession.getCurrentRunnable() != null)
                                        mSession.getCurrentRunnable().run();
                                }
                            });
                        } else {
                            if (UserValidator.isValid(mSession.getUser()))
                                Crashlytics.logException(new Throwable("Subscription Error! UserID : " + mSession.getUser().getUserID() + ", Token : " + token));
                            mSession.setCurrentRunnable(null);
                            CustomAlertDialog dialog = new CustomAlertDialog(PurchaseActivity.this, getString(R.string.livideo_subcription_error), true);
                            dialog.show();
                            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    onBackPressed();
                                }
                            });
                        }
                    }
                });
            }
        });
    }

    private void addArtistToLocalSession() {
        if (ArtistValidator.isValid(mSession.getCurrentArtist()))
            mSession.getCurrentArtist().setIsHero(1);

        if (UserValidator.isValid(mSession.getUser()))
            mSession.getUser().setHasMembership(1);
    }

}
