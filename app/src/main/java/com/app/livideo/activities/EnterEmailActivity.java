package com.app.livideo.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.app.livideo.R;
import com.app.livideo.api.APICalls;
import com.app.livideo.interfaces.DataAvailabilityHandler;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.User;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.Constants;
import com.app.livideo.utils.TextUtils;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class EnterEmailActivity extends Activity {

    @Inject RequestQueue mRequestQueue;
    @Inject Context mContext;
    @Inject LVSession mSession;

    @Bind(R.id.header_left_icon) ImageView header_left_icon;
    @Bind(R.id.header_title) TextView header_title;

    @Bind(R.id.enter_value_title) TextView enter_value_title;
    @Bind(R.id.enter_value_icon) ImageView enter_value_icon;
    @Bind(R.id.enter_value_edittext) EditText enter_value_edit_text;
    @Bind(R.id.enter_value_action_text) TextView enter_value_action_text;
    @Bind(R.id.enter_value_error_text) TextView error_text_view;
    private boolean isEmailValid, isActive = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enter_value_layout);
        ButterKnife.bind(this);
        LVInject.inject(this);
        initialize();
    }

    private void initialize() {
        // Header
        header_title.setText(R.string.number_is_confirmed);
        header_left_icon.setImageResource(R.drawable.ic_back_wshadow);

        // Body
        isActive = true;
        enter_value_title.setText(R.string.what_is_your_email);
        enter_value_icon.setImageResource(R.drawable.ic_email_off);
        enter_value_action_text.setText(getString(R.string.next_string));
    }

    @Override
    protected void onResume() {
        super.onResume();
        isActive = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActive = false;
    }

    @OnClick(R.id.enter_value_action_button)
    protected void onNextClick() {
        if (isActive && isEmailValid) {
            Intent intent = new Intent(this, EnterDisplayNameActivity.class);
            // previous intent extras
            intent.putExtra(AppConstants.IS_USER_INTENT, getIntent().getBooleanExtra(AppConstants.IS_USER_INTENT, true));
            intent.putExtra(AppConstants.USER_ID_KEY_INTENT, getIntent().getStringExtra(AppConstants.USER_ID_KEY_INTENT));
            intent.putExtra(AppConstants.DEVICE_ID_KEY_INTENT, getIntent().getStringExtra(AppConstants.DEVICE_ID_KEY_INTENT));
            intent.putExtra(AppConstants.PHONE_NUMBER_KEY_INTENT, getIntent().getStringExtra(AppConstants.PHONE_NUMBER_KEY_INTENT));
            intent.putExtra(AppConstants.ACTIVITY_KEY_INTENT, getIntent().getStringExtra(AppConstants.ACTIVITY_KEY_INTENT));

            // new
            intent.putExtra(AppConstants.EMAIL_KEY_INTENT, enter_value_edit_text.getText().toString().trim());
            startActivity(intent);
        } else {
            Toast.makeText(mContext, getString(R.string.email_registration_email), Toast.LENGTH_LONG).show();
        }
    }

    @OnTextChanged(value = R.id.enter_value_edittext, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    protected void onEmailTextChanged() {
        final String email = enter_value_edit_text.getText().toString().trim();

        // Check if email if empty
        if (TextUtils.isNullOrEmpty(email)) {
            displayUIError(true, null);
            return;
        }

        // Check if email is in valid format
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            displayUIError(true, getString(R.string.enter_valid_email));
            return;
        }

        // Check if email is available for our server
        APICalls.requestDataAvailabilityCheck(mSession, this, mContext, Constants.ACTION_EMAIL_AV, User.Keys.PARAMS_SOCIAL_ID, email,
                getIntent().getStringExtra(AppConstants.DEVICE_ID_KEY_INTENT), mRequestQueue,
                new DataAvailabilityHandler() {
                    @Override
                    public void RunCompleted(boolean isAvailable) {
                        if (isAvailable) {
                            displayUIError(false, null);
                        } else {
                            displayUIError(true, getString(R.string.email_in_use));
                        }
                    }
                });
    }

    private void displayUIError(final boolean isError, final String errorMsg) {
        isEmailValid = !isError;
        if (isError) {
            enter_value_icon.setImageResource(R.drawable.ic_email_off);
            if (errorMsg != null) {
                error_text_view.setText(errorMsg);
                error_text_view.setVisibility(View.VISIBLE);
            } else {
                error_text_view.setVisibility(View.INVISIBLE);
            }
        } else {
            enter_value_icon.setImageResource(R.drawable.ic_email_on);
            error_text_view.setVisibility(View.INVISIBLE);
        }
    }


    @OnClick(R.id.header_left_card_view)
    protected void onBackClick() {
        finish();
    }
}
