package com.app.livideo.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.app.livideo.R;
import com.app.livideo.api.APICalls;
import com.app.livideo.dialogs.CustomAlertDialog;
import com.app.livideo.dialogs.ImageUploadFromDialog;
import com.app.livideo.interfaces.ArtistListHandler;
import com.app.livideo.interfaces.ArtistTypesHandler;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.Artist;
import com.app.livideo.notifications.GCMRegistrationIntentService;
import com.app.livideo.utils.AndroidUtils;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.FileUtils;
import com.app.livideo.utils.LVUtils;
import com.app.livideo.utils.LocationUtils;
import com.app.livideo.utils.Preferences;
import com.app.livideo.utils.SessionUtils;
import com.bumptech.glide.Glide;
import com.google.android.gms.maps.model.LatLng;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EnterProfilePictureActivity extends AppCompatActivity {

    @Inject RequestQueue mRequestQueue;
    @Inject LVSession mSession;
    @Inject Context mContext;

    @Bind(R.id.header_title) TextView header_title;
    @Bind(R.id.enter_profile_pic_root) View mRootLayout;
    @Bind(R.id.enter_profile_pic_upload) ImageView upload_image_view;
    @Bind(R.id.enter_profile_pic_touch_blocker) FrameLayout touchBlocker;
    @Bind(R.id.enter_profile_pic_progress_bar) ImageView mProgressBar;
    @Bind(R.id.enter_profile_pic_action_button) CardView nextCartView;

    private boolean hasUploadedImage;
    private boolean isViewer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_profile_picture);
        ButterKnife.bind(this);
        LVInject.inject(this);
        initialize();
        APICalls.SendAWSIdentity(mContext, mRequestQueue, mSession);
    }

    private void initialize() {
        // Header
        Glide.with(mContext).load(R.drawable.loading_anim).asGif().into(mProgressBar);
        isViewer = getIntent().getBooleanExtra(AppConstants.IS_USER_INTENT, true);
        if (!isViewer) {
            header_title.setText(getIntent().getStringExtra(AppConstants.ARTIST_CATEGORY_KEY_INTENT));
        }
        touchBlocker.setOnTouchListener(AppConstants.touchListner);
    }

    @OnClick(R.id.enter_profile_pic_upload)
    protected void onUploadClick() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            AndroidUtils.requestStoragePermission(EnterProfilePictureActivity.this, mRootLayout);
        } else {
            ImageUploadFromDialog dialog = new ImageUploadFromDialog(EnterProfilePictureActivity.this);
            dialog.show();
        }
    }

    @OnClick(R.id.enter_profile_pic_action_button)
    protected void onStartWatchingClick() {
        if (hasUploadedImage) {
            touchBlocker.setVisibility(View.VISIBLE);
            LVUtils.StartLiVideo(mSession.getUser(), null, false, touchBlocker, this, mContext, mSession, mRequestQueue, null);
        } else {
            CustomAlertDialog dialog = new CustomAlertDialog(EnterProfilePictureActivity.this, getString(R.string.upload_profile_pic_requirement), false);
            dialog.show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Intent intent = new Intent(EnterProfilePictureActivity.this, CropImageActivity.class);
        switch (resultCode) {
            case Activity.RESULT_OK:
                switch (requestCode) {
                    case AppConstants.SELECT_PICTURE:
                        intent.putExtra(AppConstants.IMAGE_PATH_KEY, FileUtils.getFilePath(data.getData(), mContext));
                        startActivityForResult(intent, AppConstants.CROPPED_PICTURE);
                        break;
                    case AppConstants.CROPPED_PICTURE:
                        hasUploadedImage = true;
                        nextCartView.setCardBackgroundColor(ContextCompat.getColor(mContext, (isViewer) ? R.color.livideo_dark_pink : R.color.livideo_teal_primary));
                        Preferences.setPreference(AppConstants.KEY_HAS_UPLOADED_PROFILE_PIC, true, mContext);
                        Glide.with(mContext).load(mSession.getUser().getPictures().get(mSession.getUser().getPictures().size() - 1).getUrl()).into(upload_image_view);
                        break;
                    case AppConstants.TAKE_PICTURE:
                        File receivedFile = (File) data.getSerializableExtra(AppConstants.TAKEN_PHOTE);
                        intent.putExtra(AppConstants.IMAGE_PATH_KEY, receivedFile.getAbsolutePath());
                        startActivityForResult(intent, AppConstants.CROPPED_PICTURE);
                        break;
                }
        }
    }
}
