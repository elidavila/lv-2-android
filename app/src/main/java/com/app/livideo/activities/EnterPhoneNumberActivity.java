package com.app.livideo.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.app.livideo.R;
import com.app.livideo.adapters.CountryAdapter;
import com.app.livideo.api.APICalls;
import com.app.livideo.async_tasks.AsyncPhoneInitTask;
import com.app.livideo.interfaces.DataAvailabilityHandler;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.Country;
import com.app.livideo.models.User;
import com.app.livideo.utils.AndroidUtils;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.Constants;
import com.app.livideo.utils.PhoneUtils;
import com.app.livideo.utils.TextUtils;
import com.bumptech.glide.Glide;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.OnTouch;

public class EnterPhoneNumberActivity extends AppCompatActivity {

    protected String mLastEnteredPhone;
    protected CountryAdapter mAdapter;
    protected Country country;

    @Inject RequestQueue mRequestQueue;
    @Inject LVSession mSession;
    @Inject Context mContext;

    @Bind(R.id.header_left_icon) ImageView header_left_icon;
    @Bind(R.id.enter_phone_icon) ImageView phone_icon;
    @Bind(R.id.enter_phone_x) ImageView clear_phone_view;
    @Bind(R.id.header_title) TextView header_title;
    @Bind(R.id.enter_phone_error_text) TextView error_text_view;
    @Bind(R.id.enter_phone_spinner) Spinner mSpinner;
    @Bind(R.id.enter_phone_edittext) EditText phoneNumberEditView;
    @Bind(R.id.enter_phone_progress_bar) ImageView phone_progress_bar;
    @Bind(R.id.enter_phone_root) View mRoot;

    private String deviceID;
    int fromActivity;
    private boolean isViewer, isActive = true, isPhoneNumberValid;

    protected AdapterView.OnItemSelectedListener mOnItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            country = (Country) mSpinner.getItemAtPosition(position);
            if (mLastEnteredPhone != null && mLastEnteredPhone.startsWith(country.getCountryCodeStr()))
                return;
            phoneNumberEditView.getText().clear();
            phoneNumberEditView.setSelection(phoneNumberEditView.length());
            mLastEnteredPhone = null;
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_phone_number);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        ButterKnife.bind(this);
        LVInject.inject(this);
        initialize();
    }

    private void initialize() {
        // Header
        header_left_icon.setImageResource(R.drawable.ic_back_wshadow);
        Glide.with(mContext).load(R.drawable.loading_anim).asGif().into(phone_progress_bar);

        // Country Picker
        mSpinner.setOnItemSelectedListener(mOnItemSelectedListener);
        mAdapter = new CountryAdapter(mContext);
        mSpinner.setAdapter(mAdapter);
        new AsyncPhoneInitTask(mContext, mAdapter, mSpinner, phoneNumberEditView).execute();

        // Variables
        isActive = true;
        deviceID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        fromActivity = getIntent().getIntExtra(AppConstants.ACTIVITY_KEY_INTENT, -1);
        if (fromActivity != AppConstants.FROM_FORGOT_PW_ACTIVITY) {
            isViewer = getIntent().getBooleanExtra(AppConstants.IS_USER_INTENT, true);
            header_title.setText((isViewer)
                    ? getString(R.string.entertain_me)
                    : getString(R.string.welcome_display_name).replace("%%DISPLAYNAME%%",
                    getIntent().getStringExtra(AppConstants.DISPLAY_NAME_KEY_INTENT)));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isActive = true;
    }

    // Pass in userID, deviceID, phoneNumber, fromActivity
    @OnClick(R.id.enter_phone_send_button)
    protected void onSendConfirmationCodeClick() {
        if (isPhoneNumberValid && isActive) {
            Intent intent = new Intent(this, VerifyCodeActivity.class);
            intent.putExtra(AppConstants.PHONE_NUMBER_KEY_INTENT, mLastEnteredPhone);
            intent.putExtra(AppConstants.USER_ID_KEY_INTENT, ".");
            intent.putExtra(AppConstants.DEVICE_ID_KEY_INTENT, deviceID);

            if (fromActivity == AppConstants.FROM_FORGOT_PW_ACTIVITY) {
                intent.putExtra(AppConstants.ACTIVITY_KEY_INTENT, AppConstants.FROM_FORGOT_PW_ACTIVITY);
                startActivity(intent);
            } else {
                // Add artist intent extras
                if (!isViewer) {
                    intent.putExtra(AppConstants.DISPLAY_NAME_KEY_INTENT, getIntent().getStringExtra(AppConstants.DISPLAY_NAME_KEY_INTENT));
                    intent.putExtra(AppConstants.SOCIAL_ID_KEY_INTENT, getIntent().getStringExtra(AppConstants.SOCIAL_ID_KEY_INTENT));
                    intent.putExtra(AppConstants.SOCIAL_TYPE_KEY_INTENT, getIntent().getStringExtra(AppConstants.SOCIAL_TYPE_KEY_INTENT));
                }

                // old intent extras
                intent.putExtra(AppConstants.IS_USER_INTENT, isViewer);

                // new intent extras
                intent.putExtra(AppConstants.ACTIVITY_KEY_INTENT, AppConstants.FROM_SIGNUP);
                startActivity(intent);
            }
        }
    }

    @OnTextChanged(R.id.enter_phone_edittext)
    protected void validatePhoneNumber(CharSequence text) {
        mLastEnteredPhone = text.toString();
        if (!TextUtils.isNullOrEmpty(mLastEnteredPhone)) {
            clear_phone_view.setVisibility(View.VISIBLE);
            mLastEnteredPhone = PhoneUtils.validatePhoneNumber(mLastEnteredPhone, country);   // Validate phone number
            if (mLastEnteredPhone == null) {
                displayUIError(true, getString(R.string.invalled_phone));
            } else {
                // Check number validation from our server
                phone_progress_bar.setVisibility(View.VISIBLE);
                clear_phone_view.setVisibility(View.INVISIBLE);
                APICalls.requestDataAvailabilityCheck(mSession, EnterPhoneNumberActivity.this, mContext, Constants.ACTION_PHONE_AV, User.Keys.PARAMS_PHONE, mLastEnteredPhone, deviceID, mRequestQueue, new DataAvailabilityHandler() {
                    @Override
                    public void RunCompleted(boolean isAvailable) {
                        phone_progress_bar.setVisibility(View.GONE);
                        clear_phone_view.setVisibility(View.VISIBLE);

                        if (isAvailable || (fromActivity == AppConstants.FROM_FORGOT_PW_ACTIVITY && !isAvailable) ) {
                            displayUIError(false, null);
                        } else {
                            displayUIError(true, getString(R.string.phone_exists_already));
                        }
                    }
                });
            }
        } else {
            clear_phone_view.setVisibility(View.GONE);
            displayUIError(true, null);
        }
    }

    private void displayUIError(final boolean isError, final String errorMsg) {
        isPhoneNumberValid = !isError;
        if (isError) {
            phone_icon.setImageResource(R.drawable.ic_phone_off);
            if (errorMsg != null) {
                error_text_view.setText(errorMsg);
                error_text_view.setVisibility(View.VISIBLE);
            } else {
                error_text_view.setVisibility(View.INVISIBLE);
            }
        } else {
            phone_icon.setImageResource(R.drawable.ic_phone_on);
            error_text_view.setVisibility(View.INVISIBLE);
        }
    }

    @OnClick(R.id.enter_phone_x)
    protected void onClearPhoneTextClick() {
        phoneNumberEditView.getText().clear();
    }

    @OnTouch(R.id.enter_phone_root)
    protected boolean onRootTouch() {
        AndroidUtils.hideKeyboard(this, mRoot);
        return true;
    }

    @OnClick(R.id.header_left_card_view)
    protected void onBackClick() {
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActive = false;
        AndroidUtils.hideKeyboard(mContext, phoneNumberEditView);
    }
}
