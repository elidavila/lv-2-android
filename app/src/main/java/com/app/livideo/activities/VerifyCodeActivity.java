package com.app.livideo.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.app.livideo.R;
import com.app.livideo.api.APICalls;
import com.app.livideo.dialogs.ForgotPasswordDialog;
import com.app.livideo.interfaces.DataAvailabilityHandler;
import com.app.livideo.interfaces.LoginHandler;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.User;
import com.app.livideo.utils.AndroidUtils;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.Constants;
import com.app.livideo.utils.LVUtils;
import com.app.livideo.utils.Preferences;
import com.app.livideo.utils.TextUtils;
import com.bumptech.glide.Glide;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class VerifyCodeActivity extends AppCompatActivity {

    @Inject Context mContext;
    @Inject LVSession mSession;
    @Inject RequestQueue mRequestQueue;
    
    @Bind(R.id.verify_code_1) EditText verifyCode1;
    @Bind(R.id.verify_code_2) EditText verifyCode2;
    @Bind(R.id.verify_code_3) EditText verifyCode3;
    @Bind(R.id.verify_code_4) EditText verifyCode4;
    @Bind(R.id.header_main_img) ImageView header_logo;
    @Bind(R.id.header_left_icon) ImageView header_left_icon;
    @Bind(R.id.verify_code_icon) ImageView verify_icon;
    @Bind(R.id.header_title) TextView header_title;
    @Bind(R.id.verify_code_progress_bar) ImageView mProgressBar;
    @Bind(R.id.verify_code_resend_progress_bar) ImageView mResendProgressBar;
    @Bind(R.id.verify_code_resend_button) CardView resend_card_view;
    @Bind(R.id.verify_code_touch_blocker) FrameLayout touchBlocker;
    @Bind(R.id.verify_code_terms_text) TextView footer_text_view;
    @Bind(R.id.verify_code_footer_text) View footer1;
    
    private String deviceID, userID, mPhoneNumber, mPassword;
    private boolean isViewer, isActive = true;
    int fromActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_code);
        LVInject.inject(this);
        ButterKnife.bind(this);
        initialize();
    }

    private void initialize() {
        // Header
        header_logo.setVisibility(View.GONE);
        header_left_icon.setImageResource(R.drawable.ic_back_wshadow);

        // Variables
        isActive = true;
        fromActivity = getIntent().getIntExtra(AppConstants.ACTIVITY_KEY_INTENT, 0);
        userID = getIntent().getStringExtra(AppConstants.USER_ID_KEY_INTENT);
        deviceID = getIntent().getStringExtra(AppConstants.DEVICE_ID_KEY_INTENT);
        mPhoneNumber = getIntent().getStringExtra(AppConstants.PHONE_NUMBER_KEY_INTENT);
        isViewer = getIntent().getBooleanExtra(AppConstants.IS_USER_INTENT, true);
        mPassword = getIntent().getStringExtra(AppConstants.PASSWORD_KEY_INTENT);
        header_title.setText(mPhoneNumber);
        touchBlocker.setOnTouchListener(AppConstants.touchListner);
        AndroidUtils.hideKeyboard(mContext, verifyCode1);
        if (fromActivity != AppConstants.FROM_FORGOT_PW_ACTIVITY) {
            footer1.setVisibility(View.VISIBLE);
            footer_text_view.setVisibility(View.VISIBLE);
            footer_text_view.setText(getString((isViewer) ? R.string.viewer_agreement_text : R.string.artist_agreement_text));
        }

        // Send Code
        if (!TextUtils.isNullOrEmpty(mPhoneNumber) && fromActivity != AppConstants.FROM_NEW_DEVICE && !mSession.hasResendCode()) {
            APICalls.requestResendCode(mSession, this, mContext, userID, deviceID, mPhoneNumber, mRequestQueue, new DataAvailabilityHandler() {
                @Override
                public void RunCompleted(boolean isAvailable) {
                    if (!isAvailable) {
                        AndroidUtils.showShortToast(getString(R.string.error_coudnt_send_code_string), VerifyCodeActivity.this);
                    }
                }
            });
        }

        // Check if user has resend code in current session
        if (mSession.hasResendCode()) {
            resend_card_view.setAlpha(0.3f);
            resend_card_view.setEnabled(false);
        }
        Glide.with(mContext).load(R.drawable.loading_anim).asGif().into(mProgressBar);
        Glide.with(mContext).load(R.drawable.loading_anim).asGif().into(mResendProgressBar);
        setOnDeleteKeyListeners();
    }

    private void setOnDeleteKeyListeners() {
        verifyCode2.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_DEL && TextUtils.isNullOrEmpty(verifyCode2.getText().toString())) {
                    verifyCode1.requestFocus();
                    verify_icon.setImageResource(R.drawable.ic_phone_off);
                }
                return false;
            }
        });
        verifyCode3.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_DEL && TextUtils.isNullOrEmpty(verifyCode3.getText().toString())) {
                    verifyCode2.requestFocus();
                    verify_icon.setImageResource(R.drawable.ic_phone_off);
                }
                return false;
            }
        });
        verifyCode4.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_DEL && TextUtils.isNullOrEmpty(verifyCode4.getText().toString())) {
                    verifyCode3.requestFocus();
                    verify_icon.setImageResource(R.drawable.ic_phone_off);
                }
                return false;
            }
        });
    }

    @OnClick(R.id.verify_code_next_button)
    protected void onNextClick() {
        if (!isActive)
            return;

        String code = verifyCode1.getText().toString() + verifyCode2.getText().toString() + verifyCode3.getText().toString() + verifyCode4.getText().toString();
        if (!TextUtils.isNullOrEmpty(code) && code.length() == 4) {
            touchBlocker.setVisibility(View.VISIBLE);
            APICalls.requestCheckCode(fromActivity, mSession, this, mContext, userID, deviceID, code, mRequestQueue, new DataAvailabilityHandler() {
                @Override
                public void RunCompleted(boolean isAvailable) {
                    if (isAvailable) {
                        switch (fromActivity) {
                            case AppConstants.FROM_SIGNUP:
                                touchBlocker.setVisibility(View.GONE);
                                Intent intent;
                                // Add artist intent extras
                                if (!isViewer) {
                                    intent = new Intent(VerifyCodeActivity.this, EnterPasswordActivity.class);
                                    intent.putExtra(AppConstants.DISPLAY_NAME_KEY_INTENT, getIntent().getStringExtra(AppConstants.DISPLAY_NAME_KEY_INTENT));
                                    intent.putExtra(AppConstants.SOCIAL_ID_KEY_INTENT, getIntent().getStringExtra(AppConstants.SOCIAL_ID_KEY_INTENT));
                                    intent.putExtra(AppConstants.SOCIAL_TYPE_KEY_INTENT, getIntent().getStringExtra(AppConstants.SOCIAL_TYPE_KEY_INTENT));
                                } else {
                                    intent = new Intent(VerifyCodeActivity.this, EnterEmailActivity.class);
                                }

                                // new intent extras
                                intent.putExtra(AppConstants.IS_USER_INTENT, isViewer);
                                intent.putExtra(AppConstants.USER_ID_KEY_INTENT, userID);
                                intent.putExtra(AppConstants.DEVICE_ID_KEY_INTENT, deviceID);
                                intent.putExtra(AppConstants.PHONE_NUMBER_KEY_INTENT, mPhoneNumber);
                                intent.putExtra(AppConstants.ACTIVITY_KEY_INTENT, fromActivity);
                                startActivity(intent);
                                break;
                            case AppConstants.FROM_FORGOT_PW_ACTIVITY:
                                touchBlocker.setVisibility(View.GONE);
                                Preferences.setPreference(Constants.KEY_PHONE, mPhoneNumber, mContext);
                                ForgotPasswordDialog dialog = new ForgotPasswordDialog(userID, deviceID, VerifyCodeActivity.this, mRequestQueue, mSession);
                                dialog.show();
                                break;
                            case AppConstants.FROM_NEW_DEVICE:
                                requestLogin();
                                break;
                            default:
                                touchBlocker.setVisibility(View.GONE);
                                AndroidUtils.showShortToast(getString(R.string.retry_later), VerifyCodeActivity.this);
                                break;
                        }
                    } else {
                        touchBlocker.setVisibility(View.GONE);
                        AndroidUtils.showShortToast(getString(R.string.wrong_code_string), VerifyCodeActivity.this);
                    }
                }
            });
        } else {
            AndroidUtils.showShortToast(getString(R.string.wrong_code_string), VerifyCodeActivity.this);
        }
    }

    private void requestLogin() {
        APICalls.requestPhoneNumberLogin(this, mContext, mPhoneNumber, mPassword, userID, deviceID, mSession, mRequestQueue, new LoginHandler() {
            @Override
            public void RunCompleted(int UserCode, User user) {
                switch (UserCode) {
                    case Constants.USERCODE_SUCCESS:
                        APICalls.SendAWSIdentity(mContext, mRequestQueue, mSession);
                        LVUtils.StartLiVideo(user, mPhoneNumber, true, touchBlocker, 
                                VerifyCodeActivity.this, mContext, mSession, mRequestQueue, null);
                        break;
                    case Constants.USERCODE_INVALID_RESPONSE:
                        touchBlocker.setVisibility(View.GONE);
                        AndroidUtils.showShortToast(getString(R.string.invaled_server), VerifyCodeActivity.this);
                        break;
                    case Constants.USERCODE_NOT_EXISTS:
                        touchBlocker.setVisibility(View.GONE);
                        AndroidUtils.showShortToast(getString(R.string.user_does_not_exist), VerifyCodeActivity.this);
                        break;
                    case Constants.USERCODE_OTHER_ERROR:
                        touchBlocker.setVisibility(View.GONE);
                        AndroidUtils.showShortToast(getString(R.string.retry_later), VerifyCodeActivity.this);
                        break;
                    default:
                        touchBlocker.setVisibility(View.GONE);
                        AndroidUtils.showShortToast(getString(R.string.wrong_un_or_pw), VerifyCodeActivity.this);  //wrong password response
                }
            }
        });
    }

    @OnClick(R.id.verify_code_resend_button)
    protected void onResendCodeClick() {
        if (!mSession.hasResendCode()) {
            mSession.setHasResendCode(true);
            resend_card_view.setAlpha(0.3f);
            resend_card_view.setEnabled(false);
            mResendProgressBar.setVisibility(View.VISIBLE);
            APICalls.requestResendCode(mSession, this, mContext, userID, deviceID, mPhoneNumber, mRequestQueue, new DataAvailabilityHandler() {
                @Override
                public void RunCompleted(boolean isAvailable) {
                    mResendProgressBar.setVisibility(View.GONE);
                    if (!isAvailable) {
                        mSession.setHasResendCode(false);
                        resend_card_view.setAlpha(1);
                        resend_card_view.setEnabled(true);
                        AndroidUtils.showShortToast(getString(R.string.error_coudnt_send_code_string), VerifyCodeActivity.this);
                    }
                }
            });
        }
    }

    @OnClick(R.id.verify_code_footer_terms)
    protected void onTermsConditionsClick() {
        if (isActive) {
            Intent intent = new Intent(this, WebGeneratedActivity.class);
            intent.putExtra(Constants.USER_TYPE_KEY, Constants.VIEWER_USER);
            intent.putExtra(AppConstants.KEY_WEB_TYPE, AppConstants.WEB_TYPE_TERMS);
            startActivity(intent);
        }
    }

    @OnClick(R.id.verify_code_footer_privacy)
    protected void onPrivacyPolicyClick() {
        if (isActive) {
            Intent intent = new Intent(this, WebGeneratedActivity.class);
            intent.putExtra(AppConstants.KEY_WEB_TYPE, AppConstants.WEB_TYPE_PRIVACY);
            startActivity(intent);
        }
    }

    @OnTextChanged(value = R.id.verify_code_1, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    protected void handleCode1() {
        if (verifyCode1.getText().toString().length() == 1) {
            String code = verifyCode1.getText().toString() + verifyCode2.getText().toString() + verifyCode3.getText().toString() + verifyCode4.getText().toString();
            if (!TextUtils.isNullOrEmpty(code) && code.length() == 4) {
                APICalls.requestCheckCode(fromActivity, mSession, this, mContext, userID, deviceID, code, mRequestQueue, new DataAvailabilityHandler() {
                    @Override
                    public void RunCompleted(boolean isAvailable) {
                        verify_icon.setImageResource((isAvailable) ? R.drawable.ic_phone_check : R.drawable.ic_phone_off);
                    }
                });
            } else {
                verify_icon.setImageResource(R.drawable.ic_phone_off);
                verifyCode2.requestFocus();
            }
        }
    }

    @OnTextChanged(value = R.id.verify_code_2, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    protected void handleCode2() {
        if (verifyCode2.getText().toString().length() == 1) {
            String code = verifyCode1.getText().toString() + verifyCode2.getText().toString() + verifyCode3.getText().toString() + verifyCode4.getText().toString();
            if (!TextUtils.isNullOrEmpty(code) && code.length() == 4) {
                APICalls.requestCheckCode(fromActivity, mSession, this, mContext, userID, deviceID, code, mRequestQueue, new DataAvailabilityHandler() {
                    @Override
                    public void RunCompleted(boolean isAvailable) {
                        verify_icon.setImageResource((isAvailable) ? R.drawable.ic_phone_check : R.drawable.ic_phone_off);
                    }
                });
            } else {
                verify_icon.setImageResource(R.drawable.ic_phone_off);
                verifyCode3.requestFocus();
            }
        }
    }

    @OnTextChanged(value = R.id.verify_code_3, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    protected void handleCode3() {
        if (verifyCode3.getText().toString().length() == 1) {
            String code = verifyCode1.getText().toString() + verifyCode2.getText().toString() + verifyCode3.getText().toString() + verifyCode4.getText().toString();
            if (!TextUtils.isNullOrEmpty(code) && code.length() == 4) {
                APICalls.requestCheckCode(fromActivity, mSession, this, mContext, userID, deviceID, code, mRequestQueue, new DataAvailabilityHandler() {
                    @Override
                    public void RunCompleted(boolean isAvailable) {
                        verify_icon.setImageResource((isAvailable) ? R.drawable.ic_phone_check : R.drawable.ic_phone_off);
                    }
                });
            } else {
                verify_icon.setImageResource(R.drawable.ic_phone_off);
                verifyCode4.requestFocus();
            }
        }
    }

    @OnTextChanged(value = R.id.verify_code_4, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    protected void handleCode4() {
        if (verifyCode4.getText().toString().length() == 1) {
            String code = verifyCode1.getText().toString() + verifyCode2.getText().toString() + verifyCode3.getText().toString() + verifyCode4.getText().toString();
            if (!TextUtils.isNullOrEmpty(code) && code.length() == 4) {
                APICalls.requestCheckCode(fromActivity, mSession, this, mContext, userID, deviceID, code, mRequestQueue, new DataAvailabilityHandler() {
                    @Override
                    public void RunCompleted(boolean isAvailable) {
                        verify_icon.setImageResource((isAvailable) ? R.drawable.ic_phone_check : R.drawable.ic_phone_off);
                    }
                });
            } else {
                verify_icon.setImageResource(R.drawable.ic_phone_off);
            }
        }
    }

    @OnClick(R.id.header_left_card_view)
    protected void onBackIconClick() {
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActive = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isActive = true;
    }
}
