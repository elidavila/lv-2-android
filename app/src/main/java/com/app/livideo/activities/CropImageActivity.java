package com.app.livideo.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Bundle;
import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.app.livideo.dialogs.CustomAlertDialog;
import com.app.livideo.models.UserImage;
import com.app.livideo.models.validators.UserImageValidator;
import com.app.livideo.services.S3UploadService;
import com.app.livideo.utils.AndroidUtils;
import com.app.livideo.utils.FileUtils;
import com.bumptech.glide.Glide;
import com.isseiaoki.simplecropview.CropImageView;
import com.app.livideo.R;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.TextUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CropImageActivity extends AppCompatActivity {

    @Inject RequestQueue mRequestQueue;
    @Inject LVSession mSession;
    @Inject Context mContext;
    @Bind(R.id.header_title) TextView titleView;
    @Bind(R.id.header_right_icon) ImageView doneImageView;
    @Bind(R.id.cropImageView) CropImageView cropImageView;
    @Bind(R.id.crop_image_progress_bar) ImageView mProgressBar;

    File imageFile;
    String path;
    int rotation;
    int rotationInDegrees;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crop_image);
        LVInject.inject(this);
        ButterKnife.bind(this);
        initialize();
    }

    private void initialize() {
        // Viariables
        titleView.setText(R.string.crop_image_string);
        doneImageView.setImageResource(R.drawable.ic_checkwhite_wshadows);
        Glide.with(mContext).load(R.drawable.loading_anim).asGif().into(mProgressBar);

        // Get path of image file
        path = getIntent().getStringExtra(AppConstants.IMAGE_PATH_KEY);
        imageFile = new File(mContext.getCacheDir(), System.currentTimeMillis() + ".jpg");

        if (!TextUtils.isNullOrEmpty(path)) {
            Bitmap mBitmap = BitmapFactory.decodeFile(path);    // Get bitmap image from file path
            if (mBitmap != null) {

                // Set rotation value
                try {
                    ExifInterface exifInterface = new ExifInterface(path);
                    rotation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                    rotationInDegrees = FileUtils.exifToDegrees(rotation);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                // Check if we need to resize bitmap based on maximum size allowed
                if (mBitmap.getWidth() > AppConstants.MAX_PHOTO_SIZE || mBitmap.getHeight() > AppConstants.MAX_PHOTO_SIZE) {
                    mBitmap = FileUtils.resizeHugeBitmap(mBitmap); // resize image

                    // Check after resize if bitmap is valid size for displaying
                    if (mBitmap.getWidth() > AppConstants.MAX_PHOTO_SIZE || mBitmap.getHeight() > AppConstants.MAX_PHOTO_SIZE) {
                        displayErrorMessage();
                        return;
                    }
                }

                // Check if image needs rotation
                if (rotation != 0f) {
                    Matrix matrix = new Matrix();
                    matrix.preRotate(rotationInDegrees);   // matrix that will rotate our image
                    Bitmap adjustedBitmap = Bitmap.createBitmap(mBitmap, 0, 0, mBitmap.getWidth(), mBitmap.getHeight(), matrix, false);
                    cropImageView.setImageBitmap(adjustedBitmap);
                } else {
                    cropImageView.setImageBitmap(mBitmap);
                }
            } else {
                displayErrorMessage();
            }
        } else {
            displayErrorMessage();
        }
    }

    private void displayErrorMessage() {
        Dialog dialog = new CustomAlertDialog(this, getString(R.string.access_denied_string), false);
        dialog.show();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mSession == null) {
            AndroidUtils.restartApp(mContext);
            return;
        }
    }

    @OnClick(R.id.header_right_card_view)
    protected void OnDoneClick() {
        try {
            // COMPRESS 75%
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            Bitmap bitmap = cropImageView.getCroppedBitmap();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 75, bos);
            byte[] bitmapdata = bos.toByteArray();

            // WRITE TO FILE
            FileOutputStream fos;
            fos = new FileOutputStream(imageFile);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();

            cropImageView.setVisibility(View.GONE);
        } catch (Throwable e) {
            e.printStackTrace();
            imageFile = null;
        }

        //Add image's filepath
        if (imageFile != null) {
            UserImage userImage = new UserImage();
            userImage.setUrl("file://" + imageFile.getPath());
            mSession.getUser().getPictures().add(userImage);
            StartS3UploadService(imageFile);
            setResult(Activity.RESULT_OK);
            finish();
        } else {
            CustomAlertDialog dialog = new CustomAlertDialog(this, getString(R.string.cannot_process_image), true);
            dialog.show();
        }
    }

    private void StartS3UploadService(File file) {
        //start intent service to upload the video to S3
        Intent intent = new Intent(mContext, S3UploadService.class);
        intent.putExtra(S3UploadService.EXTRA_LOCAL_FILE, file);
        intent.putExtra(S3UploadService.EXTRA_IS_PICTURE, true);
        startService(intent);
    }
}
