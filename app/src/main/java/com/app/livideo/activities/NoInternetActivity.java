package com.app.livideo.activities;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.android.volley.RequestQueue;
import com.app.livideo.R;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.net.LVStringRequest;
import com.app.livideo.utils.AndroidUtils;

import java.util.ArrayList;

import javax.inject.Inject;

public class NoInternetActivity extends AppCompatActivity {

    @Inject Context mContext;
    @Inject LVSession mSession;
    @Inject RequestQueue mRequestQueue;
    private boolean deviceHasInternet = false;
    private boolean isBackground = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_internet);
        LVInject.inject(this);
        ConstantlyCheckInternetConnection();
    }

    private void ConstantlyCheckInternetConnection() {
        if (deviceHasInternet || isBackground) {
            return;
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (AndroidUtils.isNetworkAvailable(mContext)) {
                    deviceHasInternet = true;
                    ArrayList<LVStringRequest> mRequestList = mSession.getDelayedAPIRequestList();
                    if (mRequestList != null && !mRequestList.isEmpty()) {
                        for (int i = 0; i < mRequestList.size(); i++)
                            mRequestQueue.add(mRequestList.get(i));
                        mSession.setDelayedAPIRequestList(null);
                    }
                    finish();
                } else {
                    ConstantlyCheckInternetConnection();
                }
            }
        }, 1000);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!deviceHasInternet) {
            if (isBackground)
                isBackground = false;
            ConstantlyCheckInternetConnection();
        } else {
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        if (AndroidUtils.isNetworkAvailable(mContext))
            super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        isBackground = true;
    }
}
