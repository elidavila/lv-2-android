package com.app.livideo.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.app.livideo.fragments.WebContentFragment;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.Constants;


public class WebGeneratedActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        // If not already added to the Fragment manager add it. If you don't do this a new Fragment will be added every time this method is called (Such as on orientation change)
        if(savedInstanceState == null) {
            int userType = getIntent().getIntExtra(Constants.USER_TYPE_KEY, -1);
            int isTerms = getIntent().getIntExtra(AppConstants.KEY_WEB_TYPE, 0);

            switch (isTerms){
                case AppConstants.WEB_TYPE_TERMS:
                    if (userType == Constants.BROADCASTER_USER) {
                        getSupportFragmentManager().beginTransaction().add(android.R.id.content, WebContentFragment.newSingleType(AppConstants.TermsBroadcaster, true)).commit();
                    } else {
                        getSupportFragmentManager().beginTransaction().add(android.R.id.content, WebContentFragment.newSingleType(AppConstants.TermsViewer, true)).commit();
                    }
                    break;
                case AppConstants.WEB_TYPE_PRIVACY:
                    getSupportFragmentManager().beginTransaction().add(android.R.id.content, WebContentFragment.newSingleType(AppConstants.Privacy, true)).commit();
                    break;
                case AppConstants.WEB_TYPE_FAQ:
                    if (userType == Constants.BROADCASTER_USER) {
                        getSupportFragmentManager().beginTransaction().add(android.R.id.content, WebContentFragment.newSingleType(AppConstants.FAQBroadcaster, true)).commit();
                    } else {
                        getSupportFragmentManager().beginTransaction().add(android.R.id.content, WebContentFragment.newSingleType(AppConstants.FAQViewer, true)).commit();
                    }
                    break;
            }

        }
    }
}
