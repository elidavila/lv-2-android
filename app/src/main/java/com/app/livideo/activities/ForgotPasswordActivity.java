package com.app.livideo.activities;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.app.livideo.api.APICalls;
import com.app.livideo.interfaces.DataAvailabilityHandler;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.User;
import com.bumptech.glide.Glide;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.app.livideo.R;
import com.app.livideo.adapters.CountryAdapter;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.models.Country;
import com.app.livideo.utils.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.TreeSet;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class ForgotPasswordActivity extends AppCompatActivity {
    @Inject LVSession mSession;
    @Inject Context mContext;
    @Inject RequestQueue mRequestQueue;
    @Bind(R.id.forgot_password_number_edit_view) EditText phoneNumberEditView;
    @Bind(R.id.forgot_password_country_picker) Spinner mSpinner;
    @Bind(R.id.header_left_icon) ImageView backIcon;
    @Bind(R.id.header_img) ImageView logo;
    @Bind(R.id.phone_number_check_progress) ImageView phoneNumberProgressBar;
    @Bind(R.id.phone_check_icon) ImageView phoneCheck;
    @Bind(R.id.phone_number_check_text) TextView phoneCheckText;
    @Bind(R.id.forgot_password_submit)
    CardView createButton;
    protected SparseArray<ArrayList<Country>> mCountriesMap = new SparseArray<>();
    protected PhoneNumberUtil mPhoneNumberUtil = PhoneNumberUtil.getInstance();
    protected String mLastEnteredPhone;
    protected CountryAdapter mAdapter;
    boolean phoneAV;
    String phoneNumber, deviceID;
    Country country;
    boolean isActive = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        LVInject.inject(this);
        ButterKnife.bind(this);
        initialize();
        initCodes(mContext);
    }

    private void initialize() {
        //Header
        logo.setVisibility(View.INVISIBLE);
        backIcon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_back_wshadow));
        backIcon.setVisibility(View.VISIBLE);
        deviceID = Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.ANDROID_ID);
        Glide.with(mContext).load(R.drawable.loading_anim).asGif().into(phoneNumberProgressBar);

        //Country Picker
        isActive = true;
        mSpinner.setOnItemSelectedListener(mOnItemSelectedListener);
        mAdapter = new CountryAdapter(mContext);
        mSpinner.setAdapter(mAdapter);
        phoneNumberEditView.setFilters(new InputFilter[]{filter});
    }

    @OnClick(R.id.forgot_password_submit)
    protected void onSubmitClick() {
        AndroidUtils.hideKeyboard(mContext, phoneNumberEditView);
        if (mLastEnteredPhone != null) {
            Preferences.setPreference(com.app.livideo.utils.Constants.KEY_PHONE, mLastEnteredPhone.substring(1), mContext);
            mSession.getUser().setUserID(".");
            mSession.getUser().setDeviceID(deviceID);
            goToVerifyCodeActivity();
        } else {
            phoneCheck.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_uncheck));
            phoneCheckText.setText(mContext.getString(R.string.invalled_phone));
            phoneAV = false;
            createButton.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.livideo_trans_grey));
            createButton.setEnabled(false);
        }
    }

    @OnTextChanged(R.id.forgot_password_number_edit_view)
    void onTextChaned(CharSequence text) {
        mLastEnteredPhone = text.toString();
        if (!TextUtils.isNullOrEmpty(mLastEnteredPhone)) {
            phoneCheck.setVisibility(View.VISIBLE);
            phoneCheckText.setVisibility(View.VISIBLE);

            // Check number validation from Google
            String phoneValidation = validate();
            if (phoneValidation == null) {
                phoneCheck.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_uncheck));
                phoneCheckText.setText(mContext.getString(R.string.invalled_phone));
                phoneAV = false;
                createButton.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.livideo_trans_grey));
                createButton.setEnabled(false);
            } else {
                // Check number validation from our server
                phoneNumber = getPhoneNumber();
                phoneCheck.setVisibility(View.GONE);
                phoneCheckText.setVisibility(View.GONE);
                phoneNumberProgressBar.setVisibility(View.VISIBLE);
                APICalls.requestDataAvailabilityCheck(mSession, ForgotPasswordActivity.this, mContext, Constants.ACTION_PHONE_AV, User.Keys.PARAMS_PHONE, phoneNumber, deviceID, mRequestQueue, new DataAvailabilityHandler() {
                    @Override
                    public void RunCompleted(boolean isAvailable) {
                        phoneNumberProgressBar.setVisibility(View.GONE);
                        if (isAvailable) {
                            phoneCheck.setImageResource(R.drawable.ic_uncheck);
                            phoneCheckText.setText(mContext.getString(R.string.user_does_not_exist));
                            phoneCheck.setVisibility(View.VISIBLE);
                            phoneCheckText.setVisibility(View.VISIBLE);
                            phoneAV = false;
                        } else {
                            phoneCheck.setImageResource(R.drawable.ic_checkwhite_wshadows);
                            phoneCheck.setVisibility(View.VISIBLE);
                            phoneCheckText.setVisibility(View.GONE);
                            phoneAV = true;
                        }

                        if (phoneAV) {
                            createButton.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.livideo_teal_primary));
                            createButton.setEnabled(true);
                        } else {
                            createButton.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.livideo_trans_grey));
                            createButton.setEnabled(false);
                        }
                    }
                });
            }
        } else {
            phoneCheck.setVisibility(View.GONE);
            phoneCheckText.setVisibility(View.GONE);
        }
    }

    public String getPhoneNumber() {
        String str = mLastEnteredPhone;
        if (str != null) {
            str = str.replaceAll("\\+", "");
            str = str.replaceAll(" ", "");
            str = str.replaceAll("\\-", "");
        }
        return str;
    }

    protected String validate() {
        String region = null;
        String phoneNumber = null;
        if (mLastEnteredPhone != null) {
            try {
                if (!mLastEnteredPhone.contains("+") && country != null) {
                    mLastEnteredPhone = "+" + country.getCountryCode() + mLastEnteredPhone;
                }
                Phonenumber.PhoneNumber p = mPhoneNumberUtil.parse(mLastEnteredPhone, null);
                StringBuilder sb = new StringBuilder(16);
                sb.append('+').append(p.getCountryCode()).append(p.getNationalNumber());
                phoneNumber = sb.toString();
                region = mPhoneNumberUtil.getRegionCodeForNumber(p);
            } catch (NumberParseException ignore) {
            }
        }

        if (region != null)
            return phoneNumber;
        else
            return null;
    }

    private void goToVerifyCodeActivity() {
        if (isActive) {
            Intent intent = new Intent(mContext, VerifyCodeActivity.class);
            intent.putExtra(AppConstants.ACTIVITY_KEY_INTENT, AppConstants.FROM_FORGOT_PW_ACTIVITY);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActive = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isActive = true;
    }

    @OnClick(R.id.forgot_passowrd_root_layout)
    protected void onRootClick() {
        AndroidUtils.hideKeyboard(mContext, phoneNumberEditView);
    }


    @OnClick(R.id.header_left_card_view)
    protected void onBackIconClick() {
        finish();
    }

    //Initializes our country code
    protected AdapterView.OnItemSelectedListener mOnItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            country = (Country) mSpinner.getItemAtPosition(position);
            if (mLastEnteredPhone != null && mLastEnteredPhone.startsWith(country.getCountryCodeStr())) {
                return;
            }
            phoneNumberEditView.getText().clear();
            //phoneNumberEditView.getText().insert(phoneNumberEditView.getText().length() > 0 ? 1 : 0, String.valueOf(c.getCountryCode()));
            phoneNumberEditView.setSelection(phoneNumberEditView.length());
            mLastEnteredPhone = null;
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    protected void initCodes(Context context) {
        new AsyncPhoneInitTask(context).execute();
    }

    protected class AsyncPhoneInitTask extends AsyncTask<Void, Void, ArrayList<Country>> {
        private int mSpinnerPosition = -1;
        private Context mContext;

        public AsyncPhoneInitTask(Context context) {
            mContext = context;
        }

        @Override
        protected ArrayList<Country> doInBackground(Void... params) {
            ArrayList<Country> data = new ArrayList<>(233);
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new InputStreamReader(mContext.getAssets().open("countries.dat"), "UTF-8"));

                // do reading, usually loop until end of file reading
                String line;
                int i = 0;
                while ((line = reader.readLine()) != null) {
                    //process line
                    Country c = new Country(mContext, line, i);
                    data.add(c);
                    ArrayList<Country> list = mCountriesMap.get(c.getCountryCode());
                    if (list == null) {
                        list = new ArrayList<Country>();
                        mCountriesMap.put(c.getCountryCode(), list);
                    }
                    list.add(c);
                    i++;
                }
            } catch (IOException e) {
//                Log.e("IOExeption", e.getCause() + ""); //NON-NLS
                e.printStackTrace();
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
//                        Log.e("IOExeption", e.getCause() + ""); //NON-NLS
                        e.printStackTrace();
                    }
                }
            }

            if (!android.text.TextUtils.isEmpty(phoneNumberEditView.getText()))
                return data;

            String countryRegion = PhoneUtils.getCountryRegionFromPhone(mContext);
            int code = mPhoneNumberUtil.getCountryCodeForRegion(countryRegion);
            ArrayList<Country> list = mCountriesMap.get(code);
            if (list != null) {
                for (Country c : list) {
                    if (c.getPriority() == 0) {
                        mSpinnerPosition = c.getNum();
                        break;
                    }
                }
            }
            return data;
        }

        @Override
        protected void onPostExecute(ArrayList<Country> data) {
            mAdapter.addAll(data);
            if (mSpinnerPosition > 0)
                mSpinner.setSelection(mSpinnerPosition);
        }
    }

    InputFilter filter = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            for (int i = start; i < end; i++) {
                char c = source.charAt(i);
                if (dstart > 0 && !Character.isDigit(c))
                    return "";
            }
            return null;
        }
    };

    protected static final TreeSet<String> CANADA_CODES = new TreeSet<String>();
    static {
        CANADA_CODES.add("204");
        CANADA_CODES.add("236");
        CANADA_CODES.add("249");
        CANADA_CODES.add("250");
        CANADA_CODES.add("289");
        CANADA_CODES.add("306");
        CANADA_CODES.add("343");
        CANADA_CODES.add("365");
        CANADA_CODES.add("387");
        CANADA_CODES.add("403");
        CANADA_CODES.add("416");
        CANADA_CODES.add("418");
        CANADA_CODES.add("431");
        CANADA_CODES.add("437");
        CANADA_CODES.add("438");
        CANADA_CODES.add("450");
        CANADA_CODES.add("506");
        CANADA_CODES.add("514");
        CANADA_CODES.add("519");
        CANADA_CODES.add("548");
        CANADA_CODES.add("579");
        CANADA_CODES.add("581");
        CANADA_CODES.add("587");
        CANADA_CODES.add("604");
        CANADA_CODES.add("613");
        CANADA_CODES.add("639");
        CANADA_CODES.add("647");
        CANADA_CODES.add("672");
        CANADA_CODES.add("705");
        CANADA_CODES.add("709");
        CANADA_CODES.add("742");
        CANADA_CODES.add("778");
        CANADA_CODES.add("780");
        CANADA_CODES.add("782");
        CANADA_CODES.add("807");
        CANADA_CODES.add("819");
        CANADA_CODES.add("825");
        CANADA_CODES.add("867");
        CANADA_CODES.add("873");
        CANADA_CODES.add("902");
        CANADA_CODES.add("905");
    }
    /*
        protected OnPhoneChangedListener mOnPhoneChangedListener = new OnPhoneChangedListener() {
            @Override
            public void onPhoneChanged(String phone) {
                //Validate phone number
                mLastEnteredPhone = phone;
                if (!TextUtils.isNullOrEmpty(mLastEnteredPhone) && mLastEnteredPhone.length() > 4) {
                    phoneCheck.setVisibility(View.VISIBLE);
                    phoneCheckText.setVisibility(View.VISIBLE);

                    // Check number validation from Google
                    String phoneValidation = validate();
                    if (phoneValidation == null) {
                        phoneCheck.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_uncheck));
                        phoneCheckText.setText(mContext.getString(R.string.invalled_phone));
                        phoneAV = false;
                        createButton.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.livideo_trans_grey));
                        createButton.setEnabled(false);
                    } else {
                        // Check number validation from our server
                        phoneNumber = getPhoneNumber();
                        phoneCheck.setVisibility(View.GONE);
                        phoneCheckText.setVisibility(View.GONE);
                        phoneNumberProgressBar.setVisibility(View.VISIBLE);
                        APICalls.requestDataAvailabilityCheck(mSession, ForgotPasswordActivity.this, mContext, Constants.ACTION_PHONE_AV, User.Keys.PARAMS_PHONE, phoneNumber, deviceID, mRequestQueue, new DataAvailabilityHandler() {
                            @Override
                            public void RunCompleted(boolean isAvailable) {
                                phoneNumberProgressBar.setVisibility(View.GONE);
                                if (isAvailable) {
                                    phoneCheck.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_uncheck));
                                    phoneCheckText.setText(mContext.getString(R.string.error));
                                    phoneCheck.setVisibility(View.VISIBLE);
                                    phoneCheckText.setVisibility(View.VISIBLE);
                                    phoneAV = false;
                                } else {
                                    phoneCheck.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_check));
                                    phoneCheck.setVisibility(View.VISIBLE);
                                    phoneCheckText.setVisibility(View.GONE);
                                    phoneAV = true;
                                }

                                if (phoneAV) {
                                    createButton.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.livideo_teal_primary));
                                    createButton.setEnabled(true);
                                } else {
                                    createButton.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.livideo_trans_grey));
                                    createButton.setEnabled(false);
                                }
                            }
                        });
                    }
                } else {
                    phoneCheck.setVisibility(View.GONE);
                    phoneCheckText.setVisibility(View.GONE);
                }


                try {
                    Phonenumber.PhoneNumber p = mPhoneNumberUtil.parse(phone, null);
                    ArrayList<Country> list = mCountriesMap.get(p.getCountryCode());
                    Country country = null;
                    if (list != null) {
                        if (p.getCountryCode() == 1) {
                            String num = String.valueOf(p.getNationalNumber());
                            if (num.length() >= 3) {
                                String code = num.substring(0, 3);
                                if (CANADA_CODES.contains(code)) {
                                    for (Country c : list) {
                                        // Canada has priority 1, US has priority 0
                                        if (c.getPriority() == 1) {
                                            country = c;
                                            break;
                                        }
                                    }
                                }
                            }
                        }

                        if (country == null) {
                            for (Country c : list) {
                                if (c.getPriority() == 0) {
                                    country = c;
                                    break;
                                }
                            }
                        }
                    }

                    if (country != null) {
                        final int position = country.getNum();
                        mSpinner.post(new Runnable() {
                            @Override
                            public void run() {
                                mSpinner.setSelection(position);
                            }
                        });
                    }
                } catch (NumberParseException ignore) {
                }
            }
        };*/
}
