package com.app.livideo.activities;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.app.livideo.R;
import com.app.livideo.adapters.SearchArtistAdapter;
import com.app.livideo.api.APICalls;
import com.app.livideo.interfaces.ArtistHandler;
import com.app.livideo.interfaces.EnterStreamHandler;
import com.app.livideo.interfaces.SearchDataHandler;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.Artist;
import com.app.livideo.models.EnterStreamData;
import com.app.livideo.models.SearchData;
import com.app.livideo.models.Stream;
import com.app.livideo.models.validators.ArtistValidator;
import com.app.livideo.models.validators.StreamValidator;
import com.app.livideo.models.validators.UserValidator;
import com.app.livideo.utils.AndroidUtils;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.LVUtils;
import com.app.livideo.utils.TextUtils;
import com.bumptech.glide.Glide;

import java.util.Collections;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import butterknife.OnTextChanged;

public class SearchArtistActivity extends AppCompatActivity {
    private final int REQ_CODE_SPEECH_INPUT = 100;
    @Inject RequestQueue mRequestQueue;
    @Inject LVSession mSession;
    @Inject Context mContext;

    @Bind(R.id.header_left_icon) ImageView backButton;
    @Bind(R.id.header_right_icon) ImageView voiceButton;
    @Bind(R.id.header_search) EditText searchEditText;
    @Bind(R.id.search_artist_recyclerview) RecyclerView searchRecyclerView;
    @Bind(R.id.search_artist_no_results) TextView noResultsView;
    @Bind(R.id.search_artist_label) TextView mLabel;
    @Bind(R.id.search_artist_touch_blocker) FrameLayout touch_blocker;
    @Bind(R.id.search_artist_progress_bar) ImageView mProagressBar;
    SearchArtistAdapter mSearchArtistAdapter;
    DialogInterface.OnDismissListener mDismissHandler = new DialogInterface.OnDismissListener() {
        @Override
        public void onDismiss(DialogInterface dialog) {
            touch_blocker.setVisibility(View.GONE);
        }
    };
    private boolean isActive = false;
    private SearchArtistAdapter.OnArtistClickedListener mArtistClickedListener = new SearchArtistAdapter.OnArtistClickedListener() {
        @Override
        public void onArtistClicked(Artist artist) {
            if (isActive) {
                Intent intent = new Intent(SearchArtistActivity.this, ArtistPageActivity.class);
                intent.putExtra(AppConstants.ACTIVITY_KEY_INTENT, AppConstants.SEARCH);
                intent.putExtra(AppConstants.ARTIST_KEY_INTENT, artist);
                intent.setFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
                startActivity(intent);
            }
        }
    };
    private SearchArtistAdapter.OnStreamClickedListener mStreamClickedListener = new SearchArtistAdapter.OnStreamClickedListener() {
        @Override
        public void onStreamClicked(Stream stream) {
            performStreamClick(stream);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_artist);
        LVInject.inject(this);
        ButterKnife.bind(this);
        if (mSession == null || !UserValidator.isValid(mSession.getUser())) {
            AndroidUtils.restartApp(mContext);  // RUN CHAT FRAGMENT, AFTER RESTART
            return;
        }
        initialize();
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActive = false;
    }

    @Override
    public void onResume() {
        super.onResume();
        isActive = true;
        if (mSession == null || !UserValidator.isValid(mSession.getUser())) {
            AndroidUtils.restartApp(mContext);  // RUN CHAT FRAGMENT, AFTER RESTART
            return;
        }
        new Handler().postDelayed(new Runnable() {
            public void run() {
                searchEditText.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 0, 0, 0));
                searchEditText.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP, 0, 0, 0));
                searchEditText.setSelection(searchEditText.length());
            }
        }, 200);
    }

    private void initialize() {
        //Set assets
        backButton.setImageResource(R.drawable.ic_back_wshadow);
        voiceButton.setImageResource(R.drawable.ic_mic_white_wshadows);
        searchEditText.setVisibility(View.VISIBLE);
        Glide.with(mContext).load(R.drawable.loading_anim).asGif().into(mProagressBar);

        //Set adapter
        if (mSession.getSearchData() != null
                && (ArtistValidator.isValid(mSession.getSearchData().getArtists())
                || StreamValidator.isValid(mSession.getSearchData().getStreams())))
            addSearchView(mSession.getSearchData());
        else
            requestSearchData();
    }

    private void requestSearchData() {
        touch_blocker.setVisibility(View.VISIBLE);
        APICalls.requestSearchData(SearchArtistActivity.this, mContext, mSession, mRequestQueue, new SearchDataHandler() {
            @Override
            public void RunCompleted(int code, SearchData searchData) {
                if (isActive) {
                    switch (code) {
                        case com.app.livideo.utils.Constants.USERCODE_SUCCESS:
                            touch_blocker.setVisibility(View.GONE);
                            mSession.setSearchData(searchData);
                            if (mSession.getSearchData() != null)
                                addSearchView(mSession.getSearchData());
                            break;
                        case com.app.livideo.utils.Constants.USERCODE_INVALID_RESPONSE:
                            handleError(getString(R.string.invaled_server), true);
                            break;
                        case com.app.livideo.utils.Constants.USERCODE_NOT_EXISTS:
                            handleError(getString(R.string.user_does_not_exist), true);
                            break;
                        case com.app.livideo.utils.Constants.USERCODE_OTHER_ERROR:
                            handleError(getString(R.string.retry_later), true);
                            break;
                        default:
                            handleError(getString(R.string.api_error), true);
                            break;
                    }
                }
            }

            private void handleError(String errorString, final boolean shouldFinish) {
                if (mContext == null)
                    return;

                if (errorString != null) {
                    Toast.makeText(mContext, errorString, Toast.LENGTH_SHORT).show();
                    if (shouldFinish)
                        finish();
                } else {
                    AndroidUtils.restartApp(mContext);
                }
            }
        });
    }

    private void addSearchView(SearchData searchData) {
        // Sort all artists by name
        Collections.sort(searchData.getArtists(), AppConstants.ARTIST_NAME_COMPARATOR);

        //Set adapter
        final LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        searchRecyclerView.setLayoutManager(mLinearLayoutManager);
        mSearchArtistAdapter = new SearchArtistAdapter(searchData, mArtistClickedListener, mStreamClickedListener, noResultsView, mLabel);
        searchRecyclerView.setAdapter(mSearchArtistAdapter);
    }

    @OnTextChanged(R.id.header_search)
    void onTextChanged(CharSequence text) {
        voiceButton.setImageResource((searchEditText.length() == 0) ? R.drawable.ic_mic_white_wshadows : R.drawable.ic_x_wshadows);
        if (mSearchArtistAdapter != null) {
            mSearchArtistAdapter.bind(
                    LVUtils.getArtistsThatContainString(text.toString(), mSession.getSearchData().getArtists()),
                    LVUtils.getStreamsThatContainString(text.toString(), mSession.getSearchData().getStreams()));
        }
    }

    @OnEditorAction(R.id.header_search)
    boolean enterPressed(int actionId){
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            if (mSearchArtistAdapter != null) {
                mSearchArtistAdapter.bind(
                        LVUtils.getArtistsThatContainString(searchEditText.getText().toString(), mSession.getSearchData().getArtists()),
                        LVUtils.getStreamsThatContainString(searchEditText.getText().toString(), mSession.getSearchData().getStreams()));
            }
        }
        return false;
    }

    @OnClick(R.id.header_left_icon)
    protected void onBackIconClick() {
        finish();
    }

    @OnClick(R.id.header_right_icon)
    protected void onMicrophoneIconClick() {
        if (searchEditText.length() == 0)
            promptSpeechInput();
        else
            searchEditText.getText().clear();
    }

    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, getString(R.string.speak_now_string));
        try {
            if (isActive)
                startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            AndroidUtils.showShortToast(getString(R.string.speech_not_supported), this);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null && resultCode == RESULT_OK) {
            if (requestCode == REQ_CODE_SPEECH_INPUT) {
                searchEditText.setText(data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS).get(0));
                searchEditText.setSelection(searchEditText.length());
            } else {
                int goTo = data.getIntExtra(AppConstants.ACTIVITY_KEY_INTENT, requestCode);
                if (goTo != AppConstants.SEARCH && isActive) {
                    Intent intent = new Intent();
                    intent.putExtra(AppConstants.ACTIVITY_KEY_INTENT, goTo);
                    setResult(RESULT_OK, intent);
                    super.onBackPressed();
                }
            }
        }
    }

    public void performStreamClick(final Stream stream) {
        if (!StreamValidator.isValid(stream)) {
            LVUtils.handleError(getString(R.string.invalid_artist_object), isActive, touch_blocker, mContext, SearchArtistActivity.this);
            return;
        }

        touch_blocker.setVisibility(View.VISIBLE);
        APICalls.requestEnterStream(mSession, this, mContext, stream.getStreamId(), mRequestQueue, new EnterStreamHandler() {
            @Override
            public void RunCompleted(EnterStreamData enterStreamData) {
                if (isActive && enterStreamData != null) {
                    switch (enterStreamData.getCode()) {
                        case AppConstants.STREAMCODE_SUCCESS: // public
                            LVUtils.startVideoPlayer(stream, enterStreamData, touch_blocker, isActive, SearchArtistActivity.this);
                            break;
                        case AppConstants.STREAMCODE_NOT_SUBSCRIBED:
                        case AppConstants.STREAMCODE_IS_PRIVATE:
                            APICalls.requestArtistInfo(stream.getArtistId(), SearchArtistActivity.this, mContext, mSession, mRequestQueue, new ArtistHandler() {
                                @Override
                                public void RunCompleted(boolean isAvailable, Artist artist) {
                                    if (isAvailable) {
                                        LVUtils.beginUserSubscriptionProcess(artist, SearchArtistActivity.this, mSession, mRequestQueue, mDismissHandler, new Runnable() {
                                            @Override
                                            public void run() {
                                                touch_blocker.setVisibility(View.GONE);
                                                mSession.setCurrentRunnable(null);
                                                performStreamClick(stream);
                                            }
                                        });
                                    } else {
                                        LVUtils.handleError(getString(R.string.invalid_artist_object), false, touch_blocker, mContext, SearchArtistActivity.this);
                                    }
                                }
                            });
                            break;
                        case AppConstants.STREAMCODE_NOT_EXISTS:
                            LVUtils.handleError(getString(R.string.stream_unavailable), false, touch_blocker, mContext, SearchArtistActivity.this);
                            break;
                        case AppConstants.STREAMCODE_OTHER_ERROR:
                            LVUtils.handleError(getString(R.string.retry_later), false, touch_blocker, mContext, SearchArtistActivity.this);
                            break;
                        default:
                            LVUtils.handleError(getString(R.string.api_error), false, touch_blocker, mContext, SearchArtistActivity.this);
                            break;
                    }
                }
            }
        });
    }
}
