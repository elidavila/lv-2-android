package com.app.livideo.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.app.livideo.BuildConfig;
import com.app.livideo.R;
import com.app.livideo.adapters.ArtistChatPreviewAdapter;
import com.app.livideo.adapters.ArtistPageTimeLineAdapter;
import com.app.livideo.api.APICalls;
import com.app.livideo.dialogs.RemoveStreamDialog;
import com.app.livideo.interfaces.ArtistPageHandler;
import com.app.livideo.interfaces.ChatMessagesHandler;
import com.app.livideo.interfaces.EnterStreamHandler;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.Artist;
import com.app.livideo.models.ArtistChatMessage;
import com.app.livideo.models.ArtistPage;
import com.app.livideo.models.EnterStreamData;
import com.app.livideo.models.Stream;
import com.app.livideo.models.User;
import com.app.livideo.models.validators.ArtistChatMessageValidator;
import com.app.livideo.models.validators.ArtistValidator;
import com.app.livideo.models.validators.StreamValidator;
import com.app.livideo.models.validators.UserValidator;
import com.app.livideo.utils.AndroidUtils;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.LVUtils;
import com.app.livideo.utils.ScreenDensityUtil;
import com.app.livideo.utils.TextUtils;
import com.app.livideo.views.VerticalViewPager;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.PicassoTools;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnPageChange;

public class ArtistPageActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    @Inject RequestQueue mRequestQueue;
    @Inject LVSession mSession;
    @Inject Context mContext;

    @Bind(R.id.header_title) TextView headerTitleView;
    @Bind(R.id.header_left_icon) ImageView backButton;
    @Bind(R.id.header_right_icon) ImageView bellButton;
    @Bind(R.id.progress_bar) ImageView mProgressBar;
    @Bind(R.id.animated_arrow) ImageView animatedUpArrow;
    @Bind(R.id.artist_page_touch_blocker) public FrameLayout touch_blocker;
    @Bind(R.id.artist_page_time_line) VerticalViewPager time_line_view_pager;
    @Bind(R.id.artist_page_preview_chat) RecyclerView preview_chat_recycler_view;
    @Bind(R.id.artist_page_refresh_swipe) SwipeRefreshLayout mSwipe;

    ArtistChatPreviewAdapter chatPreviewAdapter;
    public int GO_TO = -1, mSubscriptionType;
    Artist mArtist;
    ArtistPage mArtistPage;
    boolean shouldStopUpdatingPreviewChat = false, isActive = true, isRefreshing;
    boolean isDownloadingArtistPage = false, isDownloadingArtistChat = false;
    ArtistPageTimeLineAdapter artistPageTimeLineAdapter;

    DialogInterface.OnDismissListener touchBlockerHandler = new DialogInterface.OnDismissListener() {
        @Override
        public void onDismiss(DialogInterface dialog) {
            touch_blocker.setVisibility(View.GONE);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist_page);
        LVInject.inject(this);
        ButterKnife.bind(this);
        if (mSession == null || !UserValidator.isValid(mSession.getUser())) {
            AndroidUtils.restartApp(mContext);
            return;
        }
        startArtistPageView();  // WE GOOD HERE (most hit ratio)
    }

    private void startArtistPageView() {
        initVariables();
        requestArtistPage();
        requestChatMsgs(false);
    }

    private void initVariables() {
        isActive = true;
        Glide.with(mContext).load(R.drawable.loading_anim).asGif().into(mProgressBar);
        mArtist = (Artist) getIntent().getSerializableExtra(AppConstants.ARTIST_KEY_INTENT);
        touch_blocker.setOnTouchListener(AppConstants.touchListner);
        mSwipe.setOnRefreshListener(this);
        mSwipe.setColorSchemeColors(ContextCompat.getColor(mContext, R.color.livideo_teal_primary), ContextCompat.getColor(mContext, R.color.livideo_black));
    }

    private void requestArtistPage() {
        isDownloadingArtistPage = true;
        if (isRefreshing) mSwipe.setRefreshing(true);
        else touch_blocker.setVisibility(View.VISIBLE);
        APICalls.requestArtistPage(mArtist.getArtistId(), this, mContext, mSession, mRequestQueue, new ArtistPageHandler() {
            @Override
            public void RunCompleted(boolean isAvailable, ArtistPage artistPage) {
                isDownloadingArtistPage = false;
                isRefreshing = false;
                mSwipe.setRefreshing(false);
                touch_blocker.setVisibility(View.GONE);

                if (isFinishing())
                    return;

                if (isAvailable) {
                    addArtistPageView(artistPage);  // ADD VIEW
                } else {
                    LVUtils.handleError(getString(R.string.invalid_artist_object), true, touch_blocker, mContext, ArtistPageActivity.this);
                }
            }
        });
    }

    private void addArtistPageView(ArtistPage artistPage) {
        saveArtistPageData(artistPage);
        initView();
        setSubscriptionType();
        addTimeLineView(artistPage);
        if (BuildConfig.DEBUG) Log.e("ARTIST ID", mArtist.getArtistId());
    }

    private void saveArtistPageData(ArtistPage artistPage) {
        mArtistPage = artistPage;
        mArtist = mArtistPage.getArtist();
        mSession.setCurrentArtist(mArtist);
    }

    private void initView() {
        headerTitleView.setText(mArtist.getDisplayName());
        backButton.setImageResource(R.drawable.ic_back_wshadow);
    }

    @OnPageChange(value = R.id.artist_page_time_line, callback = OnPageChange.Callback.PAGE_SELECTED)
    protected void onPageSelected(int position) {
        mSwipe.setEnabled(position == 0);
        animatedUpArrow.setAnimation(null);
        animatedUpArrow.setVisibility(View.GONE);
    }

    private void addTimeLineView(ArtistPage artistPage) {
        artistPageTimeLineAdapter = new ArtistPageTimeLineAdapter(this, getSupportFragmentManager(), artistPage, bellButton);
        time_line_view_pager.setAdapter(artistPageTimeLineAdapter);
        time_line_view_pager.getLayoutParams().height = ScreenDensityUtil.getScreenWidthPixels(mContext, 1);
        mSwipe.getLayoutParams().height = ScreenDensityUtil.getScreenWidthPixels(mContext, 1);

        final Animation fadeOut = AnimationUtils.loadAnimation(mContext, R.anim.slide_up_and_fade);
        fadeOut.setDuration(1500);
        final Animation moveUp = AnimationUtils.loadAnimation(mContext, R.anim.slide_up_and_fade);
        moveUp.setDuration(1500);
        moveUp.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                animatedUpArrow.startAnimation(fadeOut);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        fadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                animatedUpArrow.startAnimation(moveUp);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        animatedUpArrow.startAnimation(moveUp);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mSession == null || !UserValidator.isValid(mSession.getUser())) {
            AndroidUtils.restartApp(mContext);
            return;
        }

        isActive = true;
        if (ArtistValidator.isValid(mArtist)) {
            mSession.setCurrentArtist(mArtist);
            shouldStopUpdatingPreviewChat = false;
            updatePreviewChat();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        isActive = false;
        shouldStopUpdatingPreviewChat = true;
        PicassoTools.clearCache(Picasso.with(mContext));
        new GlideClearCache().execute();
        Glide.get(mContext).clearMemory();
    }

    protected class GlideClearCache extends AsyncTask {
        @Override
        protected Object doInBackground(Object[] params) {
            Glide.get(mContext).clearDiskCache();
            return null;
        }
    }

    private void updatePreviewChat() {
        if (shouldStopUpdatingPreviewChat)
            return;

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!shouldStopUpdatingPreviewChat) {
                    requestChatMsgs(true);
                    updatePreviewChat();
                }
            }
        }, AppConstants.UPDATE_CHAT_INTERVAL);
    }

    public void performStreamClick(final Stream stream) {
        if (!StreamValidator.isValid(stream)) {
            LVUtils.handleError(getString(R.string.invalid_artist_object), false, touch_blocker, mContext, ArtistPageActivity.this);
            return;
        }

        touch_blocker.setVisibility(View.VISIBLE);
        APICalls.requestEnterStream(mSession, ArtistPageActivity.this, mContext, stream.getStreamId(), mRequestQueue, new EnterStreamHandler() {
            @Override
            public void RunCompleted(EnterStreamData enterStreamData) {
                if (isActive && enterStreamData != null) {
                    switch (enterStreamData.getCode()) {
                        case AppConstants.STREAMCODE_SUCCESS: // public
                            LVUtils.startVideoPlayer(stream, enterStreamData, touch_blocker, isActive, ArtistPageActivity.this);
                            break;
                        case AppConstants.STREAMCODE_NOT_SUBSCRIBED:
                        case AppConstants.STREAMCODE_IS_PRIVATE:
                            LVUtils.beginUserSubscriptionProcess(mArtist, ArtistPageActivity.this, mSession, mRequestQueue, touchBlockerHandler, new Runnable() {
                                @Override
                                public void run() {
                                    touch_blocker.setVisibility(View.GONE);
                                    mSession.setCurrentRunnable(null);
                                    mSubscriptionType = AppConstants.SUB_TYPE_HERO;
                                    performStreamClick(stream);
                                }
                            });
                            break;
                        case AppConstants.STREAMCODE_NOT_EXISTS:
                            LVUtils.handleError(getString(R.string.stream_unavailable), false, touch_blocker, mContext, ArtistPageActivity.this);
                            break;
                        case AppConstants.STREAMCODE_OTHER_ERROR:
                            LVUtils.handleError(getString(R.string.retry_later), false, touch_blocker, mContext, ArtistPageActivity.this);
                            break;
                        default:
                            LVUtils.handleError(getString(R.string.api_error), false, touch_blocker, mContext, ArtistPageActivity.this);
                            break;
                    }
                }
            }
        });
    }

    private void addPreviewChatView() {
        // Set Layout manager first
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        preview_chat_recycler_view.setLayoutManager(mLinearLayoutManager);

        // Preview chat data/view
        List<ArtistChatMessage> latestChatMessageList = mArtist.getPreviewChatMessageList();

        if (!ArtistChatMessageValidator.isValid(latestChatMessageList)) {
            latestChatMessageList = new ArrayList<>();
            latestChatMessageList.add(new ArtistChatMessage());
        }
        preview_chat_recycler_view.setHasFixedSize(true);
        chatPreviewAdapter = new ArtistChatPreviewAdapter(latestChatMessageList, preview_chat_recycler_view.getHeight(), new Runnable() {
            @Override
            public void run() {
                if (isActive)
                    onJoinChatClick();
            }
        });
        preview_chat_recycler_view.setAdapter(chatPreviewAdapter);
    }

    private void requestChatMsgs(final boolean isUpdating) {
        if (!isUpdating) {
            if (isRefreshing) mSwipe.setRefreshing(true);
            else touch_blocker.setVisibility(View.VISIBLE);
        }
        isDownloadingArtistChat = true;
        APICalls.requestChatMsgs(this, mContext, mArtist.getArtistId(), "0", AppConstants.PREVIEW_CHAT_LIMIT, mSession, mRequestQueue, new ChatMessagesHandler() {
            @Override
            public void onRunComplete(List<ArtistChatMessage> newChatMessages) {
                isDownloadingArtistChat = false;
                if (!isDownloadingArtistPage) {
                    isRefreshing = false;
                    mSwipe.setRefreshing(false);
                    touch_blocker.setVisibility(View.GONE);
                }

                if (!ArtistChatMessageValidator.isValid(newChatMessages)) {
                    if (!isUpdating) {
                        mArtist.setPreviewChatMessageList(newChatMessages);
                        addPreviewChatView();
                    }
                    return;
                }

                if (!isUpdating) {
                    mArtist.setPreviewChatMessageList(newChatMessages);
                    addPreviewChatView();
                } else if (ArtistValidator.isValid(mArtist) && ArtistChatMessageValidator.isValid(mArtist.getPreviewChatMessageList())) {
                    List<ArtistChatMessage> oldChatMessages = mArtist.getPreviewChatMessageList();
                    if (!oldChatMessages.equals(newChatMessages)) {
                        mArtist.setPreviewChatMessageList(newChatMessages);    // Update old chat view
                        addPreviewChatView();
                    }
                }
            }
        });
    }

    protected void onJoinChatClick() {
        if (!ArtistValidator.isValid(mArtist)) {
            LVUtils.handleError(getString(R.string.invalid_artist_object), true, touch_blocker, mContext, ArtistPageActivity.this);
            return;
        }

        if (mSession.getUser().getUserRole() == User.UserRole.GUEST) {
            LVUtils.displayGuestWatchDialog(ArtistPageActivity.this, null);
            return;
        }

        switch (mSubscriptionType) {
            case AppConstants.SUB_TYPE_HERO:
            case AppConstants.SUB_TYPE_FAV:
            case AppConstants.SUB_TYPE_HAS_MEMBERSHIP:
                launchChat();
                break;
            case AppConstants.SUB_TYPE_NONE:
            default:
                LVUtils.beginUserSubscriptionProcess(mArtist, ArtistPageActivity.this, mSession, mRequestQueue, touchBlockerHandler, new Runnable() {
                    @Override
                    public void run() {
                        mSession.setCurrentRunnable(null);
                        mSubscriptionType = AppConstants.SUB_TYPE_HERO;
                        launchChat();
                    }
                });
                break;
        }
    }

    private void launchChat() {
        if (isActive) {
            Intent intent = new Intent(ArtistPageActivity.this, ChatActivity.class);
            intent.putExtra(AppConstants.ARTIST_KEY_INTENT, mArtist);
            startActivity(intent);
        }
    }

    private void setSubscriptionType() {
        if (mArtistPage.getArtist().getIsHero() == 1) {
            mSubscriptionType = AppConstants.SUB_TYPE_HERO;
        } else if (mArtistPage.getArtist().getIsWatching() == 1) {
            mSubscriptionType = AppConstants.SUB_TYPE_FAV;
        } else if (mArtistPage.getUserInfo().getHasMembership() == 1) {
            mSubscriptionType = AppConstants.SUB_TYPE_HAS_MEMBERSHIP;    // broadcasters as well
        } else {
            mSubscriptionType = AppConstants.SUB_TYPE_NONE;
        }
    }

    @OnClick(R.id.header_left_card_view)
    protected void onBackIconClick() {
        GO_TO = getIntent().getIntExtra(AppConstants.ACTIVITY_KEY_INTENT, -1);
        onBackPressed();
    }

    public void onBackPressed() {
        if (isActive) {
            mSession.setCurrentArtist(null);
            Intent intent = new Intent();
            intent.putExtra(AppConstants.ACTIVITY_KEY_INTENT, GO_TO);
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    public void performStreamLongClick(final Stream stream) {
        if (!mSession.getUser().getUserID().equalsIgnoreCase(stream.getArtistId()))
            return;

        if (StreamValidator.isValid(stream)) {
            RemoveStreamDialog dialog = new RemoveStreamDialog(ArtistPageActivity.this, stream, touch_blocker, new Runnable() {
                @Override
                public void run() {
                    if (isActive) {
                        Intent intent = getIntent();
                        finish();
                        startActivity(intent);
                    }
                }
            });
            dialog.show();
        } else {
            LVUtils.handleError(getString(R.string.invalid_artist_object), false, touch_blocker, mContext, ArtistPageActivity.this);
        }
    }

    @Override
    public void onRefresh() {
        if (!isRefreshing && mSession != null) {
            isRefreshing = true;
            startArtistPageView();
        }
    }
}
