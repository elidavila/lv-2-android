package com.app.livideo.activities;

import android.app.Activity;
import android.content.Intent;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.app.livideo.R;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.Preferences;
import com.app.livideo.utils.ViewUtils;
import com.app.livideo.views.CameraCapturePreview;
import com.bumptech.glide.Glide;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

@SuppressWarnings("deprecation")
public class CameraActivity extends AppCompatActivity
        implements View.OnClickListener, SensorEventListener {
    private static final String LOGTAG = CameraActivity.class.getSimpleName();
    private Camera mCamera;
    private CameraCapturePreview mPreview;
    private Button mTakePictureButton, mSwitchCameraButton;
    private ImageView progressBar;
    private boolean cameraFront = false;
    private Activity mActivity;
    private int CONFIRM_PICTURE = 12;
    private int mOrientation = AppConstants.PORTRAIT;
    float mDist;
    private SensorManager mSensorManager;
    private Sensor mRotationSensor;
    private static final int SENSOR_DELAY = 500 * 1000; // 500ms
    private static final int FROM_RADS_TO_DEGS = -57;
    boolean isActive = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        mActivity = this;
        setContentView(R.layout.activity_camera);
        Preferences.setPreference(AppConstants.LAST_CAMERA_USED, getBackFacingCamera(), this);
        mPreview = new CameraCapturePreview(this, (SurfaceView) findViewById(R.id.surfaceView));
        mPreview.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        ((FrameLayout) findViewById(R.id.layout)).addView(mPreview);
        mPreview.setKeepScreenOn(true);
        mTakePictureButton = (Button) findViewById(R.id.button_take_photo);
        mTakePictureButton.setOnClickListener(this);
        mTakePictureButton.setOnTouchListener(ViewUtils.tl_animation);
        progressBar = (ImageView) findViewById(R.id.camera_progress_bar);
        mSwitchCameraButton = (Button) findViewById(R.id.button_switch_camera);
        mSwitchCameraButton.setOnClickListener(this);
        isActive = true;
        Glide.with(getApplicationContext()).load(R.drawable.loading_anim).asGif().into(progressBar);
        try {
            mSensorManager = (SensorManager) getSystemService(Activity.SENSOR_SERVICE);
            mRotationSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
            mSensorManager.registerListener(this, mRotationSensor, SENSOR_DELAY);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getFrontFacingCamera() {
        int cameraId = -1;
        // Search for the front facing camera
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                cameraId = i;
                break;
            }
        }
        return cameraId;
    }

    public int getBackFacingCamera() {
        int cameraId = -1;
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                cameraId = i;
                break;
            }
        }
        return cameraId;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isActive = true;
        int numCams = Camera.getNumberOfCameras();
        if (numCams > 0) {
            try {
                mCamera = Camera.open(Preferences.getPreference(AppConstants.LAST_CAMERA_USED, this, getFrontFacingCamera()));
                mCamera.startPreview();
                mPreview.resetCamera(mCamera);
            } catch (RuntimeException ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // Get the pointer ID
        Camera.Parameters params = mCamera.getParameters();
        int action = event.getAction();

        if (event.getPointerCount() > 1) {
            // handle multi-touch events
            if (action == MotionEvent.ACTION_POINTER_DOWN) {
                mDist = getFingerSpacing(event);
            } else if (action == MotionEvent.ACTION_MOVE && params.isZoomSupported()) {
                mCamera.cancelAutoFocus();
                handleZoom(event, params);
            }
        } else {
            // handle single touch events
            if (action == MotionEvent.ACTION_UP) {
                handleFocus(event, params);
            }
        }
        return true;
    }

    private void handleZoom(MotionEvent event, Camera.Parameters params) {
        int maxZoom = params.getMaxZoom();
        int zoom = params.getZoom();
        float newDist = getFingerSpacing(event);
        if (newDist > mDist) {
            //zoom in
            if (zoom < maxZoom)
                zoom++;
        } else if (newDist < mDist) {
            //zoom out
            if (zoom > 0)
                zoom--;
        }
        mDist = newDist;
        params.setZoom(zoom);
        mCamera.setParameters(params);
    }

    public void handleFocus(MotionEvent event, Camera.Parameters params) {
        int pointerId = event.getPointerId(0);
        int pointerIndex = event.findPointerIndex(pointerId);
        // Get the pointer's current position
        float x = event.getX(pointerIndex);
        float y = event.getY(pointerIndex);

        List<String> supportedFocusModes = params.getSupportedFocusModes();
        if (supportedFocusModes != null && supportedFocusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
            mCamera.autoFocus(new Camera.AutoFocusCallback() {
                @Override
                public void onAutoFocus(boolean b, Camera camera) {
                    // currently set to auto-focus on single touch
                }
            });
        }
    }

    /**
     * Determine the space between the first two fingers
     */
    private float getFingerSpacing(MotionEvent event) {
        // ...
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return (float) Math.sqrt(x * x + y * y);
    }

    @Override
    protected void onPause() {
        isActive = false;
        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
        }
        super.onPause();
    }

    private void resetCam() {
        mCamera.startPreview();
        mPreview.resetCamera(mCamera);
    }

    private void refreshGallery(File file) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(Uri.fromFile(file));
        sendBroadcast(mediaScanIntent);
    }

//    Camera.ShutterCallback shutterCallback = new Camera.ShutterCallback() {
//        public void onShutter() {
//        }
//    };
//
//    Camera.PictureCallback rawCallback = new Camera.PictureCallback() {
//        public void onPictureTaken(byte[] data, Camera camera) {
//        }
//    };

    Camera.PictureCallback jpegCallback = new Camera.PictureCallback() {
        public void onPictureTaken(byte[] data, Camera camera) {
            new SaveImageTask().execute(data);
            resetCam();
            Log.d(LOGTAG, "onPictureTaken - jpeg");
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CONFIRM_PICTURE) {
            switch (resultCode) {
                case RESULT_OK:
                    if (getParent() == null) {
                        setResult(Activity.RESULT_OK, data);
                    } else {
                        getParent().setResult(Activity.RESULT_OK, data);
                    }
                    finish();
                    break;
                case RESULT_CANCELED:
                    mTakePictureButton.setEnabled(true);
                    progressBar.setVisibility(View.GONE);
                    break;
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_take_photo:
                System.gc();
                mCamera.takePicture(null, null, jpegCallback);
                mTakePictureButton.setEnabled(false);
                progressBar.setVisibility(View.VISIBLE);
                break;
            case R.id.button_switch_camera:
                int numberOfCameras = Camera.getNumberOfCameras();
                if (numberOfCameras > 1) {
                    if (mCamera != null) {
                        mCamera.release();
                        mCamera = null;
                    }
                    switchCamera();
                }
                break;
        }
    }

    public void switchCamera() {
        if (cameraFront) {
            int cameraId = getBackFacingCamera();
            cameraFront = false;
            if (cameraId >= 0) {
                mCamera = Camera.open(cameraId);
                mCamera.startPreview();
                mPreview.resetCamera(mCamera);
                Preferences.setPreference(AppConstants.LAST_CAMERA_USED, cameraId, this);
            }
        } else {
            int cameraId = getFrontFacingCamera();
            cameraFront = true;
            if (cameraId >= 0) {
                mCamera = Camera.open(cameraId);
                mCamera.startPreview();
                mPreview.resetCamera(mCamera);
                Preferences.setPreference(AppConstants.LAST_CAMERA_USED, cameraId, this);
            }
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor == mRotationSensor) {
            if (event.values.length > 4) {
                float[] truncatedRotationVector = new float[4];
                System.arraycopy(event.values, 0, truncatedRotationVector, 0, 4);
                update(truncatedRotationVector);
            } else {
                update(event.values);
            }
        }
    }

    private void update(float[] vectors) {
        float[] rotationMatrix = new float[9];
        SensorManager.getRotationMatrixFromVector(rotationMatrix, vectors);
        int worldAxisX = SensorManager.AXIS_X;
        int worldAxisZ = SensorManager.AXIS_Z;
        float[] adjustedRotationMatrix = new float[9];
        SensorManager.remapCoordinateSystem(rotationMatrix, worldAxisX, worldAxisZ, adjustedRotationMatrix);
        float[] orientation = new float[3];
        SensorManager.getOrientation(adjustedRotationMatrix, orientation);
        float roll = orientation[2] * FROM_RADS_TO_DEGS;

        if (roll <= 100 && roll >= 80) {
            mOrientation = AppConstants.LANDSCAPE;
            mSwitchCameraButton.setRotation(90);
        } else if (roll >= -100 && roll <= -80) {
            mOrientation = AppConstants.REVERSE_LAND;
            mSwitchCameraButton.setRotation(-90);
        } else {
            mOrientation = AppConstants.PORTRAIT;
            mSwitchCameraButton.setRotation(0);
        }
    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    private class SaveImageTask extends AsyncTask<byte[], Void, Void> {
        Uri returnUri;
        File returnFile;

        @Override
        protected Void doInBackground(byte[]... data) {
            FileOutputStream outStream;

            // Write to SD Card
            try {
                File sdCard = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
                File dir = new File(sdCard.getAbsolutePath() + "/LiVideo");
                dir.mkdirs();

                String fileName = String.format("%d.jpg", System.currentTimeMillis());
                File outFile = new File(dir, fileName);
                outStream = new FileOutputStream(outFile);
                outStream.write(data[0]);
                outStream.flush();
                outStream.close();

                Log.d(LOGTAG, "onPictureTaken - wrote bytes: " + data.length + " to " + outFile.getAbsolutePath());

                returnUri = Uri.fromFile(outFile);
                returnFile = outFile;
                refreshGallery(outFile);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (isActive) {
                    Intent intent = new Intent(mActivity, ConfirmImageActivity.class);
                    intent.setData(returnUri);
                    intent.putExtra(AppConstants.TAKEN_PHOTE, returnFile);
                    intent.putExtra(AppConstants.TAKEN_ORIENTATION, mOrientation);
                    startActivityForResult(intent, CONFIRM_PICTURE);
                }
            }
            return null;
        }
    }
}
