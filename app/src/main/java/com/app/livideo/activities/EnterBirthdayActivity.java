package com.app.livideo.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.app.livideo.R;
import com.app.livideo.api.APICalls;
import com.app.livideo.dialogs.CustomAlertDialog;
import com.app.livideo.interfaces.RegistrationHandler;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.User;
import com.app.livideo.utils.AndroidUtils;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.Constants;
import com.app.livideo.utils.Preferences;
import com.app.livideo.utils.ViewUtils;
import com.bumptech.glide.Glide;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EnterBirthdayActivity extends AppCompatActivity {

    @Inject RequestQueue mRequestQueue;
    @Inject LVSession mSession;
    @Inject Context mContext;

    @Bind(R.id.header_left_icon) ImageView header_left_icon;
    @Bind(R.id.header_title) TextView header_title;
    @Bind(R.id.enter_bday_number_picker) NumberPicker number_picker_view;
    @Bind(R.id.enter_bday_male_card_view) CardView male_card_view;
    @Bind(R.id.enter_bday_female_card_view) CardView female_card_view;
    @Bind(R.id.enter_bday_action_button) CardView next_card_view;
    @Bind(R.id.enter_bday_progress_bar) FrameLayout touch_blocker;
    @Bind(R.id.enter_bday_icon) ImageView bday_img_view;
    @Bind(R.id.progress_bar) ImageView mProgressBar;

    private static final int MIN_AGE_GROUP = 7;
    private static final int MAX_AGE_GROUP = 55;
    private boolean isMaleSelected, isFemaleSelected, isViewer, isActive = true;
    private String[] mAgeRanges;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_birthday);
        ButterKnife.bind(this);
        LVInject.inject(this);
        initialize();
        initNumberPicker();
    }

    private void initialize() {
        // Header
        header_title.setText(R.string.awesome_password);
        header_left_icon.setImageResource(R.drawable.ic_back_wshadow);
        Glide.with(mContext).load(R.drawable.loading_anim).asGif().into(mProgressBar);

        // Variables
        isViewer = getIntent().getBooleanExtra(AppConstants.IS_USER_INTENT, true);
        touch_blocker.setOnTouchListener(AppConstants.touchListner);
        isActive = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isActive = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActive = false;
    }

    @OnClick(R.id.enter_bday_action_button)
    protected void onNextClick() {
        // Check if user is too young
        if (number_picker_view.getValue() == 0) {
            CustomAlertDialog dialog = new CustomAlertDialog(this, getString(R.string.not_eligible_for_livideo), false);
            dialog.show();
            return;
        }

        // Check if user selected gender
        if (!isMaleSelected && !isFemaleSelected) {
            CustomAlertDialog dialog = new CustomAlertDialog(this, getString(R.string.you_need_to_select_a_gender), false);
            dialog.show();
            return;
        }

        // Add new intent extras
        if (isMaleSelected) getIntent().putExtra(AppConstants.GENDER_KEY_INTENT, "male");
        else getIntent().putExtra(AppConstants.GENDER_KEY_INTENT, "female");
        getIntent().putExtra(AppConstants.BIRTHDAY_KEY_INTENT, getBirthdayFromNumberPicker());

        if (isViewer) {
            requestRegisterViewer();
        } else {
            // Broadcaster
            if (isActive) {
                Intent intent = new Intent(EnterBirthdayActivity.this, EnterCategoryActivity.class);
                intent.putExtra(AppConstants.IS_USER_INTENT, isViewer);
                intent.putExtra(AppConstants.SOCIAL_ID_KEY_INTENT, getIntent().getStringExtra(AppConstants.SOCIAL_ID_KEY_INTENT));
                intent.putExtra(AppConstants.SOCIAL_TYPE_KEY_INTENT, getIntent().getStringExtra(AppConstants.SOCIAL_TYPE_KEY_INTENT));
                intent.putExtra(AppConstants.USER_ID_KEY_INTENT, getIntent().getStringExtra(AppConstants.USER_ID_KEY_INTENT));
                intent.putExtra(AppConstants.DEVICE_ID_KEY_INTENT, getIntent().getStringExtra(AppConstants.DEVICE_ID_KEY_INTENT));
                intent.putExtra(AppConstants.PHONE_NUMBER_KEY_INTENT, getIntent().getStringExtra(AppConstants.PHONE_NUMBER_KEY_INTENT));
                intent.putExtra(AppConstants.ACTIVITY_KEY_INTENT, getIntent().getStringExtra(AppConstants.ACTIVITY_KEY_INTENT));
                intent.putExtra(AppConstants.EMAIL_KEY_INTENT, getIntent().getStringExtra(AppConstants.EMAIL_KEY_INTENT));
                intent.putExtra(AppConstants.DISPLAY_NAME_KEY_INTENT, getIntent().getStringExtra(AppConstants.DISPLAY_NAME_KEY_INTENT));
                intent.putExtra(AppConstants.PASSWORD_KEY_INTENT, getIntent().getStringExtra(AppConstants.PASSWORD_KEY_INTENT));
                intent.putExtra(AppConstants.GENDER_KEY_INTENT, getIntent().getStringExtra(AppConstants.GENDER_KEY_INTENT));
                intent.putExtra(AppConstants.BIRTHDAY_KEY_INTENT, getIntent().getStringExtra(AppConstants.BIRTHDAY_KEY_INTENT));
                startActivity(intent);
            }
        }
    }

    private void requestRegisterViewer() {
        touch_blocker.setVisibility(View.VISIBLE);
        APICalls.requestEmailRegistration(EnterBirthdayActivity.this, mContext, getIntent(), mSession, mRequestQueue, new RegistrationHandler() {
            @Override
            public void RunCompleted(int RegistrationCode, String userID) {
                touch_blocker.setVisibility(View.GONE);
                switch (RegistrationCode) {
                    case Constants.REGISTRATIONCODE_SUCCESS:
                        SaveUserData(userID);
                        if (isActive) {
                            startActivity(new Intent(EnterBirthdayActivity.this, EnterProfilePictureActivity.class));
                        }
                        break;
                    case Constants.REGISTRATIONCODE_ALREADY_EXISTS:
                        AndroidUtils.showLongToast(getString(R.string.regestration_exists), EnterBirthdayActivity.this);
                        break;
                    case Constants.REGISTRATIONCODE_OTHER_ERROR:
                        AndroidUtils.showLongToast(getString(R.string.retry_later), EnterBirthdayActivity.this);
                        break;
                    default:
                        AndroidUtils.showLongToast(getString(R.string.retry_later), EnterBirthdayActivity.this);
                        break;
                }
            }
        });
    }

    NumberPicker.OnScrollListener ageValidator = new NumberPicker.OnScrollListener() {
        @Override
        public void onScrollStateChange(NumberPicker view, int scrollState) {
            switch (scrollState) {
                case NumberPicker.OnScrollListener.SCROLL_STATE_IDLE:
                    if (number_picker_view.getValue() == 0) {
                        bday_img_view.setImageResource(R.drawable.ic_cake_off);
                    } else {
                        bday_img_view.setImageResource(R.drawable.ic_cake_on);
                    }
                    break;
            }
        }
    };

    private void SaveUserData(final String newUserID) {
        if (isViewer) {
            // Viewer
            Preferences.setPreference(Constants.USER_TYPE_KEY, Constants.VIEWER_USER, mContext);
            mSession.getUser().setCurrentRole(User.UserRole.VIEWER);
        } else {
            // Artist
            Preferences.setPreference(Constants.USER_TYPE_KEY, Constants.BROADCASTER_USER, mContext);
            mSession.getUser().setCurrentRole(User.UserRole.BROADCASTER);

            Preferences.setPreference(Constants.KEY_SOCIAL_ID, getIntent().getStringExtra(AppConstants.SOCIAL_ID_KEY_INTENT), mContext);
            mSession.getUser().setSocialID(getIntent().getStringExtra(AppConstants.SOCIAL_ID_KEY_INTENT));

            Preferences.setPreference(Constants.KEY_SOCIAL_TYPE, getIntent().getStringExtra(AppConstants.SOCIAL_TYPE_KEY_INTENT), mContext);
            mSession.getUser().setSocialType(getIntent().getStringExtra(AppConstants.SOCIAL_TYPE_KEY_INTENT));
        }

        // Both
        Preferences.setPreference(Constants.KEY_USER_ID, newUserID, mContext);
        mSession.getUser().setUserID(newUserID);

        Preferences.setPreference(Constants.KEY_DEVICE, getIntent().getStringExtra(AppConstants.DEVICE_ID_KEY_INTENT), mContext);
        mSession.getUser().setDeviceID(getIntent().getStringExtra(AppConstants.DEVICE_ID_KEY_INTENT));

        Preferences.setPreference(Constants.KEY_PHONE, getIntent().getStringExtra(AppConstants.PHONE_NUMBER_KEY_INTENT), mContext);

        Preferences.setPreference(Constants.KEY_DISPLAY_NAME, getIntent().getStringExtra(AppConstants.DISPLAY_NAME_KEY_INTENT), mContext);
        mSession.getUser().setDisplayName(getIntent().getStringExtra(AppConstants.DISPLAY_NAME_KEY_INTENT));

        Preferences.setPreference(Constants.KEY_BIRTHDATE, getIntent().getStringExtra(AppConstants.BIRTHDAY_KEY_INTENT), mContext);
        mSession.getUser().setBirthdate(getIntent().getStringExtra(AppConstants.BIRTHDAY_KEY_INTENT));

        Preferences.setPreference(Constants.KEY_EMAIL, getIntent().getStringExtra(AppConstants.EMAIL_KEY_INTENT), mContext);
        mSession.getUser().setEmail(getIntent().getStringExtra(AppConstants.EMAIL_KEY_INTENT));

        Preferences.setPreference(Constants.KEY_PW, getIntent().getStringExtra(AppConstants.PASSWORD_KEY_INTENT), mContext);
    }

    private String getBirthdayFromNumberPicker() {
        int age;
        String mAgeRange = mAgeRanges[number_picker_view.getValue()];
        if (mAgeRange.contains(String.valueOf(MAX_AGE_GROUP))) {
            age = MAX_AGE_GROUP;
        } else {
            age = Integer.valueOf(mAgeRange.split(" - ")[0]) + 2;
        }

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.getInstance().get(Calendar.YEAR) - age, 5, 1);

        Date _date = new Date();
        _date.setTime(calendar.getTime().getTime());

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return format.format(_date);
    }

    private void initNumberPicker() {
        mAgeRanges = getViewerAgeRanges();
        number_picker_view.setMinValue(0);
        number_picker_view.setValue((mAgeRanges.length - 1) / 2);
        number_picker_view.setMaxValue(mAgeRanges.length - 1);
        number_picker_view.setWrapSelectorWheel(false);
        number_picker_view.setDisplayedValues(mAgeRanges);
        number_picker_view.setOnScrollListener(ageValidator);
        ViewUtils.modifyPickerDivider(number_picker_view, Color.BLACK, 0);
        ViewUtils.setNumberPickerTextColor(number_picker_view, Color.BLACK);
    }

    public String[] getViewerAgeRanges() {
        ArrayList<String> ageRangeList = new ArrayList<>();
        for (int i = MIN_AGE_GROUP; i < MAX_AGE_GROUP; i += 6)
            ageRangeList.add(String.format(Locale.getDefault(), "%d - %d", i, i + 5));

        ageRangeList.add("55+");
        return ageRangeList.toArray(new String[ageRangeList.size()]);
    }

    @OnClick(R.id.enter_bday_male_card_view)
    protected void onMaleClick() {
        // Handle Male Click
        if (isMaleSelected) {
            isMaleSelected = false;
            male_card_view.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.livideo_grey));
            next_card_view.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.livideo_grey));
        } else {
            isMaleSelected = true;
            male_card_view.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.chat_blue));
            if(isViewer){
                next_card_view.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.livideo_dark_pink));
            } else {
                next_card_view.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.livideo_teal_primary));
            }
        }

        // Handle Female default value
        female_card_view.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.livideo_grey));
        isFemaleSelected = false;
    }

    @OnClick(R.id.enter_bday_female_card_view)
    protected void onFemaleClick() {
        // Handle Female Click
        if (isFemaleSelected) {
            isFemaleSelected = false;
            female_card_view.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.livideo_grey));
            next_card_view.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.livideo_grey));
        } else {
            isFemaleSelected = true;
            female_card_view.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.livideo_light_pink));
            if(isViewer){
                next_card_view.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.livideo_dark_pink));
            } else {
                next_card_view.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.livideo_teal_primary));
            }
        }

        // Handle Male default value
        male_card_view.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.livideo_grey));
        isMaleSelected = false;
    }

    @OnClick(R.id.header_left_card_view)
    protected void onBackClick() {
        finish();
    }
}
