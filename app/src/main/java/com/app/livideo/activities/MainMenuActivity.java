package com.app.livideo.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.app.livideo.R;
import com.app.livideo.api.APICalls;
import com.app.livideo.dialogs.GuestWatchDialog;
import com.app.livideo.fragments.BlockedUsersFragment;
import com.app.livideo.fragments.DiscoverFragment;
import com.app.livideo.fragments.MePageFragment;
import com.app.livideo.fragments.SettingsPageOneFragment;
import com.app.livideo.fragments.SettingsPageTwoFragment;
import com.app.livideo.fragments.ViewerSettingsFragment;
import com.app.livideo.interfaces.ActiveStreamHandler;
import com.app.livideo.interfaces.ArtistHandler;
import com.app.livideo.interfaces.EnterStreamHandler;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.Artist;
import com.app.livideo.models.EnterStreamData;
import com.app.livideo.models.Stream;
import com.app.livideo.models.User;
import com.app.livideo.models.validators.ArtistValidator;
import com.app.livideo.models.validators.UserValidator;
import com.app.livideo.notifications.GCMListenerService;
import com.app.livideo.utils.AndroidUtils;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.FileUtils;
import com.app.livideo.utils.Preferences;
import com.app.livideo.utils.TextUtils;
import com.bumptech.glide.Glide;

import java.io.File;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainMenuActivity extends AppCompatActivity {

    @Bind(R.id.main_menu_loader) public View loader_view;
    @Bind(R.id.drawer_root_layout) DrawerLayout mDrawer;
    @Bind(R.id.main_menu_progress_bar) ImageView mProgressBar;
    @Inject Context mContext;
    @Inject LVSession mSession;
    @Inject RequestQueue mRequestQueue;

    Fragment mDiscoverFragment;
    Fragment mMeFragment;
    Fragment mSettingsPageOne;
    Fragment mSettingsPageTwo;
    Fragment mBlockedUsersPage;

    private final String STREAM = "o";
    private final String ARTIST = "d";


    boolean fromReCache = false, isActive = true, shouldExit = false, isNotificationFinished = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_main_menu);
        ButterKnife.bind(this);
        LVInject.inject(this);
        if (mSession == null || !UserValidator.isValid(mSession.getUser())) {
            AndroidUtils.restartApp(mContext);
            return;   // APP ERROR
        }
        startMainMenuView();      // WE GOOD HERE (most hit ratio)
    }

    private void handleNotificationAction() {
        if (getIntent() != null && !TextUtils.isNullOrEmpty(getIntent().getStringExtra(AppConstants.KEY_NOTIFICATION_TYPE))) {
            mDrawer.setVisibility(View.INVISIBLE);
            String notificationType = getIntent().getStringExtra(AppConstants.KEY_NOTIFICATION_TYPE);
            if (notificationType.equalsIgnoreCase(GCMListenerService.NOTIFICATION_NEW_STREAM)) {
                doStreamAction();
            } else if (notificationType.equalsIgnoreCase(GCMListenerService.NOTIFICATION_NEW_MESSAGE)) {
                doMsgAction();
            } else if (notificationType.equalsIgnoreCase(GCMListenerService.NOTIFICATION_NEW_CHAT_MESSAGE)
                    || notificationType.equalsIgnoreCase(GCMListenerService.NOTIFICATION_ENTERED_CHAT)) {
                doChatAction();
            }

            if (!getIntent().getBooleanExtra(AppConstants.KEY_FROM_RE_CACHE, false)) {
                finish();
            }
        }
        Intent intent = getIntent();
        Uri data = intent.getData();
        if(data != null){
            String uriString = data.toString();
            String[] breackUp = uriString.split("/");
            int place = -1;
            for (int x = 0; x < breackUp.length; x++) {
                if (breackUp[x].length() == 1) {
                    place = x;
                    break;
                }
            }

            if (breackUp[place].equalsIgnoreCase(ARTIST)) {
                APICalls.requestArtistByURI(this, mContext, mSession, mRequestQueue, data, new ArtistHandler() {
                    @Override
                    public void RunCompleted(boolean isAvailable, Artist artist) {
                        if (ArtistValidator.isValid(artist)) {
                            Intent intent = new Intent(MainMenuActivity.this, ArtistPageActivity.class);
                            intent.putExtra(AppConstants.ACTIVITY_KEY_INTENT, AppConstants.CHANNELS);
                            intent.putExtra(AppConstants.ARTIST_KEY_INTENT, artist);
                            startActivity(intent);
                        }
                    }
                });
            } else if (breackUp[place].equalsIgnoreCase(STREAM)) {
                APICalls.requestStreamByURI(this, mContext, mSession, mRequestQueue, data, new ActiveStreamHandler() {
                    @Override
                    public void RunCompleted(boolean isAvailable, final Stream mStream) {
                        if (!TextUtils.isNullOrEmpty(mStream.getStreamId())) {
                            loader_view.setVisibility(View.VISIBLE);
                            APICalls.requestStreamInfo(null, mContext, mStream.getStreamId(), mSession, mRequestQueue, new ActiveStreamHandler() {
                                @Override
                                public void RunCompleted(boolean isAvailable, final Stream stream) {
                                    if (isAvailable) {
                                        APICalls.requestEnterStream(mSession, null, mContext, mStream.getStreamId(), mRequestQueue, new EnterStreamHandler() {
                                            @Override
                                            public void RunCompleted(EnterStreamData enterStreamData) {
                                                loader_view.setVisibility(View.GONE);
                                                if (enterStreamData != null) {
                                                    switch (enterStreamData.getCode()) {
                                                        case AppConstants.STREAMCODE_SUCCESS: // public
                                                            isNotificationFinished = true;
                                                            stream.setUrl(enterStreamData.getUrl());
                                                            stream.setWatchingId(enterStreamData.getWatchingId());
                                                            stream.setDidStar(enterStreamData.isDidStar());
                                                            Intent newStreamIntent = new Intent(mContext, PlayerActivity.class);
                                                            newStreamIntent.putExtra(AppConstants.ACTIVE_STREAM_KEY_INTENT, stream);
                                                            startActivity(newStreamIntent);
                                                            break;
                                                    }
                                                }
                                            }
                                        });
                                    } else {
                                        loader_view.setVisibility(View.GONE);
                                        AndroidUtils.showShortToast(getString(R.string.stream_unavailable), MainMenuActivity.this);
                                    }
                                }
                            });
                        }
                    }
                });
            }
        }
    }

    private void doChatAction() {
        String artistId = getIntent().getStringExtra(AppConstants.KEY_ARTIST_ID);
        if (!TextUtils.isNullOrEmpty(artistId)) {
            loader_view.setVisibility(View.VISIBLE);
            APICalls.requestArtistInfo(artistId, null, mContext, mSession, mRequestQueue, new ArtistHandler() {
                @Override
                public void RunCompleted(boolean isAvailable, Artist mArtist) {
                    loader_view.setVisibility(View.GONE);
                    if (isAvailable) {
                        isNotificationFinished = true;
                        Intent chatIntent = new Intent(mContext, ChatActivity.class);
                        chatIntent.putExtra(AppConstants.ARTIST_KEY_INTENT, mArtist);
                        startActivity(chatIntent);
                    } else {
                        AndroidUtils.showShortToast(getString(R.string.invalid_artist_object), MainMenuActivity.this);
                    }
                }
            });
        }
    }

    private void doMsgAction() {
        isNotificationFinished = true;
        Intent intent = new Intent(this, PopUpActivity.class);
        intent.putExtras(getIntent());
        startActivity(intent);
    }

    private void doStreamAction() {
        final String streamID = getIntent().getStringExtra(AppConstants.KEY_STREAM_ID);
        if (!TextUtils.isNullOrEmpty(streamID)) {
            loader_view.setVisibility(View.VISIBLE);
            APICalls.requestStreamInfo(null, mContext, streamID, mSession, mRequestQueue, new ActiveStreamHandler() {
                @Override
                public void RunCompleted(boolean isAvailable, final Stream stream) {
                    if (isAvailable) {
                        APICalls.requestEnterStream(mSession, null, mContext, streamID, mRequestQueue, new EnterStreamHandler() {
                            @Override
                            public void RunCompleted(EnterStreamData enterStreamData) {
                                loader_view.setVisibility(View.GONE);
                                if (enterStreamData != null) {
                                    switch (enterStreamData.getCode()) {
                                        case AppConstants.STREAMCODE_SUCCESS: // public
                                            isNotificationFinished = true;
                                            stream.setUrl(enterStreamData.getUrl());
                                            stream.setWatchingId(enterStreamData.getWatchingId());
                                            stream.setDidStar(enterStreamData.isDidStar());
                                            Intent newStreamIntent = new Intent(mContext, PlayerActivity.class);
                                            newStreamIntent.putExtra(AppConstants.ACTIVE_STREAM_KEY_INTENT, stream);
                                            startActivity(newStreamIntent);
                                            break;
                                    }
                                }
                            }
                        });
                    } else {
                        loader_view.setVisibility(View.GONE);
                        AndroidUtils.showShortToast(getString(R.string.stream_unavailable), MainMenuActivity.this);
                    }
                }
            });
        }
    }

    private void startMainMenuView() {
        loader_view.setOnTouchListener(AppConstants.touchListner);
        Glide.with(mContext).load(R.drawable.loading_anim).asGif().into(mProgressBar);
        isActive = true;
        mDiscoverFragment = new DiscoverFragment();
        mMeFragment = new MePageFragment();
        mBlockedUsersPage = new BlockedUsersFragment();
        if (mSession.getUser().getUserRole() == User.UserRole.BROADCASTER) {
            mSettingsPageOne = new SettingsPageOneFragment();
            mSettingsPageTwo = new SettingsPageTwoFragment();
        } else {
            mSettingsPageOne = new ViewerSettingsFragment();
        }
        switchToDiscover();
        handleNotificationAction();
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActive = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mSession == null || !UserValidator.isValid(mSession.getUser())) {
            AndroidUtils.restartApp(mContext);
            return; // App error
        }

        isActive = true;
        displaySettingsPageOne();

        if (isNotificationFinished) {
            isNotificationFinished = false;
            mDrawer.setVisibility(View.VISIBLE);
            getIntent().removeExtra(AppConstants.KEY_NOTIFICATION_TYPE);
            displayDiscoverPage();
        }
    }

    public void openDrawer() {
        mDrawer.openDrawer(GravityCompat.START);
    }

    public void displayDiscoverPage() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(R.id.frame_container, new DiscoverFragment());
        ft.commitAllowingStateLoss();
    }

    public void displaySettingsPageOne() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(R.id.settings_container, mSettingsPageOne);
        ft.commitAllowingStateLoss();
    }

    public void displaySettingsPageTwo() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(R.id.settings_container, mSettingsPageTwo);
        ft.commitAllowingStateLoss();
    }

    public void startSearch() {
        if (isActive)
            startActivity(new Intent(this, SearchArtistActivity.class));
    }

    public void switchToDiscover() {
        if (mDiscoverFragment == null) {
            mDiscoverFragment = new DiscoverFragment();
        }

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if (mDiscoverFragment.isAdded()) {
            ft.show(mDiscoverFragment);
        } else {
            ft.addToBackStack("discover");
            ft.replace(R.id.frame_container, mDiscoverFragment);
        }
        ft.commit();
    }

    public void switchToMe() {
        if (mMeFragment == null) {
            mMeFragment = new MePageFragment();
        }

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if (mMeFragment.isAdded()) {
            ft.show(mMeFragment);
        } else {
            ft.addToBackStack("me");
            ft.replace(R.id.frame_container, mMeFragment);
        }
        ft.commit();
    }

    @Override
    public void onBackPressed() {
        if (shouldExit) {
            finish();
            android.os.Process.killProcess(android.os.Process.myPid());
            return;
        }

        this.shouldExit = true;
        Toast.makeText(this, getString(R.string.tap_again_to_exit), Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                shouldExit = false;
            }
        }, 2000);
    }

    public void launchRecording() {
        if (isActive) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                AndroidUtils.requestCameraPermission(MainMenuActivity.this, mDrawer);
            } else {
                if (mSession.getUser().getUserRole() == User.UserRole.BROADCASTER) {
                    Intent i = new Intent(this, ArtistRecordActivity.class);
                    i.putExtra(AppConstants.FROM_SPLASH, false);
                    startActivity(i);
                } else if (mSession.getUser().getUserRole() == User.UserRole.VIEWER) {
                    Intent i = new Intent(this, VideoRecordActivity.class);
                    Bundle mArgs = new Bundle();
                    mArgs.putInt(ArtistRecordActivity.RECORDING_TYPE, ArtistRecordActivity.RECORD_TYPE_OPEN_MIC);
                    i.putExtra(ArtistRecordActivity.RECORD_BUNDLE, mArgs);
                    i.putExtra(AppConstants.FROM_SPLASH, false);
                    startActivity(i);
                } else {
                    GuestWatchDialog dialog = new GuestWatchDialog(this);
                    dialog.show();
                }
            }
        }
    }

    public void startArtistChat(Artist mArtist) {
        if (isActive) {
            Intent chatIntent = new Intent(mContext, ChatActivity.class);
            chatIntent.putExtra(AppConstants.ARTIST_KEY_INTENT, mArtist);
            startActivity(chatIntent);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Intent intent = new Intent(this, CropImageActivity.class);
        switch (resultCode) {
            case Activity.RESULT_OK:
                switch (requestCode) {
                    case AppConstants.SELECT_PICTURE:
                        intent.putExtra(AppConstants.IMAGE_PATH_KEY, FileUtils.getFilePath(data.getData(), mContext));
                        startActivityForResult(intent, AppConstants.CROPPED_PICTURE);
                        break;
                    case AppConstants.CROPPED_PICTURE:
                        Preferences.setPreference(AppConstants.KEY_HAS_UPLOADED_PROFILE_PIC, true, mContext);
                        if (mSession.getUser().getUserRole() == User.UserRole.BROADCASTER) {
                            ((SettingsPageTwoFragment) mSettingsPageTwo).setDisplayImage();
                        } else {
                            ((ViewerSettingsFragment) mSettingsPageOne).setDisplayImage();
                        }
                        break;
                    case AppConstants.TAKE_PICTURE:
                        File receivedFile = (File) data.getSerializableExtra(AppConstants.TAKEN_PHOTE);
                        intent.putExtra(AppConstants.IMAGE_PATH_KEY, receivedFile.getAbsolutePath());
                        startActivityForResult(intent, AppConstants.CROPPED_PICTURE);
                        break;
                }
        }
    }

    public void startArtistPage(Artist artist) {
        if (isActive) {
            Intent intent = new Intent(mContext, ArtistPageActivity.class);
            intent.putExtra(AppConstants.ACTIVITY_KEY_INTENT, AppConstants.CHANNELS);
            intent.putExtra(AppConstants.ARTIST_KEY_INTENT, artist);
            startActivityForResult(intent, AppConstants.CHANNELS);
        }
    }

    public void displayBlockedUsers() {
        if (mBlockedUsersPage == null) {
            mBlockedUsersPage = new BlockedUsersFragment();
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(R.id.settings_container, mBlockedUsersPage);
        ft.commitAllowingStateLoss();
    }
}
