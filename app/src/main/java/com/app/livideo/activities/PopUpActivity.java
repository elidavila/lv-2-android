package com.app.livideo.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.app.livideo.R;
import com.app.livideo.dialogs.CustomAlertDialog;
import com.app.livideo.dialogs.CustomTitleAlertDialog;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.validators.UserValidator;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.TextUtils;

import javax.inject.Inject;

public class PopUpActivity extends AppCompatActivity {

    @Inject LVSession mSession;
    @Inject Context mContext;
    boolean isActive = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LVInject.inject(this);
        setContentView(R.layout.activity_pop_up);
        isActive = true;
        String dialogMessage = getIntent().getStringExtra(AppConstants.KEY_NOTIFICATION_MSG);
        if (!TextUtils.isNullOrEmpty(dialogMessage)) {
            boolean isCustomAlertTitleView = getIntent().getBooleanExtra(AppConstants.IS_TITLE_VIEW_KEY, false);
            if (isCustomAlertTitleView) {
                String dialogTitle = getIntent().getStringExtra(AppConstants.KEY_NOTIFICATION_TITLE);
                CustomTitleAlertDialog dialog = new CustomTitleAlertDialog(this, dialogTitle, dialogMessage, true);
                dialog.show();
                if (!UserValidator.isValid(mSession.getUser())) {
                    // User was out of app
                    dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            if (isActive)
                                startActivity(new Intent(PopUpActivity.this, SplashActivity.class));
                            finish();
                        }
                    });
                } else {
                    dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            finish();   // User was in the app
                        }
                    });
                }
            } else {
                CustomAlertDialog dialog = new CustomAlertDialog(this, dialogMessage, true);
                dialog.show();
                if (!UserValidator.isValid(mSession.getUser())) {
                    // User was out of app
                    dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            if (isActive)
                                startActivity(new Intent(PopUpActivity.this, SplashActivity.class));
                            finish();
                        }
                    });
                } else {
                    dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            finish();   // User was in the app
                        }
                    });
                }
            }

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isActive = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActive = false;
        finish();
    }
}
