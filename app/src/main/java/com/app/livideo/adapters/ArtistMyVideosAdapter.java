package com.app.livideo.adapters;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.livideo.R;
import com.app.livideo.activities.SplashActivity;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.models.Stream;
import com.app.livideo.models.validators.StreamValidator;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.ScreenDensityUtil;
import com.app.livideo.utils.UrlBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by MWheeler on 10/27/2015.
 * Edit: Eli Davila on 1/6/2016
 */
public class ArtistMyVideosAdapter extends RecyclerView.Adapter<ArtistMyVideosAdapter.ViewHolder> {

    @Inject Context mContext;
    private List<Stream> mMyBroadcasts;
    private OnBroadcastClickedListener mOnBroadcastClickedListener;
    private OnBroadcastLongClickedListener mOnBroadcastLongClickedListener;
    int mPadding;

    public ArtistMyVideosAdapter(List<Stream> broadcasts, OnBroadcastClickedListener listener, OnBroadcastLongClickedListener longClickedListener) {
        LVInject.inject(this);
        this.mMyBroadcasts = broadcasts;
        this.mOnBroadcastClickedListener = listener;
        this.mOnBroadcastLongClickedListener = longClickedListener;
        this.mPadding = (int) (16 * mContext.getResources().getDisplayMetrics().density);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View columnView = LayoutInflater.from(parent.getContext()).inflate(R.layout.artist_watching_item, parent, false);
        return new ViewHolder(columnView, mOnBroadcastClickedListener, mOnBroadcastLongClickedListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Stream stream = mMyBroadcasts.get(position);
        if (StreamValidator.isValid(stream)) {
            holder.bind(stream);
        }
    }

    @Override
    public int getItemCount() {
        return (mMyBroadcasts != null) ? mMyBroadcasts.size() : 0;
    }

    public void bind(List<Stream> broadcasts) {
        this.mMyBroadcasts = broadcasts;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        
        @Bind(R.id.artist_im_watching_img_label) TextView streamTextView;
        @Bind(R.id.artist_im_watching_img_view) ImageView streamImageView;
        @Bind(R.id.artist_im_watching_progress_bar) ImageView imageLoading;
        final int squareSize = ScreenDensityUtil.getScreenWidthPixels(mContext, 2) - mPadding;
        final OnBroadcastClickedListener mListener;
        final OnBroadcastLongClickedListener mLongClickedListener;
        Stream mStream;
        String thumbNailURL;
        boolean shouldStopUpdatingImage = false;
        int retriesSoFar = 1;
        int mUpdateInterval;

        public ViewHolder(View itemView, OnBroadcastClickedListener listener, OnBroadcastLongClickedListener longClickedListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
            mListener = listener;
            mLongClickedListener = longClickedListener;
            Glide.with(mContext).load(R.drawable.loading_anim).asGif().into(imageLoading);
        }

        @Override
        public void onClick(View v) {
            if (mListener != null && StreamValidator.isValid(mStream))
                mListener.onBroadcastClicked(mStream);
        }

        @Override
        public boolean onLongClick(View v) {
            if (mLongClickedListener != null && StreamValidator.isValid(mStream))
                mLongClickedListener.onBroadcastLongClicked(mStream);
            return false;
        }

        public void bind(final Stream stream) {
            streamTextView.setText("");
            streamTextView.setAllCaps(false);
            mStream = stream;
            thumbNailURL = UrlBuilder.create(SplashActivity.mThumbnailEndpoint)
                    .withSubDirectories(new String[]{mStream.getStreamId()})
                    .withExtension("jpg")
                    .build(UrlBuilder.Host.AWS);

            // Fine if we get error here
            streamImageView.getLayoutParams().width = squareSize;
            streamImageView.getLayoutParams().height = squareSize;
            streamImageView.setMinimumHeight(squareSize);
            streamImageView.setMinimumWidth(squareSize);
            streamImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageLoading.setVisibility(View.VISIBLE);
            setImage(false);
        }

        private void setImage(final boolean isUpdating) {
            Glide.with(mContext)
                    .load(thumbNailURL)
                    .override(squareSize, squareSize)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            if (thumbNailURL == null) {
                                if (imageLoading != null)
                                    imageLoading.setVisibility(View.GONE);
                            } else if (!isUpdating) {
                                retryUpdatingImage();
                            }
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            shouldStopUpdatingImage = true;
                            if (imageLoading != null)
                                imageLoading.setVisibility(View.GONE);
                            streamTextView.setText(mStream.getHeadline());
                            return false;
                        }
                    }).into(streamImageView);
        }

        private void retryUpdatingImage() {
            if (shouldStopUpdatingImage || retriesSoFar > 10) {
                if (imageLoading != null)
                    imageLoading.setVisibility(View.GONE);
                return;
            }

            if (retriesSoFar <= 5) {
                mUpdateInterval = AppConstants.UPDATE_IMAGE_FIRST_INTERVAL;
            } else {
                mUpdateInterval = AppConstants.UPDATE_IMAGE_SECOND_INTERVAL;
            }

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (!shouldStopUpdatingImage) {
                        retriesSoFar++;
                        setImage(true);
                        retryUpdatingImage();
                    }
                }
            }, mUpdateInterval);
        }
    }

    public interface OnBroadcastClickedListener {
        void onBroadcastClicked(Stream stream);
    }

    public interface OnBroadcastLongClickedListener {
        void onBroadcastLongClicked(Stream stream);
    }
}
