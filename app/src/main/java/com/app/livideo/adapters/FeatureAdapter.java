package com.app.livideo.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.app.livideo.R;
import com.app.livideo.activities.ArtistPageActivity;
import com.app.livideo.activities.ChatActivity;
import com.app.livideo.api.APICalls;
import com.app.livideo.interfaces.ActiveStreamHandler;
import com.app.livideo.interfaces.ArtistHandler;
import com.app.livideo.interfaces.EnterStreamHandler;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.Artist;
import com.app.livideo.models.EnterStreamData;
import com.app.livideo.models.Feature;
import com.app.livideo.models.Stream;
import com.app.livideo.models.User;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.LVUtils;
import com.app.livideo.utils.ScreenDensityUtil;
import com.bumptech.glide.Glide;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Eli on 4/5/2016.
 */
public class FeatureAdapter extends FragmentStatePagerAdapter {

    public static List<Feature> mFeatures;
    static int width, height;
    static boolean isActive;
    static Fragment mFragment;
    Context mContext;

    public FeatureAdapter(Fragment fragment, List<Feature> features, Context context) {
        super(fragment.getActivity().getSupportFragmentManager());
        mFragment = fragment;
        mFeatures = features;
        mContext = context;
        width = ScreenDensityUtil.getScreenWidthByPercentage(mContext, 1);
        height = (width * 9) / 16;
        isActive = true;
    }

    public void setIsActive(boolean status) {
        isActive = status;
    }

    @Override
    public Fragment getItem(int position) {
        return FeatureFragment.newInstance(position);
    }

    @Override
    public int getCount() {
        return (mFeatures != null) ? mFeatures.size() : 0;
    }

    public static class FeatureFragment extends Fragment {

        private static final int ACTION_TYPE_URL = 1;
        private static final int ACTION_TYPE_STREAM = 2;
        private static final int ACTION_TYPE_ARTIST = 3;
        private static final int ACTION_TYPE_CHAT = 4;
        public View mView;
        @Inject RequestQueue mRequestQueue;
        @Inject LVSession mSession;
        @Inject Context mContext;
        @Bind(R.id.featured_image) ImageView featured_image_view;
        @Bind(R.id.featured_progress_bar) ImageView mProgressBar;
        @Bind(R.id.featured_tagline) TextView tagline_view;
        @Bind(R.id.featured_headline) TextView headline_view;
        @Bind(R.id.featured_action_text) TextView action_text;
        @Bind(R.id.featured_touch_blocker) FrameLayout touch_blocker;
        int mNum;

        static FeatureFragment newInstance(int num) {
            FeatureFragment f = new FeatureFragment();
            Bundle args = new Bundle();
            args.putInt("num", num);
            f.setArguments(args);
            return f;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            LVInject.inject(this);
            mNum = getArguments() != null ? getArguments().getInt("num") : 1;
            setRetainInstance(true);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            mView = inflater.inflate(R.layout.discover_featured_view_item, container, false);
            ButterKnife.bind(this, mView);
            Glide.with(mContext).load(R.drawable.loading_anim).asGif().into(mProgressBar);
            featured_image_view.setScaleType(ImageView.ScaleType.FIT_XY);
            if (mFeatures != null) {
                Glide.with(mContext).load(mFeatures.get(mNum).getPictureUrl()).override(width, height).into(featured_image_view);
                tagline_view.setText(mFeatures.get(mNum).getTagline());
                headline_view.setText(mFeatures.get(mNum).getHeadline());
                action_text.setText(mFeatures.get(mNum).getActionText());
            }
            touch_blocker.setOnTouchListener(AppConstants.touchListner);
            return mView;
        }

        @Override
        public void onDestroyView() {
            super.onDestroyView();
            mView = null;
        }

        @OnClick(R.id.featured_action_button)
        protected void onFeatureActionClick() {
            if (!isActive)
                return;
            try {
                final String actionParam = mFeatures.get(mNum).getActionParam();
                switch (Integer.valueOf(mFeatures.get(mNum).getActionType())) {
                    case ACTION_TYPE_URL:
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(actionParam));
                        startActivity(browserIntent);
                        break;
                    case ACTION_TYPE_STREAM:
                        APICalls.requestStreamInfo(null, mContext, actionParam, mSession, mRequestQueue, new ActiveStreamHandler() {
                            @Override
                            public void RunCompleted(boolean isAvailable, final Stream stream) {
                                if (isAvailable) {
                                    performStreamClick(stream);
                                }
                            }
                        });
                        break;
                    case ACTION_TYPE_ARTIST:
                        APICalls.requestArtistInfo(actionParam, null, mContext, mSession, mRequestQueue, new ArtistHandler() {
                            @Override
                            public void RunCompleted(boolean isAvailable, Artist mArtist) {
                                if (isAvailable && mFragment != null && mFragment.getActivity() != null) {
                                    Intent intent = new Intent(mFragment.getActivity(), ArtistPageActivity.class);
                                    intent.putExtra(AppConstants.ARTIST_KEY_INTENT, mArtist);
                                    mFragment.getActivity().startActivity(intent);
                                }
                            }
                        });
                        break;
                    case ACTION_TYPE_CHAT:
                        if (mSession.getUser().getUserRole() == User.UserRole.GUEST) {
                            LVUtils.displayGuestWatchDialog(mFragment.getActivity(), null);
                            return;
                        }

                        APICalls.requestArtistInfo(actionParam, null, mContext, mSession, mRequestQueue, new ArtistHandler() {
                            @Override
                            public void RunCompleted(boolean isAvailable, Artist mArtist) {
                                if (isAvailable) {
                                    Intent chatIntent = new Intent(mFragment.getActivity(), ChatActivity.class);
                                    chatIntent.putExtra(AppConstants.ARTIST_KEY_INTENT, mArtist);
                                    getActivity().startActivity(chatIntent);
                                }
                            }
                        });
                        break;
                }
            } catch (Throwable ignore) {
            }
        }

        private void performStreamClick(final Stream stream) {
            touch_blocker.setVisibility(View.VISIBLE);
            APICalls.requestEnterStream(mSession, getActivity(), mContext, stream.getStreamId(), mRequestQueue, new EnterStreamHandler() {
                @Override
                public void RunCompleted(EnterStreamData enterStreamData) {
                    touch_blocker.setVisibility(View.GONE);
                    switch (enterStreamData.getCode()) {
                        case AppConstants.STREAMCODE_SUCCESS: // public
                            LVUtils.startVideoPlayer(stream, enterStreamData, touch_blocker, isActive, mFragment.getActivity());
                            break;
                        case AppConstants.STREAMCODE_NOT_SUBSCRIBED:
                        case AppConstants.STREAMCODE_IS_PRIVATE:
                            stream.setUrl(enterStreamData.getUrl());
                            stream.setWatchingId(enterStreamData.getWatchingId());
                            stream.setDidStar(enterStreamData.isDidStar());
                            APICalls.requestArtistInfo(stream.getArtistId(), mFragment.getActivity(), mContext, mSession, mRequestQueue, new ArtistHandler() {
                                @Override
                                public void RunCompleted(boolean isAvailable, Artist artist) {
                                    if (isAvailable) {
                                        LVUtils.beginUserSubscriptionProcess(artist, mFragment.getActivity(), mSession, mRequestQueue, null, new Runnable() {
                                            @Override
                                            public void run() {
                                                touch_blocker.setVisibility(View.GONE);
                                                mSession.setCurrentRunnable(null);
                                                performStreamClick(stream);
                                            }
                                        });
                                    } else {
                                        LVUtils.handleError(getString(R.string.invalid_artist_object), false, touch_blocker, mContext, mFragment.getActivity());
                                    }
                                }
                            });
                            break;
                        case AppConstants.STREAMCODE_NOT_EXISTS:
                            LVUtils.handleError(getString(R.string.stream_unavailable), false, touch_blocker, mContext, mFragment.getActivity());
                            break;
                        case AppConstants.STREAMCODE_OTHER_ERROR:
                            LVUtils.handleError(getString(R.string.retry_later), false, touch_blocker, mContext, mFragment.getActivity());
                            break;
                        default:
                            LVUtils.handleError(getString(R.string.api_error), false, touch_blocker, mContext, mFragment.getActivity());
                            break;
                    }
                }
            });
        }
    }
}
