package com.app.livideo.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.livideo.R;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.models.Artist;
import com.app.livideo.models.SearchData;
import com.app.livideo.models.Stream;
import com.app.livideo.models.validators.ArtistPictureValidator;
import com.app.livideo.models.validators.ArtistValidator;
import com.app.livideo.models.validators.StreamValidator;
import com.app.livideo.utils.LVUtils;
import com.app.livideo.utils.ScreenDensityUtil;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Eli on 9/18/2015.
 */
public class SearchArtistAdapter extends RecyclerView.Adapter<SearchArtistAdapter.RowViewHolder> {

    private static final int TYPE_ARTIST = 0, TYPE_STREAM = 1;
    @Inject Context mContext;
    private final OnArtistClickedListener mArtistClickedListener;
    private final OnStreamClickedListener mStreamClickedListener;
    List<Object> mSearchItems = new ArrayList<>();
    TextView noResultsView;
    int imageSize, rowHeightSize, mPadding, objectType;
    SearchData mSearchData;
    TextView mLabel;

    public SearchArtistAdapter(SearchData searchData, OnArtistClickedListener artistClickedListener,
                               OnStreamClickedListener streamClickedListener, TextView noResultsView, TextView label) {
        LVInject.inject(this);
        this.mSearchData = searchData;
        this.mSearchItems.addAll(mSearchData.getArtists());
        this.mSearchItems.addAll(mSearchData.getStreams());
        this.mArtistClickedListener = artistClickedListener;
        this.mStreamClickedListener = streamClickedListener;
        this.noResultsView = noResultsView;
        this.mPadding = (int) (8 * mContext.getResources().getDisplayMetrics().density);
        this.imageSize = ScreenDensityUtil.getImageSize(mContext);
        this.rowHeightSize = imageSize + mPadding;
        this.mLabel = label;
    }

    @Override
    public RowViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View channelsColumnView = LayoutInflater.from(parent.getContext()).inflate(R.layout.artist_row, parent, false);
        return new RowViewHolder(channelsColumnView, mArtistClickedListener, mStreamClickedListener);
    }

    @Override
    public void onBindViewHolder(RowViewHolder viewHolder, int position) {
        // IS STREAM
        try {
            ((Artist) (mSearchItems.get(position))).getPictures();
        } catch (Throwable ignore) {
            objectType = TYPE_STREAM;
        }

        // IS PICTURE
        try {
            ((Stream) (mSearchItems.get(position))).getStreamId();
        } catch (Throwable ignore) {
            objectType = TYPE_ARTIST;
        }

        switch (objectType) {
            case TYPE_STREAM:
                if (StreamValidator.isValid(((Stream) (mSearchItems.get(position)))))
                    viewHolder.bindStream(((Stream) (mSearchItems.get(position))));
                break;
            case TYPE_ARTIST:
                if (ArtistValidator.isValid(((Artist) (mSearchItems.get(position)))))
                    viewHolder.bindArtist(((Artist) (mSearchItems.get(position))));
                break;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mSearchItems.size();
    }

    public interface OnStreamClickedListener {
        void onStreamClicked(Stream stream);
    }

    public class RowViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @Bind(R.id.artist_img) ImageView artistImageView;
        @Bind(R.id.artist_text) TextView artistTextView;
        @Bind(R.id.artist_progress_bar) ImageView imageLoading;
        @Bind(R.id.artist_row_card_view) CardView mCardView;
        @Bind(R.id.artist_search_play) ImageView mPlayButton;
        final OnArtistClickedListener mArtistClickedListener;
        final OnStreamClickedListener mStreamClickedListener;
        Artist mArtist;
        Stream mStream;
        private String url;

        public RowViewHolder(View itemView, OnArtistClickedListener listener, OnStreamClickedListener streamClickedListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
            mArtistClickedListener = listener;
            mStreamClickedListener = streamClickedListener;
            Glide.with(mContext).load(R.drawable.loading_anim).asGif().into(imageLoading);
        }

        @Override
        public void onClick(View v) {
            if (mArtistClickedListener != null && ArtistValidator.isValid(mArtist))
                mArtistClickedListener.onArtistClicked(mArtist);
        }

        public void bindArtist(Artist artist) {
            mArtist = artist;
            mCardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mArtistClickedListener != null && ArtistValidator.isValid(mArtist))
                        mArtistClickedListener.onArtistClicked(mArtist);
                }
            });
            mPlayButton.setVisibility(View.GONE);
            url = "";
            mLabel.setText(mContext.getString(R.string.broadcaster));
            artistTextView.setText(mArtist.getDisplayName());
            if (ArtistPictureValidator.isValid(mArtist.getArtistPictures())) {
                // Set Image URL
                url = mArtist.getArtistPictures().get(mArtist.getArtistPictures().size() - 1).getUrl();

                // Set image size and download
                mCardView.setMinimumHeight(rowHeightSize);
                mCardView.getLayoutParams().height = rowHeightSize;

                artistImageView.getLayoutParams().width = imageSize;
                artistImageView.getLayoutParams().height = imageSize;
                artistImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageLoading.setVisibility(View.VISIBLE);
                setImage();
            }
        }

        private void setImage() {
            Glide.with(mContext)
                    .load(url)
                    .override(imageSize, imageSize)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                if (imageLoading != null)
                                    imageLoading.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            if (imageLoading != null)
                                imageLoading.setVisibility(View.GONE);
                            return false;
                        }
                    }).into(artistImageView);
        }

        public void bindStream(Stream stream) {
            mStream = stream;
            mCardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mStreamClickedListener != null && StreamValidator.isValid(mStream))
                        mStreamClickedListener.onStreamClicked(mStream);
                }
            });
            mPlayButton.setVisibility(View.VISIBLE);
            mLabel.setText(mContext.getString(R.string.streams));
            artistTextView.setText(mStream.getHeadline());
            url = LVUtils.getStreamThumbnailUrl(mStream.getStreamId());

            // Set image size and download
            mCardView.setMinimumHeight(rowHeightSize);
            mCardView.getLayoutParams().height = rowHeightSize;

            artistImageView.getLayoutParams().width = imageSize;
            artistImageView.getLayoutParams().height = imageSize;
            artistImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageLoading.setVisibility(View.VISIBLE);
            setImage();
        }
    }

    public void bind(List<Artist> artists, List<Stream> streams) {
        // Check if we need to display "No Results Found"
        noResultsView.setVisibility((!ArtistValidator.isValid(artists) && !StreamValidator.isValid(streams)) ? View.VISIBLE : View.GONE);

        // Update list
        List<Object> searchItems = new ArrayList<>();

        if (ArtistValidator.isValid(artists))
            searchItems.addAll(artists);

        if (StreamValidator.isValid(streams))
            searchItems.addAll(streams);

        this.mSearchItems = searchItems;
        notifyDataSetChanged();
    }

    public interface OnArtistClickedListener {
        void onArtistClicked(Artist artist);
    }
}
