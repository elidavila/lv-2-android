package com.app.livideo.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.livideo.R;
import com.app.livideo.activities.ArtistPageActivity;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.Artist;
import com.app.livideo.models.validators.ArtistValidator;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.ScreenDensityUtil;
import com.app.livideo.utils.TextUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by eli davila on 9/8/2015.
 */
public class DiscoverChannelsRowAdapter extends RecyclerView.Adapter<DiscoverChannelsRowAdapter.RowViewHolder> {

    @Inject Context mContext;
    @Inject LVSession mSession;
    private final Map<String, List<Artist>> artistTypesMap;
    private Activity activity;
    boolean isVisible = true;
    private int artistImageSize, artistNameHeightSize, mPadding,
            centerPositionOffset, centeredArtistPosition, screenWidth;

    public DiscoverChannelsRowAdapter(Map<String, List<Artist>> artistTypesList, Activity activity, boolean isVisible) {
        LVInject.inject(this);
        this.artistTypesMap = artistTypesList;
        this.activity = activity;
        this.artistImageSize = ScreenDensityUtil.getImageSize(mContext);
        this.artistNameHeightSize = artistImageSize / 4;
        this.mPadding = (int) (12 * mContext.getResources().getDisplayMetrics().density);
        this.centerPositionOffset = ScreenDensityUtil.getChannelsScroll(mContext);
        this.screenWidth = ScreenDensityUtil.getScreenWidthPixels(mContext, 1);
        this.isVisible = isVisible;
    }

    @Override
    public DiscoverChannelsRowAdapter.RowViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View channelsRowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.discover_row, parent, false);
        return new RowViewHolder(channelsRowView);
    }

    @Override
    public void onBindViewHolder(final RowViewHolder holder, final int position) {
        ArrayList<Artist> artistTypeChannelRow = (ArrayList<Artist>) artistTypesMap.values().toArray()[position];
        String artistLabel = (String) artistTypesMap.keySet().toArray()[position];
        if (!TextUtils.isNullOrEmpty(artistLabel) && ArtistValidator.isValid(artistTypeChannelRow)
                && artistTypeChannelRow.get(0).getArtistType().equalsIgnoreCase(artistLabel)) {
            holder.bind(artistLabel, artistTypeChannelRow, mContext);
        }
    }

    public class RowViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.channels_row_label) TextView textViewLabel;
        @Bind(R.id.channels_horizontal_view) RecyclerView channelsRowRecyclerView;
        LinearLayoutManager linearLayoutManager;
        public static final int NUMBER_OF_LOOPS = 500;
        int centerPosition;

        public RowViewHolder(final View rowView) {
            super(rowView);
            ButterKnife.bind(this, rowView);
        }

        public void bind(String artistLabel, List<Artist> artistChannelRow, Context mContext) {
            //Collections.sort(artistChannelRow, ARTIST_NAME_COMPARATOR);  // Sort by name

            // Set channel category
            textViewLabel.setText(artistLabel);
//            textViewLabel.setMinHeight(artistNameHeightSize);
//            textViewLabel.setMaxHeight(artistNameHeightSize);

/*            // Center channel category
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.setMargins(0, (artistImageSize / 2) - (artistNameHeightSize / 4), 0, 0);
            textViewLabel.setLayoutParams(lp);

            // Add snapping effect scroll to middle of screen
            channelsRowRecyclerView.addOnScrollListener(new SnapScrollListener(centerPositionOffset));

            // Set middle position for artist to be centered
            if (artistChannelRow.size() > 3) {
                linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
                for (int i = 0; i < artistChannelRow.size(); i++) {
                    if (ArtistValidator.isValid(artistChannelRow.get(i))) {
                        if (artistChannelRow.get(i).getArtistId().equalsIgnoreCase(SplashActivity.LIL_WAYNE_USER_ID)
                                || artistChannelRow.get(i).getArtistId().equalsIgnoreCase(SplashActivity.LIVIDEO_USER_ID)
                                || artistChannelRow.get(i).getArtistId().equalsIgnoreCase(SplashActivity.ANA_MONTANA_USER_ID)
                                || artistChannelRow.get(i).getArtistId().equalsIgnoreCase(SplashActivity.LETTY_USER_ID)) {
                            centeredArtistPosition = i;   // Save the artist position we want centered
                            break;
                        }
                    }
                }

                // Scroll to middle position
                centerPosition = (artistChannelRow.size() * NUMBER_OF_LOOPS / 2) + centeredArtistPosition;
                linearLayoutManager.scrollToPositionWithOffset(centerPosition, centerPositionOffset);
            } else {
                // No unlimited scroll here
                linearLayoutManager = new ArtistImageLayoutManager(mContext, screenWidth, artistImageSize);
                if (artistChannelRow.size() == 3)
                    linearLayoutManager.scrollToPositionWithOffset(1, 0);
            }*/

            // Set Channel Row Adapter
            linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
            channelsRowRecyclerView.setMinimumHeight(artistImageSize);
            channelsRowRecyclerView.getLayoutParams().height = artistImageSize;
            channelsRowRecyclerView.setLayoutManager(linearLayoutManager);
            channelsRowRecyclerView.setAdapter(new DiscoverColumnAdapter(artistChannelRow, mArtistClickedListener));
        }
    }

    public class ArtistImageLayoutManager extends LinearLayoutManager {
        private int mParentWidth, mItemWidth;

        public ArtistImageLayoutManager(Context context, int parentWidth, int itemWidth) {
            super(context, LinearLayoutManager.HORIZONTAL, false);
            mParentWidth = parentWidth;
            mItemWidth = itemWidth;
        }

        @Override
        public int getPaddingLeft() {
            return Math.round((mParentWidth / 2f) - (mItemWidth / 2f) - mPadding);
        }

        @Override
        public int getPaddingRight() {
            return getPaddingLeft();
        }
    }

    @Override
    public int getItemCount() {
        return artistTypesMap.size();
    }

    private DiscoverColumnAdapter.OnArtistClickedListener mArtistClickedListener = new DiscoverColumnAdapter.OnArtistClickedListener() {
        @Override
        public void onArtistClicked(Artist artist) {
            if (ArtistValidator.isValid(artist) && isVisible) {
                Intent intent = new Intent(activity, ArtistPageActivity.class);
                intent.putExtra(AppConstants.ACTIVITY_KEY_INTENT, AppConstants.CHANNELS);
                intent.putExtra(AppConstants.ARTIST_KEY_INTENT, artist);
                activity.startActivityForResult(intent, AppConstants.CHANNELS);
            }
        }
    };
}
