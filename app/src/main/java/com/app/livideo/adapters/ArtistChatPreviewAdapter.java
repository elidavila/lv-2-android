package com.app.livideo.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.livideo.R;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.models.ArtistChatMessage;
import com.app.livideo.models.validators.ArtistChatMessageValidator;
import com.app.livideo.utils.PicassoCircleTransform;
import com.app.livideo.utils.ScreenDensityUtil;
import com.app.livideo.utils.TextUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by eli davila on 9/22/2015.
 */
public class ArtistChatPreviewAdapter extends RecyclerView.Adapter<ArtistChatPreviewAdapter.ItemViewHolder> {
    @Inject Context mContext;
    Runnable mRunnable;
    private List<ArtistChatMessage> mChatMsgList;
    private int imageSize, mLayoutHeight;
    private HashMap<String, Integer> userColors;
    int random;
    List<Integer> mDisplayNameColors;
    private int lastUsedColorIndex = 0;

    public ArtistChatPreviewAdapter(List<ArtistChatMessage> chatMessageList, final int layoutHeight, final Runnable runnable) {
        LVInject.inject(this);
        this.imageSize = ScreenDensityUtil.getScreenHeightPixels(mContext, 25);
        this.mRunnable = runnable;
        this.mLayoutHeight = layoutHeight - (int) (4 * mContext.getResources().getDisplayMetrics().density);
        mDisplayNameColors = getDisplayNameColors();
        this.userColors = new HashMap<>();
        this.mChatMsgList = getChatMsgsThatFit(chatMessageList);
    }

    private List<ArtistChatMessage> getChatMsgsThatFit(List<ArtistChatMessage> chatMessageList) {
        if (!ArtistChatMessageValidator.isValid(chatMessageList) || !ArtistChatMessageValidator.isValid(chatMessageList.get(0)))
            return chatMessageList;

        TextView name = new TextView(mContext), msg = new TextView(mContext), viewMore = new TextView(mContext);
        name.setTextSize(12);
        msg.setTextSize(16);
        msg.setTextSize(14);

        Rect nameBounds = new Rect(), msgBounds = new Rect(), viewMoreBounds = new Rect();
        Paint namePaint = name.getPaint(), msgPaint = msg.getPaint(), viewMorePaint = viewMore.getPaint();

        viewMorePaint.getTextBounds(mContext.getString(R.string.view_more),0,mContext.getString(R.string.view_more).length(),viewMoreBounds);
        int totalHeight = viewMoreBounds.height();

        List<ArtistChatMessage> newChatMessages = new ArrayList<>();

        int nameHeight, msgHeight;
        for (int i = 0; i < chatMessageList.size(); i++) {
            // Get msg and name height
            namePaint.getTextBounds(chatMessageList.get(i).getDisplayName(),0, chatMessageList.get(i).getDisplayName().length(),nameBounds);
            nameHeight = nameBounds.height() + (int) (8 * mContext.getResources().getDisplayMetrics().density);
            msgPaint.getTextBounds(chatMessageList.get(i).getMessage(),0, chatMessageList.get(i).getMessage().length(),msgBounds);
            msgHeight = msgBounds.height() + (int) (8 * mContext.getResources().getDisplayMetrics().density);

            // Item count that fits
            totalHeight = (imageSize > nameHeight + msgHeight) ? imageSize + totalHeight : nameHeight + msgHeight + totalHeight;
            if (totalHeight >= mLayoutHeight) {
                break;
            } else {
                newChatMessages.add(chatMessageList.get(i));
            }
        }

        // Return new chat list that fits in given area
        if (ArtistChatMessageValidator.isValid(newChatMessages) && newChatMessages.size() > 1)
            newChatMessages.remove(newChatMessages.size() - 1);
        Collections.reverse(newChatMessages);
        return newChatMessages;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        try {
            View itemView = LayoutInflater.from(mContext).inflate(R.layout.preview_chat_item, parent, false);
            return new ItemViewHolder(itemView);
        } catch (Throwable ignore) {
            return null;
        }
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        ArtistChatMessage chatMessage = mChatMsgList.get(position);
        holder.bind(chatMessage, position);
    }

    @Override
    public int getItemCount() {
        return (mChatMsgList == null) ? 0 : mChatMsgList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.preview_chat_img) ImageView imageView;
        @Bind(R.id.preview_chat_message) TextView msgView;
        @Bind(R.id.preview_chat_name) TextView nameView;
        @Bind(R.id.preview_chat_view_more) TextView viewMoreView;

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(ArtistChatMessage chatMessage, int position) {
            if (!TextUtils.isNullOrEmpty(chatMessage.getDisplayName())){
                nameView.setText(chatMessage.getDisplayName().trim());
            }

            nameView.setTextColor(ContextCompat.getColor(mContext, getDisplayNameColorForUser(chatMessage.getMessageUserId())));
            msgView.setText(chatMessage.getMessage());
            setImage(chatMessage, imageView);

            viewMoreView.setVisibility((position == getItemCount() - 1) ? View.VISIBLE : View.GONE);
        }

        @OnClick(R.id.preview_chat_item_card_view)
        protected void onChatItemClick() {
            if (mRunnable != null)
                mRunnable.run();
        }
    }

    private Integer getDisplayNameColorForUser(String user) {
        if (this.userColors.containsKey(user))
            return this.userColors.get(user);
        do {
            random = (int) (Math.random() * mDisplayNameColors.size());
        } while (random == this.lastUsedColorIndex);
        this.lastUsedColorIndex = random;
        this.userColors.put(user, this.mDisplayNameColors.get(random));
        return this.getDisplayNameColorForUser(user);
    }

    private void setImage(ArtistChatMessage chatMessage, ImageView imageView) {
        imageView.setVisibility(View.VISIBLE);
        imageView.setMinimumWidth(imageSize);
        imageView.setMinimumHeight(imageSize);
        if (ArtistChatMessageValidator.isValid(chatMessage)) {
            if (ArtistChatMessageValidator.isImageValid(chatMessage)) {
                Picasso.with(mContext)
                        .load(chatMessage.getPictureUrl())
                        .resize((imageSize != 0) ? imageSize : 50, (imageSize != 0) ? imageSize : 50)
                        .transform(new PicassoCircleTransform())
                        .into(imageView);
            } else {
                Picasso.with(mContext)
                        .load(R.drawable.ic_username)
                        .resize((imageSize != 0) ? imageSize : 50, (imageSize != 0) ? imageSize : 50)
                        .transform(new PicassoCircleTransform())
                        .into(imageView);
            }
        }
    }

    private List<Integer> getDisplayNameColors() {
        List<Integer> mColorList = new ArrayList<>();
        mColorList.add(R.color.chat_pink);
        mColorList.add(R.color.chat_yellow);
        mColorList.add(R.color.chat_green_blue);
        mColorList.add(R.color.chat_green);
        mColorList.add(R.color.chat_blue);
        mColorList.add(R.color.chat_red);
        mColorList.add(R.color.chat_purple);
        return mColorList;
    }
}
