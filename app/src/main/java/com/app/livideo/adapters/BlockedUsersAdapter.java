package com.app.livideo.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.livideo.R;
import com.app.livideo.models.User;
import com.app.livideo.models.validators.UserImageValidator;
import com.app.livideo.utils.PicassoCircleTransform;
import com.app.livideo.utils.ScreenDensityUtil;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Eli on 5/6/2016.
 */
public class BlockedUsersAdapter extends RecyclerView.Adapter<BlockedUsersAdapter.ViewHolder> {

    List<User> mBlockedUsers;
    int imageSize;
    Context mContext;
    BlockedUserClickedListener mBlockedUserClickedListener;

    public BlockedUsersAdapter(List<User> blockedUsers, Context context, final BlockedUserClickedListener blockedUserClickedListener) {
        this.mBlockedUsers = blockedUsers;
        this.mContext = context;
        this.imageSize = ScreenDensityUtil.getScreenHeightPixels(mContext, 20);
        this.mBlockedUserClickedListener = blockedUserClickedListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_blocked_user, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        User blockedUser = mBlockedUsers.get(position);
//        if (ArtistValidator.isValid(blockedUser)) todo
            holder.bind(blockedUser);
    }

    @Override
    public int getItemCount() {
        return mBlockedUsers.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.blocked_user_img) ImageView image_view;
        @Bind(R.id.blocked_user_name) TextView text_view;
        @Bind(R.id.blocked_user_progress_bar) ProgressBar mProgressBar;
        User mBlockedUser;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(User blockedUser) {
            this.mBlockedUser = blockedUser;
            text_view.setText(mBlockedUser.getDisplayName());
            image_view.setMinimumHeight(imageSize);
            image_view.setMinimumWidth(imageSize);
            if (UserImageValidator.isValid(mBlockedUser.getPictures())) {
                mProgressBar.setVisibility(View.VISIBLE);
                Picasso.with(mContext)
                        .load(mBlockedUser.getPictures().get(mBlockedUser.getPictures().size() - 1).getUrl())
                        .resize(imageSize, imageSize)
                        .transform(new PicassoCircleTransform())
                        .into(image_view, new Callback() {
                            @Override
                            public void onSuccess() {
                                mProgressBar.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError() {
                                mProgressBar.setVisibility(View.GONE);
                            }
                        });
            }
        }

        @OnClick(R.id.blocked_user_switch)
        protected void onBlockedUserToggleClick() {
            mBlockedUserClickedListener.onBlockedUserToggleClick(mBlockedUser);
        }

        @OnClick(R.id.blocked_user_card_view)
        protected void onBlockedUserClick() {
            // leave this here
        }
    }

    public interface BlockedUserClickedListener {
        void onBlockedUserToggleClick(User blockedUser);
    }
}