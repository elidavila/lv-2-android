package com.app.livideo.adapters;

import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.app.livideo.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Michael Wheeler on 4/18/2016.
 */
public class SettingsPageAdapter extends RecyclerView.Adapter<SettingsPageAdapter.SettingsViewHolder>{

    public static final int PAGE_ONE = 0;
    public static final int PAGE_TWO = 1;
    public static final int PAGE_THREE = 2;

    Fragment[] fragments;

    public SettingsPageAdapter(Fragment[] fragmentList){
        this.fragments = fragmentList;
    }

    @Override
    public SettingsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(SettingsViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public static class SettingsViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.settings_container) public FrameLayout mFrameLayout;

        public SettingsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
