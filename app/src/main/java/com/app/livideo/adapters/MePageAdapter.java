package com.app.livideo.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.app.livideo.R;
import com.app.livideo.activities.SplashActivity;
import com.app.livideo.api.APICalls;
import com.app.livideo.fragments.MePageFragment;
import com.app.livideo.interfaces.ChatMessagesHandler;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.Artist;
import com.app.livideo.models.ArtistChatMessage;
import com.app.livideo.models.ArtistPage;
import com.app.livideo.models.Stream;
import com.app.livideo.models.User;
import com.app.livideo.models.validators.ArtistChatMessageValidator;
import com.app.livideo.models.validators.ArtistPictureValidator;
import com.app.livideo.models.validators.ArtistValidator;
import com.app.livideo.models.validators.StreamValidator;
import com.app.livideo.utils.AndroidUtils;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.LVUtils;
import com.app.livideo.utils.PicassoCircleTransform;
import com.app.livideo.utils.ScreenDensityUtil;
import com.app.livideo.utils.TimeUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;

/**
 * Created by Eli on 4/18/2016.
 */
public class MePageAdapter extends FragmentStatePagerAdapter {

    static List<Stream> mStreams;
    static MePageFragment mFragment;

    public  MePageAdapter(MePageFragment fragment, FragmentManager fm, List<Stream> timeLines) {
        super(fm);
        mFragment = fragment;
        mStreams = timeLines;
    }

    @Override
    public Fragment getItem(int position) {
        return TimeLinePageFragment.newInstance(position);
    }

    @Override
    public int getCount() {
        return mStreams.size();
    }

    public void bind(List<Stream> myTimeLines) {
        mStreams = myTimeLines;
        notifyDataSetChanged();
    }

    public static class TimeLinePageFragment extends Fragment {

        private static final String PREVIEW_CHAT_LIMIT = "10";
        private static final int SUB_TYPE_HERO = 0, SUB_TYPE_FAV = 1, SUB_TYPE_HAS_MEMBERSHIP = 2, SUB_TYPE_NONE = 3;

        @Inject Context mContext;
        @Inject LVSession mSession;
        @Inject RequestQueue mRequestQueue;

        @Bind(R.id.time_line_main_img) ImageView main_img_view;
        @Bind(R.id.time_line_play_img) ImageView play_img_view;
        @Bind(R.id.time_line_lock_img) ImageView lock_img_view;
        @Bind(R.id.time_line_artist_icon) ImageView artist_details_img_view;
        @Bind(R.id.time_line_artist_name) TextView artist_details_name_view;
        @Bind(R.id.time_line_artist_headline) TextView artist_details_headline_view;
        @Bind(R.id.time_line_artist_time_stamp) TextView artist_details_time_stamp_view;
        @Bind(R.id.time_line_progress_bar) ImageView touch_blocker;
        @Bind(R.id.me_page_preview_chat) RecyclerView preview_chat_recycler_view;
        @Bind(R.id.me_page_stream) View mStreamView;

        ArtistChatPreviewAdapter chatPreviewAdapter;
        private Stream mStream;
        private int mPosition, imageSize, mSubscriptionType;
        boolean isActive;

        static TimeLinePageFragment newInstance(int position) {
            TimeLinePageFragment f = new TimeLinePageFragment();
            Bundle args = new Bundle();
            args.putInt(ArtistPage.Keys.OBJECT_TYPE, position);
            f.setArguments(args);
            return f;
        }

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            LVInject.inject(this);
            if (!StreamValidator.isValid(mStreams)) {
                AndroidUtils.restartApp(mContext);
                return;
            }

            imageSize = ScreenDensityUtil.getScreenWidthPixels(mContext, 1);
            mPosition = (getArguments() != null) ? getArguments().getInt(ArtistPage.Keys.OBJECT_TYPE) : 0;
            mStream = mStreams.get(mPosition);
            if (mSession.getUser().getUserRole() != User.UserRole.GUEST)
                mStream.getArtist().setIsWatching(1);
            setSubscriptionType();
        }

        private void setSubscriptionType() {
            if (mStream.getArtist().getIsHero() == 1) {
                mSubscriptionType = SUB_TYPE_HERO;
            } else if (mStream.getArtist().getIsWatching() == 1) {
                mSubscriptionType = SUB_TYPE_FAV;
            } else if (mSession.getUser().getHasMembership() == 1) {
                mSubscriptionType = SUB_TYPE_HAS_MEMBERSHIP;
            }  else {
                mSubscriptionType = SUB_TYPE_NONE;
            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.item_me_page, container, false);
            ButterKnife.bind(this, rootView);
            if (!StreamValidator.isValid(mStream)) {
                AndroidUtils.restartApp(mContext);
                return  rootView;
            }
            initialize();
            createStreamView();
            requestChatMsgs(false);
            return rootView;
        }

        private void requestChatMsgs(final boolean isUpdating) {
            if (!isUpdating)
                touch_blocker.setVisibility(View.VISIBLE);
            APICalls.requestChatMsgs(mFragment.getActivity(), mContext, mStream.getArtist().getArtistId(), "0", PREVIEW_CHAT_LIMIT, mSession, mRequestQueue, new ChatMessagesHandler() {
                @Override
                public void onRunComplete(List<ArtistChatMessage> newChatMessages) {
                    if (ArtistChatMessageValidator.isValid(newChatMessages)) {
                        if (!isUpdating) {
                            touch_blocker.setVisibility(View.GONE);
                            mStream.getArtist().setPreviewChatMessageList(newChatMessages);
                            addPreviewChatView();
                        } else if (ArtistValidator.isValid(mStream.getArtist()) && ArtistChatMessageValidator.isValid(mStream.getArtist().getPreviewChatMessageList())) {
                            List<ArtistChatMessage> oldChatMessages = mStream.getArtist().getPreviewChatMessageList();
                            if (!oldChatMessages.equals(newChatMessages)) {
                                mStream.getArtist().setPreviewChatMessageList(newChatMessages);    // Update old chat view
                                addPreviewChatView();
                            }
                        }
                    } else if (!isUpdating) {
                        mStream.getArtist().setPreviewChatMessageList(newChatMessages);
                        addPreviewChatView(); // empty chat box
                    }
                }
            });
        }

        private void addPreviewChatView() {
            // Set Layout manager first
            LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false) {
                @Override
                public boolean canScrollVertically() {
                    return false;
                }
            };
            preview_chat_recycler_view.setLayoutManager(mLayoutManager);

            // Preview chat data/view
            List<ArtistChatMessage> latestChatMessageList = mStream.getArtist().getPreviewChatMessageList();

            if (!ArtistChatMessageValidator.isValid(latestChatMessageList)) {
                latestChatMessageList = new ArrayList<>();
                latestChatMessageList.add(new ArtistChatMessage());
            }
            preview_chat_recycler_view.setHasFixedSize(true);
            chatPreviewAdapter = new ArtistChatPreviewAdapter(latestChatMessageList, preview_chat_recycler_view.getHeight(), onJoinChatClickRunnable);
            preview_chat_recycler_view.setAdapter(chatPreviewAdapter);
        }

        Runnable onJoinChatClickRunnable = new Runnable() {
            @Override
            public void run() {
                if (isActive) {
                    onJoinChatClick();
                }
            }
        };

        protected void onJoinChatClick() {
            if (!ArtistValidator.isValid(mStream.getArtist())) {
                LVUtils.handleError(getString(R.string.invalid_artist_object), true, touch_blocker, mContext, mFragment.getActivity());
                return;
            }

            switch (mSubscriptionType) {
                case AppConstants.SUB_TYPE_HERO:
                case AppConstants.SUB_TYPE_FAV:
                case AppConstants.SUB_TYPE_HAS_MEMBERSHIP:
                    mFragment.parent.startArtistChat(mStream.getArtist());
                    break;
                case AppConstants.SUB_TYPE_NONE:
                default:
                    LVUtils.beginUserSubscriptionProcess(mStream.getArtist(), mFragment.getActivity(), mSession, mRequestQueue, null, new Runnable() {
                        @Override
                        public void run() {
                            mSession.setCurrentRunnable(null);
                            mFragment.parent.startArtistChat(mStream.getArtist());
                        }
                    });
                    break;
            }
        }

        @Override
        public void onResume() {
            super.onResume();
            isActive = true;
        }

        @Override
        public void onPause() {
            super.onPause();
            isActive = false;
        }

        private void initialize() {
            Glide.with(mContext).load(R.drawable.loading_anim).asGif().into(touch_blocker);
            mStreamView.getLayoutParams().height = ScreenDensityUtil.getScreenWidthPixels(mContext, 1);
            touch_blocker.setOnTouchListener(AppConstants.touchListner);
        }

        private void createStreamView() {
            play_img_view.setVisibility(View.VISIBLE);

            if (ArtistValidator.isValid(mStream.getOpenMicUser())) {
                setArtistDetailsView(mStream.getOpenMicUser());
            } else {
                setArtistDetailsView(mStream.getArtist());
            }
            setMainImage(LVUtils.getStreamThumbnailUrl(mStream.getStreamId()));
            if (!mStream.isPublic()
                    && mSubscriptionType == AppConstants.SUB_TYPE_NONE
                    && !mStream.getArtist().getArtistId().equalsIgnoreCase(SplashActivity.LIVIDEO_USER_ID)) {
                lock_img_view.setVisibility(View.VISIBLE);
            }
        }

        private void setArtistDetailsView(final Artist mArtist) {
            if (ArtistValidator.isValid(mArtist)) {
                if (ArtistPictureValidator.isValid(mArtist.getArtistPictures())) {
                    final int artistImageSize = ScreenDensityUtil.getScreenHeightPixels(mContext, 15);
                    artist_details_img_view.setMinimumWidth(artistImageSize);
                    artist_details_img_view.setMinimumHeight(artistImageSize);
                    Picasso.with(mContext)
                            .load(mArtist.getArtistPictures().get(mArtist.getArtistPictures().size() - 1).getUrl())
                            .resize(artistImageSize, artistImageSize)
                            .transform(new PicassoCircleTransform())
                            .into(artist_details_img_view, new Callback() {
                                @Override
                                public void onSuccess() {
                                    artist_details_headline_view.setText(mStream.getHeadline());
                                    artist_details_name_view.setText(mArtist.getDisplayName());
                                    artist_details_time_stamp_view.setText(TimeUtils.convertTimeForStream(mStream.getTime(), mContext));
                                }

                                @Override
                                public void onError() {

                                }
                            });
                }
            }
        }

        private void setMainImage(final String url) {
            main_img_view.setScaleType(ImageView.ScaleType.CENTER_CROP);
            touch_blocker.setVisibility(View.VISIBLE);
            Glide.with(mContext)
                    .load(url)
                    .override(imageSize, imageSize)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            if (touch_blocker != null)
                                touch_blocker.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            if (touch_blocker != null)
                                touch_blocker.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(main_img_view);
        }

        @OnClick(R.id.time_line_play_img)
        protected void onPlayButtonClick() {
            mFragment.performStreamClick(mStream);
        }

        @OnLongClick(R.id.time_line_play_img)
        protected boolean onPlayButtonLockClick() {
            mFragment.performStreamLongClick(mStream);
            return true;
        }

        @OnClick(R.id.time_line_artist_icon)
        protected void onArtistImageClick() {
            mFragment.parent.startArtistPage(mStream.getArtist());
        }
    }
}
