package com.app.livideo.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.livideo.R;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.models.ArtistChatMessage;
import com.app.livideo.models.validators.ArtistChatMessageValidator;
import com.app.livideo.utils.PicassoCircleTransform;
import com.app.livideo.utils.ScreenDensityUtil;
import com.app.livideo.utils.TextUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;

/**
 * Created by eli on 9/24/2015.
 */
public class ArtistChatAdapter extends RecyclerView.Adapter<ArtistChatAdapter.ItemViewHolder> {

    @Inject Context mContext;
    Activity mActivity;
    List<Integer> mDisplayNameColors;
    int random;
    private int chatImageSize;
    private int lastUsedColorIndex = 0;
    private List<ArtistChatMessage> chatMessageList;
    private HashMap<String, Integer> userColors;
    private String mArtistID;
    private OnImageClickedListener mOnImageClickedListener;

    public ArtistChatAdapter(Activity activity, String artistID, List<ArtistChatMessage> chatMessages, final OnImageClickedListener longClickedListener) {
        LVInject.inject(this);
        this.mActivity = activity;
        this.mArtistID = artistID;
        this.chatMessageList = chatMessages;
        this.userColors = new HashMap<>();
        this.mDisplayNameColors = getDisplayNameColors();
        this.mOnImageClickedListener = longClickedListener;
        this.chatImageSize = ScreenDensityUtil.getScreenHeightPixels(mContext, 20);
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_artist, parent, false);
        return new ItemViewHolder(itemView, mOnImageClickedListener);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        ArtistChatMessage chatMessage = chatMessageList.get(position);
        if (ArtistChatMessageValidator.isValid(chatMessage))
            holder.bind(chatMessage);
    }

    @Override
    public int getItemCount() {
        return chatMessageList.size();
    }

    public void bind(List<ArtistChatMessage> newChatMessages) {
        this.chatMessageList = newChatMessages;
        notifyDataSetChanged();
    }

    private Integer getDisplayNameColorForUser(String user) {
        if (this.userColors.containsKey(user))
            return this.userColors.get(user);
        if (user.equalsIgnoreCase(this.mArtistID))
            return R.color.livideo_white;
        do {
            random = (int) (Math.random() * mDisplayNameColors.size());
        } while (random == this.lastUsedColorIndex);
        this.lastUsedColorIndex = random;
        this.userColors.put(user, this.mDisplayNameColors.get(random));
        return this.getDisplayNameColorForUser(user);
    }

    private List<Integer> getDisplayNameColors() {
        List<Integer> mColorList = new ArrayList<>();
        mColorList.add(R.color.chat_pink);
        mColorList.add(R.color.chat_yellow);
        mColorList.add(R.color.chat_green_blue);
        mColorList.add(R.color.chat_green);
        mColorList.add(R.color.chat_blue);
        mColorList.add(R.color.chat_red);
        mColorList.add(R.color.chat_purple);
        return mColorList;
    }

    public interface OnImageClickedListener {
        void onImageLongClicked(ArtistChatMessage chatMessage);
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        final OnImageClickedListener mClickedListener;
        @Bind(R.id.chat_item_left_name_view) TextView leftName;
        @Bind(R.id.chat_item_left_msg_text) TextView leftMsg;
        @Bind(R.id.chatLeftImage) ImageView leftImage;
        @Bind(R.id.chat_item_card_view) CardView chat_item_card_view;
        ArtistChatMessage mChatMessage;

        public ItemViewHolder(View itemView, OnImageClickedListener longClickedListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mClickedListener = longClickedListener;
        }

        @SuppressLint("SetTextI18n")
        public void bind(final ArtistChatMessage chatMessage) {
            mChatMessage = chatMessage;
            setDisplayName(leftName);
            setChatMessage(leftMsg);
            setImage(mChatMessage, leftImage);

            // ARTIST PURPLE BACKGROUND
            if (mChatMessage.getMessageUserId().equalsIgnoreCase(mArtistID)) {
                chat_item_card_view.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.chat_purple));
                leftName.setTextColor(ContextCompat.getColor(mContext, R.color.livideo_white));
            } else {
                chat_item_card_view.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.tw__transparent));
            }
        }

        @OnClick(R.id.chatLeftImage)
        protected void onImageClicked() {
            if (mClickedListener != null)
                mClickedListener.onImageLongClicked(mChatMessage);
        }

        @OnClick(R.id.chat_item_left_name_view)
        protected void onNameCLick() {
            if (mClickedListener != null)
                mClickedListener.onImageLongClicked(mChatMessage);
        }

        @OnLongClick(R.id.chat_item_msg_text)
        protected boolean onItemMessageClick() {
            ClipboardManager clipboard = (ClipboardManager) mActivity.getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = null;
            if (!TextUtils.isNullOrEmpty(leftMsg.getText().toString()) && leftMsg.getVisibility() == View.VISIBLE)
                clip = ClipData.newPlainText("label", leftMsg.getText().toString());

            if (clip != null) {
                clipboard.setPrimaryClip(clip);
                Toast.makeText(mContext, R.string.message_copied_to_clipboard, Toast.LENGTH_SHORT).show();
            }
            return true;
        }

        private void setDisplayName(TextView nameView) {
            nameView.setText(mChatMessage.getDisplayName().trim());
            nameView.setTextColor(ContextCompat.getColor(mContext, getDisplayNameColorForUser(mChatMessage.getMessageUserId())));
        }

        private void setChatMessage(TextView msgView) {
            msgView.setText(mChatMessage.getMessage().trim());
        }

        private void setImage(ArtistChatMessage chatMessage, ImageView imageView) {
            imageView.setMinimumWidth(chatImageSize);
            imageView.setMinimumHeight(chatImageSize);
            if (ArtistChatMessageValidator.isImageValid(chatMessage)) {
                Picasso.with(mContext)
                        .load(chatMessage.getPictureUrl())
                        .resize(chatImageSize, chatImageSize)
                        .transform(new PicassoCircleTransform())
                        .into(imageView);
            } else {
                Picasso.with(mContext)
                        .load(R.drawable.ic_username)
                        .resize(chatImageSize, chatImageSize)
                        .transform(new PicassoCircleTransform())
                        .into(imageView);
            }
        }
    }
}