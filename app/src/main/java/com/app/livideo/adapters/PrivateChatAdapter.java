package com.app.livideo.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.app.livideo.R;
import com.app.livideo.activities.ChatActivity;
import com.app.livideo.activities.DisplaySingleImageActivity;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.models.PrivateChatMessage;
import com.app.livideo.models.User;
import com.app.livideo.models.validators.ArtistChatMessageValidator;
import com.app.livideo.models.validators.PrivateChatMessageValidator;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.PicassoCircleTransform;
import com.app.livideo.utils.ScreenDensityUtil;
import com.app.livideo.utils.TextUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;

/**
 * Created by Eli on 5/12/2016.
 */
public class PrivateChatAdapter extends RecyclerView.Adapter<PrivateChatAdapter.ItemViewHolder> {

    @Inject Context mContext;
    ChatActivity mParent;
    List<Integer> mDisplayNameColors;
    int random;
    private int chatImageSize;
    private int lastUsedColorIndex = 0;
    private List<PrivateChatMessage> chatMessageList;
    private HashMap<String, Integer> userColors;
    private String mArtistID;
    User mPrivateChatUser;
    int PIC_MSG_MAX_WIDTH;

    public PrivateChatAdapter(ChatActivity activity, User privateChatUser) {
        LVInject.inject(this);
        this.mParent = activity;
        this.mPrivateChatUser = privateChatUser;
        this.mArtistID = mPrivateChatUser.getUserID();
        this.chatMessageList = mPrivateChatUser.getPrivateChatMessages();
        this.userColors = new HashMap<>();
        this.mDisplayNameColors = getDisplayNameColors();
        this.chatImageSize = ScreenDensityUtil.getScreenHeightPixels(mContext, 20);
        this.PIC_MSG_MAX_WIDTH =  ScreenDensityUtil.getScreenWidthPixels(mContext, 1) - chatImageSize;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_artist, parent, false);
        return new ItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        PrivateChatMessage chatMessage = chatMessageList.get(position);
        if (PrivateChatMessageValidator.isValid(chatMessage))
            holder.bind(chatMessage);
    }

    @Override
    public int getItemCount() {
        return chatMessageList.size();
    }

    public void bind(List<PrivateChatMessage> newChatMessages) {
        this.chatMessageList = newChatMessages;
        notifyDataSetChanged();
    }

    private Integer getDisplayNameColorForUser(String user) {
        if (this.userColors.containsKey(user))
            return this.userColors.get(user);
//        if (user.equalsIgnoreCase(this.mArtistID))
//            return R.color.livideo_white;
        do {
            random = (int) (Math.random() * mDisplayNameColors.size());
        } while (random == this.lastUsedColorIndex);
        this.lastUsedColorIndex = random;
        this.userColors.put(user, this.mDisplayNameColors.get(random));
        return this.getDisplayNameColorForUser(user);
    }

    private List<Integer> getDisplayNameColors() {
        List<Integer> mColorList = new ArrayList<>();
        mColorList.add(R.color.chat_pink);
        mColorList.add(R.color.chat_yellow);
        mColorList.add(R.color.chat_green_blue);
        mColorList.add(R.color.chat_green);
        mColorList.add(R.color.chat_blue);
        mColorList.add(R.color.chat_red);
        mColorList.add(R.color.chat_purple);
        return mColorList;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.chat_item_left_name_view) TextView leftName;
        @Bind(R.id.chat_item_left_msg_text) TextView leftMsg;
        @Bind(R.id.chat_item_left_msg_img) ImageView leftMsgImg;
        @Bind(R.id.chatLeftImage) ImageView leftImage;
        @Bind(R.id.chat_item_card_view) CardView chat_item_card_view;
        @Bind(R.id.chat_item_pic_msg_progress_bar) ProgressBar mPicMsgProgressBar;
        PrivateChatMessage mChatMessage;

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @SuppressLint("SetTextI18n")
        public void bind(final PrivateChatMessage chatMessage) {
            mChatMessage = chatMessage;
            setDisplayName(leftName);

            leftMsgImg.setVisibility(View.GONE);
            if (mChatMessage.getMessageType().equalsIgnoreCase(AppConstants.TYPE_PICTURE))
                setPictureMessage(leftMsgImg, leftMsg);
            else
                setChatMessage(leftMsg, leftMsgImg);

            setImage(mChatMessage, leftImage);
            chat_item_card_view.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.tw__transparent));
        }

        @OnLongClick(R.id.chat_item_msg_text)
        protected boolean onItemMessageClick() {
            ClipboardManager clipboard = (ClipboardManager) mParent.getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = null;
            if (!TextUtils.isNullOrEmpty(leftMsg.getText().toString()) && leftMsg.getVisibility() == View.VISIBLE)
                clip = ClipData.newPlainText("label", leftMsg.getText().toString());

            if (clip != null) {
                clipboard.setPrimaryClip(clip);
                Toast.makeText(mContext, R.string.message_copied_to_clipboard, Toast.LENGTH_SHORT).show();
            }
            return true;
        }

        private void setDisplayName(TextView nameView) {
            nameView.setText(mChatMessage.getDisplayName().trim());
            nameView.setTextColor(ContextCompat.getColor(mContext, getDisplayNameColorForUser(mChatMessage.getUserID())));
        }

        private void setChatMessage(TextView msgView, ImageView imgView) {
            imgView.setVisibility(View.GONE);
            msgView.setVisibility(View.VISIBLE);
            msgView.setText(mChatMessage.getMessage().trim());
        }

        private void setImage(PrivateChatMessage chatMessage, ImageView imageView) {
            imageView.setMinimumWidth(chatImageSize);
            imageView.setMinimumHeight(chatImageSize);
            if (PrivateChatMessageValidator.isImageValid(chatMessage)) {
                Picasso.with(mContext)
                        .load(chatMessage.getPictureUrl())
                        .resize(chatImageSize, chatImageSize)
                        .transform(new PicassoCircleTransform())
                        .into(imageView);
            } else {
                Picasso.with(mContext)
                        .load(R.drawable.ic_username)
                        .resize(chatImageSize, chatImageSize)
                        .transform(new PicassoCircleTransform())
                        .into(imageView);
            }
        }

        private void setPictureMessage(ImageView leftMsgImg, TextView leftMsg) {
            leftMsg.setVisibility(View.GONE);
            leftMsgImg.setVisibility(View.VISIBLE);
            leftMsgImg.setMaxWidth(PIC_MSG_MAX_WIDTH);
            mPicMsgProgressBar.setVisibility(View.VISIBLE);
            Glide.with(mContext)
                    .load(mChatMessage.getMessage())
                    .centerCrop()
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            if (mPicMsgProgressBar != null)
                                mPicMsgProgressBar.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            if (mPicMsgProgressBar != null)
                                mPicMsgProgressBar.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(leftMsgImg);
        }
        
        @OnClick(R.id.chat_item_left_msg_img)
        protected void onPicMsgClick() {
            if (PrivateChatMessageValidator.isValid(mChatMessage) && !TextUtils.isNullOrEmpty(mChatMessage.getMessage())) {
                Intent intent = new Intent(mParent, DisplaySingleImageActivity.class);
                intent.putExtra(AppConstants.KEY_PICTURE_URL, mChatMessage.getMessage());
                mParent.startActivity(intent);
            }
        }
    }
}
