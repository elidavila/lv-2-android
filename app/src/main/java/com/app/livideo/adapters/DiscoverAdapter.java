package com.app.livideo.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.app.livideo.R;
import com.app.livideo.activities.ArtistPageActivity;
import com.app.livideo.api.APICalls;
import com.app.livideo.fragments.DiscoverFragment;
import com.app.livideo.interfaces.ArtistHandler;
import com.app.livideo.interfaces.EnterStreamHandler;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.Artist;
import com.app.livideo.models.Discover;
import com.app.livideo.models.EnterStreamData;
import com.app.livideo.models.Stream;
import com.app.livideo.models.validators.ArtistValidator;
import com.app.livideo.models.validators.StreamValidator;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.LVUtils;
import com.app.livideo.utils.ScreenDensityUtil;
import com.app.livideo.utils.VerticalWrapLayoutManager;

import java.util.LinkedHashMap;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnPageChange;

/**
 * Created by Eli on 4/5/2016.
 */
public class DiscoverAdapter extends RecyclerView.Adapter<DiscoverAdapter.DiscoverViewHolder> {

    private static final int TYPE_FEATURE = 0;
    private static final int TYPE_POPULAR_ARTISTS = 1;
    private static final int TYPE_LATEST_STREAMS = 2;
    private static final int TYPE_OPEN_MIC = 3;
    private static final int TYPE_CHANNEL_ARTISTS = 4;
    static final int discoverItemSize = 5;
    private final Discover discoverData;
    private final Activity mActivity;
    public FeatureViewHolder featureViewHolder;
    DiscoverFragment mFragment;
    View touch_blocker;
    @Inject Context mContext;
    @Inject LVSession mSession;
    @Inject RequestQueue mRequestQueue;
    DialogInterface.OnDismissListener mDismissListener = new DialogInterface.OnDismissListener() {
        @Override
        public void onDismiss(DialogInterface dialog) {
            touch_blocker.setVisibility(View.GONE);
        }
    };
    private List<String> mArtistTypes;
    private boolean isVisible = true;

    public DiscoverAdapter(Discover discoverData, DiscoverFragment fragment, List<String> artistTypes, View blocker, Activity activity) {
        LVInject.inject(this);
        this.discoverData = discoverData;
        this.mFragment = fragment;
        this.mArtistTypes = artistTypes;
        this.touch_blocker = blocker;
        this.mActivity = activity;
    }

    @Override
    public DiscoverViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case TYPE_FEATURE:
                ViewGroup featureView = (ViewGroup) mInflater.inflate(R.layout.discover_featured_view, parent, false);
                featureViewHolder = new FeatureViewHolder(featureView);
                return featureViewHolder;
            case TYPE_POPULAR_ARTISTS:
                ViewGroup popularArtistsView = (ViewGroup) mInflater.inflate(R.layout.discover_row, parent, false);
                return new PopularArtistsViewHolder(popularArtistsView);
            case TYPE_LATEST_STREAMS:
                ViewGroup latestStreamsView = (ViewGroup) mInflater.inflate(R.layout.discover_row, parent, false);
                return new LatestStreamsViewHolder(latestStreamsView);
            case TYPE_OPEN_MIC:
                ViewGroup openMicView = (ViewGroup) mInflater.inflate(R.layout.discover_row, parent, false);
                return new OpenMicsViewHolder(openMicView);
            case TYPE_CHANNEL_ARTISTS:
                ViewGroup channelArtistsView = (ViewGroup) mInflater.inflate(R.layout.discover_channel_artists_view, parent, false);
                return new ChannelArtistsViewHolder(channelArtistsView);
            default:
                return new DiscoverViewHolder(new View(parent.getContext()));
        }
    }

    @Override
    public void onBindViewHolder(DiscoverViewHolder holder, int position) {
        // Do nothign
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return discoverItemSize;
    }

    public void setAppIsVisible(boolean isVisible) {
        if (DiscoverViewHolder.featureAdapter != null) {
            DiscoverViewHolder.featureAdapter.setIsActive(isVisible);
        }
        this.isVisible = isVisible;
    }

    private void performStreamClick(final Stream stream) {
        // Handle cases where we do not allow video to be played
        if (!mFragment.isAdded() || stream == null)
            return;

        touch_blocker.setVisibility(View.VISIBLE);
        APICalls.requestEnterStream(mSession, mFragment.getActivity(), mContext, stream.getStreamId(), mRequestQueue, new EnterStreamHandler() {
            @Override
            public void RunCompleted(EnterStreamData enterStreamData) {
                if (mFragment.isAdded() && enterStreamData != null) {
                    switch (enterStreamData.getCode()) {
                        case AppConstants.STREAMCODE_SUCCESS: // public
                            LVUtils.startVideoPlayer(stream, enterStreamData, touch_blocker, isVisible, mFragment.getActivity());
                            break;
                        case AppConstants.STREAMCODE_NOT_SUBSCRIBED:
                        case AppConstants.STREAMCODE_IS_PRIVATE:
                            APICalls.requestArtistInfo(stream.getArtistId(), mFragment.getActivity(), mContext, mSession, mRequestQueue, new ArtistHandler() {
                                @Override
                                public void RunCompleted(boolean isAvailable, Artist artist) {
                                    if (isAvailable) {
                                        LVUtils.beginUserSubscriptionProcess(artist, mFragment.getActivity(), mSession, mRequestQueue, mDismissListener, new Runnable() {
                                            @Override
                                            public void run() {
                                                touch_blocker.setVisibility(View.GONE);
                                                mSession.setCurrentRunnable(null);
                                                performStreamClick(stream);
                                            }
                                        });
                                    } else {
                                        LVUtils.handleError(mContext.getString(R.string.invalid_artist_object), false, touch_blocker, mContext, mFragment.getActivity());
                                    }
                                }
                            });
                            break;
                        case AppConstants.STREAMCODE_NOT_EXISTS:
                            LVUtils.handleError(mContext.getString(R.string.stream_unavailable), false, touch_blocker, mContext, mFragment.getActivity());
                            break;
                        case AppConstants.STREAMCODE_OTHER_ERROR:
                            LVUtils.handleError(mContext.getString(R.string.retry_later), false, touch_blocker, mContext, mFragment.getActivity());
                            break;
                        default:
                            LVUtils.handleError(mContext.getString(R.string.api_error), false, touch_blocker, mContext, mFragment.getActivity());
                            break;
                    }
                }
            }
        });
    }

    public static class DiscoverViewHolder extends RecyclerView.ViewHolder {

        public static FeatureAdapter featureAdapter;
        public static DiscoverRowAdapter popularArtistsAdapter;
        public static DiscoverRowAdapter latestStreamsAdapter;
        public static DiscoverRowAdapter openMicsAdapter;
        public static DiscoverChannelsRowAdapter channelArtistsAdapter;

        public DiscoverViewHolder(View itemView) {
            super(itemView);
        }
    }

    public class FeatureViewHolder extends DiscoverViewHolder {

        @Bind(R.id.discover_view_pager) public ViewPager mViewPager;
        @Bind(R.id.view_pager_indicator) LinearLayout pager_indicator;
        int screenWidth;
        private ImageView[] dots;

        public FeatureViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            screenWidth = mContext.getResources().getDisplayMetrics().widthPixels;
            mViewPager.getLayoutParams().width = screenWidth;
            mViewPager.getLayoutParams().height = (screenWidth * 9) / 16;
            featureAdapter = new FeatureAdapter(mFragment, discoverData.getFeatures(), mContext);
            mViewPager.setAdapter(featureAdapter);
            mViewPager.setCurrentItem(0);
            setPageViewIndicator();
        }

        public void bind(int position) {
            // Do nothing
        }

        private void setPageViewIndicator() {
            dots = new ImageView[discoverData.getFeatures().size()];
            for (int i = 0; i < discoverData.getFeatures().size(); i++) {
                dots[i] = new ImageView(mViewPager.getContext());
                dots[i].setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.non_selected_item_dot));
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                params.setMargins(8, 0, 8, 0);
                pager_indicator.addView(dots[i], params);
            }
            if (dots != null && dots.length > 0)
                dots[0].setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.selected_item_dot));
        }

        @OnPageChange(value = R.id.discover_view_pager, callback = OnPageChange.Callback.PAGE_SELECTED)
        protected void onPageSelected(int position) {
            mFragment.restartAutoScroll();
            for (int i = 0; i < discoverData.getFeatures().size(); i++)
                dots[i].setImageResource(R.drawable.non_selected_item_dot);
            dots[position].setImageResource(R.drawable.selected_item_dot);
        }
    }

    public class PopularArtistsViewHolder extends DiscoverViewHolder {

        @Bind(R.id.channels_horizontal_view) RecyclerView popularArtistsRecyclerView;
        @Bind(R.id.channels_row_label) TextView popularArtistsTextView;

        private DiscoverRowAdapter.OnArtistClickedListener mArtistClickedListener = new DiscoverRowAdapter.OnArtistClickedListener() {
            @Override
            public void onArtistClicked(Artist artist) {
                if (isVisible) {
                    Intent intent = new Intent(mFragment.getActivity(), ArtistPageActivity.class);
                    intent.putExtra(AppConstants.ARTIST_KEY_INTENT, artist);
                    mFragment.getActivity().startActivityForResult(intent, AppConstants.CHANNELS);
                }
            }
        };

        public PopularArtistsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            // Headline
            popularArtistsTextView.setText(R.string.popular_artists);

            // Layout
            final LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
            popularArtistsRecyclerView.setLayoutManager(layoutManager);

            // View
            popularArtistsAdapter = new DiscoverRowAdapter(mFragment,
                    discoverData.getPopularArtists(),
                    (ScreenDensityUtil.getScreenWidthPixels(mContext, 1) * 5) / 12,
                    true,
                    mArtistClickedListener);
            popularArtistsRecyclerView.setAdapter(popularArtistsAdapter);
        }

        public void bind(int position) {
            // Do nothing
        }
    }

    public class LatestStreamsViewHolder extends DiscoverViewHolder {

        @Bind(R.id.channels_horizontal_view) RecyclerView latestArtistsRecyclerView;
        @Bind(R.id.channels_row_label) TextView latestArtistsTextView;
        //Handle what happens when user clicks on a stream
        private DiscoverRowAdapter.OnStreamClickedListener mStreamClickedListener = new DiscoverRowAdapter.OnStreamClickedListener() {
            @Override
            public void onStreamClicked(final Stream stream) {
                performStreamClick(stream);
            }
        };

        public LatestStreamsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            // Headline
            latestArtistsTextView.setText(R.string.latest_videos);

            // Layout
            final LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
            latestArtistsRecyclerView.setLayoutManager(layoutManager);

            // View
            latestStreamsAdapter = new DiscoverRowAdapter(mFragment, discoverData.getLatestStreams(), ScreenDensityUtil.getScreenWidthPixels(mContext, 4), mStreamClickedListener);
            latestArtistsRecyclerView.setAdapter(latestStreamsAdapter);
        }

        public void bind(int position) {
            // Do nothing
        }
    }

    public class OpenMicsViewHolder extends DiscoverViewHolder {

        @Bind(R.id.channels_horizontal_view) RecyclerView mRecyclerView;
        @Bind(R.id.channels_row_label) TextView label;
        //Handle what happens when user clicks on a stream
        private DiscoverRowAdapter.OnStreamClickedListener mStreamClickedListener = new DiscoverRowAdapter.OnStreamClickedListener() {
            @Override
            public void onStreamClicked(final Stream stream) {
                performStreamClick(stream);
            }
        };

        public OpenMicsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            // Headline
            label.setText(R.string.open_mic_caps);

            // Layout
            final LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
            mRecyclerView.setLayoutManager(layoutManager);

            // View
            openMicsAdapter = new DiscoverRowAdapter(mFragment, discoverData.getOpenMics(), ScreenDensityUtil.getScreenWidthPixels(mContext, 4), mStreamClickedListener);
            mRecyclerView.setAdapter(openMicsAdapter);
        }

        @OnClick(R.id.channels_row_label)
        public void openMicLaunch(){
            if (!StreamValidator.isValid(discoverData.getOpenMics()) || !isVisible)
                return;

            Artist artist = new Artist();
            artist.setArtistId(discoverData.getOpenMics().get(0).getArtistId());
            artist.setDisplayName(discoverData.getOpenMics().get(0).getDisplayName());

            if (ArtistValidator.isValid(artist)) {
                Intent intent = new Intent(mActivity, ArtistPageActivity.class);
                intent.putExtra(AppConstants.ACTIVITY_KEY_INTENT, AppConstants.CHANNELS);
                intent.putExtra(AppConstants.ARTIST_KEY_INTENT, artist);
                mActivity.startActivityForResult(intent, AppConstants.CHANNELS);
            } else {
                Log.e(DiscoverAdapter.class.getSimpleName(), "Invalid Artist");
            }
        }

        public void bind(int position) {
            // Do nothing
        }
    }

    public class ChannelArtistsViewHolder extends DiscoverViewHolder {

        @Bind(R.id.discover_channel_artists_recycler_view) RecyclerView mChannelArtistsRecyclerView;

        public ChannelArtistsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            // Layout
            VerticalWrapLayoutManager layoutManager = new VerticalWrapLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
            mChannelArtistsRecyclerView.setLayoutManager(layoutManager);

            // Artist Channels View
            LinkedHashMap<String, List<Artist>> artistMap = LVUtils.getMapArtistByCategorie(mArtistTypes, discoverData.getArtists());
            channelArtistsAdapter = new DiscoverChannelsRowAdapter(artistMap, mFragment.getActivity(), isVisible);
            mChannelArtistsRecyclerView.setAdapter(channelArtistsAdapter);
        }

        public void bind(int position) {
            // Do nothing
        }
    }
}