package com.app.livideo.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.livideo.R;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.Artist;
import com.app.livideo.models.Stream;
import com.app.livideo.models.validators.ArtistPictureValidator;
import com.app.livideo.models.validators.ArtistValidator;
import com.app.livideo.models.validators.StreamValidator;
import com.app.livideo.utils.LVUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Eli on 4/5/2016.
 */
public class DiscoverRowAdapter extends RecyclerView.Adapter<DiscoverRowAdapter.DiscoverRowViewHolder> {

    @Inject Context mContext;

    List<Artist> mPopularArtists;
    List<Stream> mLatestStreams;
    private int artistImageSize;
    boolean isPopularArtists;
    Fragment mFragment;
    private OnArtistClickedListener mArtistClickedListener;
    private OnStreamClickedListener mStreamClickedListener;

    public DiscoverRowAdapter(Fragment fragment, List<Artist> popularArtists, final int imageSize, final boolean isPopularArtists, DiscoverRowAdapter.OnArtistClickedListener mArtistClickedListener) {
        LVInject.inject(this);
        this.mPopularArtists = popularArtists;
        this.artistImageSize = imageSize;
        this.isPopularArtists = isPopularArtists;
        this.mFragment = fragment;
        this.mArtistClickedListener = mArtistClickedListener;
    }

    public DiscoverRowAdapter(Fragment fragment, List<Stream> latestStreams, int imageSize, DiscoverRowAdapter.OnStreamClickedListener onStreamClickedListener) {
        LVInject.inject(this);
        this.mLatestStreams = latestStreams;
        this.artistImageSize = imageSize;
        this.mFragment = fragment;
        this.mStreamClickedListener = onStreamClickedListener;
    }

    @Override
    public DiscoverRowViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View channelsColumnView = LayoutInflater.from(parent.getContext()).inflate(R.layout.channels_column, parent, false);
        if (isPopularArtists)
            return new DiscoverRowViewHolder(channelsColumnView, mArtistClickedListener);
        else
            return new DiscoverRowViewHolder(channelsColumnView, mStreamClickedListener);
    }

    @Override
    public void onBindViewHolder(DiscoverRowViewHolder holder, int position) {
        if (isPopularArtists) {
            Artist artist = mPopularArtists.get(position);
            if (ArtistValidator.isValid(artist))
                holder.bind(artist);
        } else {
            Stream stream = mLatestStreams.get(position);
            if (StreamValidator.isValid(stream))
                holder.bind(stream);
        }
    }

    @Override
    public int getItemCount() {
        if (isPopularArtists)
            return mPopularArtists.size();
        else
            return mLatestStreams.size();
    }


    public class DiscoverRowViewHolder extends RecyclerView.ViewHolder {
        Artist mArtist;
        Stream mStream;
        String url;
        OnArtistClickedListener mArtistClickedListener;
        OnStreamClickedListener mStreamClickedListener;

        @Bind(R.id.channels_column_img_label) TextView artistTextView;
        @Bind(R.id.channels_column_img_view) ImageView artistImageView;
        @Bind(R.id.channels_column_img_progress_bar) ImageView imageLoading;
        @Bind(R.id.channels_column_img_card_view) CardView mCardView;

        public DiscoverRowViewHolder(View itemView, OnArtistClickedListener artistClickedListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mArtistClickedListener = artistClickedListener;
            initialize();
        }

        public DiscoverRowViewHolder(View itemView, OnStreamClickedListener streamClickedListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mStreamClickedListener = streamClickedListener;
            initialize();
        }

        View.OnClickListener onStreamClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mStreamClickedListener != null && StreamValidator.isValid(mStream))
                    mStreamClickedListener.onStreamClicked(mStream);
            }
        };

        View.OnClickListener onArtistClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mArtistClickedListener != null && ArtistValidator.isValid(mArtist))
                    mArtistClickedListener.onArtistClicked(mArtist);
            }
        };

        private void initialize() {
            // Label
            artistTextView.setMinWidth(artistImageSize);
            artistTextView.setMaxWidth(artistImageSize);
            Glide.with(mContext).load(R.drawable.loading_anim).asGif().into(imageLoading);

            // Image
            artistImageView.setMinimumHeight(artistImageSize);
            artistImageView.setMinimumWidth(artistImageSize);
            artistImageView.getLayoutParams().width = artistImageSize;
            artistImageView.getLayoutParams().height = artistImageSize;
            artistImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

            // Click listeners
            if (isPopularArtists) {
                mCardView.setOnClickListener(onArtistClick);
            } else {
                mCardView.setOnClickListener(onStreamClick);
            }
        }

        public void bind(Artist artist) {
            mArtist = artist;

            // Label
            artistTextView.setText(artist.getDisplayName());

            // Image
            if (ArtistValidator.isValid(mArtist) && ArtistPictureValidator.isValid(mArtist.getArtistPictures())) {
                url = mArtist.getArtistPictures().get(mArtist.getArtistPictures().size() - 1).getUrl();
                imageLoading.setVisibility(View.VISIBLE);
                setImage();
            }
        }

        private void setImage() {
            Glide.with(artistImageView.getContext())
                    .load(url)
                    .centerCrop()
                    .override(artistImageSize, artistImageSize)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            if (imageLoading != null)
                                imageLoading.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            if (imageLoading != null)
                                imageLoading.setVisibility(View.GONE);
                            return false;
                        }
                    }).into(artistImageView);
        }

        public void bind(Stream stream) {
            mStream = stream;

            // Label
            artistTextView.setText(mStream.getHeadline());

            // Image
            if (StreamValidator.isValid(mStream)) {
                url = LVUtils.getStreamThumbnailUrl(mStream.getStreamId());
                imageLoading.setVisibility(View.VISIBLE);
                setImage();
            }
        }
    }

    public interface OnArtistClickedListener {
        void onArtistClicked(Artist artist);
    }

    public interface OnStreamClickedListener {
        void onStreamClicked(Stream stream);
    }
}
