package com.app.livideo.adapters;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.livideo.R;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.Artist;
import com.app.livideo.models.validators.ArtistValidator;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.ScreenDensityUtil;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by eli davila on 9/9/2015.
 */
public class DiscoverColumnAdapter extends RecyclerView.Adapter<DiscoverColumnAdapter.ArtistViewHolder> {
    @Inject Context mContext;
    private final OnArtistClickedListener mListener;
    private List<Artist> artistList;
    int artistImageSize, artistNameHeightSize;

    public DiscoverColumnAdapter(List<Artist> artistList, OnArtistClickedListener mListener) {
        LVInject.inject(this);
        this.artistList = artistList;
        this.mListener = mListener;
        this.artistImageSize = ScreenDensityUtil.getImageSize(mContext);
        this.artistNameHeightSize = ScreenDensityUtil.getImageSize(mContext) / 5;
    }

    @Override
    public ArtistViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View channelsColumnView = LayoutInflater.from(parent.getContext()).inflate(R.layout.channels_column, parent, false);
        return new ArtistViewHolder(channelsColumnView, mListener);
    }

    @Override
    public void onBindViewHolder(ArtistViewHolder viewHolder, int position) {
        Artist artist = artistList.get(position);
        if (ArtistValidator.isValid(artist))
            viewHolder.bind(artist);
    }

    public class ArtistViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        final OnArtistClickedListener mListener;
        Artist mArtist;
        String url;
        boolean shouldStopUpdatingImage = false;
        int retriesSoFar = 1;
        int mUpdateInterval;

        @Bind(R.id.channels_column_img_label) TextView artistTextView;
        @Bind(R.id.channels_column_img_view) ImageView artistImageView;
        @Bind(R.id.channels_column_img_progress_bar) ImageView imageLoading;

        public ArtistViewHolder(View itemView, OnArtistClickedListener listener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
            mListener = listener;
            Glide.with(mContext).load(R.drawable.loading_anim).asGif().into(imageLoading);
        }

        @Override
        public void onClick(View v) {
            if (mListener != null && ArtistValidator.isValid(mArtist))
                mListener.onArtistClicked(mArtist);
        }

        public void bind(Artist artist) {
            mArtist = artist;

            // Artist Name
            artistTextView.setText(artist.getDisplayName());
            artistTextView.setMinWidth(artistImageSize);
            artistTextView.setMaxWidth(artistImageSize);
            artistTextView.setMinHeight(artistNameHeightSize);

            // Artist Image
            artistImageView.setMinimumHeight(artistImageSize);
            artistImageView.setMinimumWidth(artistImageSize);
            artistImageView.getLayoutParams().width = artistImageSize;
            artistImageView.getLayoutParams().height = artistImageSize;
            artistImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

            // Load Image
            url = mArtist.getArtistPictures().get(mArtist.getArtistPictures().size() - 1).getUrl();
            imageLoading.setVisibility(View.VISIBLE);
            setImage(false);
        }

        private void setImage(final boolean isUpdating) {
            Glide.with(mContext)
                    .load(url)
                    .centerCrop()
                    .override(artistImageSize, artistImageSize)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            if (url == null) {
                                if (imageLoading != null) imageLoading.setVisibility(View.GONE);
                            } else if (!isUpdating) {
                                retryUpdatingImage();
                            }
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            shouldStopUpdatingImage = true;
                            if (imageLoading != null)
                                imageLoading.setVisibility(View.GONE);
                            return false;
                        }
                    }).into(artistImageView);
        }

        private void retryUpdatingImage() {
            if (shouldStopUpdatingImage || retriesSoFar > 10) {
                if (imageLoading != null)
                    imageLoading.setVisibility(View.GONE);
                return;
            }

            if (retriesSoFar <= 5) {
                mUpdateInterval = AppConstants.UPDATE_IMAGE_FIRST_INTERVAL;
            } else {
                mUpdateInterval = AppConstants.UPDATE_IMAGE_SECOND_INTERVAL;
            }

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (!shouldStopUpdatingImage) {
                        retriesSoFar++;

                        // Update url if needed
                        url = mArtist.getArtistPictures().get(mArtist.getArtistPictures().size() - 1).getUrl();

                        setImage(true);
                        retryUpdatingImage();
                    }
                }
            }, mUpdateInterval);
        }
    }

    @Override
    public int getItemCount() {
        return artistList.size();
    }

    public interface OnArtistClickedListener {
        void onArtistClicked(Artist artist);
    }
}
