package com.app.livideo.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.livideo.R;
import com.app.livideo.models.User;
import com.app.livideo.models.WhisperRequest;
import com.app.livideo.models.validators.UserImageValidator;
import com.app.livideo.models.validators.WhisperValidator;
import com.app.livideo.utils.PicassoCircleTransform;
import com.app.livideo.utils.ScreenDensityUtil;
import com.app.livideo.utils.TextUtils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Eli on 5/4/2016.
 */
public class WhispersAdapter extends RecyclerView.Adapter<WhispersAdapter.ViewHolder> {

    List<WhisperRequest> mChatWhispers;
    int imageSize;
    Context mContext;
    WhisperClickedListener mWhisperClickedListener;

    public WhispersAdapter(List<WhisperRequest> chatWhispers, Context context, final WhisperClickedListener whisperClickedListener) {
        this.mChatWhispers = chatWhispers;
        this.mContext = context;
        this.imageSize = ScreenDensityUtil.getScreenHeightPixels(mContext, 18);
        this.mWhisperClickedListener = whisperClickedListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_whisper, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        WhisperRequest chatWhisper = mChatWhispers.get(position);
        if (WhisperValidator.isValid(chatWhisper))
            holder.bind(chatWhisper);
    }

    @Override
    public int getItemCount() {
        return mChatWhispers.size();
    }

    public void bind(List<WhisperRequest> mWhisperRequests) {
        this.mChatWhispers = mWhisperRequests;
        notifyDataSetChanged();
    }

    public interface WhisperClickedListener {
        void onWhisperItemClick(User chatWhisper);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.whisper_img) ImageView image_view;
        @Bind(R.id.whisper_name) TextView text_view;
        @Bind(R.id.whisper_message) TextView msg_text_view;
        @Bind(R.id.whisper_progress_bar) ProgressBar mProgressBar;
        @Bind(R.id.whisper_unread_circle_view) View circleView;
        @Bind(R.id.whisper_unread_text_view) TextView unreadCountTextView;

        WhisperRequest mWhisperRequest;
        User mChatWhisper;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(WhisperRequest whisperRequest) {
            mWhisperRequest = whisperRequest;
            mChatWhisper = whisperRequest.getWhisperUser();
            image_view.setMinimumHeight(imageSize);
            image_view.setMinimumWidth(imageSize);

            if (!TextUtils.isNullOrEmpty(mChatWhisper.getDisplayName()))
                text_view.setText(mChatWhisper.getDisplayName());

            if (!TextUtils.isNullOrEmpty(mWhisperRequest.getLastMessage())) {
                msg_text_view.setText(
                        (mWhisperRequest.getLastMessage().contains(mContext.getString(R.string.s3_amazon_url)))
                                ? mContext.getString(R.string.image)
                                : mWhisperRequest.getLastMessage());
            }

            setUnreadCount();
            if (UserImageValidator.isValid(mChatWhisper.getPictures())) {
                setProfileImage();
            } else {
                Picasso.with(mContext).load(R.drawable.profile).resize(imageSize, imageSize).transform(new PicassoCircleTransform()).into(image_view);
            }
        }

        private void setProfileImage() {
            mProgressBar.setVisibility(View.VISIBLE);
            Picasso.with(mContext)
                    .load(mChatWhisper.getPictures().get(mChatWhisper.getPictures().size() - 1).getUrl())
                    .resize(imageSize, imageSize)
                    .transform(new PicassoCircleTransform())
                    .into(image_view, new Callback() {
                        @Override
                        public void onSuccess() {
                            mProgressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            mProgressBar.setVisibility(View.GONE);
                            circleView.setVisibility(View.GONE);
                        }
                    });
        }

        private void setUnreadCount() {
            if (!TextUtils.isNullOrEmpty(mWhisperRequest.getUnreadCount()) && Integer.valueOf(mWhisperRequest.getUnreadCount()) > 0) {
                if (Integer.valueOf(mWhisperRequest.getUnreadCount()) < 10) {
                    unreadCountTextView.setPadding(1, 0, 0, 0);
                }
                unreadCountTextView.setText(mWhisperRequest.getUnreadCount());
                circleView.setVisibility(View.VISIBLE);
            } else {
                circleView.setVisibility(View.GONE);
            }
        }

        @OnClick(R.id.whisper_card_view)
        protected void onWhisperItemClick() {
            mWhisperClickedListener.onWhisperItemClick(mChatWhisper);
        }
    }
}
