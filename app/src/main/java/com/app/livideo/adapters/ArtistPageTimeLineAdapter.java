package com.app.livideo.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.app.livideo.R;
import com.app.livideo.activities.ArtistPageActivity;
import com.app.livideo.api.APICalls;
import com.app.livideo.dialogs.CustomAlertDialog;
import com.app.livideo.interfaces.DataAvailabilityHandler;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.Artist;
import com.app.livideo.models.ArtistPage;
import com.app.livideo.models.ArtistPicture;
import com.app.livideo.models.Stream;
import com.app.livideo.models.validators.ArtistPageValidator;
import com.app.livideo.models.validators.ArtistPictureValidator;
import com.app.livideo.models.validators.ArtistValidator;
import com.app.livideo.models.validators.TimeLineValidator;
import com.app.livideo.models.validators.UserValidator;
import com.app.livideo.utils.AndroidUtils;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.LVUtils;
import com.app.livideo.utils.PicassoCircleTransform;
import com.app.livideo.utils.ScreenDensityUtil;
import com.app.livideo.utils.TimeUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;

/**
 * Created by Eli on 4/13/2016.
 */
public class ArtistPageTimeLineAdapter extends FragmentStatePagerAdapter {

    static ArtistPage mArtistPage;
    static ArtistPageActivity mActivity;
    static ImageView mBellButton;

    public ArtistPageTimeLineAdapter(ArtistPageActivity activity, FragmentManager fm, ArtistPage artistPage, ImageView bellButton) {
        super(fm);
        mActivity = activity;
        mArtistPage = artistPage;
        mBellButton = bellButton;
    }

    @Override
    public Fragment getItem(int position) {
        return (ArtistPageValidator.isValid(mArtistPage)) ? TimeLinePageFragment.newInstance(position) : new Fragment();
    }

    @Override
    public int getCount() {
        return (mArtistPage != null && TimeLineValidator.isValid(mArtistPage.getTimeLines())) ? mArtistPage.getTimeLines().size() : 0;
    }

    public void bind(ArtistPage artistPage) {
        mArtistPage = artistPage;
        notifyDataSetChanged();
    }

    public static class TimeLinePageFragment extends Fragment {

        private static final int TYPE_STREAM = 0, TYPE_PICTURE = 1;

        @Inject Context mContext;
        @Inject LVSession mSession;
        @Inject RequestQueue mRequestQueue;

        @Bind(R.id.time_line_main_img) ImageView main_img_view;
        @Bind(R.id.time_line_play_img) ImageView play_img_view;
        @Bind(R.id.time_line_lock_img) ImageView lock_img_view;
        @Bind(R.id.time_line_artist_icon) ImageView artist_details_img_view;
        @Bind(R.id.time_line_artist_name) TextView artist_details_name_view;
        @Bind(R.id.time_line_artist_headline) TextView artist_details_headline_view;
        @Bind(R.id.time_line_artist_time_stamp) TextView artist_details_time_stamp_view;
        @Bind(R.id.time_line_full_access_text_view) TextView full_access_text_view;
        @Bind(R.id.time_line_full_access_card_view) CardView full_access_card_view;
        @Bind(R.id.time_line_progress_bar) ImageView mProgressBar;

        private Object mTimeLine;
        private int mPosition, imageSize, objectType;
        Stream mStream;

        static TimeLinePageFragment newInstance(int position) {
            TimeLinePageFragment f = new TimeLinePageFragment();
            Bundle args = new Bundle();
            args.putInt(ArtistPage.Keys.OBJECT_TYPE, position);
            f.setArguments(args);
            return f;
        }

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            LVInject.inject(this);
            if (mSession == null || !UserValidator.isValid(mSession.getUser())) {
                AndroidUtils.restartApp(mContext);
                return; // APP ERROR
            }

            if (ArtistPageValidator.isValid(mArtistPage)) {
                startTimeLineView();
            } else if (mActivity != null) {
                mActivity.onRefresh();
            }
        }

        private void startTimeLineView() {
            imageSize = ScreenDensityUtil.getScreenWidthPixels(mContext, 1);
            mPosition = (getArguments() != null) ? getArguments().getInt(ArtistPage.Keys.OBJECT_TYPE) : 0;
            mTimeLine = mArtistPage.getTimeLines().get(mPosition);
            setObjectType();
            setSubscriptionType();
            setRetainInstance(true);
        }

        @Override
        public void onResume() {
            super.onResume();
            if (mSession == null || !UserValidator.isValid(mSession.getUser())) {
                AndroidUtils.restartApp(mContext);
                return; // App error
            }
        }

        private void setSubscriptionType() {
            if (mArtistPage.getArtist().getIsHero() == 1) {
                mActivity.mSubscriptionType = AppConstants.SUB_TYPE_HERO;
            } else if (mArtistPage.getArtist().getIsWatching() == 1) {
                mActivity.mSubscriptionType = AppConstants.SUB_TYPE_FAV;
            } else if (mArtistPage.getUserInfo().getHasMembership() == 1) {
                mActivity.mSubscriptionType = AppConstants.SUB_TYPE_HAS_MEMBERSHIP;    // broadcasters as well
            } else {
                mActivity.mSubscriptionType = AppConstants.SUB_TYPE_NONE;
            }
        }

        private void setObjectType() {
            // IS STREAM
            try {
                ((ArtistPicture) (mTimeLine)).getPictureId();
            } catch (Throwable ignore) {
                objectType = TYPE_STREAM;
            }

            // IS PICTURE
            try {
                ((Stream) (mTimeLine)).getStreamId();
            } catch (Throwable ignore) {
                objectType = TYPE_PICTURE;
            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.item_artist_page, container, false);
            ButterKnife.bind(this, rootView);

            if (mProgressBar != null) {
                Glide.with(mContext).load(R.drawable.loading_anim).asGif().into(mProgressBar);
                mProgressBar.setOnTouchListener(AppConstants.touchListner);
            }

            if (mBellButton != null) {
                mBellButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onFullAccessClick();
                    }
                });
            }

            if (mTimeLine != null) {
                switch (objectType) {
                    case TYPE_PICTURE:
                        createPictureView();
                        break;
                    case TYPE_STREAM:
                        createStreamView();
                        break;
                }
            }

            return rootView;
        }

        private void createStreamView() {
            mStream = ((Stream) mTimeLine);
            play_img_view.setVisibility(View.VISIBLE);
            if (ArtistValidator.isValid(mStream.getOpenMicUser())) {
                setArtistDetailsView(mStream.getOpenMicUser());
            } else {
                setArtistDetailsView(mArtistPage.getArtist());
            }
            setMainImage(LVUtils.getStreamThumbnailUrl(mStream.getStreamId()));
            if (!mStream.isPublic() && mActivity.mSubscriptionType == AppConstants.SUB_TYPE_NONE) {
                lock_img_view.setVisibility(View.VISIBLE);
            }
        }

        private void createPictureView() {
            setMainImage(((ArtistPicture) mTimeLine).getUrl());
            if (mPosition == 0) {
                if (!(((ArtistPicture) mTimeLine).getUserId().equalsIgnoreCase(mSession.getUser().getUserID()))) {
                    full_access_card_view.setVisibility(View.VISIBLE);
                    setFullAccessView();
                }
            }
        }

        private void setFullAccessView() {
            switch (mActivity.mSubscriptionType) {
                case AppConstants.SUB_TYPE_HERO:
                    full_access_card_view.setVisibility(View.GONE);
                    mBellButton.setVisibility(View.GONE);
                    break;
                case AppConstants.SUB_TYPE_FAV:
                    full_access_card_view.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.livideo_grey));
                    full_access_text_view.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check_white, 0, 0, 0);
                    full_access_text_view.setText(R.string.favorited);
                    mBellButton.setImageResource(R.drawable.ic_notification_on_wshadows);
                    break;
                case AppConstants.SUB_TYPE_HAS_MEMBERSHIP:
                    full_access_card_view.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.livideo_accent));
                    full_access_text_view.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_favorite, 0, 0, 0);
                    full_access_text_view.setText(R.string.favorite);
                    mBellButton.setImageResource(R.drawable.ic_notification_off_wshadows);
                    break;
                case AppConstants.SUB_TYPE_NONE:
                default:
                    mBellButton.setVisibility(View.GONE);
                    full_access_card_view.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.livideo_accent));
                    full_access_text_view.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_video_locked_fill, 0, 0, 0);
                    full_access_text_view.setText(R.string.get_full_access);
                    break;
            }
        }

        private void setArtistDetailsView(final Artist mArtist) {
            if ((ArtistValidator.isValid(mArtist)) && ArtistPictureValidator.isValid(mArtist.getArtistPictures())) {
                final int artistImageSize = ScreenDensityUtil.getScreenHeightPixels(mContext, 15);
                artist_details_img_view.setMinimumWidth(artistImageSize);
                artist_details_img_view.setMinimumHeight(artistImageSize);
                Picasso.with(mContext)
                        .load(mArtist.getArtistPictures().get(mArtist.getArtistPictures().size() - 1).getUrl())
                        .resize(artistImageSize, artistImageSize)
                        .transform(new PicassoCircleTransform())
                        .into(artist_details_img_view, new Callback() {
                            @Override
                            public void onSuccess() {
                                artist_details_headline_view.setText(((Stream) mTimeLine).getHeadline());
                                artist_details_name_view.setText(mArtist.getDisplayName());
                                artist_details_time_stamp_view.setText(TimeUtils.convertTimeForStream(mStream.getTime(), mContext));
                            }

                            @Override
                            public void onError() {

                            }
                        });
            }
        }

        private void setMainImage(final String url) {
            main_img_view.setScaleType(ImageView.ScaleType.CENTER_CROP);
            mProgressBar.setVisibility(View.VISIBLE);
            Glide.with(mContext)
                    .load(url)
                    .override(imageSize, imageSize)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            if (mProgressBar != null)
                                mProgressBar.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            if (mProgressBar != null)
                                mProgressBar.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(main_img_view);
        }

        @OnClick(R.id.time_line_full_access_card_view)
        protected void onFullAccessClick() {
            switch (mActivity.mSubscriptionType) {
                case AppConstants.SUB_TYPE_HERO:
//                    dialog = new CustomAlertDialog(mActivity, getString(R.string.how_to_unsubscribe), false);
//                    dialog.show();
                    break;
                case AppConstants.SUB_TYPE_FAV:
                    mProgressBar.setVisibility(View.VISIBLE);
                    full_access_card_view.setEnabled(false);
                    mBellButton.setEnabled(false);
                    mActivity.touch_blocker.setVisibility(View.VISIBLE);
                    APICalls.requestRemoveFromFavorites(mActivity, mContext, mArtistPage.getArtist().getArtistId(), mSession, mRequestQueue, new DataAvailabilityHandler() {
                        @Override
                        public void RunCompleted(boolean isAvailable) {
                            mProgressBar.setVisibility(View.GONE);
                            full_access_card_view.setEnabled(true);
                            mBellButton.setEnabled(true);
                            mActivity.touch_blocker.setVisibility(View.GONE);

                            if (isAvailable) {
                                if (!isAdded() || !isVisible()) {
                                    mArtistPage.getArtist().setIsWatching(0);
                                    setSubscriptionType();
                                    setFullAccessView();
                                } else {
                                    CustomAlertDialog dialog = new CustomAlertDialog(mActivity, mActivity.getString(R.string.notifications_disabled), false);
                                    dialog.show();
                                    dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                        @Override
                                        public void onDismiss(DialogInterface dialog) {
                                            mArtistPage.getArtist().setIsWatching(0);
                                            setSubscriptionType();
                                            setFullAccessView();
                                        }
                                    });
                                }
                            } else {
                                CustomAlertDialog dialog = new CustomAlertDialog(mActivity, mActivity.getString(R.string.retry_later), false);
                                dialog.show();
                            }
                        }
                    });

                    break;
                case AppConstants.SUB_TYPE_HAS_MEMBERSHIP:
                    mProgressBar.setVisibility(View.VISIBLE);
                    full_access_card_view.setEnabled(false);
                    mBellButton.setEnabled(false);
                    mActivity.touch_blocker.setVisibility(View.VISIBLE);
                    APICalls.requestAddToFavorites(mActivity, mContext, mArtistPage.getArtist().getArtistId(), mSession, mRequestQueue, new DataAvailabilityHandler() {
                        @Override
                        public void RunCompleted(boolean isAvailable) {
                            mProgressBar.setVisibility(View.GONE);
                            full_access_card_view.setEnabled(true);
                            mBellButton.setEnabled(true);
                            mActivity.touch_blocker.setVisibility(View.GONE);
                            if (isAvailable) {
                                mArtistPage.getArtist().setIsWatching(1);
                                setSubscriptionType();
                                setFullAccessView();
                            } else {
                                CustomAlertDialog dialog = new CustomAlertDialog(mActivity, getString(R.string.retry_later), false);
                                dialog.show();
                            }
                        }
                    });

                    break;
                case AppConstants.SUB_TYPE_NONE:
                default:
                    mProgressBar.setVisibility(View.VISIBLE);
                    LVUtils.beginUserSubscriptionProcess(mArtistPage.getArtist(), mActivity, mSession, mRequestQueue
                            , new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    mProgressBar.setVisibility(View.GONE);
                                }
                            }, new Runnable() {
                                @Override
                                public void run() {
                                    mProgressBar.setVisibility(View.GONE);
                                    mSession.setCurrentRunnable(null);
                                    mArtistPage.getArtist().setIsHero(1);
                                    mActivity.mSubscriptionType = AppConstants.SUB_TYPE_HERO;
                                    setSubscriptionType();
                                    setFullAccessView();
                                    Intent intent = mActivity.getIntent();
                                    mActivity.finish();
                                    startActivity(intent);
                                }
                            });
                    break;
            }
        }

        @OnClick(R.id.time_line_play_img)
        protected void onPlayButtonClick() {
            if (objectType == TYPE_STREAM) {
                mActivity.performStreamClick((Stream) (mTimeLine));
            }
        }

        @OnLongClick(R.id.time_line_play_img)
        protected boolean onPlayButtonLockClick() {
            if (objectType == TYPE_STREAM) {
                mActivity.performStreamLongClick((Stream) (mTimeLine));
            }
            return true;
        }
    }
}
