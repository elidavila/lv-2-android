package com.app.livideo.animation;

import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.TextView;

/**
 * Created by Alex on 07.10.2015.
 */
public class CircleAngleAnimation extends Animation {

    private Circle circle;

    private float oldAngle;
    private float newAngle;
    private float currentAngle;
    private TextView myText;

    public CircleAngleAnimation(Circle circle, int newAngle, TextView mText) {
        this.oldAngle = circle.getAngle();
        this.newAngle = newAngle;
        this.circle = circle;
        this.myText = mText;
        super.setDuration((long) 1000);
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation transformation) {
        float angle = oldAngle + ((newAngle - oldAngle) * interpolatedTime);
        myText.setText(String.valueOf((int) (angle / 3.6)) + "%");
        currentAngle = angle;
        circle.setAngle(angle);
        circle.requestLayout();
    }

    public float getCurrentAngel() {
        return currentAngle;
    }


}