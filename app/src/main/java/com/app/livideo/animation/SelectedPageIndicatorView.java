package com.app.livideo.animation;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;

import com.app.livideo.R;

/**
 * Created by Alex on 07.10.2015.
 */
public class SelectedPageIndicatorView extends View {

    private Bitmap indicatorBitmap;
    private float mDensity;
    private int indicatorIndex = 1;
    private int indexWidth;
    private int indicatorWidth;
    private int indicatorHeight;
    private int indexRadius;
    private Paint paintCWhite;
    private Paint paintCBlack;


    public SelectedPageIndicatorView(Context context) {
        super(context);
        init(context);
    }

    public SelectedPageIndicatorView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public SelectedPageIndicatorView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context con){
        mDensity = con.getResources().getDisplayMetrics().density;
        indicatorWidth = (int)(60*mDensity );
        indicatorHeight = (int)(20*mDensity);
        indexWidth =  indicatorWidth/4;
        indexRadius = (int)(3*mDensity);

        indicatorBitmap = Bitmap.createBitmap( indicatorWidth, indicatorHeight, Bitmap.Config.ARGB_8888);

        paintCBlack = new Paint();
        paintCBlack.setAntiAlias(true);
        paintCBlack.setStyle(Paint.Style.FILL);
        paintCBlack.setColor(ContextCompat.getColor(con, R.color.livideo_dark_grey));

        paintCWhite = new Paint();
        paintCWhite.setAntiAlias(true);
        paintCWhite.setStyle(Paint.Style.FILL);
        paintCWhite.setColor(ContextCompat.getColor(con, R.color.livideo_grey));
        reDrawBitmap();
    }

    private void reDrawBitmap(){
        indicatorBitmap = Bitmap.createBitmap( indicatorWidth, indicatorHeight, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(indicatorBitmap);
        for(int i = 0; i<4; i++){
            if(i == indicatorIndex-1 ){
                canvas.drawCircle((indexWidth*i)+ (indexWidth / 2), indicatorHeight / 2, indexRadius, paintCWhite);
            }else{
                canvas.drawCircle((indexWidth*i)+(indexWidth / 2), indicatorHeight / 2, indexRadius, paintCBlack);
            }
        }

    }


    @Override
    protected void onDraw(Canvas can){
        can.drawBitmap(indicatorBitmap,can.getWidth()/2 - indicatorWidth/2, can.getHeight()/2 - indicatorHeight/2, null );
    }


    public void setIndicatorIndex(int in){
        indicatorIndex = in;
    }

    public int getIndicatorIndex(){
        return indicatorIndex;
    }

    @Override
    public void invalidate(){
        reDrawBitmap();
        super.invalidate();
    }




}
