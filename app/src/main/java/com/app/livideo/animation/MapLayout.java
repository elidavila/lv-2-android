package com.app.livideo.animation;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.livideo.R;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Alex on 08.10.2015.
 */
public class MapLayout extends RelativeLayout {

    private int width;
    private float mDensity;
    private TextView countryText;
    private TextView countText;
    private RelativeLayout infoLayout;
    private Map<String,Cords> mapCords;
    private Context mContext;
    private Map<String, String> countryCounts;

    public MapLayout(Context context) {
        super(context);
        init(context);
    }

    public MapLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public MapLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context){
        mContext = context;
        mDensity = context.getResources().getDisplayMetrics().density;
        width = (int)(25*mDensity);
        mapCords = new HashMap<String, Cords>();
        mapCords.put(getContext().getString(R.string.america), new Cords(40, 47) );
        mapCords.put(getContext().getString(R.string.europ), new Cords(163, 37) );
        mapCords.put(getContext().getString(R.string.asia), new Cords(240, 15));
        mapCords.put(getContext().getString(R.string.africa), new Cords(180, 80));
        mapCords.put(getContext().getString(R.string.australia), new Cords(305, 120));
        countryCounts = new HashMap<String, String>();
    }

    /**
     * setCCI means set Country Text, Count Text and Info RelativeLayout
     * @param cT Country Text
     * @param countT the count Text
     * @param rl the RelativeLayout with the two textest
     */
    public void setCCI(TextView cT, TextView countT,RelativeLayout rl){
        countryText = cT;
        countText = countT;
        infoLayout = rl;
    }


    private void addViews(){

        if(this.getChildCount() > 0) {
            this.removeAllViews();
        }

        if(!countryCounts.isEmpty()) {


            for (Map.Entry<String,String> entry :countryCounts.entrySet()) {

                final String key = entry.getKey();
                final String value = entry.getValue();
                if(mapCords.containsKey(key) ) {
                    View pointView = new View(mContext);
                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(width, width);
                    pointView.setLayoutParams(params);
                    Drawable pointB = ContextCompat.getDrawable(mContext, R.drawable.ic_record_menu);
                    pointView.setBackground(pointB);
                    pointView.setX(mapCords.get(key).x * mDensity);
                    pointView.setY(mapCords.get(key).y * mDensity);
                    pointView.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            countryText.setText(key);
                            countText.setText(value);
                            infoLayout.setVisibility(VISIBLE);
                        }
                    });
                    this.addView(pointView);
                }
            }
        }
        invalidate();
    }

    public void setCountryCounts(Map<String,String> map){
        countryCounts = map;
        addViews();
    }


    private class Cords {
        Integer x;
        Integer y;

        Cords(Integer i,Integer j) {
            this.x = i;
            this.y = j;
        }
    }



}
