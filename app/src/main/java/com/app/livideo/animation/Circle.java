package com.app.livideo.animation;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;

import com.app.livideo.R;

/**
 * Created by Alex on 07.10.2015.
 */
public class Circle extends View {

    private static final int START_ANGLE_POINT = 270;
    private static final int radius = 100;
    private final Paint paint;
    private final RectF rect;
    private float mDensity;
    private Bitmap circleBackBitmap;

    private float angle;

    public Circle(Context context, AttributeSet attrs) {
        super(context, attrs);

        mDensity = context.getResources().getDisplayMetrics().density;
        final int strokeWidth = (int) (10 * mDensity);

        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(strokeWidth);
        //Circle color
        paint.setColor(ContextCompat.getColor(context, R.color.livideo_teal_primary));

        //setting the size
        rect = new RectF((strokeWidth / 2), (strokeWidth / 2), radius * mDensity - (strokeWidth / 2), radius * mDensity - (strokeWidth / 2));

        circleBackBitmap = Bitmap.createBitmap((int) (radius * mDensity), (int) (radius * mDensity), Bitmap.Config.ARGB_8888); // this creates a MUTABLE bitmap
        Canvas canvas = new Canvas(circleBackBitmap);
        Paint paintc1 = new Paint();
        paintc1.setAntiAlias(true);
        paintc1.setStyle(Paint.Style.FILL);
        paintc1.setColor(ContextCompat.getColor(context, R.color.livideo_dark_grey));
        canvas.drawCircle((int) ((radius * mDensity) / 2), (int) ((radius * mDensity) / 2), (int) ((radius * mDensity) / 2), paintc1);


        Paint paintc2 = new Paint();
        paintc2.setAntiAlias(true);
        paintc2.setStyle(Paint.Style.FILL);
        paintc2.setColor(ContextCompat.getColor(context, R.color.livideo_header_footer_background));

        canvas.drawCircle((int) ((radius * mDensity) / 2), (int) ((radius * mDensity) / 2), (int) (((radius * mDensity) / 2) - strokeWidth), paintc2);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawBitmap(circleBackBitmap, 0, 0, null);
        canvas.drawArc(rect, START_ANGLE_POINT, angle, false, paint);
    }

    public float getAngle() {
        return angle;
    }

    public void setAngle(float angle) {
        this.angle = angle;
    }
}
