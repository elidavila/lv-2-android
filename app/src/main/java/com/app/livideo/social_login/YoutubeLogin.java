package com.app.livideo.social_login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;

import com.android.volley.RequestQueue;
import com.app.livideo.BuildConfig;
import com.app.livideo.R;
import com.app.livideo.activities.ArtistSocialSignUp;
import com.app.livideo.activities.EnterPhoneNumberActivity;
import com.app.livideo.api.APICalls;
import com.app.livideo.dialogs.VerifyNewDeviceDialog;
import com.app.livideo.dialogs.YouTubeOAuthDialog;
import com.app.livideo.interfaces.LoginHandler;
import com.app.livideo.interfaces.StringResultHandler;
import com.app.livideo.interfaces.YouTubeUserDetailsHandler;
import com.app.livideo.interfaces.YoutubeGetAccessTokenHandler;
import com.app.livideo.interfaces.codeResultHandler;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.User;
import com.app.livideo.models.YouTubeLoginData;
import com.app.livideo.models.translators.YouTubeLoginDataTranslator;
import com.app.livideo.utils.AndroidUtils;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.Constants;
import com.app.livideo.utils.LVUtils;
import com.app.livideo.utils.Preferences;
import com.app.livideo.utils.TextUtils;

import javax.inject.Inject;

/**
 * Created by Eli on 6/6/2016.
 */
public class YoutubeLogin {

    public static YoutubeLogin instance;
    private static String LIVIDEO_SOCIAL_ID = "3250622944";
    private static ArtistSocialSignUp mParentActivity;
    @Inject Context mContext;
    @Inject RequestQueue mRequestQueue;
    @Inject LVSession mSession;
    private String socialID, socialType, deviceID, displayName;
    private View touch_blocker;

    private YoutubeLogin(ArtistSocialSignUp context) {
        mParentActivity = context;
    }

    public static YoutubeLogin CreateInstance(ArtistSocialSignUp context) {
        if (instance == null) instance = new YoutubeLogin(context);
        return instance.editContext(context);
    }

    private YoutubeLogin editContext(ArtistSocialSignUp context) {
        LVInject.inject(this);
        mParentActivity = context;
        this.touch_blocker = mParentActivity.touch_blocker;
        return this;
    }

    public void requestLoginWithGoogle() {
        final YouTubeLoginData mYouTubeLoginData = YouTubeLoginDataTranslator.getYouTubeLoginData(mContext);
        YouTubeOAuthDialog dialog = new YouTubeOAuthDialog(mYouTubeLoginData, mParentActivity, new YoutubeGetAccessTokenHandler() {
            @Override
            public void RunCompleted(String authCode) {
                touch_blocker.setVisibility(View.VISIBLE);
                APICalls.getYouTubeAccessToken(authCode, mYouTubeLoginData, mParentActivity, mContext, mSession, mRequestQueue, new StringResultHandler() {
                    @Override
                    public void onRunCompleted(final String accessToken) {
                        APICalls.getYouTubeSubCount(accessToken, mParentActivity, mContext, mRequestQueue, new codeResultHandler() {
                            @Override
                            public void onRunCompleted(int subCount) {
                                if (subCount == -1) {
                                    touch_blocker.setVisibility(View.GONE);
                                    AndroidUtils.showShortToast(mParentActivity.getString(R.string.internet_issues), mParentActivity);
                                    return;
                                }

                                if (subCount >= AppConstants.ARTIST_FOLLOWER_COUNT || BuildConfig.DEBUG) {
                                    APICalls.getYouTubeUserDetails(accessToken, mParentActivity, mContext, mRequestQueue, new YouTubeUserDetailsHandler() {
                                        @Override
                                        public void onRunCompleted(String mDisplayName, String mSocialID) {
                                            if (!TextUtils.isNullOrEmpty(mDisplayName) && !TextUtils.isNullOrEmpty(mSocialID)) {
                                                requestArtistSocialLogin(mDisplayName, mSocialID);
                                            } else {
                                                touch_blocker.setVisibility(View.GONE);
                                                AndroidUtils.showLongToast(mParentActivity.getString(R.string.error_no_youtube_channel), mParentActivity);
                                            }
                                        }
                                    });
                                } else {
                                    touch_blocker.setVisibility(View.GONE);
                                    AndroidUtils.showShortToast(mParentActivity.getString(R.string.not_enough_followers), mParentActivity);
                                }
                            }
                        });
                    }
                });
            }
        });
        dialog.show();
    }

    private void requestArtistSocialLogin(String mDisplayName, String mSocialID) {
        displayName = mDisplayName;
        socialID = mSocialID;
        socialType = Constants.TYPE_YOUTUBE;
        deviceID = Settings.Secure.getString(mParentActivity.getContentResolver(), Settings.Secure.ANDROID_ID);
        APICalls.requestBroadcasterSocialLogin(mSession, mParentActivity, mContext, socialID, socialType, deviceID, mRequestQueue, new LoginHandler() {
            @Override
            public void RunCompleted(int UserCode, User user) {
                switch (UserCode) {
                    case Constants.USERCODE_SUCCESS:
                        APICalls.SendAWSIdentity(mContext, mRequestQueue, mSession);
                        saveUserInfo(user, true);
                        LVUtils.StartLiVideo(mSession.getUser(), null, false, touch_blocker,
                                mParentActivity, mContext, mSession, mRequestQueue, null);
                        break;
                    case Constants.USERCODE_NOT_EXISTS:
                        // Create new user object and continue to account creation
                        touch_blocker.setVisibility(View.GONE);
                        saveUserInfo(new User(), false);
                        if (mParentActivity.isActive) {
                            Intent intent = new Intent(mParentActivity, EnterPhoneNumberActivity.class);
                            intent.putExtra(AppConstants.DISPLAY_NAME_KEY_INTENT, displayName);
                            intent.putExtra(AppConstants.SOCIAL_ID_KEY_INTENT, socialID);
                            intent.putExtra(AppConstants.SOCIAL_TYPE_KEY_INTENT, Constants.TYPE_YOUTUBE);
                            intent.putExtra(AppConstants.IS_USER_INTENT, false);
                            mParentActivity.startActivity(intent);
                        }
                        break;
                    case Constants.USERCODE_OTHER_ERROR:
                        enableTouches(true);
                        AndroidUtils.showShortToast(mParentActivity.getString(R.string.retry_later), mParentActivity);
                        break;
                    case Constants.USERCODE_OTHER_DEVICE:
                        enableTouches(true);
                        Bundle bundle = new Bundle();
                        bundle.putString(AppConstants.DEVICE_ID_KEY_INTENT, deviceID);
                        VerifyNewDeviceDialog dialog = new VerifyNewDeviceDialog(mParentActivity, bundle);
                        dialog.show();
                        break;
                    default:
                        enableTouches(true);
                        AndroidUtils.showShortToast(mContext.getString(R.string.error), mParentActivity);
                        break;
                }
            }
        });
    }


    private void enableTouches(final boolean isEnabled) {
        if (isEnabled) touch_blocker.setVisibility(View.GONE);
        else touch_blocker.setVisibility(View.VISIBLE);
    }

    private void saveUserInfo(User user, boolean isOldUser) {
        if (isOldUser) {
            Preferences.setPreference(Constants.USER_TYPE_KEY, Constants.BROADCASTER_USER, mParentActivity);
            Preferences.setPreference(Constants.KEY_DISPLAY_NAME, user.getDisplayName(), mParentActivity);
            Preferences.setPreference(Constants.KEY_SOCIAL_ID, socialID, mParentActivity);
            Preferences.setPreference(Constants.KEY_SOCIAL_TYPE, socialType, mParentActivity);
            Preferences.setPreference(Constants.KEY_DEVICE, deviceID, mParentActivity);
            Preferences.setPreference(Constants.KEY_USER_ID, user.getUserID(), mParentActivity);
        } else {
            user.setDisplayName(displayName);
            user.setSocialID(socialID);
            user.setSocialType(socialType);
        }
        user.setDeviceID(deviceID);
        user.setCurrentRole(User.UserRole.BROADCASTER);
        mSession.setUser(user);
    }
}
