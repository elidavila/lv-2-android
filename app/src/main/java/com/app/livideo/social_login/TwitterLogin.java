package com.app.livideo.social_login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;

import com.android.volley.RequestQueue;
import com.app.livideo.BuildConfig;
import com.app.livideo.R;
import com.app.livideo.activities.ArtistSocialSignUp;
import com.app.livideo.activities.EnterPhoneNumberActivity;
import com.app.livideo.api.APICalls;
import com.app.livideo.dialogs.VerifyNewDeviceDialog;
import com.app.livideo.interfaces.LoginHandler;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.User;
import com.app.livideo.utils.AndroidUtils;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.Constants;
import com.app.livideo.utils.LVUtils;
import com.app.livideo.utils.Preferences;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import javax.inject.Inject;

/**
 * Created by Eli on 12/2/2015
 */
public class TwitterLogin {

    @Inject Context mContext;
    @Inject RequestQueue mRequestQueue;
    @Inject LVSession mSession;
    private static String LIVIDEO_SOCIAL_ID = "3250622944";
    private ArtistSocialSignUp mParentActivity;
    private static TwitterLogin instance;
    private String socialID, socialType, deviceID, dispalyName;
    private View mProgressBar;

    public static TwitterLogin CreateInstance(ArtistSocialSignUp context) {
        if (instance == null) instance = new TwitterLogin(context);
        return instance.editContext(context);
    }

    private TwitterLogin(ArtistSocialSignUp context) {
        this.mParentActivity = context;
    }

    private TwitterLogin editContext(ArtistSocialSignUp context) {
        LVInject.inject(this);
        this.mParentActivity = context;
        return this;
    }

    public void loginWithTwitter(final RequestQueue requestQueue, final View progressBar) {
        Preferences.setPreference(Constants.TYPE_CALLBACK, Constants.TYPE_TWITTER, mParentActivity);
        mProgressBar = progressBar;
        enableTouches(false);
        ArtistSocialSignUp.sTwitterAuthClient = new TwitterAuthClient();
        ArtistSocialSignUp.sTwitterAuthClient.authorize(mParentActivity, new Callback<TwitterSession>() {
            @Override
            public void success(final Result<TwitterSession> result) {
                final TwitterSession session = Twitter.getSessionManager().getActiveSession();
                dispalyName = session.getUserName();
                socialID = Long.toString(session.getUserId());

                // Verify User Credentials
                new TwitterApiClient(session).getAccountService().verifyCredentials(false, true, new Callback<com.twitter.sdk.android.core.models.User>() {
                    @Override
                    public void success(final Result<com.twitter.sdk.android.core.models.User> result) {
                        // Check if User is Twitter Verified or meets our follow count requirement
                        if (result.data.verified
                                || result.data.followersCount >= AppConstants.ARTIST_FOLLOWER_COUNT
                                || socialID.equals(LIVIDEO_SOCIAL_ID)
                                || BuildConfig.DEBUG) {
                            requestArtistSocialLogin();
                        } else {
                            enableTouches(true);
                            AndroidUtils.showShortToast(mParentActivity.getString(R.string.not_enough_followers), mParentActivity);
                        }
                    }

                    private void requestArtistSocialLogin() {
                        socialType = Constants.TYPE_TWITTER;
                        deviceID = Settings.Secure.getString(mParentActivity.getContentResolver(), Settings.Secure.ANDROID_ID);
                        APICalls.requestBroadcasterSocialLogin(mSession, mParentActivity, mContext, socialID, socialType, deviceID, requestQueue, new LoginHandler() {
                            @Override
                            public void RunCompleted(int UserCode, User user) {
                                switch (UserCode) {
                                    case Constants.USERCODE_SUCCESS:
                                        APICalls.SendAWSIdentity(mContext, mRequestQueue, mSession);
                                        saveUserInfo(user, true);
                                        LVUtils.StartLiVideo(mSession.getUser(), null, false, mProgressBar,
                                                mParentActivity, mContext, mSession, mRequestQueue, null);
                                        break;
                                    case Constants.USERCODE_NOT_EXISTS:
                                        // Create new user object and continue to account creation
                                        mProgressBar.setVisibility(View.GONE);
                                        saveUserInfo(new User(), false);
                                        if (mParentActivity.isActive) {
                                            Intent intent = new Intent(mParentActivity, EnterPhoneNumberActivity.class);
                                            intent.putExtra(AppConstants.DISPLAY_NAME_KEY_INTENT, session.getUserName());
                                            intent.putExtra(AppConstants.SOCIAL_ID_KEY_INTENT, Long.toString(session.getUserId()));
                                            intent.putExtra(AppConstants.SOCIAL_TYPE_KEY_INTENT, Constants.TYPE_TWITTER);
                                            intent.putExtra(AppConstants.IS_USER_INTENT, false);
                                            mParentActivity.startActivity(intent);
                                        }
                                        break;
                                    case Constants.USERCODE_OTHER_ERROR:
                                        enableTouches(true);
                                        AndroidUtils.showShortToast(mParentActivity.getString(R.string.retry_later), mParentActivity);
                                        break;
                                    case Constants.USERCODE_OTHER_DEVICE:
                                        enableTouches(true);
                                        Bundle bundle = new Bundle();
                                        bundle.putString(AppConstants.DEVICE_ID_KEY_INTENT, deviceID);
                                        VerifyNewDeviceDialog dialog = new VerifyNewDeviceDialog(mParentActivity, bundle);
                                        dialog.show();
                                        break;
                                    default:
                                        enableTouches(true);
                                        AndroidUtils.showShortToast(mContext.getString(R.string.error), mParentActivity);
                                        break;
                                }
                            }
                        });
                    }

                    @Override
                    public void failure(TwitterException e) {
                        e.printStackTrace();
                        enableTouches(true);
                        AndroidUtils.showShortToast(e.getMessage(), mParentActivity);
                    }
                });
            }

            @Override
            public void failure(TwitterException e) {
                e.printStackTrace();
                enableTouches(true);
                AndroidUtils.showShortToast(e.getMessage(), mParentActivity);
            }
        });
    }

    private void enableTouches(final boolean isEnabled) {
        if (isEnabled) mProgressBar.setVisibility(View.GONE);
        else mProgressBar.setVisibility(View.VISIBLE);
    }

    private void saveUserInfo(User user, boolean isOldUser) {
        if (isOldUser) {
            Preferences.setPreference(Constants.USER_TYPE_KEY, Constants.BROADCASTER_USER, mParentActivity);
            Preferences.setPreference(Constants.KEY_DISPLAY_NAME, user.getDisplayName(), mParentActivity);
            Preferences.setPreference(Constants.KEY_SOCIAL_ID, socialID, mParentActivity);
            Preferences.setPreference(Constants.KEY_SOCIAL_TYPE, socialType, mParentActivity);
            Preferences.setPreference(Constants.KEY_DEVICE, deviceID, mParentActivity);
            Preferences.setPreference(Constants.KEY_USER_ID, user.getUserID(), mParentActivity);
        } else {
            user.setDisplayName(dispalyName);
            user.setSocialID(socialID);
            user.setSocialType(socialType);
        }
        user.setDeviceID(deviceID);
        user.setCurrentRole(User.UserRole.BROADCASTER);
        mSession.setUser(user);
    }
}