package com.app.livideo.social_login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.app.livideo.BuildConfig;
import com.app.livideo.R;
import com.app.livideo.activities.ArtistSocialSignUp;
import com.app.livideo.activities.EnterPhoneNumberActivity;
import com.app.livideo.api.APICalls;
import com.app.livideo.dialogs.VerifyNewDeviceDialog;
import com.app.livideo.interfaces.LoginHandler;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.User;
import com.app.livideo.utils.AndroidUtils;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.Constants;
import com.app.livideo.utils.LVUtils;
import com.app.livideo.utils.Preferences;

import net.londatiga.android.instagram.Instagram;
import net.londatiga.android.instagram.InstagramUser;

import javax.inject.Inject;

/**
 * Created by Eli on 12/3/2015
 */
public class InstagramLogin {
    @Inject Context mContext;
    @Inject RequestQueue mRequestQueue;
    @Inject LVSession mSession;
    private ArtistSocialSignUp mParentActivity;
    private static InstagramLogin instance;
    private String socialID, socialType, deviceID, dispalyName;

    public static InstagramLogin CreateInstance(ArtistSocialSignUp context) {
        if (instance == null)
            instance = new InstagramLogin(context);
        return instance.editContext(context);
    }

    private InstagramLogin(ArtistSocialSignUp context) {
        this.mParentActivity = context;
    }

    private InstagramLogin editContext(ArtistSocialSignUp context) {
        LVInject.inject(this);
        this.mParentActivity = context;
        return this;
    }

    public void loginWithInstagram(final RequestQueue requestQueue, final View mProgressBar) {
        final Instagram instagram = new Instagram(mParentActivity, Constants.IN_CLIENT_ID, Constants.IN_CLIENT_SECRET, Constants.IN_REDIRECT_URI);
        instagram.authorize(new Instagram.InstagramAuthListener() {
            @Override
            public void onSuccess(final InstagramUser instagramUser) {
                dispalyName = instagramUser.username;
                socialID = instagramUser.id;
                socialType = Constants.TYPE_INSTAGRAM;
                deviceID = Settings.Secure.getString(mParentActivity.getContentResolver(), Settings.Secure.ANDROID_ID);
                mProgressBar.setVisibility(View.VISIBLE);
                APICalls.requestBroadcasterSocialLogin(mSession, mParentActivity, mContext, socialID, socialType, deviceID, requestQueue, new LoginHandler() {
                    @Override
                    public void RunCompleted(int UserCode, User user) {
                        mProgressBar.setVisibility(View.GONE);
                        switch (UserCode) {
                            case Constants.USERCODE_SUCCESS:
                                APICalls.SendAWSIdentity(mContext, mRequestQueue, mSession);
                                saveUserInfo(user, true);
                                LVUtils.StartLiVideo(mSession.getUser(), null, false, mProgressBar,
                                        mParentActivity, mContext, mSession, mRequestQueue, null);
                                break;
                            case Constants.USERCODE_NOT_EXISTS:
                                if (instagramUser.FollowerCount >= AppConstants.ARTIST_FOLLOWER_COUNT || instagramUser.id.equals("184553988") || BuildConfig.DEBUG) {
                                    user = new User();
                                    saveUserInfo(user, false);
                                    if (mParentActivity.isActive) {
                                        Intent intent = new Intent(mParentActivity, EnterPhoneNumberActivity.class);
                                        intent.putExtra(AppConstants.DISPLAY_NAME_KEY_INTENT, dispalyName);
                                        intent.putExtra(AppConstants.SOCIAL_ID_KEY_INTENT, socialID);
                                        intent.putExtra(AppConstants.SOCIAL_TYPE_KEY_INTENT, Constants.TYPE_INSTAGRAM);
                                        intent.putExtra(AppConstants.IS_USER_INTENT, false);
                                        mParentActivity.startActivity(intent);
                                    }
                                } else {
                                    mProgressBar.setVisibility(View.GONE);
                                    Toast.makeText(mParentActivity, R.string.not_enough_followers, Toast.LENGTH_LONG).show();
                                }
                                break;
                            case Constants.USERCODE_OTHER_ERROR:
                                mProgressBar.setVisibility(View.GONE);
                                AndroidUtils.showLongToast(mParentActivity.getString(R.string.retry_later), mParentActivity);
                                break;
                            case Constants.USERCODE_OTHER_DEVICE:
                                mProgressBar.setVisibility(View.GONE);
                                Bundle bundle = new Bundle();
                                bundle.putString(AppConstants.DEVICE_ID_KEY_INTENT, deviceID);
                                VerifyNewDeviceDialog dialog = new VerifyNewDeviceDialog(mParentActivity, bundle);
                                dialog.show();
                                break;
                            default:
                                mProgressBar.setVisibility(View.GONE);
                                AndroidUtils.showLongToast(mContext.getString(R.string.error), mParentActivity);
                        }
                    }
                });
            }

            @Override
            public void onError(String error) {
                mProgressBar.setVisibility(View.GONE);
                AndroidUtils.showLongToast(error, mParentActivity);
            }

            @Override
            public void onCancel() {
                mProgressBar.setVisibility(View.GONE);
            }
        });
    }

    private void saveUserInfo(User user, boolean isOldUser) {
        if (isOldUser) {
            Preferences.setPreference(Constants.USER_TYPE_KEY, Constants.BROADCASTER_USER, mParentActivity);
            Preferences.setPreference(Constants.KEY_DISPLAY_NAME, user.getDisplayName(), mParentActivity);
            Preferences.setPreference(Constants.KEY_SOCIAL_ID, socialID, mParentActivity);
            Preferences.setPreference(Constants.KEY_SOCIAL_TYPE, socialType, mParentActivity);
            Preferences.setPreference(Constants.KEY_DEVICE, deviceID, mParentActivity);
            Preferences.setPreference(Constants.KEY_USER_ID, user.getUserID(), mParentActivity);
        } else {
            user.setDisplayName(dispalyName);
            user.setSocialID(socialID);
            user.setSocialType(socialType);
        }
        user.setDeviceID(deviceID);
        user.setCurrentRole(User.UserRole.BROADCASTER);
        mSession.setUser(user);
    }
}
