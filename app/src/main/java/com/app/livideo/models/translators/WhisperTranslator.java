package com.app.livideo.models.translators;

import com.app.livideo.models.User;
import com.app.livideo.models.WhisperRequest;
import com.app.livideo.models.validators.WhisperValidator;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.JSONHelper;
import com.app.livideo.utils.TextUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eli on 5/11/2016.
 */
public class WhisperTranslator {
    public static List<WhisperRequest> getWhispers(JSONObject jsonObject) {
        List<WhisperRequest> whisperRequests = new ArrayList<>();
        try {
            JSONArray jsonArray = JSONHelper.getJSONArray(jsonObject, WhisperRequest.Keys.REQUESTS);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject whisperObject = jsonArray.getJSONObject(i);
                WhisperRequest whisperRequest = getWhisper(whisperObject);
                if (WhisperValidator.isValid(whisperRequest))
                    whisperRequests.add(whisperRequest);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return whisperRequests;
    }

    private static WhisperRequest getWhisper(JSONObject whisperObject) {
        WhisperRequest whisperRequest = new WhisperRequest();
        whisperRequest.setLastMessage(JSONHelper.getString(whisperObject, WhisperRequest.Keys.LAST_MESSAGE));
        if (!TextUtils.isNullOrEmpty(whisperRequest.getLastMessage())) {
            User whisperUser = UserTranslator.getUser(JSONHelper.getJSONObject(whisperObject, AppConstants.USER));
            whisperRequest.setWhisperUser(whisperUser);

            whisperRequest.setChatID(JSONHelper.getString(whisperObject, WhisperRequest.Keys.CHAT_ID));
            whisperRequest.setUnreadCount(JSONHelper.getString(whisperObject, WhisperRequest.Keys.UNREAD_COUNT));
        }
        return whisperRequest;
    }
}
