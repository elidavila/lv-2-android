package com.app.livideo.models.validators;

import com.app.livideo.models.ArtistChatMessage;
import com.app.livideo.utils.TextUtils;

import java.util.List;

/**
 * Created by eli davila on 9/22/2015.
 */
public class ArtistChatMessageValidator {
    public static boolean isValid(ArtistChatMessage artistChatMessage) {
        return artistChatMessage != null && !TextUtils.isNullOrEmpty(artistChatMessage.getMessageUserId());
    }

    public static boolean isValid(List<ArtistChatMessage> chatMessageList) {
        return chatMessageList != null && chatMessageList.size() > 0;
    }

    /**
     * Checks all possible valuse to see if there is an image set
     *
     * @param chatMessage chat message object
     * @return true if image exists
     *
     * When established the server is returning the string value "null"
     *
     */
    public static boolean isImageValid(ArtistChatMessage chatMessage){
        return chatMessage != null && !TextUtils.isNullOrEmpty(chatMessage.getPictureUrl()) && !chatMessage.getPictureUrl().equalsIgnoreCase("null") ;
    }
}
