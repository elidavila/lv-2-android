package com.app.livideo.models.translators;

import android.util.Log;

import com.app.livideo.models.UserImage;
import com.app.livideo.models.validators.UserImageValidator;
import com.app.livideo.utils.JSONHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by eli davila on 9/8/2015.
 */
public class UserImageTranslator {
    public static List<UserImage> getImageObject(JSONArray picturesArray) {
        List<UserImage> meImages = new ArrayList<>();
        try {
            for(int i = 0; i < picturesArray.length(); i++) {
                JSONObject obj = picturesArray.getJSONObject(i);
                UserImage meImage = getImage(obj);
                if (UserImageValidator.isValid(meImage))
                    meImages.add(meImage);
            }
        }
        catch (Exception e) {
            Log.e("Error", "Error, parsing me image"); //NON-NLS
            e.printStackTrace();
        }
        return meImages;
    }

    private static UserImage getImage(JSONObject obj) {
        UserImage meImage = new UserImage();
        meImage.setPictureId(JSONHelper.getString(obj, UserImage.Keys.PICTURE_ID));
        meImage.setUrl(JSONHelper.getString(obj, UserImage.Keys.URL));
        return meImage;
    }
}
