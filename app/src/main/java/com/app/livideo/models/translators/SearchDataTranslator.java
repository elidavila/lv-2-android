package com.app.livideo.models.translators;

import android.util.Log;

import com.app.livideo.models.Artist;
import com.app.livideo.models.ArtistPage;
import com.app.livideo.models.SearchData;
import com.app.livideo.models.Stream;
import com.app.livideo.models.validators.ArtistValidator;
import com.app.livideo.models.validators.StreamValidator;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.JSONHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eli on 4/20/2016.
 */
public class SearchDataTranslator {
    public static SearchData getSearchData(JSONObject jsonObject) {
        List<Artist> artists = new ArrayList<>();
        List<Stream> streams = new ArrayList<>();
        try {
            if (JSONHelper.containsKey(jsonObject, SearchData.Keys.SEARCH_ARTISTS)) {
                JSONArray dataArray = JSONHelper.getJSONArray(jsonObject, SearchData.Keys.SEARCH_ARTISTS);
                for (int i = 0; i < dataArray.length(); i++) {
                    JSONObject obj = dataArray.getJSONObject(i);
                    String objectType = JSONHelper.getString(obj, ArtistPage.Keys.OBJECT_TYPE);
                    if (objectType.equalsIgnoreCase(Stream.Keys.STREAM)) {
                        Stream stream = StreamTranslator.getStream(obj);
                        if (StreamValidator.isValid(stream)) {
                            streams.add(stream);
                        }
                    }

                    if (objectType.equalsIgnoreCase(AppConstants.ARTIST)) {
                        Artist artist = ArtistTranslator.getArtist(obj);
                        if (ArtistValidator.isValid(artist)) {
                            artists.add(artist);
                        }
                    }
                }
            }
        } catch (Exception e) {
            Log.e("ERROR", "PARSING SEARCH DATA");
            e.printStackTrace();
        }

        SearchData searchData = new SearchData();
        searchData.setStreams(streams);
        searchData.setArtists(artists);
        return searchData;
    }
}
