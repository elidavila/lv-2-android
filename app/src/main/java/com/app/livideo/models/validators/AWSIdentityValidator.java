package com.app.livideo.models.validators;

import com.app.livideo.models.AWSIdentity;
import com.app.livideo.utils.TextUtils;

/**
 * Created by eli davila on 9/25/2015.
 */
public class AWSIdentityValidator {
    public static boolean isValid(AWSIdentity awsIdentity) {
        return awsIdentity != null && !TextUtils.isNullOrEmpty(awsIdentity.getToken());
    }
}
