package com.app.livideo.models.validators;

import com.app.livideo.models.WhisperRequest;
import com.app.livideo.utils.TextUtils;

import java.util.List;

/**
 * Created by Eli on 5/11/2016.
 */
public class WhisperValidator {
    public static boolean isValid(List<WhisperRequest> whisperRequests) {
        return whisperRequests != null && whisperRequests.size() > 0;
    }

    public static boolean isValid(WhisperRequest whisperRequest) {
        return whisperRequest != null && UserValidator.isValid(whisperRequest.getWhisperUser());
    }
}
