package com.app.livideo.models.validators;

import com.app.livideo.models.UserImage;
import com.app.livideo.utils.TextUtils;

import java.util.List;

/**
 * Created by edavila on 9/8/2015.
 */
public class UserImageValidator {
    public static boolean isValid(UserImage meImage) {
        return meImage != null && !TextUtils.isNullOrEmpty(meImage.getUrl());
    }

    public static boolean isValid(List<UserImage> meImages) {
        return meImages != null && meImages.size() > 0;
    }
    
}
