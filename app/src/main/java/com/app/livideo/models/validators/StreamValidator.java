package com.app.livideo.models.validators;

import com.app.livideo.models.Stream;
import com.app.livideo.utils.TextUtils;

import java.util.List;

/**
 * Created by eli davila on 9/10/2015.
 */
public class StreamValidator {
    public static boolean isValid(Stream stream) {
        return stream != null && !TextUtils.isNullOrEmpty(stream.getStreamId());
    }

    public static boolean isValid(List<Stream> streams) {
        return streams != null && streams.size() > 0;
    }
}
