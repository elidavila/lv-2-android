package com.app.livideo.models;

import java.io.Serializable;

/**
 * Created by Eli on 9/22/2015.
 */
public class ArtistChatMessage implements Serializable {
    private String messageId, messageUserId, message, displayName, pictureUrl;

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getMessageUserId() {
        return messageUserId;
    }

    public void setMessageUserId(String messageUserId) {
        this.messageUserId = messageUserId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;

        if (getClass() != obj.getClass())
            return false;

        final ArtistChatMessage other = (ArtistChatMessage) obj;
        return (this.messageId == null) ? other.messageId == null : this.messageId.equals(other.messageId);

    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + (this.messageId != null ? this.messageId.hashCode() : 0);
        return hash;
    }

    @SuppressWarnings("HardCodedStringLiteral")
    public static class Keys {
        public static final String MESSAGES = "Messages"; //paramater
        public static final String MESSAGE_ID = "MessageID";
        public static final String MESSAGE_ARTIST_ID = "MArtistID";
        public static final String MESSAGE_USERS_ID = "MUserID";
        public static final String MESSAGE = "Message";
        public static final String USER_ID = "UserID";
        public static final String DISPLAY_NAME = "Displayname";
        public static final String PICTURE_URL = "PictureURL";
        public static final String LIMIT = "Limit";
    }
}
