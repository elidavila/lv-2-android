package com.app.livideo.models;

/**
 * Created by Eli on 5/11/2016.
 */
public class WhisperRequest {
    String chatID;
    private User whisperUser;
    private String unreadCount;
    private String lastMessage;

    public String getUnreadCount() {
        return unreadCount;
    }

    public void setUnreadCount(String unreadCount) {
        this.unreadCount = unreadCount;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public User getWhisperUser() {
        return whisperUser;
    }

    public void setWhisperUser(User whisperUser) {
        this.whisperUser = whisperUser;
    }

    public String getChatID() {
        return chatID;
    }

    public void setChatID(String chatID) {
        this.chatID = chatID;
    }

    @Override
    public boolean equals(Object obj) {
        boolean isEqual;
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WhisperRequest other = (WhisperRequest) obj;

        isEqual = (this.lastMessage == null) ? other.lastMessage == null : this.lastMessage.equals(other.lastMessage);
        if (!isEqual)
            return false;

        isEqual = (this.unreadCount == null) ? other.unreadCount == null : this.unreadCount.equals(other.unreadCount);
        return isEqual;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + (this.lastMessage != null ? this.lastMessage.hashCode() : 0);
        hash = 53 * hash + (this.unreadCount != null ? this.unreadCount.hashCode() : 0);
        return hash;
    }

    @SuppressWarnings("HardCodedStringLiteral")
    public static class Keys {
        public static final String REQUESTS = "Requests";
        public static final String CHAT_ID = "ChatID";
        public static final String UNREAD_COUNT = "UnreadCount";
        public static final String LAST_MESSAGE = "LastMessage";
    }
}
