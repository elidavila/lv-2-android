package com.app.livideo.models.translators;

import android.content.Context;
import android.util.Log;

import com.app.livideo.R;
import com.app.livideo.models.YouTubeLoginData;
import com.app.livideo.utils.JSONHelper;

import org.json.JSONObject;

/**
 * Created by Eli on 6/10/2016.
 */
public class YouTubeLoginDataTranslator {
    public static YouTubeLoginData getYouTubeLoginData(final Context mContext) {
        JSONObject clientSecrets = JSONHelper.getJSONResource(mContext, R.raw.client_secrets);
        YouTubeLoginData youTubeLoginData = new YouTubeLoginData();
        try {
            JSONObject jsonObject = clientSecrets.getJSONObject("installed");
            youTubeLoginData.setAuthURI(JSONHelper.getString(jsonObject, YouTubeLoginData.Keys.AUTH_URI));
            youTubeLoginData.setClientID(JSONHelper.getString(jsonObject, YouTubeLoginData.Keys.CLIENT_ID));
            youTubeLoginData.setRedirectURI(JSONHelper.getString(jsonObject, YouTubeLoginData.Keys.REDIRECT_URI));
            youTubeLoginData.setTokenURI(JSONHelper.getString(jsonObject, YouTubeLoginData.Keys.TOKEN_URI));
        } catch (Throwable e) {
            e.printStackTrace();
            Log.e("Parsing Error", "In YouTubeLoginData");
        }
        return youTubeLoginData;
    }
}
