package com.app.livideo.models.translators;

import android.util.Log;

import com.app.livideo.models.Artist;
import com.app.livideo.models.Discover;
import com.app.livideo.models.Stream;
import com.app.livideo.models.validators.ArtistValidator;
import com.app.livideo.models.validators.StreamValidator;
import com.app.livideo.utils.JSONHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eli on 4/4/2016.
 */
public class DiscoverTranslator {
    public static Discover getDiscover(String jsonString) {
        Discover discover = new Discover();
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            if (JSONHelper.containsKey(jsonObject, Discover.Keys.DISCOVER)) {
                JSONObject discoverObject = JSONHelper.getJSONObject(jsonObject, Discover.Keys.DISCOVER);

                // FEATURED
                discover.setFeatures(JSONHelper.containsKey(discoverObject, Discover.Keys.FEATURED)
                        ? FeatureTranslator.getFeaturesList(discoverObject) : null);

                // POPULAR
                discover.setPopularArtists(JSONHelper.containsKey(discoverObject, Discover.Keys.POPULAR)
                        ? getArtistList(discoverObject.getJSONArray(Discover.Keys.POPULAR)) : null);

                // LATEST
                discover.setLatestStreams(JSONHelper.containsKey(discoverObject, Discover.Keys.LATEST)
                        ? getStreamList(discoverObject.getJSONArray(Discover.Keys.LATEST)) : null);

                // OPEN MIC
                discover.setOpenMics(JSONHelper.containsKey(discoverObject, Discover.Keys.OPEN_MIC)
                        ? getStreamList(discoverObject.getJSONArray(Discover.Keys.OPEN_MIC)) : null);

                // CHANNEL ARTISTS
                discover.setArtists(JSONHelper.containsKey(discoverObject, Discover.Keys.ARTISTS)
                        ? getArtistList(discoverObject.getJSONArray(Discover.Keys.ARTISTS)) : null);

            }
        } catch (Exception e) {
            Log.e("ERROR", "PARSING DISCOVER ERROR");
            e.printStackTrace();
        }
        return discover;
    }

    private static List<Artist> getArtistList(JSONArray artistsArray) {
        List<Artist> artists = new ArrayList<>();
        try {
            for (int i = 0; i < artistsArray.length(); i++) {
                JSONObject obj = artistsArray.getJSONObject(i);
                Artist artist = ArtistTranslator.getArtist(obj);
                if (ArtistValidator.isValid(artist))
                    artists.add(artist);
            }
        } catch (Exception e) {
            Log.e("ERROR", "PARSING ARTIST ERROR"); //NON-NLS
            e.printStackTrace();
        }
        return artists;
    }

    private static List<Stream> getStreamList(JSONArray streamsArray) {
        List<Stream> streams = new ArrayList<>();
        try {
            for (int i = 0; i < streamsArray.length(); i++) {
                JSONObject obj = streamsArray.getJSONObject(i);
                Stream stream = StreamTranslator.getStream(obj);
                if (StreamValidator.isValid(stream))
                    streams.add(stream);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return streams;
    }
}
