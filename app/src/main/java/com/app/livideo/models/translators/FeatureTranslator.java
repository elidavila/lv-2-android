package com.app.livideo.models.translators;

import com.app.livideo.models.Discover;
import com.app.livideo.models.Feature;
import com.app.livideo.models.validators.FeatureValidator;
import com.app.livideo.utils.JSONHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eli on 4/4/2016.
 */
public class FeatureTranslator {

    public static List<Feature> getFeaturesList(JSONObject discoverObject) {
        List<Feature> features = new ArrayList<>();
        try {
            JSONArray streamsArray = JSONHelper.getJSONArray(discoverObject, Discover.Keys.FEATURED);
            for (int i = 0; i < streamsArray.length(); i++) {
                JSONObject obj = streamsArray.getJSONObject(i);
                Feature feature = getFeature(obj);
                if (FeatureValidator.isValid(feature))
                    features.add(feature);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return features;
    }

    public static Feature getFeature(JSONObject obj) {
        Feature feature = new Feature();
        feature.setTagline(JSONHelper.getString(obj, Feature.Keys.TAGLINE));
        feature.setHeadline(JSONHelper.getString(obj, Feature.Keys.HEADLINE));
        feature.setActionText(JSONHelper.getString(obj, Feature.Keys.ACTION_TEXT));
        feature.setActionType(JSONHelper.getString(obj, Feature.Keys.ACTION_TYPE));
        feature.setActionParam(JSONHelper.getString(obj, Feature.Keys.ACTION_PARAM));
        feature.setPosition(JSONHelper.getString(obj, Feature.Keys.POSITION));
        feature.setPictureUrl(JSONHelper.getString(obj, Feature.Keys.PICTURE_URL));
        return feature;
    }
}
