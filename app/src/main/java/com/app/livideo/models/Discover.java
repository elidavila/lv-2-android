package com.app.livideo.models;

import java.util.List;

/**
 * Created by Eli on 4/4/2016.
 */
public class Discover {

    List<Stream> latestStreams;
    List<Artist> popularArtists;
    List<Artist> artists;
    List<Feature> features;
    List<Stream> openMics;

    public List<Feature> getFeatures() {
        return features;
    }

    public void setFeatures(List<Feature> features) {
        this.features = features;
    }

    public List<Stream> getLatestStreams() {
        return latestStreams;
    }

    public void setLatestStreams(List<Stream> latestStreams) {
        this.latestStreams = latestStreams;
    }

    public List<Artist> getPopularArtists() {
        return popularArtists;
    }

    public void setPopularArtists(List<Artist> popularArtists) {
        this.popularArtists = popularArtists;
    }

    public List<Artist> getArtists() {
        return artists;
    }

    public void setArtists(List<Artist> artists) {
        this.artists = artists;
    }

    public void setOpenMics(List<Stream> openMics) {
        this.openMics = openMics;
    }

    public List<Stream> getOpenMics() {
        return openMics;
    }

    @SuppressWarnings("HardCodedStringLiteral")
    public static class Keys {
        public static final String DISCOVER = "Discover";
        public static final String LATEST = "Latest";
        public static final String POPULAR = "Popular";
        public static final String FEATURED = "Featured";
        public static final String ARTISTS = "Artists";
        public static final String OPEN_MIC = "OpenMic";
    }
}
