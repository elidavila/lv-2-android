package com.app.livideo.models.translators;

import com.app.livideo.models.UserInfo;
import com.app.livideo.utils.JSONHelper;

import org.json.JSONObject;

/**
 * Created by Eli on 4/12/2016.
 */
public class UserInfoTranslator {

    public static UserInfo getUserInfo(JSONObject jsonObject) {
        UserInfo userInfo = new UserInfo();
        userInfo.setHasMembership(JSONHelper.getInt(jsonObject, UserInfo.Keys.HAS_MEMBER_sHIP));
        return userInfo;
    }
}
