package com.app.livideo.models.translators;

import com.app.livideo.models.Stream;
import com.app.livideo.models.validators.StreamValidator;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.JSONHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by eli davila on 9/9/2015.
 */
public class StreamTranslator {

    public static List<Stream> getStreams(String jsonString) {
        List<Stream> streams = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            if (JSONHelper.containsKey(jsonObject, Stream.Keys.STREAMS)) {
                JSONArray streamsArray = JSONHelper.getJSONArray(jsonObject, Stream.Keys.STREAMS);
                for (int i = 0; i < streamsArray.length(); i++) {
                    JSONObject obj = streamsArray.getJSONObject(i);
                    Stream mStream = getStream(obj);
                    if (StreamValidator.isValid(mStream))
                        streams.add(mStream);
                }
            }
        } catch (Exception e) { /*Log.e("ERROR", "PARSING Active Stream error.");*/ } //NON-NLS
        return streams;
    }

    public static Stream getStream(JSONObject obj) {
        Stream stream = new Stream();
        stream.setStreamId(JSONHelper.getString(obj, Stream.Keys.STREAM_ID));
        stream.setHeadline(JSONHelper.getString(obj, Stream.Keys.HEADLINE));
        stream.setTime(JSONHelper.getString(obj, Stream.Keys.TIME));
        stream.setArtistId(JSONHelper.getString(obj, Stream.Keys.ARTIST_ID));
        stream.setDuration(JSONHelper.getString(obj, Stream.Keys.DURATION));
        stream.setMessage(JSONHelper.getString(obj, Stream.Keys.MESSAGE));
        stream.setUrl(JSONHelper.getString(obj, Stream.Keys.URL));
        stream.setShortURI(JSONHelper.getString(obj, Stream.Keys.SHORT_URI));
        stream.setDisplayName(JSONHelper.getString(obj, Stream.Keys.DISPLAY_NAME));
        stream.setArtist(ArtistTranslator.getArtist(JSONHelper.getJSONObject(obj, AppConstants.ARTIST)));
        stream.setOpenMicUser(ArtistTranslator.getArtist(JSONHelper.getJSONObject(obj, Stream.Keys.OPEN_MIC_USER)));
        try {
            stream.setIsPublic((Integer.valueOf(JSONHelper.getString(obj, Stream.Keys.IS_PUBLIC)) == 1));
        } catch (Exception ignore) {
            stream.setIsPublic(false);
        }
        return stream;
    }
}
