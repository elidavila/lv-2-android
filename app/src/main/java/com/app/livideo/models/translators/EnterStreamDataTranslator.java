package com.app.livideo.models.translators;

import android.util.Log;

import com.app.livideo.api.APICalls;
import com.app.livideo.models.Stream;
import com.app.livideo.models.EnterStreamData;
import com.app.livideo.utils.JSONHelper;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Eli on 4/8/2016.
 */
public class EnterStreamDataTranslator {

    public static EnterStreamData getEnterStreamData(String jsonString) {
        EnterStreamData enterStreamData = new EnterStreamData();
        try {
            JSONObject json = new JSONObject(jsonString);
            if (JSONHelper.containsKey(json, APICalls.ERROR_CODE_KEY)) {
                enterStreamData.setCode(Integer.valueOf(JSONHelper.getString(json, APICalls.ERROR_CODE_KEY)));
            } else if (JSONHelper.containsKey(json, APICalls.CODE_KEY)) {
                enterStreamData.setCode(Integer.valueOf(JSONHelper.getString(json, APICalls.CODE_KEY)));
                enterStreamData.setUrl(JSONHelper.getString(json, Stream.Keys.URL));
                enterStreamData.setWatchingId(JSONHelper.getInt(json, Stream.Keys.WATCHING_ID));
                enterStreamData.setDidStar(JSONHelper.getBoolean(json, Stream.Keys.DID_STAR));
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("PARSING ERROR", "IN ENTER STREAM");
        }
        return enterStreamData;
    }

}
