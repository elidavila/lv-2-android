package com.app.livideo.models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Eli on 9/4/2015.
 */
public class Artist extends User implements Serializable, Comparable<Artist> {
    private String birthdate;
    private String userId;
    private String displayName;
    private String socialType;
    private String timeZone;
    private String artistType;
    private String isAllAges;

    private List<ArtistPicture> artistPictures;
    private List<ArtistChatMessage> chatMessageList, previewChatMessageList;
    private int isWatching, isHero;

    public int getIsHero() {
        return isHero;
    }

    public void setIsHero(int isHero) {
        this.isHero = isHero;
    }

    public int getIsWatching() {
        return isWatching;
    }

    public void setIsWatching(int isWatching) {
        this.isWatching = isWatching;
    }

    @Override
    public String getSocialType() {
        return socialType;
    }

    @Override
    public void setSocialType(String socialType) {
        this.socialType = socialType;
    }

    @Override
    public String getIsAllAges() {
        return isAllAges;
    }

    @Override
    public void setIsAllAges(String isAllAges) {
        this.isAllAges = isAllAges;
    }

    public List<ArtistChatMessage> getPreviewChatMessageList() {
        return previewChatMessageList;
    }

    public void setPreviewChatMessageList(List<ArtistChatMessage> previewChatMessageList) {
        this.previewChatMessageList = previewChatMessageList;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getArtistId() {
        return userId;
    }

    public void setArtistId(String userId) {
        this.userId = userId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getArtistType() {
        return artistType;
    }

    public void setArtistType(String artistType) {
        this.artistType = artistType;
    }

    public List<ArtistPicture> getArtistPictures() {
        return artistPictures;
    }

    public void setArtistPictures(List<ArtistPicture> artistPictures) {
        this.artistPictures = artistPictures;
    }

    public List<ArtistChatMessage> getChatMessageList() {
        return chatMessageList;
    }

    public void setChatMessageList(List<ArtistChatMessage> chatMessageList) {
        this.chatMessageList = chatMessageList;
    }

    @Override
    public int compareTo(Artist artist) {
        return displayName.compareTo(artist.getDisplayName());
    }

    @SuppressWarnings("HardCodedStringLiteral")
    public static class Keys {
        public static final String ARTISTS = "Artists";
        public static final String BIRTHDATE = "Birthdate";
        public static final String USER_ID = "UserID";
        public static final String DISPLAY_NAME = "Displayname";
        public static final String TIME_ZONE = "TimeZone";
        public static final String PICTURES = "Pictures";
        public static final String ARTIST_TYPE = "ArtistType";
        public static final String IS_ALL_AGES = "isAllAges";
        public static final String SOCIAL_TYPE = "SocialType";
        public static final String PAYMENT_METHOD = "PaymentMethod";
        public static final String IS_WATCHING = "isWatching";
        public static final String IS_HERO = "isHero";
        public static final String KEY_SHORT_URI = "shortURI";
    }
}
