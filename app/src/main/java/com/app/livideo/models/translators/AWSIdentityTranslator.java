package com.app.livideo.models.translators;

import android.util.Log;

import com.app.livideo.models.AWSIdentity;
import com.app.livideo.models.User;
import com.app.livideo.utils.Constants;
import com.app.livideo.utils.JSONHelper;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by eli davila on 9/25/2015.
 */
public class AWSIdentityTranslator {

    public static AWSIdentity getAWSIdentity(String jsonString) {
        AWSIdentity awsIdentity = new AWSIdentity();
        try {
            JSONObject obj = new JSONObject(jsonString);
            awsIdentity.setId(JSONHelper.getString(obj, AWSIdentity.Keys.ID));
            awsIdentity.setToken(JSONHelper.getString(obj, AWSIdentity.Keys.TOKEN));
        } catch (Exception e) {
            Log.d("ERROR", "PARSING AWS LOGIN ERROR"); //NON-NLS
            e.printStackTrace();
        }
        return awsIdentity;
    }

    public static Map<String, String> getAWSLoginParams(String userID, String deviceID) {
        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_AWS_LOGIN);
        params.put(User.Keys.PARAMS_USER_ID, userID);
        params.put(User.Keys.PARAMS_DEVICE_ID, deviceID);
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);
        return params;
    }
}
