package com.app.livideo.models.translators;

import android.util.Log;

import com.app.livideo.models.Artist;
import com.app.livideo.models.ArtistPicture;
import com.app.livideo.models.User;
import com.app.livideo.models.validators.ArtistPictureValidator;
import com.app.livideo.models.validators.ArtistValidator;
import com.app.livideo.utils.JSONHelper;
import com.app.livideo.utils.TextUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by eli davila on 9/3/2015.
 */
public class ArtistTranslator {
    public static List<Artist> getArtistList(String jsonString) {
        List<Artist> artists = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            if (JSONHelper.containsKey(jsonObject, Artist.Keys.ARTISTS)) {
                JSONArray artistsArray = JSONHelper.getJSONArray(jsonObject, Artist.Keys.ARTISTS);
                for (int i = 0; i < artistsArray.length(); i++) {
                    JSONObject obj = artistsArray.getJSONObject(i);
                    Artist artist = getArtist(obj);
                    if (ArtistValidator.isValid(artist)) {
                        artists.add(artist);
                    }
                }
            }
        } catch (Exception e) {
            Log.e("ERROR", "PARSING ARTIST ERROR"); //NON-NLS
            e.printStackTrace();
        }
        return artists;
    }

    public static Artist getArtist(JSONObject obj) {
        Artist artist = new Artist();
        artist.setBirthdate(JSONHelper.getString(obj, Artist.Keys.BIRTHDATE));
        artist.setArtistId(JSONHelper.getString(obj, Artist.Keys.USER_ID));
        artist.setDisplayName(JSONHelper.getString(obj, Artist.Keys.DISPLAY_NAME));
        artist.setSocialType(JSONHelper.getString(obj, Artist.Keys.SOCIAL_TYPE));
        artist.setTimeZone(JSONHelper.getString(obj, Artist.Keys.TIME_ZONE));
        artist.setArtistType(JSONHelper.getString(obj, Artist.Keys.ARTIST_TYPE));
        artist.setIsAllAges(JSONHelper.getString(obj, Artist.Keys.IS_ALL_AGES));
        artist.setEmail(JSONHelper.getString(obj, User.Keys.EMAIL));
        artist.setIsWatching(JSONHelper.getInt(obj, Artist.Keys.IS_WATCHING));
        artist.setIsHero((JSONHelper.getBoolean(obj, Artist.Keys.IS_HERO)) ? 1 : 0);

        // PICTURES
        JSONArray picturesArray = JSONHelper.getJSONArray(obj, Artist.Keys.PICTURES);
        if (picturesArray != null) {
            List<ArtistPicture> artistPictures = ArtistPictureTranslator.getPictures(picturesArray);
            if (ArtistPictureValidator.isValid(artistPictures)) {
                artist.setArtistPictures(artistPictures);
            }
        }
        return artist;
    }
}
