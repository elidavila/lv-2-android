package com.app.livideo.models.translators;

import android.util.Log;

import com.app.livideo.models.PrivateChatMessage;
import com.app.livideo.models.validators.PrivateChatMessageValidator;
import com.app.livideo.utils.JSONHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eli on 5/11/2016.
 */
public class PrivateChatMessageTranslator {
    public static List<PrivateChatMessage> getPrivateChatMessages(String jsonString) {
        List<PrivateChatMessage> chatMessageList = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            if(JSONHelper.containsKey(jsonObject, PrivateChatMessage.Keys.MESSAGES)) {
                JSONArray messagesArray = JSONHelper.getJSONArray(jsonObject, PrivateChatMessage.Keys.MESSAGES);
                for (int i = 0; i < messagesArray.length(); i++) {
                    JSONObject obj = messagesArray.getJSONObject(i);
                    PrivateChatMessage privateChatMessage = getPrivateChatMessage(obj);
                    if (PrivateChatMessageValidator.isValid(privateChatMessage))
                        chatMessageList.add(privateChatMessage);
                }
            }
        } catch (Exception e) {
            Log.e("ERROR", "PARSING PRIVATE MESSAGE ERROR");  //NON-NLS
            e.printStackTrace();
        }
        return chatMessageList;
    }

    private static PrivateChatMessage getPrivateChatMessage(JSONObject obj) {
        PrivateChatMessage privateChatMessage = new PrivateChatMessage();
        privateChatMessage.setMessage(JSONHelper.getString(obj, PrivateChatMessage.Keys.MESSAGE));
        privateChatMessage.setDisplayName(JSONHelper.getString(obj, PrivateChatMessage.Keys.DISPLAY_NAME));
        privateChatMessage.setPictureUrl(JSONHelper.getString(obj, PrivateChatMessage.Keys.PICTURE_URL));
        privateChatMessage.setChatID(JSONHelper.getString(obj, PrivateChatMessage.Keys.CHAT_ID));
        privateChatMessage.setIdMessage(JSONHelper.getString(obj, PrivateChatMessage.Keys.ID_MESSAGE));
        privateChatMessage.setMessageType(JSONHelper.getString(obj, PrivateChatMessage.Keys.MESSAGE_TYPE));
        privateChatMessage.setSysct(JSONHelper.getString(obj, PrivateChatMessage.Keys.SYSCT));
        privateChatMessage.setUserID(JSONHelper.getString(obj, PrivateChatMessage.Keys.USER_ID));
        privateChatMessage.setUserIDTarget(JSONHelper.getString(obj, PrivateChatMessage.Keys.USER_ID_TARGET));
        return privateChatMessage;
    }
}
