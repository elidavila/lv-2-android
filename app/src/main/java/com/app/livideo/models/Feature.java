package com.app.livideo.models;

import java.io.Serializable;

/**
 * Created by Eli on 4/4/2016.
 */
public class Feature implements Serializable {
    String actionParam;
    String actionText;
    String actionType;
    String headline;
    String pictureUrl;
    String position;
    String tagline;

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String getActionParam() {
        return actionParam;
    }

    public void setActionParam(String actionParam) {
        this.actionParam = actionParam;
    }

    public String getActionText() {
        return actionText;
    }

    public void setActionText(String actionText) {
        this.actionText = actionText;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getTagline() {
        return tagline;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    @SuppressWarnings("HardCodedStringLiteral")
    public static class Keys {
        public static final String TAGLINE = "Tagline";
        public static final String HEADLINE = "Headline";
        public static final String ACTION_TEXT = "ActionText";
        public static final String ACTION_TYPE = "ActionType";
        public static final String ACTION_PARAM = "ActionParam";
        public static final String POSITION = "Position";
        public static final String PICTURE_URL = "PictureURL";
    }
}
