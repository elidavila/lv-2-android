package com.app.livideo.models;

/**
 * Created by eli on 12/1/2015.
 */
public class VineLoginData {
    String code;
    String success;
    String error;
    VineData vineData;

    public VineData getVineData() {
        return vineData;
    }

    public void setVineData(VineData vineData) {
        this.vineData = vineData;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
