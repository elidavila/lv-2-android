package com.app.livideo.models.translators;

import android.util.Log;

import com.app.livideo.models.ArtistPicture;
import com.app.livideo.models.validators.ArtistPictureValidator;
import com.app.livideo.utils.JSONHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by eli davila on 9/4/2015.
 */
public class ArtistPictureTranslator {
    public static List<ArtistPicture> getPictures(JSONArray picturesArray) {
        List<ArtistPicture> artistPictures = new ArrayList<>();
        try {
            for (int i = 0; i < picturesArray.length(); i++) {
                JSONObject obj = picturesArray.getJSONObject(i);
                ArtistPicture artistPicture = getArtistPicture(obj);
                if (ArtistPictureValidator.isValid(artistPicture))
                    artistPictures.add(artistPicture);
            }
        } catch (Exception e) {
            Log.e("Parsing Error", "Error parsing artist pictures"); //NON-NLS
            e.printStackTrace();
        }
        return artistPictures;
    }

    public static ArtistPicture getArtistPicture(JSONObject obj) {
        ArtistPicture artistPicture = new ArtistPicture();
        artistPicture.setPictureId(JSONHelper.getString(obj, ArtistPicture.Keys.PICTURE_ID));
        artistPicture.setUserId(JSONHelper.getString(obj, ArtistPicture.Keys.USER_ID));
        artistPicture.setUrl(JSONHelper.getString(obj, ArtistPicture.Keys.URL));
        return artistPicture;
    }
}