package com.app.livideo.models;

/**
 * Created by eli davila on 9/25/2015.
 */
public class AWSIdentity {
    private String id, token;

    public String getToken() { return token; }
    public void setToken(String token) { this.token = token; }

    public String getId() { return id; }
    public void setId(String id) { this.id = id; }

    @SuppressWarnings("HardCodedStringLiteral")
    public static class Keys {
        public static final String ID = "id";
        public static final String TOKEN = "token";
    }
}
