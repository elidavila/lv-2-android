package com.app.livideo.models.validators;

import java.util.List;

/**
 * Created by eli davila on 9/14/2015.
 */
public class StringListValidator {
    public static boolean isValid(List<String> stringList) {
        return stringList != null && stringList.size() > 0;
    }
}
