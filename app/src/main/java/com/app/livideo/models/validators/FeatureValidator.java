package com.app.livideo.models.validators;

import com.app.livideo.models.Feature;
import com.app.livideo.utils.TextUtils;

import java.util.List;

/**
 * Created by Eli on 4/4/2016.
 */
public class FeatureValidator {
    public static boolean isValid(Feature feature) {
        return feature != null && !TextUtils.isNullOrEmpty(feature.getPictureUrl());
    }

    public static boolean isValid(List<Feature> features) {
        return features != null && features.size() > 0;
    }
}
