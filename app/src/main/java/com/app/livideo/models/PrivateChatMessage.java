package com.app.livideo.models;

/**
 * Created by Eli on 5/11/2016.
 */
public class PrivateChatMessage {

    String idMessage;
    String userID;
    String userIDTarget;
    String messageType;
    String message;
    String chatID;
    String sysct;
    String displayName;
    String pictureUrl;

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getIdMessage() {
        return idMessage;
    }

    public void setIdMessage(String idMessage) {
        this.idMessage = idMessage;
    }

    public String getChatID() {
        return chatID;
    }

    public void setChatID(String chatID) {
        this.chatID = chatID;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }


    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUserIDTarget() {
        return userIDTarget;
    }

    public void setUserIDTarget(String userIDTarget) {
        this.userIDTarget = userIDTarget;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public String getSysct() {
        return sysct;
    }

    public void setSysct(String sysct) {
        this.sysct = sysct;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    @SuppressWarnings("HardCodedStringLiteral")
    public static class Keys {
        public static final String MESSAGES = "Messages";
        public static final String MESSAGE = "Message";
        public static final String SYSCT = "SYSCT";
        public static final String USER_ID = "UserID";
        public static final String DISPLAY_NAME = "Displayname";
        public static final String PICTURE_URL = "PictureURL";

        public static final String ID_MESSAGE = "ID_Message";
        public static final String USER_ID_TARGET = "UserID_Target";
        public static final String MESSAGE_TYPE = "MessageType";
        public static final String CHAT_ID = "ChatID";
    }
}
