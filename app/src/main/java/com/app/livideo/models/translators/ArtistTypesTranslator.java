package com.app.livideo.models.translators;

import android.util.Log;

import com.app.livideo.utils.JSONHelper;
import com.app.livideo.utils.TextUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by eli davila on 9/8/2015.
 */
public class ArtistTypesTranslator {
    private static String CATEGORIE_KEY = "Name";

    public static List<String> getArtistTypesList(String jsonString) {
        List<String> artistTypesList = new ArrayList<>();
        try {
            JSONArray artistTypesArray = new JSONArray(jsonString);
            for (int i = 0; i < artistTypesArray.length(); i++) {
                JSONObject obj = artistTypesArray.getJSONObject(i);
                String channelCategorie = JSONHelper.getString(obj, CATEGORIE_KEY);
                if (!TextUtils.isNullOrEmpty(channelCategorie))
                    artistTypesList.add(channelCategorie);
            }
        } catch (Exception e) {
//            Log.d("ERROR", "Parsing Artist Types Error."); //NON-NLS
            e.printStackTrace();
        }
        return artistTypesList;
    }

}
