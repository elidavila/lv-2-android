package com.app.livideo.models.validators;

import com.app.livideo.models.User;
import com.app.livideo.utils.TextUtils;

import java.util.List;

/**
 * Created by eli davila on 8/25/2015.
 */
public class UserValidator {
    public static boolean isValid(User user) {
        return user != null && !TextUtils.isNullOrEmpty(user.getUserID());
    }

    public static boolean isValid(List<User> users) {
        return users != null && users.size() > 0;
    }
}
