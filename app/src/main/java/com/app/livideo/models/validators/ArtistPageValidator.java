package com.app.livideo.models.validators;

import com.app.livideo.models.ArtistPage;
import com.app.livideo.utils.TextUtils;

/**
 * Created by Eli on 4/12/2016.
 */
public class ArtistPageValidator {
    public static boolean isValid(ArtistPage artistPage) {
        return artistPage != null && (ArtistValidator.isValid(artistPage.getArtist()) || TimeLineValidator.isValid(artistPage.getTimeLines()));
    }
}
