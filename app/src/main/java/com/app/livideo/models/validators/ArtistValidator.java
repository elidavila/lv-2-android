package com.app.livideo.models.validators;

import com.app.livideo.models.Artist;
import com.app.livideo.utils.TextUtils;

import java.util.List;

/**
 * Created by eli davila on 9/3/2015.
 */
public class ArtistValidator {
    public static boolean isValid(Artist artist) {
        return artist != null && !TextUtils.isNullOrEmpty(artist.getArtistId());
    }

    public static boolean isValid(List<Artist> artistList) {
        return artistList != null && artistList.size() > 0;
    }
}
