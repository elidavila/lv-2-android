package com.app.livideo.models.translators;

import android.util.Log;

import com.app.livideo.models.Artist;
import com.app.livideo.models.User;
import com.app.livideo.models.UserImage;
import com.app.livideo.models.UserInfo;
import com.app.livideo.models.validators.UserImageValidator;
import com.app.livideo.utils.JSONHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eli on 8/24/2015.
 */
public class UserTranslator {
    public static User getUser(JSONObject obj) {
        User user = new User();
        try {
            if (!JSONHelper.containsKey(obj, User.Keys.ERROR)) {
                user.setBirthdate(JSONHelper.getString(obj, User.Keys.BIRTHDATE));
                user.setUserID(JSONHelper.getString(obj, User.Keys.USER_ID));
                user.setDisplayName(JSONHelper.getString(obj, User.Keys.DISPLAY_NAME));
                user.setSocialID(JSONHelper.getString(obj, User.Keys.SOCIAL_ID));
                user.setSocialType(JSONHelper.getString(obj, User.Keys.SOCIAL_TYPE));
                user.setTimeZone(JSONHelper.getString(obj, User.Keys.TIME_ZONE));
                user.setShortURL(JSONHelper.getString(obj, Artist.Keys.KEY_SHORT_URI));
                user.setEmail(JSONHelper.getString(obj, User.Keys.EMAIL));
                try {
                    user.setPhotoPosition(Integer.valueOf(JSONHelper.getString(obj, User.Keys.RANDOMIZE_PHOTO)));
                } catch (Exception e) {
                    // Do nothing
                }

                //Get Pictures
                JSONArray picturesArray = JSONHelper.getJSONArray(obj, User.Keys.PICTURES);
                List<UserImage> meImageList = UserImageTranslator.getImageObject(picturesArray);
                if (UserImageValidator.isValid(meImageList))
                    user.setPictures(meImageList);

                //Get Artist User Attributes
                user.setArtistType(JSONHelper.getString(obj, Artist.Keys.ARTIST_TYPE));
                user.setIsAllAges(JSONHelper.getString(obj, Artist.Keys.IS_ALL_AGES));
                user.setCurrentRole((JSONHelper.containsKey(obj, User.Keys.IS_ARTIST)) ? User.UserRole.BROADCASTER : User.UserRole.VIEWER);
                user.setHasMembership(JSONHelper.getInt(obj, UserInfo.Keys.HAS_MEMBERSHIP));
            }
        } catch (Exception e) {
            Log.e("JSON ERROR", "Error parsing user object.");
        }
        return user;
    }

    public static List<User> getUsers(JSONObject jsonObject) {
        List<User> users = new ArrayList<>();
        try {
            JSONArray userArray = JSONHelper.getJSONArray(jsonObject, User.Keys.USERS);
            for (int i = 0; i < userArray.length(); i++) {
                users.add(getUser(userArray.getJSONObject(i)));
            }
            return users;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return users;
    }
}
