package com.app.livideo.models.validators;

import com.app.livideo.models.PrivateChatMessage;
import com.app.livideo.utils.TextUtils;

import java.util.List;

/**
 * Created by Eli on 9/22/2015.
 */
public class PrivateChatMessageValidator {
    public static boolean isValid(PrivateChatMessage chatMessage) {
        return chatMessage != null && !TextUtils.isNullOrEmpty(chatMessage.getUserID());
    }

    public static boolean isValid(List<PrivateChatMessage> chatMessageList) {
        return chatMessageList != null && chatMessageList.size() > 0;
    }

    public static boolean isImageValid(PrivateChatMessage chatMessage) {
        return chatMessage != null && !TextUtils.isNullOrEmpty(chatMessage.getPictureUrl()) && !chatMessage.getPictureUrl().equalsIgnoreCase("null") ;
    }
}
