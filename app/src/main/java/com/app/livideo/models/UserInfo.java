package com.app.livideo.models;

/**
 * Created by Eli on 4/12/2016.
 */
public class UserInfo {
    int hasMembership;

    public int getHasMembership() {
        return hasMembership;
    }

    public void setHasMembership(int hasMembership) {
        this.hasMembership = hasMembership;
    }

    @SuppressWarnings("HardCodedStringLiteral")
    public static class Keys {
        public static final String HAS_MEMBERSHIP = "hasMemberShip";
        public static final String HAS_MEMBER_sHIP = "hasMembership";
    }
}
