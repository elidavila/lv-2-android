package com.app.livideo.models.validators;

import java.util.List;

/**
 * Created by Eli on 4/12/2016.
 */
public class TimeLineValidator {
    public static boolean isValid(List<Object> timeLines) {
        return timeLines != null && timeLines.size() > 0;
    }

    public static boolean isValid(Object timeLineObject) {
        return timeLineObject != null;
    }
}
