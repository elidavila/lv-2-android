package com.app.livideo.models;

import java.io.Serializable;

/**
 * Created by eli on 9/1/2015.
 */
public class UserImage implements Serializable {
    public String pictureId, url;

    public String getPictureId() { return pictureId; }
    public void setPictureId(String pictureId) { this.pictureId = pictureId; }

    public String getUrl() { return url; }
    public void setUrl(String url) { this.url = url; }

    @SuppressWarnings("HardCodedStringLiteral")
    public static class Keys {
        public static final String PICTURE_ID = "PictureID";
        public static final String URL = "URL";
    }
}
