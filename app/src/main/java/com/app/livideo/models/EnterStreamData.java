package com.app.livideo.models;

/**
 * Created by Eli on 4/8/2016.
 */
public class EnterStreamData {
    int code;
    String url;
    int watchingId;
    boolean didStar;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getWatchingId() {
        return watchingId;
    }

    public void setWatchingId(int watchingId) {
        this.watchingId = watchingId;
    }

    public boolean isDidStar() {
        return didStar;
    }

    public void setDidStar(boolean didStar) {
        this.didStar = didStar;
    }
}
