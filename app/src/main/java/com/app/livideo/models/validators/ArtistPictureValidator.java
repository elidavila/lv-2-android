package com.app.livideo.models.validators;

import com.app.livideo.models.ArtistPicture;
import com.app.livideo.utils.TextUtils;

import java.util.List;

/**
 * Created by eli davila on 9/4/2015.
 */
public class ArtistPictureValidator {
    public static boolean isValid(ArtistPicture artistPicture) {
        return artistPicture != null && !TextUtils.isNullOrEmpty(artistPicture.getUrl());
    }

    public static boolean isValid(List<ArtistPicture> artistPictureList) {
        return artistPictureList != null && artistPictureList.size() > 0;
    }
}
