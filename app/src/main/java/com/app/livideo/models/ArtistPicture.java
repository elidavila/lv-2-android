package com.app.livideo.models;

import java.io.Serializable;

/**
 * Created by eli davila on 9/4/2015.
 */
public class ArtistPicture implements Serializable {
    private String pictureId, userId, url;

    public String getPictureId() { return pictureId; }
    public void setPictureId(String pictureId) { this.pictureId = pictureId; }

    public String getUserId() { return userId; }
    public void setUserId(String userId) { this.userId = userId; }

    public String getUrl() { return url; }
    public void setUrl(String url) { this.url = url; }

    @SuppressWarnings("HardCodedStringLiteral")
    public static class Keys {
        public static final String PICTURE_ID = "PictureID";
        public static final String USER_ID = "UserID";
        public static final String URL = "URL";
    }
}
