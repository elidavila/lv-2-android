package com.app.livideo.models.validators;

import java.util.List;

/**
 * Created by eli davila on 9/8/2015.
 */
public class ArtistTypesValidator {
    public static boolean isValid(List<String> artistTypesList) {
        return artistTypesList != null && artistTypesList.size() > 0;
    }
}
