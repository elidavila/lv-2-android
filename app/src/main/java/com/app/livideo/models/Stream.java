package com.app.livideo.models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by eli on 10/2/2015.
 */

public class Stream implements Serializable {

    String streamId;
    String headline;
    String time;
    String artistId;
    String message;
    String url;
    String displayName;

    public String getShortURI() {
        return shortURI;
    }

    public void setShortURI(String shortURI) {
        this.shortURI = shortURI;
    }

    String shortURI;
    private int watchingId;
    private String duration;
    private boolean isPublic;
    private List<ArtistChatMessage> streamComments;
    private boolean didStar;
    private Artist artist;
    private Artist openMicUser;

    public Artist getOpenMicUser() {
        return openMicUser;
    }

    public void setOpenMicUser(Artist openMicUser) {
        this.openMicUser = openMicUser;
    }

    public Artist getArtist() {
        return artist;
    }

    public void setArtist(Artist artist) {
        this.artist = artist;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public List<ArtistChatMessage> getStreamComments() {
        return streamComments;
    }

    public void setStreamComments(List<ArtistChatMessage> streamComments) {
        this.streamComments = streamComments;
    }

    public boolean isDidStar() {
        return didStar;
    }

    public void setDidStar(boolean didStar) {
        this.didStar = didStar;
    }

    public String getStreamId() {
        return streamId;
    }

    public void setStreamId(String streamId) {
        this.streamId = streamId;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }


    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getArtistId() {
        return artistId;
    }

    public void setArtistId(String artistId) {
        this.artistId = artistId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getWatchingId() {
        return watchingId;
    }

    public void setWatchingId(int watchingId) {
        this.watchingId = watchingId;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Stream other = (Stream) obj;
        return (this.streamId == null) ? other.streamId == null : this.streamId.equals(other.streamId);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + (this.streamId != null ? this.streamId.hashCode() : 0);
        return hash;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public void setIsPublic(boolean isPublic) {
        this.isPublic = isPublic;
    }

    public boolean isPublic() {
        return isPublic;
    }

    @SuppressWarnings("HardCodedStringLiteral")
    public static class Keys {
        public static final String STREAMS = "Streams";
        public static final String STREAM_ID = "StreamID";
        public static final String HEADLINE = "Headline";
        public static final String TIME = "Time";
        public static final String ARTIST_ID = "ArtistID";
        public static final String MESSAGE = "Message";
        public static final String USER_ID = "UserID";
        public static final String URL = "URL";
        public static final String SHORT_URI = "shortURI";
        public static final String WATCHING_ID = "WatchingID";
        public static final String DURATION = "Duration";
        public static final String IS_PUBLIC = "isPublic";
        public static final String DID_STAR = "DidStar";
        public static final String DO_STAR = "DoStar";
        public static final String STARS = "Stars";
        public static final String RID = "RID";
        public static final String STREAM = "Stream";
        public static final String DISPLAY_NAME = "Displayname";
        public static final String OPEN_MIC_USER = "OpenMicUser";
    }
}
