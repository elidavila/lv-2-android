package com.app.livideo.models.translators;

import android.util.Log;

import com.app.livideo.models.ArtistChatMessage;
import com.app.livideo.models.validators.ArtistChatMessageValidator;
import com.app.livideo.utils.JSONHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by eli davila on 9/22/2015.
 */
public class ArtistChatMessageTranslator {

    public static List<ArtistChatMessage> getChatMessageList(String jsonString) {
        List<ArtistChatMessage> chatMessageList = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            if(JSONHelper.containsKey(jsonObject, ArtistChatMessage.Keys.MESSAGES)) {
                JSONArray messagesArray = JSONHelper.getJSONArray(jsonObject, ArtistChatMessage.Keys.MESSAGES);
                for (int i = 0; i < messagesArray.length(); i++) {
                    JSONObject obj = messagesArray.getJSONObject(i);
                    ArtistChatMessage artistChatMessage = getArtistChatMessage(obj);
                    if (ArtistChatMessageValidator.isValid(artistChatMessage))
                        chatMessageList.add(artistChatMessage);
                }
            }
        } catch (Exception e) {
            Log.d("ERROR", "PARSING ARTIST MESSAGE ERROR");  //NON-NLS
            e.printStackTrace();
        }
        return chatMessageList;
    }

    private static ArtistChatMessage getArtistChatMessage(JSONObject obj) {
        ArtistChatMessage artistChatMessage = new ArtistChatMessage();
        artistChatMessage.setMessageId(JSONHelper.getString(obj, ArtistChatMessage.Keys.MESSAGE_ID));
        artistChatMessage.setMessage(JSONHelper.getString(obj, ArtistChatMessage.Keys.MESSAGE));
        artistChatMessage.setMessageUserId(JSONHelper.getString(obj, ArtistChatMessage.Keys.MESSAGE_USERS_ID));
        artistChatMessage.setDisplayName(JSONHelper.getString(obj, ArtistChatMessage.Keys.DISPLAY_NAME));
        artistChatMessage.setPictureUrl(JSONHelper.getString(obj, ArtistChatMessage.Keys.PICTURE_URL));
        return artistChatMessage;
    }
}
