package com.app.livideo.models.translators;

import android.util.Log;

import com.app.livideo.utils.Constants;

import org.json.JSONObject;

/**
 * Created by Christian on 23.10.15.
 */
public class ArtistUploadVideoTranslator {

    public static final String CLASSNAME = "ArtistUploadVideoTranslator".substring(0,22);   //short string for logcat

    //Constants
    public static final String RESPONSE_STREAM_ID = "StreamID";
    public static final String RESPONSE_SHORT_URI = "shortURI";
    public static final String RESPONSE_STREAM_RECYCLING_ID = "RecyclingID";

    public static final String PARAM_STREAM_JSON_KEY_HEADLINE = "Headline";
    public static final String PARAM_STREAM_JSON_KEY_TIME = "Time";
    public static final String PARAM_STREAM_JSON_KEY_LOCATION_LO = "Location_LO";
    public static final String PARAM_STREAM_JSON_KEY_LOCATION_LA = "Location_LA";
    public static final String PARAM_STREAM_JSON_KEY_ARTIST_ID = "ArtistID";
    public static final String IS_PUBLIC = "isPublic";

    public static final String RESPONSE_RETURN_CODE = "Code";


    ////////////////////////////////////////////////////////////////////////////////////////////////
    public static String getResult(String jsonString) {
        try {
            JSONObject jsonObject = new JSONObject(jsonString);

//            Log.d(CLASSNAME, jsonObject.toString());
//
//            Log.d(CLASSNAME, "ContinentData: "+jsonObject.has(RESPONSE_STREAM_ID));
//            Log.d(CLASSNAME, "CountData: " + jsonObject.has(RESPONSE_STREAM_RECYCLING_ID));

            return (String)jsonObject.get(RESPONSE_STREAM_ID);
        }
        catch (Exception e){
//            Log.e(CLASSNAME, "getResult:JSONEX "+e.getMessage());
        }

        return null;
    }

    public static String getShortURI(String jsonString) {
        try {
            JSONObject jsonObject = new JSONObject(jsonString);

//            Log.d(CLASSNAME, jsonObject.toString());
//
//            Log.d(CLASSNAME, "ContinentData: "+jsonObject.has(RESPONSE_STREAM_ID));
//            Log.d(CLASSNAME, "CountData: " + jsonObject.has(RESPONSE_STREAM_RECYCLING_ID));

            return (String)jsonObject.get(RESPONSE_SHORT_URI);
        }
        catch (Exception e){
//            Log.e(CLASSNAME, "getResult:JSONEX "+e.getMessage());
        }

        return null;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    //validation
    public static int getSuccesscode(String jsonString) {
        int returnCode = Constants.USERCODE_INVALID_RESPONSE;

        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            returnCode = (int)jsonObject.get(RESPONSE_RETURN_CODE);

            return returnCode;
        }
        catch (Exception e) {
//            Log.e(CLASSNAME, "getSuccesscode-JSONError: "+e.getMessage());
        }

        return Constants.USERCODE_INVALID_RESPONSE;
    }
    public static boolean succeeded(String jsonString) {
        return getSuccesscode(jsonString) == Constants.USERCODE_SUCCESS;
    }

}
