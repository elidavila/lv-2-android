package com.app.livideo.models.translators;

import com.app.livideo.models.Artist;
import com.app.livideo.models.ArtistPage;
import com.app.livideo.models.Stream;
import com.app.livideo.models.validators.ArtistPictureValidator;
import com.app.livideo.models.validators.ArtistValidator;
import com.app.livideo.utils.JSONHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eli on 4/12/2016.
 */
public class TimeLineTranslator {

    public static List<Object> getTimeLines(JSONArray jsonArray, Artist artist) {
        List<Object> mTimeLines = new ArrayList<>();
        if (ArtistValidator.isValid(artist) && ArtistPictureValidator.isValid(artist.getArtistPictures()))
            mTimeLines.add(artist.getArtistPictures().get(artist.getArtistPictures().size() - 1));
        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                Object timeLineObject = getTimeLine(jsonObject);
                if (timeLineObject != null)
                    mTimeLines.add(timeLineObject);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mTimeLines;
    }

    private static Object getTimeLine(JSONObject jsonObject) {
        Object timeLineObject = null;
        if (JSONHelper.getString(jsonObject, ArtistPage.Keys.OBJECT_TYPE).equalsIgnoreCase(Stream.Keys.STREAM)) {
            timeLineObject = StreamTranslator.getStream(jsonObject);
        } else if (JSONHelper.getString(jsonObject, ArtistPage.Keys.OBJECT_TYPE).equalsIgnoreCase(ArtistPage.Keys.PICTURE)) {
            timeLineObject = ArtistPictureTranslator.getArtistPicture(jsonObject);
        }
        return timeLineObject;
    }
}
