package com.app.livideo.models;

import com.app.livideo.models.validators.UserImageValidator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eli on 8/24/2015.
 */
public class User implements Serializable {
    // User Info
    private String birthdate, userID, displayName, socialID, socialType, isPaying, timeZone, email, deviceId, phoneNumber, shortURL;
    private List<UserImage> pictures;
    private UserRole currentRole;
    private int hasMembership;

    // Artist Info
    private String artistType, isAllAges;
    private boolean isEnteredChat;
    private int photoPosition = -1;
    private List<PrivateChatMessage> privateChatMessages;

    public String getShortURL() {
        return shortURL;
    }

    public void setShortURL(String shortURL) {
        this.shortURL = shortURL;
    }

    public int getHasMembership() {
        return hasMembership;
    }

    public void setHasMembership(int hasMembership) {
        this.hasMembership = hasMembership;
    }

    public boolean isEnteredChat() {
        return isEnteredChat;
    }

    public void setEnteredChat(boolean enteredChat) {
        isEnteredChat = enteredChat;
    }

    public String getArtistType() {
        return artistType;
    }

    public void setArtistType(String artistType) {
        this.artistType = artistType;
    }

    public String getIsAllAges() {
        return isAllAges;
    }

    public void setIsAllAges(String isAllAges) {
        this.isAllAges = isAllAges;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String bday) {
        this.birthdate = bday;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String name) {
        this.displayName = name;
    }

    public String getSocialID() {
        return socialID;
    }

    public void setSocialID(String socialID) {
        this.socialID = socialID;
    }

    public String getSocialType() {
        return socialType;
    }

    public void setSocialType(String socialType) {
        this.socialType = socialType;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<UserImage> getPictures() {
        if (UserImageValidator.isValid(pictures)) {
            return pictures;
        } else {
            pictures = new ArrayList<>();
            return pictures;
        }
    }

    public void setPictures(List<UserImage> pictures) {
        this.pictures = pictures;
    }

    public String getDeviceID() {
        return deviceId;
    }

    public void setDeviceID(String deviceId) {
        this.deviceId = deviceId;
    }

    public int getPhotoPosition() {
        return photoPosition;
    }

    public void setPhotoPosition(int photoPosition) {
        this.photoPosition = photoPosition;
    }

    public UserRole getUserRole() {
        return currentRole;
    }

    public void setCurrentRole(UserRole currentRole) {
        this.currentRole = currentRole;
    }

    public List<PrivateChatMessage> getPrivateChatMessages() {
        return privateChatMessages;
    }

    public void setPrivateChatMessages(List<PrivateChatMessage> privateChatMessages) {
        this.privateChatMessages = privateChatMessages;
    }

    public enum UserRole {
        VIEWER, GUEST, BROADCASTER
    }

    @SuppressWarnings("HardCodedStringLiteral")
    public static class Keys {
        //Response key values
        public static final String BIRTHDATE = "Birthdate";
        public static final String USER_ID = "UserID";
        public static final String DISPLAY_NAME = "Displayname";
        public static final String SOCIAL_ID = "SocialID";
        public static final String SOCIAL_TYPE = "SocialType";
        public static final String TIME_ZONE = "TimeZone";
        public static final String EMAIL = "Email";
        public static final String PICTURES = "Pictures";
        public static final String ERROR = "ErrorCode";

        //Parameters
        public static final String PARAMS_EMAIL = "Email";
        public static final String PARAMS_SOCIAL_ID = "SocialID";
        public static final String PARAMS_SOCIAL_TYPE = "SocialType";
        public static final String PARAMS_DISPLAY_NAME = "Displayname";
        public static final String PARAMS_USER_ID = "UserID";
        public static final String PARAMS_PHONE = "phoneNumber";
        public static final String PARAMS_DEVICE_ID = "DeviceID";
        public static final String PARAMS_PASSWORD = "Password";
        public static final String PARAMS_API_VERSION = "APIVERSION";
        public static final String PARAMS_CODE = "Code";
        public static final String PARAMS_BIRTHDAY = "Birthdate";
        public static final String PARAMS_IS_PHONE = "isPhone";
        public static final String LANG = "Lang";
        public static final String RANDOMIZE_PHOTO = "RandomizePhotos";
        public static final String GENDER = "gender";
        public static final String PASSWORD = "Password";
        public static final String IS_ARTIST = "isArtist";
        public static final String USERS = "Users";
    }

    public static class ArtistTypes {
        public static final String COMEDY = "comedy";
        public static final String FASHION = "fashion";
        public static final String LIFSTYLE = "lifestyle";
        public static final String MODEL = "model";
        public static final String MUSIC = "music";
        public static final String RADIO = "radio";
        public static final String SPORTS = "sports";
        public static final String TVFILM = "tv & film";
        public static final String YOUTUBE = "youtube";
    }
}
