package com.app.livideo.models.validators;

import com.app.livideo.models.VineData;
import com.app.livideo.utils.TextUtils;

/**
 * Created by eli on 12/1/2015.
 */
public class VineDataValidator {
    public static boolean isValid(VineData vineData) {
        return vineData != null && !TextUtils.isNullOrEmpty(vineData.getUserId());
    }

}
