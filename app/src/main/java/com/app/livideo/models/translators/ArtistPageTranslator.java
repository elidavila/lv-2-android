package com.app.livideo.models.translators;

import com.app.livideo.models.ArtistPage;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.JSONHelper;

import org.json.JSONObject;

/**
 * Created by Eli on 4/12/2016.
 */
public class ArtistPageTranslator {
    public static ArtistPage getArtistPage(JSONObject jsonObject) {
        ArtistPage mArtistPage = new ArtistPage();
        mArtistPage.setArtist(ArtistTranslator.getArtist(JSONHelper.getJSONObject(jsonObject, AppConstants.ARTIST)));
        mArtistPage.setTimeLines(TimeLineTranslator.getTimeLines(JSONHelper.getJSONArray(jsonObject, ArtistPage.Keys.TIME_LINE), mArtistPage.getArtist()));
        mArtistPage.setUserInfo(UserInfoTranslator.getUserInfo(JSONHelper.getJSONObject(jsonObject, ArtistPage.Keys.USER_INFO)));
        return mArtistPage;
    }
}
