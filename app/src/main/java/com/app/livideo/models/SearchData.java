package com.app.livideo.models;

import java.util.List;

/**
 * Created by Eli on 4/19/2016.
 */
public class SearchData {

    List<Artist> artists;
    List<Stream> streams;

    public List<Stream> getStreams() {
        return streams;
    }

    public void setStreams(List<Stream> streams) {
        this.streams = streams;
    }

    public List<Artist> getArtists() {
        return artists;
    }

    public void setArtists(List<Artist> artists) {
        this.artists = artists;
    }

    public class Keys {
        public static final String SEARCH_ARTISTS = "SearchArtists";
    }
}
