package com.app.livideo.models;

/**
 * Created by Eli on 6/10/2016.
 */
public class YouTubeLoginData {

    String authURI;
    String clientID;
    String tokenURI;
    String redirectURI;

    public String getAuthURI() {
        return authURI;
    }

    public void setAuthURI(String authURI) {
        this.authURI = authURI;
    }

    public String getRedirectURI() {
        return redirectURI;
    }

    public void setRedirectURI(String redirectURI) {
        this.redirectURI = redirectURI;
    }


    public String getClientID() {
        return clientID;
    }

    public void setClientID(String clientID) {
        this.clientID = clientID;
    }

    public String getTokenURI() {
        return tokenURI;
    }

    public void setTokenURI(String tokenURI) {
        this.tokenURI = tokenURI;
    }


    @SuppressWarnings("HardCodedStringLiteral")
    public static class Keys {
        public static final String CLIENT_ID = "client_id";
        public static final String AUTH_URI = "auth_uri";
        public static final String TOKEN_URI = "token_uri";
        public static final String REDIRECT_URI = "redirect_uri";
        public static final String SCOPE = "scope";
        public static final String RESPONSE_TYPE = "response_type";
        public static final String CODE = "code";
        public static final String GRANT_TYPE = "grant_type";
        public static final String AUTHORIZATION_CODE = "authorization_code";
        public static final String MY_SUBSCRIBERS = "mySubscribers";
        public static final String ID = "id";
        public static final String PART = "part";
        public static final String ACCESS_TOKEN = "access_token";
        public static final String SNIPPET = "snippet";
        public static final String MINE = "mine";
        public static final String ITEMS = "items";
        public static final String TITLE = "title";
        public static final String PAGE_INFO = "pageInfo";
        public static final String TOTAL_RESULTS = "totalResults";
    }
}
