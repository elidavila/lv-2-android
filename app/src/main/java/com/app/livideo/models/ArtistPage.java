package com.app.livideo.models;

import java.util.List;

/**
 * Created by Eli on 4/12/2016.
 */
public class ArtistPage {
    Artist artist;
    List<Object> timeLines;
    UserInfo userInfo;

    public Artist getArtist() {
        return artist;
    }

    public void setArtist(Artist artist) {
        this.artist = artist;
    }

    public List<Object> getTimeLines() {
        return timeLines;
    }

    public void setTimeLines(List<Object> timeLines) {
        this.timeLines = timeLines;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    @SuppressWarnings("HardCodedStringLiteral")
    public static class Keys {
        public static final String TIME_LINE = "TimeLine";
        public static final String OBJECT_TYPE = "ObjectType";
        public static final String PICTURE = "Picture";
        public static final String USER_INFO = "UserInfo";
    }
}
