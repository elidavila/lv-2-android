package com.app.livideo.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.app.livideo.R;
import com.app.livideo.api.APICalls;
import com.app.livideo.interfaces.CodeNumberHandler;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.translators.ArtistUploadVideoTranslator;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.S3Uploader;
import com.app.livideo.utils.TextUtils;

import java.io.File;

import javax.inject.Inject;

public class S3UploadService extends Service {

    //params
    public static final String EXTRA_STREAM_TITLE = "com.app.livideo.services.extra.STREAM_TITLE";
    public static final String EXTRA_FILENAME_ON_S3 = "com.app.livideo.services.extra.FILENAME_ON_S3";
    public static final String EXTRA_LOCAL_FILE = "com.app.livideo.services.extra.LOCAL_FILE";
    public static final String EXTRA_PUSH_ON_UPLOAD_COMPLETED = "com.app.livideo.services.extra.PUSH_ON_UPLOAD_COMPLETED";
    public static final String EXTRA_IS_PICTURE = "com.app.livideo.services.extra.EXTRA_IS_PICTURE";

    //constants
    public static final String VIDEO_UPLOAD_FILE_EXT = "mov";
    public static final String VIDEO_UPLOAD_S3_DIRECTORY = "streams";
    public static final String WAKELOCK_TAG = "s3upload.wakelock.tag";
    ////////////////////////////////////////////////////////////////////////////////////////////////

    @Inject RequestQueue mRequestQueue;
    @Inject LVSession mSession;
    @Inject Context mContext;
    private boolean hasSentNotifcation = false;
    String mPrivateUserID;

    public S3UploadService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        LVInject.inject(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            File local_file = (File) intent.getSerializableExtra(EXTRA_LOCAL_FILE);
            boolean isPicture = intent.getBooleanExtra(EXTRA_IS_PICTURE, false);

            if (isPicture) {
                String mPrivatePictureId = intent.getStringExtra(AppConstants.PRIVATE_PICTURE_ID);
                if (!TextUtils.isNullOrEmpty(mPrivatePictureId)) {
                    mPrivateUserID = intent.getStringExtra(AppConstants.PRIVATE_USER_ID);
                    UploadImageToS3(local_file,
                            mPrivateUserID + mSession.getUser().getUserID(),
                            mPrivatePictureId);
                } else {
                    UploadImageToS3(local_file, mSession.getUser().getUserID(), java.util.UUID.randomUUID().toString());
                }
            } else {
                //read information from intent
                String stream_title = intent.getStringExtra(EXTRA_STREAM_TITLE);
                String filename_s3 = intent.getStringExtra(EXTRA_FILENAME_ON_S3);
                boolean pushOnUploadCompleted = intent.getBooleanExtra(EXTRA_PUSH_ON_UPLOAD_COMPLETED, false);
                uploadVideoToS3(stream_title, filename_s3, local_file, pushOnUploadCompleted, startId);
            }
        }
        return START_STICKY;
    }

    private void UploadImageToS3(File imageFile, final String folderName, final String newPictureId) {
        final String fileName, extension;
        if (imageFile != null) {
            fileName = newPictureId;
            extension = "jpg";
            S3Uploader s3Uploader = S3Uploader.getInstance(mSession);
            s3Uploader.startUploading(folderName, fileName, extension, imageFile, mContext, new TransferListener() {
                @Override
                public void onStateChanged(int i, TransferState transferState) {
                    if (transferState.equals(TransferState.COMPLETED)) {
                        if (folderName.equalsIgnoreCase(mSession.getUser().getUserID())) {
                            APICalls.requestImageUpload(mSession, null, mContext, mSession.getUser().getUserID(), mSession.getUser().getDeviceID(), newPictureId, mRequestQueue);
                        } else {
                            APICalls.sendPrivateChatMessage(null, mContext, mPrivateUserID, AppConstants.TYPE_PICTURE, newPictureId, mSession, mRequestQueue, new CodeNumberHandler() {
                                @Override
                                public void RunCompleted(int code) {
                                    if (code == AppConstants.USERCODE_BLOCKED) {
                                        Toast.makeText(mContext, getString(R.string.user_has_blocked_you), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        }
                    }
                }

                @Override
                public void onProgressChanged(int i, long l, long l1) {
                }

                @Override
                public void onError(int i, Exception e) {
                }
            });
        }
    }

    private void uploadVideoToS3(final String stream_title, final String filename_s3, final File local_file,
                                 final boolean pushOnUploadCompleted, final int serviceStartID) {
        //acquire wakelock
        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        final PowerManager.WakeLock wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, WAKELOCK_TAG);
        wakeLock.acquire();

        //get S3Uploader Instance
        S3Uploader s3Uploader = S3Uploader.getInstance(mSession);

        s3Uploader.startUploading(
                VIDEO_UPLOAD_S3_DIRECTORY,
                filename_s3,
                VIDEO_UPLOAD_FILE_EXT,
                local_file,
                mContext,
                new TransferListener() {
                    @Override
                    public void onStateChanged(int id, TransferState transferState) {
                        switch (transferState) {
                            case IN_PROGRESS:
                                break;
                            case COMPLETED:
                                Log.d(this.getClass().getName(), "S3 upload completed!");
                                //fire create thumbnail request to server
                                APICalls.requestCreateThumbnail(mSession, null, mContext,
                                        mSession.getUser().getUserID(),
                                        mSession.getUser().getDeviceID(),
                                        filename_s3,
                                        new Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String response) {
                                                Log.d(this.getClass().getName(), "requestCreateThumbnail - response: " + response);
                                            }
                                        },
                                        new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError error) {
                                                Log.e(this.getClass().getName(), "requestCreateThumbnail - error: " + error.getMessage());
                                            }
                                        },
                                        mRequestQueue
                                );
                                //send push if pushOnUploadCompleted is set
                                if (pushOnUploadCompleted && !hasSentNotifcation) {
                                    hasSentNotifcation = true;
                                    APICalls.requestPushNotificationNewStream(mSession, null, mContext,
                                            mSession.getUser().getUserID(),
                                            mSession.getUser().getDeviceID(),
                                            filename_s3,
                                            new Response.Listener<String>() {
                                                @Override
                                                public void onResponse(String response) {
                                                    Log.e(this.getClass().getName(), "SCHEDULED STREAM: " + response);
                                                    if (ArtistUploadVideoTranslator.succeeded(response)) {
                                                        // TODO: This so far does nothing. I have not seen any notification after recording - Eli
                                                        Notification.Builder notificationBuilder = new Notification.Builder(mContext);
                                                        notificationBuilder
                                                                .setOngoing(false)
                                                                .setShowWhen(true)
                                                                .setSmallIcon(R.mipmap.ic_launcher)
                                                                .setContentTitle(getResources().getString(R.string.app_name))
                                                                .setContentInfo(stream_title + " has been pushed!");

                                                        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                                                        Notification notification = notificationBuilder.build();

                                                        startForeground(222, notification);   //ID 222 for notification video pushed
                                                    }
                                                }
                                            },
                                            new Response.ErrorListener() {
                                                @Override
                                                public void onErrorResponse(VolleyError error) {
                                                    error.printStackTrace();
                                                    Log.e(this.getClass().getName(), "SCHEDULED STREAM ERROR" + error.toString());
                                                }
                                            },
                                            mRequestQueue
                                    );
                                }
                                //release wakelock
                                wakeLock.release();
                                stopForeground(false);
                                stopSelf(serviceStartID);
                                break;
                            case CANCELED:
                                wakeLock.release();
                                stopForeground(false);
                                stopSelf(serviceStartID);
                                break;
                        }
                    }

                    @Override
                    public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                        int progress = bytesCurrent == bytesTotal ? 101 : (int) (100 * bytesCurrent / bytesTotal);
                        Log.d(this.getClass().getName(), "Current Upload Progress: " + progress + "% - " + bytesCurrent + "/" + bytesTotal + " bytes");
                        setupNotification(id, progress, 100, stream_title);
                    }

                    @Override
                    public void onError(int id, Exception e) {
                        Log.e(this.getClass().getName(), e.getMessage());
                        setupErrorNotification(id, stream_title);
                    }
                }
        );
    }

    private void setupNotification(int id, int progress, int max, String stream_title) {
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);

        if (progress > max) {     //to avoid rounding errors progress will be set to 101% on complete
            notificationBuilder
                    .setOngoing(false)
                    .setShowWhen(true)
                    .setSmallIcon(android.R.drawable.stat_sys_upload_done)
                    .setContentTitle(getResources().getString(R.string.notification_upload_video_title_done))
                    .setContentInfo(getResources().getString(R.string.notification_upload_video_stream_title) + stream_title);
        } else {
            notificationBuilder
                    .setOngoing(true)
                    .setShowWhen(false)
                    .setSmallIcon(android.R.drawable.stat_sys_upload)
                    .setContentTitle(getResources().getString(R.string.notification_upload_video_title))
                    .setContentInfo(getResources().getString(R.string.notification_upload_video_stream_title) + stream_title + " - " + progress + "%")
                    .setProgress(max, progress, false);
        }

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Notification notification = notificationBuilder.build();

        startForeground(id, notification);
    }

    private void setupErrorNotification(int id, String stream_title) {
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this)
                        .setOngoing(false)
                        .setShowWhen(true)
                        .setSmallIcon(android.R.drawable.stat_notify_error)
                        .setContentTitle(getResources().getString(R.string.notification_upload_video_title_failed))
                        .setContentText(stream_title);

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Notification notification = notificationBuilder.build();

        startForeground(id, notification);
    }
}
