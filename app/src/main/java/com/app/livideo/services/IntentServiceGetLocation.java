package com.app.livideo.services;

import android.app.IntentService;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.ResultReceiver;

import java.util.List;
import java.util.Locale;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 */
@SuppressWarnings("HardCodedStringLiteral")
public class IntentServiceGetLocation extends IntentService {

    public static final int RESULT_SUCCESS = 0;
    public static final int RESULT_FAILURE = 1;
    protected ResultReceiver mReceiver;
    public static final String EXTRA_LOCATION = "com.app.livideo.receivers.extra.LOCATION";
    public static final String EXTRA_RECEIVER = "com.app.livideo.receivers.extra.RECEIVER";
    public static final String RESULT_ADDRESS = "com.app.livideo.receivers.result.ADDRESS";

    public IntentServiceGetLocation() {
        super("IntentServiceLocationReceiver");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        mReceiver = intent.getParcelableExtra(EXTRA_RECEIVER);
        Location location = intent.getParcelableExtra(EXTRA_LOCATION);
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            if (addresses != null && addresses.size() > 0) {
                deliverResultToReceiver(RESULT_SUCCESS, addresses.get(0));
            } else {
                deliverResultToReceiver(RESULT_FAILURE, null);
            }
        } catch (Exception e) {
            deliverResultToReceiver(RESULT_FAILURE, null);
        }
    }

    private void deliverResultToReceiver(int resultCode, Address address) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(RESULT_ADDRESS, address);
        mReceiver.send(resultCode, bundle);
    }

}
