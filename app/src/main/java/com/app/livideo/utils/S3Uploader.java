package com.app.livideo.utils;

import android.content.Context;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.app.livideo.activities.SplashActivity;
import com.app.livideo.lv_app.LVSession;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Alexander on 19.10.15.
 */
public class S3Uploader {

    LVSession mSession;
    private static S3Uploader ourInstance = new S3Uploader();

    public static S3Uploader getInstance(LVSession session) {
        ourInstance.mSession = session;
        return ourInstance;
    }

    private S3Uploader() {
    }


    /**
     * Upload files to Amazon S3
     *
     * @param folder
     * @param FileName
     * @param extension
     * @param sourceFile
     * @param mContext
     * @param transferListener
     * <p>
     *      observer.setTransferListener(new TransferListener() {<br/><br/>
     *           public void onStateChanged(<b>int id, String newState</b>) {<br/>
     *               // Do something in the callback.<br/>
     *           }<br/>
     *<br/>
     *           public void onProgressChanged(<b>int id, long bytesCurrent, long bytesTotal</b>) {<br/>
     *               // Do something in the callback.<br/>
     *           }<br/>
     *<br/>
     *           public void onError(<b>int id, Exception e</b>) {<br/>
     *               // Do something in the callback.<br/>
     *           }<br/><br/>
     *       });<br/>
     * </p>
     */
    public void startUploading(String folder, String FileName, String extension, File sourceFile, Context mContext, TransferListener transferListener)
    {
        AmazonS3 s3 = new AmazonS3Client(mSession.getAWSCredentials());
        s3.setRegion(Region.getRegion(Regions.US_EAST_1));
        TransferUtility transferUtility = new TransferUtility(s3, mContext);
        ObjectMetadata myObjectMetadata = new ObjectMetadata();
        Map<String, String> userMetadata = new HashMap<String,String>();
        myObjectMetadata.setUserMetadata(userMetadata);
        TransferObserver observer = transferUtility.upload(
                SplashActivity.mImgUploadString,        /* The bucket to upload to: UrlBuilder.create(UrlBuilder.Endpoint.PRODUCTION_IMG_UPLOAD */
                folder + "/" + FileName + "." + extension,       /* The key for the uploaded object */
                sourceFile,          /* The file where the data to upload exists */
                myObjectMetadata  /* The ObjectMetadata associated with the object*/
        );
        observer.setTransferListener(transferListener);
    }
}
