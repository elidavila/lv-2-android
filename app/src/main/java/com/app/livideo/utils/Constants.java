package com.app.livideo.utils;

/**
 * Created by eli davila on 8/24/2015.
 */
@SuppressWarnings("HardCodedStringLiteral")
public interface Constants {

    //Instagram
    String IN_CLIENT_ID = "bfa955764ee74a6ab0814c18d6b9ab7a";
    String IN_CLIENT_SECRET = "09efb27d5a3e471387a8dcd63f7561a8";
    String IN_REDIRECT_URI = "igbfa955764ee74a6ab0814c18d6b9ab7a://authorize";

    //Action Codes
    String ACTION_NORMAL_LOGIN = "98";
    String ACTION_CREATE_LIVIDEOUSER = "99";
    String ACTION_LOGIN_USER = "100";
    String ACTION_LOGIN_ARTIST = "101";
    String ACTION_GET_ARTIST_DATA = "103";
    String ACTION_SET_DEVICE_TOKEN = "104";
    String ACTION_RESET_BADGE = "107";
    String ACTION_SET_USER_DATA = "108";
    String ACTION_SET_ARTIST_DATA = "109";
    String ACTION_NEW_SUBSCRIPTION = "110";
    String ACTION_REMOVE_SUBSCRIPTION = "111";
    String ACTION_GET_CHANNELS_ARTISTS = "112";
    String ACTION_GET_SUBSCRIBED_ARTISTS = "113";
    String ACTION_ENTER_STREAM = "116";
    String ACTION_GET_MISSED_STREAMS_COUNT = "120";
    String ACTION_STREAM_NEW_COMMENT = "124";
    String ACTION_GET_STREAM_COMMENTS = "125";
    String ACTION_STREAM_COMMENT_LIKE = "126";
    String ACTION_NEW_NOTIFICATION = "127";
    String ACTION_NEW_PRODUCERMESSAGE = "128";
    String ACTION_PUSH_STREAM_NOTIFICATION = "131";
    String ACTION_GET_CHANNELS_CATEGORIES = "132";
    String ACTION_AWS_LOGIN = "133";
    String ACTION_GET_PAYMENT_INFO = "134";
    String ACTION_SET_PAYMENT_INFO = "135";
    String ACTION_USERNAME_AV = "136";
    String ACTION_EMAIL_AV = "137";
    String ACTION_REQUEST_CODE = "138";
    String ACTION_CHECK_CODE = "139";
    String ACTION_CHANGE_PASSWORD = "140";
    String ACTION_GET_CONTINENTS = "146";
    String ACTION_PHONE_AV = "147";
    String ACTION_SEND_CHAT_MESSAGE = "149";
    String ACTION_GET_ARTIST_CHAT_MESSAGES = "150";
    String ACTION_GET_ARTIST_MESSAGE_HEADLINES = "151";
    String ACTION_UPDATE_USER_LOCATION = "152";
    String ACTION_GET_CITYS = "153";
    String ACTION_GET_REVENUE = "154";
    String ACTION_LOGOUT = "155";
    String ACTION_GET_CACHED_STREAMS = "156";
    String ACTION_CHECK_STREAM_ALLOWED = "157";
    String ACTION_BROADCASTER_STREAMS = "158";
    String ACTION_CREATE_THUMBNAIL = "159";
    String ACTION_EXPORT_REPORT = "160";
    String ACTION_ENTERED_CHAT = "162";
    String ACTION_GET_ARTIST_MISSED_STREAMS = "163";
    String ACTION_REMOVE_ARTIST_FROM_FAVORITES = "166";
    String ACTION_ADD_ARTIST_TO_FAVORITES = "167";
    String ACTION_END_STREAM = "169";
    String ACTION_REMOVE_STREAM = "171";
    String ACTION_LOG_INVITES = "172";
    String ACTION_MUTE_SUBSCRIPTION = "174";
    String ACTION_STAR_UNSTAR_STREAM = "175";
    String ACTION_GET_STREAM_STARS= "176";
    String ACTION_GET_DISCOVER_PAGE = "177";
    String ACTION_GET_SEARCH_DATA = "178";
    String ACTION_GET_BIG_DATA_INFO = "181";
    String ACTION_SET_HERO_ARTIST = "182";
    String ACTION_GET_ARTIST_PAGE = "179";
    String ACTION_GET_ME_PAGE = "180";
    String ACTION_NEW_ARTIST = "190";
    String ACTION_UPLOAD_PICTURE = "200";
    String ACTION_DELETE_PICTURE = "201";
    String ACTION_GET_STREAM_DATA = "300";
    String ACTION_NEW_STREAM = "301";
    String ACTION_GET_SUBSCRIPTION_COUNT = "400";
    String ACTION_GET_STREAM_BY_URL = "1200";
    String ACTION_GET_ARTIST_BY_URL = "1310";
    String ACTION_BLOCK_USER = "1900";
    String ACTION_UNBLOCK_USER = "1901";
    String ACTION_GET_BLOCKED_USERS = "1902";
    String ACTION_START_PRIVATE_CHAT = "1903";
    String ACTION_WRITE_PRIVATE_CHAT = "1904";
    String ACTION_GET_PRIVATE_CHAT_MESSAGES = "1905";
    String ACTION_GET_UNIQUE_ID = "1906";
    String ACTION_GET_WHISPER_REQUESTS  = "1909";
    String ACTION_END_PRIVATE_CHAT = "1910";


    int ACTION_GET_USER_DATA = 102;
    int ACTION_GET_PICTURES = 202;

    // User Codes
    int USERCODE_NOT_EXISTS = 1;
    int USERCODE_SUCCESS = 0;
    int USERCODE_OTHER_ERROR = 20;
    int USERCODE_WRONG_PASSWORD = 2;
    int USERCODE_INVALID_RESPONSE = 3;
    int USERCODE_OTHER_DEVICE = 40;
    int USERCODE_NO_PRODUCER = -2;

    // Registration Codes
    int REGISTRATIONCODE_ALREADY_EXISTS = 1;
    int REGISTRATIONCODE_SUCCESS = 0;
    int REGISTRATIONCODE_OTHER_ERROR = 20;

    // Social Type
    String TYPE_TWITTER = "1";
    String TYPE_INSTAGRAM = "4";
    String TYPE_YOUTUBE = "5";
    String TYPE_CALLBACK = "CallbackType";

    // Device Type
    String DEVICE_ANDROID = "1";
    String DEVICE_TYPE_PARAM = "DeviceType";
    String DEBUG_PARAM = "Debug";
    String TOKEN_PARAM = "Token";

    // Constants
    String API_ACTION = "Action";
    String API_ERROR_CODE = "ErrorCode";
    String API_VERSION = "1.4";
    String USER_TYPE_KEY = "lvUserType";
    int VIEWER_USER = 1;
    int BROADCASTER_USER = 2;
    int GUEST_USER = 3;
    String KEY_DISPLAY_NAME = "lvDisplayName";
    String KEY_PW = "lvPassword";
    String KEY_DEVICE = "lvDevice";
    String KEY_EMAIL = "lvEmail";
    String KEY_PHONE = "lvPhone";
    String KEY_SOCIAL_TYPE = "lvSocialType";
    String KEY_SOCIAL_ID = "lvSocialId";
    String KEY_USER_ID = "lvUserId";
    String KEY_BIRTHDATE = "lvBirthdate";

    String SHORT_URI = "shortURI";

    int DONT_OPEN_CAMERA_FIRST = 0;
    int OPEN_CAMERA_FIRST = 1;
    String CAMERA_TYPE_KEY = "lvCameraFirst";

    //Artist Connect Keys
    String PARAMS_NOTIFICATION_KEY_MESSAGE = "Message";
    String PARAMS_NOTIFICATION_KEY_COUNTRY = "Country";
    String PARAMS_NOTIFICATION_KEY_CITY = "City";

    //Register Video To Upload
    String PARAM_STREAM_KEY_STREAM = "Stream";
    String PARAM_STREAM_KEY_IS_DELAYED = "Delayed";
    String PARAM_STREAM_KEY_IS_DELAYED_WITH_QUOTES = "`Delayed`";
    String PARAM_STREAM_KEY_STREAM_ID = "StreamID";

    //Check Stream Allowed
    String KEY_STREAM_SECONDS_LEFT = "AVStreams";

    //Invitation CONSTANTS
    String PARAMETER_INVITATION_TYPE = "InviteType";
    String PARAMETER_INVITEES = "Invitees";
    String INVITE_TYPE_EMAIL = "1";
    String INVITE_TYPE_TEXT = "2";

    String BIG_DATA_STATUS_REQUEST = "status";
    String BIG_DATA_STATUS = "3";
    String BIG_DATA_LINK = "Link";
}
