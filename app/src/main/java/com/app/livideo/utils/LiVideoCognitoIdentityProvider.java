package com.app.livideo.utils;

import android.content.Context;
import android.provider.Settings;

import com.amazonaws.auth.AWSAbstractCognitoDeveloperIdentityProvider;
import com.amazonaws.regions.Regions;
import com.android.volley.RequestQueue;
import com.app.livideo.api.APICalls;
import com.app.livideo.models.AWSIdentity;
import com.app.livideo.models.translators.AWSIdentityTranslator;

/**
 * Created by Alexander on 19.10.15.
 */
public class LiVideoCognitoIdentityProvider extends AWSAbstractCognitoDeveloperIdentityProvider {
    private static final String developerProvider = "login.com.app.livideo";
    private Context mContext;
    private RequestQueue mQueue;
    public LiVideoCognitoIdentityProvider(String accountId, String identityPoolId, Regions region, Context context, RequestQueue queue) {
        super(accountId, identityPoolId, region);
        this.mContext = context;
        this.mQueue = queue;
    }
    @Override
    public String refresh() {
        setToken(null);
        String userID = ".";
        String deviceID = Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.ANDROID_ID);
        String response = APICalls.requestAWSIdentity(userID, deviceID, mQueue);
        if (response != null)
        {
            AWSIdentity awsIdentity = AWSIdentityTranslator.getAWSIdentity(response);
            update(awsIdentity.getId(), awsIdentity.getToken());
            return token;
        }
        return null;

    }
    @Override
    public String getProviderName() {
        return developerProvider;
    }
    @Override
    public String getIdentityId() {

        if (this.identityId == null) {
            refresh();
            return this.identityId;
        } else {
            return identityId;
        }

    }
}
