package com.app.livideo.utils;

import android.content.Context;

import com.app.livideo.models.Country;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

public class PhoneUtils {

    public static String getCountryRegionFromPhone(Context paramContext) {
        String code = paramContext.getResources().getConfiguration().locale.getCountry();
        if (code != null)
            return code.toUpperCase();
        return null;
    }

    public static String validatePhoneNumber(String phoneNumber, Country country) {
        String region = null;
        String mPhoneNumber = null;
        if (phoneNumber != null) {
            try {
                if (!phoneNumber.contains("+") && country != null) {
                    phoneNumber = "+" + country.getCountryCode() + phoneNumber;
                }
                Phonenumber.PhoneNumber p = PhoneNumberUtil.getInstance().parse(phoneNumber, null);
                StringBuilder sb = new StringBuilder(16);
                sb.append('+').append(p.getCountryCode()).append(p.getNationalNumber());
                mPhoneNumber = sb.toString();
                region = PhoneNumberUtil.getInstance().getRegionCodeForNumber(p);
            } catch (NumberParseException ignore) {
            }
        }

        if (region != null) {
            mPhoneNumber = mPhoneNumber.replaceAll("\\+", "");
            return mPhoneNumber;
        } else {
            return null;
        }
    }
}