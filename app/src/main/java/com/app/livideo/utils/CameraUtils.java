package com.app.livideo.utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.SurfaceView;

import com.app.livideo.views.VideoCapturePreview;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Christian on 07.10.2015.
 */
public class CameraUtils {

    private static Context mContext;

    private static int mCurrentSelectedCamera = 0;
    public static Camera mCamera;
    private static MediaRecorder mRecorder;

    private static boolean isRecording = false;
    private static File currentVideoCaptureFile = null;
    static final int cameraHeight = 640;
    static final int cameraWidth = 480;

    public static void setContext(Context context){
        mContext = context;
    }

    public static boolean deviceHasCamera(Context context){
        return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }
    public static boolean cameraHasParameters(Camera c){
        try{
            Camera.Parameters parameters = c.getParameters();
            return true;
        }
        catch (Exception e){

        }
        return false;
    }

    public static void assignCamera(){
        if(mCamera==null)
            mCamera=getCameraInstance(mCurrentSelectedCamera);

        //set preview size
        if(mCamera!=null)
            setCameraPreviewSize();

        //enable autofocus
        if(mCamera!=null)
            enableCameraAutofocus();
    }
    public static Camera getCameraInstance(){
        Camera camera = null;

        try{camera = Camera.open();}
        catch (Exception e){}

        return camera;
    }
    public static Camera getCameraInstance(int index){
        Camera camera = null;

        try{camera = Camera.open(index);}
        catch (Exception e){}

        return camera;
    }
    public static void setCameraPreviewSize(){
        try {
            Camera.Parameters parameters = mCamera.getParameters();
            int[] previewSize = getCameraPreviewSize();
            parameters.setPreviewSize(previewSize[0], previewSize[1]);
            mCamera.setParameters(parameters);
        }
        catch (Exception e){
            Log.e("CameraUtils", "setCameraPreviewSize failed!"); //NON-NLS
            e.printStackTrace();
        }
    }
    public static void enableCameraAutofocus(){
        try {
            Camera.Parameters parameters = mCamera.getParameters();
            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);     //video autofocus mode
            parameters.setRecordingHint(true);
            mCamera.setParameters(parameters);
        }
        catch (Exception e){
            Log.e("CameraUtils", "enableCameraAutofocus failed!"); //NON-NLS
            e.printStackTrace();
        }
    }
    public static int[] getCameraPreviewSize(){
        int[] ps = new int[2];

        int screenWidth = mContext.getResources().getDisplayMetrics().widthPixels;
        int screenHeight = mContext.getResources().getDisplayMetrics().heightPixels;

        float aspectRatio = (float)screenHeight/screenWidth;
        float diff = Float.MAX_VALUE;
        Camera.Size optimalSize = null;

        for(Camera.Size size : mCamera.getParameters().getSupportedPreviewSizes()){
            float ratio = (float)size.width/size.height;
            if(Math.abs(ratio-aspectRatio)<0.1 && Math.abs(size.height-screenHeight)<diff){
                optimalSize = size;
                diff=Math.abs(size.height-screenHeight);
            }
        }

        if(optimalSize==null){
            for(Camera.Size size : mCamera.getParameters().getSupportedPreviewSizes()){
                float ratio = (float)size.width/size.height;
                if(Math.abs(size.height-screenHeight)<diff){
                    optimalSize = size;
                    diff=Math.abs(size.height-screenHeight);
                }
            }
        }

        ps[0] = optimalSize.width;
        ps[1] = optimalSize.height;

        Log.d("CameraUtils", "getCameraPreviewSize: x:" + ps[0] + " y: " + ps[1]); //NON-NLS

        return ps;
    }
    public static int[] getCameraPreviewSize(Context context, Camera camera){
        int[] ps = new int[2];

        int screenWidth = context.getResources().getDisplayMetrics().widthPixels;
        int screenHeight = context.getResources().getDisplayMetrics().heightPixels;

        float aspectRatio = (float)screenHeight/screenWidth;
        float diff = Float.MAX_VALUE;
        Camera.Size optimalSize = null;

        for(Camera.Size size : camera.getParameters().getSupportedPreviewSizes()){
            float ratio = (float)size.width/size.height;
            if(Math.abs(ratio-aspectRatio)<0.1 && Math.abs(size.height-screenHeight)<diff){
                optimalSize = size;
                diff=Math.abs(size.height-screenHeight);
            }
        }

        if(optimalSize==null){
            for(Camera.Size size : camera.getParameters().getSupportedPreviewSizes()){
                float ratio = (float)size.width/size.height;
                if(Math.abs(size.height-screenHeight)<diff){
                    optimalSize = size;
                    diff=Math.abs(size.height-screenHeight);
                }
            }
        }

        ps[0] = optimalSize.width;
        ps[1] = optimalSize.height;

        Log.d("CameraUtils", "getCameraPreviewSize: x:" + ps[0] + " y: " + ps[1]); //NON-NLS

        return ps;
    }
    public static int[] getCameraPreviewSize(CamcorderProfile profile){
        int[] ps = new int[2];

        //set preview aspect ratio & camera parameters
        int x = profile.videoFrameWidth;
        int y = profile.videoFrameHeight;

        ps[0] = x;
        ps[1] = y;

        Log.d("CameraUtils", "getCameraPreviewSize: x:" + ps[0] + " y: " + ps[1]); //NON-NLS

        return ps;
    }

    public static void selectNextCamera(){
        int count = Camera.getNumberOfCameras();
        mCurrentSelectedCamera = (mCurrentSelectedCamera+1) % count;
        assignCamera();
    }

    public static void releaseCamera(){
        if(mCamera!=null){
            mCamera.release();
            mCamera=null;
        }
    }
    public static void releaseVideoRecorder(){
        if(mRecorder!=null){
            mRecorder.reset();
            mRecorder.release();
            mRecorder=null;
            mCamera.lock();
        }
    }

    //returns a file-object to a new mp4-file created in <Video-Path>/LiVideo
    public static File getVideoCaptureFile() {
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES), "LiVideo");

        if (!storageDir.exists()){
            if (!storageDir.mkdirs()) {
                Log.e("CameraUtils", "getVideoFile - unable to create directory!"); //NON-NLS
                return null;
            }
        }

        String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

        return new File(storageDir.getPath() + File.separator + "VID_"+timestamp+".mp4");
    }

    //prepares the MediaRecorder and creates video file (call after released!)
    public static boolean setupVideoRecorder(SurfaceView previewSurface, int recordSec){

        if(mRecorder==null)
            mRecorder = new MediaRecorder();
        else {
            Log.e("CameraUtils", "setupVideoRecorder: mediaRecorder not null!"); //NON-NLS
            return false;
        }

        try {
            mCamera.unlock();
        }
        catch (Exception e){
            Log.e("CameraUtils", "setupVideoRecorder: "+e.getMessage()); //NON-NLS
            e.printStackTrace();
        }
        mRecorder.setCamera(mCamera);

        //set sources
        mRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        mRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);

        //set record quality
//        CamcorderProfile profile = CamcorderProfile.get(mCurrentSelectedCamera, CamcorderProfile.QUALITY_HIGH);
//        profile.fileFormat = MediaRecorder.OutputFormat.MPEG_4;
//        mRecorder.setProfile(profile);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        mRecorder.setVideoFrameRate(30);
        mRecorder.setVideoSize(cameraWidth, cameraHeight);
        mRecorder.setVideoEncodingBitRate(1500);
        mRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
        mRecorder.setMaxDuration(recordSec * 1000);
        //set output-file
        currentVideoCaptureFile = getVideoCaptureFile();
        mRecorder.setOutputFile(currentVideoCaptureFile != null ? currentVideoCaptureFile.toString() : null);

        //set preview surface
        mRecorder.setPreviewDisplay(previewSurface.getHolder().getSurface());

        //set preview surface aspect ratio
//        int[] size = getCameraPreviewSize(profile);
//        ((VideoCapturePreview) previewSurface).setAspectRatio(cameraWidth, cameraHeight);

        //set display orientation
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        Camera.getCameraInfo(mCurrentSelectedCamera, cameraInfo);

        mRecorder.setOrientationHint(cameraInfo.orientation); //degrees

        try{
            mRecorder.prepare();
        }
        catch (Exception e){
            Log.e("CameraUtils", "setupVideoRecorder: error while preparing recorder object\n"+e.getMessage()); //NON-NLS
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public static void toggleFlashTorch(boolean isOn){
        Camera.Parameters cp = mCamera.getParameters();
        if (isOn) {
            cp.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
        } else {
            cp.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
        }
        mCamera.setParameters(cp);
    }

    public static boolean isRecording(){
        return isRecording;
    }
    public static void startRecording(SurfaceView previewSurface, int secondsLeft){
        if(!isRecording){
            releaseVideoRecorder();
            if(setupVideoRecorder(previewSurface, secondsLeft)){
                try{
                    mRecorder.start();
                    isRecording=true;
                    if(onRecordingListener!=null){
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                onRecordingListener.onStartRecording();
                            }
                        });
                    }
                    startTimer();
                }
                catch (Exception e){
                    Log.e("CameraUtils", "startRecording: "+e.getMessage()); //NON-NLS
                    e.printStackTrace();
                    releaseVideoRecorder();
                }
            }
        }
    }
    public static void stopRecording(){
        if(isRecording){
            try{
                mRecorder.stop();
                releaseVideoRecorder();
                isRecording=false;
                stopTimer();
                if(onRecordingListener!=null){
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            onRecordingListener.onStopRecording(currentVideoCaptureFile);
                        }
                    });
                }
                mCamera.lock();
            }
            catch (Exception e){
                Log.e("CameraUtils", "stopRecording: "+e.getMessage()); //NON-NLS
                e.printStackTrace();
            }
        }
    }


    // timer schedule
    private static Timer mTimer;
    private static int secondsCount = 0;
    private static TimerTask mSchedule;
    private static void assignTimerTask() {
        mSchedule = new TimerTask() {
            @Override
            public void run() {
                if (secondsCount < 30) {
                    if (onRecordingListener != null) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                onRecordingListener.onRecordingProgress(++secondsCount);
                            }
                        });
                    }
                } else {
                    stopRecording();
                    stopTimer();
                }
            }
        };
    }
    private static void startTimer(){
        if(mTimer==null) {
            secondsCount = 0;
            assignTimerTask();
            mTimer = new Timer();
            mTimer.scheduleAtFixedRate(mSchedule, 1000, 1000);
        }
    }
    private static void stopTimer(){
        if(mTimer!=null) {
            mTimer.cancel();
            mTimer = null;
            mSchedule = null;
        }
    }

    // listener
    public interface OnRecordingListener {
        void onStartRecording();
        void onRecordingProgress(int secondsCount);
        void onStopRecording(File videoFile);
    }
    private static OnRecordingListener onRecordingListener = null;
    public static void setOnRecordingListener(OnRecordingListener listener) {
        onRecordingListener = listener;
    }
}
