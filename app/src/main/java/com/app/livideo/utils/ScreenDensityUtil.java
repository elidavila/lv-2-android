package com.app.livideo.utils;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;

/**
 * Created by MWheeler on 11/19/2015.
 */
public class ScreenDensityUtil {
    public static final int WIDTH = 0;
    public static final int HEIGHT = 1;

    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public static int convertDpToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        int px = (int) (dp * (metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px A value in px (pixels) unit. Which we need to convert into db
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent dp equivalent to px value
     */
    public static int convertPixelsToDp(float px, Context context){
        return (int) (px * (context.getResources().getDisplayMetrics().density));
    }

    public static int getScreenWidthByPercentage(Context context, double percent) {
        return (int) (context.getResources().getDisplayMetrics().widthPixels * percent);
    }

    public static int getScreenHeightByPercentage(Context context, double percent) {
        return (int) (context.getResources().getDisplayMetrics().heightPixels * percent);
    }

    public static int getScreenHeightPixels(Context context, int divisionFactor) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        return (dm.heightPixels / divisionFactor);
    }

    public static int getScreenWidthPixels(Context context, int divisionFactor) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        return (dm.widthPixels / divisionFactor);
    }

    public static int getWidthFor3Tiles(Context context) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        return (dm.widthPixels / 3);
    }

    public static int getOneThirdOfScreenHeight(Context context) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        return (dm.heightPixels / 3);
    }

    public static int[] getScreenDimensions(Context context) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        return new int[] {dm.widthPixels, dm.heightPixels};
    }


    public static int getHeightForArtistPictures(Context context) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        return (int) (Math.ceil(dm.heightPixels / 3.8));
    }

    public static int getViewerResize(Context context) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        int densityDpi = dm.densityDpi;

        switch (densityDpi) {
            case DisplayMetrics.DENSITY_MEDIUM://1
                return 125;
            case DisplayMetrics.DENSITY_HIGH://1.5
                return 188;
            case DisplayMetrics.DENSITY_280: //1.75
                return 219;
            case DisplayMetrics.DENSITY_XHIGH://2
                return 250;
            case DisplayMetrics.DENSITY_360://2.25
                return 281;
            case DisplayMetrics.DENSITY_400://2.5
                return 313;
            case DisplayMetrics.DENSITY_420://2.625
                return 328;
            case DisplayMetrics.DENSITY_XXHIGH://3
                return 375;
            case DisplayMetrics.DENSITY_560://3.5
//                return 438;
            case DisplayMetrics.DENSITY_XXXHIGH://4
                return 500;
            default:
                return -1;
        }
    }

    public static int getBroadcasterResize(Context context) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        int densityDpi = dm.densityDpi;
        switch (densityDpi) {
            case DisplayMetrics.DENSITY_MEDIUM://1
                return 200;
            case DisplayMetrics.DENSITY_HIGH://1.5
                return 300;
            case DisplayMetrics.DENSITY_280: //1.75
                return 350;
            case DisplayMetrics.DENSITY_XHIGH://2
                return 400;
            case DisplayMetrics.DENSITY_360://2.25
                return 450;
            case DisplayMetrics.DENSITY_400://2.5
                return 500;
            case DisplayMetrics.DENSITY_420://2.625
                return 525;
            case DisplayMetrics.DENSITY_XXHIGH://3
                return 600;
            case DisplayMetrics.DENSITY_560://3.5
                return 700;
            case DisplayMetrics.DENSITY_XXXHIGH://4
                return 800;
            default:
                return -1;
        }
    }

    public static int getFlatArtistResize(Context context) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        int densityDpi = dm.densityDpi;
        switch (densityDpi) {
            case DisplayMetrics.DENSITY_MEDIUM://1
                return 172;
            case DisplayMetrics.DENSITY_HIGH://1.5
                return 258;
            case DisplayMetrics.DENSITY_280: //1.75
                return 301;
            case DisplayMetrics.DENSITY_XHIGH://2
                return 344;
            case DisplayMetrics.DENSITY_360://2.25
                return 387;
            case DisplayMetrics.DENSITY_400://2.5
                return 430;
            case DisplayMetrics.DENSITY_420://2.625
                return 452;
            case DisplayMetrics.DENSITY_XXHIGH://3
                return 490;
            case DisplayMetrics.DENSITY_560://3.5
                return 602;
            case DisplayMetrics.DENSITY_XXXHIGH://4
                return 602;
            default:
                return -1;
        }
    }

    public static int getFancyCoverResize(Context context) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        int densityDpi = dm.densityDpi;
        switch (densityDpi) {
            case DisplayMetrics.DENSITY_MEDIUM://1
                return 181;
            case DisplayMetrics.DENSITY_HIGH://1.5
                return 272;
            case DisplayMetrics.DENSITY_280: //1.75
                return 317;
            case DisplayMetrics.DENSITY_XHIGH://2
                return 362;
            case DisplayMetrics.DENSITY_360://2.25
                return 407;
            case DisplayMetrics.DENSITY_400://2.5
                return 453;
            case DisplayMetrics.DENSITY_420://2.625
                return 475;
            case DisplayMetrics.DENSITY_XXHIGH://3
                return 543;
            case DisplayMetrics.DENSITY_560://3.5
                return 800;
            case DisplayMetrics.DENSITY_XXXHIGH://4
                return 725;
            default:
                return -1;
        }
    }

    public static int getArtistMePageItemResize(Context context) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        return (dm.widthPixels / 2);
    }

    public static int getChannelsScroll(Context context) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        return ((dm.widthPixels / 2) - (getImageSize(context) / 2) - (12*(int)dm.density));
    }

    public static int getPlusPadding(Context context) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        return (dm.widthPixels / 10);
    }

    public static int getFancyCoverSpacing(Context context) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        int densityDpi = dm.densityDpi;

        switch (densityDpi) {
            case DisplayMetrics.DENSITY_MEDIUM://1
                return -50;
            case DisplayMetrics.DENSITY_HIGH://1.5
                return -75;
            case DisplayMetrics.DENSITY_280: //1.75
                return -88;
            case DisplayMetrics.DENSITY_XHIGH://2
                return -100;
            case DisplayMetrics.DENSITY_360://2.25
                return -113;
            case DisplayMetrics.DENSITY_400://2.5
                return -125;
            case DisplayMetrics.DENSITY_420://2.625
                return -131;
            case DisplayMetrics.DENSITY_XXHIGH://3
                return -150;
            case DisplayMetrics.DENSITY_560://3.5
                return -175;
            case DisplayMetrics.DENSITY_XXXHIGH://4
                return -200;
            default:
                return -1;
        }
    }

    public static int getImageSize(Context context) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        return (dm.widthPixels / 4);
    }

    public static int getArtistMePageItemArtistLabel(Context context) {
        return (getArtistMePageItemResize(context) / 9);
    }

    public static int getOneFourthOfScreenHeight(Context context) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        return (dm.heightPixels / 4);
    }


    public static int getOneEighthOfScreenHeight(Context context) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        return (dm.heightPixels / 8);
    }

    public static int get1by15OfScreenHeight(Context context) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        return (dm.heightPixels / 15);
    }
}
