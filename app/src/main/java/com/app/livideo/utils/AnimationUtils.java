package com.app.livideo.utils;

import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;

/**
 * Created by Eli on 4/9/2016.
 */
public class AnimationUtils {

    public static AlphaAnimation getMyAlphaAnimation(final int alphaStart, final int alphaEnd, final int duration, final Animation.AnimationListener animationListener) {
        AlphaAnimation playButtonAnimation = new AlphaAnimation(alphaStart, alphaEnd);
        playButtonAnimation.setInterpolator(new AccelerateInterpolator());
        playButtonAnimation.setDuration(duration);
        playButtonAnimation.setAnimationListener(animationListener);
        return playButtonAnimation;
    }
}
