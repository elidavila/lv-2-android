package com.app.livideo.utils;

import android.Manifest;
import android.view.MotionEvent;
import android.view.View;

import com.app.livideo.models.Artist;

import java.util.Comparator;

/**
 * Created by MWheeler on 9/29/2015.
 * <p/>
 * Used to store general constants for application
 */
@SuppressWarnings("HardCodedStringLiteral")
public interface AppConstants {

    //URL
    String FAQBroadcaster = "http://web.livideo.com/faq.php?app=true&broadcaster=true";
    String FAQViewer = "http://web.livideo.com/faq.php?app=true";
    String TermsBroadcaster = "http://web.livideo.com/terms-service.php?app=true&broadcaster=true";
    String TermsViewer = "http://web.livideo.com/terms-service.php?app=true";
    String Privacy = "http://web.livideo.com/privacy-policy.php?app=true";

    String USER = "User";
    String ARTIST = "Artist";

    // LIVIDEO USER ID
    String LIVIDEO_USER_ID_DEBUG = "1.de.5668d6b3f3171";
    String LIVIDEO_USER_ID_PROD = "1.de.5668d6b3f3171";
    String LIVIDEO_USER_ID_SOFT_LAUNCH = "2.nca.5604914771551";

    // LIL WAYNE USER ID
    String LIL_WAYNE_USER_ID_PROD = "1.nva.56d650423260c";
    String LIL_WAYNE_USER_ID_SOFT_LAUNCH = "2.nca.5605FD18C85F3";

    // LETTY MARTINEZ USER ID
    String LETTY_USER_ID_SOFT_LAUNCH = "2.nca.560c2b6e41089";
    String LETTY_USER_ID_PROD = "1.de.5671d47acba8b";

    // ANA MONTANA USER ID
    String ANA_MONTANA_USER_ID_PROD = "1.de.568dd601ef9aa";

    String ARTIST_KEY_INTENT = "Artist";
    String IS_USER_INTENT = "IS_USER_INTENT";
    String ACTIVITY_KEY_INTENT = "Activity";
    String COUNT_ANNOTATION = "%%COUNT%%";
    String IMAGE_PATH_KEY = "IMAGE_PATH";
    String IMAGE_ID_KEY = "IMAGE_ID";
    String ACTIVE_STREAM_KEY_INTENT = "Stream";
    String PREVIOUS_FILE_KEY = "previousFileName";
    String TAKEN_PHOTE = "taken_photo";
    String TAKEN_ORIENTATION = "orientation";
    String LAST_CAMERA_USED = "lastCameraUsed";
    int LANDSCAPE = 0;
    int PORTRAIT = 1;

    int REVERSE_LAND = 2;
    String SEND_ENTERED_CHAT_MESSAGE = "sendMessage";
    String SHOW_CONTACTS_DIALOGUE = "showContactsDialogue";
    String GO_TO_FRAGMENT = "MenuSelection";
    String UPDATE_BIG_DATA_ACTION = "updateBigData";
    String UPDATE_STREAM_COMMENTS_ACTION = "updateStreamComments";
    int SEARCH = -1;
    int ME = 1;
    int CHANNELS = 0;
    int TRANSITION_RESULT = 1010;

    String FROM_SPLASH = "fromSplash";
    String CONTACT_TYPE = "contactType";
    String CONTACT_PHONE = "phone";

    String CONTACT_EMAIL = "email";

    String SHOW_GO_LIVE_WARNING = "showGoLiveWarning";
    String KEY_NOTIFICATION_TITLE = "KEY_NOTIFICATION_TITLE";
    String KEY_NOTIFICATION_MSG = "KEY_NOTIFICATION_MESSAGE";
    String KEY_FROM_RE_CACHE = "KEY_FROM_RE_CACHE";

    // S3 Upload Thumbnails
    String DEBUG_IMG_UPLOAD = "livideodebug";
    String PRODUCTION_IMG_UPLOAD = "livideoprod";
    String SOFT_LAUNCH_IMG_UPLOAD = "livideosoftlaunch";

    // Boolean flags
    String KEY_HAS_SEEN_TUTORIAL = "livideo_tutorial";
    String KEY_HAS_UPLOADED_PROFILE_PIC = "KEY_HAS_UPLOADED_PROFILE_PIC";

    //Streaming Servers
    String SOFTLAUNCH_SERVER = "live_softlaunch/";
    String DEBUG_SERVER = "live_debug/";
    String PRODUCTION_SERVER = "live_prod/";


    //Image clicks
    int CROPPED_PICTURE = 2;
    int SELECT_PICTURE = 1;
    int TAKE_PICTURE = 3;

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    String TWITTER_KEY = "EWvPAm4LIo8CKbldO5B0t2kAX";
    String TWITTER_SECRET = "RKdy2QFBKz3ipzAzVXPGGX2e3JiOjMVWv9eFjP3r5c68gVN3pJ";

    // Updates on images that fail to load
    int UPDATE_IMAGE_FIRST_INTERVAL = 3000;
    int UPDATE_IMAGE_SECOND_INTERVAL = 5000;

    String STREAM_TITLE_LABEL = "STREAM_TITLE";
    String KEY_WEB_TYPE = "webType";
    int WEB_TYPE_TERMS = 0;
    int WEB_TYPE_PRIVACY = 1;
    int WEB_TYPE_FAQ = 2;

    int BROADCASTER_MAX_IMAGES = 25;
    int VIEWER_MAX_IMAGES = 1;

    String TEAL_COLOR = "#19A7A1";
    String GREY_COLOR = "#A7A7A7";
    String BLACK_COLOR = "#000000";
    String ARTIST_POSITION_KEY_INTENT = "ARTIST_POSITION_KEY_INTENT";

    String IS_TITLE_VIEW_KEY = "IS_TITLE_VIEW_KEY";
    // FROM ACTIVITY WORKFLOW
    String FROM_PAGE_KEY = "FROM_PAGE_KEY";
    int FROM_DISCOVER_PAGE = 1;
    int FROM_ME_PAGE = 2;
    int FROM_GUIDE_PAGE = 3;
    int FROM_CONNECT_PAGE = 4;
    int FROM_RECORD_PAGE = 5;
    int FROM_ARTIST_PAGE = 6;

    int FROM_SIGNUP = 7;
    int FROM_FORGOT_PW_ACTIVITY = 8;
    int FROM_NEW_DEVICE = 9;
    int ARTIST_FOLLOWER_COUNT = 50000;

    // Key Intents
    String VIEWER_OR_ARTIST_DIALOG_KEY = "VIEWER_OR_ARTIST_DIALOG_KEY";
    String KEY_IS_BROADCASTER = "KEY_IS_BROADCASTER";
    String DEVICE_ID_KEY_INTENT = "DEVICE_ID_KEY_INTENT";
    String PHONE_NUMBER_KEY_INTENT = "PHONE_NUMBER_KEY_INTENT";
    String USER_ID_KEY_INTENT = "USER_ID_KEY_INTENT";
    String EMAIL_KEY_INTENT = "EMAIL_KEY_INTENT";
    String DISPLAY_NAME_KEY_INTENT = "DISPLAY_NAME_KEY_INTENT";
    String PASSWORD_KEY_INTENT = "PASSWORD_KEY_INTENT";
    String GENDER_KEY_INTENT = "GENDER_KEY_INTENT";
    String BIRTHDAY_KEY_INTENT = "BIRTHDAY_KEY_INTENT";
    String SOCIAL_ID_KEY_INTENT = "SOCIAL_ID_KEY_INTENT";
    String SOCIAL_TYPE_KEY_INTENT = "SOCIAL_TYPE_KEY_INTENT";
    String IS_ALL_AGES_KEY = "IS_ALL_AGES_KEY";
    String ARTIST_CATEGORY_KEY_INTENT = "ARTIST_CATEGORY_KEY_INTENT";
    String ARTIST_MONEY_KEY_INTENT = "ARTIST_MONEY_KEY_INTENT";

    // Permissions
    String[] PERMISSIONS_STORAGE = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    String[] PERMISSIONS_CONTACTS = {Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CONTACTS};
    String[] PERMISSIONS_CAMERA = {Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA,
            Manifest.permission.RECORD_AUDIO, Manifest.permission.READ_PHONE_STATE};
    int REQUEST_STORAGE = 111;
    int REQUEST_CAMERA = 0;
    int REQUEST_QUE = 0;

    // Regular Code
    String SUCCESS = "0";

    // User Code
    String USERCODE_NOT_EXISTS = "1";
    String USERCODE_SUCCESS = "0";
    int USERCODE_BLOCKED = 10;
    String USERCODE_OTHER_ERROR = "20";
    String USERCODE_IS_ARTIST = "30";
    String USERCODE_DEVICE_IS_NOT_ALLOWED = "40";
    String USERCODE_WRONG_PASSWORD = "2";

    // Stream Codes
    int STREAMCODE_SUCCESS = 0;
    int STREAMCODE_NOT_EXISTS = 1;
    int STREAMCODE_NOT_SUBSCRIBED = 2;
    int STREAMCODE_IS_PRIVATE = 5;
    int STREAMCODE_OTHER_ERROR = 20;

    // Subscribing Codes
    String SUBSCRIBING_SUCCESS = "0";
    String SUBSCRIBING_ALREADY_SUBSCRIBED = "1";
    String SUBSCRIBING_TO_YOUNG = "2";
    String SUBSCRIBING_OTHER_ERROR = "20";

    int UPDATE_CHAT_INTERVAL = 1300;
    String PREVIEW_CHAT_LIMIT = "10";
    View.OnTouchListener touchListner = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            return true;
        }
    };
    int SUB_TYPE_HERO = 0, SUB_TYPE_FAV = 1, SUB_TYPE_HAS_MEMBERSHIP = 2, SUB_TYPE_NONE = 3;

    Comparator<? super Artist> ARTIST_NAME_COMPARATOR = new Comparator<Artist>() {
        @Override
        public int compare(Artist lhs, Artist rhs) {
            return lhs.getDisplayName().compareTo(rhs.getDisplayName());
        }
    };
    String PRIVATE_PICTURE_ID = "PRIVATE_PICTURE_ID";
    String TYPE_MESSAGE = "1";
    String TYPE_PICTURE = "2";

    int MAX_PHOTO_SIZE = 4096;
    String PRIVATE_USER_ID = "PRIVATE_USER_ID";
    String KEY_PICTURE_URL = "KEY_PICTURE_URL";
    int NO_ARTIST_WHISPER = 1;
    int NON_MEMBERSHIP_WHISPER = 2;
    String KEY_NOTIFICATION_TYPE = "KEY_NOTIFICATION_TYPE";
    String PARAM_INFO = "Info";
    String KEY_ARTIST_ID = "KEY_ARTIST_ID";
    String KEY_STREAM_ID = "KEY_STREAM_ID";
    String KEY_YOUTUBE_REFRESH_TOKEN = "KEY_YOUTUBE_REFRESH_TOKEN";
}
