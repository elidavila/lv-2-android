package com.app.livideo.utils;

import android.app.Activity;
import android.content.Context;
import android.widget.DatePicker;

import com.app.livideo.R;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by eli davila on 9/11/2015.
 */
public class TimeUtils {

    private static final String TIME_PLACEHOLDER = "%%TIME%%";

    public static String getDateFromDatePicker(DatePicker datePicker) {
        int day = datePicker.getDayOfMonth();
        int month = datePicker.getMonth();
        int year = datePicker.getYear();

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);

        Date _date = new Date();
        _date.setTime(calendar.getTime().getTime());

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = format.format(_date);

        return date;
    }

    public static String getRegularTime(String dateTime) {
        String time = "";
        String[] dateTimeList = dateTime.split(" ");
        if (dateTimeList[1] != null) {
            int hour;
            String AMPM;
            String[] timeList = dateTimeList[1].trim().split(":");

            try {
                hour = Integer.parseInt(timeList[0]);
            } catch (NumberFormatException e) {
                hour = 0;
            }

            //Convert military time to regular time
            if (hour >= 13) {
                hour -= 12;
                AMPM = "PM";
            } else if (hour == 12) {
                AMPM = "PM";
            } else {
                AMPM = "AM";
            }

            //Set the regular time string
            time = hour + ":00 " + AMPM;
        }
        return time;
    }


    public static String convertToLocalTime(final String fromTimeZoneString, final String fromDateTime) {
        int[] dateTimeList = getTimeList(fromDateTime);
        final DateTimeZone fromTimeZone = DateTimeZone.forID(fromTimeZoneString);
        final DateTime dateTime = new DateTime(dateTimeList[0], dateTimeList[1], dateTimeList[2], dateTimeList[3], dateTimeList[4], dateTimeList[5], fromTimeZone);

        DateTime localTime;
        int hourOffset = getCurrentTimeZoneOffset();
        if (hourOffset != 0)
            localTime = dateTime.plusHours(hourOffset);
        else
            localTime = dateTime;

        final DateTimeFormatter outputFormatter = DateTimeFormat.forPattern("hh:mm a").withZone(fromTimeZone);
        return outputFormatter.print(localTime);
    }

    /**
     * This function will take the time recived from our server and convert it to local time
     *
     * @param dateTime Pass in Date time received from our server
     * @return String array where position 0 is date in MM/DD/YYYY format and position 1 is Local time in 12H format
     */
    public static String[] getLocalDateTimePair(String dateTime) {
        DateTime originalDate = DateTime.parse(dateTime, DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").withZoneUTC());
        DateTime intermediateTime = new DateTime(originalDate, DateTimeZone.getDefault());
        String parser[] = intermediateTime.toString().split("T");
        String finalDelivery[] = new String[2];
        StringBuilder sb = new StringBuilder();
        String busted[] = parser[0].split("-");
        sb.append(busted[1]).append("/").append(busted[2]).append("/").append(busted[0]);
        finalDelivery[0] = sb.toString();
        String timeString[] = parser[1].split(":");
        sb = new StringBuilder();
        int hour = Integer.parseInt(timeString[0]);
        String suffix;
        if (hour >= 13) {
            hour -= 12;
            suffix = " pm";
        } else if (hour == 12) {
            suffix = " pm";
        } else if (hour == 0) {
            hour = 12;
            suffix = " am";
        } else {
            suffix = " am";
        }

        sb.append(hour).append(":").append(timeString[1]).append(suffix);
        finalDelivery[1] = sb.toString();

        return finalDelivery;
    }

    /**
     * This function will take two date time pairs from our server and compare the times
     *
     * @param first Pass in Date time received from our server
     * @param second Pass in Date time received from our server
     * @return True if time difference is not within the same minute
     */
    public static boolean compairDateTime(String first, String second) {
        DateTime firstDateTime = DateTime.parse(first, DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").withZoneUTC());
        DateTime secondDateTime = DateTime.parse(second, DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").withZoneUTC());
        String firstParser[] = firstDateTime.toString().split("T");
        String secondParser[] = secondDateTime.toString().split("T");
        String firstDate[] = firstParser[0].split("-");
        String secondDate[] = secondParser[0].split("-");
        String firstTime[] = firstParser[1].split(":");
        String secondTime[] = secondParser[1].split(":");
        if (firstDate[0].equalsIgnoreCase(secondDate[0])) {
            if (firstDate[1].equalsIgnoreCase(secondDate[1])) {
                if (firstDate[2].equalsIgnoreCase(secondDate[2])) {
                    if (Integer.parseInt(firstTime[0]) == Integer.parseInt(secondTime[0])){
                        return Math.abs(Integer.parseInt(firstTime[1]) - Integer.parseInt(secondTime[1])) >= 1;
                    } else {
                        return true;
                    }
                } else{
                    return true;
                }
            } else {
                return true;
            }
        } else {
            return true;
        }
    }


    public static int[] getTimeList(String time) {
        //2015-09-29 22:00:00
        int[] dateTimeList = new int[6];
        if (!TextUtils.isNullOrEmpty(time)) {
            String[] list = time.split(" ");
            String[] dateList = list[0].split("-");
            String[] timeList = list[1].split(":");

            for (int i = 0; i < dateList.length; i++)
                dateTimeList[i] = Integer.valueOf(dateList[i]);

            for (int i = 0; i < timeList.length; i++)
                dateTimeList[i + 3] = Integer.valueOf(timeList[i]);
        }
        return dateTimeList;
    }

    public static int getCurrentTimeZoneOffset() {
        DateTimeZone dateTimeZone = DateTimeZone.getDefault();
        return (int) TimeUnit.MILLISECONDS.toHours(dateTimeZone.getOffset(DateTime.now().getMillis()));
    }

    public static String convertTimeForStream(String dateTime, Context activity) {
        DateTime originalDate = DateTime.parse(dateTime, DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").withZoneUTC());
        DateTime intermediateTime = new DateTime(originalDate, DateTimeZone.getDefault());
        DateTime now = new DateTime();
        Period period = new Period(intermediateTime, now);

        // PAST
        if (period.getYears() == 0 && period.getMonths() == 0 && period.getWeeks() == 0 && period.getDays() == 0 && period.getHours() == 0 && period.getMinutes() == 0 && period.getSeconds() != 0 && period.getSeconds() <= 5) {
            return activity.getString(R.string.just_now);
        } else if (period.getYears() == 0 && period.getMonths() == 0 && period.getWeeks() == 0 && period.getDays() == 0 && period.getHours() == 0 && period.getMinutes() == 0 && period.getSeconds() != 0) {
            return activity.getString(R.string.seconds_ago).replace(TIME_PLACEHOLDER, "" + period.getSeconds());
        } else if (period.getYears() == 0 && period.getMonths() == 0 && period.getWeeks() == 0 && period.getDays() == 0 && period.getHours() == 0 && period.getMinutes() != 0 && period.getMinutes() < 2) {
            return activity.getString(R.string.minut_ago);
        } else if (period.getYears() == 0 && period.getMonths() == 0 && period.getWeeks() == 0 && period.getDays() == 0 && period.getHours() == 0 && period.getMinutes() != 0) {
            return activity.getString(R.string.past_minutes).replace(TIME_PLACEHOLDER, "" + period.getMinutes());
        } else if (period.getYears() == 0 && period.getMonths() == 0 && period.getWeeks() == 0 && period.getDays() == 0 && period.getHours() != 0 && period.getHours() < 2) {
            return activity.getString(R.string.an_hour_ago);
        } else if (period.getYears() == 0 && period.getMonths() == 0 && period.getWeeks() == 0 && period.getDays() == 0 && period.getHours() != 0) {
            return activity.getString(R.string.hours_ago).replace(TIME_PLACEHOLDER, "" + period.getHours());
        } else if (period.getYears() == 0 && period.getMonths() == 0 && period.getWeeks() == 0 && period.getDays() != 0 && period.getDays() < 2) {
            return activity.getString(R.string.yesterday);
        } else if (period.getYears() == 0 && period.getMonths() == 0 && period.getWeeks() == 0 && period.getDays() != 0) {
            return activity.getString(R.string.days_ago).replace(TIME_PLACEHOLDER, "" + period.getDays());
        } else if (period.getYears() == 0 && period.getMonths() == 0 && period.getWeeks() != 0 && period.getWeeks() < 2) {
            return activity.getString(R.string.last_week);
        } else if (period.getYears() == 0 && period.getMonths() == 0 && period.getWeeks() != 0) {
            return activity.getString(R.string.weeks_age).replace(TIME_PLACEHOLDER, "" + period.getWeeks());
        } else if (period.getYears() == 0 && period.getMonths() != 0 && period.getMonths() < 2) {
            return activity.getString(R.string.last_month);
        } else if (period.getYears() == 0 && period.getMonths() != 0) {
            return activity.getString(R.string.months_ago).replace(TIME_PLACEHOLDER, "" + period.getMonths());
        } else if (period.getYears() != 0 && period.getYears() < 2) {
            return activity.getString(R.string.last_year);
        } else if (period.getYears() != 0) {
            return activity.getString(R.string.years_ago).replace(TIME_PLACEHOLDER, "" + period.getYears());
        } else {
            return "";
        }
    }

}
