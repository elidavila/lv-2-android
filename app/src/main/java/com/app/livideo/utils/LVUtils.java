package com.app.livideo.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.app.livideo.activities.ArtistRecordActivity;
import com.app.livideo.activities.MainMenuActivity;
import com.app.livideo.activities.PlayerActivity;
import com.app.livideo.activities.SignInActivity;
import com.app.livideo.activities.SplashActivity;
import com.app.livideo.api.APICalls;
import com.app.livideo.dialogs.AreYou18Dialog;
import com.app.livideo.dialogs.ExclusiveWatchDialog;
import com.app.livideo.dialogs.GuestWatchDialog;
import com.app.livideo.dialogs.SubscribeSuccessDialog;
import com.app.livideo.interfaces.ArtistTypesHandler;
import com.app.livideo.interfaces.DataAvailabilityHandler;
import com.app.livideo.interfaces.SessionHandler;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.Artist;
import com.app.livideo.models.ArtistPicture;
import com.app.livideo.models.EnterStreamData;
import com.app.livideo.models.Stream;
import com.app.livideo.models.User;
import com.app.livideo.models.UserImage;
import com.app.livideo.models.validators.ArtistValidator;
import com.app.livideo.models.validators.StreamValidator;
import com.app.livideo.models.validators.StringListValidator;
import com.app.livideo.models.validators.UserValidator;
import com.app.livideo.notifications.GCMRegistrationIntentService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class LVUtils {

    private static final Comparator<? super Artist> ARTIST_NAME_COMPARATOR = new Comparator<Artist>() {
        @Override
        public int compare(Artist lhs, Artist rhs) {
            return lhs.getDisplayName().compareTo(rhs.getDisplayName());
        }
    };
    private static final Comparator<? super Stream> STREAM_HEADLINE_COMPARATOR = new Comparator<Stream>() {
        @Override
        public int compare(Stream lhs, Stream rhs) {
            return lhs.getHeadline().compareTo(rhs.getHeadline());
        }
    };

    public static void StartLiVideo(final User mUser, String phone, final boolean shouldSaveUser,
                                    final View touch_blocker, final Activity mActivity,
                                    final Context mContext, final LVSession mSession,
                                    final RequestQueue mRequestQueue, final SessionHandler handler) {
        if (shouldSaveUser)
            SaveUserData(mUser, phone, mContext, mSession);

        SessionUtils.isCaching = true;
        APICalls.SendUserLocation(mActivity, mContext, mSession, mRequestQueue);
        mActivity.startService(new Intent(mActivity, GCMRegistrationIntentService.class));

        if (touch_blocker != null)
            touch_blocker.setVisibility(View.VISIBLE);

        APICalls.requestArtistTypes(mActivity, mContext, mSession, mRequestQueue, new ArtistTypesHandler() {
            @Override
            public void RunCompleted(boolean isAvailable, List<String> artistTypes) {
                if (touch_blocker != null)
                    touch_blocker.setVisibility(View.GONE);

                if (handler != null) {
                    handler.RunCompleted(true, Preferences.getPreference(Constants.USER_TYPE_KEY, mContext, -1));
                } else {
                    Intent intent;
                    if (mUser.getUserRole() == User.UserRole.BROADCASTER
                            && Preferences.getPreference(Constants.CAMERA_TYPE_KEY, mContext, -1) == Constants.OPEN_CAMERA_FIRST) {
                        intent = new Intent(mActivity, ArtistRecordActivity.class);
                        intent.putExtra(AppConstants.FROM_SPLASH, true);
                    } else {
                        intent = new Intent(mContext, MainMenuActivity.class);
                    }
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    mActivity.startActivity(intent);
                    mActivity.finish();
                }
            }
        });
    }

    private static void SaveUserData(User user, String phone, Context mContext, LVSession mSession) {
        if (user.getUserRole() == User.UserRole.GUEST) {
            Preferences.setPreference(Constants.KEY_USER_ID, "temp_" + user.getUserID(), mContext);
            Preferences.setPreference(Constants.USER_TYPE_KEY, Constants.GUEST_USER, mContext);
            user.setCurrentRole(User.UserRole.GUEST);
        } else if (user.getUserRole() == User.UserRole.VIEWER) {
            Preferences.setPreference(Constants.KEY_USER_ID, user.getUserID(), mContext);
            Preferences.setPreference(Constants.USER_TYPE_KEY, Constants.VIEWER_USER, mContext);
            user.setCurrentRole(User.UserRole.VIEWER);
        } else {
            Preferences.setPreference(Constants.KEY_USER_ID, user.getUserID(), mContext);
            Preferences.setPreference(Constants.USER_TYPE_KEY, Constants.BROADCASTER_USER, mContext);
            user.setCurrentRole(User.UserRole.BROADCASTER);
        }

        if (!TextUtils.isNullOrEmpty(phone))
            Preferences.setPreference(Constants.KEY_PHONE, phone, mContext);
        Preferences.setPreference(Constants.KEY_DEVICE, Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.ANDROID_ID), mContext);
        user.setDeviceID(Preferences.getPreference(Constants.KEY_DEVICE, mContext, ""));
        mSession.setUser(user);
    }

    public static void reCacheUser(final View touch_blocker, final Context mContext, final Activity mActivity, final Runnable runnable) {
        SessionUtils sessionUtils = new SessionUtils(mActivity, mContext);
        if (touch_blocker != null) touch_blocker.setVisibility(View.VISIBLE);
        sessionUtils.cacheUserSession(new SessionHandler() {
            @Override
            public void RunCompleted(boolean successful, int userType) {
                if (touch_blocker != null) touch_blocker.setVisibility(View.GONE);
                runnable.run();
            }
        });
    }

    public static String getStreamThumbnailUrl(String streamID) {
        return UrlBuilder.create(SplashActivity.mThumbnailEndpoint)
                .withSubDirectories(new String[]{streamID})
                .withExtension("jpg")
                .build(UrlBuilder.Host.AWS);
    }

    public static boolean isSubscribedArtist(Artist mArtist, List<Artist> subscribedArtistList) {
        if (ArtistValidator.isValid(subscribedArtistList) && ArtistValidator.isValid(mArtist))
            for (int i = 0; i < subscribedArtistList.size(); i++)
                if (ArtistValidator.isValid(subscribedArtistList.get(i)) && subscribedArtistList.get(i).getArtistId().equals(mArtist.getArtistId()))
                    if (subscribedArtistList.get(i).getArtistId().equals(mArtist.getArtistId()))
                        return true;
        return false;
    }

    //Returns a map of streams based on time key value
    public static Map<String, List<Stream>> getMapStreamsByTime(List<String> timeList, List<Stream> guideStreamList) {
        Map<String, List<Stream>> mapStreams = new HashMap<>();
        for (int i = 0; i < timeList.size(); i++) {
            List<Stream> streamList = new ArrayList<>();
            for (int x = 0; x < guideStreamList.size(); x++) {
                if (timeList.get(i).equalsIgnoreCase(TimeUtils.getRegularTime(guideStreamList.get(x).getTime())))
                    streamList.add(guideStreamList.get(x));
            }
            mapStreams.put(timeList.get(i), streamList);
        }
        return mapStreams;
    }

    //Returns a list of artist that begins with the input
    public static List<Artist> getArtistsThatContainString(String input, List<Artist> artists) {
        List<Artist> categorizedArtists = new ArrayList<>();
        if (ArtistValidator.isValid(artists)) {
            for (int i = 0; i < artists.size(); i++)
                if (artists.get(i).getDisplayName().toUpperCase().contains(input.toUpperCase()))
                    categorizedArtists.add(artists.get(i));

            Collections.sort(artists, ARTIST_NAME_COMPARATOR);  //sort by name
        }
        return categorizedArtists;
    }

    //Returns a list of artist lists based on its categorie (type)
    public static List<List<Artist>> getListOfArtistsByCategorie(List<String> categorieList, List<Artist> channelArtistsList) {
        List<List<Artist>> doubleListOfArtists = new ArrayList<>();

        // Search through each artist category
        if (StringListValidator.isValid(categorieList)) {
            for (int i = 0; i < categorieList.size(); i++) {
                List<Artist> artistList = new ArrayList<>();
                if (ArtistValidator.isValid(channelArtistsList)) {
                    for (int x = 0; x < channelArtistsList.size(); x++) {
                        // Add all artist in category match
                        Artist artist = channelArtistsList.get(x);
                        String label = categorieList.get(i);
                        if (ArtistValidator.isValid(artist) && !TextUtils.isNullOrEmpty(label) && label.equalsIgnoreCase(artist.getArtistType())) {
                            artistList.add(channelArtistsList.get(x));
                        }
                    }

                    // Add categorized artist list to our double list
                    if (ArtistValidator.isValid(artistList))
                        doubleListOfArtists.add(artistList);
                }
            }
        }
        return doubleListOfArtists;
    }

    //Returns a list of artist lists based on its categorie (type)
    public static LinkedHashMap<String, List<Artist>> getMapArtistByCategorie(List<String> categorieList, List<Artist> channelArtistsList) {
        LinkedHashMap<String, List<Artist>> mapArtistByCategorie = new LinkedHashMap<>();

        // Search through each artist category
        if (StringListValidator.isValid(categorieList)) {
            for (int i = 0; i < categorieList.size(); i++) {
                List<Artist> artistList = new ArrayList<>();
                String label = "";
                if (ArtistValidator.isValid(channelArtistsList)) {
                    for (int x = 0; x < channelArtistsList.size(); x++) {
                        // Add all artist in category match
                        Artist artist = channelArtistsList.get(x);
                        label = categorieList.get(i);
                        if (ArtistValidator.isValid(artist) && !TextUtils.isNullOrEmpty(label) && label.equalsIgnoreCase(artist.getArtistType())) {
                            artistList.add(channelArtistsList.get(x));
                        }
                    }

                    // Add categorized artist list to our double list
                    if (ArtistValidator.isValid(artistList))
                        mapArtistByCategorie.put(label, artistList);
                }
            }
        }
        return mapArtistByCategorie;
    }

    public static Artist getArtistById(List<Artist> artistList, String userID) {
        if (ArtistValidator.isValid(artistList)) {
            for (int i = 0; i < artistList.size(); i++) {
                Artist artist = artistList.get(i);
                if (ArtistValidator.isValid(artist) && artist.getArtistId().equalsIgnoreCase(userID)) {
                    return artistList.get(i);
                }
            }
        }
        return null;
    }

    public static Stream getActiveStreamById(List<Stream> streams, String streamId) {
        for (int i = 0; i < streams.size(); i++) {
            if (streams.get(i).getStreamId().equalsIgnoreCase(streamId))
                return streams.get(i);
        }
        return null;
    }

    public static UserImage getRandomUserImage(User user) {
        if (UserValidator.isValid(user)) {
            List<UserImage> userImages = user.getPictures();
            return userImages.get(getRandomArtistPhotoIndex(userImages.size()));
        }
        return new UserImage();
    }

    public static ArtistPicture getRandomArtistImage(Artist artist) {
        if (ArtistValidator.isValid(artist)) {
            List<ArtistPicture> userImages = artist.getArtistPictures();
            return userImages.get(getRandomArtistPhotoIndex(userImages.size()));
        }
        return new ArtistPicture();
    }

    public static ArtistPicture getRandomArtistImage(Stream stream, List<Artist> artistChannels) {
        if (StreamValidator.isValid(stream) && ArtistValidator.isValid(artistChannels))
            for (int i = 0; i < artistChannels.size(); i++)
                if (stream.getArtistId().equalsIgnoreCase(artistChannels.get(i).getArtistId()))
                    return artistChannels.get(i).getArtistPictures().get(getRandomArtistPhotoIndex(artistChannels.get(i).getArtistPictures().size()));
        return new ArtistPicture();
    }

    private static int getRandomArtistPhotoIndex(int size) {
        Random rand = new Random();
        return rand.nextInt((size));
    }

    public static boolean isEqualLists(List<?> l1, List<?> l2) {
        // make a copy of the list so the original list is not changed, and remove() is supported
        ArrayList<?> cp = new ArrayList<>(l1);
        for (Object o : l2) {
            if (!cp.remove(o)) {
                return false;
            }
        }
        return cp.isEmpty();
    }

    public static Artist getArtistUser(User user) {
        Artist artist = new Artist();
        if (UserValidator.isValid(user)) {
            artist.setBirthdate(user.getBirthdate());
            artist.setArtistId(user.getUserID());
            artist.setDisplayName(user.getDisplayName());
            artist.setSocialID(user.getSocialID());
            artist.setSocialType(user.getSocialType());
            artist.setTimeZone(user.getTimeZone());
            artist.setArtistType(user.getArtistType());
            artist.setIsAllAges(user.getIsAllAges());
        }
        artist.setEnteredChat(false);
        return artist;
    }

    public static Stream getNewActiveStream(String streamId, String headline) {
        Stream stream = new Stream();
        stream.setStreamId(streamId);
        stream.setHeadline(headline);
        return stream;
    }

    public static List<Stream> getStreamsThatContainString(String input, List<Stream> streams) {
        List<Stream> categorizedStreams = new ArrayList<>();
        if (StreamValidator.isValid(streams)) {
            for (int i = 0; i < streams.size(); i++)
                if (streams.get(i).getHeadline().toUpperCase().contains(input.toUpperCase()))
                    categorizedStreams.add(streams.get(i));
            Collections.sort(streams, STREAM_HEADLINE_COMPARATOR);  //sort by name
        }
        return categorizedStreams;
    }

    public static void beginUserSubscriptionProcess(final Artist mArtist, final Activity mActivity,
                                                    final LVSession mSession,
                                                    final RequestQueue mRequestQueue,
                                                    final DialogInterface.OnDismissListener mDismissHandler,
                                                    final Runnable mRunnable) {
        if (mSession.getUser().getUserRole() == User.UserRole.GUEST) {
            displayGuestWatchDialog(mActivity, mDismissHandler);
        } else if (mSession.getUser().getUserRole() == User.UserRole.VIEWER) {
            beginSubscriptionProcess(mArtist, mActivity, mDismissHandler, new Runnable() {
                @Override
                public void run() {
                    displayExclusiveWatchDialog(mArtist, mActivity, mDismissHandler, mRunnable);
                }
            });
        } else if (mSession.getUser().getUserRole() == User.UserRole.BROADCASTER) {
            beginSubscriptionProcess(mArtist, mActivity, mDismissHandler, new Runnable() {
                @Override
                public void run() {
                    subscribeArtistToArtist(mArtist, mActivity, mActivity.getApplicationContext(), mSession, mRequestQueue, mRunnable);
                }
            });
        }
    }

    public static void beginSubscriptionProcess(final Artist mArtist,
                                                final Activity mActivity,
                                                final DialogInterface.OnDismissListener mDismissHandler,
                                                final Runnable mRunnable) {
        if (Integer.valueOf(mArtist.getIsAllAges()) == 1) {
            mRunnable.run();
        } else {
            displayAreYou18Dialog(mActivity, mDismissHandler, mRunnable);
        }
    }

    public static void subscribeArtistToArtist(final Artist mArtist,
                                               final Activity mActivity,
                                               final Context mContext,
                                               final LVSession mSession,
                                               final RequestQueue mRequestQueue,
                                               final Runnable mRunnable) {
        APICalls.requestFreeArtistSubscription(mActivity, mContext, mSession, mArtist.getArtistId(), mRequestQueue, new DataAvailabilityHandler() {
            @Override
            public void RunCompleted(boolean isAvailable) {
                if (isAvailable) {
                    displaySubscribeSuccessDialog(mActivity, new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            if (mRunnable != null)
                                mRunnable.run();
                        }
                    });
                }
            }
        });
    }

    public static void displaySubscribeSuccessDialog(final Activity mActivity, final DialogInterface.OnDismissListener dismissListener) {
        SubscribeSuccessDialog dialog = new SubscribeSuccessDialog(mActivity);
        dialog.show();
        dialog.setOnDismissListener(dismissListener);
    }

    public static void displayExclusiveWatchDialog(final Artist mArtist,
                                                   final Activity mActivity,
                                                   final DialogInterface.OnDismissListener mDismissHandler,
                                                   final Runnable mRunnable) {
        ExclusiveWatchDialog dialog = new ExclusiveWatchDialog(mActivity, mArtist, mRunnable);
        dialog.show();
        if (mDismissHandler != null)
            dialog.setOnDismissListener(mDismissHandler);
    }

    public static void displayGuestWatchDialog(final Activity mActivity,
                                               final DialogInterface.OnDismissListener mDismissHandler) {
        GuestWatchDialog dialog = new GuestWatchDialog(mActivity);
        dialog.show();
        if (mDismissHandler != null)
            dialog.setOnDismissListener(mDismissHandler);
    }

    public static void displayAreYou18Dialog(final Activity mActivity,
                                             final DialogInterface.OnDismissListener mDismissHandler,
                                             final Runnable mRunnable) {
        AreYou18Dialog dialog = new AreYou18Dialog(mActivity, mRunnable);
        dialog.show();
        if (mDismissHandler != null)
            dialog.setOnDismissListener(mDismissHandler);
    }

    public static void handleError(String errorString, final boolean shouldFinish, final View touch_blocker, final Context mContext, final Activity mActivity) {
        if (mContext == null)
            return;

        if (touch_blocker != null)
            touch_blocker.setVisibility(View.GONE);

        if (errorString != null) {
            Toast.makeText(mContext, errorString, Toast.LENGTH_SHORT).show();
            if (shouldFinish && mActivity != null)
                mActivity.finish();
        } else {
            AndroidUtils.restartApp(mContext);
        }
    }

    public static void startVideoPlayer(Stream stream, EnterStreamData enterStreamData, View touch_blocker,
                                        final boolean isActive, final Activity mActivity) {
        touch_blocker.setVisibility(View.GONE);
        if (isActive) {
            stream.setUrl(enterStreamData.getUrl());
            stream.setWatchingId(enterStreamData.getWatchingId());
            stream.setDidStar(enterStreamData.isDidStar());
            Intent intent = new Intent(mActivity, PlayerActivity.class);
            intent.putExtra(AppConstants.ACTIVE_STREAM_KEY_INTENT, stream);
            mActivity.startActivity(intent);
        }
    }

    public static void setServerUrl() {
        switch (UrlBuilder.DEFAULT_HOST) {
            case SOFT_LAUNCH_SERVER:
                SplashActivity.LIVIDEO_USER_ID = AppConstants.LIVIDEO_USER_ID_SOFT_LAUNCH;
                SplashActivity.LIL_WAYNE_USER_ID = AppConstants.LIL_WAYNE_USER_ID_SOFT_LAUNCH;
                SplashActivity.LETTY_USER_ID = AppConstants.LETTY_USER_ID_SOFT_LAUNCH;
                SplashActivity.mServerUrl = UrlBuilder.create(UrlBuilder.Endpoint.API).build(UrlBuilder.Host.SOFT_LAUNCH_SERVER);
                SplashActivity.mThumbnailEndpoint = UrlBuilder.Endpoint.SOFT_LAUNCH_THUMBNAILS;
                SplashActivity.mImgUploadEndpoint = UrlBuilder.Endpoint.SOFT_LAUNCH_IMG_UPLOAD;
                SplashActivity.mImgUploadString = AppConstants.SOFT_LAUNCH_IMG_UPLOAD;
                SplashActivity.mStreamingServer = AppConstants.SOFTLAUNCH_SERVER;
                break;
            case PRODUCTION_SERVER:
                SplashActivity.LIVIDEO_USER_ID = AppConstants.LIVIDEO_USER_ID_PROD;
                SplashActivity.LIL_WAYNE_USER_ID = AppConstants.LIL_WAYNE_USER_ID_PROD;
                SplashActivity.LETTY_USER_ID = AppConstants.LETTY_USER_ID_PROD;
                SplashActivity.ANA_MONTANA_USER_ID = AppConstants.ANA_MONTANA_USER_ID_PROD;
                SplashActivity.mServerUrl = UrlBuilder.create(UrlBuilder.Endpoint.API).build(UrlBuilder.Host.PRODUCTION_SERVER);
                SplashActivity.mThumbnailEndpoint = UrlBuilder.Endpoint.PRODUCTION_THUMBNAILS;
                SplashActivity.mImgUploadEndpoint = UrlBuilder.Endpoint.PRODUCTION_IMG_UPLOAD;
                SplashActivity.mImgUploadString = AppConstants.PRODUCTION_IMG_UPLOAD;
                SplashActivity.mStreamingServer = AppConstants.PRODUCTION_SERVER;
                break;
            case DEBUG_SERVER:
                SplashActivity.LIVIDEO_USER_ID = AppConstants.LIVIDEO_USER_ID_DEBUG;
                SplashActivity.mServerUrl = UrlBuilder.create(UrlBuilder.Endpoint.API).build(UrlBuilder.Host.DEBUG_SERVER);
                SplashActivity.mThumbnailEndpoint = UrlBuilder.Endpoint.DEBUG_THUMBNAILS;
                SplashActivity.mImgUploadEndpoint = UrlBuilder.Endpoint.DEBUG_IMG_UPLOAD;
                SplashActivity.mImgUploadString = AppConstants.DEBUG_IMG_UPLOAD;
                SplashActivity.mStreamingServer = AppConstants.DEBUG_SERVER;
                break;
        }
    }

    public static void logoutUser(final Activity mActivity, final Context mContext,
                                  final LVSession mSession, final RequestQueue mRequestQueue) {
        boolean isBroadcaster = Preferences.getPreference(AppConstants.KEY_IS_BROADCASTER, mContext, false);
        boolean hasUploadedProfilePic = Preferences.getPreference(AppConstants.KEY_HAS_UPLOADED_PROFILE_PIC, mContext, true);

        APICalls.requestLogout(mSession, mActivity, mContext, mSession.getUser().getUserID(), mSession.getUser().getDeviceID(), mRequestQueue, new Runnable() {
            @Override
            public void run() {
                //Clear session
                SessionUtils.isCaching = false;
                mSession.clearSession();
            }
        });

        //Clear memory
        try {
            DeleteCacheUtils.cleanDirectory(mActivity.getCacheDir());
        } catch (IOException e) {
            Log.e("Error", "Clearing cache directory.");
        }

        // Save 1 time views only
        PreferenceManager.getDefaultSharedPreferences(mActivity).edit().clear().commit();
        Preferences.setPreference(AppConstants.VIEWER_OR_ARTIST_DIALOG_KEY, false, mContext);
        Preferences.setPreference(AppConstants.KEY_IS_BROADCASTER, isBroadcaster, mContext);
        Preferences.setPreference(AppConstants.KEY_HAS_UPLOADED_PROFILE_PIC, hasUploadedProfilePic, mContext);

        //Go to login Activity
        Intent intent = new Intent(mActivity, SignInActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        mActivity.startActivity(intent);
        mActivity.finish();
    }
}
