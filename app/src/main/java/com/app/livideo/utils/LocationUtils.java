package com.app.livideo.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.provider.Settings;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.model.LatLng;
import com.app.livideo.R;
import com.app.livideo.services.IntentServiceGetLocation;

/**
 * Created by Christian on 23.10.15.
 */
public class LocationUtils implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private Context activityContext;
    private static GoogleApiClient mGoogleApiClient;
    protected OnLocationListener onLocationListener;
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;

    //last known location
    Location location;

    /**
     * Create a new Object of LocationUtils with an Activity Context and an OnLocationListener and get location,
     * geo-coded-address and LatLng-Object.
     * <p/>
     *
     * @param activityContext Context of an Activity (to start an intentService for geo-coding
     * @param listener        get the result data
     *                        <p/>
     *                        Listener has two callbacks:
     *                        onGetLocation (Location location, Address address, LatLng latitudeLongitude)
     *                        onErrorWhileGettingLocation (String message)
     */
    public LocationUtils(Context activityContext, OnLocationListener listener) {
        this.activityContext = activityContext;
        this.onLocationListener = listener;
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                connectToGooglePlayServices();
            }
        });
    }

    public interface OnLocationListener {
        void onGetLocation(Location location, Address address, LatLng latitudeLongitude);

        void onErrorWhileGettingLocation(String message);
    }

    protected synchronized void connectToGooglePlayServices() {
        mGoogleApiClient = new GoogleApiClient.Builder(this.activityContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location != null) {
            if (!Geocoder.isPresent()) {
                String error = "onConnected - no Geocoder available!";
                Log.e(this.getClass().getName(), error);
                onLocationListener.onErrorWhileGettingLocation(error);
                return;
            }
            //start intent service
            Intent intent = new Intent(this.activityContext, IntentServiceGetLocation.class);
            intent.putExtra(IntentServiceGetLocation.EXTRA_RECEIVER, new AddressResultReceiver(new Handler()));
            intent.putExtra(IntentServiceGetLocation.EXTRA_LOCATION, location);
            this.activityContext.startService(intent);
        } else {
            String error = "onConnected - no last location";
            Log.e(this.getClass().getName(), error);
            onLocationListener.onErrorWhileGettingLocation(error);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        String error = "Google Play Services onConnectionSuspended!";
        Log.e(this.getClass().getName(), error);
        onLocationListener.onErrorWhileGettingLocation(error);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        String error = "ArtistVideoShareObj Connection failed!";
        Log.e(this.getClass().getName(), error);
        onLocationListener.onErrorWhileGettingLocation(error);
    }

    @SuppressLint("ParcelCreator")
    private class AddressResultReceiver extends ResultReceiver {

        public AddressResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            Address address;
            if (resultCode == IntentServiceGetLocation.RESULT_SUCCESS) {
                address = resultData.getParcelable(IntentServiceGetLocation.RESULT_ADDRESS);
                onLocationListener.onGetLocation(location, address, new LatLng(location.getLatitude(), location.getLongitude()));
                return; //no error
            }
            onLocationListener.onErrorWhileGettingLocation("No Address found!");
        }
    }

    public interface OnLocationServicesDialogDismissedListener {
        void onLocationServicesDialogDismissedListener();
    }

    public static void checkIfLocationIsAvailable(final Context context, final OnLocationServicesDialogDismissedListener listener) {
        if (!locationServicesAvailable(context)) {
            new AlertDialog.Builder(context)
                    .setMessage(R.string.alertdialog_no_location_services_message)
                    .setPositiveButton(R.string.alertdialog_no_location_servides_go_to_settings, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            context.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        }
                    })
                    .setNegativeButton(R.string.dialog_string_close, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            listener.onLocationServicesDialogDismissedListener();
                        }
                    })
                    .setCancelable(false)
                    .show();
        }
    }

    public static boolean locationServicesAvailable(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    public static void promptEnableLocation(final Activity mActivity) {
        if (mGoogleApiClient != null) {
            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);

            builder.setAlwaysShow(true); //this is the key ingredient

            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            // All location settings are satisfied. The client can initialize location requests here.
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the user a dialog.
                            try {
                                // Show the dialog by calling startResolutionForResult(), and check the result in onActivityResult().
                                status.startResolutionForResult(mActivity, REQUEST_CHECK_SETTINGS);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have no way to fix the settings so we won't show the dialog.
                            break;
                    }
                }
            });
        }
    }
}