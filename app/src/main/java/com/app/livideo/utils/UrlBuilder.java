package com.app.livideo.utils;

import java.util.Collections;
import java.util.Map;

public class UrlBuilder {

    public static final Host DEFAULT_HOST = Host.DEBUG_SERVER;
    private static final Scheme DEFAULT_SCHEME = Scheme.HTTPS;
    private String[] subDirectories = null;
    private Map<String, String> mParams = Collections.emptyMap();
    private Scheme mScheme = DEFAULT_SCHEME;
    private Endpoint mEndpoint;
    private String mExtension;

    public static UrlBuilder create(Endpoint endpoint) {
        UrlBuilder builder = new UrlBuilder();
        builder.mEndpoint = endpoint;
        return builder;
    }

    public UrlBuilder withSubDirectories(String[] directories) {
        subDirectories = directories;
        return this;
    }

    public UrlBuilder withExtension(String extension) {
        mExtension = extension;
        return this;
    }

    public UrlBuilder withParams(Map<String, String> params) {
        mParams = params;
        return this;
    }

    public String build(Host host) {
        StringBuilder sb = new StringBuilder();
        sb.append(mScheme.getScheme());
        sb.append(host.getHost());
        sb.append(mEndpoint.getPath());

        if (subDirectories != null && subDirectories.length > 0) {
            for (String subDirectory : subDirectories) {
                sb.append(subDirectory);
                sb.append("/");
            }
            sb.setLength(sb.length() - 1);
        }

        if (mParams.size() > 0) {
            sb.append("?");
            for (String key : mParams.keySet()) {
                sb.append(key);
                sb.append("=");
                sb.append(mParams.get(key));
                sb.append("&");
            }
            sb.setLength(sb.length() - 1);
        }

        if (!TextUtils.isNullOrEmpty(mExtension)) {
            sb.append(".");
            sb.append(mExtension);
        }

        return sb.toString();
    }

    @SuppressWarnings("HardCodedStringLiteral")
    public enum Scheme {
        HTTP("http://"),
        HTTPS("https://");

        private final String mScheme;

        Scheme(String scheme) {
            mScheme = scheme;
        }

        public String getScheme() {
            return mScheme;
        }
    }

    @SuppressWarnings("HardCodedStringLiteral")
    public enum Host {
        DEBUG_SERVER("api.wereliveinfive.com/debug/"),
        PRODUCTION_SERVER("api.wereliveinfive.com/prod/"),
        SOFT_LAUNCH_SERVER("api.wereliveinfive.com/"),
        AWS("s3.amazonaws.com/");

        private final String mHost;

        Host(String host) {
            mHost = host;
        }

        public String getHost() {
            return mHost;
        }
    }

    @SuppressWarnings("HardCodedStringLiteral")
    public enum Endpoint {
        API("API.php"),

        // Our Server Thumbnails
        DEBUG_THUMBNAILS("livideodebug/thumbnails/"),
        PRODUCTION_THUMBNAILS("livideoprod/thumbnails/"),
        SOFT_LAUNCH_THUMBNAILS("livideo/thumbnails/"),

        // S3 Upload Thumbnails
        DEBUG_IMG_UPLOAD("livideodebug/"),
        PRODUCTION_IMG_UPLOAD("livideoprod/"),
        SOFT_LAUNCH_IMG_UPLOAD("livideosoftlaunch/");

        private final String mPath;

        Endpoint(String path) {
            mPath = path;
        }

        public String getPath() {
            return mPath;
        }
    }

    public static String buildUrlParams(Map<String, String> params) {
        String parameters = "";
        for (String key : params.keySet()) {
            String value = params.get(key);
            parameters += key + "=" + value + "&";
        }
        parameters = parameters.substring(0, parameters.length() - 1);
        return parameters;
    }
}
