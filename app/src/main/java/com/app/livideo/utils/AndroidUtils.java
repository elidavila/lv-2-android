package com.app.livideo.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.app.livideo.R;
import com.app.livideo.dialogs.CustomAlertDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

/**
 * Created by eli davila on 8/25/2015.
 */
public class AndroidUtils {

    public static void notifyUserWithRingTone(Context mContext) {
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(mContext, notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void requestCameraPermission(final Activity mActivity, final View mRootLayout) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(mActivity, Manifest.permission.CAMERA)
                || ActivityCompat.shouldShowRequestPermissionRationale(mActivity, Manifest.permission.RECORD_AUDIO)
                || ActivityCompat.shouldShowRequestPermissionRationale(mActivity, Manifest.permission.READ_EXTERNAL_STORAGE)
                || ActivityCompat.shouldShowRequestPermissionRationale(mActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                || ActivityCompat.shouldShowRequestPermissionRationale(mActivity, Manifest.permission.READ_PHONE_STATE)) {
            Snackbar.make(mRootLayout, R.string.permission_dual_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok_caps_string, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ActivityCompat.requestPermissions(mActivity,
                                    AppConstants.PERMISSIONS_CAMERA,
                                    AppConstants.REQUEST_CAMERA);
                        }
                    })
                    .show();
        } else {
            ActivityCompat.requestPermissions(mActivity, AppConstants.PERMISSIONS_CAMERA, AppConstants.REQUEST_CAMERA);
        }
    }

    public static void requestContactsPermission(final Activity mActivity, final View mRootLayout) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(mActivity, Manifest.permission.WRITE_CONTACTS)
                || ActivityCompat.shouldShowRequestPermissionRationale(mActivity, Manifest.permission.READ_CONTACTS)) {
            Snackbar.make(mRootLayout, R.string.contacts_access,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok_caps_string, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ActivityCompat.requestPermissions(mActivity,
                                    AppConstants.PERMISSIONS_CONTACTS,
                                    AppConstants.REQUEST_QUE);
                        }
                    })
                    .show();
        } else {
            ActivityCompat.requestPermissions(mActivity, AppConstants.PERMISSIONS_CONTACTS, AppConstants.REQUEST_QUE);
        }
    }

    public static void requestStoragePermission(final Activity mActivity, final View mRootLayout) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(mActivity, Manifest.permission.READ_EXTERNAL_STORAGE)
                || ActivityCompat.shouldShowRequestPermissionRationale(mActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Snackbar.make(mRootLayout, R.string.permission_contacts_rationale, Snackbar.LENGTH_INDEFINITE).setAction(R.string.ok_caps_string, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ActivityCompat.requestPermissions(mActivity, AppConstants.PERMISSIONS_STORAGE, AppConstants.REQUEST_STORAGE);
                }
            }).show();
        } else {
            ActivityCompat.requestPermissions(mActivity, AppConstants.PERMISSIONS_STORAGE, AppConstants.REQUEST_STORAGE);
        }
    }

    public static boolean checkPlayServices(Context mContext, final Activity activity) {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(mContext);
        if (resultCode != ConnectionResult.SUCCESS) {
            CustomAlertDialog dialog = new CustomAlertDialog(activity, mContext.getString(R.string.device_does_not_support_google_play_services), true);
            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    Uri playServices = Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.gms&hl=en");
                    Intent i = new Intent(Intent.ACTION_VIEW, playServices);
                    activity.startActivity(i);
                    System.exit(0);
                }
            });
            dialog.show();

            return false;
        }
        return true;
    }

    //Creates a long toast for us with the appropriate message
    public static void showLongToast(String message, Activity activity) {
        if (activity != null)
            Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
    }

    //Creates a short toast for us with the appropriate message
    public static void showShortToast(String message, Activity activity) {
        if (activity != null)
            Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
    }

    //Checks if the device has network access
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        } else {
            return false;
        }
    }

    public static void restartApp(Context context) {
        if (context != null) {
            Intent i = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            context.startActivity(i);
        }
    }

    public static void hideKeyboard(Context context, View view) {
        if (context != null && view != null) {
            InputMethodManager in = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            in.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public static void showKeyboard(Context context, View view) {
        InputMethodManager in = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        in.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }

    /**
     * <p>
     * create a new KeyboardHelper-Object with the contentView(fragment compatible), the global height (Activity->WindowManager->defaultScreen->Height), and a callback to register
     * and you are notified whenever the keyboard is opened or closed like this:
     * </p>
     * <pre>
     *     final int globalHeight = getActivity().getWindow().getWindowManager().getDefaultDisplay().getHeight();
     *     AndroidUtils.KeyboardHelper keyboardHelper = new AndroidUtils.KeyboardHelper(contentView, globalHeight, this);
     *         -> then implement the interface in the activity/fragment
     * </pre>
     * <p>
     * -> works only when the activity is resized when the softkeyboard shows up!
     * </p>
     */
    public static class KeyboardHelper {

        private View contentView;
        private int globalHeight;
        private boolean keyboardOpen;
        private KeyboardListener keyboardListener;
        public KeyboardHelper(View contentView, int globalHeight, KeyboardListener keyboardListener) {
            this.contentView = contentView;
            this.globalHeight = globalHeight;
            this.keyboardOpen = false;
            this.keyboardListener = keyboardListener;
            registerViewTreeObserver();
        }

        private void registerViewTreeObserver() {
            contentView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    int currentHeight = contentView.getHeight();

                    if ((float) globalHeight / currentHeight > 1.25) {
                        if (!keyboardOpen) {
                            //keyboard opened
                            keyboardListener.OnKeyboardOpened();
                            keyboardOpen = true;
                        }
                    } else {
                        if (keyboardOpen) {
                            //keyboard closed
                            keyboardListener.OnKeyboardClosed();
                            keyboardOpen = false;
                        }
                    }
                }
            });
        }

        public interface KeyboardListener {
            void OnKeyboardOpened();

            void OnKeyboardClosed();
        }
    }
}
