package com.app.livideo.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.location.Address;
import android.location.Location;
import android.util.Log;

import com.app.livideo.R;
import com.google.android.gms.maps.model.LatLng;

import java.io.File;

/**
 * Created by MWheeler on 12/10/2015.
 */
public class ArtistVideoShareObj implements LocationUtils.OnLocationListener {

    private String streamTitle;
    private String locationString;
    private Address address;
    private boolean shareLocation;
    //    private boolean shareFacebook;
    private boolean isPublic;
    private boolean shareTwitter;
    private File videoFile;
    private Activity activityContext;
    private OnSetupFinishedListener setupFinishedListener;

    public ArtistVideoShareObj(String streamTitle, File videoFile,
                               boolean shareLocation,/* boolean shareFacebook,*/ boolean shareTwitter, boolean isPublic,
                               Activity activityContext, OnSetupFinishedListener listener) {
        this.streamTitle = streamTitle;
        this.videoFile = videoFile;
        this.shareLocation = shareLocation;
//        this.shareFacebook = shareFacebook;
        this.isPublic = isPublic;
        this.shareTwitter = shareTwitter;
        this.activityContext = activityContext;
        this.setupFinishedListener = listener;

        setLocationString();
    }

    private void setLocationString() {
        if (!shareLocation) {
            locationString = "";
            this.setupFinishedListener.onSetupFinished(this);
        } else {
            LocationUtils locationUtils = new LocationUtils(activityContext, this);
            Log.d(this.getClass().getName(), "create new LocationUtilsObject");
        }
    }

    @Override
    public void onGetLocation(Location location, Address address, LatLng latitudeLongitude) {
        setAddress(address);
    }

    @Override
    public void onErrorWhileGettingLocation(String message) {
        Log.e(this.getClass().getName(), "Unable to share Location. Obj-Setup finished without sharing Location.\n" + message);
        setAddress(null);
    }

    public void setAddress(Address address) {
        if (address != null) {
            this.address = address;
            this.locationString = " near " + address.getSubLocality();
        }
        this.setupFinishedListener.onSetupFinished(this);
    }

//    private void shareToFacebook() {
//        try {
//            //request additional permissions
//            LoginManager.getInstance().registerCallback(CallbackManager.Factory.create(), new FacebookCallback<LoginResult>() {
//                @Override
//                public void onSuccess(LoginResult loginResult) {
//                    shareToFacebookAfterLogin();
//                }
//
//                @Override
//                public void onCancel() {
//                    Log.e(this.getClass().getName(), "FB Login failed - onCancel");
//                }
//
//                @Override
//                public void onError(FacebookException e) {
//                    Log.e(this.getClass().getName(), "FB Login failed - onError");
//                }
//            });
//
//            if (AccessToken.getCurrentAccessToken() == null || !AccessToken.getCurrentAccessToken().getPermissions().contains(Arrays.asList("publish_actions")))
//                LoginManager.getInstance().logInWithPublishPermissions((AppCompatActivity) activityContext, Arrays.asList("publish_actions"));
//            else
//                shareToFacebookAfterLogin();
//        } catch (Exception e) {
//            Log.e(this.getClass().getName(), "Facebook Share Error! " + e.getMessage());
//            e.printStackTrace();
//            showErrorDialog("Facebook");
//        }
//    }
//
//    private void shareToFacebookAfterLogin() {
//        ShareLinkContent shareLinkContent = new ShareLinkContent.Builder()
//                .setContentTitle(streamTitle + locationString)
//                .setContentDescription("I've just postet a new video on LiVideo!")
//                .build();
//        ShareApi.share(shareLinkContent, new FacebookCallback<Sharer.Result>() {
//            @Override
//            public void onSuccess(Sharer.Result result) {
//                Log.d(this.getClass().getName(), "shareToFacebook: success\n" + result.toString());
//            }
//
//            @Override
//            public void onCancel() {
//                Log.d(this.getClass().getName(), "shareToFacebook: canceled");
//                showErrorDialog("Facebook");
//            }
//
//            @Override
//            public void onError(FacebookException e) {
//                Log.d(this.getClass().getName(), "shareToFacebook: error\n" + e.getMessage());
//                showErrorDialog("Facebook");
//            }
//        });
//    }

    private void showErrorDialog() {
        new AlertDialog.Builder(activityContext)
                .setMessage(activityContext.getResources().getString(R.string.dialog_error_share))
                .setCancelable(false)
                .setNeutralButton(R.string.dialog_string_close, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////// Getter Methods ////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////

    public String getStreamTitle() {
        return streamTitle + locationString;
    }

    public String getLocationString() {
        return locationString;
    }

    public Address getAddressObject() {
        return address;
    }

    public File getVideoFile() {
        return videoFile;
    }

    public void setVideoFile(File file) {
        videoFile = file;
    }

    public String getVideoFilePath() {
        return videoFile.toString();
    }

    public interface OnSetupFinishedListener {
        void onSetupFinished(ArtistVideoShareObj shareObj);
    }

    public boolean getIsPublic() {
        return isPublic;
    }
}