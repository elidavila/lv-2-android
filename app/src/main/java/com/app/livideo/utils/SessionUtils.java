package com.app.livideo.utils;

import android.app.Activity;
import android.content.Context;
import android.provider.Settings;

import com.android.volley.RequestQueue;
import com.app.livideo.R;
import com.app.livideo.api.APICalls;
import com.app.livideo.interfaces.LoginHandler;
import com.app.livideo.interfaces.SessionHandler;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.User;

import javax.inject.Inject;

/**
 * Created by eli on 9/3/2015.
 */
public class SessionUtils {

    @Inject RequestQueue mRequestQueue;
    @Inject LVSession mSession;
    private Context mContext;
    private String deviceID, userID;
    public static boolean isCaching;
    SessionHandler mHandler;
    Activity mActivity;

    public SessionUtils(Activity activity, Context context) {
        LVInject.inject(this);
        this.mActivity = activity;
        this.mContext = context;
        isCaching = false;
    }

    public void cacheUserSession(SessionHandler handler) {
        this.mHandler = handler;
        APICalls.SendAWSIdentity(mContext, mRequestQueue, mSession);
        switch (Preferences.getPreference(Constants.USER_TYPE_KEY, mContext, -1)) {
            case Constants.GUEST_USER:
                LoginUser(Constants.GUEST_USER);
                break;
            case Constants.VIEWER_USER:
                LoginUser(Constants.VIEWER_USER);
                break;
            case Constants.BROADCASTER_USER:
                LoginBroadcaster();
                break;
            default:
                mHandler.RunCompleted(false, 0);
                break;
        }
    }

    private void LoginUser(int userType) {
        deviceID = Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.ANDROID_ID);

        if (userType == Constants.GUEST_USER)
            userID = "temp_" + deviceID;
        else
            userID = Preferences.getPreference(Constants.KEY_USER_ID, mContext, "");

        APICalls.requestLoginUser(mActivity, userID, deviceID, mContext, mSession, mRequestQueue, new LoginHandler() {
            @Override
            public void RunCompleted(int UserCode, User user) {
                switch (UserCode) {
                    case Constants.USERCODE_SUCCESS:
                        LVUtils.StartLiVideo(user, null, true, null, mActivity, mContext, mSession, mRequestQueue, mHandler);
                        break;
                    case Constants.USERCODE_INVALID_RESPONSE:
                        AndroidUtils.showShortToast(mContext.getString(R.string.invaled_server), mActivity);
                        mHandler.RunCompleted(false, 0);
                        break;
                    case Constants.USERCODE_NOT_EXISTS:
                        AndroidUtils.showShortToast(mContext.getString(R.string.user_does_not_exist), mActivity);
                        mHandler.RunCompleted(false, 0);
                        break;
                    case Constants.USERCODE_OTHER_ERROR:
                        AndroidUtils.showShortToast(mContext.getString(R.string.retry_later), mActivity);
                        mHandler.RunCompleted(false, 0);
                        break;
                    default:
                        AndroidUtils.showShortToast(mContext.getString(R.string.wrong_un_or_pw), mActivity);  //wrong password response
                        mHandler.RunCompleted(false, 0);
                }
            }
        });
    }

    private void LoginBroadcaster() {
        userID = Preferences.getPreference(Constants.KEY_USER_ID, mContext, "");
        deviceID = Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.ANDROID_ID);
        APICalls.requestBroadcasterHackLogin(mSession, mActivity, mContext, userID, deviceID, mRequestQueue, new LoginHandler() {
            @Override
            public void RunCompleted(int UserCode, User user) {
                switch (UserCode) {
                    case Constants.USERCODE_SUCCESS:
                        LVUtils.StartLiVideo(user, null, true, null, mActivity, mContext, mSession, mRequestQueue, mHandler);
                        break;
                    case Constants.USERCODE_INVALID_RESPONSE:
                        AndroidUtils.showShortToast(mContext.getString(R.string.invaled_server), mActivity);
                        mHandler.RunCompleted(false, 0);
                        break;
                    case Constants.USERCODE_NOT_EXISTS:
                        AndroidUtils.showShortToast(mContext.getString(R.string.invalid_artist_id), mActivity);
                        mHandler.RunCompleted(false, 0);
                        break;
                    case Constants.USERCODE_OTHER_ERROR:
                        AndroidUtils.showShortToast(mContext.getString(R.string.retry_later), mActivity);
                        mHandler.RunCompleted(false, 0);
                        break;
                }
            }
        });
    }
}
