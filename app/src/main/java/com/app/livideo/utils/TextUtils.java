package com.app.livideo.utils;

import java.security.MessageDigest;
import java.util.Formatter;
import java.util.Locale;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextUtils {

    private static final String REGEX_EMAIL = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";

    public static boolean isEmailValid(String email) {
        boolean isValid = false;
        Pattern pattern = Pattern.compile(REGEX_EMAIL, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        if (matcher.matches())
            isValid = true;
        return isValid;
    }

    public static boolean isNullOrEmpty(String input) {
        return input == null || input.length() == 0 || input.equalsIgnoreCase("null");
    }

    //Encrypts password to SHA1
    public static String getSHA1String(String password) {
        String sha1;
        try {
            MessageDigest crypt = MessageDigest.getInstance("SHA-1");
            crypt.reset();
            crypt.update(password.getBytes("UTF-8"));
            sha1 = byteToHex(crypt.digest());
        } catch (Exception ex) {
            return password;
        }
        return sha1;
    }

    private static String byteToHex(final byte[] hash) {
        Formatter formatter = new Formatter();
        for (byte b : hash)
            formatter.format("%02x", b);
        String result = formatter.toString();
        formatter.close();
        return result;
    }

    public static String toCamelCase(String sourceString) {
        String[] parts = sourceString.split("\\s");
        String returnString = "";
        for (int i = 0; i < parts.length; i++) {
            if (i == 0) {
                returnString = returnString + firstCharacterUpperCase(parts[i]);
            } else {
                returnString = returnString + " " + firstCharacterUpperCase(parts[i]);
            }
        }
        return returnString;
    }

    private static String firstCharacterUpperCase(String s) {
        return s.substring(0, 1).toUpperCase(Locale.getDefault()) + s.substring(1).toLowerCase(Locale.getDefault());
    }

    public static int getRandomInt(int min, int max) {
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return randomNum;
    }

    public static boolean textHasUpperCase(String pass) {
        for (int i = 0; i < pass.length(); i++) {
            if (Character.isUpperCase(pass.charAt(i))) {
                return true;
            }
        }
        return false;
    }

    public static boolean textHasNumber(String pass) {
        for (int i = 0; i < pass.length(); i++) {
            if (Character.getNumericValue(pass.charAt(i)) == 0 ||
                    Character.getNumericValue(pass.charAt(i)) == 1 ||
                    Character.getNumericValue(pass.charAt(i)) == 2 ||
                    Character.getNumericValue(pass.charAt(i)) == 3 ||
                    Character.getNumericValue(pass.charAt(i)) == 4 ||
                    Character.getNumericValue(pass.charAt(i)) == 5 ||
                    Character.getNumericValue(pass.charAt(i)) == 6 ||
                    Character.getNumericValue(pass.charAt(i)) == 7 ||
                    Character.getNumericValue(pass.charAt(i)) == 8 ||
                    Character.getNumericValue(pass.charAt(i)) == 9) {
                return true;
            }
        }
        return false;
    }
}
