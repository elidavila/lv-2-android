package com.app.livideo.utils.inappbilling;

/**
 * Created by MWheeler on 10/2/2015.
 */
@SuppressWarnings("HardCodedStringLiteral")
public interface IabConstants {

    /**
     * Public key in base64 generated from Google developer account used to
     * allow in app billing
     */
    String PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAq8wv03zpvpjnXERuzJTqWIGz8nMABa8r/TQb5T9neBDQULZjiuQ13T4Qdqq3ON8lHvQRBk7g/AmdlOZC69Zfmry7bEVUsXiC2J7FvLeZ5h0IqUkSbswa+1YGz/v0CwGukQLar/bkHAPxpZytc/c3PQfq84jN8O1v+qBkm1apOnfk9VNkcsJ5wAdM6pHMNWRlkYO7rwO1PkYaep49FNEbamj4ZkMVHGAALOmLZlHcM1I0vDO5IW+BLg+ctWrKAxEGXx42jBp9gJbmcQVaSzf4Q0JdTZAZS97FrqT6S2sYPXvd/Ox6jhaoer9Ocn6LgT7I44LPeqv2eo5f5L+WOq+zswIDAQAB";


}
