package com.app.livideo.lv_app;

import com.amazonaws.auth.CognitoCredentialsProvider;
import com.app.livideo.models.Artist;
import com.app.livideo.models.Discover;
import com.app.livideo.models.SearchData;
import com.app.livideo.models.Stream;
import com.app.livideo.models.User;
import com.app.livideo.net.LVStringRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Eli on 8/24/2015.
 */
public class LVSession {
    private User user;
    private List<String> channelCategorieList;
    private Artist currentArtist;
    private CognitoCredentialsProvider awsCredentials;
    private boolean hasResendCode;
    private ArrayList<LVStringRequest> delayedAPIRequestList;
    private Runnable currentRunnable;
    private Discover discoverData;
    private List<Stream> mTimeLines;
    private SearchData searchData;
    private User currentPrivateChatUser;

    public User getCurrentPrivateChatUser() {
        return currentPrivateChatUser;
    }

    public void setCurrentPrivateChatUser(User currentPrivateChatUser) {
        this.currentPrivateChatUser = currentPrivateChatUser;
    }

    public void clearSession() {
        user = null;
        channelCategorieList = null;
        currentArtist = null;
        awsCredentials = null;
        hasResendCode = false;
        delayedAPIRequestList = null;
        currentRunnable = null;
        discoverData = null;
        mTimeLines = null;
        searchData = null;
        currentPrivateChatUser = null;
    }

    public User getUser() {
        if (user == null)
            user = new User();
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<String> getArtistTypesList() {
        return channelCategorieList;
    }

    public void setArtistTypesList(List<String> channelCategorieList) {
        this.channelCategorieList = channelCategorieList;
    }

    public Artist getCurrentArtist() {
        return currentArtist;
    }

    public void setCurrentArtist(Artist currentArtist) {
        this.currentArtist = currentArtist;
    }

    public CognitoCredentialsProvider getAWSCredentials() {
        return awsCredentials;
    }

    public void setAWSCredentials(CognitoCredentialsProvider awsIdentity) {
        this.awsCredentials = awsIdentity;
    }

    public void setHasResendCode(boolean hasResendCode) {
        this.hasResendCode = hasResendCode;
    }

    public boolean hasResendCode() {
        return hasResendCode;
    }

    public ArrayList<LVStringRequest> getDelayedAPIRequestList() {
        if (delayedAPIRequestList == null) {
            delayedAPIRequestList = new ArrayList<>();
        }
        return delayedAPIRequestList;
    }

    public void setDelayedAPIRequestList(ArrayList<LVStringRequest> delayedAPIRequestList) {
        this.delayedAPIRequestList = delayedAPIRequestList;
    }

    public Runnable getCurrentRunnable() {
        return currentRunnable;
    }

    public void setCurrentRunnable(Runnable currentRunnable) {
        this.currentRunnable = currentRunnable;
    }

    public Discover getDiscoverData() {
        return discoverData;
    }

    public void setDiscoverData(Discover discoverData) {
        this.discoverData = discoverData;
    }

    public List<Stream> getMyTimeLines() {
        return mTimeLines;
    }

    public void setMyTimeLines(List<Stream> mTimeLines) {
        this.mTimeLines = mTimeLines;
    }

    public SearchData getSearchData() {
        return searchData;
    }

    public void setSearchData(SearchData searchData) {
        this.searchData = searchData;
    }
}
