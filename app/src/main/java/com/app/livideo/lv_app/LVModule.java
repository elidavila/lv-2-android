package com.app.livideo.lv_app;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.app.livideo.activities.ArtistPageActivity;
import com.app.livideo.activities.ArtistSocialSignUp;
import com.app.livideo.activities.ChatActivity;
import com.app.livideo.activities.ContactsSelectionActivity;
import com.app.livideo.activities.CropImageActivity;
import com.app.livideo.activities.EnterBirthdayActivity;
import com.app.livideo.activities.EnterCategoryActivity;
import com.app.livideo.activities.EnterDisplayNameActivity;
import com.app.livideo.activities.EnterEmailActivity;
import com.app.livideo.activities.EnterMoneyActivity;
import com.app.livideo.activities.EnterPhoneNumberActivity;
import com.app.livideo.activities.EnterProfilePictureActivity;
import com.app.livideo.activities.ForgotPasswordActivity;
import com.app.livideo.activities.GetStartedActivity;
import com.app.livideo.activities.MainMenuActivity;
import com.app.livideo.activities.NewSettingsActivity;
import com.app.livideo.activities.NoInternetActivity;
import com.app.livideo.activities.OpenMicReviewActivity;
import com.app.livideo.activities.PlayerActivity;
import com.app.livideo.activities.PopUpActivity;
import com.app.livideo.activities.PurchaseActivity;
import com.app.livideo.activities.SearchArtistActivity;
import com.app.livideo.activities.SignInActivity;
import com.app.livideo.activities.SplashActivity;
import com.app.livideo.activities.VerifyCodeActivity;
import com.app.livideo.activities.VideoRecordActivity;
import com.app.livideo.adapters.ArtistChatAdapter;
import com.app.livideo.adapters.ArtistChatPreviewAdapter;
import com.app.livideo.adapters.ArtistMyVideosAdapter;
import com.app.livideo.adapters.ArtistPageTimeLineAdapter;
import com.app.livideo.adapters.DiscoverAdapter;
import com.app.livideo.adapters.DiscoverChannelsRowAdapter;
import com.app.livideo.adapters.DiscoverColumnAdapter;
import com.app.livideo.adapters.DiscoverRowAdapter;
import com.app.livideo.adapters.FeatureAdapter;
import com.app.livideo.adapters.MePageAdapter;
import com.app.livideo.adapters.PrivateChatAdapter;
import com.app.livideo.adapters.SearchArtistAdapter;
import com.app.livideo.dialogs.ExclusiveWatchDialog;
import com.app.livideo.dialogs.GuestWatchDialog;
import com.app.livideo.dialogs.InviteContactsDialog;
import com.app.livideo.dialogs.PrivateChatDialog;
import com.app.livideo.dialogs.RemoveStreamDialog;
import com.app.livideo.dialogs.SubscribeSuccessDialog;
import com.app.livideo.fragments.ArtistRecordHeadlineFragment;
import com.app.livideo.fragments.ArtistRecordViewFragment;
import com.app.livideo.fragments.BlockedUsersFragment;
import com.app.livideo.fragments.ChatArtistFragment;
import com.app.livideo.fragments.ChatPrivateFragment;
import com.app.livideo.fragments.DiscoverFragment;
import com.app.livideo.fragments.GoLiveFragment;
import com.app.livideo.fragments.MePageFragment;
import com.app.livideo.fragments.OpenMicFragment;
import com.app.livideo.fragments.ScheduledFragment;
import com.app.livideo.fragments.SettingsPageOneFragment;
import com.app.livideo.fragments.SettingsPageTwoFragment;
import com.app.livideo.fragments.ViewerSettingsFragment;
import com.app.livideo.fragments.WhispersFragment;
import com.app.livideo.net.LVJsonObjectRequest;
import com.app.livideo.net.LVStringRequest;
import com.app.livideo.notifications.GCMListenerService;
import com.app.livideo.notifications.GCMRegistrationIntentService;
import com.app.livideo.services.S3UploadService;
import com.app.livideo.social_login.InstagramLogin;
import com.app.livideo.social_login.TwitterLogin;
import com.app.livideo.social_login.YoutubeLogin;
import com.app.livideo.utils.SessionUtils;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(
        injects = {
                SplashActivity.class,
                LVStringRequest.class,
                LVApplication.class,
                LVJsonObjectRequest.class,
                SessionUtils.class,
                DiscoverColumnAdapter.class,
                DiscoverChannelsRowAdapter.class,
                ArtistChatPreviewAdapter.class,
                ChatArtistFragment.class,
                VerifyCodeActivity.class,
                SearchArtistActivity.class,
                ExclusiveWatchDialog.class,
                PlayerActivity.class,
                PurchaseActivity.class,
                CropImageActivity.class,
                ArtistMyVideosAdapter.class,
                ForgotPasswordActivity.class,
                ArtistRecordHeadlineFragment.class,
                ArtistRecordViewFragment.class,
                S3UploadService.class,
                GCMRegistrationIntentService.class,
                GCMListenerService.class,
                GuestWatchDialog.class,
                ContactsSelectionActivity.class,
                InviteContactsDialog.class,
                TwitterLogin.class,
                InstagramLogin.class,
                ExclusiveWatchDialog.class,
                VideoRecordActivity.class,
                GoLiveFragment.class,
                NoInternetActivity.class,
                ArtistChatAdapter.class,
                SearchArtistAdapter.class,
                RemoveStreamDialog.class,
                ChatActivity.class,
                SubscribeSuccessDialog.class,
                PopUpActivity.class,
                VideoRecordActivity.class,
                ScheduledFragment.class,
                GetStartedActivity.class,
                SignInActivity.class,
                EnterPhoneNumberActivity.class,
                EnterEmailActivity.class,
                EnterDisplayNameActivity.class,
                EnterBirthdayActivity.class,
                EnterProfilePictureActivity.class,
                ArtistSocialSignUp.class,
                EnterCategoryActivity.class,
                EnterMoneyActivity.class,
                MainMenuActivity.class,
                NewSettingsActivity.class,
                OpenMicFragment.class,
                DiscoverFragment.class,
                FeatureAdapter.FeatureFragment.class,
                DiscoverRowAdapter.class,
                DiscoverAdapter.class,
                OpenMicReviewActivity.class,
                ArtistPageActivity.class,
                ArtistPageTimeLineAdapter.TimeLinePageFragment.class,
                SettingsPageOneFragment.class,
                SettingsPageTwoFragment.class,
                ViewerSettingsFragment.class,
                MePageFragment.class,
                MePageAdapter.TimeLinePageFragment.class,
                WhispersFragment.class,
                BlockedUsersFragment.class,
                PrivateChatDialog.class,
                ChatPrivateFragment.class,
                PrivateChatAdapter.class,
                YoutubeLogin.class
        }
)
public class LVModule {
    private Context mContext;

    public LVModule(Context context) {
        mContext = context;
    }

    @Singleton
    @Provides
    Context provideApplicationContext() {
        return mContext;
    }

    @Singleton
    @Provides
    RequestQueue provideRequestQueue(Context context) {
        return Volley.newRequestQueue(context);
    }

    @Singleton
    @Provides
    LVSession provideSession() {
        return new LVSession();
    }
}
