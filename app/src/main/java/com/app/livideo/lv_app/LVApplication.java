package com.app.livideo.lv_app;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.app.livideo.utils.AppConstants;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.facebook.FacebookSdk;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import javax.inject.Inject;

import dagger.ObjectGraph;
import io.fabric.sdk.android.Fabric;

/**
 * Created by eli on 8/24/2015.
 */
public class LVApplication extends Application {

    @Inject Context mContext;

    @Override
    public void onCreate() {
        MultiDex.install(this);
        super.onCreate();

        LVInject.initialize(ObjectGraph.create(new LVModule(this)));
        LVInject.inject(this);

        FacebookSdk.sdkInitialize(mContext);

        TwitterAuthConfig authConfig = new TwitterAuthConfig(AppConstants.TWITTER_KEY, AppConstants.TWITTER_SECRET);
        Fabric.with(this,
                new Twitter(new TwitterAuthConfig(AppConstants.TWITTER_KEY, AppConstants.TWITTER_SECRET)),
                new Crashlytics.Builder().core(new CrashlyticsCore.Builder().build()).build(), new Twitter(authConfig));
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
