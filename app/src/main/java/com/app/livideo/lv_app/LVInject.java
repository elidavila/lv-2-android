package com.app.livideo.lv_app;

import dagger.ObjectGraph;

/**
 * Created by eli on 8/24/2015.
 */
public class LVInject {
    private static ObjectGraph sObjectGraph;

    public static void initialize(ObjectGraph graph) {
        sObjectGraph = graph;
    }

    public static <T> T inject(T instance) {
        return sObjectGraph.inject(instance);
    }
}
