package com.app.livideo.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentProviderOperation;
import android.content.OperationApplicationException;
import android.os.Bundle;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.view.Window;

import com.app.livideo.R;
import com.app.livideo.utils.AndroidUtils;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.Preferences;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by eli on 12/14/2015.
 */
public class AddLiVideoContactDialog extends Dialog {
    Activity mActivity;

    public AddLiVideoContactDialog(Activity activity) {
        super(activity);
        this.mActivity = activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_add_livideo_contact);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.dialog_add_livideo_contact_ok)
    protected void onOkClick() {
        addContact(mActivity.getString(R.string.app_name), mActivity.getString(R.string.livideo_phone), mActivity.getString(R.string.livideo_support_livideo));
        Preferences.setPreference(AppConstants.SHOW_CONTACTS_DIALOGUE, false, getContext());
        dismiss();
    }

    @OnClick(R.id.dialog_add_livideo_contact_cancel)
    protected void onCancelClick() {
        dismiss();
    }

    private void addContact(String name, String phoneNumber, String email) {
        ArrayList<ContentProviderOperation> ops = new ArrayList<>();
        int rawContactID = ops.size();

        // Adding insert operation to operations listto insert a new raw contact in the table ContactsContract.RawContacts
        ops.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
                .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
                .build());

        // Adding insert operation to operations list to insert display name in the table ContactsContract.Data
        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, name)
                .build());

        // Adding insert operation to operations list to insert Mobile Number in the table ContactsContract.Data
        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, phoneNumber)
                .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
                .build());

        // Email
        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Email.ADDRESS, email)
                .withValue(ContactsContract.CommonDataKinds.Email.TYPE, ContactsContract.CommonDataKinds.Email.TYPE_WORK)
                .build());

        // Executing all the insert operations as a single database transaction
        try {
            getContext().getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
            AndroidUtils.showShortToast(mActivity.getString(R.string.added_livideo_to_contacts_string), mActivity);
        } catch (RemoteException | OperationApplicationException e) {
            e.printStackTrace();
            AndroidUtils.showShortToast(mActivity.getString(R.string.retry_later), mActivity);
        }
    }
}
