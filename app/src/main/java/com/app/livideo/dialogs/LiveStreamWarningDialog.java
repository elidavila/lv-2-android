package com.app.livideo.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.Window;

import com.app.livideo.R;
import com.app.livideo.activities.ArtistRecordActivity;
import com.app.livideo.fragments.ArtistRecordHeadlineFragment;
import com.app.livideo.utils.Preferences;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class LiveStreamWarningDialog extends Dialog {
    Activity mActivity;
    ArtistRecordHeadlineFragment mFragment;

    public LiveStreamWarningDialog(Activity activity, ArtistRecordHeadlineFragment fragment) {
        super(activity);
        this.mActivity = activity;
        this.mFragment = fragment;
    }

    public LiveStreamWarningDialog(Activity activity) {
        super(activity);
        this.mActivity = activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_live_stream_warning);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.go_live_button)
    protected void onGoLiveClick() {
        Bundle args = new Bundle();
        Preferences.setPreference(ArtistRecordActivity.SHARED_PREF_IS_GOING_LIVE, true, mActivity);
        mFragment.setArgs(args);
        mFragment.onGoLiveClick();
        dismiss();
    }

    @OnClick(R.id.go_live_cancel_button)
    protected void onCancelClick() {
        dismiss();
    }

}
