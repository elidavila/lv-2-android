package com.app.livideo.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.android.volley.RequestQueue;
import com.app.livideo.R;
import com.app.livideo.activities.ChatActivity;
import com.app.livideo.api.APICalls;
import com.app.livideo.interfaces.CodeNumberHandler;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.ArtistChatMessage;
import com.app.livideo.models.User;
import com.app.livideo.models.UserImage;
import com.app.livideo.utils.AndroidUtils;
import com.app.livideo.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Eli on 5/2/2016.
 */
public class PrivateChatDialog extends Dialog {

    @Inject Context mContext;
    @Inject LVSession mSession;
    @Inject RequestQueue mRequestQueue;

    ChatActivity mParent;
    ArtistChatMessage mChatMessage;
    EditText mEditText;

    @Bind(R.id.whisper_button) CardView whisperButton;
    @Bind(R.id.block_button) CardView blockButton;

    public PrivateChatDialog(ChatActivity parent, ArtistChatMessage chatMessage, EditText editText) {
        super(parent);
        this.mParent = parent;
        this.mChatMessage = chatMessage;
        this.mEditText = editText;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_private_chat);
        LVInject.inject(this);
        ButterKnife.bind(this);
        if (mChatMessage.getMessageUserId().equalsIgnoreCase(mSession.getCurrentArtist().getArtistId())
                || mChatMessage.getMessageUserId().equalsIgnoreCase(mSession.getUser().getUserID())
                || mSession.getUser().getUserRole() == User.UserRole.GUEST
                || mSession.getUser().getHasMembership() == 0) {
            whisperButton.setVisibility(View.GONE);
            blockButton.setVisibility(View.GONE);
        }

        if (mSession.getUser().getHasMembership() == 0 || mSession.getUser().getUserRole() == User.UserRole.GUEST)
            blockButton.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.mention_button)
    protected void onMentionClick() {
        dismiss();
        mEditText.setText(String.format("%s@%s", mEditText.getText().toString(), mChatMessage.getDisplayName()));
        mEditText.setSelection(mEditText.length());
        mEditText.requestFocus();
        ((InputMethodManager)mParent.getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    @OnClick(R.id.whisper_button)
    protected void onWhisperClick() {
        User user = new User();
        user.setUserID(mChatMessage.getMessageUserId());
        user.setDisplayName(mChatMessage.getDisplayName());
        UserImage image = new UserImage();
        image.setUrl(mChatMessage.getPictureUrl());
        List<UserImage> pictures = new ArrayList<>();
        pictures.add(image);
        user.setPictures(pictures);
        mParent.startPrivateChat(user);
        dismiss();
    }

    @OnClick(R.id.block_button)
    protected void onBlockClick() {
        APICalls.requestBlockUser(mChatMessage.getMessageUserId(), mParent, mContext, mSession, mRequestQueue, new CodeNumberHandler() {
            @Override
            public void RunCompleted(int code) {
                AndroidUtils.showShortToast((code == Constants.USERCODE_SUCCESS)
                        ? mChatMessage.getDisplayName() + mContext.getString(R.string.is_now_blocked)
                        : mContext.getString(R.string.unable_to_block)
                        , mParent);
            }
        });
        dismiss();
    }

    @OnClick(R.id.private_chat_cancel)
    protected void onCancelClick() {
        dismiss();
    }
}