package com.app.livideo.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.Window;
import android.widget.TextView;

import com.app.livideo.R;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.Artist;
import com.app.livideo.models.Stream;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by eli davila on 9/30/2015.
 */
public class SubscribeSuccessDialog extends Dialog {

    @Inject LVSession mSession;
    Activity activity;

    public SubscribeSuccessDialog(Activity activity) {
        super(activity);
        LVInject.inject(this);
        this.activity = activity;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_subscribe_success);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.subscribe_success_ok_button)
    protected void onOkClick() {
        activity.finish();
        dismiss();
    }
}