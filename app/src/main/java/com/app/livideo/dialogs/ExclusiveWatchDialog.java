package com.app.livideo.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.livideo.BuildConfig;
import com.app.livideo.R;
import com.app.livideo.activities.PurchaseActivity;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.Artist;
import com.app.livideo.models.validators.ArtistValidator;
import com.app.livideo.utils.AndroidUtils;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.TextUtils;
import com.app.livideo.utils.inappbilling.IabConstants;
import com.app.livideo.utils.inappbilling.IabHelper;
import com.app.livideo.utils.inappbilling.IabResult;
import com.app.livideo.utils.inappbilling.Inventory;
import com.app.livideo.utils.inappbilling.SkuDetails;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Michael Wheeler on 9/30/2015.
 *
 * Displays what you get for subscribing before purchasing
 */
public class ExclusiveWatchDialog extends Dialog implements IabConstants {

    @Bind(R.id.exclusive_watch_text) TextView confimationText;
    @Inject LVSession mSession;
    @Inject Context mContext;
    Activity activity;
    Runnable mRunnable;
    IabHelper billingHelper;
    static final String ITEM_SKU = "livideo.membership";
    SkuDetails packageDetails;
    private Artist mArtist;

    public ExclusiveWatchDialog(Activity activity, Artist artist, Runnable runnable) {
        super(activity);
        LVInject.inject(this);
        this.activity = activity;
        if (mSession != null && ArtistValidator.isValid(artist))
            mSession.setCurrentArtist(artist);
        this.mRunnable = runnable;
        this.mArtist = artist;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_exclusive_watch);
        LVInject.inject(this);
        ButterKnife.bind(this);

        if (ArtistValidator.isValid(mArtist) && !BuildConfig.DEBUG) {
            final ArrayList<String> skuList = new ArrayList<>();
            skuList.add(ITEM_SKU);

            final IabHelper.QueryInventoryFinishedListener inventoryFinishedListener = new IabHelper.QueryInventoryFinishedListener() {
                @Override
                public void onQueryInventoryFinished(IabResult result, Inventory inv) {
                    if (inv != null && result != null && result.isSuccess()) {
                        packageDetails = inv.getSkuDetails(ITEM_SKU);
                        String price = mContext.getString(R.string.cost_usd);
                        if (packageDetails != null) {
                            String sku = packageDetails.getSku();
                            price = packageDetails.getPrice();

                            if (!TextUtils.isNullOrEmpty(sku) && !TextUtils.isNullOrEmpty(price) && ArtistValidator.isValid(mArtist)) {
                                if (sku.equalsIgnoreCase(ITEM_SKU)) {
                                    confimationText.setText(activity.getString(R.string.exclusive_watch_start_string).replace("%%COST%%", price));
                                } else if (activity != null) {
                                    AndroidUtils.showLongToast(activity.getString(R.string.retry_later), activity);
                                    dismiss();
                                }
                            }
                        } else if (activity != null) {
                            AndroidUtils.showLongToast(mContext.getString(R.string.retry_later), activity);
                            dismiss();
                        }
                    }
                }
            };

            billingHelper = new IabHelper(activity, PUBLIC_KEY);
            billingHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
                @Override
                public void onIabSetupFinished(IabResult result) {
                    if (result != null && result.isSuccess()) {
                        billingHelper.queryInventoryAsync(true, skuList, inventoryFinishedListener);
                    }
                }
            });
        } else if (BuildConfig.DEBUG) {
            String price = mContext.getString(R.string.cost_usd);
            confimationText.setText(activity.getString(R.string.exclusive_watch_start_string).replace("%%COST%%", price));
        } else if (activity != null) {
            AndroidUtils.showLongToast(activity.getString(R.string.retry_later), activity);
            dismiss();
        }
    }

    @OnClick(R.id.exclusive_watch_button)
    protected void onWatchClick() {
        if (ArtistValidator.isValid(mArtist)) {
            mSession.setCurrentRunnable(mRunnable);
            Intent purchaseIntent = new Intent(activity, PurchaseActivity.class);
            purchaseIntent.putExtra(AppConstants.ARTIST_KEY_INTENT, mArtist);
            activity.startActivity(purchaseIntent);
            dismiss();
        } else if (activity != null) {
            AndroidUtils.showLongToast(activity.getString(R.string.retry_later), activity);
            dismiss();
        }
    }

    @OnClick(R.id.exclusive_watch_cancel_button)
    protected void onCancelClick() {
        dismiss();
    }
}
