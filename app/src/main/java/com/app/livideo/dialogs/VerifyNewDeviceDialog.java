package com.app.livideo.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;

import com.app.livideo.R;
import com.app.livideo.activities.VerifyCodeActivity;
import com.app.livideo.utils.AppConstants;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by eli on 12/10/2015.
 */
public class VerifyNewDeviceDialog extends Dialog {

    final Activity mActivity;
    Bundle mBundle;

    public VerifyNewDeviceDialog(Activity activity, Bundle bundle) {
        super(activity);
        this.mActivity = activity;
        this.mBundle = bundle;
    }

    public VerifyNewDeviceDialog(Activity activity) {
        super(activity);
        this.mActivity = activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_new_device);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.new_device_verify)
    protected void onVerifyClick() {
        Intent intent = new Intent(mActivity, VerifyCodeActivity.class);
        intent.putExtra(AppConstants.ACTIVITY_KEY_INTENT, AppConstants.FROM_NEW_DEVICE);
        intent.putExtra(AppConstants.USER_ID_KEY_INTENT, ".");
        intent.putExtras(mBundle);
        mActivity.startActivity(intent);
        dismiss();
        mActivity.finish();
    }
}
