package com.app.livideo.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;

import com.android.volley.RequestQueue;
import com.app.livideo.R;
import com.app.livideo.api.APICalls;
import com.app.livideo.interfaces.DataAvailabilityHandler;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.Stream;
import com.app.livideo.utils.AndroidUtils;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by eli on 1/21/2016.
 */
public class RemoveStreamDialog extends Dialog {

    @Inject RequestQueue mRequestQueue;
    @Inject LVSession mSession;
    @Inject Context mContext;
    Stream mStream;
    Activity activity;
    FrameLayout mProgressBar;
    Runnable mRunnable;

    public RemoveStreamDialog(Activity activity, Stream stream, FrameLayout progressBar, final Runnable runnable) {
        super(activity);
        LVInject.inject(this);
        this.mStream = stream;
        this.activity = activity;
        this.mProgressBar = progressBar;
        this.mRunnable = runnable;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_remove_stream);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.remove_stream_delete)
    protected void onDeleteStreamClick() {
        dismiss();
        if (mProgressBar != null) mProgressBar.setVisibility(View.VISIBLE);
        APICalls.requestRemoveStream(activity, mContext, mStream, mSession, mRequestQueue, new DataAvailabilityHandler() {
            @Override
            public void RunCompleted(boolean isAvailable) {
                if (mProgressBar != null) mProgressBar.setVisibility(View.GONE);
                if (isAvailable) {
                    mRunnable.run();
                } else {
                    AndroidUtils.showShortToast(activity.getString(R.string.error_unable_to_remove_stream), activity);
                }
            }
        });
    }

    @OnClick(R.id.remove_stream_cancel)
    protected void onCancelCallClick() {
        dismiss();
    }
}
