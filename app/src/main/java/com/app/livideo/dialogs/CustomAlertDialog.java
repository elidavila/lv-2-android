package com.app.livideo.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.Window;
import android.widget.ScrollView;
import android.widget.TextView;

import com.app.livideo.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by eli on 9/30/2015.
 */
public class CustomAlertDialog extends Dialog {
    @Bind(R.id.custom_alert_scroll) ScrollView alertMessageScroll;
    @Bind(R.id.custom_alert_message_text) TextView alertMessageView;
    @Bind(R.id.custom_alert_ok_text) TextView okView;
    String message;
    boolean needsTransparency;

    public CustomAlertDialog(Activity activity, String message, boolean needsTransparency) {
        super(activity);
        this.message = message;
        this.needsTransparency = needsTransparency;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_custom_alert);
        ButterKnife.bind(this);
        alertMessageView.setText(message);
        if (needsTransparency) {
            alertMessageScroll.setBackground(null);
            alertMessageView.setBackground(null);
            okView.setBackground(null);
        }
    }

    @OnClick(R.id.custom_alert_ok_button)
    protected void onOkClick() {
        dismiss();
    }
}