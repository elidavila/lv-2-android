package com.app.livideo.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.app.livideo.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Michael Wheeler on 9/30/2015.
 *
 * Show twitter in a dialog if Twitter app is not installed
 */
public class TwitterViewDialog extends Dialog {
    String mString;

    @Bind(R.id.web_container) WebView container;


    public TwitterViewDialog(Activity activity, String string) {
        super(activity);
        this.mString = string;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_twiter_web);
        ButterKnife.bind(this);
        container.loadUrl(mString);
        container.getSettings().setJavaScriptEnabled(true);
        container.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
    }

//    @OnClick(R.id.record_headline_x)
//    protected void onCancelClick() {
//        dismiss();
//    }
}