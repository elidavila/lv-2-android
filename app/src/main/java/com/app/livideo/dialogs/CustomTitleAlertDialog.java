package com.app.livideo.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.Window;
import android.widget.TextView;

import com.app.livideo.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by eli on 2/05/2016.
 */
public class CustomTitleAlertDialog extends Dialog {
    @Bind(R.id.custom_alert_title_text) TextView alertTitleView;
    @Bind(R.id.custom_alert_message_text) TextView alertMessageView;
    @Bind(R.id.custom_alert_ok_text) TextView okView;
    String title, message;
    boolean needsTransparency;

    public CustomTitleAlertDialog(Activity activity, String title, String message, boolean needsTransparency) {
        super(activity);
        this.title = title;
        this.message = message;
        this.needsTransparency = needsTransparency;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_custom_title_alert);
        ButterKnife.bind(this);
        alertTitleView.setText(title);
        alertMessageView.setText(message);
        if (needsTransparency) {
            alertTitleView.setBackground(null);
            alertMessageView.setBackground(null);
            okView.setBackground(null);
        }
    }

    @OnClick(R.id.custom_alert_ok_button)
    protected void onOkClick() {
        dismiss();
    }
}