package com.app.livideo.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.Window;
import android.widget.Toast;

import com.app.livideo.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by eli davila on 9/30/2015.
 */
public class ShareImageDialog extends Dialog {
    Activity activity;
    Uri bmpUri;
    boolean isFile;
    File mFile;
    Bitmap mBitmap;
    String mPictureId;

    public ShareImageDialog(Activity activity, boolean isFile, File mFile, Bitmap mBitmap, String pictureId) {
        super(activity);
        this.activity = activity;
        this.isFile = isFile;
        this.mFile = mFile;
        this.mBitmap = mBitmap;
        this.mPictureId = pictureId;
    }

    public ShareImageDialog(Activity activity) {
        super(activity);
        this.activity = activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_share_image);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.share_image_save_gallery)
    protected void onSaveGalleryClick() {
        if (isFile) {
            bmpUri = Uri.fromFile(mFile);
        } else {
            bmpUri = getLocalBitmapUri(mBitmap);
        }

        if (bmpUri != null) {
            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis());
            values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
            values.put(MediaStore.MediaColumns.DATA, bmpUri.getPath());
            activity.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        }
        dismiss();
    }

    @OnClick(R.id.share_image_send_message)
    protected void onSendTextMessageClick() {
        if (isFile) {
            bmpUri = Uri.fromFile(mFile);
        } else {
            bmpUri = getLocalBitmapUri(mBitmap);
        }

        if (bmpUri != null) {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
            shareIntent.putExtra("sms_body", "#LiVideo");
            shareIntent.setType("image/jpg");
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            activity.startActivity(Intent.createChooser(shareIntent, activity.getString(R.string.send_string)));
        } else {
            Toast.makeText(getContext(), activity.getString(R.string.retry_later), Toast.LENGTH_LONG).show();
        }
        dismiss();
    }

    @OnClick(R.id.share_image_email)
    protected void onSendEmailClick() {
        if (isFile) {
            bmpUri = Uri.fromFile(mFile);
        } else {
            bmpUri = getLocalBitmapUri(mBitmap);
        }

        if (bmpUri != null) {
            Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "#LiVideo");
            emailIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
            emailIntent.setType("image/png");
            activity.startActivity(Intent.createChooser(emailIntent, ""));
        } else {
            Toast.makeText(getContext(), activity.getString(R.string.retry_later), Toast.LENGTH_LONG).show();
        }
        dismiss();
    }

    private Uri getLocalBitmapUri(Bitmap bmp) {
        // Store image to default external storage directory
        if (bmp != null) {
            Uri bmpUri = null;
            try {
                File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), mPictureId + ".jpg");
                file.getParentFile().mkdirs();
                FileOutputStream out = new FileOutputStream(file);
                bmp.compress(Bitmap.CompressFormat.JPEG, 75, out);
                out.close();
                bmpUri = Uri.fromFile(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bmpUri;
        } else {
            return null;
        }
    }

    @OnClick(R.id.share_image_cancel)
    protected void onCancelClick() {
        dismiss();
    }
}