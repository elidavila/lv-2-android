package com.app.livideo.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.Window;

import com.app.livideo.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Mike on 6/6/2016.
 *
 * Dialog to inform the user that here is a new version available and takes them to Google Play to
 * Update if they want (Yes)
 */
public class NewUpdateDialog extends Dialog {

    Activity mActivity;

    public NewUpdateDialog(Activity activity) {
        super(activity);
        this.mActivity = activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_are_you_18);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.are_you_18_yes)
    protected void onYesClick() {
        //TODO Send to play store
        dismiss();
    }

    @OnClick(R.id.are_you_18_no)
    protected void onNoClick() {
        dismiss();
    }
}
