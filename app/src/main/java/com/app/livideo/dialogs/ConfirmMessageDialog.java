package com.app.livideo.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;

import com.app.livideo.R;
import com.app.livideo.utils.TextUtils;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ConfirmMessageDialog extends Dialog {
    Activity activity;
    String[] recipientsList;
    @Bind(R.id.message_edit_text)
    EditText message;
    private String newMessage;

    public ConfirmMessageDialog(Activity activity, String[] recipients) {
        super(activity);
        this.activity = activity;
        this.recipientsList = recipients;
    }

    public ConfirmMessageDialog(Activity activity) {
        super(activity);
        this.activity = activity;
    }

    public ConfirmMessageDialog(Activity activity, String[] recipients, String message) {
        super(activity);
        this.activity = activity;
        this.recipientsList = recipients;
        this.newMessage = message;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_confirm_message);
        ButterKnife.bind(this);
        if (!TextUtils.isNullOrEmpty(newMessage))
            message.setText(newMessage);
    }

    @OnClick(R.id.confirm_send)
    protected void setTextClickListener() {
        final String sendMessage = message.getText().toString();
        Runnable r = new Runnable() {
            @Override
            public void run() {
                SmsManager smsManager = SmsManager.getDefault();
                if (smsManager != null) {
                    for (String aRecipient : recipientsList)
                        smsManager.sendTextMessage(aRecipient, null, sendMessage, null, null);
                } else if (activity != null) {
                    Toast.makeText(activity, activity.getString(R.string.access_denied_string), Toast.LENGTH_SHORT).show();
                }
            }
        };
        new Thread(r).start();
        dismiss();
        if (activity != null) activity.finish();
    }

    @OnClick(R.id.confirm_cancel)
    protected void onCancelClick() {
        dismiss();
    }
}