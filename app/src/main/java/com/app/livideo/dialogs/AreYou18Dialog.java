package com.app.livideo.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.Window;

import com.app.livideo.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by eli on 2/15/2016.
 */
public class AreYou18Dialog extends Dialog {

    Activity mActivity;
    Runnable mRunnable;

    public AreYou18Dialog(Activity activity, Runnable runnable) {
        super(activity);
        this.mActivity = activity;
        this.mRunnable = runnable;
    }

    public AreYou18Dialog(Activity activity) {
        super(activity);
        this.mActivity = activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_are_you_18);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.are_you_18_yes)
    protected void onYesClick() {
        if (mRunnable != null)
            mRunnable.run();
        dismiss();
    }

    @OnClick(R.id.are_you_18_no)
    protected void onNoClick() {
        dismiss();
    }
}
