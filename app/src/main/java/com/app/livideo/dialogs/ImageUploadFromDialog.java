package com.app.livideo.dialogs;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.Window;

import com.app.livideo.R;
import com.app.livideo.activities.CameraActivity;
import com.app.livideo.utils.AppConstants;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by MWheeler on 1/9/2016.
 */
public class ImageUploadFromDialog extends Dialog {

    Activity mActivity;
    View mLayout;

    public ImageUploadFromDialog(Activity activity) {
        super(activity);
        this.mActivity = activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_upload_image_from);
        mLayout = findViewById(R.id.upload_root);
        ButterKnife.bind(this);
        String deviceMan = android.os.Build.MANUFACTURER;
        if (deviceMan.equalsIgnoreCase("HTC")){
            CardView cv = (CardView) findViewById(R.id.upload_image_from_camera);
            cv.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.upload_image_from_camera)
    protected void onCameraClick() {
        if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestCameraPermission();
        } else {
            Intent launchCameraIntent = new Intent(mActivity, CameraActivity.class);
            mActivity.startActivityForResult(launchCameraIntent, AppConstants.TAKE_PICTURE);
            dismiss();
        }
    }

    @OnClick(R.id.upload_image_from_gallery)
    protected void onGalleryClick() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        mActivity.startActivityForResult(galleryIntent, AppConstants.SELECT_PICTURE);
        dismiss();
    }

    @OnClick(R.id.upload_image_from_cancel)
    protected void onCancelClick() {
        dismiss();
    }

    private void requestCameraPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(mActivity, Manifest.permission.CAMERA)) {
            Snackbar.make(mLayout, R.string.permission_camera_rationale, Snackbar.LENGTH_INDEFINITE).setAction(R.string.ok_caps_string, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ActivityCompat.requestPermissions(mActivity, new String[]{Manifest.permission.CAMERA}, AppConstants.REQUEST_CAMERA);
                }
            }).show();
        } else {
            ActivityCompat.requestPermissions(mActivity, new String[]{Manifest.permission.CAMERA}, AppConstants.REQUEST_CAMERA);
        }
    }
}
