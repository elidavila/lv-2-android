package com.app.livideo.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.Window;

import com.app.livideo.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by eli on 1/26/2016.
 */
public class FlagChatMessageDialog extends Dialog {

    public FlagChatMessageDialog(Activity activity) {
        super(activity);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_flag_chat_message);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.flag_chat_message_submit)
    protected void onSubmitClick() {
        dismiss();
    }

    @OnClick(R.id.flag_chat_message_cancel)
    protected void onSignUpClick() {
        dismiss();
    }
}
