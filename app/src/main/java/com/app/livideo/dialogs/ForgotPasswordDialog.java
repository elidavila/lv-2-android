package com.app.livideo.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;

import com.android.volley.RequestQueue;
import com.app.livideo.R;
import com.app.livideo.activities.SignInActivity;
import com.app.livideo.api.APICalls;
import com.app.livideo.interfaces.DataAvailabilityHandler;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.utils.AndroidUtils;
import com.app.livideo.utils.TextUtils;
import com.bumptech.glide.Glide;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by eli davila on 9/30/2015.
 */
public class ForgotPasswordDialog extends Dialog {

    @Bind(R.id.forgot_password_pw) EditText pwView;
    @Bind(R.id.forgot_password_reenter_pw) EditText reEnterPwView;
    @Bind(R.id.forgot_password_progress_bar) ImageView mProgressBar;
    Activity activity;
    RequestQueue mRequestQueue;
    LVSession mSession;
    String userID, deviceID;

    public ForgotPasswordDialog(String userID, String deviceID, Activity activity, RequestQueue requestQueue, LVSession session) {
        super(activity);
        this.activity = activity;
        this.mRequestQueue = requestQueue;
        this.mSession = session;
        this.userID = userID;
        this.deviceID = deviceID;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_forgot_password);
        ButterKnife.bind(this);
        Glide.with(getContext()).load(R.drawable.loading_anim).asGif().into(mProgressBar);
    }

    @OnClick(R.id.forgot_password_done)
    protected void onDoneClick() {
        if (TextUtils.isNullOrEmpty(pwView.getText().toString().trim()) || pwView.getText().toString().length() < 7) {
            AndroidUtils.showLongToast(activity.getString(R.string.email_registration_password), activity);
            return;
        }

        if (pwView.getText().toString().contains(" ") || reEnterPwView.getText().toString().contains(" ")) {
            AndroidUtils.showLongToast(activity.getString(R.string.password_no_spaces), activity);
            return;
        }

        if (pwView.getText().toString().trim().equals(reEnterPwView.getText().toString().trim())) {
            String pw = TextUtils.getSHA1String(reEnterPwView.getText().toString().trim());
            mProgressBar.setVisibility(View.VISIBLE);
            APICalls.requestChangePassword(userID, deviceID, activity, pw, getContext(), mSession, mRequestQueue, new DataAvailabilityHandler() {
                @Override
                public void RunCompleted(boolean isAvailable) {
                    mProgressBar.setVisibility(View.GONE);
                    if (isAvailable) {
                        dismiss();
                        AndroidUtils.showShortToast(activity.getString(R.string.password_changed), activity);
                        Intent intent = new Intent(activity, SignInActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        activity.startActivity(intent);
                    } else {
                        CustomAlertDialog dialog = new CustomAlertDialog(activity, activity.getString(R.string.an_error_string), true);
                        dialog.show();
                    }
                }
            });
        } else {
            CustomAlertDialog dialog = new CustomAlertDialog(activity, activity.getString(R.string.error_pw_dont_match), true);
            dialog.show();
        }
    }

    @OnClick(R.id.forgot_password_cancel)
    protected void onCancelClick() {
        dismiss();
    }
}