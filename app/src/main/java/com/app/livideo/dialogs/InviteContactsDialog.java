package com.app.livideo.dialogs;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.app.livideo.R;
import com.app.livideo.activities.ContactsSelectionActivity;
import com.app.livideo.utils.AppConstants;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InviteContactsDialog extends Dialog {
    private static String[] PERMISSIONS_CONTACT = {Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CONTACTS, Manifest.permission.SEND_SMS};
    private static int REQUEST_CONTACTS = 0;
    Activity activity;
    final boolean fromWeb;
    String URL;
    View mLayout;
    @Bind(R.id.invite_title) TextView title;

    public InviteContactsDialog(Activity activity) {
        super(activity);
        this.activity = activity;
        this.fromWeb = false;
    }

    public InviteContactsDialog(Activity activity, String url) {
        super(activity);
        this.activity = activity;
        this.fromWeb = true;
        URL = url;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_invite_contacts);
        mLayout = findViewById(R.id.invite_root);
        ButterKnife.bind(this);
        if (fromWeb) {
            title.setText(getContext().getString(R.string.share_string));
        }
    }

    @OnClick(R.id.invite_email)
    protected void setEmailClickListener() {
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {
            // Contacts permissions have not been granted.
            requestContactsPermissions();
        } else {
            if (fromWeb) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", "", null));
                emailIntent.putExtra(Intent.EXTRA_TEXT, URL);
                activity.startActivity(Intent.createChooser(emailIntent, ""));
            } else {
                Intent contactPickerIntent = new Intent(activity, ContactsSelectionActivity.class);
                contactPickerIntent.putExtra(AppConstants.CONTACT_TYPE, AppConstants.CONTACT_EMAIL);
                activity.startActivity(contactPickerIntent);
            }
            dismiss();
        }
    }

    @OnClick(R.id.invite_text)
    protected void setTextClickListener() {
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_CONTACTS)
                != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(activity, Manifest.permission.SEND_SMS)
                != PackageManager.PERMISSION_GRANTED) {
            // Contacts permissions have not been granted.
            requestContactsPermissions();
        } else {
            if (fromWeb) {
                try {
                    Intent shareIntent = new Intent(Intent.ACTION_VIEW);
                    shareIntent.putExtra("sms_body", URL);
                    shareIntent.setType("vnd.android-dir/mms-sms");
                    activity.startActivity(shareIntent);
                } catch (Throwable e) {
                    LaunchContactsSelectionActivity(true);
                }
            } else {
                LaunchContactsSelectionActivity(false);
            }
            dismiss();
        }
    }

    private void LaunchContactsSelectionActivity(final boolean fromWeb) {
        Intent contactPickerIntent = new Intent(activity, ContactsSelectionActivity.class);
        contactPickerIntent.putExtra(AppConstants.CONTACT_TYPE, AppConstants.CONTACT_PHONE);
        if (fromWeb) {
            contactPickerIntent.putExtra(AppConstants.SHOW_CONTACTS_DIALOGUE, URL);
        }
        activity.startActivity(contactPickerIntent);
    }

    @OnClick(R.id.invite_cancel)
    protected void onCancelClick() {
        dismiss();
    }

    private void requestContactsPermissions() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                Manifest.permission.READ_CONTACTS)
                || ActivityCompat.shouldShowRequestPermissionRationale(activity,
                Manifest.permission.WRITE_CONTACTS)
                || ActivityCompat.shouldShowRequestPermissionRationale(activity,
                Manifest.permission.SEND_SMS)) {

            Snackbar.make(mLayout, R.string.permission_contacts_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok_caps_string, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ActivityCompat
                                    .requestPermissions(activity, PERMISSIONS_CONTACT,
                                            REQUEST_CONTACTS);
                        }
                    })
                    .show();
        } else {
            ActivityCompat.requestPermissions(activity, PERMISSIONS_CONTACT, REQUEST_CONTACTS);
        }
    }
}