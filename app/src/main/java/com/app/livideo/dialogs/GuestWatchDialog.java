package com.app.livideo.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;

import com.android.volley.RequestQueue;
import com.app.livideo.R;
import com.app.livideo.activities.ViewerOrArtistActivity;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.utils.LVUtils;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by eli on 9/30/2015.
 */
public class GuestWatchDialog extends Dialog {

    @Inject RequestQueue mRequestQueue;
    @Inject LVSession mSession;
    @Inject Context mContext;
    Activity mActivity;

    public GuestWatchDialog(Activity activity) {
        super(activity);
        this.mActivity = activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_guest_watch);
        LVInject.inject(this);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.guest_watch_login)
    protected void onLoginClick() {
        LVUtils.logoutUser(mActivity, mContext, mSession, mRequestQueue);
        dismiss();
    }

    @OnClick(R.id.guest_watch_sign_up)
    protected void onSignUpClick() {
        LVUtils.logoutUser(mActivity, mContext, mSession, mRequestQueue);
        dismiss();
        mActivity.startActivity(new Intent(mActivity, ViewerOrArtistActivity.class));
    }

    @OnClick(R.id.guest_watch_cancel)
    protected void onCancelClick() {
        dismiss();
    }
}