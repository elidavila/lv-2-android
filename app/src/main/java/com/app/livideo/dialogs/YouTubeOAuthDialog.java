package com.app.livideo.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.app.livideo.R;
import com.app.livideo.interfaces.YoutubeGetAccessTokenHandler;
import com.app.livideo.models.YouTubeLoginData;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Eli on 6/10/2016.
 */
public class YouTubeOAuthDialog extends Dialog {

    @Bind(R.id.web_container) WebView container;
    final Activity mActivity;
    boolean callOnce = false;
    YoutubeGetAccessTokenHandler getAccessTokenHandler;
    private final String mScope = "https://www.googleapis.com/auth/youtube.readonly", mUrl;
    private final String question = "?", EQUALS = "=", AND = "&", CODE = "code", SUCCESS_CODE = "code=";

    public YouTubeOAuthDialog(YouTubeLoginData mYouTubeLoginData, Activity activity, final YoutubeGetAccessTokenHandler handler) {
        super(activity);
        this.mActivity = activity;
        this.callOnce = false;
        this.getAccessTokenHandler = handler;
        this.mUrl = mYouTubeLoginData.getAuthURI() + question
                + YouTubeLoginData.Keys.CLIENT_ID + EQUALS + mYouTubeLoginData.getClientID() + AND
                + YouTubeLoginData.Keys.REDIRECT_URI + EQUALS + mYouTubeLoginData.getRedirectURI() + AND
                + YouTubeLoginData.Keys.SCOPE + EQUALS + mScope + AND
                + YouTubeLoginData.Keys.RESPONSE_TYPE + EQUALS + CODE;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_twiter_web);
        ButterKnife.bind(this);
        container.loadUrl(mUrl);
        container.getSettings().setJavaScriptEnabled(true);
        container.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }

            @Override
            public void onLoadResource(WebView view, String url) {
                String title = view.getTitle();
                if (title.contains(SUCCESS_CODE) && !callOnce) {
                    callOnce = true;
                    dismiss();
                    String authCode = title.substring(title.indexOf(SUCCESS_CODE) + SUCCESS_CODE.length(), title.length()).trim();
                    getAccessTokenHandler.RunCompleted(authCode);
                }
            }
        });

    }
}
