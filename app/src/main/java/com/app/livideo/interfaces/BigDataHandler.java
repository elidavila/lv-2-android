package com.app.livideo.interfaces;

import java.util.Map;

/**
 * Created by Alex on 14.10.2015.
 */
public interface BigDataHandler {
    void RunCompleted(int UserCode, Map<String, Object> map);
}
