package com.app.livideo.interfaces;

import com.app.livideo.models.Stream;

import java.util.List;

/**
 * Created by eli on 10.12.2015.
 */
public interface GuideStreamHandler {
    void RunCompleted(int status, List<Stream> streamList);
}
