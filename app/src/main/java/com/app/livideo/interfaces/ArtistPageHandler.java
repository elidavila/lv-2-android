package com.app.livideo.interfaces;

import com.app.livideo.models.ArtistPage;

/**
 * Created by Eli on 4/12/2016.
 */
public interface ArtistPageHandler {
    void RunCompleted(boolean isAvailable, ArtistPage artistPage);
}
