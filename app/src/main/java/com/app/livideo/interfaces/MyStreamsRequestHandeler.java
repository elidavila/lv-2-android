package com.app.livideo.interfaces;

import com.app.livideo.models.Stream;

import java.util.List;

/**
 * Created by MWheeler on 10/20/2015.
 */
public interface MyStreamsRequestHandeler {
    void RunCompleted(List<Stream> myStreamsList);
}
