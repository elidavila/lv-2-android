package com.app.livideo.interfaces;

import com.app.livideo.models.WhisperRequest;

import java.util.List;

/**
 * Created by Eli on 5/11/2016.
 */
public interface WhisperRequestHandler {
    void RunCompleted(int code, List<WhisperRequest> whisperRequests);
}
