package com.app.livideo.interfaces;

import com.app.livideo.models.Artist;

/**
 * Created by eli on 12/18/2015.
 */
public interface ArtistHandler {
    void RunCompleted(boolean isAvailable, Artist artist);
}
