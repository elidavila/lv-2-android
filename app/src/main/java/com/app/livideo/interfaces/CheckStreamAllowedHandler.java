package com.app.livideo.interfaces;

/**
 * Created by eli on 2/3/2016.
 */
public interface CheckStreamAllowedHandler {
    void RunCompleted(int streamSecondsLeft);
}
