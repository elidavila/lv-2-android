package com.app.livideo.interfaces;

import com.app.livideo.models.Discover;

/**
 * Created by Eli on 4/4/2016.
 */
public interface DiscoverPageHandler {
    void RunCompleted(int code, Discover discover);
}