package com.app.livideo.interfaces;

import com.app.livideo.models.SearchData;

/**
 * Created by Eli on 4/19/2016.
 */
public interface SearchDataHandler {
    void RunCompleted(int UserCode, SearchData searchData);
}
