package com.app.livideo.interfaces;

import com.app.livideo.models.User;
import com.app.livideo.models.WhisperRequest;

import java.util.List;

public interface UserListHandler {
    void RunCompleted(int code, List<User> users);
}
