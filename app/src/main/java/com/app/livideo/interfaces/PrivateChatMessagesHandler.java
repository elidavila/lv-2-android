package com.app.livideo.interfaces;

import com.app.livideo.models.PrivateChatMessage;

import java.util.List;

/**
 * Created by Eli on 5/11/2016.
 */
public interface PrivateChatMessagesHandler {
    void onRunCompleted(List<PrivateChatMessage> privateChatMessages);
}
