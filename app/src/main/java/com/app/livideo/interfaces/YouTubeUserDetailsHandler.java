package com.app.livideo.interfaces;

/**
 * Created by Eli on 6/13/2016.
 */
public interface YouTubeUserDetailsHandler {
    void onRunCompleted(final String displayName, final String socialID);
}
