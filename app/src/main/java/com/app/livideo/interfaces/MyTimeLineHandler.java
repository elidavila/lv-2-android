package com.app.livideo.interfaces;

import com.app.livideo.models.Stream;

import java.util.List;

/**
 * Created by Eli on 4/17/2016.
 */
public interface MyTimeLineHandler {
    void RunCompleted(int UserCode, List<Stream> mTimeLines);
}
