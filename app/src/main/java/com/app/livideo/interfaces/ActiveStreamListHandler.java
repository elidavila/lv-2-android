package com.app.livideo.interfaces;

import com.app.livideo.models.Stream;

import java.util.List;

/**
 * Created by eli on 10/19/2015.
 */
public interface ActiveStreamListHandler {
    void RunCompleted(int status, List<Stream> streamList);
}
