package com.app.livideo.interfaces;

/**
 * Created by KaiKu on 08.10.2015.
 */
public interface DataAvailabilityHandler {
    void RunCompleted(boolean isAvailable);
}
