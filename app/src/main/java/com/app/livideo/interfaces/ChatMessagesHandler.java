package com.app.livideo.interfaces;

import com.app.livideo.models.ArtistChatMessage;

import java.util.List;

/**
 * Created by MWheeler on 10/20/2015.
 */
public interface ChatMessagesHandler {
    void onRunComplete(List<ArtistChatMessage> chatMessageList);
}
