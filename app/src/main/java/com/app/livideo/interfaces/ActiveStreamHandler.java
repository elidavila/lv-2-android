package com.app.livideo.interfaces;

import com.app.livideo.models.Stream;

/**
 * Created by eli on 10/19/2015.
 */
public interface ActiveStreamHandler {
    void RunCompleted(boolean isAvailable, Stream mStream);
}
