package com.app.livideo.interfaces;

import com.app.livideo.models.Artist;

import java.util.List;

/**
 * Created by eli on 11/12/2015.
 */
public interface ArtistListHandler {
    void RunCompleted(boolean isAvailable, List<Artist> artistList);
}
