package com.app.livideo.interfaces;

/**
 * Created by eli on 5/13/2016.
 */
public interface StringResultHandler {
    void onRunCompleted(String result);
}
