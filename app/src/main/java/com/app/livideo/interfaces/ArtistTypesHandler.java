package com.app.livideo.interfaces;

import java.util.List;

/**
 * Created by eli on 11/12/2015.
 */
public interface ArtistTypesHandler {
    void RunCompleted(boolean isAvailable, List<String> artistTypes);
}
