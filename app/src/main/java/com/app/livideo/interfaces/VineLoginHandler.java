package com.app.livideo.interfaces;

import com.app.livideo.models.VineData;

import java.util.Map;

/**
 * Created by KaiKu on 15.10.2015.
 */
public interface VineLoginHandler {
    void LoginCompleted(boolean successful, VineData vineData);
    void onError();
}
