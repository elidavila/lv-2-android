package com.app.livideo.interfaces;

import com.app.livideo.models.EnterStreamData;

public interface EnterStreamHandler {
    void RunCompleted(EnterStreamData enterStreamData);
}