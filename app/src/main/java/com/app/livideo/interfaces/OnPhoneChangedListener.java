package com.app.livideo.interfaces;

public interface OnPhoneChangedListener {

    void onPhoneChanged(String phone);

}
