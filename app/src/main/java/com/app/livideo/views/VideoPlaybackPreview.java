package com.app.livideo.views;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup;
import android.widget.FrameLayout;

/**
 * Created by Christian on 01.11.2015.
 */
public class VideoPlaybackPreview extends SurfaceView implements SurfaceHolder.Callback {


    //constructors
    public VideoPlaybackPreview(Context context) {
        super(context);
    }

    public VideoPlaybackPreview(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public VideoPlaybackPreview(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }



    public void setAspectRatio(int width, int height){
        if(width==0 && height==0) {
            this.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            return;
        }
        //set aspect ratio
        float aspectRatio = (float)height/width;

        int newWidth = this.getDisplay().getWidth();
        int newHeight = (int)(this.getDisplay().getWidth()*aspectRatio);

        this.setLayoutParams(new FrameLayout.LayoutParams(newWidth, newHeight, Gravity.CENTER));
        Log.d(this.getClass().getName(), "setAspectRatio:\nx: " + width + ", y: " + height + "\nRatio: " + String.valueOf(aspectRatio)
            +"\nNew Size of View: "+getDisplay().getWidth()+" x "+(int)(getDisplay().getWidth()*aspectRatio));
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        //
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

        //exit if preview surface does not exist
        if(holder.getSurface()==null)
            return;

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        //
    }
}
