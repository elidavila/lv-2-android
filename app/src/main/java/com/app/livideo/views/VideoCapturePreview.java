package com.app.livideo.views;

import android.content.Context;
import android.hardware.Camera;
import android.util.Log;
import android.view.Gravity;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.app.livideo.utils.CameraUtils;
import com.crashlytics.android.Crashlytics;

/**
 * Created by Christian on 07.10.2015.
 */
public class VideoCapturePreview extends SurfaceView implements SurfaceHolder.Callback {

    private Camera camera;

    public VideoCapturePreview(Context context) {
        super(context);
    }

    public void reassignSurface(){
        this.getHolder().removeCallback(this);
        this.camera = CameraUtils.mCamera;
        this.getHolder().addCallback(this);
    }

    public void setAspectRatio(int width, int height){
        if(width==0 && height==0) {
            this.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            return;
        }
        //set aspect ratio
        float aspectRatio = (float)height/width;

        int newWidth = this.getDisplay().getWidth();
        int newHeight = (int)(this.getDisplay().getWidth()*aspectRatio);

        this.setLayoutParams(new FrameLayout.LayoutParams(newWidth, newHeight, Gravity.CENTER));
        Log.d(this.getClass().getName(), "setAspectRatio:\nx: " + width + ", y: " + height + "\nRatio: " + String.valueOf(aspectRatio)
            +"\nNew Size of View: "+getDisplay().getWidth()+" x "+(int)(getDisplay().getWidth()*aspectRatio));
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            //set Rotation
            camera.setDisplayOrientation(90);  //degrees
            //set AspectRatio
            if(CameraUtils.cameraHasParameters(camera))
                setAspectRatio(camera.getParameters().getPreviewSize().width, camera.getParameters().getPreviewSize().height);
            //set Preview Display
            camera.setPreviewDisplay(holder);
            camera.startPreview();
        }
        catch (Exception e) {
            Log.e(this.getClass().getName(), "surfaceCreated: "+e.getMessage());
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

        //exit if preview surface does not exist
        if(holder.getSurface()==null || CameraUtils.isRecording())
            return;

        try {
            camera.stopPreview();
        }
        catch (Exception e){
            Crashlytics.logException(e); }   //ignore

        try {   //restart preview
            camera.setPreviewDisplay(holder);
            camera.startPreview();
        }
        catch (Exception e) {
            Log.e(this.getClass().getName(), "surfaceChanged: "+e.getMessage());
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        //
    }
}
