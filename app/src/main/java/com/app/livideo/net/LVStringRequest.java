package com.app.livideo.net;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.utils.Preferences;

import java.io.UnsupportedEncodingException;
import java.util.Map;

import javax.inject.Inject;

/**
 * Created by eli davila on 8/25/2015.
 */
public class LVStringRequest extends Request<String> {
    @Inject Context mContext;
    private final Response.Listener<String> mListener;
    Map<String, String> params;
    private static final int LV_TIMEOUT = 60*1000;

    public LVStringRequest(int method, String url, Map<String, String> parameters, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(method, url, errorListener);
        this.mListener = listener;
        this.params = parameters;
        LVInject.inject(this);
        setRetryPolicy(new DefaultRetryPolicy(LV_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    @Override
    protected Map<String, String> getParams() {
        return params;
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {
        //Get our cookie and save it for future authentication
        Preferences.setCookie(response.headers, mContext);

        String parsed;
        try {
            parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
        } catch (UnsupportedEncodingException var4) {
            parsed = new String(response.data);
        }

        return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
    }

    @Override
    protected void deliverResponse(String response) {
        this.mListener.onResponse(response);
    }
}
