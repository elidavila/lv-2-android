package com.app.livideo.fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.app.livideo.R;
import com.app.livideo.activities.MainMenuActivity;
import com.app.livideo.adapters.DiscoverAdapter;
import com.app.livideo.api.APICalls;
import com.app.livideo.interfaces.ArtistTypesHandler;
import com.app.livideo.interfaces.DiscoverPageHandler;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.Discover;
import com.app.livideo.models.validators.DiscoverValidator;
import com.app.livideo.models.validators.StringListValidator;
import com.app.livideo.utils.AndroidUtils;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.PicassoTools;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Eli on 4/4/2016.
 */
public class DiscoverFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private static final int AUTO_SCROLLING_DELAY = 5000;
    @Inject RequestQueue mRequestQueue;
    @Inject LVSession mSession;
    @Inject Context mContext;
    @Bind(R.id.discover_refresh_swipe) SwipeRefreshLayout mSwipe;
    @Bind(R.id.discover_progress_bar) ImageView mProgressBar;
    @Bind(R.id.discover_recycler_view) RecyclerView recyclerView;
    @Bind(R.id.home_toggle) ImageView homeToggle;
    @Bind(R.id.me_toggle) ImageView meToggle;
    @Bind(R.id.header_left_icon) ImageView header_left_icon;
    @Bind(R.id.header_right_icon) ImageView header_right_icon;
    @Bind(R.id.header_main_img) ImageView header_main_img;
    private boolean isDownloading, stopSliding = false, isActive = true;
    private Runnable animateViewPager;
    private Handler handler;
    DiscoverAdapter discoverAdapter;
    MainMenuActivity parent;
    View touch_blocker;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View discoverView = inflater.inflate(R.layout.fragment_discover, container, false);
        LVInject.inject(this);
        ButterKnife.bind(this, discoverView);
        startDiscoverView();
        return discoverView;
    }

    private void startDiscoverView() {
        // Variables
        parent = (MainMenuActivity) getActivity();
        homeToggle.setSelected(true);
        header_main_img.setVisibility(View.VISIBLE);
        isActive = true;
        header_left_icon.setImageResource(R.drawable.ic_navhamburgermenu_wshadows);
        header_right_icon.setImageResource(R.drawable.ic_search_wshadow);
        touch_blocker = parent.loader_view;

        // Layout
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        // Channels Data/View
        if (DiscoverValidator.isValid(mSession.getDiscoverData()) && StringListValidator.isValid(mSession.getArtistTypesList())) {
            addDiscoverView(mSession.getArtistTypesList(), mSession.getDiscoverData());
        }
    }

    private void handleError(String errorString, final boolean shouldFinish) {
        if (mContext == null)
            return;

        if (errorString != null) {
            Toast.makeText(mContext, errorString, Toast.LENGTH_SHORT).show();
            if (shouldFinish && getActivity() != null)
                getActivity().finish();
        } else {
            AndroidUtils.restartApp(mContext);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        isActive = true;
        stopSliding = false;
        startNeededRequests();
        if (discoverAdapter != null && discoverAdapter.featureViewHolder != null) {
            discoverAdapter.setAppIsVisible(true);
            runnable(mSession.getDiscoverData().getFeatures().size());
            handler.postDelayed(animateViewPager, AUTO_SCROLLING_DELAY);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        isActive = false;

        PicassoTools.clearCache(Picasso.with(mContext));
        new GlideClearCache().execute();
        Glide.get(mContext).clearMemory();

        if (handler != null) {
            stopSliding = true;
            handler.removeCallbacks(animateViewPager);
        }

        if (discoverAdapter != null) {
            discoverAdapter.setAppIsVisible(false);
        }
    }

    protected class GlideClearCache extends AsyncTask {
        @Override
        protected Object doInBackground(Object[] params) {
            Glide.get(mContext).clearDiskCache();
            return null;
        }
    }

    public void restartAutoScroll() {
        stopSliding = true;
        handler.removeCallbacks(animateViewPager);
        stopSliding = false;
        runnable(mSession.getDiscoverData().getFeatures().size());
        handler.postDelayed(animateViewPager, AUTO_SCROLLING_DELAY);
    }

    private void startNeededRequests() {
        if (!StringListValidator.isValid(mSession.getArtistTypesList())) {
            requestArtistTypes(false);
        } else if (!DiscoverValidator.isValid(mSession.getDiscoverData())) {
            requestDiscoverPage(false);
        }
    }

    public void runnable(final int size) {
        handler = new Handler();
        animateViewPager = new Runnable() {
            public void run() {
                if (!stopSliding) {
                    if (discoverAdapter != null && discoverAdapter.featureViewHolder != null && discoverAdapter.featureViewHolder.mViewPager != null) {
                        try {
                            if (discoverAdapter.featureViewHolder.mViewPager.getCurrentItem() == size - 1) {
                                discoverAdapter.featureViewHolder.mViewPager.setCurrentItem(0);
                            } else {
                                discoverAdapter.featureViewHolder.mViewPager.setCurrentItem(discoverAdapter.featureViewHolder.mViewPager.getCurrentItem() + 1, true);
                            }
                        } catch (Throwable ignore) {
                        }
                    }
                    handler.postDelayed(animateViewPager, AUTO_SCROLLING_DELAY);
                }
            }
        };
    }

    private void requestArtistTypes(final boolean isRefreshing) {
        if (isRefreshing) mSwipe.setRefreshing(true);
        else touch_blocker.setVisibility(View.VISIBLE);

        APICalls.requestArtistTypes(getActivity(), mContext, mSession, mRequestQueue, new ArtistTypesHandler() {
            @Override
            public void RunCompleted(boolean isAvailable, List<String> artistTypes) {
                if (isAvailable) {
                    mSession.setArtistTypesList(artistTypes);
                    requestDiscoverPage(isRefreshing);
                } else {
                    isDownloading = false;
                    mSwipe.setRefreshing(false);
                    touch_blocker.setVisibility(View.GONE);
                }
            }
        });
    }

    private void requestDiscoverPage(final boolean isRefreshing) {
        if (isRefreshing) mSwipe.setRefreshing(true);
        else touch_blocker.setVisibility(View.VISIBLE);
        APICalls.requestDiscoverPage(getActivity(), mContext, mSession, mRequestQueue, new DiscoverPageHandler() {
            @Override
            public void RunCompleted(int code, final Discover discover) {
                isDownloading = false;
                mSwipe.setRefreshing(false);
                touch_blocker.setVisibility(View.GONE);
                if (isVisible()) {
                    switch (code) {
                        case com.app.livideo.utils.Constants.USERCODE_SUCCESS:
                            mSession.setDiscoverData(discover);
                            addDiscoverView(mSession.getArtistTypesList(), discover);
                            break;
                        case com.app.livideo.utils.Constants.USERCODE_INVALID_RESPONSE:
                            handleError(getString(R.string.invaled_server), false);
                            break;
                        case com.app.livideo.utils.Constants.USERCODE_NOT_EXISTS:
                            handleError(getString(R.string.user_does_not_exist), false);
                            break;
                        case com.app.livideo.utils.Constants.USERCODE_OTHER_ERROR:
                            handleError(getString(R.string.retry_later), false);
                            break;
                        default:
                            handleError(getString(R.string.api_error), false);
                            break;
                    }
                }
            }
        });
    }

    private void addDiscoverView(List<String> artistTypes, Discover discoverData) {
        // REFRESH SWIPE
        mSwipe.setOnRefreshListener(this);
        mSwipe.setColorSchemeColors(ContextCompat.getColor(mContext, R.color.livideo_teal_primary), ContextCompat.getColor(mContext, R.color.livideo_black));

        // DISCOVER VIEW
        if (isActive) {
            discoverAdapter = new DiscoverAdapter(discoverData, this, artistTypes, touch_blocker, getActivity());
            recyclerView.setAdapter(discoverAdapter);
            runnable(discoverData.getFeatures().size());
            handler.postDelayed(animateViewPager, AUTO_SCROLLING_DELAY);
        }
    }

    @Override
    public void onRefresh() {
        if (!isDownloading && mSession != null) {
            isDownloading = true;
            requestArtistTypes(true);
        }
    }

    @OnClick(R.id.header_left_card_view)
    public void drawerClick() {
        parent.openDrawer();
    }

    @OnClick(R.id.header_right_card_view)
    public void searchClick() {
        parent.startSearch();
    }

    @OnClick(R.id.discover_button)
    public void discoverClick() {
        if (!homeToggle.isSelected() && parent != null) {
            homeToggle.setSelected(true);
            meToggle.setSelected(false);
            parent.switchToDiscover();
        }
    }

    @OnClick(R.id.record_button)
    public void recordClick() {
        if (parent != null)
            parent.launchRecording();
    }

    @OnClick(R.id.me_button)
    public void meClick() {
        if (!meToggle.isSelected() && parent != null) {
            homeToggle.setSelected(false);
            meToggle.setSelected(true);
            parent.switchToMe();
        }
    }

}
