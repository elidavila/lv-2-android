package com.app.livideo.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.app.livideo.R;
import com.app.livideo.activities.BigDataViewer;
import com.app.livideo.activities.MainMenuActivity;
import com.app.livideo.api.APICalls;
import com.app.livideo.dialogs.InviteContactsDialog;
import com.app.livideo.interfaces.BigDataHandler;
import com.app.livideo.interfaces.BigDataLinkHandler;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.User;
import com.app.livideo.utils.AndroidUtils;
import com.app.livideo.utils.LVUtils;
import com.app.livideo.utils.TextUtils;

import java.util.Map;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingsPageOneFragment extends Fragment {

    @Bind(R.id.earnings_number) TextView earningsNumber;
    @Bind(R.id.views_number) TextView viewsNumber;
    @Bind(R.id.fans_number) TextView fansNumber;
    @Bind(R.id.category_image) ImageView categoryImage;
    @Bind(R.id.category_title) TextView categoryName;
    @Bind(R.id.category_number) TextView categoryNumber;
    @Bind(R.id.livideo_number) TextView livideoNumber;
    @Bind(R.id.bigdata_stats) View bigDataStats;

    @Inject Context mContext;
    @Inject LVSession mSession;
    @Inject RequestQueue mRequestQueue;

    MainMenuActivity mParent;
    boolean isActive = true;

    public SettingsPageOneFragment() {
        // Required empty public constructor
    }

    @OnClick(R.id.blocked_users)
    protected void onBlockedUsersClick() {
        mParent.displayBlockedUsers();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_settings_page_one, container, false);
        ButterKnife.bind(this, mView);
        LVInject.inject(this);
        mParent = (MainMenuActivity) getActivity();
        bigDataStats.setVisibility((mSession.getUser().getUserRole() == User.UserRole.BROADCASTER) ? View.VISIBLE : View.GONE);
        isActive = true;
        requestGetSubscriptionCount();
        setCategory();
        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
        isActive = true;
    }

    @Override
    public void onPause() {
        super.onPause();
        isActive = false;
    }

    private void requestGetSubscriptionCount() {
        final String userId = mSession.getUser().getUserID();
        String deviceID = Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.ANDROID_ID);
        APICalls.requestBigDataInfo(mSession, getActivity(), mContext, userId, deviceID, mRequestQueue, new BigDataHandler() {
            @Override
            public void RunCompleted(int UserCode, Map<String, Object> map) {
                switch (UserCode) {
                    case com.app.livideo.utils.Constants.USERCODE_SUCCESS:
                        float category = Float.parseFloat((String) map.get("SubscriptionCount_Category"));
                        float livideo = Float.parseFloat((String) map.get("SubscriptionCount_All"));
                        int subscriptions = Integer.parseInt((String) map.get("SubscriptionCount_Count"));
                        float perDay = Float.parseFloat((String) map.get("Revenue_PerDay"));
                        int viewMonth = Integer.parseInt((String) map.get("Revenue_ViewMonth"));
                        earningsNumber.setText("$%%COST%%".replace("%%COST%%", displayRevenue(perDay, 30)));
                        earningsNumber.setVisibility(View.VISIBLE);
                        categoryNumber.setText(categoryNumber.getText().toString().replace("$$00$$", String.valueOf((int) category)));
                        categoryNumber.setVisibility(View.VISIBLE);
                        livideoNumber.setText(livideoNumber.getText().toString().replace("$$00$$", String.valueOf((int) livideo)));
                        livideoNumber.setVisibility(View.VISIBLE);
                        fansNumber.setText(String.valueOf(subscriptions));
                        fansNumber.setVisibility(View.VISIBLE);
                        viewsNumber.setText(String.valueOf(viewMonth));
                        viewsNumber.setVisibility(View.VISIBLE);
                        break;
                    case com.app.livideo.utils.Constants.USERCODE_OTHER_ERROR:
                        AndroidUtils.showLongToast(mContext.getString(R.string.cant_load), getActivity());
                        break;
                    default:
//                        AndroidUtils.showLongToast(mContext.getString(R.string.error_code) + String.valueOf(UserCode), mContext);
                        break;
                }
            }
        });
    }

    public void setCategory() {
        categoryName.setText(mSession.getUser().getArtistType());
        categoryName.setVisibility(View.VISIBLE);
        if (!TextUtils.isNullOrEmpty(mSession.getUser().getArtistType())) {
            if (mSession.getUser().getArtistType().equalsIgnoreCase(User.ArtistTypes.COMEDY)) {
                categoryImage.setImageResource(R.drawable.comedy);
            } else if (mSession.getUser().getArtistType().equalsIgnoreCase(User.ArtistTypes.FASHION)) {
                categoryImage.setImageResource(R.drawable.fashion);
            } else if (mSession.getUser().getArtistType().equalsIgnoreCase(User.ArtistTypes.LIFSTYLE)) {
                categoryImage.setImageResource(R.drawable.lifestyle);
            } else if (mSession.getUser().getArtistType().equalsIgnoreCase(User.ArtistTypes.MODEL)) {
                categoryImage.setImageResource(R.drawable.model);
            } else if (mSession.getUser().getArtistType().equalsIgnoreCase(User.ArtistTypes.MUSIC)) {
                categoryImage.setImageResource(R.drawable.music);
            } else if (mSession.getUser().getArtistType().equalsIgnoreCase(User.ArtistTypes.RADIO)) {
                categoryImage.setImageResource(R.drawable.radio);
            } else if (mSession.getUser().getArtistType().equalsIgnoreCase(User.ArtistTypes.SPORTS)) {
                categoryImage.setImageResource(R.drawable.sports);
            } else if (mSession.getUser().getArtistType().equalsIgnoreCase(User.ArtistTypes.TVFILM)) {
                categoryImage.setImageResource(R.drawable.tv_film);
            } else if (mSession.getUser().getArtistType().equalsIgnoreCase(User.ArtistTypes.YOUTUBE)) {
                categoryImage.setImageResource(R.drawable.youtube);
            }
        } else {
            categoryImage.setImageResource(R.drawable.smallfive);
        }
        categoryImage.setVisibility(View.VISIBLE);
    }

    private String displayRevenue(float perday, int days) {
        String returnValue;
        float total = perday * days;
        String step = String.valueOf(total);
        String[] split = step.split("\\.");
        returnValue = split[0];
        return returnValue;
    }

    @OnClick(R.id.settings_tab)
    public void openSettings() {
        mParent.displaySettingsPageTwo();
    }

    @OnClick(R.id.log_out)
    public void logOut() {
        LVUtils.logoutUser(getActivity(), mContext, mSession, mRequestQueue);
    }

    @OnClick(R.id.invite_friends)
    public void launchFreindInvite() {
        final InviteContactsDialog dialog = new InviteContactsDialog(getActivity());
        dialog.show();
    }

    @OnClick(R.id.full_stats_button)
    public void fullStatsClick() {
        APICalls.requestExportReport(getActivity(), mContext, mSession, mRequestQueue, new BigDataLinkHandler() {
            @Override
            public void RunCompleted(String url) {
                if (isActive) {
                    Intent intent = new Intent(getActivity(), BigDataViewer.class);
                    intent.putExtra(WebContentFragment.URL, url);
                    startActivity(intent);
                }
            }
        });
    }

}
