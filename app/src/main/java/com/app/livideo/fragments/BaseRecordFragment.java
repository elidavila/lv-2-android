package com.app.livideo.fragments;

import android.support.v4.app.Fragment;

import java.io.File;

/**
 * Created by MWheeler on 5/26/2016.
 */
public abstract class BaseRecordFragment extends Fragment {
    abstract public void onStopRecording(final File videoFile);
    abstract public void stopRecording();
}
