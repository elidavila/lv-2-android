package com.app.livideo.fragments;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.hardware.Camera;
import android.location.Address;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.app.livideo.R;
import com.app.livideo.activities.ArtistRecordActivity;
import com.app.livideo.activities.MainMenuActivity;
import com.app.livideo.api.APICalls;
import com.app.livideo.dialogs.CustomAlertDialog;
import com.app.livideo.dialogs.LiveStreamWarningDialog;
import com.app.livideo.dialogs.TwitterViewDialog;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.Stream;
import com.app.livideo.models.translators.StreamTranslator;
import com.app.livideo.models.translators.ArtistUploadVideoTranslator;
import com.app.livideo.utils.AndroidUtils;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.ArtistVideoShareObj;
import com.app.livideo.utils.Connectivity;
import com.app.livideo.utils.LocationUtils;
import com.app.livideo.utils.Preferences;
import com.app.livideo.utils.ScreenDensityUtil;
import com.app.livideo.utils.SoftKeyboard;
import com.app.livideo.views.CameraCapturePreview;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.fabric.sdk.android.services.network.UrlUtils;

public class ArtistRecordHeadlineFragment extends Fragment {

    @Bind(R.id.record_headline_location) public CheckBox mLocationShare;
    @Bind(R.id.record_headline_twitter) CheckBox mTwitterShare;
    @Bind(R.id.record_headline_surface_view) SurfaceView mCameraView;
    @Bind(R.id.record_headline_frame_layout) FrameLayout mBackgroundView;
    @Bind(R.id.lock_video_button) CheckBox mLockVideo;
    @Bind(R.id.artist_record_preview_footer) LinearLayout mFooter;
    @Bind(R.id.artist_record_preview_root) ViewGroup mRoot;
    @Bind(R.id.record_headline_edit_text) EditText mHeadlineView;
    @Bind(R.id.go_live_button) CardView liveButton;
    @Inject RequestQueue mRequestQueue;
    @Inject LVSession mSession;
    @Inject Context mContext;
    public Address mLastLocation;
    private CameraCapturePreview mPreview;
    private Camera mCamera;
    private Bundle mArgs;
    public SoftKeyboard softKeyboard;
    boolean isLiveAvailable;
    private final int TWITER_CODE = 2;

    public boolean isKeyboardOpen() {
        return keyboardIsOpen;
    }

    private boolean keyboardIsOpen = false;

    public ArtistRecordHeadlineFragment() {
        // Required empty public constructor
    }

    public void setArgs(Bundle mArgs) {
        this.mArgs.putAll(mArgs);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View contentView = inflater.inflate(R.layout.fragment_artist_record_headline, container, false);
        ButterKnife.bind(this, contentView);
        LVInject.inject(this);
        initialize();
        checkUploadSpeed();
        return contentView;
    }

    private void initialize() {
        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mArgs = new Bundle();

        // Reset go live to false
        Preferences.setPreference(ArtistRecordActivity.SHARED_PREF_IS_GOING_LIVE, false, mContext);
        mPreview = new CameraCapturePreview(mContext, mCameraView);
        mPreview.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mBackgroundView.addView(mPreview);
        mPreview.setKeepScreenOn(true);
        String defaultHeadline = getResources().getString(R.string.default_hint_string);
        mHeadlineView.setHint(defaultHeadline);
        mHeadlineView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!keyboardIsOpen) {
                    softKeyboardShow();
                }
            }
        });

        mHeadlineView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    softKeyboardHide();
                    softKeyboard.closeSoftKeyboard();
                }
                return false;
            }
        });

        InputMethodManager im = (InputMethodManager) getActivity().getSystemService(Service.INPUT_METHOD_SERVICE);
        softKeyboard = new SoftKeyboard(mRoot, im);
        softKeyboard.setSoftKeyboardCallback(new SoftKeyboard.SoftKeyboardChanged() {
            @Override
            public void onSoftKeyboardHide() {
                if (getActivity() != null) {
                    (getActivity()).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                                    RelativeLayout.LayoutParams.MATCH_PARENT,
                                    RelativeLayout.LayoutParams.WRAP_CONTENT
                            );
                            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                            params.removeRule(RelativeLayout.BELOW);
                            params.setMargins(0, 0, 0, 0);
                            mFooter.setLayoutParams(params);
                        }
                    });
                }
            }

            @Override
            public void onSoftKeyboardShow() {
                if (getActivity() != null) {
                    keyboardIsOpen = true;
                    (getActivity()).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                                    RelativeLayout.LayoutParams.MATCH_PARENT,
                                    RelativeLayout.LayoutParams.WRAP_CONTENT
                            );
                            params.removeRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                            params.addRule(RelativeLayout.BELOW, R.id.record_headline_edit_text);
                            params.setMargins(0, ScreenDensityUtil.convertPixelsToDp(16, mContext), 0, 0);
                            mFooter.setLayoutParams(params);
                        }
                    });
                }
            }
        });
    }

    public void softKeyboardHide() {
        if (getActivity() != null) {
            keyboardIsOpen = false;
            (getActivity()).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                            RelativeLayout.LayoutParams.MATCH_PARENT,
                            RelativeLayout.LayoutParams.WRAP_CONTENT
                    );
                    params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                    params.removeRule(RelativeLayout.BELOW);
                    params.setMargins(0, 0, 0, 0);
                    mFooter.setLayoutParams(params);
                }
            });
        }
    }

    public void softKeyboardShow() {
        if (getActivity() != null) {
            keyboardIsOpen = true;
            (getActivity()).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                            RelativeLayout.LayoutParams.MATCH_PARENT,
                            RelativeLayout.LayoutParams.WRAP_CONTENT
                    );
                    params.removeRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                    params.addRule(RelativeLayout.BELOW, R.id.record_headline_edit_text);
                    params.setMargins(0, ScreenDensityUtil.convertPixelsToDp(16, mContext), 0, 0);
                    mFooter.setLayoutParams(params);
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getArguments() != null) {
            mLocationShare.setChecked(getArguments().getBoolean(ArtistRecordActivity.SHARE_LOCATION));
            mTwitterShare.setChecked(getArguments().getBoolean(ArtistRecordActivity.SHARE_TWITTER));
            mLockVideo.setChecked(getArguments().getBoolean(ArtistUploadVideoTranslator.IS_PUBLIC));
        }

        if (Camera.getNumberOfCameras() > 0) {
            try {
                mCamera = Camera.open(Preferences.getPreference(AppConstants.LAST_CAMERA_USED, getActivity(), getFrontFacingCamera()));
                mCamera.startPreview();
                mPreview.resetCamera(mCamera);
            } catch (RuntimeException ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onPause() {
        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
        }
        super.onPause();
    }

    @OnClick(R.id.pre_record_button)
    protected void onPreRecordClick() {
        Preferences.setPreference(ArtistRecordActivity.SHARED_PREF_IS_GOING_LIVE, false, mContext);
        fillArguments();
        mArgs.putInt(ArtistRecordActivity.RECORDING_TYPE, ArtistRecordActivity.RECORD_TYPE_SCHEDULE);
        mArgs.putBoolean(ArtistUploadVideoTranslator.IS_PUBLIC, (!mLockVideo.isChecked()));
        ((ArtistRecordActivity) getActivity()).switchToRecordScreen(mArgs);
    }

    private void fillArguments() {
        if (mHeadlineView.getText().toString().isEmpty()) {
            mArgs.putString(AppConstants.STREAM_TITLE_LABEL, mHeadlineView.getHint().toString());
        } else {
            mArgs.putString(AppConstants.STREAM_TITLE_LABEL, mHeadlineView.getText().toString());
        }
        mArgs.putBoolean(ArtistRecordActivity.SHARE_LOCATION, mLocationShare.isChecked());
        if (mLocationShare.isChecked() && mLastLocation != null)
            mArgs.putString(ArtistRecordActivity.LOCATION, mLastLocation.getSubLocality());
        mArgs.putBoolean(ArtistRecordActivity.SHARE_TWITTER, mTwitterShare.isChecked());
    }

    @OnClick(R.id.go_live_button)
    public void onGoLiveClick() {
        if (!isLiveAvailable) {
            CustomAlertDialog dialog = new CustomAlertDialog(getActivity(), mContext.getString(R.string.internet_too_slow_for_live), false);
            dialog.show();
            return;
        }

        if (Preferences.getPreference(AppConstants.SHOW_GO_LIVE_WARNING, getContext(), true)) {
            Preferences.setPreference(AppConstants.SHOW_GO_LIVE_WARNING, false, getContext());
            LiveStreamWarningDialog dialog = new LiveStreamWarningDialog(getActivity(), this);
            dialog.show();
        } else {
            Preferences.setPreference(ArtistRecordActivity.SHARED_PREF_IS_GOING_LIVE, true, mContext);
            goLive();
        }
    }

    private void shareToTwitter(String link) {
        String streamTitle = (mHeadlineView.getText().toString().isEmpty())
                ? mHeadlineView.getHint().toString()    // if
                : mHeadlineView.getText().toString();   // else

        Intent intent = getTwitterIntent(streamTitle , link);
        if (intent != null) {
            getActivity().startActivityForResult(intent, TWITER_CODE);
        } else {
            launchTwitterDialog(streamTitle, link);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == TWITER_CODE){
            if(resultCode == Activity.RESULT_OK){
                Preferences.setPreference(ArtistRecordActivity.SHARED_PREF_IS_GOING_LIVE, true, mContext);
                ((ArtistRecordActivity) getActivity()).switchToRecordScreen(mArgs);
                mBackgroundView.removeView(mPreview);
                mPreview = null;
            }
        }
    }

    private Intent getTwitterIntent(String streamTitle, String link) {
        Intent tweetIntent = new Intent();
        tweetIntent.setType("text/plain");
        tweetIntent.putExtra(Intent.EXTRA_TEXT, getActivity().getString(R.string.out_tweet).replace("%%TITTLE%%", streamTitle).replace("%%LINK%%", link));
        final PackageManager packageManager = getActivity().getPackageManager();
        List<ResolveInfo> list = packageManager.queryIntentActivities(tweetIntent, PackageManager.MATCH_DEFAULT_ONLY);
        Iterator iterator = list.iterator();
        ResolveInfo resolveInfo;
        do {
            if (!iterator.hasNext()) {
                return null;
            }
            resolveInfo = (ResolveInfo) iterator.next();
        } while (!resolveInfo.activityInfo.packageName.startsWith("com.twitter.android"));

        tweetIntent.setClassName(resolveInfo.activityInfo.packageName, resolveInfo.activityInfo.name);
        return tweetIntent;
    }

    public void launchTwitterDialog(String streamTitle, String link) {
        String url = String.format("https://twitter.com/intent/tweet?text=%s&url=%s",
                UrlUtils.urlEncode(getActivity().getString(R.string.out_tweet).replace("%%TITTLE%%", streamTitle).replace("%%LINK%%", link)),
                UrlUtils.urlEncode("lvdi.io"));
        TwitterViewDialog dialog = new TwitterViewDialog(getActivity(), url);
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                Preferences.setPreference(ArtistRecordActivity.SHARED_PREF_IS_GOING_LIVE, true, mContext);
                ((ArtistRecordActivity) getActivity()).switchToRecordScreen(mArgs);
                mBackgroundView.removeView(mPreview);
                mPreview = null;
            }
        });
        dialog.show();
    }

    public void goLive() {
        fillArguments();
        File videoFile = new File(Preferences.getPreference(ArtistRecordActivity.SHARED_PREF_DATA_KEY_VIDEO_FILE_PATH, mContext, ""));
        ArtistVideoShareObj obj = new ArtistVideoShareObj(
                mArgs.getString(AppConstants.STREAM_TITLE_LABEL),
                videoFile,
                mArgs.getBoolean(ArtistRecordActivity.SHARE_LOCATION),
                mArgs.getBoolean(ArtistRecordActivity.SHARE_TWITTER),
                !mLockVideo.isChecked(),
                getActivity(),
                new ArtistVideoShareObj.OnSetupFinishedListener() {
                    @Override
                    public void onSetupFinished(ArtistVideoShareObj shareObj) {
                        sendUploadRequest(shareObj);
                    }
                }
        );
    }

    private void sendUploadRequest(final ArtistVideoShareObj shareObj) {
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
        sdf.setTimeZone(TimeZone.getTimeZone("gmt"));

        //setup JSON data
        JSONObject jsonObjectStreamInfo = new JSONObject();
        try {
            jsonObjectStreamInfo.put(ArtistUploadVideoTranslator.PARAM_STREAM_JSON_KEY_ARTIST_ID, mSession.getUser().getUserID());
            jsonObjectStreamInfo.put(ArtistUploadVideoTranslator.PARAM_STREAM_JSON_KEY_HEADLINE, shareObj.getStreamTitle());
            jsonObjectStreamInfo.put(ArtistUploadVideoTranslator.PARAM_STREAM_JSON_KEY_TIME, sdf.format(new Date((new Date()).getTime())));
            if (shareObj.getIsPublic()) {
                jsonObjectStreamInfo.put(ArtistUploadVideoTranslator.IS_PUBLIC, "1");
            }
        } catch (Exception e) {
            Crashlytics.logException(e);
        }

        //send API request
        APICalls.requestLiveStream(mSession, getActivity(), mContext,
                mSession.getUser().getUserID(),
                mSession.getUser().getDeviceID(),
                jsonObjectStreamInfo,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Stream newStream = StreamTranslator.getStream(jsonObject);
                        mArgs.putString(Stream.Keys.STREAM_ID, newStream.getStreamId());
                        mArgs.putInt(ArtistRecordActivity.RECORDING_TYPE, ArtistRecordActivity.RECORD_TYPE_GO_LIVE);
                        if (getActivity() != null){
                            if (mTwitterShare.isChecked()) {
                                shareToTwitter(newStream.getShortURI());
                            } else {
                                Preferences.setPreference(ArtistRecordActivity.SHARED_PREF_IS_GOING_LIVE, true, mContext);
                                ((ArtistRecordActivity) getActivity()).switchToRecordScreen(mArgs);
                                mBackgroundView.removeView(mPreview);
                                mPreview = null;
                            }

                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(this.getClass().getName(), "requestNewStream:Error " + error.getMessage());
                    }
                },
                mRequestQueue
        );
    }

    @OnClick(R.id.record_headline_location)
    public void setLocationSelected() {
        if (!mLocationShare.isChecked()) {
            if (LocationUtils.locationServicesAvailable(mContext)) {
                // Get Current Location
                new LocationUtils(getActivity(), new LocationUtils.OnLocationListener() {
                    @Override
                    public void onGetLocation(Location location, Address address, LatLng latitudeLongitude) {
                        if (address != null) {
                            mLastLocation = address;
                        }
                    }

                    @Override
                    public void onErrorWhileGettingLocation(String message) {
                        if (getActivity() != null)
                            AndroidUtils.showShortToast(message, getActivity());
                    }
                });
            } else {
                LocationUtils.promptEnableLocation(getActivity());
            }
        }
    }

    @OnClick(R.id.record_headline_x)
    protected void onExitClick() {
        if (getActivity() == null)
            return;

        if (getActivity().getIntent().getBooleanExtra(AppConstants.FROM_SPLASH, true)) {
            startActivity(new Intent(getContext(), MainMenuActivity.class));
            getActivity().finish();
        } else {
            getActivity().onBackPressed();
        }
    }

    public int getFrontFacingCamera() {
        int cameraId = -1;
        // Search for the front facing camera
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                cameraId = i;
                break;
            }
        }
        return cameraId;
    }

    public void checkUploadSpeed() {
        if (Connectivity.isConnectedFast(mContext)) {
            isLiveAvailable = true;
            liveButton.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.livideo_red));
        } else {
            isLiveAvailable = false;
            liveButton.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.livideo_trans_grey));
        }
    }
}
