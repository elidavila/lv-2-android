package com.app.livideo.fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.app.livideo.R;
import com.app.livideo.activities.MainMenuActivity;
import com.app.livideo.activities.WebGeneratedActivity;
import com.app.livideo.api.APICalls;
import com.app.livideo.dialogs.ImageUploadFromDialog;
import com.app.livideo.dialogs.InviteContactsDialog;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.UserImage;
import com.app.livideo.utils.AndroidUtils;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.Constants;
import com.app.livideo.utils.LVUtils;
import com.app.livideo.utils.PicassoCircleTransform;
import com.app.livideo.utils.Preferences;
import com.app.livideo.utils.ScreenDensityUtil;
import com.app.livideo.utils.SessionUtils;
import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class ViewerSettingsFragment extends Fragment {

    @Bind(R.id.left_drawer) View rootView;
    @Bind(R.id.display_name_text) TextView displayName;
    @Bind(R.id.display_image_view) ImageView displayImage;

    @Inject Context mContext;
    @Inject LVSession mSession;
    @Inject RequestQueue mRequestQueue;

    boolean isActive = true;
    MainMenuActivity mParent;
    int chatImageSize;

    public ViewerSettingsFragment() {
        // Required empty public constructor
    }

    @OnClick(R.id.blocked_users)
    protected void onBlockedUsersClick() {
        mParent.displayBlockedUsers();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_viewer_settings, container, false);
        ButterKnife.bind(this, view);
        LVInject.inject(this);
        mParent = (MainMenuActivity) getActivity();
        isActive = true;
        displayName.setText(mSession.getUser().getDisplayName());
        chatImageSize = ScreenDensityUtil.getScreenHeightPixels(mContext, 10);
        setDisplayImage();
        return view;
    }

    public void setDisplayImage(){
        displayImage.setMinimumWidth(chatImageSize);
        displayImage.setMinimumHeight(chatImageSize);
        List<UserImage> images = mSession.getUser().getPictures();
        if (!images.isEmpty()) {
            Picasso.with(mContext)
                    .load(images.get(images.size() -1).getUrl())
                    .resize(chatImageSize, chatImageSize)
                    .transform(new PicassoCircleTransform())
                    .into(displayImage);
        } else {
            Picasso.with(mContext)
                    .load(R.drawable.profile)
                    .resize(chatImageSize, chatImageSize)
                    .transform(new PicassoCircleTransform())
                    .into(displayImage);
        }
    }

    @OnClick(R.id.log_out)
    public void logOut() {
        LVUtils.logoutUser(mParent, mContext, mSession, mRequestQueue);
    }

    @OnClick(R.id.display_image_view)
    public void uploadImage() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            AndroidUtils.requestStoragePermission(getActivity(), rootView);
        } else {
            ImageUploadFromDialog dialog = new ImageUploadFromDialog(getActivity());
            dialog.show();
        }
    }

    @OnClick(R.id.invite_friends)
    public void launchFreindInvite() {
        final InviteContactsDialog dialog = new InviteContactsDialog(getActivity());
        dialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        isActive = true;
    }

    @Override
    public void onPause() {
        super.onPause();
        isActive = false;
    }

    @OnClick(R.id.help_tab)
    public void navigateToFAQ() {
        if (isActive) {
            Intent intent = new Intent(getActivity(), WebGeneratedActivity.class);
            intent.putExtra(Constants.USER_TYPE_KEY, Constants.VIEWER_USER);
            intent.putExtra(AppConstants.KEY_WEB_TYPE, AppConstants.WEB_TYPE_FAQ);
            startActivity(intent);
        }
    }

    @OnClick(R.id.terms_and_conditions)
    public void navigateToTerms() {
        if (isActive) {
            Intent intent = new Intent(getActivity(), WebGeneratedActivity.class);
            intent.putExtra(Constants.USER_TYPE_KEY, Constants.VIEWER_USER);
            intent.putExtra(AppConstants.KEY_WEB_TYPE, AppConstants.WEB_TYPE_TERMS);
            startActivity(intent);
        }
    }

    @OnClick(R.id.privacy_policy)
    public void navigateToPrivacy() {
        if (isActive) {
            Intent intent = new Intent(getActivity(), WebGeneratedActivity.class);
            intent.putExtra(Constants.USER_TYPE_KEY, Constants.VIEWER_USER);
            intent.putExtra(AppConstants.KEY_WEB_TYPE, AppConstants.WEB_TYPE_PRIVACY);
            startActivity(intent);
        }
    }

}
