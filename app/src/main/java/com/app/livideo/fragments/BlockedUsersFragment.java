package com.app.livideo.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.app.livideo.R;
import com.app.livideo.activities.MainMenuActivity;
import com.app.livideo.adapters.BlockedUsersAdapter;
import com.app.livideo.api.APICalls;
import com.app.livideo.interfaces.UserListHandler;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.User;
import com.app.livideo.models.validators.UserValidator;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Eli on 5/5/2016.
 */
public class BlockedUsersFragment extends Fragment {

    @Inject Context mContext;
    @Inject LVSession mSession;
    @Inject RequestQueue mRequestQueue;

    @Bind(R.id.blocked_users_recycler_view) RecyclerView blocked_users_recycler_view;
    @Bind(R.id.blocked_users_progress_bar) ProgressBar mProgressBar;
    @Bind(R.id.blocked_users_no_users) TextView no_blocked_users_view;

    MainMenuActivity mParent;
    BlockedUsersAdapter mAdapter;

    public BlockedUsersFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_blocked_users, container, false);
        ButterKnife.bind(this, mView);
        LVInject.inject(this);
        mParent = (MainMenuActivity) getActivity();

        // Layout
        final LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        blocked_users_recycler_view.setLayoutManager(layoutManager);

        // View
        requestBlockedUsers();
        return mView;
    }

    @OnClick(R.id.header_left_card_view)
    protected void onCloseDrawerClick() {
        mParent.displaySettingsPageOne();
    }

    private void requestBlockedUsers() {
        mProgressBar.setVisibility(View.VISIBLE);
        APICalls.requestBlockedUsers(mParent, mContext, mSession, mRequestQueue, new UserListHandler() {
            @Override
            public void RunCompleted(int code, List<User> users) {
                mProgressBar.setVisibility(View.GONE);
                if (code == com.app.livideo.utils.Constants.USERCODE_SUCCESS && UserValidator.isValid(users)) {
                    no_blocked_users_view.setVisibility(View.GONE);
                    addBlockedUsersView(users);
                } else {
                    no_blocked_users_view.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void addBlockedUsersView(List<User> users) {
        mAdapter = new BlockedUsersAdapter(users, mContext, mBlockedUserClickedListener);
        blocked_users_recycler_view.setAdapter(mAdapter);
    }

    BlockedUsersAdapter.BlockedUserClickedListener mBlockedUserClickedListener = new BlockedUsersAdapter.BlockedUserClickedListener() {
        @Override
        public void onBlockedUserToggleClick(User blockedUser) {
            APICalls.requestUnblockUser(blockedUser.getUserID(), mParent, mContext, mSession, mRequestQueue, null);
        }
    };
}
