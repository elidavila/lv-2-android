package com.app.livideo.fragments;

import android.app.Activity;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.app.livideo.R;
import com.app.livideo.dialogs.InviteContactsDialog;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WebContentFragment extends Fragment {
    public static final String URL = "url";
    private static final String SHOW = "boolean";
    @Bind(R.id.web_container) WebView webContainer;
    @Bind(R.id.webcover) View webcover;
    private String mURL;
    private boolean display;

    public WebContentFragment() {
        // Required empty public constructor
    }

    public static WebContentFragment newSingleType(String param1, boolean showAll) {
        WebContentFragment fragment = new WebContentFragment();
        Bundle args = new Bundle();
        args.putString(URL, param1);
        args.putBoolean(SHOW, showAll);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            try {
                mURL = getArguments().getString(URL);
                display = getArguments().getBoolean(SHOW, true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        RelativeLayout relativeLayout = (RelativeLayout) inflater.inflate(R.layout.fragment_web_content, container, false);
        ButterKnife.bind(this, relativeLayout);
        CookieManager.getInstance().setAcceptCookie(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.setAcceptThirdPartyCookies(webContainer, true);
        }

        webContainer.getSettings().setJavaScriptEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webContainer.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_COMPATIBILITY_MODE);
        }
        webContainer.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        webContainer.addJavascriptInterface(new MyJavaScriptInterface(getActivity()), "Android");

        if (!mURL.isEmpty()) {
            webContainer.loadUrl(mURL);
        }

        webcover.postDelayed(new Runnable() {
            public void run() {
                webcover.setVisibility(View.GONE);
            }
        }, 1000);
        return relativeLayout;
    }

    @OnClick(R.id.record_headline_x)
    protected void onXIconClick() {
        if (getFragmentManager().getBackStackEntryCount() == 0) {
            getActivity().onBackPressed();
        } else {
            getFragmentManager().popBackStack();
        }
    }

//    @OnClick(R.id.both_header_right_icon)
//    protected void onShareClick() {
//        InviteContactsDialog dialog = new InviteContactsDialog(getActivity(), mURL);
//        dialog.show();
//    }

    public class MyJavaScriptInterface {
        Activity activity;

        MyJavaScriptInterface(Activity activity) {
            this.activity = activity;
        }

        @JavascriptInterface
        public void closeActivity() {
            activity.finish();
        }
    }
}
