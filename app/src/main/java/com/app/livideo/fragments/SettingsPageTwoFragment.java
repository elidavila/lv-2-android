package com.app.livideo.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.app.livideo.R;
import com.app.livideo.activities.MainMenuActivity;
import com.app.livideo.activities.WebGeneratedActivity;
import com.app.livideo.api.APICalls;
import com.app.livideo.dialogs.ImageUploadFromDialog;
import com.app.livideo.interfaces.DataAvailabilityHandler;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.UserImage;
import com.app.livideo.models.validators.UserValidator;
import com.app.livideo.utils.AndroidUtils;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.Constants;
import com.app.livideo.utils.PicassoCircleTransform;
import com.app.livideo.utils.Preferences;
import com.app.livideo.utils.ScreenDensityUtil;
import com.app.livideo.utils.ViewUtils;
import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

public class SettingsPageTwoFragment extends Fragment {

    @Bind(R.id.settings_camera_switch) SwitchCompat cameraSwitch;
    @Bind(R.id.content_rating_switch) SwitchCompat contentSwitch;
    @Bind(R.id.display_name_edit) TextView editButton;
    @Bind(R.id.display_name_text) TextView displayName;
    @Bind(R.id.profile) ImageView displayImage;
    @Bind(R.id.name_edit_box) EditText displayBox;
    @Bind(R.id.category_picker) NumberPicker categoryPicker;
    @Bind(R.id.category) TextView categoryTitle;
    @Bind(R.id.category_edit) TextView categoryEdit;
    @Bind(R.id.setting2_root) View rootView;
    @Bind(R.id.short_uri_edit_box) EditText shortURIEditBox;
    @Bind(R.id.short_uri_text) TextView urlShortLink;
    @Bind(R.id.short_uri_edit) TextView urlEdit;

    @Inject Context mContext;
    @Inject LVSession mSession;
    @Inject RequestQueue mRequestQueue;

    MainMenuActivity mParent;
    boolean disPlayNameSave = false, artistTypeState = false, isActive = true;
    int chatImageSize;
    private static final String IS_NOT_ALL_AGES = "0";

    public SettingsPageTwoFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings_page_two, container, false);
        ButterKnife.bind(this, view);
        LVInject.inject(this);
        isActive = true;
        mParent = (MainMenuActivity) getActivity();
        displayName.setText(mSession.getUser().getDisplayName());
        urlShortLink.setText(mSession.getUser().getShortURL());
        urlShortLink.setPaintFlags(urlShortLink.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        int cameraFirst = Preferences.getPreference(Constants.CAMERA_TYPE_KEY, getActivity(), -1);
        cameraSwitch.setChecked(cameraFirst == Constants.OPEN_CAMERA_FIRST);
        if (mSession != null && UserValidator.isValid(mSession.getUser()) && mSession.getUser().getIsAllAges() != null
                && mSession.getUser().getIsAllAges().equalsIgnoreCase(IS_NOT_ALL_AGES)) {
            contentSwitch.setChecked(true);
            contentSwitch.setEnabled(false);
        } else {
            contentSwitch.setChecked(false);
        }

        chatImageSize = ScreenDensityUtil.getScreenHeightPixels(mContext, 10);
        List<UserImage> images = mSession.getUser().getPictures();

        if (!images.isEmpty()) {
            Picasso.with(mContext)
                    .load(images.get(mSession.getUser().getPictures().size() - 1).getUrl())
                    .resize(chatImageSize, chatImageSize)
                    .transform(new PicassoCircleTransform())
                    .into(displayImage);
        } else {
            Picasso.with(mContext)
                    .load(R.drawable.profile)
                    .resize(chatImageSize, chatImageSize)
                    .transform(new PicassoCircleTransform())
                    .into(displayImage);
        }

        addCategoryPickerView();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        isActive = true;
    }

    @Override
    public void onPause() {
        super.onPause();
        isActive = false;
    }

    public void setDisplayImage() {
        List<UserImage> images = mSession.getUser().getPictures();
        if (!images.isEmpty()) {
            Picasso.with(mContext)
                    .load(images.get(images.size() - 1).getUrl())
                    .resize(chatImageSize, chatImageSize)
                    .transform(new PicassoCircleTransform())
                    .into(displayImage);
        } else {
            Picasso.with(mContext)
                    .load(R.drawable.profile)
                    .resize(chatImageSize, chatImageSize)
                    .transform(new PicassoCircleTransform())
                    .into(displayImage);
        }
    }

    private void addCategoryPickerView() {
        //Artist Types Picker
        categoryPicker.setMinValue(0);
        categoryPicker.setMaxValue(mSession.getArtistTypesList().size() - 1);
        categoryPicker.setWrapSelectorWheel(false);
        categoryPicker.setDisplayedValues(mSession.getArtistTypesList().toArray(new String[mSession.getArtistTypesList().size()]));
        ViewUtils.modifyPickerDivider(categoryPicker, Color.BLACK, 2);
        categoryTitle.setText(mSession.getUser().getArtistType());
    }

    @OnClick(R.id.back_button)
    public void backButton() {
        mParent.displaySettingsPageOne();
    }

    @OnClick(R.id.profile)
    public void launceProfile() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            AndroidUtils.requestStoragePermission(getActivity(), rootView);
        } else {
            ImageUploadFromDialog dialog = new ImageUploadFromDialog(getActivity());
            dialog.show();
        }
    }

    @OnCheckedChanged(R.id.settings_camera_switch)
    public void cameraSwitch(boolean checked) {
        if (checked)
            Preferences.setPreference(Constants.CAMERA_TYPE_KEY, Constants.OPEN_CAMERA_FIRST, getActivity());
        else
            Preferences.setPreference(Constants.CAMERA_TYPE_KEY, Constants.DONT_OPEN_CAMERA_FIRST, getActivity());
    }

    @OnCheckedChanged(R.id.content_rating_switch)
    public void contentSwitch(boolean checked) {
        APICalls.requestSetArtistData(getActivity(), mContext, mSession.getUser().getArtistType(),
                mSession.getUser().getEmail(), String.valueOf(checked), mSession.getUser().getShortURL(), mSession, mRequestQueue, new DataAvailabilityHandler() {
                    @Override
                    public void RunCompleted(boolean isAvailable) {
                    }
                });
    }

    @OnClick(R.id.display_name_edit)
    public void editDisplayName() {
        if (disPlayNameSave) {
            if (!displayBox.getText().toString().isEmpty()) {
                displayBox.setVisibility(View.GONE);
                editButton.setText(R.string.actionbar_edit_string);
                disPlayNameSave = false;
                APICalls.requestSetUserData(getActivity(), mSession, displayBox.getText().toString().trim(), mRequestQueue, mContext, new DataAvailabilityHandler() {
                    @Override
                    public void RunCompleted(boolean isAvailable) {
                        if (isAvailable) {
                            displayName.setText(mSession.getUser().getDisplayName());
                        }
                    }
                });
                displayBox.setText("");
            } else {
                displayBox.setVisibility(View.GONE);
                editButton.setText(R.string.actionbar_edit_string);
                disPlayNameSave = false;
            }
        } else {
            displayBox.setVisibility(View.VISIBLE);
            editButton.setText(R.string.save_string);
            disPlayNameSave = true;
        }
        hideKeyboardFrom(getActivity(), displayBox);
    }

    public static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @OnClick(R.id.category_edit)
    public void editCategoryName() {
        if (artistTypeState) {
            categoryPicker.setVisibility(View.GONE);
            categoryEdit.setText(R.string.actionbar_edit_string);
            artistTypeState = false;
            APICalls.requestSetArtistData(getActivity(), mContext, mSession.getArtistTypesList().get(categoryPicker.getValue()),
                    mSession.getUser().getEmail(), mSession.getUser().getIsAllAges(), mSession.getUser().getShortURL(), mSession, mRequestQueue, new DataAvailabilityHandler() {
                        @Override
                        public void RunCompleted(boolean isAvailable) {
                            categoryTitle.setText(mSession.getUser().getArtistType());
                        }
                    });
            addCategoryPickerView();
//            setCategory();
        } else {
            categoryPicker.setVisibility(View.VISIBLE);
            categoryEdit.setText(R.string.save_string);
            artistTypeState = true;
        }
    }

    @OnClick(R.id.short_uri_edit)
    public void editShortURI() {
        if (artistTypeState) {
            if (shortURIEditBox.getText().length() >= 1){
                shortURIEditBox.setVisibility(View.GONE);
                urlEdit.setText(R.string.actionbar_edit_string);
                artistTypeState = false;
                APICalls.requestSetArtistData(getActivity(), mContext, mSession.getArtistTypesList().get(categoryPicker.getValue()),
                        mSession.getUser().getEmail(), mSession.getUser().getIsAllAges(), generateNewShortURL(shortURIEditBox.getText().toString()) , mSession, mRequestQueue, new DataAvailabilityHandler() {
                            @Override
                            public void RunCompleted(boolean isAvailable) {
                                urlShortLink.setText(mSession.getUser().getShortURL());
                            }
                        });
            } else {
                Toast.makeText(mContext, R.string.char_min, Toast.LENGTH_SHORT).show();
            }
        } else {
            shortURIEditBox.setVisibility(View.VISIBLE);
            urlEdit.setText(R.string.save_string);
            artistTypeState = true;
        }
    }

    private String generateNewShortURL(String shortCode){
        return "https://lvdi.io/d/" +shortCode;
    }

    @OnClick(R.id.short_uri_text)
    public void coppyToClipboard(){
        if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
            android.text.ClipboardManager clipboard = (android.text.ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
            clipboard.setText(urlShortLink.getText().toString());
        } else {
            android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
            android.content.ClipData clip = android.content.ClipData.newPlainText("Copied Text", urlShortLink.getText().toString());
            clipboard.setPrimaryClip(clip);
        }
        Toast.makeText(mContext, R.string.copied_clip, Toast.LENGTH_SHORT).show();
    }


    @OnClick(R.id.help_tab)
    public void navigateToFAQ() {
        if (isActive) {
            Intent intent = new Intent(getActivity(), WebGeneratedActivity.class);
            intent.putExtra(Constants.USER_TYPE_KEY, Constants.BROADCASTER_USER);
            intent.putExtra(AppConstants.KEY_WEB_TYPE, AppConstants.WEB_TYPE_FAQ);
            startActivity(intent);
        }
    }

    @OnClick(R.id.terms_and_conditions)
    public void navigateToTerms() {
        if (isActive) {
            Intent intent = new Intent(getActivity(), WebGeneratedActivity.class);
            intent.putExtra(Constants.USER_TYPE_KEY, Constants.BROADCASTER_USER);
            intent.putExtra(AppConstants.KEY_WEB_TYPE, AppConstants.WEB_TYPE_TERMS);
            startActivity(intent);
        }
    }

    @OnClick(R.id.privacy_policy)
    public void navigateToPrivacy() {
        if (isActive) {
            Intent intent = new Intent(getActivity(), WebGeneratedActivity.class);
            intent.putExtra(Constants.USER_TYPE_KEY, Constants.BROADCASTER_USER);
            intent.putExtra(AppConstants.KEY_WEB_TYPE, AppConstants.WEB_TYPE_PRIVACY);
            startActivity(intent);
        }
    }

}
