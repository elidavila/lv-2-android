package com.app.livideo.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.app.livideo.R;
import com.app.livideo.activities.ChatActivity;
import com.app.livideo.adapters.WhispersAdapter;
import com.app.livideo.api.APICalls;
import com.app.livideo.interfaces.WhisperRequestHandler;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.User;
import com.app.livideo.models.WhisperRequest;
import com.app.livideo.models.validators.UserValidator;
import com.app.livideo.models.validators.WhisperValidator;
import com.app.livideo.utils.Constants;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WhispersFragment extends Fragment {

    @Inject Context mContext;
    @Inject LVSession mSession;
    @Inject RequestQueue mRequestQueue;

    @Bind(R.id.ghost_chatting_recycler_view) RecyclerView whisper_recycler_view;
    @Bind(R.id.ghost_chatting_progress_bar) ProgressBar mProgressBar;
    @Bind(R.id.ghost_chatting_no_whispers_view) TextView no_whispers_view;

    ChatActivity mParent;
    WhispersAdapter mAdapter;
    List<WhisperRequest> mWhisperRequests;
    public boolean shouldStopUpdatingWhispers = false;
    public boolean isUpdating = false;

    public WhispersFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_whispers, container, false);
        ButterKnife.bind(this, mView);
        LVInject.inject(this);
        mParent = (ChatActivity) getActivity();
        shouldStopUpdatingWhispers = false;
        isUpdating = false;

        // Layout
        final LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        whisper_recycler_view.setLayoutManager(layoutManager);

        // View
        requestWhispers(false);
        return mView;
    }

    private void requestWhispers(final boolean isUpdating) {
        if (!isUpdating)
            mProgressBar.setVisibility(View.VISIBLE);
        APICalls.requestWhispers(mParent, mContext, mSession, mRequestQueue, new WhisperRequestHandler() {
            @Override
            public void RunCompleted(int code, List<WhisperRequest> whisperRequests) {
                mProgressBar.setVisibility(View.GONE);
                if (code == Constants.USERCODE_SUCCESS && WhisperValidator.isValid(whisperRequests)) {
                    no_whispers_view.setVisibility(View.GONE);
                    if (isUpdating && WhisperValidator.isValid(mWhisperRequests)) {
                        if (!mWhisperRequests.equals(whisperRequests))
                            updateWhisperView(whisperRequests);
                    } else {
                        addWhispersView(whisperRequests);
                    }
                } else {
                    no_whispers_view.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private boolean isReallyDifferent(List<WhisperRequest> whisperRequests) {
        if (!UserValidator.isValid(mSession.getCurrentPrivateChatUser()))
            return true;

        for (WhisperRequest whisperRequest : whisperRequests) {
            if (!whisperRequest.getUnreadCount().equalsIgnoreCase("0")) {
                return true;
            }
        }
        return false;
    }

    private void updateWhisperView(List<WhisperRequest> whisperRequests) {
        mParent.handleGhostIconAnimation(isReallyDifferent(whisperRequests));
        mWhisperRequests = whisperRequests;
        mAdapter.bind(mWhisperRequests);
    }

    private void addWhispersView(List<WhisperRequest> whisperRequests) {
        mWhisperRequests = whisperRequests;
        if (isVisible()) {
            mAdapter = new WhispersAdapter(whisperRequests, mContext, mWhisperClickedListener);
            whisper_recycler_view.setAdapter(mAdapter);
            for (WhisperRequest whisperRequest : whisperRequests) {
                if (Integer.valueOf(whisperRequest.getUnreadCount()) > 0) {
                    mParent.handleGhostIconAnimation(true);
                    return;
                }
            }
        }
    }

    @OnClick(R.id.chat_whisper_right_card_view)
    protected void onCloseDrawerClick() {
        mParent.closeDrawer();
    }

    WhispersAdapter.WhisperClickedListener mWhisperClickedListener = new WhispersAdapter.WhisperClickedListener() {
        @Override
        public void onWhisperItemClick(User chatWhisper) {
            mParent.startPrivateChat(chatWhisper);//, chatID);
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        if (!isUpdating) {
            shouldStopUpdatingWhispers = false;
            updateWhispers();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        shouldStopUpdatingWhispers = true;
    }

    public void updateWhispers() {
        if (shouldStopUpdatingWhispers) {
            isUpdating = false;
            return;
        }

        isUpdating = true;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!shouldStopUpdatingWhispers) {
                    requestWhispers(true);
                    updateWhispers();
                } else {
                    isUpdating = false;
                }
            }
        }, 7000);
    }
}
