package com.app.livideo.fragments;


import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.app.livideo.R;
import com.app.livideo.activities.ArtistRecordActivity;
import com.app.livideo.api.APICalls;
import com.app.livideo.interfaces.CheckStreamAllowedHandler;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.utils.AndroidUtils;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.CameraHelper;
import com.app.livideo.utils.Preferences;
import com.app.livideo.utils.ViewUtils;
import com.app.livideo.views.ScheduledCapturePreview;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class ScheduledFragment extends BaseRecordFragment implements AndroidUtils.KeyboardHelper.KeyboardListener {// implements SensorEventListener {

    private Camera mCamera;
    private MediaRecorder mMediaRecorder;

    private static final String TAG = "Recorder";
    private ImageView captureButton;
    private static File currentVideoCaptureFile = null;
    private static int mCurrentSelectedCamera = 0;
    static final int cameraHeight = 720;
    static final int cameraWidth = 480;

    CountDownTimer recordingTimer;

    private static final String RTMP_BASE = "rtmp://live-streaming.wereliveinfive.com:8888/";
    private static final String SOFTLAUNCH = "live_softlaunch/";
    private static final String DEBUG = "live_debug/";
    private static final String PRODUCTION = "live_prod/";
    private boolean KILLAPP = false;
    private int streamSecondsLeft = 120;   //get this information from api and enable recording button afterwards!
    private boolean doFragmentChange = true;
    private boolean callOnce = true;
    CamcorderProfile mProfile;
    private ScheduledCapturePreview mCameraPreview;

    @Bind(R.id.activity_artist_record_video_footer_progressbar) ProgressBar footer_progressbar;
    @Bind(R.id.activity_artist_record_video_footer_txt_seconds) TextView footer_txt_seconds;
    @Bind(R.id.record_capture_button) ImageView mCaptureButton;
    @Bind(R.id.record_headline_x) ImageView xButton;
    @Bind(R.id.flash_image) ImageView mFlashButton;
    @Bind(R.id.flash_button) View flashView;
    @Bind(R.id.front_flash) View frontFlash;
    @Bind(R.id.recording_identifier) View recordingIdentifier;

    @Inject LVSession mSession;
    @Inject RequestQueue mRequestQueue;
    @Inject Context mContext;

    View contentView;

    public ScheduledFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        contentView = inflater.inflate(R.layout.fragment_scheduled, container, false);
        ButterKnife.bind(this, contentView);
        LVInject.inject(this);
        init();
        return contentView;
    }

    private void init() {
        Preferences.setPreference(ArtistRecordActivity.SHARED_PREF_IS_RECDORDING, false, mContext);
        mCameraPreview = new ScheduledCapturePreview(mContext, (SurfaceView) contentView.findViewById(R.id.cameraView));
        mCameraPreview.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        ((FrameLayout) contentView.findViewById(R.id.layout)).addView(mCameraPreview);
        mCameraPreview.setKeepScreenOn(true);
        if (!mContext.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)) {
            flashView.setVisibility(View.GONE);
        }
        String movieDirectoryPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES).getAbsolutePath() + File.separator + "LiVideo" + File.separator;

        File movieDir = new File(movieDirectoryPath);
        if (!movieDir.mkdirs())
            Log.e("MOVIEPATH", "Couldn't create: " + movieDirectoryPath);
        resetFooterAfterRecording();
        String headerAddOn = "";
        if (getArguments().getBoolean(ArtistRecordActivity.SHARE_LOCATION)) {
            headerAddOn = " - " + getArguments().getString(ArtistRecordActivity.LOCATION);
        }

        setCaptureButton(false);
        APICalls.requestCheckStreamAllowed(mSession, getActivity(), mContext, mRequestQueue, new CheckStreamAllowedHandler() {
            @Override
            public void RunCompleted(int secondsLeft) {
                if (isAdded()) {
                    streamSecondsLeft = secondsLeft;
                    setCaptureButton(true);
                }
            }
        });
        setListeners();
    }

    private void setListeners() {
        mCaptureButton.setOnTouchListener(ViewUtils.tl_animation);

        AndroidUtils.KeyboardHelper keyboardHelper = new AndroidUtils.KeyboardHelper(
                getActivity().findViewById(android.R.id.content),
                getActivity().getWindowManager().getDefaultDisplay().getHeight(),
                this
        );
    }


    @Override
    public void onResume() {
        super.onResume();
        mCamera = CameraHelper.getDefaultCameraInstance();
        mCameraPreview.resetCamera(mCamera);
    }

    /**
     * Enables/Disables capture button.
     *
     * @param active
     */
    private void setCaptureButton(boolean active) {
        if (active) {
            mCaptureButton.setAlpha(1.f);
        } else {
            mCaptureButton.setAlpha(.4f);
        }
        mCaptureButton.setSelected(Preferences.getPreference(ArtistRecordActivity.SHARED_PREF_IS_RECDORDING, mContext, false));
        mCaptureButton.setEnabled(active);
    }

    @Override
    public void onPause() {
        super.onPause();
        releaseMediaRecorder();
        releaseCamera();
    }

    @Override
    public void onStopRecording(final File videoFile) {
        if (recordingTimer != null)
            recordingTimer.cancel();
        recordingIdentifier.setVisibility(View.GONE);
        xButton.setVisibility(View.VISIBLE);
        recordingTimer = null;
        resetFooterAfterRecording();
        Preferences.setPreference(ArtistRecordActivity.SHARED_PREF_DATA_KEY_VIDEO_FILE_PATH, videoFile.getAbsolutePath(), mContext);
        Intent intent = new Intent();
        intent.putExtra(ArtistRecordActivity.RECORD_BUNDLE, getArguments());
        getActivity().setResult(Activity.RESULT_OK, intent);
        getActivity().finish();
    }

    private void setupFooterForRecording() {
        footer_progressbar.setMax(streamSecondsLeft);
        setFooterProgress(0);
        footer_progressbar.setVisibility(View.VISIBLE);
        footer_txt_seconds.setVisibility(View.VISIBLE);
    }

    private void resetFooterAfterRecording() {
        footer_progressbar.setVisibility(View.GONE);
        footer_txt_seconds.setVisibility(View.GONE);
    }


    /**
     * Sets the footers progressbar & text field (remaining time)
     *
     * @param secondsCount
     */
    private void setFooterProgress(int secondsCount) {
        int progress = streamSecondsLeft - secondsCount;
        footer_progressbar.setProgress(secondsCount);
        footer_txt_seconds.setText(getMinutesString(progress));
        footer_txt_seconds.setTextColor(progress > 5 ? getResources().getColor(R.color.livideo_text_primary_enabled) : getResources().getColor(R.color.livideo_red));
    }

    private boolean shouldShowFooter() {
        return streamSecondsLeft >= 0;
    }

    /**
     * @param seconds
     * @return the minutes as formatted string
     */
    public static String getMinutesString(int seconds) {
        String progressString = "";
        if ((float) seconds / 60 > 1)
            progressString += String.format("%1.0f:", Math.floor((float) seconds / 60));    //add minutes string
        progressString += String.format(seconds < 10 ? "%d" : "%02d", seconds % 60);    //add seconds string
        Log.d("TIME LEFT", seconds + ", " + progressString);

        return progressString;
    }

    private void releaseMediaRecorder() {
        if (mMediaRecorder != null) {
            mMediaRecorder.reset();
            mMediaRecorder.release();
            mMediaRecorder = null;
            mCamera.lock();
        }
    }

    private void releaseCamera() {
        if (mCamera != null) {
            mCamera.release();
            mCamera = null;
        }
    }

    public void onStartRecording() {
        if (recordingTimer != null) {
            recordingTimer.cancel();
            recordingTimer = null;
        }
        recordingTimer = new CountDownTimer(streamSecondsLeft * 1000, 1000) {
            int count = 0;
            boolean footerReady = false;

            @Override
            public void onTick(long millisUntilFinished) {
                if (!footerReady)
                    setupFooterForRecording();
                onRecordingProgress(++count);
            }

            @Override
            public void onFinish() {
                onStopRecording(currentVideoCaptureFile);
            }
        }.start();
        recordingIdentifier.setVisibility(View.VISIBLE);
        xButton.setVisibility(View.GONE);
    }

    public void onRecordingProgress(int secondsCount) {
        if (shouldShowFooter()) {
            setFooterProgress(secondsCount);
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private boolean prepareVideoRecorder() {
        Camera.Parameters parameters = mCamera.getParameters();
        List<Camera.Size> mSupportedPreviewSizes = parameters.getSupportedPreviewSizes();
        Camera.Size optimalSize = CameraHelper.getOptimalPreviewSize(mSupportedPreviewSizes,
                mCameraPreview.getWidth(), mCameraPreview.getHeight());
        mProfile = CamcorderProfile.get(CamcorderProfile.QUALITY_480P);
        mProfile.videoFrameWidth = optimalSize.width;
        mProfile.videoFrameHeight = optimalSize.height;
        mProfile.fileFormat = MediaRecorder.OutputFormat.MPEG_4;
        mProfile.videoCodec = MediaRecorder.VideoEncoder.H264;
        mProfile.audioCodec = MediaRecorder.AudioEncoder.AAC;
        parameters.setPreviewSize(mProfile.videoFrameWidth, mProfile.videoFrameHeight);
        mCamera.setParameters(parameters);
        mMediaRecorder = new MediaRecorder();
        mCamera.unlock();
        mMediaRecorder.setCamera(mCamera);
        mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mMediaRecorder.setProfile(mProfile);
        currentVideoCaptureFile = getVideoCaptureFile();
        mMediaRecorder.setOutputFile(currentVideoCaptureFile != null ? currentVideoCaptureFile.toString() : null);
        try {
            mMediaRecorder.setPreviewDisplay(mCameraPreview.getHolder().getSurface());
        } catch (Exception e) {
            //TODO Change to crashlytics
            Log.e(TAG, "Surface texture is unavailable or unsuitable" + e.getMessage());
            return false;
        }
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        Camera.getCameraInfo(mCurrentSelectedCamera, cameraInfo);
        mMediaRecorder.setOrientationHint(cameraInfo.orientation);
        try {
            mMediaRecorder.prepare();
        } catch (Exception e) {
            //TODO change to crashlytics
            Log.d(TAG, "IllegalStateException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder();
            return false;
        }
        return true;
    }

    public static File getVideoCaptureFile() {
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES), "LiVideo");
        if (!storageDir.exists()) {
            if (!storageDir.mkdirs()) {
                Log.e("CameraUtils", "getVideoFile - unable to create directory!"); //NON-NLS
                return null;
            }
        }
        String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return new File(storageDir.getPath() + File.separator + "VID_" + timestamp + ".mp4");
    }

    public void startRecording() {
        if (!Preferences.getPreference(ArtistRecordActivity.SHARED_PREF_IS_RECDORDING, mContext, false)) {
            releaseVideoRecorder();
            if (prepareVideoRecorder()) {
                try {
                    mMediaRecorder.start();
                    Preferences.setPreference(ArtistRecordActivity.SHARED_PREF_IS_RECDORDING, true, mContext);
                    onStartRecording();
                    setCaptureButton(true);
                } catch (Exception e) {
                    Log.e("CameraUtils", "startRecording: " + e.getMessage()); //NON-NLS
                    e.printStackTrace();
                    releaseVideoRecorder();
                }
            }
        }
    }

    public void releaseVideoRecorder() {
        if (mMediaRecorder != null) {
            mMediaRecorder.reset();
            mMediaRecorder.release();
            mMediaRecorder = null;
            mCamera.lock();
        }
    }

    @Override
    public void stopRecording() {
        setCaptureButton(false);
        recordingTimer.cancel();
        mMediaRecorder.stop();  // stop the recording
        releaseMediaRecorder(); // release the MediaRecorder object
        mCamera.lock();         // take camera access back from MediaRecorder
        Preferences.setPreference(ArtistRecordActivity.SHARED_PREF_IS_RECDORDING, false, mContext);
        releaseCamera();
    }

    @OnClick(R.id.record_headline_x)
    protected void onCancelClicked() {
        getActivity().onBackPressed();
    }

    @OnClick(R.id.switch_camera_button)
    public void switchCamera() {
        if (Preferences.getPreference(AppConstants.LAST_CAMERA_USED, mContext, Camera.CameraInfo.CAMERA_FACING_BACK) == Camera.CameraInfo.CAMERA_FACING_BACK) {
            selectNextCamera();
            Preferences.setPreference(AppConstants.LAST_CAMERA_USED, Camera.CameraInfo.CAMERA_FACING_FRONT, mContext);
        } else {
            selectNextCamera();
            Preferences.setPreference(AppConstants.LAST_CAMERA_USED, Camera.CameraInfo.CAMERA_FACING_BACK, mContext);
        }

        if (mFlashButton.isSelected()) {
            if (Preferences.getPreference(AppConstants.LAST_CAMERA_USED, mContext, 0) == Camera.CameraInfo.CAMERA_FACING_BACK) {
                Camera.Parameters cp = mCamera.getParameters();
                cp.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                mCamera.setParameters(cp);
                frontFlash.setVisibility(View.GONE);
            } else {
                WindowManager.LayoutParams layout = getActivity().getWindow().getAttributes();
                layout.screenBrightness = 1F;
                getActivity().getWindow().setAttributes(layout);
                frontFlash.setVisibility(View.VISIBLE);
            }
        }
    }

    public void selectNextCamera() {
        int count = Camera.getNumberOfCameras();
        mCurrentSelectedCamera = (mCurrentSelectedCamera + 1) % count;
        assignCamera();
    }

    public void assignCamera() {
        if (mCamera == null){
            mCamera = getCameraInstance(mCurrentSelectedCamera);
        } else{
            mCamera.stopPreview();
            mCamera.release();
            mCamera = getCameraInstance(mCurrentSelectedCamera);
        }

        //set preview size
        if (mCamera != null)
            mCameraPreview.resetCamera(mCamera);


//        //enable autofocus
//        if (mCamera != null)
//            enableCameraAutofocus();
    }

//    public void setCameraPreviewSize() {
//        try {
//            Camera.Parameters parameters = mCamera.getParameters();
//            parameters.setPreviewSize(mCameraPreview.getWidth(),  mCameraPreview.getHeight());
//            mCamera.setParameters(parameters);
//        } catch (Exception e) {
//            Log.e("CameraUtils", "setCameraPreviewSize failed!"); //NON-NLS
//            e.printStackTrace();
//        }
//    }

    public static Camera getCameraInstance(int index) {
        Camera camera = null;

        try {
            camera = Camera.open(index);
        } catch (Exception e) {
        }

        return camera;
    }

    @OnClick(R.id.record_capture_button)
    protected void onCaptureButtonClick() {
        Log.d(this.getClass().getName(), "onCaptureButtonClicked");
        if (Preferences.getPreference(ArtistRecordActivity.SHARED_PREF_IS_RECDORDING, mContext, false)) {
            setCaptureButton(false);
            mMediaRecorder.stop();  // stop the recording
            releaseMediaRecorder(); // release the MediaRecorder object
            mCamera.lock();         // take camera access back from MediaRecorder
            Preferences.setPreference(ArtistRecordActivity.SHARED_PREF_IS_RECDORDING, false, mContext);
            releaseCamera();
            onStopRecording(currentVideoCaptureFile);
        } else {
            setCaptureButton(false);
            startRecording();
//            new MediaPrepareTask().execute(null, null, null);
        }
    }

    @OnClick(R.id.flash_button)
    protected void onFlashClick() {
        mFlashButton.setSelected(!mFlashButton.isSelected());
        if (Preferences.getPreference(AppConstants.LAST_CAMERA_USED, mContext, 0) == Camera.CameraInfo.CAMERA_FACING_BACK) {
            Camera.Parameters cp = mCamera.getParameters();
            if (mFlashButton.isSelected()) {
                cp.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
            } else {
                cp.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
            }
            mCamera.setParameters(cp);
        } else {
            if (mFlashButton.isSelected()) {
                WindowManager.LayoutParams layout = getActivity().getWindow().getAttributes();
                layout.screenBrightness = 1F;
                getActivity().getWindow().setAttributes(layout);
                frontFlash.setVisibility(View.VISIBLE);
            } else {
                frontFlash.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void OnKeyboardOpened() {

    }

    @Override
    public void OnKeyboardClosed() {

    }
}
