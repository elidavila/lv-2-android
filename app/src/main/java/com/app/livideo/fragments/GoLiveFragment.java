package com.app.livideo.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.app.livideo.R;
import com.app.livideo.activities.ArtistRecordActivity;
import com.app.livideo.activities.SplashActivity;
import com.app.livideo.api.APICalls;
import com.app.livideo.dialogs.CustomAlertDialog;
import com.app.livideo.interfaces.ChatMessagesHandler;
import com.app.livideo.interfaces.CheckStreamAllowedHandler;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.ArtistChatMessage;
import com.app.livideo.models.Stream;
import com.app.livideo.models.validators.ArtistChatMessageValidator;
import com.app.livideo.models.validators.ArtistValidator;
import com.app.livideo.utils.AndroidUtils;
import com.app.livideo.utils.CameraUtils;
import com.app.livideo.utils.FileUtils;
import com.app.livideo.utils.LVUtils;
import com.app.livideo.utils.PicassoCircleTransform;
import com.app.livideo.utils.Preferences;
import com.app.livideo.utils.ScreenDensityUtil;
import com.app.livideo.utils.ViewUtils;
import com.crashlytics.android.Crashlytics;
import com.squareup.picasso.Picasso;

import org.mp4parser.Container;
import org.mp4parser.IsoFile;
import org.mp4parser.boxes.iso14496.part12.MovieHeaderBox;
import org.mp4parser.muxer.Movie;
import org.mp4parser.muxer.builder.DefaultMp4Builder;
import org.mp4parser.muxer.container.mp4.MovieCreator;
import org.mp4parser.muxer.tracks.TextTrackImpl;
import org.mp4parser.support.Matrix;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import eu.agilio.streamer.CameraPreview;
import eu.agilio.streamer.RtmpStreamer;
import eu.agilio.streamer.callback.ConnectionNotifier;
import eu.agilio.streamer.callback.MovieCreatorCallBack;
import eu.agilio.streamer.callback.StreamerStateCallback;
import eu.agilio.streamer.manager.CameraManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class GoLiveFragment extends Fragment
        implements CameraUtils.OnRecordingListener, AndroidUtils.KeyboardHelper.KeyboardListener, StreamerStateCallback/*, SensorEventListener*/ {

    CameraPreview mCameraPreview;
    CameraManager mCameraManager;
    CountDownTimer recordingTimer;

    private static final String RTMP_BASE = "rtmp://live-streaming.wereliveinfive.com:8888/";
    private boolean KILLAPP = false;
    private int streamSecondsLeft = 120;   //get this information from api and enable recording button afterwards!
    private SharedPreferences sharedPreferences;
    private boolean callOnce = true;
    private static final int UPDATE_CHAT_INTERVAL = 500;
    private int CHAT_REQUEST_LIMIT = 1;
    private static final Object synchronizationLock = new Object();
    private LinkedList<ArtistChatMessage> streamMessages;

    //views
    @Bind(R.id.activity_artist_record_video_cameraview) FrameLayout mCameraView;
    @Bind(R.id.activity_artist_record_video_switch_cameras_btn_img) ImageView btn_switch_cameras_img;
    @Bind(R.id.record_headline_x) ImageView xButton;
    @Bind(R.id.switch_camera_button) View switchCameraButton;
    @Bind(R.id.activity_artist_record_video_footer_progressbar) ProgressBar footer_progressbar;
    @Bind(R.id.activity_artist_record_video_footer_txt_seconds) TextView footer_txt_seconds;
    @Bind(R.id.activity_artist_record_video_container_footer) RelativeLayout footer_container;
    @Bind(R.id.record_capture_button) ImageView mCaptureButton;
    @Bind(R.id.flash_image) ImageView flash_image;
    @Bind(R.id.stream_comment) ImageView showChat;
    @Bind(R.id.messages_container) ViewGroup chatContainer;
    @Bind(R.id.messages_scroll_view) View chatView;
    @Bind(R.id.flash_button) View flashButton;
    @Bind(R.id.front_flash) View frontFlash;
    @Bind(R.id.recording_identifier) View recordingIdentifier;

    @Inject LVSession mSession;
    @Inject RequestQueue mRequestQueue;
    @Inject Context mContext;

    public GoLiveFragment() {
    }

//    Handler chatRemoverHandler;
//    Runnable chatRemovalRunnable;
//    Thread chatClearThread;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View contentView = inflater.inflate(R.layout.fragment_video_record_view, container, false);
        ButterKnife.bind(this, contentView);
        LVInject.inject(this);
        initialize();
        onResumeView();
        streamMessages = new LinkedList<>();
        return contentView;
    }

    private void initialize() {
        Preferences.setPreference(ArtistRecordActivity.SHARED_PREF_IS_RECDORDING, false, mContext);
        if (!getContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)) {
            flashButton.setVisibility(View.GONE);
        }
        mSession.setCurrentArtist(LVUtils.getArtistUser(mSession.getUser()));

        //RTMP-Streamer Lib
        sharedPreferences = getActivity().getSharedPreferences(Constants.PREF, Context.MODE_PRIVATE);
        mCameraManager = new CameraManager();
        mCameraManager.aquireCamera(sharedPreferences.getBoolean(Constants.USE_FRONT_CAMERA, Constants.DEF_USE_FRONT_CAMERA));
        final int[] cameraPreviewSize = CameraUtils.getCameraPreviewSize(getActivity(), mCameraManager.getCamera());
        final int cameraHeight = 480;
        final int cameraWidth = 640;
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] params) {
                try {
                    RtmpStreamer.getInstance().setKeyframeInterval(2);
                    RtmpStreamer.getInstance().enableAdaptiveBitRate();
                    RtmpStreamer.getInstance().setDisplayOrientation(90);
                    RtmpStreamer.getInstance().init(mCameraManager, RTMP_BASE + SplashActivity.mStreamingServer + getArguments().getString(Stream.Keys.STREAM_ID),
                            Constants.DEF_RECORDING_BIT_RATE,
                            Constants.DEF_VIDEO_BIT_RATE,
                            Constants.DEF_MIN_BIT_RATE, Constants.DEF_MAX_BIT_RATE,
                            cameraWidth, cameraHeight,
                            GoLiveFragment.this, getActivity(), new ConnectionNotifier() {
                                @Override
                                public void timeout() {
                                    Log.e(getActivity().getClass().getName(), "no internet connection or the connection could not be initialised");
                                    RtmpStreamer.getInstance().stopStreamer();
                                }
                            });
                    RtmpStreamer.getInstance().setVideoOrientation(90);
                    RtmpStreamer.getInstance().enableRecording();
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                    Crashlytics.logException(throwable);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                try {
                    mCameraPreview = new CameraPreview(getActivity(), mCameraManager);
                    mCameraView.addView(mCameraPreview);
                } catch (Exception e) {
                    e.printStackTrace();
                    Crashlytics.logException(e);
                    CustomAlertDialog cad = new CustomAlertDialog(getActivity(), getActivity().getString(R.string.streaming_not_supported), false);
                    cad.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            getActivity().onBackPressed();
                        }
                    });
                    cad.show();
                }
            }
        };

        asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        String movieDirectoryPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES).getAbsolutePath() + File.separator + "LiVideo" + File.separator;

        File movieDir = new File(movieDirectoryPath);
        if (!movieDir.mkdirs())
            Log.e("MOVIEPATH", "Couldn't create: " + movieDirectoryPath);

        RtmpStreamer.getInstance().setMovieDirectory(movieDirectoryPath);
        RtmpStreamer.getInstance().setMovieCreatorCallBack(new MovieCreatorCallBack() {
            @Override
            public void movieCreationStarted() {
                //starts merging audio&video files together
            }

            @Override
            public void movieCreationEnded(String s) {
                if (!KILLAPP) {

                    int method = -1;    //no mp4 conversion

                    Log.i("video file path", s);

                    File currentVideoFile = null;
                    if (method >= 0) {
                        currentVideoFile = new File(s + ".tmp");
                        if (new File(s).renameTo(currentVideoFile))
                            Log.d("ORIENTATION TAG", "renamed file to: " + currentVideoFile.getAbsolutePath());
                    }

                    boolean work = FileUtils.renameFileExtension(s, "mov");

                    String[] split = s.split("\\.");
                    StringBuilder sb = new StringBuilder();
                    sb.append(split[0]).append(".").append("mov");
                    File outputFile = new File(sb.toString());
                    // set mp4 rotation tag
                    try {
                        if (method == 0) {
                            IsoFile isoFile = new IsoFile(currentVideoFile.getAbsolutePath());
                            MovieHeaderBox movieHeaderBox = isoFile.getMovieBox().getMovieHeaderBox();
                            movieHeaderBox.setMatrix(Matrix.ROTATE_90);   //rotate to portrait
                            isoFile.writeContainer(new FileOutputStream(outputFile).getChannel());
                            isoFile.close();
                        }

                        if (method == 1) {
                            Movie movie = MovieCreator.build(currentVideoFile.getAbsolutePath());
                            Container outputContainer = new DefaultMp4Builder().build(movie);
                            TextTrackImpl subtitle = new TextTrackImpl();
                            subtitle.getTrackMetaData().setLanguage("eng");
                            subtitle.getSubs().add(new TextTrackImpl.Line(0, currentVideoFile.length(), "LiVideo"));
                            outputContainer.writeContainer(new FileOutputStream(outputFile).getChannel());
                        }

                        if (method >= 0 && currentVideoFile.delete())
                            Log.d("ORIENTATION TAG", "deleted temp file: " + currentVideoFile.getAbsolutePath());
                    } catch (Exception e) {
                        Log.e("ORIENTATION TAG", "error: " + e.getMessage());
                    }

                    final File _outputFile = outputFile;
                    (getActivity()).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            onStopRecording(_outputFile);
                        }
                    });
                }
            }
        });

        resetFooterAfterRecording();

        //request API Call
        setCaptureButton(false);
        APICalls.requestCheckStreamAllowed(mSession, getActivity(), mContext, mRequestQueue, new CheckStreamAllowedHandler() {
            @Override
            public void RunCompleted(int secondsLeft) {
                if (isAdded()) {
                    streamSecondsLeft = secondsLeft;
                }
            }
        });
        setCaptureButton(true);
        setListeners();

//        chatRemoverHandler = new Handler();
//        chatRemovalRunnable = new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    Thread.sleep(2000);
//                }
//                catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//                chatRemoverHandler.post(new Runnable(){
//                    public void run() {
//                        chatContainer.removeViewAt(0);
//                        streamMessages.remove(0);
//                    }
//                });
//            }
//        };
//
//        chatClearThread = new Thread(chatRemovalRunnable){
//            @Override
//            public synchronized void start() {
//                if (this.isInterrupted()){
//                    try {
//                        chatClearThread = (Thread) this.clone();
//                    } catch (CloneNotSupportedException e) {
//                        e.printStackTrace();
//                    }
//                } else {
//                    super.start();
//                }
//            }
//        };
    }

    private void setListeners() {
        btn_switch_cameras_img.setOnTouchListener(ViewUtils.tl_animation);
        flash_image.setOnTouchListener(ViewUtils.tl_animation);
        mCaptureButton.setOnTouchListener(ViewUtils.tl_animation);
        AndroidUtils.KeyboardHelper keyboardHelper = new AndroidUtils.KeyboardHelper(
                (getActivity()).findViewById(android.R.id.content),
                (getActivity()).getWindowManager().getDefaultDisplay().getHeight(),
                this);
    }

    /**
     * Enables/Disables capture button.
     *
     * @param active
     */
    private void setCaptureButton(boolean active) {
        if (active) {
            mCaptureButton.setAlpha(1.f);
        } else {
            mCaptureButton.setAlpha(.4f);
        }
        mCaptureButton.setSelected(isRecording);
        mCaptureButton.setEnabled(active);
    }

    public void stopRecording() {
        RtmpStreamer.getInstance().stopStreamer();
    }

    @Override
    public void event(int i) {
        switch (i) {
            case RtmpStreamer.NEW_STATE_READY:
                Log.d("RTMP STREAMER STATE", "STATE_READY");
                if (recordingTimer != null) {
                    recordingTimer.cancel();
                    recordingTimer = null;
                }
                if (!KILLAPP) {
                    if (getActivity() != null) {
                        (getActivity()).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                (getActivity()).getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                                resetFooterAfterRecording();
                                setCaptureButton(true);
                            }
                        });
                    }
                }
                break;
            case RtmpStreamer.NEW_STATE_STARTING:
                Log.d("RTMP STREAMER STATE", "STATE_STARTING");
                if (getActivity() != null) {
                    (getActivity()).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            recordingIdentifier.setVisibility(View.VISIBLE);
                            xButton.setVisibility(View.GONE);
                            showChat.setVisibility(View.VISIBLE);
                            showChat.setSelected(false);
                            showChatStream();
                            setCaptureButton(false);
                        }
                    });
                }
                break;
            case RtmpStreamer.NEW_STATE_STREAMING:
                Log.d("RTMP STREAMER STATE", "STATE_STREAMING");
                isRecording = true;
                if (getActivity() != null) {
                    //Is always in live stream
                    if (callOnce) {
                        callOnce = false;
                        APICalls.broadcastGoingLive(getActivity(), mContext, mSession, getArguments().getString(Stream.Keys.STREAM_ID), mRequestQueue, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.printStackTrace();
                                Log.e("GCM Server Response", "Unsuccessfull");
                            }
                        });
                    }
                    (getActivity()).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            (getActivity()).getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                            onStartRecording();
                            setCaptureButton(true);
                        }
                    });
                    Preferences.setPreference(ArtistRecordActivity.SHARED_PREF_IS_RECDORDING, true, mContext);
                }
                break;
            case RtmpStreamer.NEW_STATE_STOPPING:
                Log.d("RTMP STREAMER STATE", "STATE_STOPPING");

                if (getActivity() != null) {
                    (getActivity()).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            recordingIdentifier.setVisibility(View.GONE);
                            xButton.setVisibility(View.VISIBLE);
                            setCaptureButton(false);
                        }
                    });
                }
                break;
            case RtmpStreamer.NEW_STATE_STOPPED:
                Log.d("RTMP STREAMER STATE", "STATE_STOPPED");
                RtmpStreamer.getInstance().reset();
                isRecording = false;
                if (getActivity() != null) {
                    if (!KILLAPP) {
                        Preferences.setPreference(ArtistRecordActivity.SHARED_PREF_IS_RECDORDING, false, mContext);
                    }
                    (getActivity()).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            setCaptureButton(true);
                        }
                    });
                }
                break;
            case RtmpStreamer.NEW_STATE_INVALID:
                Log.d("RTMP STREAMER STATE", "STATE_INVALID | STATE_ERROR_STARTING");
                break;
            case RtmpStreamer.ERROR_STREAMING:
                Log.d("RTMP STREAMER STATE", "STATE_ERROR_STREAMING");
                break;
        }
    }

    private void obtainCameraResolutions(boolean useFrontCamera) {
        int width = sharedPreferences.getInt(Constants.WIDTH, Constants.DEF_WIDTH);
        int height = sharedPreferences.getInt(Constants.HEIGHT, Constants.DEF_HEIGHT);
        List<Camera.Size> possibleResolutions = mCameraManager.getResolutions(useFrontCamera);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        for (Camera.Size size : possibleResolutions) {
            width = size.width;
            height = size.height;
            editor.putInt(Constants.WIDTH, width);
            editor.putInt(Constants.HEIGHT, height);
            if (height == 720 && width == 1280) {
                break;
            }
        }
        editor.commit();
    }

    private static boolean isRecording = false;

    @OnClick(R.id.record_capture_button)
    protected void onCaptureButtonClick() {
        Log.d(this.getClass().getName(), "onCaptureButtonClicked");
        if (isRecording) {
            switchCameraButton.setEnabled(false);
            Log.d("RECORDING", "STOP_RECORDING");
            RtmpStreamer.getInstance().stopStreamer();
        } else {
            RtmpStreamer.getInstance().startStreamer();
            Log.d("RECORDING", "START_RECORDING");
        }
    }

    @OnClick(R.id.record_headline_x)
    protected void onCancelButtonClick() {
        getActivity().onBackPressed();
    }

    @Override
    public void OnKeyboardOpened() {
    }

    @Override
    public void OnKeyboardClosed() {
    }

    //switch cameras
    @OnClick(R.id.switch_camera_button)
    protected void onSwitchCamerasButtonClick() {
        try {
            mCameraManager.flipCamera(mCameraPreview);
        } catch (IOException e) {
            e.printStackTrace();
        }
        sharedPreferences.edit().putBoolean(Constants.USE_FRONT_CAMERA, !sharedPreferences.getBoolean(Constants.USE_FRONT_CAMERA, Constants.DEF_USE_FRONT_CAMERA)).apply();
        if (flash_image.isSelected()) {
            if (!sharedPreferences.getBoolean(Constants.USE_FRONT_CAMERA, Constants.DEF_USE_FRONT_CAMERA)) {
                Camera.Parameters cp = mCameraManager.getCamera().getParameters();
                cp.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                mCameraManager.getCamera().setParameters(cp);
                frontFlash.setVisibility(View.GONE);
            } else {
                WindowManager.LayoutParams layout = getActivity().getWindow().getAttributes();
                layout.screenBrightness = 1F;
                getActivity().getWindow().setAttributes(layout);
                frontFlash.setVisibility(View.VISIBLE);
            }
        }
    }

    @OnClick(R.id.flash_button)
    protected void onFlashClick() {
        flash_image.setSelected(!flash_image.isSelected());
        if (!sharedPreferences.getBoolean(Constants.USE_FRONT_CAMERA, Constants.DEF_USE_FRONT_CAMERA)) {
            Camera.Parameters cp = mCameraManager.getCamera().getParameters();
            if (flash_image.isSelected()) {
                cp.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
            } else {
                cp.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
            }
            mCameraManager.getCamera().setParameters(cp);
        } else {
            if (flash_image.isSelected()) {
                WindowManager.LayoutParams layout = getActivity().getWindow().getAttributes();
                layout.screenBrightness = 1F;
                getActivity().getWindow().setAttributes(layout);
                frontFlash.setVisibility(View.VISIBLE);
            } else {
                frontFlash.setVisibility(View.GONE);
            }
        }
    }

    @OnClick(R.id.activity_artist_record_video_cameraview)
    protected void onCameraViewClick() {
        try {
            mCameraManager.getCamera().autoFocus(new Camera.AutoFocusCallback() {
                @Override
                public void onAutoFocus(boolean success, Camera camera) {
                    if (success) {
                        mCameraManager.getCamera().cancelAutoFocus();
                    }
                }
            });
        } catch (Exception e) {
            Log.e(this.getClass().getName(), "Camera Autofocus failed! (RTMP)");
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (Preferences.getPreference(ArtistRecordActivity.SHARED_PREF_IS_RECDORDING, mContext, false)) {
            recordingTimer.cancel();
            KILLAPP = true;
        }
    }

    private void setupFooterForRecording() {
        footer_progressbar.setMax(streamSecondsLeft);
        setFooterProgress(0);
        footer_progressbar.setVisibility(View.VISIBLE);
        footer_txt_seconds.setVisibility(View.VISIBLE);
    }

    private void resetFooterAfterRecording() {
        footer_progressbar.setVisibility(View.GONE);
        footer_txt_seconds.setVisibility(View.GONE);
    }


    /**
     * Sets the footers progressbar & text field (remaining time)
     *
     * @param secondsCount
     */
    private void setFooterProgress(int secondsCount) {
        int progress = streamSecondsLeft - secondsCount;
        footer_progressbar.setProgress(secondsCount);
        footer_txt_seconds.setText(getMinutesString(progress));
        footer_txt_seconds.setTextColor(progress > 5 ? getResources().getColor(R.color.livideo_text_primary_enabled) : getResources().getColor(R.color.livideo_red));
    }

    private boolean shouldShowFooter() {
        return streamSecondsLeft >= 0;
    }


    /**
     * @param seconds
     * @return the minutes as formatted string
     */
    public static String getMinutesString(int seconds) {
        String progressString = "";
        if ((float) seconds / 60 > 1)
            progressString += String.format("%1.0f" + "m:", Math.floor((float) seconds / 60));    //add minutes string
        progressString += String.format(seconds < 10 ? "%d" + "s" : "%02d" + "s", seconds % 60);    //add seconds string
        Log.d("TIME LEFT", seconds + ", " + progressString);

        return progressString;
    }

    @Override
    public void onStartRecording() {
        if (recordingTimer != null) {
            recordingTimer.cancel();
            recordingTimer = null;
        }
        if (shouldShowFooter()) {
            recordingTimer = new CountDownTimer(streamSecondsLeft * 1000, 1000) {
                int count = 0;
                boolean footerReady = false;

                @Override
                public void onTick(long millisUntilFinished) {
                    if (!footerReady)
                        setupFooterForRecording();
                    //int secondsUntilFinished = Math.round((float)millisUntilFinished/1000);
                    onRecordingProgress(++count);
                }

                @Override
                public void onFinish() {
                    RtmpStreamer.getInstance().stopStreamer();
                }
            }.start();
        }
    }

    @Override
    public void onRecordingProgress(int secondsCount) {
        if (shouldShowFooter()) {
            setFooterProgress(secondsCount);
        }
    }

    @Override
    public void onStopRecording(final File videoFile) {
        if (recordingTimer != null)
            recordingTimer.cancel();
        recordingTimer = null;
        resetFooterAfterRecording();
        Preferences.setPreference(ArtistRecordActivity.SHARED_PREF_DATA_KEY_VIDEO_FILE_PATH, videoFile.getAbsolutePath(), mContext);
        APICalls.requestCreateThumbnail(mSession, null, mContext,
                mSession.getUser().getUserID(),
                mSession.getUser().getDeviceID(),
                getArguments().getString(Stream.Keys.STREAM_ID) + ".mov",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(this.getClass().getName(), "requestCreateThumbnail - response: " + response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(this.getClass().getName(), "requestCreateThumbnail - error: " + error.getMessage());
                    }
                },
                mRequestQueue
        );
        getActivity().setResult(Activity.RESULT_FIRST_USER);
        getActivity().finish();
    }

    private void onResumeView() {
        if (mCameraManager != null)
            mCameraManager.aquireCamera(sharedPreferences.getBoolean(Constants.USE_FRONT_CAMERA, Constants.DEF_USE_FRONT_CAMERA));
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (isRecording) {
            RtmpStreamer.getInstance().forceStop();
            RtmpStreamer.getInstance().stopStreamer();
        }
        if (mCameraManager != null) {
            mCameraManager.setPreViewCallBack(null);
            mCameraManager.releaseCamera();
        }
        RtmpStreamer.getInstance().setCameraIsDirty();
    }

    private void updateChat() {
        if (chatView.getVisibility() == View.GONE)
            return;

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (chatView.getVisibility() == View.VISIBLE) {
                    requestChatMsgs();
                    updateChat();
                }
            }
        }, UPDATE_CHAT_INTERVAL);
    }

    @OnClick(R.id.stream_comment)
    public void showChatStream() {
        if (!showChat.isSelected()) {
            showChat.setSelected(true);
            chatView.setVisibility(View.VISIBLE);
            updateChat();
        } else {
            showChat.setSelected(false);
            chatView.setVisibility(View.GONE);
        }
    }

    private void requestChatMsgs() {
        if (ArtistValidator.isValid(mSession.getCurrentArtist())) {
            String messageId = "0";

            if (ArtistChatMessageValidator.isValid(mSession.getCurrentArtist().getChatMessageList())) {
                messageId = mSession.getCurrentArtist().getChatMessageList().get(mSession.getCurrentArtist().getChatMessageList().size() - 1).getMessageId();
            }

            APICalls.requestChatMsgs(getActivity(), mContext, mSession.getCurrentArtist().getArtistId(), messageId, String.valueOf(CHAT_REQUEST_LIMIT), mSession, mRequestQueue, new ChatMessagesHandler() {
                @Override
                public void onRunComplete(List<ArtistChatMessage> newChatMessages) {
                    if (!ArtistValidator.isValid(mSession.getCurrentArtist()) || newChatMessages == null) {
                        if (isVisible() && getActivity() != null) {
                            AndroidUtils.showShortToast(getActivity().getString(R.string.retry_later), getActivity());
                            getActivity().finish();
                        }
                        return;
                    }

                    // INITIALIZE CHECK
                    if (!ArtistChatMessageValidator.isValid(mSession.getCurrentArtist().getChatMessageList())
                            && ArtistChatMessageValidator.isValid(newChatMessages)) {
                        addToMessageList(newChatMessages);
                    }
                }
            });
        }
    }

    private void addToMessageList(List<ArtistChatMessage> newChatMessages) {
        ArtistChatMessage chatMessage = newChatMessages.get(0);

        if (!streamMessages.contains(chatMessage) && isVisible()) {
//            chatClearThread.interrupt();
            streamMessages.add(chatMessage);
            int chatImageSize = ScreenDensityUtil.getScreenHeightPixels(mContext, 20);

            final ViewGroup newView = (ViewGroup) LayoutInflater.from(getActivity()).inflate(
                    R.layout.item_live_chat, chatContainer, false);
            ImageView imageView = (ImageView) newView.findViewById(R.id.chatLeftImage);
            ((TextView) newView.findViewById(R.id.chat_item_left_name_view)).setText(chatMessage.getDisplayName());
            ((TextView) newView.findViewById(R.id.chat_item_left_msg_text)).setText(chatMessage.getMessage());
            ((CardView) newView.findViewById(R.id.chat_item_card_view)).setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.livideo_trans_white));
            imageView.setMinimumWidth(chatImageSize);
            imageView.setMinimumHeight(chatImageSize);
            if (ArtistChatMessageValidator.isImageValid(chatMessage)) {
                Picasso.with(mContext)
                        .load(chatMessage.getPictureUrl())
                        .resize(chatImageSize, chatImageSize)
                        .transform(new PicassoCircleTransform())
                        .into(imageView);
            } else {
                Picasso.with(mContext)
                        .load(R.drawable.ic_username)
                        .resize(chatImageSize, chatImageSize)
                        .transform(new PicassoCircleTransform())
                        .into(imageView);
            }
            if (streamMessages.size() > 4) {
                chatContainer.removeViewAt(0);
                streamMessages.remove(0);
                chatContainer.addView(newView, streamMessages.size() - 1);
            } else {
                chatContainer.addView(newView, streamMessages.size() - 1);
            }
        }
//        else {
//            chatClearThread.start();
//        }
    }
}


//RTMP Streamer Constants
@SuppressWarnings("HardCodedStringLiteral")
class Constants {
    /**
     * SharedPreferences
     */
    public static final String PREF = "streamer_settings";
    public static final String URL_LINK = "url_link";
    public static final String FPS = "frame_per_second";
    public static final String KFI = "key_frame_interval";
    public static final String SAMPLE_RATE = "sample_rate";
    public static final String AUDIO_BIT_RATE = "audio_bit_rate";
    public static final String USER = "username";
    public static final String PASS = "password";
    public static final String WIDTH = "width";
    public static final String HEIGHT = "height";
    public static final String VIDEO_BIT_RATE = "video_bit_rate";
    public static final String VIDEO_RECORDING_BIT_RATE = "video_recording_bit_rate";
    public static final String USE_FRONT_CAMERA = "use_front_camera";
    public static final String RECORDING_ENABLED = "recording_enabled";
    public static final String ADAPTIVE_BITRATE_ENABLED = "adaptive_bitrate_enabled";
    public static final String VIDEO_STABILIZATION_ENABLED = "video_stabilization";
    public static final String SECURED_ENABLED = "secured";
    public static final String MIC_ON = "mic_on";
    public static final String MAX_FPS = "max_fps";
    public static final String MIN_FPS = "min_fps";
    /**
     * Default Values
     */
    public static final String DEF_URL_LINK = "rtmp://23.235.227.213/test/myTestUI";
    public static final String DEF_USER = "";
    public static final String DEF_PASS = "";
    public static final int DEF_VIDEO_BIT_RATE = 1500;
    public static final int DEF_RECORDING_BIT_RATE = 1500;
    public static final int DEF_MIN_BIT_RATE = 200;
    public static final int DEF_MAX_BIT_RATE = 1500;
    public static final int DEF_WIDTH = 720;
    public static final int DEF_HEIGHT = 480;
    public static final int DEF_FPS = 30;
    public static final int DEF_KFI = 1;
    public static final int DEF_SAMPLE_RATE = 44100;
    public static final int DEF_AUDIO_BIT_RATE = 64;
    public static final int DEF_MIN_FPS = 20;
    public static final int DEF_MAX_FPS = 30;
    public static final boolean DEF_USE_FRONT_CAMERA = false;
    public static final boolean DEF_RECORDING_ENABLED = false;
    public static final boolean DEF_ADAPTIVE_BITRATE_ENABLED = false;
    public static final boolean DEF_VIDEO_STABILIZATION_ENABLED = false;
    public static final boolean DEF_SECURED = false;
    public static final boolean DEF_MIC_ON = true;
}