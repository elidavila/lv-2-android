package com.app.livideo.fragments;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.app.livideo.R;
import com.app.livideo.activities.ArtistRecordActivity;
import com.app.livideo.activities.OpenMicReviewActivity;
import com.app.livideo.api.APICalls;
import com.app.livideo.dialogs.TwitterViewDialog;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.User;
import com.app.livideo.models.translators.ArtistUploadVideoTranslator;
import com.app.livideo.services.S3UploadService;
import com.app.livideo.utils.AndroidUtils;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.ArtistVideoShareObj;
import com.app.livideo.utils.Constants;
import com.app.livideo.utils.Preferences;
import com.app.livideo.views.VideoPlaybackPreview;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.fabric.sdk.android.services.network.UrlUtils;

public class ArtistRecordViewFragment extends Fragment {

    //views
    @Bind(R.id.artist_record_view_preview_frame) VideoPlaybackPreview frame_preview;
    @Bind(R.id.record_headline_edit_text) EditText header_title;
    @Bind(R.id.record_headline_location) public CheckBox mLocationShare;
    @Bind(R.id.record_headline_twitter) CheckBox mTwitterShare;
    @Bind(R.id.lock_video_button) CheckBox mLockVideo;
    @Bind(R.id.send_button_text) TextView sendButtonText;

//    @Bind(R.id.both_header_right_text) TextView headerRightText;

    @Inject LVSession mSession;
    @Inject RequestQueue mRequestQueue;
    @Inject Context mContext;

    private User user;
    private MediaPlayer mMediaPlayer;
    private File currentVideoFile;
    private Bundle mArgs;
    private final int TWITER_CODE = 2;

    public ArtistRecordViewFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View contentView = inflater.inflate(R.layout.fragment_artist_record_view, container, false);
        ButterKnife.bind(this, contentView);
        LVInject.inject(this);
        init();
        return contentView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //currentVideoFile = null;
    }

    private void init() {
        mArgs = getArguments();
        user = mSession.getUser();
        mLocationShare.setChecked(mArgs.getBoolean(ArtistRecordActivity.SHARE_LOCATION));
        mTwitterShare.setChecked(mArgs.getBoolean(ArtistRecordActivity.SHARE_TWITTER));
        mLockVideo.setChecked(!mArgs.getBoolean(ArtistUploadVideoTranslator.IS_PUBLIC));
        header_title.setText(mArgs.getString(AppConstants.STREAM_TITLE_LABEL, getResources().getString(R.string.app_name)));

        if (mSession.getUser().getUserRole() == User.UserRole.VIEWER) {
            sendButtonText.setText(R.string.send_to_open);
            mLockVideo.setVisibility(View.GONE);
        }
        //start video playback
        String filepath = Preferences.getPreference(ArtistRecordActivity.SHARED_PREF_DATA_KEY_VIDEO_FILE_PATH, mContext, "");
        currentVideoFile = new File(filepath);

        if (!currentVideoFile.isFile() || !currentVideoFile.exists()) {
            Log.e(this.getClass().getName(), "Video File has been deleted!");
            AndroidUtils.showLongToast("Video File has been deleted!", getActivity());
            ((ArtistRecordActivity) getActivity()).switchToRecordScreen(mArgs);
            return;
        }
        startVideoPlaybackAfterRecording(currentVideoFile);
    }

    @OnClick(R.id.record_headline_x)
    protected void onCancelClicked() {
        getActivity().onBackPressed();
    }

    @OnClick(R.id.re_record_button)
    protected void onRetakeButtonClick() {
        resetVideoPlayback(true);
        //clear video filePath
        Preferences.setPreference(ArtistRecordActivity.SHARED_PREF_DATA_KEY_VIDEO_FILE_PATH, "", mContext);
        //change fragments
        if (mSession.getUser().getUserRole() == User.UserRole.VIEWER){
            ((OpenMicReviewActivity) getActivity()).switchToRecordScreen(mArgs);
        } else {
            ((ArtistRecordActivity) getActivity()).switchToRecordScreen(mArgs);
        }
    }

    @OnClick(R.id.post_button)
    protected void onBroadcastButtonClick() {
        resetVideoPlayback(false);
        broadcastVideo();
    }

    private void shareToTwitter(ArtistVideoShareObj shareObj, String filename_on_s3, String link) {
        String streamTitle;
        if (header_title.getText().toString().isEmpty()) {
            streamTitle = header_title.getHint().toString();
        } else {
            streamTitle = header_title.getText().toString();
        }
        Intent intent = getTwitterIntent(streamTitle, link);
        if (intent != null) {
            getActivity().startActivityForResult(intent, TWITER_CODE);
            //start intent service to upload the video to S3
            startS3UploadService(shareObj, filename_on_s3);
        } else {
            launchTwitterDialog(streamTitle, link, shareObj, filename_on_s3);
        }
    }

    private Intent getTwitterIntent(String streamTitle, String link) {
        Intent tweetIntent = new Intent();
        tweetIntent.setType("text/plain");
        tweetIntent.putExtra(Intent.EXTRA_TEXT, getActivity().getString(R.string.out_tweet).replace("%%TITTLE%%", streamTitle).replace("%%LINK%%", link));
        final PackageManager packageManager = getActivity().getPackageManager();
        List<ResolveInfo> list = packageManager.queryIntentActivities(tweetIntent, PackageManager.MATCH_DEFAULT_ONLY);
        Iterator iterator = list.iterator();
        ResolveInfo resolveInfo;
        do {
            if (!iterator.hasNext()) {
                return null;
            }
            resolveInfo = (ResolveInfo) iterator.next();
        } while (!resolveInfo.activityInfo.packageName.startsWith("com.twitter.android"));

        tweetIntent.setClassName(resolveInfo.activityInfo.packageName, resolveInfo.activityInfo.name);
        return tweetIntent;
    }

    public void launchTwitterDialog(String streamTitle, String link, final ArtistVideoShareObj shareObj, final String filename_on_s3) {
        String url = String.format("https://twitter.com/intent/tweet?text=%s&url=%s",
                UrlUtils.urlEncode(getActivity().getString(R.string.out_tweet).replace("%%TITTLE%%", streamTitle).replace("%%LINK%%", link)),
                UrlUtils.urlEncode("lvdi.io"));
        TwitterViewDialog dialog = new TwitterViewDialog(getActivity(), url);
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                //start intent service to upload the video to S3
                startS3UploadService(shareObj, filename_on_s3);
            }
        });
        dialog.show();
    }


    @OnClick(R.id.artist_record_view_preview_frame)
    protected void onCameraViewClick() {
        restartVideoPlayback();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////// Playback Functions ///////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////

    private void startVideoPlaybackAfterRecording(File videoFile) {
        try {
//            Movie video = MovieCreator.build(videoFile.getAbsolutePath());
//            TextTrackImpl subtitle = new TextTrackImpl();
//            subtitle.getTrackMetaData().setLanguage("eng");
//
//            subtitle.getSubs().add(new TextTrackImpl.Line(0, 120000, "LiVideo"));
//            video.addTrack(subtitle);
//
//            DefaultMp4Builder builder = new DefaultMp4Builder();
//            Container c = builder.build(video);
//            c.writeContainer(new FileOutputStream(videoFile.getAbsolutePath()).getChannel());

//            FileOutputStream out = new FileOutputStream(videoFile.getPath().replace("mp4","mov"));
//            // Transfer bytes from in to out
//            byte[] buf = new byte[1024];
//            int len;
//            while ((len = newImput.read(buf)) > 0) {
//                out.write(buf, 0, len);
//            }
//            newImput.close();
            //currentVideoFile = videoFile;
            Uri fileUri = Uri.fromFile(videoFile);
//            Uri fileUri = Uri.fromFile(new File(videoFile.getAbsolutePath()));
            //Uri fileUri = Uri.parse("http://softlaunch.wereliveinfive.com/2.nca.560e4912d223b.mov");
            mMediaPlayer = MediaPlayer.create(getContext(), fileUri);

            frame_preview.post(new Runnable() {
                @Override
                public void run() {
                    mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    mMediaPlayer.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                        @Override
                        public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
                            frame_preview.setAspectRatio(width, height);
                        }
                    });
                    mMediaPlayer.setDisplay(frame_preview.getHolder());
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            synchronized (this) {
                                restartVideoPlayback();
                            }
                        }
                    }, 1000);
                }
            });

        } catch (Exception e) {
            Log.e(this.getClass().getName(), "startVideoPlaybackAfterRecording:\n" + e.getMessage()); //NON-NLS
            e.printStackTrace();
        }
    }

    private void restartVideoPlayback() {
        if (mMediaPlayer != null) {
            try {
                mMediaPlayer.stop();
                mMediaPlayer.prepare();
                mMediaPlayer.seekTo(0);
                mMediaPlayer.start();
            } catch (Exception e) {
                Log.e(this.getClass().getName(), "restartVideoPlayback: " + e.getMessage()); //NON-NLS
                e.printStackTrace();
            }
        }
    }

    private synchronized void resetVideoPlayback(boolean deleteFile) {
        if (mMediaPlayer != null) {
            mMediaPlayer.stop();
            mMediaPlayer.reset();
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
        if (deleteFile)
            currentVideoFile.delete();
    }

    private void broadcastVideo() {
        File videoFile = new File(Preferences.getPreference(ArtistRecordActivity.SHARED_PREF_DATA_KEY_VIDEO_FILE_PATH, mContext, ""));

        if (!videoFile.exists()) {
            AndroidUtils.showLongToast("Video File has been deleted!", getActivity());
            getActivity().finish();
            return;
        }

        if (mSession.getUser().getUserRole() == User.UserRole.VIEWER){
            ArtistVideoShareObj obj = new ArtistVideoShareObj(
                    header_title.getText().toString(),
                    videoFile,
                    mArgs.getBoolean(ArtistRecordActivity.SHARE_LOCATION),
                    mArgs.getBoolean(ArtistRecordActivity.SHARE_TWITTER),
                    true, //TODO Find out if i need to make them public or private
                    getActivity(),
                    new ArtistVideoShareObj.OnSetupFinishedListener() {
                        @Override
                        public void onSetupFinished(ArtistVideoShareObj shareObj) {
                            sendUploadRequest(shareObj);  //Create stram object

                        }
                    }
            );
        } else {
            ArtistVideoShareObj obj = new ArtistVideoShareObj(
                    header_title.getText().toString(),
                    videoFile,
                    mArgs.getBoolean(ArtistRecordActivity.SHARE_LOCATION),
                    mArgs.getBoolean(ArtistRecordActivity.SHARE_TWITTER),
                    mArgs.getBoolean(ArtistUploadVideoTranslator.IS_PUBLIC),
                    getActivity(),
                    new ArtistVideoShareObj.OnSetupFinishedListener() {
                        @Override
                        public void onSetupFinished(ArtistVideoShareObj shareObj) {
                            sendUploadRequest(shareObj);  //Create stram object

                        }
                    }
            );
        }

    }

    //create ShareObject
    private void sendUploadRequest(final ArtistVideoShareObj shareObj) {
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
        sdf.setTimeZone(TimeZone.getTimeZone("gmt"));
        String time = sdf.format(new Date((new Date()).getTime()));

        //setup JSON data
        JSONObject jsonObjectStreamInfo = new JSONObject();
        try {
            jsonObjectStreamInfo.put(ArtistUploadVideoTranslator.PARAM_STREAM_JSON_KEY_ARTIST_ID, user.getUserID());
            jsonObjectStreamInfo.put(ArtistUploadVideoTranslator.PARAM_STREAM_JSON_KEY_HEADLINE, shareObj.getStreamTitle());
            jsonObjectStreamInfo.put(Constants.PARAM_STREAM_KEY_IS_DELAYED_WITH_QUOTES, "1");
            jsonObjectStreamInfo.put(ArtistUploadVideoTranslator.PARAM_STREAM_JSON_KEY_TIME, time);
            if (shareObj.getIsPublic()) {
                jsonObjectStreamInfo.put(ArtistUploadVideoTranslator.IS_PUBLIC, "1");
            }
            Log.d(this.getClass().getName(), "testApi:\n" + jsonObjectStreamInfo.toString());
        } catch (Exception e) {
            Log.e(this.getClass().getName(), "testApi:JSONEX: " + e.getMessage());
        }

        //send API request
        APICalls.requestNewStream(mSession, getActivity(), mContext,
                user.getUserID(),
                user.getDeviceID(),
                jsonObjectStreamInfo,
                time,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        //check if succeeded
                        if (!ArtistUploadVideoTranslator.succeeded(response)) {
                            AndroidUtils.showShortToast("Unable to upload video ...", getActivity());
                            return;
                        }

                        //get result when succeeded
                        String result = ArtistUploadVideoTranslator.getResult(response);
                        File oldVideo = new File(
                                Preferences.getPreference(ArtistRecordActivity.SHARED_PREF_DATA_KEY_VIDEO_FILE_PATH, mContext, "")
                        );
                        String movieDirectoryPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES).getAbsolutePath() + File.separator + "LiVideo" + File.separator;
                        File newVideo = new File(movieDirectoryPath + result + ".mov");
                        try {
                            copy(oldVideo, newVideo);
                            Log.d(this.getClass().getName(), "requestNewStreamResult: " + result);
                            oldVideo.delete();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        shareObj.setVideoFile(newVideo);
                        if (mTwitterShare.isChecked()) {
                            shareToTwitter(shareObj, result, ArtistUploadVideoTranslator.getShortURI(response));
                        } else{
                            startS3UploadService(shareObj, result);
                        }

                    }
                }

                ,
                new Response.ErrorListener()

                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(this.getClass().getName(), "requestNewStream:Error " + error.getMessage());
                    }
                }

                , mRequestQueue
        );
    }


    public void copy(File src, File dst) throws IOException {
        InputStream in = new FileInputStream(src);
        OutputStream out = new FileOutputStream(dst);

        // Transfer bytes from in to out
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
    }

    private void startS3UploadService(ArtistVideoShareObj shareObj, String filename_on_s3) {
        //start intent service to upload the video to S3
        Intent intent = new Intent(getContext(), S3UploadService.class);
        intent.putExtra(S3UploadService.EXTRA_STREAM_TITLE, shareObj.getStreamTitle());
        intent.putExtra(S3UploadService.EXTRA_FILENAME_ON_S3, filename_on_s3);
        intent.putExtra(S3UploadService.EXTRA_LOCAL_FILE, shareObj.getVideoFile());
        intent.putExtra(S3UploadService.EXTRA_PUSH_ON_UPLOAD_COMPLETED, true);
        getActivity().startService(intent);
        getActivity().onBackPressed();
    }
}