package com.app.livideo.fragments;

import android.Manifest;
import android.content.ContentProviderOperation;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.app.livideo.R;
import com.app.livideo.activities.NewSettingsActivity;
import com.app.livideo.activities.TutorialActivity;
import com.app.livideo.utils.AndroidUtils;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.Constants;
import com.app.livideo.utils.Preferences;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * <p/>
 * to handle interaction events.
 */
public class HelpContentFragment extends Fragment {
    @Bind(R.id.add_livideo_contact_tab) RelativeLayout addContactView;
    @Bind(R.id.tutorial_tab) View helpView;
    @Bind(R.id.help_root) View rootView;
    NewSettingsActivity parent;
    private int userType;
    boolean isActive = true;

    /**
     * Id to identify a camera permission request.
     */
    private static final int REQUEST_QUE = 0;
    /**
     * Permissions required to read and write to external storage.
     */
    private static String[] PERMISSIONS = {Manifest.permission.READ_CONTACTS,
            Manifest.permission.WRITE_CONTACTS};

    public HelpContentFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        RelativeLayout relativeLayout = (RelativeLayout) inflater.inflate(R.layout.fragment_help_content, container, false);
        ButterKnife.bind(this, relativeLayout);
        parent = (NewSettingsActivity) getActivity();
        userType = Preferences.getPreference(com.app.livideo.utils.Constants.USER_TYPE_KEY, getContext(), -1);
        if (userType == Constants.BROADCASTER_USER) {
            addContactView.setVisibility(View.VISIBLE);
            helpView.setVisibility(View.VISIBLE);
        }
        isActive = true;
        return relativeLayout;
    }

    @OnClick(R.id.tutorial_tab)
    protected void onTutorialClick() {
        if (isActive) {
            Intent intent = new Intent(getActivity(), TutorialActivity.class);
            intent.putExtra(AppConstants.KEY_IS_BROADCASTER, userType == Constants.BROADCASTER_USER);
            startActivity(intent);
        }
    }

    @OnClick(R.id.faq_tab)
    protected void onFAQClick() {
        parent.navigateToFAQ();
    }

    @OnClick(R.id.add_livideo_contact_tab)
    protected void onAddContact() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_CONTACTS) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestContactsPermission();
        } else {
            addContact("LiVideo", "1-949-267-3583", "support@livideo.com");
        }
    }

    private void requestContactsPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_CONTACTS)
                || ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_CONTACTS)
                ) {
            Snackbar.make(rootView, R.string.contacts_access,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok_caps_string, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ActivityCompat.requestPermissions(getActivity(),
                                    PERMISSIONS,
                                    REQUEST_QUE);
                        }
                    })
                    .show();
        } else {
            ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, REQUEST_QUE);
        }
    }

    private void addContact(String name, String phoneNumber, String email) {
        ArrayList<ContentProviderOperation> ops = new ArrayList<>();
        int rawContactID = ops.size();

        // Adding insert operation to operations list
        // to insert a new raw contact in the table ContactsContract.RawContacts
        ops.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
                .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
                .build());

        // Adding insert operation to operations list
        // to insert display name in the table ContactsContract.Data
        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, name)
                .build());

        // Adding insert operation to operations list
        // to insert Mobile Number in the table ContactsContract.Data
        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, phoneNumber)
                .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
                .build());
/*
        // Adding insert operation to operations list
        // to  insert Home Phone Number in the table ContactsContract.Data
        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, etHomePhone.getText().toString())
                .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_HOME)
                .build());

        // Adding insert operation to operations list
        // to insert Home Email in the table ContactsContract.Data
        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Email.ADDRESS, etHomeEmail.getText().toString())
                .withValue(ContactsContract.CommonDataKinds.Email.TYPE, ContactsContract.CommonDataKinds.Email.TYPE_HOME)
                .build()); */

        // Adding insert operation to operations list
        // to insert Work Email in the table ContactsContract.Data
        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Email.ADDRESS, email)
                .withValue(ContactsContract.CommonDataKinds.Email.TYPE, ContactsContract.CommonDataKinds.Email.TYPE_WORK)
                .build());

        try {
            // Executing all the insert operations as a single database transaction
            getActivity().getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
            AndroidUtils.showShortToast(parent.getString(R.string.added_livideo_to_contacts_string), parent);
        } catch (RemoteException | OperationApplicationException e) {
            e.printStackTrace();
            AndroidUtils.showShortToast(parent.getString(R.string.retry_later), parent);
        }
    }
}
