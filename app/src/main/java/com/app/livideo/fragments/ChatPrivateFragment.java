package com.app.livideo.fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.app.livideo.R;
import com.app.livideo.activities.ChatActivity;
import com.app.livideo.adapters.PrivateChatAdapter;
import com.app.livideo.api.APICalls;
import com.app.livideo.dialogs.ImageUploadFromDialog;
import com.app.livideo.interfaces.CodeNumberHandler;
import com.app.livideo.interfaces.PrivateChatMessagesHandler;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.PrivateChatMessage;
import com.app.livideo.models.User;
import com.app.livideo.models.validators.PrivateChatMessageValidator;
import com.app.livideo.models.validators.UserValidator;
import com.app.livideo.utils.AndroidUtils;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.TextUtils;
import com.app.livideo.utils.ViewUtils;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

/**
 * Created by Eli on 5/9/2016.
 */
public class ChatPrivateFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {


    final Object synchronizationLock = new Object();
    @Inject RequestQueue mRequestQueue;
    @Inject LVSession mSession;
    @Inject Context mContext;

    @Bind(R.id.chat_send_msg_button) TextView sendButton;
    @Bind(R.id.chat_edit_msg) EditText messageContent;
    @Bind(R.id.header_title) TextView headerTitle;
    @Bind(R.id.private_chat_progress_bar) ImageView mProgressBar;
    @Bind(R.id.header_left_icon) ImageView headerLeftIcon;
    @Bind(R.id.chat_add_img) ImageView insertImageButton;
    @Bind(R.id.private_chat_refresh_swipe) SwipeRefreshLayout mSwipe;
    @Bind(R.id.private_chat_recycler_view) RecyclerView chatRecyclerView;
    @Bind(R.id.private_chat_root) View mRootLayout;

    ChatActivity mParent;
    User mPrivateChatUser;
    LinearLayoutManager mLayoutManager;
    PrivateChatAdapter mPrivateChatAdapter;
    public boolean shouldStopUpdatingChat = false, isDownloading = false, isUserAtBottom;
    int CHAT_REQUEST_LIMIT = 70;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_chat_private, container, false);
        LVInject.inject(this);
        ButterKnife.bind(this, mView);
        insertImageButton.setVisibility(View.VISIBLE);
        headerLeftIcon.setImageResource(R.drawable.ic_back_wshadow);
        mParent = (ChatActivity) getActivity();

        mPrivateChatUser = (User) mParent.getIntent().getSerializableExtra(AppConstants.USER);
        if (UserValidator.isValid(mPrivateChatUser)) {
            mSession.setCurrentPrivateChatUser(mPrivateChatUser);
            headerTitle.setText(mPrivateChatUser.getDisplayName());

            // Set Send button in keyboard
            sendButton.setEnabled(false);
            sendButton.setAlpha(0.3f);
            messageContent.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    boolean handled = false;
                    if (actionId == EditorInfo.IME_ACTION_SEND && sendButton.isEnabled()) {
                        sendChatMessage();
                        handled = true;
                    }
                    return handled;
                }
            });

            // Set Refresh swipe
            mSwipe.setOnRefreshListener(this);
            mSwipe.setColorSchemeColors(ContextCompat.getColor(mContext, R.color.livideo_teal_primary), ContextCompat.getColor(mContext, R.color.livideo_black));

            // Chat layout
            mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
            chatRecyclerView.setLayoutManager(mLayoutManager);

            // Load chat messages
            requestPrivateChatMsgs(false, true, false);
        } else {
            AndroidUtils.showShortToast(mParent.getString(R.string.retry_later), mParent);
            mParent.finish();
        }
        return mView;
    }

    @OnClick(R.id.chat_send_msg_button)
    protected void sendChatMessage() {
        if (mSession.getUser().getUserRole() != User.UserRole.GUEST) {
            String message = messageContent.getText().toString().trim();
            if (!TextUtils.isNullOrEmpty(message)) {
                //Clear stuff
                sendButton.setEnabled(false);
                messageContent.setText("");

                //Send request to server
                shouldStopUpdatingChat = true;
                APICalls.sendPrivateChatMessage(mParent, mContext, mPrivateChatUser.getUserID(), AppConstants.TYPE_MESSAGE,
                        message, mSession, mRequestQueue, new CodeNumberHandler() {
                            @Override
                            public void RunCompleted(int code) {
                                if (code == AppConstants.STREAMCODE_SUCCESS) {
                                    sendButton.setEnabled(true);
                                    requestPrivateChatMsgs(true, true, false);
                                } else if (code == AppConstants.USERCODE_BLOCKED) {
                                    AndroidUtils.showShortToast(mPrivateChatUser.getDisplayName() + getString(R.string.has_blocked_you), mParent);
                                }
                            }
                        });
            }
        }
    }

    private void requestPrivateChatMsgs(final boolean isUpdating, final boolean shouldScrollToBottom, final boolean isLoadingMore) {
        if (!UserValidator.isValid(mPrivateChatUser) || isDownloading)
            return;

        if (isLoadingMore) {
            mSwipe.setRefreshing(true);
        } else if (mProgressBar != null && !isUpdating) {
            mProgressBar.setVisibility(View.VISIBLE);
        }

        isDownloading = true;
        String messageId = "0";

        if (!isLoadingMore && isUpdating && PrivateChatMessageValidator.isValid(mSession.getCurrentPrivateChatUser().getPrivateChatMessages())) {
            messageId = mSession.getCurrentPrivateChatUser().getPrivateChatMessages().get(mSession.getCurrentPrivateChatUser().getPrivateChatMessages().size() - 1).getIdMessage();
        }

        APICalls.requestPrivateChatMessages(mParent, mContext, mPrivateChatUser.getUserID(),
                messageId, String.valueOf(CHAT_REQUEST_LIMIT), mSession, mRequestQueue,
                new PrivateChatMessagesHandler() {
                    @Override
                    public void onRunCompleted(List<PrivateChatMessage> newChatMessages) {
                        mProgressBar.setVisibility(View.GONE);
                        mSwipe.setRefreshing(false);
                        isDownloading = false;

                        // ERROR
                        synchronized (synchronizationLock) {
                            if (!UserValidator.isValid(mPrivateChatUser) || newChatMessages == null) {
                                if (isVisible() && mParent != null) {
                                    AndroidUtils.showShortToast(mParent.getString(R.string.retry_later), mParent);
                                    mParent.finish();
                                }
                                return;
                            }

                            // REVERSE LIST
                            Collections.reverse(newChatMessages);

                            // INITIALIZE CHECK
                            if (!isUpdating || mPrivateChatAdapter == null
                                    || !PrivateChatMessageValidator.isValid(mSession.getCurrentPrivateChatUser().getPrivateChatMessages())) {
                                addChatView(newChatMessages);
                                return;
                            }

                            // UPDATE & LOADING MORE
                            if (PrivateChatMessageValidator.isValid(newChatMessages)) {
                                if (isLoadingMore && PrivateChatMessageValidator.isValid(mSession.getCurrentPrivateChatUser().getPrivateChatMessages())) {
                                    List<PrivateChatMessage> oldChatMessages = mSession.getCurrentPrivateChatUser().getPrivateChatMessages();
                                    if (!oldChatMessages.equals(newChatMessages) && oldChatMessages.size() != newChatMessages.size()) {
                                        mSession.getCurrentPrivateChatUser().setPrivateChatMessages(newChatMessages);
                                        mPrivateChatAdapter.bind(newChatMessages);
                                        chatRecyclerView.scrollToPosition((newChatMessages.size() - oldChatMessages.size())
                                                + mLayoutManager.findLastCompletelyVisibleItemPosition());
                                    }
                                    shouldStopUpdatingChat = false;
                                    updateChat();
                                } else {
                                    isUserAtBottom = ViewUtils.isLastItemDisplaying(chatRecyclerView);
                                    mSession.getCurrentPrivateChatUser().getPrivateChatMessages().addAll(newChatMessages);
                                    mPrivateChatAdapter.bind(mSession.getCurrentPrivateChatUser().getPrivateChatMessages());
                                    if (shouldScrollToBottom || isUserAtBottom) {
                                        chatRecyclerView.scrollToPosition(mSession.getCurrentPrivateChatUser().getPrivateChatMessages().size() - 1); // User sent message
                                        if (shouldScrollToBottom) {
                                            shouldStopUpdatingChat = false;
                                            updateChat();
                                        }
                                    } else {
                                        chatRecyclerView.scrollToPosition(mSession.getCurrentPrivateChatUser().getPrivateChatMessages().size()); // do not scroll (hack)
                                    }
                                }
                            }
                        }
                    }
                });
    }

    private void addChatView(List<PrivateChatMessage> newChatMessages) {
        mSession.getCurrentPrivateChatUser().setPrivateChatMessages(newChatMessages);
        if (UserValidator.isValid(mSession.getCurrentPrivateChatUser())
                && PrivateChatMessageValidator.isValid(mSession.getCurrentPrivateChatUser().getPrivateChatMessages())) {
            // Set chat data
            mPrivateChatAdapter = new PrivateChatAdapter(mParent, mSession.getCurrentPrivateChatUser());
            chatRecyclerView.setAdapter(mPrivateChatAdapter);
            chatRecyclerView.scrollToPosition(mSession.getCurrentPrivateChatUser().getPrivateChatMessages().size() - 1);
        }
    }

    private void updateChat() {
        if (shouldStopUpdatingChat)
            return;

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!shouldStopUpdatingChat || !isDownloading) {
                    requestPrivateChatMsgs(true, false, false);
                    updateChat();
                }
            }
        }, AppConstants.UPDATE_CHAT_INTERVAL);
    }

    @OnClick(R.id.chat_add_img)
    protected void onAddImageClick() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            AndroidUtils.requestStoragePermission(getActivity(), mRootLayout);
        } else {
            ImageUploadFromDialog dialog = new ImageUploadFromDialog(mParent);
            dialog.show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mSession == null || !UserValidator.isValid(mSession.getUser())) {
            AndroidUtils.restartApp(mContext);
            return;
        }

        shouldStopUpdatingChat = false;
        updateChat();
    }

    @Override
    public void onPause() {
        super.onPause();
        shouldStopUpdatingChat = true;
        ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(mProgressBar.getWindowToken(), 0);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        shouldStopUpdatingChat = true;
    }

    @OnTextChanged(R.id.chat_edit_msg)
    protected void onChatMessageChange(CharSequence text) {
        if (text.length() == 0) {
            sendButton.setEnabled(false);
            sendButton.setAlpha(0.3f);
        } else {
            sendButton.setEnabled(true);
            sendButton.setAlpha(1);
        }
    }

    @OnClick(R.id.header_left_icon)
    protected void onBackClick() {
        mParent.onBackPressed();
    }

    @Override
    public void onRefresh() {
        if (!isDownloading) {
            CHAT_REQUEST_LIMIT += 50;
            shouldStopUpdatingChat = true;
            requestPrivateChatMsgs(true, false, true);
        }
    }
}
