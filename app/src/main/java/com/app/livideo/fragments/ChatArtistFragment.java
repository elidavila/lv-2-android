package com.app.livideo.fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.app.livideo.R;
import com.app.livideo.activities.ChatActivity;
import com.app.livideo.adapters.ArtistChatAdapter;
import com.app.livideo.api.APICalls;
import com.app.livideo.dialogs.AddLiVideoContactDialog;
import com.app.livideo.dialogs.PrivateChatDialog;
import com.app.livideo.interfaces.ChatMessagesHandler;
import com.app.livideo.interfaces.DataAvailabilityHandler;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.Artist;
import com.app.livideo.models.ArtistChatMessage;
import com.app.livideo.models.User;
import com.app.livideo.models.WhisperRequest;
import com.app.livideo.models.validators.ArtistChatMessageValidator;
import com.app.livideo.models.validators.ArtistValidator;
import com.app.livideo.models.validators.UserValidator;
import com.app.livideo.models.validators.WhisperValidator;
import com.app.livideo.utils.AndroidUtils;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.LVUtils;
import com.app.livideo.utils.Preferences;
import com.app.livideo.utils.TextUtils;
import com.app.livideo.utils.ViewUtils;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.PicassoTools;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class ChatArtistFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private static final Object synchronizationLock = new Object();

    @Inject RequestQueue mRequestQueue;
    @Inject LVSession mSession;
    @Inject Context mContext;

    @Bind(R.id.header_left_icon) ImageView backButton;
    @Bind(R.id.header_right_icon) ImageView privateChatButton;
    @Bind(R.id.viewer_artist_chat_recycler_view) RecyclerView chatRecyclerView;
    @Bind(R.id.chat_send_msg_button) TextView sendButton;
    @Bind(R.id.chat_edit_msg) EditText messageContent;
    @Bind(R.id.header_title) TextView headerTitle;
    @Bind(R.id.viewer_artist_chat_progress_bar) public ImageView mProgressBar;
    @Bind(R.id.artist_chat_blocker) View mBlocker;
    @Bind(R.id.chat_refresh_swipe) SwipeRefreshLayout mSwipe;

    Animation anim_out, anim_in;
    Animation.AnimationListener anim_out_listener, anim_in_listener;
    boolean isDownloading = false, isBlocked = false, isUserAtBottom;
    ArtistChatAdapter artistChatAdapter;
    LinearLayoutManager mLayoutManager;
    ChatActivity mParent;
    View mView;
    private int CHAT_REQUEST_LIMIT = 70;
    private boolean shouldStopUpdatingChat = false;
    private ArtistChatAdapter.OnImageClickedListener onImageLongClickedListener = new ArtistChatAdapter.OnImageClickedListener() {
        @Override
        public void onImageLongClicked(ArtistChatMessage chatMessage) {
            if (!chatMessage.getMessageUserId().equalsIgnoreCase(mSession.getUser().getUserID())) {
                PrivateChatDialog dialog = new PrivateChatDialog(mParent, chatMessage, messageContent);
                dialog.show();
            }
        }
    };

    @OnClick(R.id.header_right_card_view)
    protected void onWhisperIconClick() {
        mParent.openDrawer();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.activity_chat_artist, container, false);
        LVInject.inject(this);
        ButterKnife.bind(this, mView);
        if (mSession == null || !UserValidator.isValid(mSession.getUser())) {
            AndroidUtils.restartApp(mContext);
            return mView;
        } else {
            beginChatProcess(); // WE GOOD HERE
        }
        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mSession == null || !UserValidator.isValid(mSession.getUser())) {
            AndroidUtils.restartApp(mContext);
            return;
        }

        // Set current artist to display their chat
        Artist mArtist = (Artist) getActivity().getIntent().getSerializableExtra(AppConstants.ARTIST_KEY_INTENT);
        if (mArtist.getArtistId().equalsIgnoreCase(mSession.getUser().getUserID()))
            mSession.setCurrentArtist(LVUtils.getArtistUser(mSession.getUser()));
        else
            mSession.setCurrentArtist(mArtist);

        if (ArtistValidator.isValid(mSession.getCurrentArtist())) {
            if (mArtist.getArtistId().equalsIgnoreCase(mSession.getUser().getUserID()))
                checkLiVideoContactDialog();
            shouldStopUpdatingChat = false;
            updateChat();
        }

        if (mParent.mWhispersFragment != null && WhisperValidator.isValid(mParent.mWhispersFragment.mWhisperRequests))
            for (WhisperRequest whisperRequest : mParent.mWhispersFragment.mWhisperRequests) {
                if (WhisperValidator.isValid(whisperRequest) && !whisperRequest.getUnreadCount().equalsIgnoreCase("0")) {
                    startAnimatingGhostIcon();
                    break;
                }
            }
    }

    private void beginChatProcess() {
        initialize();
        if (ArtistValidator.isValid(mSession.getCurrentArtist())) {
            if (ArtistChatMessageValidator.isValid(mSession.getCurrentArtist().getChatMessageList())) {
                addChatView(mSession.getCurrentArtist().getChatMessageList());          // Chat already cached
            } else {
                requestChatMsgs(false, true, false); // Get new chat data
            }
        } else {
            AndroidUtils.showShortToast(getActivity().getString(R.string.retry_later), getActivity());
            getActivity().finish();
        }
    }

    @OnTextChanged(R.id.chat_edit_msg)
    protected void onChatMessageChange(CharSequence text) {
        if (text.length() == 0) {
            sendButton.setEnabled(false);
            sendButton.setAlpha(0.3f);
        } else {
            sendButton.setEnabled(true);
            sendButton.setAlpha(1);
        }
    }

    private void initialize() {
        // Header
        mParent = (ChatActivity) getActivity();
        privateChatButton.setImageResource(R.drawable.ic_masquerademask2nostick_wshadows);
        backButton.setImageResource(R.drawable.ic_x_wshadows);
        Glide.with(mContext).load(R.drawable.loading_anim).asGif().into(mProgressBar);

        // Chat layout
        mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        chatRecyclerView.setLayoutManager(mLayoutManager);
        mBlocker.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return isBlocked;
            }
        });

        // Set our current artist
        Artist mArtist = (Artist) getActivity().getIntent().getSerializableExtra(AppConstants.ARTIST_KEY_INTENT);
        if (ArtistValidator.isValid(mArtist)) {
            if (mArtist.getArtistId().equalsIgnoreCase(mSession.getUser().getUserID())) {
                mSession.setCurrentArtist(LVUtils.getArtistUser(mSession.getUser()));
            } else {
                mSession.setCurrentArtist(mArtist);
            }
            headerTitle.setText(mSession.getCurrentArtist().getDisplayName());
        }

        // Set Send button in keyboard
        sendButton.setEnabled(false);
        sendButton.setAlpha(0.3f);
        messageContent.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEND && sendButton.isEnabled()) {
                    sendChatMessage();
                    handled = true;
                }
                return handled;
            }
        });

        // Set Refresh swipe
        mSwipe.setOnRefreshListener(ChatArtistFragment.this);
        mSwipe.setColorSchemeColors(ContextCompat.getColor(mContext, R.color.livideo_teal_primary), ContextCompat.getColor(mContext, R.color.livideo_black));

        // Animation listener for ghost button
        anim_in = AnimationUtils.loadAnimation(mContext, R.anim.fade_in);
        anim_in_listener = new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                privateChatButton.startAnimation(anim_out);
            }
        };

        anim_out = AnimationUtils.loadAnimation(mContext, R.anim.fade_out);
        anim_out_listener = new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                privateChatButton.startAnimation(anim_in);
            }
        };
    }

    @Override
    public void onPause() {
        super.onPause();
        shouldStopUpdatingChat = true;
        ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(mBlocker.getWindowToken(), 0);
        if (UserValidator.isValid(mSession.getCurrentPrivateChatUser()) && !mParent.ignoreEndPrivateChat)
            APICalls.requestEndPrivateChat(mSession.getCurrentPrivateChatUser().getUserID(), mParent, mContext, mRequestQueue, mSession);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        shouldStopUpdatingChat = true;
        PicassoTools.clearCache(Picasso.with(mContext));
    }

    @Override
    public void onDetach() {
        super.onDetach();
        shouldStopUpdatingChat = true;
    }

    private void updateChat() {
        if (shouldStopUpdatingChat)
            return;

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!shouldStopUpdatingChat || !isDownloading) {
                    requestChatMsgs(true, false, false);
                    updateChat();
                }
            }
        }, AppConstants.UPDATE_CHAT_INTERVAL);
    }

    private void addChatView(List<ArtistChatMessage> newChatMessages) {
        mSession.getCurrentArtist().setChatMessageList(newChatMessages);
        if (ArtistValidator.isValid(mSession.getCurrentArtist()) && ArtistChatMessageValidator.isValid(mSession.getCurrentArtist().getChatMessageList())) {
            // Set chat data
            artistChatAdapter = new ArtistChatAdapter(getActivity(), mSession.getCurrentArtist().getArtistId(), mSession.getCurrentArtist().getChatMessageList(), onImageLongClickedListener);
            chatRecyclerView.setAdapter(artistChatAdapter);
            chatRecyclerView.scrollToPosition(mSession.getCurrentArtist().getChatMessageList().size() - 1);
        }
    }

    @OnTextChanged(R.id.chat_edit_msg)
    void onTextChaned(CharSequence text) {
        if (text.toString().trim().length() > 0) {
            sendButton.setEnabled(true);
        } else {
            sendButton.setEnabled(false);
        }
    }

    @OnClick(R.id.chat_send_msg_button)
    protected void sendChatMessage() {
        if (mSession.getUser().getUserRole() != User.UserRole.GUEST) {
            String message = messageContent.getText().toString().trim();
            if (!TextUtils.isNullOrEmpty(message) && ArtistValidator.isValid(mSession.getCurrentArtist())) {
                //Clear stuff
                sendButton.setEnabled(false);
                messageContent.setText("");

                //Send request to server
                shouldStopUpdatingChat = true;
                APICalls.requestSendChatMessage(getActivity(), mContext, mSession.getCurrentArtist().getArtistId(),
                        message, mSession, mRequestQueue, new DataAvailabilityHandler() {
                            @Override
                            public void RunCompleted(boolean isAvailable) {
                                sendButton.setEnabled(true);
                                requestChatMsgs(true, true, false);
                            }
                        });
            }
        }
    }

    private void requestChatMsgs(final boolean isUpdating, final boolean shouldScrollToBottom, final boolean isLoadingMore) {
        if (!ArtistValidator.isValid(mSession.getCurrentArtist()) || isDownloading)
            return;

        if (isLoadingMore) {
            mSwipe.setRefreshing(true);
        } else if (mProgressBar != null && !isUpdating) {
            mProgressBar.setVisibility(View.VISIBLE);
        }

        isDownloading = true;
        String messageId = "0";

        if (!isLoadingMore && isUpdating && ArtistChatMessageValidator.isValid(mSession.getCurrentArtist().getChatMessageList())) {
            messageId = mSession.getCurrentArtist().getChatMessageList().get(mSession.getCurrentArtist().getChatMessageList().size() - 1).getMessageId();
        }

        APICalls.requestChatMsgs(getActivity(), mContext, mSession.getCurrentArtist().getArtistId(), messageId, String.valueOf(CHAT_REQUEST_LIMIT), mSession, mRequestQueue, new ChatMessagesHandler() {
            @Override
            public void onRunComplete(List<ArtistChatMessage> newChatMessages) {
                mProgressBar.setVisibility(View.GONE);
                isDownloading = false;
                if (isLoadingMore)
                    mSwipe.setRefreshing(false);

                // ERROR
                synchronized (synchronizationLock) {
                    if (!ArtistValidator.isValid(mSession.getCurrentArtist()) || newChatMessages == null) {
                        if (isVisible() && getActivity() != null) {
                            AndroidUtils.showShortToast(getActivity().getString(R.string.retry_later), getActivity());
                            getActivity().finish();
                        }
                        return;
                    }

                    // REVERSE LIST
                    Collections.reverse(newChatMessages);

                    // INITIALIZE CHECK
                    if (!isUpdating || artistChatAdapter == null || mSession.getCurrentArtist().getChatMessageList() == null) {
                        addChatView(newChatMessages);
                        return;
                    }

                    // UPDATE & LOADING MORE
                    if (ArtistChatMessageValidator.isValid(newChatMessages)) {
                        if (isLoadingMore && ArtistChatMessageValidator.isValid(mSession.getCurrentArtist().getChatMessageList())) {
                            List<ArtistChatMessage> oldChatMessages = mSession.getCurrentArtist().getChatMessageList();
                            if (!oldChatMessages.equals(newChatMessages) && oldChatMessages.size() != newChatMessages.size()) {
                                mSession.getCurrentArtist().setChatMessageList(newChatMessages);
                                artistChatAdapter.bind(newChatMessages);
                                chatRecyclerView.scrollToPosition((newChatMessages.size() - oldChatMessages.size())
                                        + mLayoutManager.findLastCompletelyVisibleItemPosition());
                            }
                            shouldStopUpdatingChat = false;
                            updateChat();
                        } else if (isAdded()) {
                            isUserAtBottom = ViewUtils.isLastItemDisplaying(chatRecyclerView);
                            mSession.getCurrentArtist().getChatMessageList().addAll(newChatMessages);
                            artistChatAdapter.bind(mSession.getCurrentArtist().getChatMessageList());
                            if (shouldScrollToBottom || isUserAtBottom) {
                                chatRecyclerView.scrollToPosition(mSession.getCurrentArtist().getChatMessageList().size() - 1); // User sent message
                                if (shouldScrollToBottom) {
                                    shouldStopUpdatingChat = false;
                                    updateChat();
                                }
                            } else {
                                chatRecyclerView.scrollToPosition(mSession.getCurrentArtist().getChatMessageList().size()); // do not scroll (hack)
                            }
                        }
                    }
                }
            }
        });

    }

    @OnClick(R.id.header_left_icon)
    protected void onBackIconClick() {
        mParent.onBackPressedFromArtistChat();
    }

    // Show dialog to add contacts in broadcaster hasnt added livideo contact yet
    private void checkLiVideoContactDialog() {
        if (isVisible()) {
            if (Preferences.getPreference(AppConstants.SHOW_CONTACTS_DIALOGUE, mContext, true) && mSession.getUser().getUserID().equalsIgnoreCase(mSession.getCurrentArtist().getArtistId())) {
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_CONTACTS) != PackageManager.PERMISSION_GRANTED
                        || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                    AndroidUtils.requestContactsPermission(getActivity(), mView);
                } else {
                    AddLiVideoContactDialog dialog = new AddLiVideoContactDialog(getActivity());
                    dialog.show();
                }
            }
        }
    }

    @Override
    public void onRefresh() {
        CHAT_REQUEST_LIMIT += 50;
        shouldStopUpdatingChat = true;
        isDownloading = false;
        requestChatMsgs(true, false, true);
    }

    public void startAnimatingGhostIcon() {
        privateChatButton.setImageResource(R.drawable.ic_masquerademask2nostick_wshadowspurple);
        anim_in.setAnimationListener(anim_in_listener);
        anim_out.setAnimationListener(anim_out_listener);
        privateChatButton.startAnimation(anim_out);
    }

    public void stopAnimatingGhostIcon() {
        if (privateChatButton.getAnimation() != null) {
            privateChatButton.setImageResource(R.drawable.ic_masquerademask2nostick_wshadows);
            privateChatButton.getAnimation().cancel();
            privateChatButton.clearAnimation();
            anim_out.setAnimationListener(null);
            anim_in.setAnimationListener(null);
            privateChatButton.setAnimation(null);
        }
    }
}
