package com.app.livideo.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.app.livideo.R;
import com.app.livideo.activities.MainMenuActivity;
import com.app.livideo.activities.SplashActivity;
import com.app.livideo.adapters.MePageAdapter;
import com.app.livideo.api.APICalls;
import com.app.livideo.dialogs.RemoveStreamDialog;
import com.app.livideo.interfaces.EnterStreamHandler;
import com.app.livideo.interfaces.MyTimeLineHandler;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.EnterStreamData;
import com.app.livideo.models.Stream;
import com.app.livideo.models.User;
import com.app.livideo.models.validators.StreamValidator;
import com.app.livideo.models.validators.UserImageValidator;
import com.app.livideo.models.validators.UserValidator;
import com.app.livideo.utils.AndroidUtils;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.LVUtils;
import com.app.livideo.utils.PicassoCircleTransform;
import com.app.livideo.utils.TextUtils;
import com.app.livideo.views.VerticalViewPager;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnPageChange;

/**
 * Created by Eli on 4/17/2016.
 */
public class MePageFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    @Inject Context mContext;
    @Inject LVSession mSession;
    @Inject RequestQueue mRequestQueue;

    @Bind(R.id.header_left_icon) ImageView header_left_icon;
    @Bind(R.id.header_right_icon) ImageView header_right_icon;
    @Bind(R.id.header_img) ImageView header_img_icon;
    @Bind(R.id.header_title) TextView header_title;
    @Bind(R.id.home_toggle) ImageView homeToggle;
    @Bind(R.id.me_toggle) ImageView meToggle;
    @Bind(R.id.animated_arrow) ImageView animatedUpArrow;
    @Bind(R.id.me_page_time_line) VerticalViewPager mViewPager;
    @Bind(R.id.me_page_refresh_swipe) SwipeRefreshLayout mSwipe;

    boolean isActive, isDownloading = false;
    public MainMenuActivity parent;
    MePageAdapter mPageAdapter;
    View touch_blocker;

    public MePageFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_me_page, container, false);
        ButterKnife.bind(this, mView);
        LVInject.inject(this);
        if (mSession == null || !UserValidator.isValid(mSession.getUser()) || getActivity() == null) {
            AndroidUtils.restartApp(mContext);
        } else {
            startMeView();      // WE GOOD HERE
        }
        return mView;
    }

    private void startMeView() {
        meToggle.setSelected(true);
        parent = (MainMenuActivity) getActivity();
        touch_blocker = parent.loader_view;
        addHeaderFooterView();
        if (StreamValidator.isValid(mSession.getMyTimeLines())) {
            addMeView(mSession.getMyTimeLines());
        } else {
            requestMePage(false);
        }
    }

    private void addMeView(List<Stream> mTimeLines) {
        if (StreamValidator.isValid(mTimeLines)) {
            addMeTimeLineView(mTimeLines);
        } else {
            LVUtils.handleError(getString(R.string.invalid_artist_object), false, touch_blocker, mContext, getActivity());
        }
    }

    private void addHeaderFooterView() {
        // HEADER
        header_left_icon.setImageResource(R.drawable.ic_navhamburgermenu_wshadows);
        if (mSession.getUser().getUserRole() == User.UserRole.BROADCASTER)
            header_right_icon.setImageResource(R.drawable.ic_chat_white_two_on_wshadow);
        if (UserImageValidator.isValid(mSession.getUser().getPictures())) {
            header_img_icon.setVisibility(View.VISIBLE);
            header_title.setVisibility(View.GONE);
            Picasso.with(mContext)
                    .load(mSession.getUser().getPictures().get(mSession.getUser().getPictures().size() - 1).getUrl())
                    .transform(new PicassoCircleTransform())
                    .into(header_img_icon, new Callback() {
                        @Override
                        public void onSuccess() {
                            header_title.setText(mSession.getUser().getDisplayName());
                            header_title.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onError() {
                            header_title.setVisibility(View.VISIBLE);
                        }
                    });
        } else {
            Picasso.with(mContext).load(R.drawable.ic_username).transform(new PicassoCircleTransform()).into(header_img_icon);
            header_title.setText(mSession.getUser().getDisplayName());
            header_title.setVisibility(View.VISIBLE);
        }
    }

    private void addMeTimeLineView(List<Stream> mTimeLines) {
        // REFRESH SWIPE
        mSwipe.setOnRefreshListener(this);
        mSwipe.setColorSchemeColors(ContextCompat.getColor(mContext, R.color.livideo_teal_primary), ContextCompat.getColor(mContext, R.color.livideo_accent));

        // VIEW
        mPageAdapter = new MePageAdapter(MePageFragment.this, this.getChildFragmentManager(), mTimeLines);
        mViewPager.setAdapter(mPageAdapter);
        mViewPager.setOffscreenPageLimit(2);

        // ARROW ANIMATION
        animatedUpArrow.setVisibility(View.VISIBLE);
        final Animation fadeOut = AnimationUtils.loadAnimation(mContext, R.anim.slide_up_and_fade);
        fadeOut.setDuration(1500);
        final Animation moveUp = AnimationUtils.loadAnimation(mContext, R.anim.slide_up_and_fade);
        moveUp.setDuration(1500);
        moveUp.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                animatedUpArrow.startAnimation(fadeOut);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        fadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                animatedUpArrow.startAnimation(moveUp);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        animatedUpArrow.startAnimation(moveUp);
    }

    private void requestMePage(final boolean isRefreshing) {
        if (!isRefreshing) {
            touch_blocker.setVisibility(View.VISIBLE);
        } else {
            mSwipe.setRefreshing(true);
        }

        APICalls.requestMePage("", getActivity(), mContext, mSession, mRequestQueue, new MyTimeLineHandler() {
            @Override
            public void RunCompleted(int code, List<Stream> mTimeLines) {
                touch_blocker.setVisibility(View.GONE);
                mSwipe.setRefreshing(false);
                isDownloading = false;
                if (isVisible()) {
                    switch (code) {
                        case com.app.livideo.utils.Constants.USERCODE_SUCCESS:
                            mSession.setMyTimeLines(mTimeLines);
                            addMeView(mTimeLines); // ADD VIEW
                            break;
                        case com.app.livideo.utils.Constants.USERCODE_INVALID_RESPONSE:
                            LVUtils.handleError(getString(R.string.invaled_server), false, touch_blocker, mContext, getActivity());
                            break;
                        case com.app.livideo.utils.Constants.USERCODE_NOT_EXISTS:
                            LVUtils.handleError(getString(R.string.user_does_not_exist), false, touch_blocker, mContext, getActivity());
                            break;
                        case com.app.livideo.utils.Constants.USERCODE_OTHER_ERROR:
                            LVUtils.handleError(getString(R.string.retry_later), false, touch_blocker, mContext, getActivity());
                            break;
                        default:
                            LVUtils.handleError(getString(R.string.api_error), false, touch_blocker, mContext, getActivity());
                            break;
                    }
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mSession == null || !UserValidator.isValid(mSession.getUser())) {
            AndroidUtils.restartApp(mContext);
            return; // App error
        }
        isActive = true;
    }

    @Override
    public void onPause() {
        super.onPause();
        isActive = false;
    }

    @OnPageChange(R.id.me_page_time_line)
    protected void onPageSelected(int position) {
        mSwipe.setEnabled(position == 0);
        animatedUpArrow.setAnimation(null);
        animatedUpArrow.setVisibility(View.GONE);

        if (StreamValidator.isValid(mSession.getMyTimeLines())) {
            if (position == mSession.getMyTimeLines().size() - 2) {
                requestMoreMePage();
            }
        }
    }

    private void requestMoreMePage() {
        APICalls.requestMePage(mSession.getMyTimeLines().get(mSession.getMyTimeLines().size() - 1).getStreamId(),
                getActivity(), mContext, mSession, mRequestQueue, new MyTimeLineHandler() {
            @Override
            public void RunCompleted(int code, List<Stream> mTimeLines) {
                switch (code) {
                    case com.app.livideo.utils.Constants.USERCODE_SUCCESS:
                        if (StreamValidator.isValid(mTimeLines)) {
                            mSession.getMyTimeLines().addAll(mTimeLines);
                            mPageAdapter.bind(mSession.getMyTimeLines());
                        }
                        break;
                    case com.app.livideo.utils.Constants.USERCODE_INVALID_RESPONSE:
                        LVUtils.handleError(getString(R.string.invaled_server), false, touch_blocker, mContext, getActivity());
                        break;
                    case com.app.livideo.utils.Constants.USERCODE_NOT_EXISTS:
                        LVUtils.handleError(getString(R.string.user_does_not_exist), false, touch_blocker, mContext, getActivity());
                        break;
                    case com.app.livideo.utils.Constants.USERCODE_OTHER_ERROR:
                        LVUtils.handleError(getString(R.string.retry_later), false, touch_blocker, mContext, getActivity());
                        break;
                    default:
                        LVUtils.handleError(getString(R.string.api_error), false, touch_blocker, mContext, getActivity());
                        break;
                }
            }
        });
    }


    @Override
    public void onRefresh() {
        if (!isDownloading) {
            isDownloading = true;
            requestMePage(true);
        }
    }

    /*************************
     * HEADER & FOOTER NAVIGATION
     ************************************/
    @OnClick(R.id.header_left_card_view)
    public void onHeaderLeftClick() {
        parent.openDrawer();
    }

    @OnClick(R.id.header_right_card_view)
    public void onHeaderRightClick() {
        parent.startArtistChat(LVUtils.getArtistUser(mSession.getUser()));
    }

    @OnClick(R.id.discover_button)
    public void discoverClick() {
        if (!homeToggle.isSelected() && parent != null) {
            homeToggle.setSelected(true);
            meToggle.setSelected(false);
            parent.switchToDiscover();
        }
    }

    @OnClick(R.id.record_button)
    public void recordClick() {
        parent.launchRecording();
    }

    @OnClick(R.id.me_button)
    public void meClick() {
        if (meToggle.isSelected() && mViewPager != null && mPageAdapter != null) {
            mViewPager.setCurrentItem(0);
        } else if (parent != null) {
            homeToggle.setSelected(false);
            meToggle.setSelected(true);
            parent.switchToMe();
        }
    }

    /********************************************************************************************/

    public void performStreamClick(final Stream stream) {
        if (!StreamValidator.isValid(stream)) {
            LVUtils.handleError(getString(R.string.invalid_artist_object), false, touch_blocker, mContext, getActivity());
            return;
        }

        touch_blocker.setVisibility(View.VISIBLE);
        APICalls.requestEnterStream(mSession, getActivity(), mContext, stream.getStreamId(), mRequestQueue, new EnterStreamHandler() {
            @Override
            public void RunCompleted(EnterStreamData enterStreamData) {
                if (isActive && enterStreamData != null) {
                    switch (enterStreamData.getCode()) {
                        case AppConstants.STREAMCODE_SUCCESS: // public
                            LVUtils.startVideoPlayer(stream, enterStreamData, touch_blocker, isActive, getActivity());
                            break;
                        case AppConstants.STREAMCODE_NOT_SUBSCRIBED:
                        case AppConstants.STREAMCODE_IS_PRIVATE:
                            LVUtils.beginUserSubscriptionProcess(stream.getArtist(), getActivity(), mSession, mRequestQueue, mDismissHandler, new Runnable() {
                                @Override
                                public void run() {
                                    touch_blocker.setVisibility(View.GONE);
                                    mSession.setCurrentRunnable(null);
                                    performStreamClick(stream);
                                }
                            });
                            break;
                        case AppConstants.STREAMCODE_NOT_EXISTS:
                            LVUtils.handleError(getString(R.string.stream_unavailable), false, touch_blocker, mContext, getActivity());
                            break;
                        case AppConstants.STREAMCODE_OTHER_ERROR:
                            touch_blocker.setVisibility(View.GONE);
                            LVUtils.handleError(getString(R.string.retry_later), false, touch_blocker, mContext, getActivity());
                            break;
                        default:
                            touch_blocker.setVisibility(View.GONE);
                            LVUtils.handleError(getString(R.string.api_error), false, touch_blocker, mContext, getActivity());
                            break;
                    }
                }
            }
        });
    }

    public void performStreamLongClick(final Stream stream) {
        if (!mSession.getUser().getUserID().equalsIgnoreCase(stream.getArtistId()))
            return;

        if (StreamValidator.isValid(stream)) {
            RemoveStreamDialog dialog = new RemoveStreamDialog(getActivity(), stream, null, new Runnable() {
                @Override
                public void run() {
                    onRefresh();
                }
            });
            dialog.show();
        } else {
            LVUtils.handleError(getString(R.string.invalid_artist_object), false, touch_blocker, mContext, getActivity());
        }
    }

    DialogInterface.OnDismissListener mDismissHandler = new DialogInterface.OnDismissListener() {
        @Override
        public void onDismiss(DialogInterface dialog) {
            touch_blocker.setVisibility(View.GONE);
        }
    };
}
