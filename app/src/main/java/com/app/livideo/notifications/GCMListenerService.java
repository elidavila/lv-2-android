package com.app.livideo.notifications;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.app.livideo.R;
import com.app.livideo.activities.MainMenuActivity;
import com.app.livideo.activities.SplashActivity;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.validators.ArtistValidator;
import com.app.livideo.models.validators.UserValidator;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.TextUtils;
import com.google.android.gms.gcm.GcmListenerService;

import javax.inject.Inject;

public class GCMListenerService extends GcmListenerService {

    @Inject Context mContext;
    @Inject LVSession mSession;
    
    public static final String NOTIFICATION_NEW_STREAM = "S", NOTIFICATION_NEW_MESSAGE = "M",
            NOTIFICATION_START_CACHING = "C", NOTIFICATION_NEW_CHAT_MESSAGE = "P", NOTIFICATION_ENTERED_CHAT = "K",
            TYPE = "Type", MESSAGE = "alert", TITLE = "LiVideo", ID = "ID";
    private String message, notificationType;
    private int requestID;
    private PendingIntent pendingIntent;
    Bundle mData;

    public GCMListenerService() {
        LVInject.inject(this);
    }

    @Override
    public void onMessageReceived(String from, Bundle data) {
        if (data != null) {
            this.mData = data;
            Log.e("Notification Data", data.toString());
            message = data.getString(MESSAGE);
            notificationType = data.getString(TYPE);
            if (!TextUtils.isNullOrEmpty(notificationType)) {
                setActionAndNotify(UserValidator.isValid(mSession.getUser()));
            }
        }
    }

    private void setActionAndNotify(final boolean isUserValid) {
        if ( (notificationType.equalsIgnoreCase(GCMListenerService.NOTIFICATION_NEW_CHAT_MESSAGE) || notificationType.equalsIgnoreCase(GCMListenerService.NOTIFICATION_ENTERED_CHAT) )
                && (!TextUtils.isNullOrEmpty(mData.getString(ID))
                    && notificationType.equalsIgnoreCase(GCMListenerService.NOTIFICATION_NEW_CHAT_MESSAGE)
                    && ArtistValidator.isValid(mSession.getCurrentArtist())
                    && mData.getString(ID).equalsIgnoreCase(mSession.getCurrentArtist().getArtistId())) )
                return; // DO NOT NOTIFY IF THEY ARE IN ARTIST PAGE OR ARTIST CHAT

        requestID = (int) System.currentTimeMillis();

        Intent intent = new Intent(mContext, (isUserValid) ? MainMenuActivity.class : SplashActivity.class);
        intent.putExtra(AppConstants.KEY_NOTIFICATION_TYPE, notificationType);

        if (notificationType.equalsIgnoreCase(NOTIFICATION_NEW_STREAM)) {
            intent.putExtra(AppConstants.KEY_STREAM_ID, mData.getString(ID));
        } else if (notificationType.equalsIgnoreCase(NOTIFICATION_NEW_MESSAGE)) {
            intent.putExtra(AppConstants.KEY_NOTIFICATION_MSG, mData.getString(MESSAGE));
            intent.putExtra(AppConstants.KEY_NOTIFICATION_TITLE, getString(R.string.new_message_string));
            intent.putExtra(AppConstants.IS_TITLE_VIEW_KEY, true);
        } else if (notificationType.equalsIgnoreCase(NOTIFICATION_NEW_CHAT_MESSAGE) || notificationType.equalsIgnoreCase(NOTIFICATION_ENTERED_CHAT)) {
            intent.putExtra(AppConstants.KEY_ARTIST_ID, mData.getString(ID));
        }

        pendingIntent = PendingIntent.getActivity(mContext, requestID, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        notifyUser();
    }

    private void notifyUser() {
        if (!TextUtils.isNullOrEmpty(notificationType)) {
            // Create the push notification with data
            Bitmap icon = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_launcher);
            if (!notificationType.equalsIgnoreCase(NOTIFICATION_START_CACHING)) {
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.notification_icon)
                        .setLargeIcon(icon)
                        .setStyle(new NotificationCompat.BigPictureStyle().bigPicture(icon))
                        .setContentTitle(TITLE)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setAutoCancel(true)
                        .setLights(ContextCompat.getColor(mContext, R.color.livideo_teal_primary), 1000, 1000)
                        .setOnlyAlertOnce(true)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                        .setContentText(message);
                mBuilder.setContentIntent(pendingIntent);

                // Notify user
                NotificationManager mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
                mNotificationManager.notify(requestID, mBuilder.build());
            }
        }
    }
}
