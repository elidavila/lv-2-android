package com.app.livideo.notifications;

import android.app.Activity;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.android.volley.RequestQueue;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.utils.Constants;
import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.app.livideo.R;
import com.app.livideo.api.APICalls;
import com.app.livideo.lv_app.LVInject;
import com.app.livideo.utils.Preferences;

import java.io.IOException;

import javax.inject.Inject;

public class GCMRegistrationIntentService extends IntentService {

    @Inject RequestQueue mRequestQueue;
    @Inject Context mContext;
    @Inject LVSession mSession;

    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    private static final String TAG = "RegIntentService";
    private static final String[] TOPICS = {"global"};

    public GCMRegistrationIntentService() {
        super(TAG);
        LVInject.inject(this);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            synchronized (TAG) {
                InstanceID instanceID = InstanceID.getInstance(this);
                String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId), GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                sendRegistrationToServer(token);
                subscribeTopics(token);
                Preferences.setPreference(SENT_TOKEN_TO_SERVER, true, mContext);
            }
        } catch (Exception e) {
            Preferences.setPreference(SENT_TOKEN_TO_SERVER, false, mContext);
        }
        Intent registrationComplete = new Intent(REGISTRATION_COMPLETE);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    private void sendRegistrationToServer(String token) {
        if (token == null)
            return;
        APICalls.requestGCMRegistration(Preferences.getPreference(Constants.KEY_USER_ID, mContext, ""), Preferences.getPreference(Constants.KEY_DEVICE, mContext, ""), null, mContext, token, mSession, mRequestQueue);
    }

    private void subscribeTopics(String token) throws IOException {
        for (String topic : TOPICS) {
            GcmPubSub pubSub = GcmPubSub.getInstance(this);
            pubSub.subscribe(token, "/topics/" + topic, null);
        }
    }
}
