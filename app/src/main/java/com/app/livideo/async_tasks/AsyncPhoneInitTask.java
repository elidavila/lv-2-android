package com.app.livideo.async_tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.util.SparseArray;
import android.widget.EditText;
import android.widget.Spinner;

import com.app.livideo.adapters.CountryAdapter;
import com.app.livideo.models.Country;
import com.app.livideo.utils.PhoneUtils;
import com.google.i18n.phonenumbers.PhoneNumberUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by eli on 3/17/2016.
 */
public class AsyncPhoneInitTask extends AsyncTask<Void, Void, ArrayList<Country>> {

    protected SparseArray<ArrayList<Country>> mCountriesMap = new SparseArray<>();
    protected PhoneNumberUtil mPhoneNumberUtil = PhoneNumberUtil.getInstance();
    private int mSpinnerPosition = -1;
    private Context mContext;
    private CountryAdapter mAdapter;
    private Spinner mSpinner;
    private EditText phoneNumberEditView;

    public AsyncPhoneInitTask(Context mContext, CountryAdapter mAdapter, Spinner mSpinner, EditText phoneNumberEditView) {
        this.mContext = mContext;
        this.mAdapter = mAdapter;
        this.mSpinner = mSpinner;
        this.phoneNumberEditView = phoneNumberEditView;
    }

    @Override
    protected ArrayList<Country> doInBackground(Void... params) {
        ArrayList<Country> data = new ArrayList<>(233);
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(mContext.getAssets().open("countries.dat"), "UTF-8")); //NON-NLS
            String line;
            int i = 0;
            while ((line = reader.readLine()) != null) {
                //process line
                Country c = new Country(mContext, line, i);
                data.add(c);
                ArrayList<Country> list = mCountriesMap.get(c.getCountryCode());
                if (list == null) {
                    list = new ArrayList<Country>();
                    mCountriesMap.put(c.getCountryCode(), list);
                }
                list.add(c);
                i++;
            }
        } catch (IOException e) {
            Log.e("IOExeption", e.getCause() + ""); //NON-NLS
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    Log.e("IOExeption", e.getCause() + ""); //NON-NLS
                    e.printStackTrace();
                }
            }
        }


        if (!android.text.TextUtils.isEmpty(phoneNumberEditView.getText()))
            return data;

        String countryRegion = PhoneUtils.getCountryRegionFromPhone(mContext);
        int code = mPhoneNumberUtil.getCountryCodeForRegion(countryRegion);
        ArrayList<Country> list = mCountriesMap.get(code);
        if (list != null) {
            for (Country c : list) {
                if (c.getPriority() == 0) {
                    mSpinnerPosition = c.getNum();
                    break;
                }
            }
        }
        return data;
    }

    @Override
    protected void onPostExecute(ArrayList<Country> data) {
        mAdapter.addAll(data);
        if (mSpinnerPosition > 0)
            mSpinner.setSelection(mSpinnerPosition);
    }
}