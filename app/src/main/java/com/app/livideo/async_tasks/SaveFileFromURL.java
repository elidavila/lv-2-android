package com.app.livideo.async_tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by eli on 11/4/2015.
 */
public class SaveFileFromURL extends AsyncTask<String, String, String[]> {

    private String[] mStreamIds;
    private Context mContext;

    public SaveFileFromURL(String[] streamIds, Context context) {
        this.mStreamIds = streamIds;
        this.mContext = context;
    }

    /**
     * Before starting background thread
     * Show Progress Bar Dialog
     * */
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    /**
     * Downloading file in background thread
     * */
    @Override
    protected String[] doInBackground(String... f_url) {
        int count;
        try {
            for (int i = 0; i < f_url.length; i++) {
                URL url = new URL(f_url[i]);
                URLConnection conection = url.openConnection();
                conection.connect();
                int lenghtOfFile = conection.getContentLength();

                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                // Output stream to write file
                File file = new File(mContext.getCacheDir() + "/LIVIDEO_CACHE", mStreamIds[i]);
                FileOutputStream output = new FileOutputStream(file);

                byte data[] = new byte[1024];
                long total = 0;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile)); // After this onProgressUpdate will be called
                    output.write(data, 0, count);   // writing data to file
                }

                output.flush(); // flushing output
                output.close(); // closing streams
                input.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return f_url;
    }

    /**
     * After completing background task
     * Dismiss the progress dialog
     * **/
    @Override
    protected void onPostExecute(String[] file_url) {
    }
}
