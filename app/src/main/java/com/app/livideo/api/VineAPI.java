package com.app.livideo.api;

import android.os.AsyncTask;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.app.livideo.interfaces.VineLoginHandler;
import com.app.livideo.models.VineData;
import com.app.livideo.models.VineLoginData;
import com.app.livideo.models.validators.VineDataValidator;
import com.app.livideo.net.LVStringRequest;
import com.app.livideo.utils.JSONHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by KaiKu on 15.10.2015.
 * Edit: Eli 12/1/2015
 */
public class VineAPI {
    public static void Login(final String user, final String password, final VineLoginHandler handler) {
        new AsyncTask<Void, Void, VineData>() {
            @Override
            protected VineData doInBackground(Void... params) {
                try {
                    Map<String, Object> stringObjectMap = VineAPI.RequestWithDictionary(new HashMap<String, String>() {{
                        put("username", user);
                        put("password", password);
                    }}, "");

                    if (stringObjectMap == null) {
                        return null;
                    }

                    String userID = String.valueOf(((HashMap) stringObjectMap.get("data")).get("userId"));
                    VineData userData = VineAPI.RequestWithDictionary(userID, "GET");

                    return userData;
                }

                catch(Exception ex) {
                    return null;
                }
            }

            @Override
            protected void onPostExecute(VineData userData) {
                if (VineDataValidator.isValid(userData)) {
                    handler.LoginCompleted(true, userData);
                } else {
                    handler.onError();
                }

            }
        }.execute();
    }

    public static VineData RequestWithDictionary(String userID, String method) {
        try {
            URL url = new URL("https://community-vineapp.p.mashape.com/users/profiles/" + userID);
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Accept","application/json");
            connection.setRequestProperty("X-Mashape-Key", "Xc50XhqHmFmshUuNS2J0VtIYPRVQp1oGyJWjsnfS1X8CLWjqLa");
            connection.setUseCaches(false);
            connection.setDoInput(true);

            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            StringBuilder response = new StringBuilder();
            String line;

            while((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }

            rd.close();
            connection.disconnect();

            if (response.toString().equalsIgnoreCase("")) {
                return null;
            }

            JSONObject json = new JSONObject(response.toString());
            JSONObject dataObject = JSONHelper.getJSONObject(json, "data");
            VineData vineData = new VineData();
            vineData.setUserId(JSONHelper.getString(dataObject, "userId"));
            vineData.setFollowerCount(JSONHelper.getString(dataObject, "followerCount"));
            vineData.setVerified(JSONHelper.getString(dataObject, "verified"));
            vineData.setAvatarUrl(JSONHelper.getString(dataObject, "avatarUrl"));
            vineData.setEmail(JSONHelper.getString(dataObject, "email"));
            vineData.setUserName(JSONHelper.getString(dataObject, "username"));
            vineData.setFollowing(JSONHelper.getString(dataObject, "following"));
            vineData.setDescription(JSONHelper.getString(dataObject, "description"));
            vineData.setTwitterId(JSONHelper.getString(dataObject, "twitterId"));
            vineData.setTwitterConnected(JSONHelper.getString(dataObject, "twitterConnected"));
            vineData.setLikeCount(JSONHelper.getString(dataObject, "likeCount"));
            vineData.setPostCount(JSONHelper.getString(dataObject, "postCount"));
            vineData.setPhoneNumber(JSONHelper.getString(dataObject, "phoneNumber"));
            vineData.setLocation(JSONHelper.getString(dataObject, "location"));
            vineData.setFollowingCount(JSONHelper.getString(dataObject, "followingCount"));
            //handler.LoginCompleted(true, vineData);
            //Map<String, Object> map = JSONHelper.JSONToMap(json);

            return vineData;
        }

        catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public static Map<String, Object> RequestWithDictionary(HashMap<String, String> parameter, String method) {
        try {
            URL url = new URL("https://community-vineapp.p.mashape.com/users/authenticate");
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("Accept","application/json");
            connection.setRequestProperty("X-Mashape-Key", "Xc50XhqHmFmshUuNS2J0VtIYPRVQp1oGyJWjsnfS1X8CLWjqLa");

            String parameters = "";

            for (String key : parameter.keySet()) {
                String value = parameter.get(key);
                parameters += key + "=" + value + "&";
            }

            parameters = parameters.substring(0, parameters.length()-1);

            connection.setRequestProperty("Content-length", Integer.toString(parameters.getBytes().length));
            connection.setUseCaches(false);
            connection.setDoOutput(true);

            DataOutputStream wr = new DataOutputStream (connection.getOutputStream());
            wr.writeBytes(parameters);
            wr.close();

            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            StringBuilder response = new StringBuilder();
            String line;

            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }

            rd.close();
            connection.disconnect();

            if (response.toString().equalsIgnoreCase("")) {
                return null;
            }

            JSONObject json = new JSONObject(response.toString());
            Map<String, Object> map = JSONHelper.JSONToMap(json);

            return map;
        }

        catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }
}
