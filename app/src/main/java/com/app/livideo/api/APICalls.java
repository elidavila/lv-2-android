package com.app.livideo.api;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Location;
import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.util.Log;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.StringRequest;
import com.app.livideo.R;
import com.app.livideo.activities.ChatActivity;
import com.app.livideo.activities.NoInternetActivity;
import com.app.livideo.activities.SplashActivity;
import com.app.livideo.interfaces.ActiveStreamHandler;
import com.app.livideo.interfaces.ArtistHandler;
import com.app.livideo.interfaces.ArtistListHandler;
import com.app.livideo.interfaces.ArtistPageHandler;
import com.app.livideo.interfaces.ArtistTypesHandler;
import com.app.livideo.interfaces.BigDataHandler;
import com.app.livideo.interfaces.BigDataLinkHandler;
import com.app.livideo.interfaces.ChatMessagesHandler;
import com.app.livideo.interfaces.CheckStreamAllowedHandler;
import com.app.livideo.interfaces.CodeNumberHandler;
import com.app.livideo.interfaces.DataAvailabilityHandler;
import com.app.livideo.interfaces.DiscoverPageHandler;
import com.app.livideo.interfaces.EnterStreamHandler;
import com.app.livideo.interfaces.YouTubeUserDetailsHandler;
import com.app.livideo.interfaces.codeResultHandler;
import com.app.livideo.interfaces.LoginHandler;
import com.app.livideo.interfaces.MyStreamsRequestHandeler;
import com.app.livideo.interfaces.MyTimeLineHandler;
import com.app.livideo.interfaces.PrivateChatMessagesHandler;
import com.app.livideo.interfaces.RegistrationHandler;
import com.app.livideo.interfaces.SearchDataHandler;
import com.app.livideo.interfaces.StarStreamHandler;
import com.app.livideo.interfaces.StringResultHandler;
import com.app.livideo.interfaces.UserListHandler;
import com.app.livideo.interfaces.WhisperRequestHandler;
import com.app.livideo.lv_app.LVSession;
import com.app.livideo.models.Artist;
import com.app.livideo.models.ArtistChatMessage;
import com.app.livideo.models.ArtistPage;
import com.app.livideo.models.Discover;
import com.app.livideo.models.Stream;
import com.app.livideo.models.User;
import com.app.livideo.models.UserImage;
import com.app.livideo.models.YouTubeLoginData;
import com.app.livideo.models.translators.AWSIdentityTranslator;
import com.app.livideo.models.translators.ArtistChatMessageTranslator;
import com.app.livideo.models.translators.ArtistPageTranslator;
import com.app.livideo.models.translators.ArtistTranslator;
import com.app.livideo.models.translators.ArtistTypesTranslator;
import com.app.livideo.models.translators.ArtistUploadVideoTranslator;
import com.app.livideo.models.translators.DiscoverTranslator;
import com.app.livideo.models.translators.EnterStreamDataTranslator;
import com.app.livideo.models.translators.PrivateChatMessageTranslator;
import com.app.livideo.models.translators.SearchDataTranslator;
import com.app.livideo.models.translators.StreamTranslator;
import com.app.livideo.models.translators.UserTranslator;
import com.app.livideo.models.translators.WhisperTranslator;
import com.app.livideo.models.validators.ArtistPageValidator;
import com.app.livideo.models.validators.ArtistTypesValidator;
import com.app.livideo.models.validators.ArtistValidator;
import com.app.livideo.models.validators.DiscoverValidator;
import com.app.livideo.models.validators.StreamValidator;
import com.app.livideo.models.validators.UserValidator;
import com.app.livideo.net.LVStringRequest;
import com.app.livideo.utils.AndroidUtils;
import com.app.livideo.utils.AppConstants;
import com.app.livideo.utils.Constants;
import com.app.livideo.utils.JSONHelper;
import com.app.livideo.utils.LVUtils;
import com.app.livideo.utils.LiVideoCognitoIdentityProvider;
import com.app.livideo.utils.LocationUtils;
import com.app.livideo.utils.Preferences;
import com.app.livideo.utils.TextUtils;
import com.app.livideo.utils.UrlBuilder;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ExecutionException;

/**
 * Created by eli on 10/1/2015.
 */
public class APICalls {

    public static String ID_KEY = "ID"; //NON-NLS
    public static String CODE_KEY = "Code"; //NON-NLS
    public static String ERROR_CODE_KEY = "ErrorCode"; //NON-NLS

    public static void requestArtistTypes(Activity mActivity, Context mContext, final LVSession mSession, RequestQueue mRequestQueue, final ArtistTypesHandler handler) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_GET_CHANNELS_CATEGORIES);
        params.put(User.Keys.PARAMS_USER_ID, ".");
        params.put(User.Keys.PARAMS_DEVICE_ID, Preferences.getPreference(Constants.KEY_DEVICE, mContext, ""));
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                List<String> artistTypes = ArtistTypesTranslator.getArtistTypesList(response);
                if (ArtistTypesValidator.isValid(artistTypes)) {
                    mSession.setArtistTypesList(artistTypes);
                    if (handler != null)
                        handler.RunCompleted(true, artistTypes);
                } else {
                    if (handler != null)
                        handler.RunCompleted(false, null);
                }
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "Error requesting artist types.");
                if (handler != null)
                    handler.RunCompleted(false, null);
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static String requestAWSIdentity(String userId, String deviceId, RequestQueue mRequestQueue) {
        Map<String, String> params = AWSIdentityTranslator.getAWSLoginParams(userId, deviceId);
        RequestFuture<String> future = RequestFuture.newFuture();
        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, future, future);
        mRequestQueue.add(mRequest);
        try {
            return future.get();
        } catch (InterruptedException e) {
            return null;
        } catch (ExecutionException e) {
            return null;
        }
    }

    public static void requestSubscribedArtists(Activity mActivity, Context mContext, LVSession mSession, RequestQueue mRequestQueue, final ArtistListHandler handler) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_GET_SUBSCRIBED_ARTISTS);
        params.put(User.Keys.PARAMS_USER_ID, mSession.getUser().getUserID());
        params.put(User.Keys.PARAMS_DEVICE_ID, mSession.getUser().getDeviceID());
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                handler.RunCompleted(true, ArtistTranslator.getArtistList(response));
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "Error requesting subscribed artists.");
                handler.RunCompleted(false, null);
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requestArtistByURI(Activity mActivity, Context mContext, LVSession mSession, RequestQueue mRequestQueue, Uri data, final ArtistHandler handler){
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_GET_ARTIST_BY_URL);
        params.put(User.Keys.PARAMS_USER_ID, mSession.getUser().getUserID());
        params.put(User.Keys.PARAMS_DEVICE_ID, mSession.getUser().getDeviceID());
        params.put(Constants.SHORT_URI, data.toString());
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    handler.RunCompleted(true, ArtistTranslator.getArtist(jsonObject));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "Error requesting subscribed artists.");
                handler.RunCompleted(false, null);
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requestStreamByURI(Activity mActivity, Context mContext, LVSession mSession, RequestQueue mRequestQueue, Uri data, final ActiveStreamHandler handler){
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_GET_STREAM_BY_URL);
        params.put(User.Keys.PARAMS_USER_ID, mSession.getUser().getUserID());
        params.put(User.Keys.PARAMS_DEVICE_ID, mSession.getUser().getDeviceID());
        params.put(Constants.SHORT_URI, data.toString());
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    handler.RunCompleted(true, StreamTranslator.getStream(jsonObject));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "Error requesting subscribed artists.");
                handler.RunCompleted(false, null);
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requestChatMsgs(Activity mActivity, Context mContext, String artistId, String messageId, String limit, LVSession mSession, RequestQueue mRequestQueue, final ChatMessagesHandler handeler) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(ArtistChatMessage.Keys.MESSAGE_ARTIST_ID, artistId);
        params.put(ArtistChatMessage.Keys.MESSAGE_ID, messageId);
        params.put(ArtistChatMessage.Keys.LIMIT, limit);
        params.put(Constants.API_ACTION, Constants.ACTION_GET_ARTIST_CHAT_MESSAGES);
        params.put(User.Keys.PARAMS_USER_ID, mSession.getUser().getUserID());
        params.put(User.Keys.PARAMS_DEVICE_ID, mSession.getUser().getDeviceID());
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String jsonString) {
                List<ArtistChatMessage> chatMessageList = ArtistChatMessageTranslator.getChatMessageList(jsonString);
                handeler.onRunComplete(chatMessageList);
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "Error requesting chat messages.");
                handeler.onRunComplete(null);
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requestStreamChatMsgs(Activity mActivity, Context mContext, String streamId, String artistId, String messageId, String limit, LVSession mSession, RequestQueue mRequestQueue, final ChatMessagesHandler handeler) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(ArtistChatMessage.Keys.MESSAGE_ARTIST_ID, artistId);
        params.put(ArtistChatMessage.Keys.MESSAGE_ID, messageId);
        params.put(ArtistChatMessage.Keys.LIMIT, limit);
        params.put(Constants.API_ACTION, Constants.ACTION_GET_ARTIST_CHAT_MESSAGES);
        params.put(User.Keys.PARAMS_USER_ID, mSession.getUser().getUserID());
        params.put(User.Keys.PARAMS_DEVICE_ID, mSession.getUser().getDeviceID());
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);
        params.put(Stream.Keys.STREAM_ID, streamId);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String jsonString) {
                List<ArtistChatMessage> chatMessageList = ArtistChatMessageTranslator.getChatMessageList(jsonString);
                handeler.onRunComplete(chatMessageList);
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "Error requesting chat messages.");
                handeler.onRunComplete(null);
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requestResendCode(LVSession mSession, Activity mActivity, Context mContext, String userId, String deviceId, String phoneNumber, RequestQueue mRequestQueue, final DataAvailabilityHandler handler) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_REQUEST_CODE);
        params.put(User.Keys.PARAMS_PHONE, phoneNumber);
        params.put(User.Keys.PARAMS_DEVICE_ID, deviceId);
        params.put(User.Keys.PARAMS_USER_ID, userId);
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                handler.RunCompleted(true);
//                try {
//                    JSONObject json = new JSONObject(response);
//                    String result = JSONHelper.getString(json, CODE_KEY);
//                    if (result.equalsIgnoreCase("0")) {
//                        handler.RunCompleted(true);
//                    } else {
//                        handler.RunCompleted(false);
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                    handler.RunCompleted(false);
//                }
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "Error requesting resend code.");
                handler.RunCompleted(false);
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requestCheckCode(final int fromActivity, LVSession mSession, Activity mActivity, Context mContext, String userId, String deviceId, String code, RequestQueue mRequestQueue, final DataAvailabilityHandler handler) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_CHECK_CODE);
        params.put(User.Keys.PARAMS_CODE, code);
        params.put(User.Keys.PARAMS_DEVICE_ID, deviceId);
        params.put(User.Keys.PARAMS_USER_ID, userId);
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);
        if (fromActivity == AppConstants.FROM_NEW_DEVICE) {
            params.put(User.Keys.PARAMS_PHONE, Preferences.getPreference(Constants.KEY_PHONE, mContext, ""));
        }

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    Map<String, Object> map = JSONHelper.JSONToMap(json);
                    if (map == null)
                        handler.RunCompleted(false);
                    else if (map.toString().contains("false"))
                        handler.RunCompleted(false); //NON-NLS
                    else
                        handler.RunCompleted(true);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("Volley Error", "Error requesting check code.");
                    handler.RunCompleted(false); //NON-NLS
                }
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                handler.RunCompleted(false); //NON-NLS
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requestDataAvailabilityCheck(LVSession mSession, Activity mActivity, Context mContext, String action, String key, String data, String deviceID, RequestQueue mRequestQueue, final DataAvailabilityHandler handler) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, action);
        params.put(key, data);
        params.put(User.Keys.PARAMS_DEVICE_ID, deviceID);
        params.put(User.Keys.PARAMS_USER_ID, ".");
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    Map<String, Object> map = JSONHelper.JSONToMap(json);

                    if (map == null)
                        handler.RunCompleted(false);
                    else if (map.toString().contains("false"))
                        handler.RunCompleted(false); //NON-NLS
                    else
                        handler.RunCompleted(true);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("Volley Error", "Error requesting data available.");
                    handler.RunCompleted(false); //NON-NLS
                }
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                handler.RunCompleted(false); //NON-NLS
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    /**
     * SOCIAL LOGIN
     */
    public static void requestBroadcasterSocialLogin(LVSession mSession, Activity mActivity, final Context mContext, String socialID, String socialType, String deviceID, RequestQueue mRequestQueue, final LoginHandler handler) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_LOGIN_ARTIST);
        params.put(User.Keys.PARAMS_USER_ID, socialID);
        params.put(User.Keys.PARAMS_SOCIAL_TYPE, socialType);
        params.put(User.Keys.PARAMS_DEVICE_ID, deviceID);
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);
        params.put(User.Keys.LANG, Locale.getDefault().toString());
        params.put(User.Keys.TIME_ZONE, TimeZone.getDefault().getID());

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    Map<String, Object> map = JSONHelper.JSONToMap(json);

                    if (map == null) {
                        handler.RunCompleted(Constants.USERCODE_OTHER_ERROR, null);
                    } else if (map.containsKey(Constants.API_ERROR_CODE)) {
                        int errorCode = (int) map.get(Constants.API_ERROR_CODE);
                        if (errorCode == 40)
                            Preferences.setPreference(Constants.KEY_PHONE, (String) map.get(AppConstants.PARAM_INFO), mContext);
                        handler.RunCompleted(errorCode, null);
                    } else {
                        User user = UserTranslator.getUser(json);
                        if (map.containsKey(CODE_KEY)) {
                            handler.RunCompleted((int) map.get(CODE_KEY), user);
                        } else {
                            handler.RunCompleted(Constants.USERCODE_SUCCESS, user);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("JSON Error", "Error requesting social login.");
                    handler.RunCompleted(Constants.USERCODE_INVALID_RESPONSE, null);
                }
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "Error requesting social login.");
                handler.RunCompleted(Constants.USERCODE_INVALID_RESPONSE, null);
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }


    /**
     * EMAIL REGISTRATION
     */
    public static void requestEmailRegistration(Activity mActivity, Context mContext, Intent intent, LVSession mSession, RequestQueue mRequestQueue, final RegistrationHandler handler) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> userparams = new HashMap<>();
        userparams.put(User.Keys.TIME_ZONE, TimeZone.getDefault().getID());
        userparams.put(User.Keys.PARAMS_BIRTHDAY, intent.getStringExtra(AppConstants.BIRTHDAY_KEY_INTENT));
        userparams.put(User.Keys.PARAMS_EMAIL, intent.getStringExtra(AppConstants.EMAIL_KEY_INTENT));
        userparams.put(User.Keys.PARAMS_DISPLAY_NAME, intent.getStringExtra(AppConstants.DISPLAY_NAME_KEY_INTENT));
        userparams.put(User.Keys.PARAMS_PASSWORD, intent.getStringExtra(AppConstants.PASSWORD_KEY_INTENT));
        userparams.put(User.Keys.PARAMS_PHONE, intent.getStringExtra(AppConstants.PHONE_NUMBER_KEY_INTENT));
        userparams.put(User.Keys.GENDER, intent.getStringExtra(AppConstants.GENDER_KEY_INTENT));

        String jsonObject = new JSONObject(userparams).toString();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_CREATE_LIVIDEOUSER);
        params.put(AppConstants.USER, jsonObject);
        params.put(User.Keys.PARAMS_USER_ID, intent.getStringExtra(AppConstants.USER_ID_KEY_INTENT));
        params.put(User.Keys.PARAMS_DEVICE_ID, intent.getStringExtra(AppConstants.DEVICE_ID_KEY_INTENT));
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    Map<String, Object> map = JSONHelper.JSONToMap(json);

                    if (map == null) {
                        handler.RunCompleted(Constants.USERCODE_OTHER_ERROR, null);
                    } else if (map.containsKey(Constants.API_ERROR_CODE)) {
                        handler.RunCompleted((int) map.get(Constants.API_ERROR_CODE), null);
                    } else {
                        handler.RunCompleted(Constants.USERCODE_SUCCESS, json.getString(User.Keys.USER_ID));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("JSON Error", "Error requesting email registration.");
                    handler.RunCompleted(Constants.USERCODE_INVALID_RESPONSE, null);
                }
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "Error requesting email registration.");
                handler.RunCompleted(Constants.USERCODE_INVALID_RESPONSE, null);
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

/*    *//**
     * VIEWER SOCIAL REGISTRATION
     *//*
    public static void requestViewerSocialRegistration(final Context mContext, final String birthdate, RequestQueue mRequestQueue, final RegistrationHandler handler) {
        Map<String, String> userparams = new HashMap<>();
        userparams.put(User.Keys.PARAMS_BIRTHDAY, birthdate);
        userparams.put(User.Keys.TIME_ZONE, TimeZone.getDefault().getID());
        userparams.put(User.Keys.PARAMS_DISPLAY_NAME, Preferences.getPreference(Constants.KEY_DISPLAY_NAME, mContext, ""));
        userparams.put(User.Keys.PARAMS_SOCIAL_ID, Preferences.getPreference(Constants.KEY_SOCIAL_ID, mContext, ""));
        userparams.put(User.Keys.PARAMS_SOCIAL_TYPE, Preferences.getPreference(Constants.KEY_SOCIAL_TYPE, mContext, ""));

        JSONObject jsonObject = new JSONObject(userparams);
        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_NEW_USER);
        params.put(AppConstants.USER, jsonObject.toString());
        params.put(User.Keys.PARAMS_USER_ID, ".");
        params.put(User.Keys.PARAMS_DEVICE_ID, Preferences.getPreference(Constants.KEY_DEVICE, mContext, ""));
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    Map<String, Object> map = JSONHelper.JSONToMap(json);

                    if (map == null) {
                        handler.RunCompleted(Constants.USERCODE_OTHER_ERROR, null);
                    } else if (map.containsKey(Constants.API_ERROR_CODE)) {
                        handler.RunCompleted((int) map.get(Constants.API_ERROR_CODE), null);
                    } else {
                        User user = new User();
                        user.setUserID(String.valueOf(map.get(User.Keys.USER_ID)));
                        user.setSocialID(Preferences.getPreference(Constants.KEY_SOCIAL_ID, mContext, ""));
                        user.setSocialType(Preferences.getPreference(Constants.KEY_SOCIAL_TYPE, mContext, ""));
                        user.setCurrentRole(User.UserRole.VIEWER);
                        user.setBirthdate(birthdate);
                        user.setDisplayName(Preferences.getPreference(Constants.KEY_DISPLAY_NAME, mContext, ""));
                        user.setDeviceID(Preferences.getPreference(Constants.KEY_DEVICE, mContext, ""));
                        user.setTimeZone(TimeZone.getDefault().getID());
                        user.setOK(true);
                        handler.RunCompleted(Constants.USERCODE_SUCCESS, user);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    handler.RunCompleted(Constants.USERCODE_INVALID_RESPONSE, null);
                }
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "Error requesting viewer social registration.");
                handler.RunCompleted(Constants.USERCODE_INVALID_RESPONSE, null);
            }
        };

        LVStringRequest request = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        mRequestQueue.add(request);
    }*/

    /**
     * BROADCASTER SOCIAL REGISTRATION
     */
    public static void requestBroadcasterSocialRegistration(LVSession mSession, Activity mActivity, Context mContext, Intent intent, RequestQueue mRequestQueue, final RegistrationHandler handler) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> artistparams = new HashMap<>();
        artistparams.put(User.Keys.TIME_ZONE, TimeZone.getDefault().getID());
        artistparams.put(User.Keys.PARAMS_BIRTHDAY, intent.getStringExtra(AppConstants.BIRTHDAY_KEY_INTENT));
        artistparams.put(User.Keys.PARAMS_DISPLAY_NAME, intent.getStringExtra(AppConstants.DISPLAY_NAME_KEY_INTENT));
        artistparams.put(User.Keys.PARAMS_SOCIAL_ID, intent.getStringExtra(AppConstants.SOCIAL_ID_KEY_INTENT));
        artistparams.put(User.Keys.PARAMS_SOCIAL_TYPE, intent.getStringExtra(AppConstants.SOCIAL_TYPE_KEY_INTENT));
        artistparams.put(Artist.Keys.ARTIST_TYPE, intent.getStringExtra(AppConstants.ARTIST_CATEGORY_KEY_INTENT));
        artistparams.put(User.Keys.PARAMS_PHONE, intent.getStringExtra(AppConstants.PHONE_NUMBER_KEY_INTENT));
        artistparams.put(Artist.Keys.IS_ALL_AGES, intent.getStringExtra(AppConstants.IS_ALL_AGES_KEY));
        artistparams.put(User.Keys.PASSWORD, intent.getStringExtra(AppConstants.PASSWORD_KEY_INTENT));
        artistparams.put(User.Keys.GENDER, intent.getStringExtra(AppConstants.GENDER_KEY_INTENT));
        artistparams.put(Artist.Keys.PAYMENT_METHOD, intent.getStringExtra(AppConstants.ARTIST_MONEY_KEY_INTENT));

        JSONObject jsonObject = new JSONObject(artistparams);
        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_NEW_ARTIST);
        params.put(AppConstants.ARTIST, jsonObject.toString());
        params.put(User.Keys.PARAMS_USER_ID, ".");
        params.put(User.Keys.PARAMS_DEVICE_ID, intent.getStringExtra(AppConstants.DEVICE_ID_KEY_INTENT));
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);
        params.put(User.Keys.LANG, Locale.getDefault().toString());

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    Map<String, Object> map = JSONHelper.JSONToMap(json);

                    if (map == null) {
                        handler.RunCompleted(Constants.USERCODE_OTHER_ERROR, null);
                    } else if (map.containsKey(Constants.API_ERROR_CODE)) {
                        handler.RunCompleted((int) map.get(Constants.API_ERROR_CODE), null);
                    } else {
                        handler.RunCompleted(Constants.USERCODE_SUCCESS, String.valueOf(map.get(User.Keys.USER_ID)));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    handler.RunCompleted(Constants.USERCODE_INVALID_RESPONSE, null);
                }
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "Error requesting broadcaster social registration.");
                handler.RunCompleted(Constants.USERCODE_INVALID_RESPONSE, null);
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    /**
     * GET SUBSCRIBED ARTISTS
     */
/*    public static void requestSubscribedArtists(final Activity mActivity, Context mContext, final Class activityToGoTo, final LVSession mSession, RequestQueue mRequestQueue, final ProgressBar progressBar) {
        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_GET_SUBSCRIBED_ARTISTS);
        params.put(User.Keys.PARAMS_USER_ID, mSession.getUser().getUserID());
        params.put(User.Keys.PARAMS_DEVICE_ID, mSession.getUser().getDeviceID());
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String jsonString) {
                if (progressBar != null)
                    progressBar.setVisibility(View.GONE);

                List<Artist> subscribedArtists = ArtistTranslator.getArtistList(jsonString);
                mSession.setSubscribedArtistList(subscribedArtists);
                Intent i = new Intent(mActivity, activityToGoTo);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mActivity.startActivity(i);
                mActivity.finish();
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (progressBar != null)
                    progressBar.setVisibility(View.GONE);
                Log.e("Volley Error", "Couldn't request subscribed artists0."); //NON-NLS
                error.printStackTrace();
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }*/
    public static void requestSendChatMessage(Activity mActivity, Context mContext, String artistId,
                                              String message, LVSession mSession, RequestQueue mRequestQueue,
                                              final DataAvailabilityHandler handler) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_SEND_CHAT_MESSAGE);
        params.put(ArtistChatMessage.Keys.MESSAGE, message);
        params.put(ArtistChatMessage.Keys.MESSAGE_USERS_ID, mSession.getUser().getUserID());
        params.put(ArtistChatMessage.Keys.MESSAGE_ARTIST_ID, artistId);
        params.put(User.Keys.PARAMS_DEVICE_ID, mSession.getUser().getDeviceID());
        params.put(ArtistChatMessage.Keys.USER_ID, mSession.getUser().getUserID());
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String jsonString) {
                handler.RunCompleted(true);
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Request Error", "Error sending chat message."); //NON-NLS
                error.printStackTrace();
                handler.RunCompleted(true);
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requestSendStreamChatMessage(Activity mActivity, Context mContext,
                                                    String streamId, String artistId, String message,
                                                    LVSession mSession, RequestQueue mRequestQueue,
                                                    final DataAvailabilityHandler handler) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_SEND_CHAT_MESSAGE);
        params.put(ArtistChatMessage.Keys.MESSAGE, message);
        params.put(ArtistChatMessage.Keys.MESSAGE_USERS_ID, mSession.getUser().getUserID());
        params.put(ArtistChatMessage.Keys.MESSAGE_ARTIST_ID, artistId);
        params.put(User.Keys.PARAMS_DEVICE_ID, mSession.getUser().getDeviceID());
        params.put(ArtistChatMessage.Keys.USER_ID, mSession.getUser().getUserID());
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);
        params.put(Stream.Keys.STREAM_ID, streamId);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String jsonString) {
                handler.RunCompleted(true);
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Request Error", "Error sending chat message."); //NON-NLS
                error.printStackTrace();
                handler.RunCompleted(true);
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requestAddArtistToSubscriptions(Activity mActivity, Context mContext, LVSession mSession, final String RID, final String token, RequestQueue mRequestQueue, final DataAvailabilityHandler handler) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_NEW_SUBSCRIPTION);
        params.put(Stream.Keys.RID, RID);
        params.put(User.Keys.PARAMS_USER_ID, mSession.getUser().getUserID());
        params.put(User.Keys.PARAMS_DEVICE_ID, mSession.getUser().getDeviceID());
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);
        params.put(Constants.TOKEN_PARAM, token);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    String result = JSONHelper.getString(json, CODE_KEY);
                    if (result.equalsIgnoreCase("0")) {
                        handler.RunCompleted(true);
                    } else {
                        handler.RunCompleted(false);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    handler.RunCompleted(false);
                }
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "Error requesting add artist to subscriptions.");
                handler.RunCompleted(false);
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requestAddHeroId(Activity mActivity, Context mContext, LVSession mSession, final String RID, RequestQueue mRequestQueue, final Runnable subscribeRunnable) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_SET_HERO_ARTIST);
        params.put(Stream.Keys.RID, RID);
        params.put(User.Keys.PARAMS_USER_ID, mSession.getUser().getUserID());
        params.put(User.Keys.PARAMS_DEVICE_ID, mSession.getUser().getDeviceID());
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (subscribeRunnable != null)
                    subscribeRunnable.run();
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "Error requesting add artist to subscriptions.");
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requestFreeArtistSubscription(Activity mActivity, Context mContext, LVSession mSession, final String RID, RequestQueue mRequestQueue, final DataAvailabilityHandler handler) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_NEW_SUBSCRIPTION);
        params.put(Stream.Keys.RID, RID);
        params.put(User.Keys.PARAMS_USER_ID, mSession.getUser().getUserID());
        params.put(User.Keys.PARAMS_DEVICE_ID, mSession.getUser().getDeviceID());
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    String result = JSONHelper.getString(json, CODE_KEY);
                    if (result.equalsIgnoreCase("0")) {
                        handler.RunCompleted(true);
                    } else {
                        handler.RunCompleted(false);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    handler.RunCompleted(false);
                }
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "Error requesting add artist to subscriptions.");
                handler.RunCompleted(false);
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requestEnterStream(LVSession mSession, Activity mActivity, Context mContext,
                                          String streamId, RequestQueue mRequestQueue,
                                          final EnterStreamHandler handler) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_ENTER_STREAM);
        params.put(Stream.Keys.STREAM_ID, streamId);
        params.put(User.Keys.PARAMS_USER_ID, mSession.getUser().getUserID());
        params.put(User.Keys.PARAMS_DEVICE_ID, mSession.getUser().getDeviceID());
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                handler.RunCompleted(EnterStreamDataTranslator.getEnterStreamData(response));
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "Error requesting enter stream.");
                handler.RunCompleted(null);
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    //Logs in the broadcaster the hacky way
    public static void requestBroadcasterHackLogin(LVSession mSession, Activity mActivity, final Context mContext, String userId, String deviceId, RequestQueue mRequestQueue, final LoginHandler handler) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_LOGIN_ARTIST);
        params.put(User.Keys.PARAMS_USER_ID, userId);
        params.put(User.Keys.PARAMS_DEVICE_ID, deviceId);
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);
        params.put(User.Keys.LANG, Locale.getDefault().toString());
        params.put(User.Keys.TIME_ZONE, TimeZone.getDefault().getID());

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    Map<String, Object> map = JSONHelper.JSONToMap(json);

                    if (map == null) {
                        handler.RunCompleted(Constants.USERCODE_OTHER_ERROR, null);
                    } else if (map.containsKey(Constants.API_ERROR_CODE)) {
                        int errorCode = (int) map.get(Constants.API_ERROR_CODE);
                        if (errorCode == 40)
                            Preferences.setPreference(Constants.KEY_PHONE, (String) map.get(AppConstants.PARAM_INFO), mContext);
                        handler.RunCompleted(errorCode, null);
                    } else {
                        User user = UserTranslator.getUser(json);
                        if (UserValidator.isValid(user))
                            handler.RunCompleted(Constants.USERCODE_SUCCESS, user);
                        else
                            handler.RunCompleted(Constants.USERCODE_INVALID_RESPONSE, null);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    handler.RunCompleted(Constants.USERCODE_INVALID_RESPONSE, null);
                }
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "Error requesting broadcaster login.");
                handler.RunCompleted(Constants.USERCODE_INVALID_RESPONSE, null);
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }//End of hacky login

    public static void requestBigDataInfo(LVSession mSession, Activity mActivity, Context mContext, String userID, String deviceID, RequestQueue mRequestQueue, final BigDataHandler handler) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_GET_BIG_DATA_INFO);
        params.put(User.Keys.PARAMS_USER_ID, userID);
        params.put(User.Keys.PARAMS_DEVICE_ID, deviceID);
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    Map<String, Object> map = JSONHelper.JSONToMap(json);

                    if (map == null) {
                        handler.RunCompleted(Constants.USERCODE_OTHER_ERROR, null);
                    } else if (map.containsKey(Constants.API_ERROR_CODE)) {
                        handler.RunCompleted((int) map.get(Constants.API_ERROR_CODE), null);
                    } else {
                        handler.RunCompleted(Constants.USERCODE_SUCCESS, map);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    handler.RunCompleted(Constants.USERCODE_INVALID_RESPONSE, null);
                }
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "Error requesting get subscription count.");
                handler.RunCompleted(Constants.USERCODE_INVALID_RESPONSE, null);
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requestPhoneNumberLogin(Activity mActivity, final Context mContext, String phoneNumber, String pw, String userID, String deviceID, LVSession mSession, RequestQueue mRequestQueue, final LoginHandler handler) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_NORMAL_LOGIN);
        params.put(User.Keys.PARAMS_EMAIL, phoneNumber);
        params.put(User.Keys.PARAMS_PASSWORD, pw);
        params.put(User.Keys.PARAMS_IS_PHONE, Constants.DEVICE_ANDROID);
        params.put(User.Keys.PARAMS_USER_ID, userID);
        params.put(User.Keys.PARAMS_DEVICE_ID, deviceID);
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);
        params.put(User.Keys.LANG, Locale.getDefault().toString());
        params.put(User.Keys.TIME_ZONE, TimeZone.getDefault().getID());

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    Map<String, Object> map = JSONHelper.JSONToMap(json);

                    if (map == null) {
                        handler.RunCompleted(Constants.USERCODE_OTHER_ERROR, null);
                    } else if (map.containsKey(Constants.API_ERROR_CODE)) {
                        int errorCode = (int) map.get(Constants.API_ERROR_CODE);
                        if (errorCode == 40)
                            Preferences.setPreference(Constants.KEY_PHONE, (String) map.get(AppConstants.PARAM_INFO), mContext);
                        handler.RunCompleted(errorCode, null);
                    } else {
                        User user = UserTranslator.getUser(json);
                        if (UserValidator.isValid(user))
                            handler.RunCompleted(Constants.USERCODE_SUCCESS, user);
                        else
                            handler.RunCompleted(Constants.USERCODE_INVALID_RESPONSE, null);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    handler.RunCompleted(Constants.USERCODE_INVALID_RESPONSE, null);
                }
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "Error requesting phone number login.");
                handler.RunCompleted(Constants.USERCODE_INVALID_RESPONSE, null);
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requestImageUpload(LVSession mSession, Activity mActivity, Context mContext,
                                          String userId, String deviceId, String newPictureId, RequestQueue mRequestQueue) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_UPLOAD_PICTURE);
        params.put(User.Keys.PARAMS_USER_ID, userId);
        params.put(User.Keys.PARAMS_DEVICE_ID, deviceId);
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);
        params.put(UserImage.Keys.PICTURE_ID, newPictureId);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "Error requesting image upload.");
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requestSetUserData(Activity mActivity, final LVSession mSession, final String displayName, RequestQueue mRequestQueue, Context mContext, final DataAvailabilityHandler handler) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_SET_USER_DATA);
        params.put(User.Keys.PARAMS_USER_ID, mSession.getUser().getUserID());
        params.put(User.Keys.PARAMS_DEVICE_ID, mSession.getUser().getDeviceID());
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);
        params.put(User.Keys.DISPLAY_NAME, displayName);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mSession.getUser().setDisplayName(displayName);
                handler.RunCompleted(true);
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "Error requesting set user data.");
                handler.RunCompleted(false);
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requestSetArtistData(Activity mActivity, Context mContext, final String artistType, String email, String isAllAges, String shortURL, final LVSession mSession, RequestQueue mRequestQueue, final DataAvailabilityHandler handler) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_SET_ARTIST_DATA);
        params.put(User.Keys.PARAMS_USER_ID, mSession.getUser().getUserID());
        params.put(User.Keys.PARAMS_DEVICE_ID, mSession.getUser().getDeviceID());
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);
        params.put(Artist.Keys.ARTIST_TYPE, artistType);
        params.put(Artist.Keys.KEY_SHORT_URI, shortURL);
        params.put(Artist.Keys.IS_ALL_AGES, isAllAges);
        params.put(User.Keys.RANDOMIZE_PHOTO, String.valueOf(mSession.getUser().getPhotoPosition()));
        if (email != null)
            params.put(User.Keys.PARAMS_EMAIL, email);
        else
            params.put(User.Keys.PARAMS_EMAIL, "");

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mSession.getUser().setArtistType(artistType);
                handler.RunCompleted(true);
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "Error requesting set artist data.");
                handler.RunCompleted(false);
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requestChangePassword(String userId, String deviceId, Activity mActivity,
                                             String password, Context mContext, LVSession mSession,
                                             RequestQueue mRequestQueue, final DataAvailabilityHandler handler) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_CHANGE_PASSWORD);
        params.put(User.Keys.PARAMS_USER_ID, userId);
        params.put(User.Keys.PARAMS_DEVICE_ID, deviceId);
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);
        params.put(User.Keys.PARAMS_PASSWORD, password);
        params.put(User.Keys.PARAMS_PHONE, Preferences.getPreference(Constants.KEY_PHONE, mContext, ""));

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                handler.RunCompleted(true);
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "Error requesting change password.");
                handler.RunCompleted(false);
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requestCachedStreams(Activity mActivity, Context mContext, LVSession mSession, RequestQueue mRequestQueue, final MyStreamsRequestHandeler handler) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_GET_CACHED_STREAMS);
        params.put(User.Keys.PARAMS_USER_ID, mSession.getUser().getUserID());
        params.put(User.Keys.PARAMS_DEVICE_ID, mSession.getUser().getDeviceID());
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    List<Stream> streamList = StreamTranslator.getStreams(json.toString());
                    handler.RunCompleted(streamList);
                } catch (JSONException e) {
                    e.printStackTrace();
                    handler.RunCompleted(null);
                }
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "In request cached streams.");
                handler.RunCompleted(null);
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    /**
     * register new video to upload
     */
    public static void requestNewStream(LVSession mSession, Activity mActivity, Context mContext, String userID, String deviceID, JSONObject stream, String time, Response.Listener<String> response, Response.ErrorListener error, RequestQueue mRequestQueue) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_NEW_STREAM);
        params.put(Constants.PARAM_STREAM_KEY_STREAM, stream.toString());
        params.put(Constants.PARAM_STREAM_KEY_IS_DELAYED, "1");
        params.put(ArtistUploadVideoTranslator.PARAM_STREAM_JSON_KEY_TIME, time);
        params.put(User.Keys.PARAMS_USER_ID, userID);
        params.put(User.Keys.PARAMS_DEVICE_ID, deviceID);
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    /**
     * Regester for a live stream
     */
    public static void requestLiveStream(LVSession mSession, Activity mActivity, Context mContext, String userID, String deviceID, JSONObject stream, Response.Listener<String> response, Response.ErrorListener error, RequestQueue mRequestQueue) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_NEW_STREAM);
        params.put(Constants.PARAM_STREAM_KEY_STREAM, stream.toString());
        params.put(User.Keys.PARAMS_USER_ID, userID);
        params.put(User.Keys.PARAMS_DEVICE_ID, deviceID);
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void broadcastGoingLive(Activity mActivity, Context mContext, LVSession mSession, String streamId, RequestQueue mRequestQueue, Response.Listener<String> response, Response.ErrorListener error) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_PUSH_STREAM_NOTIFICATION);
        params.put(User.Keys.PARAMS_USER_ID, mSession.getUser().getUserID());
        params.put(User.Keys.PARAMS_DEVICE_ID, mSession.getUser().getDeviceID());
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);
        params.put(Stream.Keys.STREAM_ID, streamId);

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    /**
     * notify fans that a new video has been uploaded
     */
    public static void requestPushNotificationNewStream(LVSession mSession, Activity mActivity, Context mContext, String userID, String deviceID, String streamID, Response.Listener<String> response, Response.ErrorListener error, RequestQueue mRequestQueue) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_PUSH_STREAM_NOTIFICATION);
        params.put(Constants.PARAM_STREAM_KEY_STREAM_ID, streamID);
        params.put(User.Keys.PARAMS_USER_ID, userID);
        params.put(User.Keys.PARAMS_DEVICE_ID, deviceID);
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    /**
     * creates thumbnail for the uploaded video
     */
    public static void requestCreateThumbnail(LVSession mSession, Activity mActivity, Context mContext, String userID, String deviceID, String streamID, Response.Listener<String> response, Response.ErrorListener error, RequestQueue mRequestQueue) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_CREATE_THUMBNAIL);
        params.put(Constants.PARAM_STREAM_KEY_STREAM_ID, streamID);
        params.put(User.Keys.PARAMS_USER_ID, userID);
        params.put(User.Keys.PARAMS_DEVICE_ID, deviceID);
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requestGCMRegistration(String userID, String deviceID, Activity mActivity, Context mContext, String token, LVSession mSession, RequestQueue mRequestQueue) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        final Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_SET_DEVICE_TOKEN);
        params.put(User.Keys.PARAMS_USER_ID, userID);
        params.put(User.Keys.PARAMS_DEVICE_ID, deviceID);
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);
        params.put(Constants.DEVICE_TYPE_PARAM, Constants.DEVICE_ANDROID);
        params.put(Constants.DEBUG_PARAM, "0");
        params.put(Constants.TOKEN_PARAM, token);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("GCM Server Response", "Successfull");
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("GCM Server Response", "Unsuccessfull");
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requestUpdateUserLocation(Activity mActivity, Context mContext, String countryName, String locality, String state, LVSession mSession, RequestQueue mRequestQueue) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_UPDATE_USER_LOCATION);
        params.put(User.Keys.PARAMS_USER_ID, Preferences.getPreference(Constants.KEY_USER_ID, mContext, ""));
        params.put(User.Keys.PARAMS_DEVICE_ID, Preferences.getPreference(Constants.KEY_DEVICE, mContext, ""));
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);
        params.put(Constants.PARAMS_NOTIFICATION_KEY_COUNTRY, countryName);
        params.put(Constants.PARAMS_NOTIFICATION_KEY_CITY, locality);
        params.put("State", state);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Location Response", "Successfull");
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Location Error", "Unsuccessful");
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requestLogout(LVSession mSession, Activity mActivity, Context mContext, String userId, String deviceId, RequestQueue mRequestQueue, final Runnable mRunnable) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_LOGOUT);
        params.put(User.Keys.PARAMS_USER_ID, userId);
        params.put(User.Keys.PARAMS_DEVICE_ID, deviceId);
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (mRunnable != null)
                    mRunnable.run();
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "Error requesting logout.");
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requestResetBadge(Activity mActivity, Context mContext, LVSession mSession, RequestQueue mRequestQueue) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_RESET_BADGE);
        params.put(User.Keys.PARAMS_USER_ID, mSession.getUser().getUserID());
        params.put(User.Keys.PARAMS_DEVICE_ID, mSession.getUser().getDeviceID());
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                // Do nothing
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "Error requesting reset badge.");
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requestLoginUser(Activity mActivity, String userId, String deviceId, final Context mContext, LVSession mSession, RequestQueue mRequestQueue, final LoginHandler handler) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_LOGIN_USER);
        params.put(User.Keys.PARAMS_USER_ID, userId);
        params.put(User.Keys.PARAMS_DEVICE_ID, deviceId);
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);
        params.put(User.Keys.LANG, Locale.getDefault().toString());
        params.put(User.Keys.TIME_ZONE, TimeZone.getDefault().getID());

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    Map<String, Object> map = JSONHelper.JSONToMap(json);

                    if (map == null) {
                        handler.RunCompleted(Constants.USERCODE_OTHER_ERROR, null);
                    } else if (map.containsKey(Constants.API_ERROR_CODE)) {
                        int errorCode = (int) map.get(Constants.API_ERROR_CODE);
                        if (errorCode == 40)
                            Preferences.setPreference(Constants.KEY_PHONE, (String) map.get(AppConstants.PARAM_INFO), mContext);
                        handler.RunCompleted(errorCode, null);
                    } else {
                        User user = UserTranslator.getUser(json);
                        if (UserValidator.isValid(user))
                            handler.RunCompleted(Constants.USERCODE_SUCCESS, user);
                        else
                            handler.RunCompleted(Constants.USERCODE_INVALID_RESPONSE, null);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    handler.RunCompleted(Constants.USERCODE_INVALID_RESPONSE, null);
                }
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "Error requesting login user.");
                handler.RunCompleted(Constants.USERCODE_INVALID_RESPONSE, null);
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requestAddToFavorites(Activity mActivity, Context mContext, String artistId, LVSession mSession, RequestQueue mRequestQueue, final DataAvailabilityHandler handler) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_ADD_ARTIST_TO_FAVORITES);
        params.put(User.Keys.PARAMS_USER_ID, mSession.getUser().getUserID());
        params.put(User.Keys.PARAMS_DEVICE_ID, mSession.getUser().getDeviceID());
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);
        params.put(Stream.Keys.ARTIST_ID, artistId);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject json = new JSONObject(response);
                    String result = JSONHelper.getString(json, CODE_KEY);

                    if (result.equalsIgnoreCase("1")) {
                        if(handler != null)
                            handler.RunCompleted(true);
                    } else {
                        if(handler != null)
                            handler.RunCompleted(false);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    handler.RunCompleted(false);
                }
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "Error requesting add to favorites.");
                if (handler != null)
                    handler.RunCompleted(false);
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requestRemoveFromFavorites(Activity mActivity, Context mContext, String artistId, LVSession mSession, RequestQueue mRequestQueue, final DataAvailabilityHandler handler) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_REMOVE_ARTIST_FROM_FAVORITES);
        params.put(User.Keys.PARAMS_USER_ID, mSession.getUser().getUserID());
        params.put(User.Keys.PARAMS_DEVICE_ID, mSession.getUser().getDeviceID());
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);
        params.put(Stream.Keys.ARTIST_ID, artistId);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    String result = JSONHelper.getString(json, CODE_KEY);

                    if (result.equalsIgnoreCase("1")) {
                        if(handler != null)
                            handler.RunCompleted(true);
                    } else {
                        if(handler != null)
                            handler.RunCompleted(false);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    handler.RunCompleted(false);
                }
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "Error requesting remove from favorites.");
                if (handler != null)
                    handler.RunCompleted(false);
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requestCheckStreamAllowed(LVSession mSession, Activity mActivity, Context mContext, RequestQueue mRequestQueue, final CheckStreamAllowedHandler mHandler) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        final Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_CHECK_STREAM_ALLOWED);
        params.put(User.Keys.PARAMS_USER_ID, mSession.getUser().getUserID());
        params.put(User.Keys.PARAMS_DEVICE_ID, mSession.getUser().getDeviceID());
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject result = new JSONObject(response);
                    mHandler.RunCompleted(result.getInt(com.app.livideo.utils.Constants.KEY_STREAM_SECONDS_LEFT));
                } catch (JSONException e) {
                    e.printStackTrace();
                    mHandler.RunCompleted(0);
                }
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY_ERROR", "Checking stream allowed: " + params.toString());
                error.printStackTrace();
                mHandler.RunCompleted(0);
            }
        };


        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requestExportReport(Activity mActivity, final Context mContext, LVSession mSession, RequestQueue mRequestQueue, final BigDataLinkHandler handler) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_EXPORT_REPORT);
        params.put(User.Keys.PARAMS_USER_ID, mSession.getUser().getUserID());
        params.put(User.Keys.PARAMS_DEVICE_ID, mSession.getUser().getDeviceID());
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);
        params.put(Constants.BIG_DATA_STATUS_REQUEST, Constants.BIG_DATA_STATUS);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject result = new JSONObject(response);
                    DisplayMetrics dm = mContext.getResources().getDisplayMetrics();
                    String translatedUrl = result.getString(Constants.BIG_DATA_LINK)
                            .replace("%%width%%", String.valueOf(dm.widthPixels / (int) dm.density))
                            .replace("%%height%%", String.valueOf(dm.heightPixels / (int) dm.density));
                    handler.RunCompleted(translatedUrl);
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "Error requesting remove from favorites.");
                handler.RunCompleted("");
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requestEndStream(Activity mActivity, Context mContext, Stream currentStream, LVSession mSession, RequestQueue mRequestQueue) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_END_STREAM);
        params.put(User.Keys.PARAMS_USER_ID, mSession.getUser().getUserID());
        params.put(User.Keys.PARAMS_DEVICE_ID, mSession.getUser().getDeviceID());
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);
        params.put(Stream.Keys.STREAM_ID, currentStream.getStreamId());
        params.put(Stream.Keys.WATCHING_ID, String.valueOf(currentStream.getWatchingId()));

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                // Do nothing
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "Error requesting end stream.");
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requstLogInvites(Activity mActivity, Context mContext, LVSession mSession, RequestQueue mRequestQueue, String inviteType, String contactsList) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_LOG_INVITES);
        params.put(User.Keys.PARAMS_USER_ID, mSession.getUser().getUserID());
        params.put(User.Keys.PARAMS_DEVICE_ID, mSession.getUser().getDeviceID());
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);
        params.put(Constants.PARAMETER_INVITATION_TYPE, inviteType);
        params.put(Constants.PARAMETER_INVITEES, contactsList);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                // Do nothing
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "Error requesting end stream.");
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requestStreamInfo(Activity mActivity, Context mContext, String streamId, LVSession mSession, RequestQueue mRequestQueue, final ActiveStreamHandler handler) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_GET_STREAM_DATA);
        params.put(User.Keys.PARAMS_USER_ID, mSession.getUser().getUserID());
        params.put(User.Keys.PARAMS_DEVICE_ID, mSession.getUser().getDeviceID());
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);
        params.put(Stream.Keys.STREAM_ID, streamId);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Stream mStream = StreamTranslator.getStream(jsonObject);
                    if (StreamValidator.isValid(mStream))
                        handler.RunCompleted(true, mStream);
                    else
                        handler.RunCompleted(false, null);
                } catch (JSONException e) {
                    e.printStackTrace();
                    handler.RunCompleted(false, null);
                }
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "Error requesting stream info.");
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requestArtistInfo(String artistId, Activity mActivity, Context mContext, LVSession mSession, RequestQueue mRequestQueue, final ArtistHandler handler) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_GET_ARTIST_DATA);
        params.put(User.Keys.PARAMS_USER_ID, mSession.getUser().getUserID());
        params.put(User.Keys.PARAMS_DEVICE_ID, mSession.getUser().getDeviceID());
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);
        params.put(Stream.Keys.RID, artistId);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Artist mArtist = ArtistTranslator.getArtist(jsonObject);
                    if (ArtistValidator.isValid(mArtist)) {
                        handler.RunCompleted(true, mArtist);
                    } else {
                        handler.RunCompleted(false, null);
                    }
                } catch (JSONException e) {
                    Log.e("Parsing Error", "Couldnt parse artist info");
                }
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "Error requesting artist info.");
                handler.RunCompleted(false, null);
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requestRemoveStream(Activity mActivity, final Context mContext, Stream mStream,
                                           LVSession mSession, RequestQueue mRequestQueue,
                                           final DataAvailabilityHandler handler) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_REMOVE_STREAM);
        params.put(User.Keys.PARAMS_USER_ID, mSession.getUser().getUserID());
        params.put(User.Keys.PARAMS_DEVICE_ID, mSession.getUser().getDeviceID());
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);
        params.put(Stream.Keys.STREAM_ID, mStream.getStreamId());

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    Map<String, Object> map = JSONHelper.JSONToMap(json);

                    if (map == null) {
                        handler.RunCompleted(false);
                    } else if (map.containsKey(CODE_KEY)) {
                        if ((int) map.get(CODE_KEY) == 0)
                            handler.RunCompleted(true);
                        else
                            handler.RunCompleted(false);
                    } else {
                        handler.RunCompleted(false);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("JSON Error", "Error requesting remove stream.");
                    handler.RunCompleted(false);
                }
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "Error requesting remove stream.");
                handler.RunCompleted(false);
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void SendAWSIdentity(final Context mContext, final RequestQueue mRequestQueue, final LVSession mSession) {
        LiVideoCognitoIdentityProvider developerProvider = new LiVideoCognitoIdentityProvider(null, "us-east-1:4095ea44-6e1a-4545-9647-985e6a2aa0eb", Regions.US_EAST_1, mContext, mRequestQueue);
        CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(mContext, developerProvider, Regions.US_EAST_1);
        mSession.setAWSCredentials(credentialsProvider);
    }

    public static void SendUserLocation(final Activity mActivity, final Context mContext, final LVSession mSession, final RequestQueue mRequestQueue) {
        new LocationUtils(mContext, new LocationUtils.OnLocationListener() {
            @Override
            public void onGetLocation(Location location, Address address, LatLng latitudeLongitude) {
                if (address != null)
                    APICalls.requestUpdateUserLocation(mActivity, mContext, address.getCountryName(), address.getSubLocality(), address.getAdminArea(), mSession, mRequestQueue);
            }

            @Override
            public void onErrorWhileGettingLocation(String message) {
            }
        });
    }

    public static void requestDiscoverPage(final Activity mActivity, final Context mContext, final LVSession mSession, final RequestQueue mRequestQueue, final DiscoverPageHandler handler) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        final Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_GET_DISCOVER_PAGE);
        params.put(User.Keys.PARAMS_USER_ID, Preferences.getPreference(Constants.KEY_USER_ID, mContext, ""));
        params.put(User.Keys.PARAMS_DEVICE_ID, Preferences.getPreference(Constants.KEY_DEVICE, mContext, ""));
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    Map<String, Object> map = JSONHelper.JSONToMap(json);

                    if (map == null) {
                        handler.RunCompleted(Constants.USERCODE_OTHER_ERROR, null);
                    } else if (map.containsKey(Constants.API_ERROR_CODE)) {
                        int errorCode = (int) map.get(Constants.API_ERROR_CODE);
                        if (errorCode == 40)
                            Preferences.setPreference(Constants.KEY_PHONE, (String) map.get(AppConstants.PARAM_INFO), mContext);
                        handler.RunCompleted(errorCode, null);
                    } else if (map.containsKey(CODE_KEY)) {
                        handler.RunCompleted((int) map.get(CODE_KEY), null);
                    } else {
                        Discover mDiscover = DiscoverTranslator.getDiscover(response);
                        if (DiscoverValidator.isValid(mDiscover)) {
                            handler.RunCompleted(Constants.USERCODE_SUCCESS, mDiscover);
                        } else {
                            handler.RunCompleted(-1, null);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("JSON Error", "Error requesting discover page.");
                    handler.RunCompleted(Constants.USERCODE_INVALID_RESPONSE, null);
                }
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "Discover Page: " + params.toString());
                handler.RunCompleted(Constants.USERCODE_INVALID_RESPONSE, null);
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requestStarStream(final String streamID, final String didStar,
                                         final Activity mActivity, final Context mContext,
                                         final LVSession mSession, final RequestQueue mRequestQueue,
                                         final StarStreamHandler handler) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        final Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_STAR_UNSTAR_STREAM);
        params.put(User.Keys.PARAMS_USER_ID, mSession.getUser().getUserID());
        params.put(User.Keys.PARAMS_DEVICE_ID, mSession.getUser().getDeviceID());
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);
        params.put(Stream.Keys.STREAM_ID, streamID);
        params.put(Stream.Keys.DO_STAR, didStar);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    Map<String, Object> map = JSONHelper.JSONToMap(json);

                    if (map == null) {
                        handler.RunCompleted(Constants.USERCODE_OTHER_ERROR, "0");
                    } else if (map.containsKey(Constants.API_ERROR_CODE)) {
                        int errorCode = (int) map.get(Constants.API_ERROR_CODE);
                        if (errorCode == 40)
                            Preferences.setPreference(Constants.KEY_PHONE, (String) map.get(AppConstants.PARAM_INFO), mContext);
                        handler.RunCompleted(errorCode, "0");
                    } else if (map.containsKey(CODE_KEY)) {
                        handler.RunCompleted((int) map.get(CODE_KEY), "0");
                    } else {
                        handler.RunCompleted(Constants.USERCODE_SUCCESS, JSONHelper.getString(json, Stream.Keys.STARS));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("JSON Error: star stream", params.toString());
                    handler.RunCompleted(Constants.USERCODE_INVALID_RESPONSE, "0");
                }
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "Error requesting star stream.");
                handler.RunCompleted(Constants.USERCODE_INVALID_RESPONSE, "0");
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requestGetStreamStars(final String streamID,
                                             final Activity mActivity, final Context mContext,
                                             final LVSession mSession, final RequestQueue mRequestQueue,
                                             final StarStreamHandler handler) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_GET_STREAM_STARS);
        params.put(User.Keys.PARAMS_USER_ID, mSession.getUser().getUserID());
        params.put(User.Keys.PARAMS_DEVICE_ID, mSession.getUser().getDeviceID());
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);
        params.put(Stream.Keys.STREAM_ID, streamID);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    Map<String, Object> map = JSONHelper.JSONToMap(json);

                    if (map == null) {
                        handler.RunCompleted(Constants.USERCODE_OTHER_ERROR, "0");
                    } else if (map.containsKey(Constants.API_ERROR_CODE)) {
                        int errorCode = (int) map.get(Constants.API_ERROR_CODE);
                        if (errorCode == 40)
                            Preferences.setPreference(Constants.KEY_PHONE, (String) map.get(AppConstants.PARAM_INFO), mContext);
                        handler.RunCompleted(errorCode, "0");
                    } else if (map.containsKey(CODE_KEY)) {
                        handler.RunCompleted((int) map.get(CODE_KEY), "0");
                    } else {
                        handler.RunCompleted(Constants.USERCODE_SUCCESS, JSONHelper.getString(json, Stream.Keys.STARS));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("JSON Error", "Error requesting get star streams.");
                    handler.RunCompleted(Constants.USERCODE_INVALID_RESPONSE, "0");
                }
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "Error requesting get stream stars.");
                handler.RunCompleted(Constants.USERCODE_INVALID_RESPONSE, "0");
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requestArtistPage(final String artistId, final Activity mActivity, final Context mContext,
                                         LVSession mSession, RequestQueue mRequestQueue, final ArtistPageHandler handler) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_GET_ARTIST_PAGE);
        params.put(User.Keys.PARAMS_USER_ID, Preferences.getPreference(Constants.KEY_USER_ID, mContext, ""));
        params.put(User.Keys.PARAMS_DEVICE_ID, Preferences.getPreference(Constants.KEY_DEVICE, mContext, ""));
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);
        params.put(Stream.Keys.RID, artistId);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    ArtistPage mArtistPage = ArtistPageTranslator.getArtistPage(jsonObject);
                    if (ArtistPageValidator.isValid(mArtistPage)) {
                        handler.RunCompleted(true, mArtistPage);
                    } else {
                        handler.RunCompleted(false, null);
                    }
                } catch (JSONException e) {
                    Log.e("Parsing Error", "Couldnt parse artist info");
                }
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "Error requesting artist info.");
                handler.RunCompleted(false, null);
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requestMePage(String lastStreamID, FragmentActivity mActivity, final Context mContext, LVSession mSession, final RequestQueue mRequestQueue, final MyTimeLineHandler handler) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_GET_ME_PAGE);
        params.put(User.Keys.PARAMS_USER_ID, mSession.getUser().getUserID());
        params.put(User.Keys.PARAMS_DEVICE_ID, mSession.getUser().getDeviceID());
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);

        params.put(Stream.Keys.STREAM_ID, lastStreamID);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    Map<String, Object> map = JSONHelper.JSONToMap(json);

                    if (map == null) {
                        handler.RunCompleted(Constants.USERCODE_OTHER_ERROR, null);
                    } else if (map.containsKey(Constants.API_ERROR_CODE)) {
                        int errorCode = (int) map.get(Constants.API_ERROR_CODE);
                        if (errorCode == 40)
                            Preferences.setPreference(Constants.KEY_PHONE, (String) map.get(AppConstants.PARAM_INFO), mContext);
                        handler.RunCompleted(errorCode, null);
                    } else if (map.containsKey(CODE_KEY)) {
                        handler.RunCompleted((int) map.get(CODE_KEY), StreamTranslator.getStreams(response));
                    } else {
                        handler.RunCompleted(Constants.USERCODE_SUCCESS, StreamTranslator.getStreams(response));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("JSON Error", "Error requesting me page.");
                    handler.RunCompleted(Constants.USERCODE_INVALID_RESPONSE, null);
                }
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "Error requesting me page.");
                handler.RunCompleted(Constants.USERCODE_INVALID_RESPONSE, null);
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requestSearchData(Activity mActivity, final Context mContext, LVSession mSession,
                                         RequestQueue mRequestQueue, final SearchDataHandler handler) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_GET_SEARCH_DATA);
        params.put(User.Keys.PARAMS_USER_ID, mSession.getUser().getUserID());
        params.put(User.Keys.PARAMS_DEVICE_ID, mSession.getUser().getDeviceID());
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    Map<String, Object> map = JSONHelper.JSONToMap(json);

                    if (map == null) {
                        handler.RunCompleted(Constants.USERCODE_OTHER_ERROR, null);
                    } else if (map.containsKey(Constants.API_ERROR_CODE)) {
                        int errorCode = (int) map.get(Constants.API_ERROR_CODE);
                        if (errorCode == 40)
                            Preferences.setPreference(Constants.KEY_PHONE, (String) map.get(AppConstants.PARAM_INFO), mContext);
                        handler.RunCompleted(errorCode, null);
                    } else if (map.containsKey(CODE_KEY)) {
                        handler.RunCompleted((int) map.get(CODE_KEY), SearchDataTranslator.getSearchData(json));
                    } else {
                        handler.RunCompleted(Constants.USERCODE_SUCCESS, SearchDataTranslator.getSearchData(json));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("JSON Error", "Error requesting search data.");
                    handler.RunCompleted(Constants.USERCODE_INVALID_RESPONSE, null);
                }
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "Error requesting search data.");
                handler.RunCompleted(Constants.USERCODE_INVALID_RESPONSE, null);
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requestBlockUser(String blockUserID, Activity mActivity, final Context mContext, LVSession mSession,
                                        RequestQueue mRequestQueue, final CodeNumberHandler handler) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_BLOCK_USER);
        params.put(User.Keys.PARAMS_USER_ID, mSession.getUser().getUserID());
        params.put(User.Keys.PARAMS_DEVICE_ID, mSession.getUser().getDeviceID());
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);
        params.put(Stream.Keys.RID, blockUserID);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    Map<String, Object> map = JSONHelper.JSONToMap(json);

                    if (map == null) {
                        handler.RunCompleted(Constants.USERCODE_OTHER_ERROR);
                    } else if (map.containsKey(Constants.API_ERROR_CODE)) {
                        int errorCode = (int) map.get(Constants.API_ERROR_CODE);
                        if (errorCode == 40)
                            Preferences.setPreference(Constants.KEY_PHONE, (String) map.get(AppConstants.PARAM_INFO), mContext);
                        handler.RunCompleted(errorCode);
                    } else if (map.containsKey(CODE_KEY)) {
                        handler.RunCompleted((int) map.get(CODE_KEY));
                    } else {
                        handler.RunCompleted(Constants.USERCODE_SUCCESS);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("JSON Error", "Error requesting block user.");
                    handler.RunCompleted(Constants.USERCODE_INVALID_RESPONSE);
                }
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "Error requesting block user.");
                handler.RunCompleted(Constants.USERCODE_INVALID_RESPONSE);
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requestUnblockUser(String blockedUserID, Activity mActivity, final Context mContext, LVSession mSession,
                                          RequestQueue mRequestQueue, final CodeNumberHandler handler) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_UNBLOCK_USER);
        params.put(User.Keys.PARAMS_USER_ID, mSession.getUser().getUserID());
        params.put(User.Keys.PARAMS_DEVICE_ID, mSession.getUser().getDeviceID());
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);
        params.put(Stream.Keys.RID, blockedUserID);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    if (handler == null)
                        return;
                    JSONObject json = new JSONObject(response);
                    Map<String, Object> map = JSONHelper.JSONToMap(json);

                    if (map == null) {
                        handler.RunCompleted(Constants.USERCODE_OTHER_ERROR);
                    } else if (map.containsKey(Constants.API_ERROR_CODE)) {
                        int errorCode = (int) map.get(Constants.API_ERROR_CODE);
                        if (errorCode == 40)
                            Preferences.setPreference(Constants.KEY_PHONE, (String) map.get(AppConstants.PARAM_INFO), mContext);
                        handler.RunCompleted(errorCode);
                    } else if (map.containsKey(CODE_KEY)) {
                        handler.RunCompleted((int) map.get(CODE_KEY));
                    } else {
                        handler.RunCompleted(Constants.USERCODE_SUCCESS);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("JSON Error", "Error requesting block user.");
                    if (handler != null)
                        handler.RunCompleted(Constants.USERCODE_INVALID_RESPONSE);
                }
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "Error requesting block user.");
                if (handler != null)
                    handler.RunCompleted(Constants.USERCODE_INVALID_RESPONSE);
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requestBlockedUsers(Activity mActivity, final Context mContext, LVSession mSession,
                                           RequestQueue mRequestQueue, final UserListHandler handler) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_GET_BLOCKED_USERS);
        params.put(User.Keys.PARAMS_USER_ID, mSession.getUser().getUserID());
        params.put(User.Keys.PARAMS_DEVICE_ID, mSession.getUser().getDeviceID());
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    Map<String, Object> map = JSONHelper.JSONToMap(json);

                    if (map == null) {
                        handler.RunCompleted(Constants.USERCODE_OTHER_ERROR, null);
                    } else if (map.containsKey(Constants.API_ERROR_CODE)) {
                        int errorCode = (int) map.get(Constants.API_ERROR_CODE);
                        if (errorCode == 40)
                            Preferences.setPreference(Constants.KEY_PHONE, (String) map.get(AppConstants.PARAM_INFO), mContext);
                        handler.RunCompleted(errorCode, null);
                    } else if (map.containsKey(CODE_KEY)) {
                        handler.RunCompleted((int) map.get(CODE_KEY), UserTranslator.getUsers(json));
                    } else {
                        handler.RunCompleted(Constants.USERCODE_SUCCESS, UserTranslator.getUsers(json));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("JSON Error", "Error requesting blocked users.");
                    handler.RunCompleted(Constants.USERCODE_INVALID_RESPONSE, null);
                }
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "Error requesting blocked users.");
                handler.RunCompleted(Constants.USERCODE_INVALID_RESPONSE, null);
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requestWhispers(Activity mActivity, final Context mContext, LVSession mSession,
                                       RequestQueue mRequestQueue, final WhisperRequestHandler handler) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_GET_WHISPER_REQUESTS);
        params.put(User.Keys.PARAMS_USER_ID, mSession.getUser().getUserID());
        params.put(User.Keys.PARAMS_DEVICE_ID, mSession.getUser().getDeviceID());
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    Map<String, Object> map = JSONHelper.JSONToMap(json);

                    if (map == null) {
                        handler.RunCompleted(Constants.USERCODE_OTHER_ERROR, null);
                    } else if (map.containsKey(Constants.API_ERROR_CODE)) {
                        int errorCode = (int) map.get(Constants.API_ERROR_CODE);
                        if (errorCode == 40)
                            Preferences.setPreference(Constants.KEY_PHONE, (String) map.get(AppConstants.PARAM_INFO), mContext);
                        handler.RunCompleted(errorCode, null);
                    } else if (map.containsKey(CODE_KEY)) {
                        handler.RunCompleted((int) map.get(CODE_KEY), WhisperTranslator.getWhispers(json));
                    } else {
                        handler.RunCompleted(Constants.USERCODE_SUCCESS, WhisperTranslator.getWhispers(json));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("JSON Error", "Error requesting whisper.");
                    handler.RunCompleted(Constants.USERCODE_INVALID_RESPONSE, null);
                }
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "Error requesting whispers.");
                handler.RunCompleted(Constants.USERCODE_INVALID_RESPONSE, null);
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void sendPrivateChatMessage(final Activity mActivity, final Context mContext, String privateUserID, String msgType,
                                              String message, LVSession mSession, RequestQueue mRequestQueue,
                                              final CodeNumberHandler handler) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_WRITE_PRIVATE_CHAT);

        params.put(ArtistChatMessage.Keys.MESSAGE, message);
        params.put(Stream.Keys.RID, privateUserID);
        params.put("Type", msgType);

        params.put(User.Keys.USER_ID, mSession.getUser().getUserID());
        params.put(User.Keys.PARAMS_DEVICE_ID, mSession.getUser().getDeviceID());
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String jsonString) {
                try {
                    JSONObject json = new JSONObject(jsonString);
                    Map<String, Object> map = JSONHelper.JSONToMap(json);

                    if (map == null) {
                        handler.RunCompleted(Constants.USERCODE_OTHER_ERROR);
                    } else if (map.containsKey(Constants.API_ERROR_CODE)) {
                        int errorCode = (int) map.get(Constants.API_ERROR_CODE);
                        if (errorCode == 40)
                            Preferences.setPreference(Constants.KEY_PHONE, (String) map.get(AppConstants.PARAM_INFO), mContext);
                        handler.RunCompleted(errorCode);
                    } else if (map.containsKey(CODE_KEY)) {
                        handler.RunCompleted((int) map.get(CODE_KEY));
                    } else {
                        handler.RunCompleted(Constants.USERCODE_SUCCESS);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("JSON Error", "Error requesting start private chat.");
                    handler.RunCompleted(Constants.USERCODE_INVALID_RESPONSE);
                }
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Request Error", "Error sending private chat message."); //NON-NLS
                error.printStackTrace();
                handler.RunCompleted(-1);
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requestPrivateChatMessages(ChatActivity mActivity, Context mContext, String privateUserID, String messageId,
                                                  String limit, LVSession mSession, RequestQueue mRequestQueue,
                                                  final PrivateChatMessagesHandler handler) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Stream.Keys.RID, privateUserID);
        params.put("LastID", messageId);

//        params.put(ArtistChatMessage.Keys.LIMIT, limit);
        params.put(Constants.API_ACTION, Constants.ACTION_GET_PRIVATE_CHAT_MESSAGES);
        params.put(User.Keys.PARAMS_USER_ID, mSession.getUser().getUserID());
        params.put(User.Keys.PARAMS_DEVICE_ID, mSession.getUser().getDeviceID());
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String jsonString) {
                handler.onRunCompleted(PrivateChatMessageTranslator.getPrivateChatMessages(jsonString));
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Volley Error", "Error requesting chat messages.");
                handler.onRunCompleted(null);
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requestEndPrivateChat(String privateUserID, final ChatActivity mActivity, Context mContext,
                                             RequestQueue mRequestQueue, LVSession mSession) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_END_PRIVATE_CHAT);

        params.put(Stream.Keys.RID, privateUserID);

        params.put(User.Keys.USER_ID, mSession.getUser().getUserID());
        params.put(User.Keys.PARAMS_DEVICE_ID, mSession.getUser().getDeviceID());
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String jsonString) {
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Request Error", "Error sending private chat message."); //NON-NLS
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requestStartPrivateChat(String privateUserID, final ChatActivity mActivity,
                                               final Context mContext, RequestQueue mRequestQueue,
                                               LVSession mSession, final CodeNumberHandler handler) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_START_PRIVATE_CHAT);

        params.put(Stream.Keys.RID, privateUserID);

        params.put(User.Keys.USER_ID, mSession.getUser().getUserID());
        params.put(User.Keys.PARAMS_DEVICE_ID, mSession.getUser().getDeviceID());
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String jsonString) {
                try {
                    JSONObject json = new JSONObject(jsonString);
                    Map<String, Object> map = JSONHelper.JSONToMap(json);

                    if (map == null) {
                        handler.RunCompleted(Constants.USERCODE_OTHER_ERROR);
                    } else if (map.containsKey(Constants.API_ERROR_CODE)) {
                        int errorCode = (int) map.get(Constants.API_ERROR_CODE);
                        if (errorCode == 40)
                            Preferences.setPreference(Constants.KEY_PHONE, (String) map.get(AppConstants.PARAM_INFO), mContext);
                        handler.RunCompleted(errorCode);
                    } else if (map.containsKey(CODE_KEY)) {
                        handler.RunCompleted((int) map.get(CODE_KEY));
                    } else {
                        handler.RunCompleted(Constants.USERCODE_SUCCESS);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("JSON Error", "Error requesting start private chat.");
                    handler.RunCompleted(Constants.USERCODE_INVALID_RESPONSE);
                }
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Request Error", "Error sending private chat message."); //NON-NLS
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void requestUniquePictureID(final Activity mActivity, Context mContext,
                                              RequestQueue mRequestQueue, LVSession mSession,
                                              final StringResultHandler handler) {
        if (TextUtils.isNullOrEmpty(SplashActivity.mServerUrl))
            LVUtils.setServerUrl();

        Map<String, String> params = new HashMap<>();
        params.put(Constants.API_ACTION, Constants.ACTION_GET_UNIQUE_ID);
        params.put(User.Keys.USER_ID, mSession.getUser().getUserID());
        params.put(User.Keys.PARAMS_DEVICE_ID, mSession.getUser().getDeviceID());
        params.put(User.Keys.PARAMS_API_VERSION, Constants.API_VERSION);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String jsonString) {
                try {
                    JSONObject json = new JSONObject(jsonString);
                    Map<String, Object> map = JSONHelper.JSONToMap(json);
                    if (map.containsKey(ID_KEY)) {
                        handler.onRunCompleted((String) map.get(ID_KEY));
                    } else {
                        handler.onRunCompleted(null);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("JSON Error", "Error requesting whisper.");
                    handler.onRunCompleted(null);
                }
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Request Error", "Error gettting unique picture id."); //NON-NLS
                handler.onRunCompleted(null);
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, SplashActivity.mServerUrl, params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void getYouTubeAccessToken(String authCode, final YouTubeLoginData mYouTubeLoginData, final Activity mActivity,
                                             final Context mContext, final LVSession mSession,
                                             final RequestQueue mRequestQueue, final StringResultHandler handler) {
        Map<String, String> params = new HashMap<>();
        params.put(YouTubeLoginData.Keys.CODE, authCode);
        params.put(YouTubeLoginData.Keys.CLIENT_ID, mYouTubeLoginData.getClientID());
        params.put(YouTubeLoginData.Keys.REDIRECT_URI, mYouTubeLoginData.getRedirectURI());
        params.put(YouTubeLoginData.Keys.GRANT_TYPE, YouTubeLoginData.Keys.AUTHORIZATION_CODE);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String jsonString) {
                try {
                    JSONObject json = new JSONObject(jsonString);
                    Map<String, Object> map = JSONHelper.JSONToMap(json);
                    if (map.containsKey("access_token")) {
                        String refreshToken = (String) map.get("refresh_token");
                        Preferences.setPreference(AppConstants.KEY_YOUTUBE_REFRESH_TOKEN, refreshToken, mContext);

                        String accessToken = (String) map.get("access_token");
                        handler.onRunCompleted(accessToken);
                    } else {
                        handler.onRunCompleted(null);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("JSON Error", "Youtube access token");
                    handler.onRunCompleted(null);
                }
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Request Error", "Youtube access token");
                handler.onRunCompleted(null);
            }
        };

        LVStringRequest mRequest = new LVStringRequest(Request.Method.POST, mYouTubeLoginData.getTokenURI(), params, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            mSession.getDelayedAPIRequestList().add(mRequest);
            mActivity.startActivity(new Intent(mActivity, NoInternetActivity.class));
        }
    }

    public static void getYouTubeSubCount(final String accessToken, final Activity mActivity, final Context mContext,
                                          final RequestQueue mRequestQueue, final codeResultHandler handler) {
        Map<String, String> params = new HashMap<>();
        params.put(YouTubeLoginData.Keys.PART, YouTubeLoginData.Keys.ID);
        params.put(YouTubeLoginData.Keys.MY_SUBSCRIBERS, "true");
        params.put(YouTubeLoginData.Keys.ACCESS_TOKEN, accessToken);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String jsonString) {
                try {
                    JSONObject json = new JSONObject(jsonString);
                    JSONObject pageInfoObject = JSONHelper.getJSONObject(json, YouTubeLoginData.Keys.PAGE_INFO);
                    int subCount = Integer.valueOf(JSONHelper.getString(pageInfoObject, YouTubeLoginData.Keys.TOTAL_RESULTS));
                    handler.onRunCompleted(subCount);
                } catch (Throwable e) {
                    e.printStackTrace();
                    Log.e("JSON Error", "Youtube sub count");
                    handler.onRunCompleted(-1);
                }
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Request Error", "Youtube sub count");
                handler.onRunCompleted(-1);
            }
        };

        String mUrl = "https://www.googleapis.com/youtube/v3/subscriptions?" + UrlBuilder.buildUrlParams(params);
        StringRequest mRequest = new StringRequest(Request.Method.GET, mUrl, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            handler.onRunCompleted(-1);
        }
    }

    public static void getYouTubeUserDetails(final String accessToken, final Activity mActivity, final Context mContext,
                                             final RequestQueue mRequestQueue, final YouTubeUserDetailsHandler handler) {
        Map<String, String> params = new HashMap<>();
        params.put(YouTubeLoginData.Keys.PART, YouTubeLoginData.Keys.SNIPPET);
        params.put(YouTubeLoginData.Keys.MINE, "true");
        params.put(YouTubeLoginData.Keys.ACCESS_TOKEN, accessToken);

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String jsonString) {
                try {
                    JSONObject json = new JSONObject(jsonString);
                    JSONObject mItem = JSONHelper.getJSONArray(json, YouTubeLoginData.Keys.ITEMS).getJSONObject(0);
                    String socialID = JSONHelper.getString(mItem, YouTubeLoginData.Keys.ID);

                    JSONObject mSnippet = JSONHelper.getJSONObject(mItem, YouTubeLoginData.Keys.SNIPPET);
                    String displayName = JSONHelper.getString(mSnippet, YouTubeLoginData.Keys.TITLE);

                    handler.onRunCompleted(displayName, socialID);
                } catch (Throwable e) {
                    e.printStackTrace();
                    Log.e("JSON Error", "Youtube sub count");
                    handler.onRunCompleted(null, null);
                }
            }
        };

        Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Request Error", "Youtube sub count");
                handler.onRunCompleted(null, null);
            }
        };

        String mUrl = "https://www.googleapis.com/youtube/v3/channels?" + UrlBuilder.buildUrlParams(params);
        StringRequest mRequest = new StringRequest(Request.Method.GET, mUrl, response, error);
        if (AndroidUtils.isNetworkAvailable(mContext)) {
            mRequestQueue.add(mRequest);
        } else if (mActivity != null) {
            AndroidUtils.showShortToast(mActivity.getString(R.string.internet_issues), mActivity);
            handler.onRunCompleted(null, null);
        }
    }
}