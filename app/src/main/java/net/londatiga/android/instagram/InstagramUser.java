package net.londatiga.android.instagram;

import java.io.Serializable;

/**
 * Instagram user
 * 
 * @author Lorensius W. L. T
 *
 */
public class InstagramUser implements Serializable {
	public String id;
	public String username;
	public String fullName;
	public String profilPicture;
	public String accessToken;
	public int FollowerCount;
}